
create table seb.payment_reference_entry (
	payment_entry_id integer not null generated always as identity (start with 1, increment by 1, cache 20, no minvalue, no maxvalue, no cycle, no order),
	payment_id integer not null,
	contract_account_number varchar(12) not null,
	amount decimal(13,2) not null,
	billing_attempt_count integer null,
	billing_attempt_last_datetime timestamp null,
	billing_attempt_last_error varchar(1000) null,
	billed integer not null,
	primary key(payment_entry_id),
	foreign key payment_reference_entry_payment_id (payment_id) references seb.payment_reference on delete cascade
);

insert into seb.payment_reference_entry (payment_id, contract_account_number, amount, billing_attempt_count, billing_attempt_last_datetime, billing_attempt_last_error, billed) select payment_id, contract_account_number, amount, billing_attempt_count, billing_attempt_last_datetime, billing_attempt_last_error, case when status = 'BILLED' then 1 else 0 end from seb.payment_reference;
insert into seb.setting (setting_key, setting_value) values ('PAYMENT_CUTOFF_START', '23:30:00');
insert into seb.setting (setting_key, setting_value) values ('PAYMENT_CUTOFF_END', '00:00:00');

alter table seb.payment_reference drop column contract_account_number;
alter table seb.payment_reference drop column amount;
alter table seb.payment_reference drop column billing_attempt_count;
alter table seb.payment_reference drop column billing_attempt_last_datetime;
alter table seb.payment_reference drop column billing_attempt_last_error;

alter table seb.payment_reference add column notification_email varchar(50);
update seb.payment_reference set notification_email = user_email;
alter table seb.payment_reference alter column notification_email set not null;

-- db2 reorg table seb.payment_reference (when alter error 23 or 7) 





/*
 * 1. Changed hibernate.cfg.xml
 * 2. Changed Common.adapter
 */