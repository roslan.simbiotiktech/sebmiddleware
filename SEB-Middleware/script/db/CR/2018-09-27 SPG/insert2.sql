
update seb.setting set setting_value = '3' where setting_key = 'CONTRACT_MAX_SUBSCRIPTION';

alter table seb.payment_reference add column spg_bank_id int null;
alter table seb.payment_reference add column spg_bank_name varchar(200) null;
alter table seb.payment_reference add column spg_bank_type varchar(4) null;
alter table seb.payment_reference add column spg_card_type varchar(1) null;

-- must initialize SPG.fpx_table first with all the Bank Name 

update seb.payment_reference set spg_card_type = bank_id where gateway_id = 'MPG';
update seb.payment_reference as P set P.spg_bank_id = (SELECT bank_id from spg.fpx_bank where bank_fpx_id = P.bank_id and bank_type = 'B2C') where P.gateway_id = 'FPX';
update seb.payment_reference as P set P.spg_bank_name = (SELECT bank_name from spg.fpx_bank where bank_fpx_id = P.bank_id and bank_type = 'B2C') where P.gateway_id = 'FPX';
update seb.payment_reference as P set P.spg_bank_type = (SELECT bank_type from spg.fpx_bank where bank_fpx_id = P.bank_id and bank_type = 'B2C') where P.gateway_id = 'FPX';

alter table seb.payment_reference drop column bank_id;

insert into seb.payment_gateway (gateway_id, merchant_id, reference_prefix, min_amount, max_amount) values ('SPG', 'SEBCARES', 'S', 1.00, 1000000);
insert into seb.payment_gateway_url (gateway_id, url_name, url_value) values ('SPG', 'BANK_LIST_URL', 'http://10.110.30.66:8080/SEB-PaymentGateway/fpx/banks');
insert into seb.payment_gateway_url (gateway_id, url_name, url_value) values ('SPG', 'BANK_QUERY_URL', 'http://10.110.30.66:8080/SEB-PaymentGateway/fpx/bank');
insert into seb.payment_gateway_url (gateway_id, url_name, url_value) values ('SPG', 'RETURN_URL', 'https://sebcares.sarawakenergy.com.my/spg/indirect_page.html');
insert into seb.payment_gateway_url (gateway_id, url_name, url_value) values ('SPG', 'INDIRECT_URL', 'https://sebcares.sarawakenergy.com.my/eServices/index.html#/paynowStatus');
insert into seb.payment_gateway_url (gateway_id, url_name, url_value) values ('SPG', 'FPX_QUERY_URL', 'http://10.110.30.66:8080/SEB-PaymentGateway/fpx/query');
insert into seb.payment_gateway_url (gateway_id, url_name, url_value) values ('SPG', 'FPX_SUBMIT_URL', 'https://sebcares.sarawakenergy.com.my/sebpayment/fpx/payment');
insert into seb.payment_gateway_url (gateway_id, url_name, url_value) values ('SPG', 'MPG_QUERY_URL', 'http://10.110.30.66:8080/SEB-PaymentGateway/maybank/query');
insert into seb.payment_gateway_url (gateway_id, url_name, url_value) values ('SPG', 'MPG_SUBMIT_URL', 'https://sebcares.sarawakenergy.com.my/sebpayment/maybank/payment');

insert into seb.payment_gateway_parameter (gateway_id, param_name, param_value) values ('SPG', 'SIGNATURE_SECRET', '$8d5=%v9u&JRynXB');
insert into seb.payment_gateway_parameter (gateway_id, param_name, param_value) values ('SPG', 'PRODUCT_DESCRIPTION', 'SESCO Bill Payment');

-- db2 reorg table seb.payment_reference (when alter error 23 or 7) 