
alter table seb.payment_reference add column reference_prefix varchar(5) null;

update seb.payment_reference set reference_prefix = 'M' where gateway_id = 'MPG';
update seb.payment_reference set reference_prefix = 'F' where gateway_id = 'FPX';

update seb.payment_reference set reference_prefix = 'F' where gateway_id = 'SPG' and spg_bank_name is not null;
update seb.payment_reference set reference_prefix = 'M' where gateway_id = 'SPG' and spg_bank_name is null;

alter table seb.payment_reference alter column reference_prefix set not null;

-- db2 reorg table seb.payment_reference (when alter error 23 or 7)


alter table seb.report alter column loc_address set data type varchar(300);
db2 reorg table seb.report