alter table seb.billing_transaction drop primary key;

alter table seb.billing_transaction alter column billing_id set not null;

reorg table seb.billing_transaction;

alter table seb.billing_transaction add primary key (billing_id, invoice_number);