update seb.setting set setting_value = '3' where setting_key = 'CONTRACT_MAX_SUBSCRIPTION';

create table seb.sfdc_report (
	trans_id bigint not null generated always as identity (start with 1, increment by 1, cache 20, no minvalue, no maxvalue, no cycle, no order),
	case_number varchar(50) null,
	case_id varchar(50) null,
	classification varchar(255) null,
	report_type varchar(255) not null,
	category varchar(255) not null,
	issue_type varchar(15) null,
	status varchar(255) not null,
	sub_status varchar(255) null,
	previous_notified_status varchar(255) null,
	seb_status varchar(255) not null,
	contract_account_number varchar(12) null,
	remark varchar(500) null,
	channel varchar(20) not null,
	client_os varchar(255) not null,
	user_email varchar(50) not null,
	user_name varchar(160) not null,
	user_mobile_number varchar(20) not null,
	station varchar(20) null,
	description varchar(300) not null,
	loc_longitude varchar(15) null,
	loc_latitude varchar(15) null,
	loc_address varchar(120) null,
	photo_1 blob null,
	photo_1_type varchar(20) null,
	photo_2 blob null,
	photo_2_type varchar(20) null,
	photo_3 blob null,
	photo_3_type varchar(20) null,
	updated_datetime timestamp generated always for each row on update as row change timestamp not null,
	created_datetime timestamp with default current timestamp not null,
	deleted_flag integer not null,
	primary key (trans_id)
);

create index idx_sfdc_report_case_id on seb.sfdc_report (case_id);

alter table seb.user add column sfdc_id varchar(50) null;
alter table seb.user add column sfdc_updated_datetime timestamp null;
alter table seb.user add column sfdc_sync_datetime timestamp null;
create index idx_user_sfdc_id on seb.user (sfdc_id);
reorg table seb.user;

alter table seb.contract add column sfdc_id varchar(50) null;
create index idx_contract_sfdc_id on seb.contract (sfdc_id);
reorg table seb.contract;

alter table seb.subscription add column sfdc_id varchar(50) null;
alter table seb.subscription add column sfdc_updated_datetime timestamp null;
alter table seb.subscription add column sfdc_sync_datetime timestamp null;
create index idx_subscription_sfdc_id on seb.subscription (sfdc_id);
reorg table seb.subscription;

alter table seb.power_alert add column sfdc_id varchar(50) null;
reorg table seb.power_alert;

insert into seb.setting (setting_key, setting_value) values ('SF_INSTANCE_URL', 'https://test.salesforce.com');
insert into seb.setting (setting_key, setting_value) values ('SF_CLIENT_ID', '3MVG9e2mBbZnmM6nTNqVlELiUikb8p2HRge5g_g5oB_N2plyzmHjsSesn07qx51wcBKOzzqFBlaQFY45Qmyj6');
insert into seb.setting (setting_key, setting_value) values ('SF_CLIENT_SECRET', '4348439042887395801');

insert into seb.setting (setting_key, setting_value) values ('SF_USERNAME', 'sebc@lavaprotocols.com');
insert into seb.setting (setting_key, setting_value) values ('SF_PASSWORD', 'S@bC@repass123YjBnZF5HqH0XWMWrOV1p04FNB');

-- UAT Credentials
-- insert into seb.setting (setting_key, setting_value) values ('SF_API_CLIENT_ID', 'SFDCHUSR2');
-- insert into seb.setting (setting_key, setting_value) values ('SF_API_CLIENT_SECRET', 'ufyXsHs89XzusUgE');

-- Production Credentials
insert into seb.setting (setting_key, setting_value) values ('SF_API_CLIENT_ID', 'SFDCHUSR2');
insert into seb.setting (setting_key, setting_value) values ('SF_API_CLIENT_SECRET', 'xe0Z2Zc8jMsCaZlB');

alter table seb.subscription add notify_by_mobile_push integer not null with default 1;

create table seb.bill_ready_notification (
	notification_id integer not null generated always as identity (start with 1, increment by 1, cache 20, no minvalue, no maxvalue, no cycle, no order),
	user_email varchar(50) not null,
	text varchar(120) not null,
	contract_account_number varchar(12) not null,
	invoice_number varchar(12) not null,
	invoice_date timestamp not null,
	status varchar(10) not null,
	sending_datetime timestamp null,
	send_counter integer not null default 0,
	created_datetime timestamp with default current timestamp not null,
	primary key(notification_id)
);

create index idx_bill_ready_notification on seb.bill_ready_notification (status, sending_datetime, send_counter);

