
db2 "alter table seb.report add issue_type varchar(15) null"
db2 "alter table seb.report add deleted_flag integer not null with default"

db2 "delete from seb.setting where setting_key = 'POWER_ALERT_PUBLISH_DELAY'"

db2 "insert into seb.setting (setting_key, setting_value) values ('POWER_ALERT_UNSCHEDULE_REMOVE_HOURS', '24')"
db2 "insert into seb.setting (setting_key, setting_value) values ('POWER_ALERT_SCHEDULE_REMOVE_HOURS', '0')"


deploy Report.adapter