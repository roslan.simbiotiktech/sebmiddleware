alter table seb.user add remark varchar(500) null;

alter table seb.report add category varchar(10) null;
update seb.report set category = 'ENQUIRIES' where report_type = 'GENERAL_INQUIRY';

alter table seb.otp_verification add TYPE varchar(20) null;
update seb.otp_verification set TYPE = 'ALL';
alter table seb.otp_verification ALTER TYPE set not null;

REORG TABLE seb.otp_verification;

update seb.cust_service_location set region = 'KUCHING' where station = 'Bau';
update seb.cust_service_location set region = 'KUCHING' where station = 'Kota Samarahan';
update seb.cust_service_location set region = 'KUCHING' where station = 'Kuching';
update seb.cust_service_location set region = 'KUCHING' where station = 'Lundu';
update seb.cust_service_location set region = 'KUCHING' where station = 'Siburan';
update seb.cust_service_location set region = 'KUCHING' where station = 'Simujan';


update seb.cust_service_location set region = 'SERIAN' where station = 'Serian ';

update seb.cust_service_location set region = 'SRI_AMAN' where station = 'Betong';
update seb.cust_service_location set region = 'SRI_AMAN' where station = 'Pusa';
update seb.cust_service_location set region = 'SRI_AMAN' where station = 'Saratok';
update seb.cust_service_location set region = 'SRI_AMAN' where station = 'Sri Aman';


update seb.cust_service_location set region = 'SIBU' where station = 'Dalat';
update seb.cust_service_location set region = 'SIBU' where station = 'Daro';
update seb.cust_service_location set region = 'SIBU' where station = 'Kanowit';
update seb.cust_service_location set region = 'SIBU' where station = 'Kapit';
update seb.cust_service_location set region = 'SIBU' where station = 'Mukah';
update seb.cust_service_location set region = 'SIBU' where station = 'Sibu';

update seb.cust_service_location set region = 'SARIKEI' where station = 'Belawai';
update seb.cust_service_location set region = 'SARIKEI' where station = 'Bintangor';
update seb.cust_service_location set region = 'SARIKEI' where station = 'Sarikei';


update seb.cust_service_location set region = 'MIRI' where station = 'Bekenu';
update seb.cust_service_location set region = 'MIRI' where station = 'Marudi';
update seb.cust_service_location set region = 'MIRI' where station = 'Miri';

update seb.cust_service_location set region = 'BINTULU' where station = 'Bintulu';

update seb.cust_service_location set region = 'LIMBANG' where station = 'Lawas';
update seb.cust_service_location set region = 'LIMBANG' where station = 'Limbang';

alter table seb.subscription add notify_by_email integer not null with default 0;
alter table seb.subscription add notify_by_sms integer not null with default 0;

insert into seb.task_tracker (task_key, task_datetime) values ('BILL_READINESS_NOTIFICATION', CURRENT_TIMESTAMP);

alter table seb.billing_transaction add created_datetime timestamp with default current timestamp not null;
update seb.billing_transaction set created_datetime = invoice_date;
create index idx_billing_transaction_created_datetime on seb.billing_transaction (created_datetime);

insert into seb.setting (SETTING_KEY, SETTING_VALUE) values ('PAYNOW_URL', 'https://sebcares.sarawakenergy.com.my/eServices/index.html#/user/paynow/{ca}/summary');

create table seb.mobile_prefix (
	country_name varchar(100) not null,
	prefix varchar(10) not null,
	primary key(country_name)
);

insert into seb.mobile_prefix (country_name, prefix) values ('Afghanistan', '93');
insert into seb.mobile_prefix (country_name, prefix) values ('Albania', '355');
insert into seb.mobile_prefix (country_name, prefix) values ('Algeria', '213');
insert into seb.mobile_prefix (country_name, prefix) values ('America Samoa', '1684');
insert into seb.mobile_prefix (country_name, prefix) values ('Andorra', '376');
insert into seb.mobile_prefix (country_name, prefix) values ('Angola', '244');
insert into seb.mobile_prefix (country_name, prefix) values ('Anguilla', '1264');
insert into seb.mobile_prefix (country_name, prefix) values ('Antigua & Barbuda', '1268');
insert into seb.mobile_prefix (country_name, prefix) values ('Argetina', '54');
insert into seb.mobile_prefix (country_name, prefix) values ('Armenia', '374');
insert into seb.mobile_prefix (country_name, prefix) values ('Aruba', '297');
insert into seb.mobile_prefix (country_name, prefix) values ('Ascension Island', '247');
insert into seb.mobile_prefix (country_name, prefix) values ('Australia', '61');
insert into seb.mobile_prefix (country_name, prefix) values ('Austria', '43');
insert into seb.mobile_prefix (country_name, prefix) values ('Azerbaijan', '994');
insert into seb.mobile_prefix (country_name, prefix) values ('Bahamas', '1242');
insert into seb.mobile_prefix (country_name, prefix) values ('Bahrain', '973');
insert into seb.mobile_prefix (country_name, prefix) values ('Bangladesh', '880');
insert into seb.mobile_prefix (country_name, prefix) values ('Barbados', '1246');
insert into seb.mobile_prefix (country_name, prefix) values ('Belarus', '375');
insert into seb.mobile_prefix (country_name, prefix) values ('Belgium', '32');
insert into seb.mobile_prefix (country_name, prefix) values ('Belize', '501');
insert into seb.mobile_prefix (country_name, prefix) values ('Benin', '229');
insert into seb.mobile_prefix (country_name, prefix) values ('Bermuda', '1441');
insert into seb.mobile_prefix (country_name, prefix) values ('Bhutan', '975');
insert into seb.mobile_prefix (country_name, prefix) values ('Bolivia', '591');
insert into seb.mobile_prefix (country_name, prefix) values ('Bosnia & Herzegovina', '387');
insert into seb.mobile_prefix (country_name, prefix) values ('Botswana', '267');
insert into seb.mobile_prefix (country_name, prefix) values ('Brazil', '55');
insert into seb.mobile_prefix (country_name, prefix) values ('British Indian Ocean Territory', '246');
insert into seb.mobile_prefix (country_name, prefix) values ('British Virgin Islands', '1284');
insert into seb.mobile_prefix (country_name, prefix) values ('Brunei', '673');
insert into seb.mobile_prefix (country_name, prefix) values ('Bulgaria', '359');
insert into seb.mobile_prefix (country_name, prefix) values ('Burkina Faso', '226');
insert into seb.mobile_prefix (country_name, prefix) values ('Burundi', '257');
insert into seb.mobile_prefix (country_name, prefix) values ('Cambodia', '855');
insert into seb.mobile_prefix (country_name, prefix) values ('Cameroon', '237');
insert into seb.mobile_prefix (country_name, prefix) values ('Canada', '1');
insert into seb.mobile_prefix (country_name, prefix) values ('Cape Verde', '238');
insert into seb.mobile_prefix (country_name, prefix) values ('Caribbean Netherlands', '599');
insert into seb.mobile_prefix (country_name, prefix) values ('Cayman Islands', '1345');
insert into seb.mobile_prefix (country_name, prefix) values ('Central African Republic', '236');
insert into seb.mobile_prefix (country_name, prefix) values ('Chad', '235');
insert into seb.mobile_prefix (country_name, prefix) values ('Chile', '56');
insert into seb.mobile_prefix (country_name, prefix) values ('China', '86');
insert into seb.mobile_prefix (country_name, prefix) values ('Colombia', '57');
insert into seb.mobile_prefix (country_name, prefix) values ('Comoros', '269');
insert into seb.mobile_prefix (country_name, prefix) values ('Congo (DRC)', '243');
insert into seb.mobile_prefix (country_name, prefix) values ('Congo (Republic)', '242');
insert into seb.mobile_prefix (country_name, prefix) values ('Cook Islands', '682');
insert into seb.mobile_prefix (country_name, prefix) values ('Costa Rica', '506');
insert into seb.mobile_prefix (country_name, prefix) values ('Cote d''lvoire', '225');
insert into seb.mobile_prefix (country_name, prefix) values ('Croatia', '385');
insert into seb.mobile_prefix (country_name, prefix) values ('Cuba', '53');
insert into seb.mobile_prefix (country_name, prefix) values ('Curacao', '599');
insert into seb.mobile_prefix (country_name, prefix) values ('Cyprus', '357');
insert into seb.mobile_prefix (country_name, prefix) values ('Czech Republic', '420');
insert into seb.mobile_prefix (country_name, prefix) values ('Denmark', '45');
insert into seb.mobile_prefix (country_name, prefix) values ('Djibouti', '253');
insert into seb.mobile_prefix (country_name, prefix) values ('Dominica', '1767');
insert into seb.mobile_prefix (country_name, prefix) values ('Dominican Republic', '1809');
insert into seb.mobile_prefix (country_name, prefix) values ('Ecuador', '593');
insert into seb.mobile_prefix (country_name, prefix) values ('Egypt', '20');
insert into seb.mobile_prefix (country_name, prefix) values ('El Salvador', '503');
insert into seb.mobile_prefix (country_name, prefix) values ('Equatorial Guinea', '240');
insert into seb.mobile_prefix (country_name, prefix) values ('Eritrea', '291');
insert into seb.mobile_prefix (country_name, prefix) values ('Estonia', '372');
insert into seb.mobile_prefix (country_name, prefix) values ('Ethiopia', '251');
insert into seb.mobile_prefix (country_name, prefix) values ('Falkland Islands', '500');
insert into seb.mobile_prefix (country_name, prefix) values ('Faroe Islands', '298');
insert into seb.mobile_prefix (country_name, prefix) values ('Fiji', '679');
insert into seb.mobile_prefix (country_name, prefix) values ('Finland', '358');
insert into seb.mobile_prefix (country_name, prefix) values ('France', '33');
insert into seb.mobile_prefix (country_name, prefix) values ('French Guiana', '594');
insert into seb.mobile_prefix (country_name, prefix) values ('French Polynesia', '689');
insert into seb.mobile_prefix (country_name, prefix) values ('Gabon', '241');
insert into seb.mobile_prefix (country_name, prefix) values ('Gambia', '220');
insert into seb.mobile_prefix (country_name, prefix) values ('Georgia', '995');
insert into seb.mobile_prefix (country_name, prefix) values ('Germany', '49');
insert into seb.mobile_prefix (country_name, prefix) values ('Ghana', '233');
insert into seb.mobile_prefix (country_name, prefix) values ('Gibraltar', '350');
insert into seb.mobile_prefix (country_name, prefix) values ('Greece', '30');
insert into seb.mobile_prefix (country_name, prefix) values ('Greenland', '299');
insert into seb.mobile_prefix (country_name, prefix) values ('Grenada', '1473');
insert into seb.mobile_prefix (country_name, prefix) values ('Guadeloupe', '590');
insert into seb.mobile_prefix (country_name, prefix) values ('Guam', '1671');
insert into seb.mobile_prefix (country_name, prefix) values ('Guatemala', '502');
insert into seb.mobile_prefix (country_name, prefix) values ('Guinea', '224');
insert into seb.mobile_prefix (country_name, prefix) values ('Guinea-Bissau', '245');
insert into seb.mobile_prefix (country_name, prefix) values ('Guyana', '592');
insert into seb.mobile_prefix (country_name, prefix) values ('Haiti', '509');
insert into seb.mobile_prefix (country_name, prefix) values ('Honduras', '504');
insert into seb.mobile_prefix (country_name, prefix) values ('Hong Kong', '852');
insert into seb.mobile_prefix (country_name, prefix) values ('Hungary', '36');
insert into seb.mobile_prefix (country_name, prefix) values ('Iceland', '354');
insert into seb.mobile_prefix (country_name, prefix) values ('India', '91');
insert into seb.mobile_prefix (country_name, prefix) values ('Indonesia', '62');
insert into seb.mobile_prefix (country_name, prefix) values ('Iran', '98');
insert into seb.mobile_prefix (country_name, prefix) values ('Iraq', '964');
insert into seb.mobile_prefix (country_name, prefix) values ('Ireland', '353');
insert into seb.mobile_prefix (country_name, prefix) values ('Israel', '972');
insert into seb.mobile_prefix (country_name, prefix) values ('Italy', '39');
insert into seb.mobile_prefix (country_name, prefix) values ('Jamaica', '1876');
insert into seb.mobile_prefix (country_name, prefix) values ('Japan', '81');
insert into seb.mobile_prefix (country_name, prefix) values ('Jordan', '962');
insert into seb.mobile_prefix (country_name, prefix) values ('Kazakhstan', '7');
insert into seb.mobile_prefix (country_name, prefix) values ('Kenya', '254');
insert into seb.mobile_prefix (country_name, prefix) values ('Kiribati', '686');
insert into seb.mobile_prefix (country_name, prefix) values ('Kuwait', '965');
insert into seb.mobile_prefix (country_name, prefix) values ('Kyrgyzstan', '996');
insert into seb.mobile_prefix (country_name, prefix) values ('Laos', '856');
insert into seb.mobile_prefix (country_name, prefix) values ('Latvia', '371');
insert into seb.mobile_prefix (country_name, prefix) values ('Lebanon', '961');
insert into seb.mobile_prefix (country_name, prefix) values ('Lesotho', '266');
insert into seb.mobile_prefix (country_name, prefix) values ('Liberia', '231');
insert into seb.mobile_prefix (country_name, prefix) values ('Libya', '218');
insert into seb.mobile_prefix (country_name, prefix) values ('Liechtenstein', '423');
insert into seb.mobile_prefix (country_name, prefix) values ('Lithuania', '370');
insert into seb.mobile_prefix (country_name, prefix) values ('Luxembourg', '352');
insert into seb.mobile_prefix (country_name, prefix) values ('Macau', '853');
insert into seb.mobile_prefix (country_name, prefix) values ('Macedonia', '389');
insert into seb.mobile_prefix (country_name, prefix) values ('Madagascar', '261');
insert into seb.mobile_prefix (country_name, prefix) values ('Malawi', '265');
insert into seb.mobile_prefix (country_name, prefix) values ('Malaysia', '60');
insert into seb.mobile_prefix (country_name, prefix) values ('Maldives', '960');
insert into seb.mobile_prefix (country_name, prefix) values ('Mali', '223');
insert into seb.mobile_prefix (country_name, prefix) values ('Malta', '356');
insert into seb.mobile_prefix (country_name, prefix) values ('Marshall Islands', '692');
insert into seb.mobile_prefix (country_name, prefix) values ('Martinique', '596');
insert into seb.mobile_prefix (country_name, prefix) values ('Mauritania', '222');
insert into seb.mobile_prefix (country_name, prefix) values ('Mauritius', '230');
insert into seb.mobile_prefix (country_name, prefix) values ('Mexico', '52');
insert into seb.mobile_prefix (country_name, prefix) values ('Micronesia', '691');
insert into seb.mobile_prefix (country_name, prefix) values ('Moldova', '373');
insert into seb.mobile_prefix (country_name, prefix) values ('Monaco', '377');
insert into seb.mobile_prefix (country_name, prefix) values ('Mongolia', '976');
insert into seb.mobile_prefix (country_name, prefix) values ('Montenegro', '382');
insert into seb.mobile_prefix (country_name, prefix) values ('Montserrat', '1664');
insert into seb.mobile_prefix (country_name, prefix) values ('Morocco', '212');
insert into seb.mobile_prefix (country_name, prefix) values ('Mozambique', '258');
insert into seb.mobile_prefix (country_name, prefix) values ('Myanmar', '95');
insert into seb.mobile_prefix (country_name, prefix) values ('Namibia', '264');
insert into seb.mobile_prefix (country_name, prefix) values ('Nauru', '674');
insert into seb.mobile_prefix (country_name, prefix) values ('Nepal', '977');
insert into seb.mobile_prefix (country_name, prefix) values ('Netherlands', '31');
insert into seb.mobile_prefix (country_name, prefix) values ('New Caledonia', '687');
insert into seb.mobile_prefix (country_name, prefix) values ('New Zealand', '64');
insert into seb.mobile_prefix (country_name, prefix) values ('Nicaragua', '505');
insert into seb.mobile_prefix (country_name, prefix) values ('Niger', '227');
insert into seb.mobile_prefix (country_name, prefix) values ('Nigeria', '234');
insert into seb.mobile_prefix (country_name, prefix) values ('Niue', '683');
insert into seb.mobile_prefix (country_name, prefix) values ('Norfolk Island', '6723');
insert into seb.mobile_prefix (country_name, prefix) values ('Northern Mariana Islands', '1');
insert into seb.mobile_prefix (country_name, prefix) values ('North Korea', '850');
insert into seb.mobile_prefix (country_name, prefix) values ('Norway', '47');
insert into seb.mobile_prefix (country_name, prefix) values ('Oman', '968');
insert into seb.mobile_prefix (country_name, prefix) values ('Pakistan', '92');
insert into seb.mobile_prefix (country_name, prefix) values ('Palau', '680');
insert into seb.mobile_prefix (country_name, prefix) values ('Palestine', '970');
insert into seb.mobile_prefix (country_name, prefix) values ('Panama', '507');
insert into seb.mobile_prefix (country_name, prefix) values ('Papua New Guinea', '675');
insert into seb.mobile_prefix (country_name, prefix) values ('Paraguay', '595');
insert into seb.mobile_prefix (country_name, prefix) values ('Peru', '51');
insert into seb.mobile_prefix (country_name, prefix) values ('Philippines', '63');
insert into seb.mobile_prefix (country_name, prefix) values ('Poland', '48');
insert into seb.mobile_prefix (country_name, prefix) values ('Portugal', '351');
insert into seb.mobile_prefix (country_name, prefix) values ('Puerto Rico', '1787');
insert into seb.mobile_prefix (country_name, prefix) values ('Qatar', '974');
insert into seb.mobile_prefix (country_name, prefix) values ('Reunion', '262');
insert into seb.mobile_prefix (country_name, prefix) values ('Romania', '40');
insert into seb.mobile_prefix (country_name, prefix) values ('Russia', '7');
insert into seb.mobile_prefix (country_name, prefix) values ('Rwanda', '250');
insert into seb.mobile_prefix (country_name, prefix) values ('Samoa', '685');
insert into seb.mobile_prefix (country_name, prefix) values ('San Marino', '378');
insert into seb.mobile_prefix (country_name, prefix) values ('Sao Tome & Principe', '239');
insert into seb.mobile_prefix (country_name, prefix) values ('Saudi Arabia', '966');
insert into seb.mobile_prefix (country_name, prefix) values ('Senegal', '221');
insert into seb.mobile_prefix (country_name, prefix) values ('Serbia', '381');
insert into seb.mobile_prefix (country_name, prefix) values ('Seychelles', '248');
insert into seb.mobile_prefix (country_name, prefix) values ('Sierra Leone', '232');
insert into seb.mobile_prefix (country_name, prefix) values ('Singapore', '65');
insert into seb.mobile_prefix (country_name, prefix) values ('Sint Maarten', '1721');
insert into seb.mobile_prefix (country_name, prefix) values ('Slovakia', '421');
insert into seb.mobile_prefix (country_name, prefix) values ('Slovenia', '386');
insert into seb.mobile_prefix (country_name, prefix) values ('Solomon Islands', '677');
insert into seb.mobile_prefix (country_name, prefix) values ('Somalia', '252');
insert into seb.mobile_prefix (country_name, prefix) values ('South Africa', '27');
insert into seb.mobile_prefix (country_name, prefix) values ('South Korea', '82');
insert into seb.mobile_prefix (country_name, prefix) values ('South Sudan', '211');
insert into seb.mobile_prefix (country_name, prefix) values ('Spain', '34');
insert into seb.mobile_prefix (country_name, prefix) values ('Sri Lanka', '94');
insert into seb.mobile_prefix (country_name, prefix) values ('St. Barthelemy', '590');
insert into seb.mobile_prefix (country_name, prefix) values ('St. Helena', '290');
insert into seb.mobile_prefix (country_name, prefix) values ('St. Kitts & Nevis', '1869');
insert into seb.mobile_prefix (country_name, prefix) values ('St. Lucia', '1758');
insert into seb.mobile_prefix (country_name, prefix) values ('St. Martin', '590');
insert into seb.mobile_prefix (country_name, prefix) values ('St. Pierre & Miquelon', '508');
insert into seb.mobile_prefix (country_name, prefix) values ('St. Vincent & Grenadines', '1784');
insert into seb.mobile_prefix (country_name, prefix) values ('Sudan', '249');
insert into seb.mobile_prefix (country_name, prefix) values ('Suriname', '597');
insert into seb.mobile_prefix (country_name, prefix) values ('Swaziland', '268');
insert into seb.mobile_prefix (country_name, prefix) values ('Sweden', '46');
insert into seb.mobile_prefix (country_name, prefix) values ('Switzerland', '41');
insert into seb.mobile_prefix (country_name, prefix) values ('Syria', '963');
insert into seb.mobile_prefix (country_name, prefix) values ('Taiwan', '886');
insert into seb.mobile_prefix (country_name, prefix) values ('Tajikistan', '992');
insert into seb.mobile_prefix (country_name, prefix) values ('Tanzania', '255');
insert into seb.mobile_prefix (country_name, prefix) values ('Thailand', '66');
insert into seb.mobile_prefix (country_name, prefix) values ('Timor-Leste', '670');
insert into seb.mobile_prefix (country_name, prefix) values ('Togo', '228');
insert into seb.mobile_prefix (country_name, prefix) values ('Tokelau', '690');
insert into seb.mobile_prefix (country_name, prefix) values ('Tonga', '676');
insert into seb.mobile_prefix (country_name, prefix) values ('Trinidad & Tobago', '1868');
insert into seb.mobile_prefix (country_name, prefix) values ('Tunisia', '216');
insert into seb.mobile_prefix (country_name, prefix) values ('Turkey', '90');
insert into seb.mobile_prefix (country_name, prefix) values ('Turkmenistan', '993');
insert into seb.mobile_prefix (country_name, prefix) values ('Turks & Caicos Islands', '1649');
insert into seb.mobile_prefix (country_name, prefix) values ('Tuvalu', '688');
insert into seb.mobile_prefix (country_name, prefix) values ('U.S. Virgin Islands', '1340');
insert into seb.mobile_prefix (country_name, prefix) values ('Uganda', '256');
insert into seb.mobile_prefix (country_name, prefix) values ('Ukraine', '380');
insert into seb.mobile_prefix (country_name, prefix) values ('United Arab Emirates', '971');
insert into seb.mobile_prefix (country_name, prefix) values ('United Kingdom', '44');
insert into seb.mobile_prefix (country_name, prefix) values ('United States', '1');
insert into seb.mobile_prefix (country_name, prefix) values ('Uruguay', '598');
insert into seb.mobile_prefix (country_name, prefix) values ('Uzbekistan', '998');
insert into seb.mobile_prefix (country_name, prefix) values ('Vanuatu', '678');
insert into seb.mobile_prefix (country_name, prefix) values ('Vatican City', '379');
insert into seb.mobile_prefix (country_name, prefix) values ('Venezuela', '58');
insert into seb.mobile_prefix (country_name, prefix) values ('Vietnam', '84');
insert into seb.mobile_prefix (country_name, prefix) values ('Wallis & Futuna', '681');
insert into seb.mobile_prefix (country_name, prefix) values ('Yemen', '967');
insert into seb.mobile_prefix (country_name, prefix) values ('Zambia', '260');
insert into seb.mobile_prefix (country_name, prefix) values ('Zimbabwe', '263');

