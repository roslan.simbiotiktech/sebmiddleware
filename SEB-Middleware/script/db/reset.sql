truncate table user immediate
truncate table subscription immediate
truncate table remember_me immediate
truncate table customer_audit_trail immediate
truncate table email_verification immediate
truncate table mobile_verification immediate
truncate table otp_verification immediate
truncate table report immediate
truncate table report_notification immediate
truncate table notification_history immediate
truncate table tag_subscriptions immediate


-- Delete User
delete from user where user_id = 'abbysie@sarawakenergy.com.my'
delete from subscription where user_email = 'abbysie@sarawakenergy.com.my'
delete from remember_me where user_email = 'abbysie@sarawakenergy.com.my'
delete from customer_audit_trail where user_id = 'abbysie@sarawakenergy.com.my'
delete from email_verification where user_email = 'abbysie@sarawakenergy.com.my'
delete from otp_verification where user_email = 'abbysie@sarawakenergy.com.my'
delete from report where user_email = 'abbysie@sarawakenergy.com.my'
delete from report_notification where user_email = 'abbysie@sarawakenergy.com.my'
delete from notification_history where user_email = 'abbysie@sarawakenergy.com.my'
delete from tag_subscriptions where user_email = 'abbysie@sarawakenergy.com.my'

delete from ldap...

-- Restore Old Data For User
update user set status = 'PENDING', user_id = 'tomin' , existing_email = 'paul.kanwj@sarawakenergy.com.my' where user_id = 'paul.kanwj@sarawakenergy.com.my'
update subscription set status = 'INACTIVE', user_email = 'tomin' where user_email = 'paul.kanwj@sarawakenergy.com.my'
delete from remember_me where user_email = 'paul.kanwj@sarawakenergy.com.my'
delete from customer_audit_trail where user_id = 'paul.kanwj@sarawakenergy.com.my'
delete from email_verification where user_email = 'paul.kanwj@sarawakenergy.com.my'
delete from otp_verification where user_email = 'paul.kanwj@sarawakenergy.com.my'
delete from report where user_email = 'paul.kanwj@sarawakenergy.com.my'
delete from report_notification where user_email = 'paul.kanwj@sarawakenergy.com.my'
delete from notification_history where user_email = 'paul.kanwj@sarawakenergy.com.my'
delete from tag_subscriptions where user_email = 'paul.kanwj@sarawakenergy.com.my'



db2 "update user set status = 'PENDING', user_id = 'abbysie', existing_email = 'abbysie@sarawakenergy.com.my', existing_password = '5ae7baf3f66f0796d224ac949d4e5bf6bdf45238cf3da032f2a6ca840365e66e' where user_id = 'abbysie@sarawakenergy.com.my'"
db2 "update subscription set status = 'INACTIVE', user_email = 'abbysie' where user_email = 'abbysie@sarawakenergy.com.my'"
db2 "delete from remember_me where user_email = 'abbysie@sarawakenergy.com.my'"
db2 "delete from customer_audit_trail where user_id = 'abbysie@sarawakenergy.com.my'"
db2 "delete from email_verification where user_email = 'abbysie@sarawakenergy.com.my'"
db2 "delete from otp_verification where user_email = 'abbysie@sarawakenergy.com.my'"
db2 "delete from report where user_email = 'abbysie@sarawakenergy.com.my'"
db2 "delete from report_notification where user_email = 'abbysie@sarawakenergy.com.my'"
db2 "delete from notification_history where user_email = 'abbysie@sarawakenergy.com.my'"
db2 "delete from tag_subscriptions where user_email = 'abbysie@sarawakenergy.com.my'"



-- Restore Old Data (Hwa version)
db2 "select * from user where user_id = 'paulkan89@yahoo.com'"

db2 "update user set status = 'PENDING', user_id = 'tomin' where user_id = 'paulkan89@yahoo.com'"
db2 "update subscription set status = 'INACTIVE', user_email = 'tomin' where user_email = 'paulkan89@yahoo.com'"

db2 "update user set status = 'PENDING', user_id = 'abbysie' where user_id = 'abbysie@sarawakenergy.com.my'"
db2 "update subscription set status = 'INACTIVE', user_email = 'abbysie' where user_email = 'abbysie@sarawakenergy.com.my'"

db2 "update user set status = 'PENDING', user_id = 'corb89' where user_id = 'corbin.atek@sarawakenergy.com.my'"
db2 "update subscription set status = 'INACTIVE', user_email = 'corb89' where user_email = 'corbin.atek@sarawakenergy.com.my'"

db2 "update user set status = 'PENDING', user_id = 'birdypk' where user_id = 'birdypeter@gmail.com'"
db2 "update subscription set status = 'INACTIVE', user_email = 'birdypk' where user_email = 'birdypeter@gmail.com'"

db2 "update user set status = 'PENDING', user_id = 'chrlyn' where user_id = 'chrlyn85@gmail.com'"
db2 "update subscription set status = 'INACTIVE', user_email = 'chrlyn' where user_email = 'chrlyn85@gmail.com'"


db2 "delete from remember_me where user_email in ('paulkan89@yahoo.com', 'abbysie@sarawakenergy.com.my', 'corbin.atek@sarawakenergy.com.my', 'chrlyn85@gmail.com')"
db2 "delete from customer_audit_trail where user_id in ('paulkan89@yahoo.com', 'abbysie@sarawakenergy.com.my', 'corbin.atek@sarawakenergy.com.my', 'chrlyn85@gmail.com')"
db2 "delete from email_verification where user_email in ('paulkan89@yahoo.com', 'abbysie@sarawakenergy.com.my', 'corbin.atek@sarawakenergy.com.my', 'chrlyn85@gmail.com')"
db2 "delete from otp_verification where user_email in ('paulkan89@yahoo.com', 'abbysie@sarawakenergy.com.my', 'corbin.atek@sarawakenergy.com.my', 'chrlyn85@gmail.com')"
db2 "delete from report where user_email in ('paulkan89@yahoo.com', 'abbysie@sarawakenergy.com.my', 'corbin.atek@sarawakenergy.com.my', 'chrlyn85@gmail.com')"
db2 "delete from report_notification where user_email in ('paulkan89@yahoo.com', 'abbysie@sarawakenergy.com.my', 'corbin.atek@sarawakenergy.com.my', 'chrlyn85@gmail.com')"
db2 "delete from notification_history where user_email in ('paulkan89@yahoo.com', 'abbysie@sarawakenergy.com.my', 'corbin.atek@sarawakenergy.com.my', 'chrlyn85@gmail.com')"
db2 "delete from tag_subscriptions where user_email in ('paulkan89@yahoo.com', 'abbysie@sarawakenergy.com.my', 'corbin.atek@sarawakenergy.com.my', 'chrlyn85@gmail.com')"