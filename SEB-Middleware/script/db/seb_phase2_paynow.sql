
create table seb.fpx_bank (
        bank_id varchar(20) not null,
        description varchar(64) not null,
        image_path varchar(500) not null,
        bank_type varchar(4) not null,
        primary key(bank_id)
);

insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('ABB0232', 'Affin Bank Berhad', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2C/ABB0232_02.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('ABB0233', 'Affin Bank Berhad', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2C/ABB0232_02.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('ABMB0212', 'Alliance Bank Malaysia Berhad ', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2C/ABMB0212.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('ABMB0213', 'Alliance Bank Malaysia Berhad ', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2C/ABMB0212.png', 'B2B1');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('AMBB0208', 'AmBank Berhad', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2B/AMBB0208_02.png', 'B2B1');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('AMBB0209', 'AmBank Berhad', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2C/AMBB0209_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('BCBB0235', 'CIMB Bank Berhad', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2C/BCBB0235_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('BIMB0340', 'Bank Islam Malaysia Berhad', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2C/BIMB0340_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('BKR1602', 'Bank Kerjasama Rakyat Malaysia Berhad', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2C/BKR.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('BKRM0602', 'Bank Kerjasama Rakyat Malaysia Berhad', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2C/BKR.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('BMMB0341', 'Bank Muamalat Malaysia Berhad', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2C/BMMB0341_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('HLB0224', 'Hong Leong Bank Berhad', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2C/HLB0224_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('HLB0225', 'Hong Leong Bank Berhad', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2B/HLB0224_02.png', 'B2B1');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('MB2U0227', 'Malayan Banking Berhad (M2U)', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2C/MB2U0227_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('MBB0227', 'Malayan Banking Berhad (M2E)', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2B/MBB0227_02.png', 'B2B1');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('MBB0228', 'Malayan Banking Berhad (M2E)', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2B/MBB0227_02.png', 'B2B1');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('PBB0233', 'Public Bank Berhad', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2C/PBB0233_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('RHB0218', 'RHB Bank Berhad', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2C/RHB0218_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('SCB0214', 'Standard Chartered Bank', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2C/SCB0216_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('SCB0215', 'Standard Chartered Bank', 'https://sebcares.sarawakenergy.com.my/FPXIcons/B2C/SCB0216_01.png', 'B2B1');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('TEST0021', 'Test Bank A SBI Positive Scenario', 'https://sebcares.sarawakenergy.com.my/FPXIcons/TEST/TEST0012_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('TEST0022', 'Test Bank B SBI Positive Scenario', 'https://sebcares.sarawakenergy.com.my/FPXIcons/TEST/TEST0012_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('TEST0023', 'Test Bank C SBI Negative Scenario', 'https://sebcares.sarawakenergy.com.my/FPXIcons/TEST/TEST0012_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('TEST0024', 'Test Bank D SBI Negative Scenario', 'https://sebcares.sarawakenergy.com.my/FPXIcons/TEST/TEST0012_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('TEST0025', 'Test Bank E SBI B2B2 Transaction', 'https://sebcares.sarawakenergy.com.my/FPXIcons/TEST/TEST0012_01.png', 'B2B1');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('TEST0001', 'Test Bank A Non SBI Positive Scenario', 'https://sebcares.sarawakenergy.com.my/FPXIcons/TEST/TEST0012_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('TEST0002', 'Test Bank B Non SBI Positive Scenario', 'https://sebcares.sarawakenergy.com.my/FPXIcons/TEST/TEST0012_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('TEST0003', 'Test Bank C Non SBI Negative Scenario', 'https://sebcares.sarawakenergy.com.my/FPXIcons/TEST/TEST0012_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('TEST0004', 'Test Bank D Non SBI Negative Scenario', 'https://sebcares.sarawakenergy.com.my/FPXIcons/TEST/TEST0012_01.png', 'B2C');
insert into seb.fpx_bank (bank_id, description, image_path, bank_type) values ('TEST0005', 'Test Bank E Non SBI B2B2 Transaction', 'https://sebcares.sarawakenergy.com.my/FPXIcons/TEST/TEST0012_01.png', 'B2B1');

delete from seb.fpx_bank where bank_type != 'B2C';

create table seb.fpx_bank_active_list (
        bank_id varchar(20) not null,
        status varchar(20) not null,
        primary key(bank_id)
);

insert into seb.fpx_bank_active_list select bank_id, 'A' from seb.fpx_bank;

create view seb.fpx_bank_view (bank_id, description, image_path, bank_type) as select fb.bank_id, fb.description, fb.image_path, fb.bank_type from seb.fpx_bank fb join seb.fpx_bank_active_list fbal on fb.bank_id = fbal.bank_id where fbal.status = 'A';

create table seb.payment_gateway (
	gateway_id varchar(20) not null,
	merchant_id varchar(256) not null,
	reference_prefix varchar(5) not null,
	primary key(gateway_id)
);

insert into seb.payment_gateway (gateway_id, merchant_id, reference_prefix) values ('FPX', 'SE00026402', 'F');
insert into seb.payment_gateway (gateway_id, merchant_id, reference_prefix) values ('MPG', '02700770364875001454', 'M');

alter table seb.payment_gateway add min_amount decimal(13,2) not null default 1.0;
alter table seb.payment_gateway add max_amount decimal(13,2) not null default 30000.0;

update seb.payment_gateway set max_amount = 5000 where gateway_id = 'MPG';

create table seb.payment_gateway_url (
	gateway_id varchar(20) not null,
	url_name varchar(128) not null,
	url_value varchar(256) not null,
	primary key(gateway_id, url_name),
	foreign key payment_gateway_url_gateway_id (gateway_id) references seb.payment_gateway on delete cascade
);

insert into seb.payment_gateway_url (gateway_id, url_name, url_value) values ('FPX', 'SUBMIT_URL', 'https://www.mepsfpx.com.my/FPXMain/seller2DReceiver.jsp');
insert into seb.payment_gateway_url (gateway_id, url_name, url_value) values ('FPX', 'QUERY_URL', 'https://www.mepsfpx.com.my/FPXMain/sellerNVPTxnStatus.jsp');
insert into seb.payment_gateway_url (gateway_id, url_name, url_value) values ('FPX', 'INDIRECT_URL', 'http://10.110.30.51/eServices/index.html#/paynowStatus');
insert into seb.payment_gateway_url (gateway_id, url_name, url_value) values ('FPX', 'BANK_LIST_URL', 'https://www.mepsfpx.com.my/FPXMain/RetrieveBankList');

insert into seb.payment_gateway_url (gateway_id, url_name, url_value) values ('MPG', 'SUBMIT_URL', 'https://cards.maybank.com/BPG/admin/payment/PaymentInterface.jsp');
insert into seb.payment_gateway_url (gateway_id, url_name, url_value) values ('MPG', 'RETURN_URL', 'https://sebcares.sarawakenergy.com.my/mpg/indirect_page.html');
insert into seb.payment_gateway_url (gateway_id, url_name, url_value) values ('MPG', 'QUERY_URL', 'https://cards.maybank.com/BPG/admin/payment/process');
insert into seb.payment_gateway_url (gateway_id, url_name, url_value) values ('MPG', 'INDIRECT_URL', 'http://10.110.30.51/eServices/index.html#/paynowStatus');

create table seb.payment_gateway_parameter (
	gateway_id varchar(20) not null,
	param_name varchar(50) not null,
	param_value varchar(256) not null,
	primary key(gateway_id, param_name),
	foreign key payment_gateway_parameter_gateway_id (gateway_id) references seb.payment_gateway on delete cascade
);

insert into seb.payment_gateway_parameter (gateway_id, param_name, param_value) values ('FPX', 'EXCHANGE_ID', 'EX00006342');
insert into seb.payment_gateway_parameter (gateway_id, param_name, param_value) values ('FPX', 'SELLER_BANK_CODE', '01');
insert into seb.payment_gateway_parameter (gateway_id, param_name, param_value) values ('FPX', 'PRODUCT_DESCRIPTION', 'SESCO Bill Payment');
insert into seb.payment_gateway_parameter (gateway_id, param_name, param_value) values ('FPX', 'VERSION', '5.0');
insert into seb.payment_gateway_parameter (gateway_id, param_name, param_value) values ('FPX', 'KEY_PATH', '/opt/application/certificates/fpx/EX00006342.key');
insert into seb.payment_gateway_parameter (gateway_id, param_name, param_value) values ('FPX', 'PUBLIC_KEY_PATH', '/opt/application/certificates/fpx/');
insert into seb.payment_gateway_parameter (gateway_id, param_name, param_value) values ('FPX', 'MAIN_CERTIFICATE', 'fpxprod.cer');
insert into seb.payment_gateway_parameter (gateway_id, param_name, param_value) values ('FPX', 'CURRENT_CERTIFICATE', 'fpxprod_current.cer');

insert into seb.payment_gateway_parameter (gateway_id, param_name, param_value) values ('MPG', 'HASH_KEY', 'h9CJTrpK');
insert into seb.payment_gateway_parameter (gateway_id, param_name, param_value) values ('MPG', 'TRANSACTION_DESCRIPTION', 'SESCO Bill Payment');
insert into seb.payment_gateway_parameter (gateway_id, param_name, param_value) values ('MPG', 'MERCHANT_ID_AMEX', '02701700467275100309');
insert into seb.payment_gateway_parameter (gateway_id, param_name, param_value) values ('MPG', 'HASH_KEY_AMEX', 'mOkG4lTb');

create table seb.payment_reference (
	payment_id integer not null generated always as identity (start with 500000, increment by 1, cache 20, no minvalue, no maxvalue, no cycle, no order),
	gateway_id varchar(20) not null,
	bank_id varchar(20) null,
	status varchar(10) not null,
	user_email varchar(50) not null, 
	contract_account_number varchar(12) not null,
	amount decimal(13,2) not null,
	is_mobile integer not null with default,
	gateway_ref varchar (50) null, 
	gateway_status varchar (50) null,
	payment_datetime timestamp null,
	billing_attempt_count integer null,
	billing_attempt_last_datetime timestamp null,
	billing_attempt_last_error varchar(1000) null,
	updated_datetime timestamp generated by default for each row on update as row change timestamp not null,
	created_datetime timestamp with default current timestamp not null,
	primary key(payment_id)
);

create index idx_payment_reference on seb.payment_reference (status);

create table seb.payment_reference_data (
	payment_id integer not null,
	name varchar(50) not null,
	value varchar(4096) not null,
	primary key(payment_id, name),
	foreign key payment_reference_data_payment_id (payment_id) references seb.payment_reference on delete cascade
);

create table seb.billing_attempt_matrix (
    attempt_from integer not null,
    attempt_to integer not null,
    retry_minutes integer not null,
	primary key(attempt_from, attempt_to)
);

insert into seb.billing_attempt_matrix (attempt_from, attempt_to, retry_minutes) values (0, 1, 3);
insert into seb.billing_attempt_matrix (attempt_from, attempt_to, retry_minutes) values (2, 5, 10);
insert into seb.billing_attempt_matrix (attempt_from, attempt_to, retry_minutes) values (6, 10, 30);
insert into seb.billing_attempt_matrix (attempt_from, attempt_to, retry_minutes) values (11, 20, 60);
