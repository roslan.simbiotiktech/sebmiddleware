List All
& ldapsearch -x -b "ou=Customer,dc=seb,dc=com.my"

Search by ID
& ldapsearch -x -b "ou=Customer,dc=seb,dc=com.my" -s sub "email=name@email*"

Delete by ID
& ldapdelete "email=hah@gmail.com,ou=Customer,dc=seb,dc=com.my" -D 'cn=Manager,dc=seb,dc=com.my' -W

