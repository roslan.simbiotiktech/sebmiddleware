package com.seb.middleware.api;

import java.io.BufferedOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class API extends APIBase {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7168789569016987491L;
	private static final Logger logger = LogManager.getLogger(API.class);
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		doPost(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		long start = System.currentTimeMillis();
		logger.info("Received new API request.");
		APIRequest apiRequest = digestRequest(request);
		
		logger.debug(apiRequest);
		Router router = new Router(apiRequest);
		APIResponse apiResponse = router.process();
		logger.debug(apiResponse);
		
		try {
			doResponse(response, apiResponse);
		} catch (IOException e) {
			logger.error("Error writing response to API", e);
		}
		long total = System.currentTimeMillis() - start;
		logger.info("API request completed. Time taken " + total + "ms.");
	}
	
	private void doResponse(HttpServletResponse response, APIResponse apiResponse) throws IOException{
		response.setStatus(apiResponse.getStatus());
		BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
		out.write(apiResponse.getBody().getBytes("UTF-8"));
		out.close();
	}
	
	private APIRequest digestRequest(HttpServletRequest request){
		
		APIRequest apiRequest = new APIRequest();
		apiRequest.setUrl(request.getRequestURL().toString());
		apiRequest.setPathInfo(getPathInfo(request));
		apiRequest.setHeaders(getHeaders(request));
		apiRequest.setGetParameters(getUrlParameters(request));
		apiRequest.setBody(getRequestBody(request));
		return apiRequest;
		
	}
	
}
