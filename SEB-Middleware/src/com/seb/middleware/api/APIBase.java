package com.seb.middleware.api;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class APIBase extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7855226462053439045L;
	private static final Logger logger = LogManager.getLogger(APIBase.class);
	
	protected String [] getPathInfo(HttpServletRequest request){
		if(request.getPathInfo() == null) return null;
		
		StringBuilder pathInfo = new StringBuilder(request.getPathInfo().trim());
		cleanPath(pathInfo);

		if(pathInfo.length() == 0){
			return null;
		}else{
			return pathInfo.toString().split("/");
		}
	}
	
	private void cleanPath(StringBuilder pathInfo){
		for(int i = 0; i < pathInfo.length(); i++){
			char c = pathInfo.charAt(i);
			if(c == '/'){
				pathInfo.deleteCharAt(i--);
			}else{
				break;
			}
		}
		
		for(int i = pathInfo.length() - 1; i >= 0; i--){
			char c = pathInfo.charAt(i);
			if(c == '/'){
				pathInfo.deleteCharAt(i);
			}else{
				break;
			}
		}
	}
	
	protected HashMap<String, String[]> getHeaders(HttpServletRequest request){
		HashMap<String, String[]> map = new HashMap<String, String[]>();
		
		Enumeration<String> headerNames = request.getHeaderNames();
		while(headerNames.hasMoreElements()){
			String next = headerNames.nextElement();
			Enumeration<String> values = request.getHeaders(next);
			if(values.hasMoreElements()){
				String [] headerValues = new String[]{values.nextElement()};
				while(values.hasMoreElements()){
					headerValues = addArray(headerValues, values.nextElement());
				}
				
				map.put(next, headerValues);
			}
		}
		
		return map;
	}
	
	protected HashMap<String, String[]> getUrlParameters(HttpServletRequest request){
		HashMap<String, String[]> map = new HashMap<String, String[]>();
		
		if(request.getQueryString() != null && !request.getQueryString().isEmpty()){
			try {
				String decoded = URLDecoder.decode(request.getQueryString(), "UTF-8");
				String [] parameters = decoded.split("&");
				for(int i = 0; i < parameters.length; i++){
					String parameter = parameters[i];
					String[] valuePair = parameter.split("=");
					if(valuePair.length == 2){
						String key = valuePair[0].trim();
						String value = valuePair[1].trim();
						if(map.containsKey(key)){
							String [] exist = map.get(key);
							String [] combine = addArray(exist, value);
							map.put(key, combine);
						}else{
							String[] values = new String[]{value};
							map.put(key, values);
						}
					}
				}
				
			} catch (UnsupportedEncodingException e) {
				logger.error("Unable to Decode Query String:", e);
			}
			
		}

		return map;
	}
	
	protected String getRequestBody(HttpServletRequest request){
		if(request.getContentLength() > 0){
			try {
				return IOUtils.toString(request.getInputStream(), "UTF-8");
			} catch (IOException e) {
				logger.error("Error reading request body", e);
			}
		}
		
		return null;
	}
	
	private String[] addArray(String[] source, String entry) {
	    source  = Arrays.copyOf(source, source.length + 1);
	    source[source.length - 1] = entry;
	    return source;
	}
}
