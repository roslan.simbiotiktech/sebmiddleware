package com.seb.middleware.api;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class APIRequest {

	private String url;
	private String [] pathInfo;
	private HashMap<String, String[]> headers;
	private HashMap<String, String[]> getParameters;
	private String body;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String[] getPathInfo() {
		return pathInfo;
	}
	public void setPathInfo(String[] pathInfo) {
		this.pathInfo = pathInfo;
	}
	public HashMap<String, String[]> getHeaders() {
		return headers;
	}
	public void setHeaders(HashMap<String, String[]> headers) {
		this.headers = headers;
	}
	public HashMap<String, String[]> getGetParameters() {
		return getParameters;
	}
	public void setGetParameters(HashMap<String, String[]> getParameters) {
		this.getParameters = getParameters;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	
	private String printMap(HashMap<String, String[]> maps){
		if(maps == null) return "null";
		
		StringBuilder sb = new StringBuilder("{");
		
		Set<Entry<String, String[]>> entrySet = maps.entrySet();
		Iterator<Entry<String, String[]>> iterator = entrySet.iterator();
		while(iterator.hasNext()){
			Entry<String, String[]> next = iterator.next();
			sb.append(next.getKey());
			sb.append("=");
			sb.append(Arrays.toString(next.getValue()));
			if(iterator.hasNext()){
				sb.append(",");
			}
		}
		sb.append("}");
		
		return sb.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("APIRequest[");
		sb.append("url=").append(url);
		sb.append(",pathInfo=").append(Arrays.toString(pathInfo));
		sb.append(",headers=").append(printMap(headers));
		sb.append(",getParameters=").append(printMap(getParameters));
		//sb.append(",body=").append(body); // sensitive information
		sb.append("]");
		
		return sb.toString();
	}
}
