package com.seb.middleware.api;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import com.anteater.library.json.Json;
import com.anteater.library.json.exception.FieldException;
import com.google.gson.JsonSyntaxException;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.api.services.response.Error;
import com.seb.middleware.api.services.response.Response;
import com.seb.middleware.api.services.response.ResponseStatus;
import com.seb.middleware.utility.ResourceUtil;

public class APIResponse {

	public static final String ERROR_CODE_SERVER_EXCEPTION = "MWR2010";
	public static final String ERROR_CODE_INVALID_INPUT = "MWR2005";
	public static final String ERROR_CODE_EMAIL_EXISTED = "MWR2006";
	public static final String ERROR_CODE_WARNING = "MWW1000";
	
	public static final String LOGIN_ERROR_CODE_MISSING_CREDENTIAL = "EC2001";
	public static final String LOGIN_ERROR_CODE_INVALID_CREDENTIAL = "EC2002";
	
	private Response response;
	private Throwable throwable;

	private String locale = "en";
	
	private HashMap<String, String[]> headers;
	private String body;
	private int status = HttpServletResponse.SC_OK;
	
	public APIResponse(Response response){
		this.response = response;
		processResponse();
	}

	public APIResponse(Throwable throwable){
		this.throwable = throwable;
		processExceptionalResponse();
	}
	
	public APIResponse(int httpErrorCode){
		status = httpErrorCode;
		processHttpErrorResponse();
	}
	
	private void processResponse(){
		this.body = new Json().preserve(response);
	}
	
	private void processExceptionalResponse(){
		
		status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		
		if(throwable != null){
			if(throwable instanceof JsonSyntaxException){
				status = HttpServletResponse.SC_BAD_REQUEST;
			}else if(throwable instanceof FieldException){
				status = HttpServletResponse.SC_OK;
				this.response = getFieldErrorResponse((FieldException)throwable);
				processResponse();
			}else if(throwable instanceof ServiceException){
				status = HttpServletResponse.SC_OK;
				this.response = getServiceErrorResponse((ServiceException)throwable);
				processResponse();
			}
		}
		
		processHttpErrorResponse();
	}

	private void processHttpErrorResponse(){

		if(this.body == null || this.body.isEmpty()){
			switch(status){
			case HttpServletResponse.SC_NOT_FOUND:
				this.response = getGeneralErrorResponse("error.general_404");
				break;
			case HttpServletResponse.SC_INTERNAL_SERVER_ERROR:
				this.response = getGeneralErrorResponse("error.general_500");
				break;
			case HttpServletResponse.SC_BAD_REQUEST:
				this.response = getGeneralErrorResponse("error.general_400");
				break;
			case HttpServletResponse.SC_OK:
				break;
			default:
				this.response = getGeneralErrorResponse("error.general_500");
			}
			
			this.status = HttpServletResponse.SC_OK; // For Easy integration. All kind of error will be return status 200 with JSON error message.
			processResponse();
		}
	}
	
	private Response getFieldErrorResponse(FieldException ex){
		String userLocale = null;
		if(ex.getInstance() != null && ex.getInstance() instanceof Request){
			userLocale = ((Request)ex.getInstance()).getLocale();
		}

		ArrayList<String> messages =  ResourceUtil.get(ex, userLocale);
		
		Error error = new Error();
		error.setCode(ERROR_CODE_INVALID_INPUT);
		error.setMessages(messages.toArray(new String[messages.size()]));
		
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setStatus(-1);
		responseStatus.setError(error);
		
		Response response = new Response();
		response.setResponseStatus(responseStatus);

		return response;
	}
	
	private Response getServiceErrorResponse(ServiceException ex){

		Error error = new Error();
		error.setCode(ex.getErrorCode());
		error.setMessages(new String[] {ex.getLocalizedError()});
		
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setStatus(ex.getStatus());
		responseStatus.setError(error);
		
		Response response = new Response();
		response.setResponseStatus(responseStatus);

		return response;
	}
	
	private Response getGeneralErrorResponse(String messageKey){

		Error error = new Error();
		error.setCode(ERROR_CODE_SERVER_EXCEPTION);
		error.setMessages(new String[] {ResourceUtil.get(messageKey, getLocale())});
		
		ResponseStatus responseStatus = new ResponseStatus(false);
		responseStatus.setError(error);
		
		Response response = new Response();
		response.setResponseStatus(responseStatus);

		return response;
	}
		
	public HashMap<String, String[]> getHeaders() {
		return headers;
	}

	public String getBody() {
		return body;
	}

	public int getStatus() {
		return status;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	@Override
	public String toString() {
		return "APIResponse [throwable=" + throwable + ", headers=" + headers + ", status=" + status
				+ "]";
	}
}
