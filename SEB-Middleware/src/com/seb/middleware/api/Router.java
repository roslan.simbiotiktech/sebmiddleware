package com.seb.middleware.api;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.json.Json;
import com.anteater.library.json.exception.FieldException;
import com.google.gson.JsonSyntaxException;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.api.services.response.Response;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.CustomerAuditTrail;

public class Router {

	private static final Logger logger = LogManager.getLogger(Router.class);
	
	private APIRequest request;
	
	public Router(APIRequest request){
		this.request = request;
	}
	
	public APIResponse process(){
		
		ServiceMeta meta = getServiceMeta();

		if(meta == null){
			return new APIResponse(HttpServletResponse.SC_NOT_FOUND);
		}else{
			CustomerAuditTrail audit = null;
			Request serviceRequest = null;
			try {
				serviceRequest = new Json().convert(meta.getRequestClass(), request.getBody());
				
				logger.info(serviceRequest);
				
				Constructor<? extends ServiceBase<? extends Request, ? extends Response>> serviceConstructor = meta.getServiceClass().getConstructor(serviceRequest.getClass());
				
				ServiceBase<? extends Request, ? extends Response> serviceBase = serviceConstructor.newInstance(serviceRequest);
				serviceBase.setServiceMeta(meta);
				
				audit = serviceBase.getAuditTrail();
				Response serviceResponse = serviceBase.perform();
				if(audit != null){
					int success = 0;
					if(serviceResponse != null && serviceResponse.getResponseStatus().getStatus() == 0){
						success = 1;
					}
					audit.setSuccess(success);
				}
				logger.info(serviceResponse);
				return new APIResponse(serviceResponse);
			} catch (ServiceException e) {
				if(e.isPrint()) logger.error("Service Exception", e);
				return generateResponse(e, serviceRequest);
			} catch (JsonSyntaxException | FieldException e) {
				logger.warn(request.getBody());
				logger.error("Validation Exception", e);
				return generateResponse(e, serviceRequest);
			} catch (NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
				logger.error("Internal Server Exception", e);
				return generateResponse(e, serviceRequest);
			} catch (Throwable e) {
				logger.error("Internal Server Exception", e);
				return generateResponse(e, serviceRequest);
			} finally {
				if(audit != null){
					insertAudit(audit);
				}
			}
		}
	}
	
	private APIResponse generateResponse(Throwable t, Request serviceRequest){
		APIResponse response = new APIResponse(t);
		if(serviceRequest != null && serviceRequest.getLocale() != null) response.setLocale(serviceRequest.getLocale());
		
		logger.info("Service Error : " + response.getBody());
		return response;
	}
	
	private static final String UNDEFINED_SERVICE = "undefined";
	
	private ServiceMeta getServiceMeta(){
		String requestedService = null;
		String[] pathInfo = request.getPathInfo();
		
		if(pathInfo != null && pathInfo.length > 0){
			requestedService = pathInfo[pathInfo.length-1]; // Last Path is service name
		}
		if(requestedService == null || requestedService.isEmpty()) requestedService = UNDEFINED_SERVICE;
		logger.info("Requesting: " + requestedService);
		
		return RoutingService.getServiceMap().get(requestedService);
	}
	
	private void insertAudit(CustomerAuditTrail audit){
		DatabaseFacade db = new DatabaseFacade();
		try{
			db.beginTransaction();
			db.insert(audit);
			db.commit();
			
		} catch (DatabaseFacadeException e) {
			logger.error("error writing audit log: " + audit.toString());
		}finally{
			db.close();
		}
	}
}
