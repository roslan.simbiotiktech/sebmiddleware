package com.seb.middleware.api;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.reflect.ClassPath;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.api.services.response.Response;
import com.seb.middleware.core.BaseService;

public class RoutingService implements BaseService{

	private static final Logger logger = LogManager.getLogger(RoutingService.class);
	
	private static HashMap<String, ServiceMeta> servicesMap = null;
		
	private boolean isServiceSubClass(Class<?> clazz){
		if(clazz.getSuperclass() != null){
			if(clazz.getSuperclass() == ServiceBase.class){
				return true;
			}else{
				return isServiceSubClass(clazz.getSuperclass());
			}
		}else{
			return false;
		}
	}

	public static HashMap<String, ServiceMeta> getServiceMap(){
		return servicesMap;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void startService() {
		logger.info("Starting Routing Service.");
		
		servicesMap = new HashMap<String, ServiceMeta>();
		
		ClassPath classpath;
		try {
			classpath = ClassPath.from(Thread.currentThread().getContextClassLoader());
			for (ClassPath.ClassInfo classInfo : classpath.getTopLevelClassesRecursive("com.seb.middleware.api.services.impl")) {
				Class<?> clazz = Class.forName(classInfo.getName());
				if(clazz.isAnnotationPresent(Service.class)){
					Service annotation = clazz.getAnnotation(Service.class);
					if(isServiceSubClass(clazz)){
						ParameterizedType parameterizedType =  (ParameterizedType) clazz.getGenericSuperclass();
						Type[] genericTypes = parameterizedType.getActualTypeArguments();
						Class<? extends Request> T = (Class<? extends Request>) genericTypes[0];
						Class<? extends ServiceBase<? extends Request, ? extends Response>> serviceClass = (Class<? extends ServiceBase<? extends Request, ? extends Response>>) clazz;
						ServiceMeta meta = new ServiceMeta();
						meta.setServiceName(annotation.name());
						meta.setPublicService(annotation.publicService());
						meta.setAuditable(annotation.auditable());
						meta.setServiceClass(serviceClass);
						meta.setRequestClass(T);
						
						if(servicesMap.containsKey(meta.getServiceName())){
							logger.info("WARNING: Class " + clazz.getSimpleName() 
									+ " having the same service name with another clazz. (" 
									+ meta.getServiceName() + ").");
						}else{
							logger.info("ADDING: Class " + clazz.getSimpleName() + " | " + meta.getServiceName());
							servicesMap.put(meta.getServiceName(), meta);	
						}
					}else{
						logger.info("WARNING: Class " + clazz.getSimpleName() + " must be the subclass of ServiceBase");
					}
				}
			}
		} catch (IOException e) {
			logger.error("IOException", e);
		} catch (ClassNotFoundException e) {
			logger.error("ClassNotFoundException", e);
		}
		
		logger.info("Routing Service Started.");
	}

	@Override
	public void stopService() {
		logger.info("Stopping Routing Service.");
		logger.info("Routing Service Stopped.");
	}
}
