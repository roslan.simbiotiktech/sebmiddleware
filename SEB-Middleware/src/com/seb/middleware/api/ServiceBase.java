package com.seb.middleware.api;

import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.api.services.response.Response;
import com.seb.middleware.jpa.dao.CustomerAuditTrail;

public abstract class ServiceBase <T extends Request, K extends Response>{
	
	private ServiceMeta serviceMeta;
	protected int SUCCESS = 0;
	protected int FAILED = -1;
	protected T request;
	
	public ServiceBase(T request){
		this.request = request;
	}
	
	public void setServiceMeta(ServiceMeta meta){
		serviceMeta = meta;
	}
	
	/*
	 * Doesn't required validation, 
	 *  validation already been enforced in surface layer
	 */
	public abstract K perform() throws ServiceException;
	
	/*
	 * Service sub-class must override this method to provide audit info
	 */
	public String getAuditActivity(){
		return null;
	}
	
	/*
	 * Service sub-class must override this method to provide audit info
	 */
	public String getAuditUser(){
		return null;
	}
	
	public CustomerAuditTrail getAuditTrail(){
		if(serviceMeta.isAuditable()){
			String auditUser = getAuditUser();
			String auditActivity = getAuditActivity();
			
			if(auditUser != null && auditActivity != null){
				CustomerAuditTrail audit = new CustomerAuditTrail();
				audit.setUserId(auditUser);
				audit.setActivity(auditActivity);
				audit.setSuccess(0);
				return audit;	
			}
		}
		
		return null;
	}
}
