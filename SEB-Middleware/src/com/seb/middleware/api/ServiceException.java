package com.seb.middleware.api;


public class ServiceException extends Exception {

	private static final long serialVersionUID = -3781380900836503696L;

	private int status = -1;
	private String localizedError;
	private String errorCode;
	private boolean print = true;
	
	public ServiceException(String localizedError, String errorCode) {
		this(null, localizedError, errorCode);
	}
	
	public ServiceException(Throwable t, String localizedError, String errorCode) {
		super(t);
		this.localizedError = localizedError;
		this.errorCode = errorCode;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getLocalizedError() {
		return localizedError;
	}

	public void setLocalizedErrors(String localizedError) {
		this.localizedError = localizedError;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public boolean isPrint() {
		return print;
	}

	public void setPrint(boolean print) {
		this.print = print;
	}
	
}
