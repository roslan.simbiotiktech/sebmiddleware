package com.seb.middleware.api;

import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.api.services.response.Response;

public class ServiceMeta {

	private String serviceName;
	private Class<? extends ServiceBase<? extends Request, ? extends Response>> serviceClass;
	private Class<? extends Request> requestClass;
	private boolean publicService;
	private boolean auditable;
	
	public String getServiceName() {
		return serviceName;
	}
	
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
	public Class<? extends ServiceBase<? extends Request, ? extends Response>> getServiceClass() {
		return serviceClass;
	}
	
	public void setServiceClass(Class<? extends ServiceBase<? extends Request, ? extends Response>> serviceClass) {
		this.serviceClass = serviceClass;
	}
	
	public boolean isPublicService() {
		return publicService;
	}
	
	public void setPublicService(boolean publicService) {
		this.publicService = publicService;
	}
	
	public boolean isAuditable() {
		return auditable;
	}

	public void setAuditable(boolean auditable) {
		this.auditable = auditable;
	}

	public Class<? extends Request> getRequestClass() {
		return requestClass;
	}
	
	public void setRequestClass(Class<? extends Request> requestClass) {
		this.requestClass = requestClass;
	}

	@Override
	public String toString() {
		return "ServiceMeta [serviceName=" + serviceName + ", serviceClass=" + serviceClass + ", requestClass=" + requestClass + ", publicService="
				+ publicService + ", auditable=" + auditable + "]";
	}
}
