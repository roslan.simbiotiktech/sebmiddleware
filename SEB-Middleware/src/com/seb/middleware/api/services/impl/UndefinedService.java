package com.seb.middleware.api.services.impl;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.api.services.response.Response;

@Service(name = "undefined", publicService = true)
public class UndefinedService extends ServiceBase<Request, Response>{
	
	public UndefinedService(Request request) {
		super(request);
	}

	@Override
	public Response perform() throws ServiceException {
		return new Response();
	}

}
