package com.seb.middleware.api.services.impl.account;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;

import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.ActivateExistingUserRequest;
import com.seb.middleware.api.services.response.account.ActivateExistingUserResponse;
import com.seb.middleware.constant.PreferredCommunicationMethod;
import com.seb.middleware.constant.SubscriptionStatus;
import com.seb.middleware.constant.UserStatus;
import com.seb.middleware.constant.VerificationCodeStatus;
import com.seb.middleware.email.EmailManager;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.Subscription;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.jpa.helper.VerificationHelper;
import com.seb.middleware.ldap.LDAPConnector;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "activate_existing_user", publicService = true, auditable = true)
public class ActivateExistingUserService extends ServiceBase<ActivateExistingUserRequest, ActivateExistingUserResponse>{

	private static final Logger logger = LogManager.getLogger(ActivateExistingUserService.class);
	
	public ActivateExistingUserService(ActivateExistingUserRequest request) {
		super(request);
	}

	@Override
	public ActivateExistingUserResponse perform() throws ServiceException {
		
		ActivateExistingUserResponse response = new ActivateExistingUserResponse();
		
		String email = request.getEmail();
		
		DatabaseFacade db = new DatabaseFacade();
		LDAPConnector ldap = new LDAPConnector();
		boolean ldapMigrated = false;
		
		try{
			MemberHelper memberHelper = new MemberHelper(db);
			SettingHelper settingHelper = new SettingHelper(db);
			
			User existingUser = memberHelper.getUser(request.getUserId());
			if(existingUser != null){
				if(existingUser.getStatus() == UserStatus.PENDING){
					boolean existed = true;
					User user = memberHelper.getUser(email);
					VerificationHelper verificatonHelper = new VerificationHelper(db);
					
					if(user == null){
						boolean existLdap = ldap.isUserExist(email);
						if(!existLdap) {
							existed = false;	
						}else{
							logger.info("unable to activate existing user with email: " + email + ", already existed in LDAP.");
						}
					}else{
						logger.info("unable to activate existing user with email: " + email + ", already existed in Database.");
					}

					if(!existed){
						
						db.beginTransaction();
						
						VerificationCodeStatus emailVerified = verificatonHelper.consumeEmailVerification(email, request.getActivationCode());
						
						if(emailVerified == VerificationCodeStatus.VALID){
							String existingId = request.getUserId();
							
							Person existPerson = ldap.getUser(request.getUserId());					
							ldap.updateUserToNewEmail(existPerson, email);
							ldapMigrated = true;
			
							// Step 1: Update User
							String hqlUpdateUser = "update User set userId = :newEmail, status = :userStatus, preferredCommunicationMethod = :communicationMethod, sfdcUpdatedDate = :now where userId = :oldUserId";
							db.createQuery(hqlUpdateUser)
									.setParameter("newEmail", email)
									.setParameter("oldUserId", existingId)
									.setParameter("userStatus", UserStatus.ACTIVE)
									.setParameter("now", new Date())
									.setParameter("communicationMethod", PreferredCommunicationMethod.valueOf(request.getPreferredCommunicationMethod()))
									.executeUpdate();

							// Step 2: Update Remember_Me Flag
							String hqlUpdateRememberMe = "update RememberMe set userEmail = :newEmail where userEmail = :oldUserId";
							db.createQuery(hqlUpdateRememberMe)
									.setParameter("newEmail", email)
									.setParameter("oldUserId", existingId)
									.executeUpdate();
							
							// Step 3: Active Subscriptions
							SubscriptionHelper subscriptionHelper = new SubscriptionHelper(db);
							ArrayList<Subscription> inactiveSubscriptions = subscriptionHelper.getSubscriptions(existingId, SubscriptionStatus.INACTIVE);
							if(inactiveSubscriptions != null && !inactiveSubscriptions.isEmpty()){
								int maxSubscribeAllowed = settingHelper.getContractMaxSubscriptions();
								
								logger.info("user " + existingId + " have a total " + inactiveSubscriptions.size() + " to migrate.");
								for(int i = 0; i < inactiveSubscriptions.size(); i++){
									Subscription subscription = inactiveSubscriptions.get(i);
									logger.info("Migrating " + 1 + "/" + inactiveSubscriptions.size());
									logger.info("Contract Account No: " + subscription.getContractAccNo());
									logger.info("Contract Account Name: " + subscription.getContract().getContractAccountName());
									
									// Update to new email
									subscription.setUserEmail(email);
									subscription.setSfdcUpdatedDate(new Date());
									
									if(subscriptionHelper.isContractAccountCanSubscribe(maxSubscribeAllowed, subscription.getContractAccNo())){
										subscription.setStatus(SubscriptionStatus.ACTIVE);
										logger.info("Migration suceeded for subscription : " + subscription.getContractAccNo());
									}else{
										logger.info("Migration failed for subscription : Subscriptions max");
										logger.info(subscription);
									}
									db.update(subscription);
								}
							}else{
								logger.info("user " + existingId + " does no have any subscription to migrate.");
							}
							
							db.commit();
							logger.info("User activation completed, new ID:" + email + ", old ID: " + request.getUserId());
							response.getResponseStatus().setStatus(SUCCESS);
							
							//Send Email to user upon successful register account
							try{
								EmailManager manager = new EmailManager();
								manager.sendSuccessRegisterEmail(email, settingHelper.getFaqUrl());
							}catch(Exception ex){
								logger.error("Error while sending success activate email to [" + request.getEmail() + "].", ex);
							}
							
							return response;
						}else{
							if(emailVerified == VerificationCodeStatus.INVALID){
								logger.warn("Incorrect verification code for email : " + email + ", code : " + request.getActivationCode());
								throw new ServiceException(
										ResourceUtil.get("error.email_activation_incorrect", 
												request.getLocale()), 
												APIResponse.ERROR_CODE_INVALID_INPUT);
							}else{
								logger.warn("Expired verification code for email : " + email + ", code : " + request.getActivationCode());
								throw new ServiceException(
										ResourceUtil.get("error.email_activation_expired", 
												request.getLocale()), 
												APIResponse.ERROR_CODE_INVALID_INPUT);
							}							
						}
					}else{
						throw new ServiceException(
								ResourceUtil.get("error.user_email_taken", 
										request.getLocale()), 
										APIResponse.ERROR_CODE_EMAIL_EXISTED);
					}
				}else{
					logger.error("User Id: " + request.getUserId() + " doesnt required activation.");
					throw new ServiceException(
							ResourceUtil.get("error.general_500", 
									request.getLocale()), 
									APIResponse.ERROR_CODE_SERVER_EXCEPTION);
				}
			}else{
				logger.error("User Id: " + request.getUserId() + " doesnt exist in the system.");
				throw new ServiceException(
						ResourceUtil.get("error.general_500", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			}
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		} catch (DatabaseFacadeException e) {
			logger.error("Error activating existing user", e);
			if(ldapMigrated){
				try {
					Person existPerson = ldap.getUser(request.getEmail());
					ldap.updateUserToNewEmail(existPerson, request.getUserId());
					logger.error("revert changes from ldap successfully.");
				} catch (NamingException e1) {
					logger.error("Error reverting changes from ldap. " 
							+ request.getEmail() + " is suppose to be " + request.getUserId() 
							+ " if the revert was successful.", e);
				}
			}
			
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			logger.error("Verification code error", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getUserId();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("Activate existing e-Service account with Email: ");
		sb.append(request.getEmail());
		return sb.toString();
	}
}
