package com.seb.middleware.api.services.impl.account;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.naming.AuthenticationException;
import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.ChangePasswordRequest;
import com.seb.middleware.api.services.response.account.ChangePasswordResponse;
import com.seb.middleware.constant.OtpType;
import com.seb.middleware.constant.VerificationCodeStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.VerificationHelper;
import com.seb.middleware.ldap.LDAPConnector;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "change_password", publicService = true, auditable = true)
public class ChangePasswordService extends ServiceBase<ChangePasswordRequest, ChangePasswordResponse> {

	private static final Logger logger = LogManager.getLogger(ChangePasswordService.class);
	
	public ChangePasswordService(ChangePasswordRequest request) {
		super(request);
	}

	@Override
	public ChangePasswordResponse perform() throws ServiceException {
		ChangePasswordResponse response = new ChangePasswordResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		LDAPConnector ldap = new LDAPConnector();
		try {

			db.beginTransaction();
			MemberHelper memberHelper = new MemberHelper(db);
			User user = memberHelper.getUser(request.getEmail());
			
			if((request.getOtp() == null || request.getOtp().isEmpty()) && user.getPasswordExpired() == 0){
				logger.error("Unable to change password without OTP");
				throw new ServiceException(
							ResourceUtil.get("error.general_error_database", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			}else{
				if(request.getOtp() == null || request.getOtp().isEmpty()){
					performChange(memberHelper, ldap, db, response);
					return response;
				}else{
					VerificationHelper verification = new VerificationHelper(db);
					VerificationCodeStatus status = verification.consumeOTPVerification(request.getEmail(), request.getOtp(), OtpType.CHANGE_PASS);
					
					if(status == VerificationCodeStatus.VALID){
						performChange(memberHelper, ldap, db, response);
						return response;
					}else{
						if(status == VerificationCodeStatus.INVALID){
							throw new ServiceException(
									ResourceUtil.get("error.one_time_password_incorrect", 
											request.getLocale()), 
											APIResponse.ERROR_CODE_INVALID_INPUT);
						}else{
							throw new ServiceException(
									ResourceUtil.get("error.one_time_password_expired", 
											request.getLocale()), 
											APIResponse.ERROR_CODE_INVALID_INPUT);
						}
						
					}	
				}
			}	
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		} catch (DatabaseFacadeException e) {
			logger.error("Error changing password", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			logger.error("Verification code error", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} finally {
			db.close();
		}
	}
	
	private void performChange(MemberHelper memberHelper, LDAPConnector ldap, 
			DatabaseFacade db, ChangePasswordResponse response) throws AuthenticationException, NamingException, DatabaseFacadeException{
		User user = memberHelper.getUser(request.getEmail());
		if(user.getPasswordExpired() == 1){
			user.setPasswordExpired(0);
			db.update(user);
		}
		
		ldap.resetPassword(request.getEmail(),  request.getNewPassword());
		
		db.commit();
		
		logger.debug("Password changed successfully for : " + request.getEmail());
		response.getResponseStatus().setStatus(SUCCESS);
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		return "Change password";
	}
}
