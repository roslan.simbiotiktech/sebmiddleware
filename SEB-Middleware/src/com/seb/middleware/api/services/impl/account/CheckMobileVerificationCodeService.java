package com.seb.middleware.api.services.impl.account;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.CheckMobileVerificationCodeRequest;
import com.seb.middleware.api.services.response.account.CheckMobileVerificationCodeResponse;
import com.seb.middleware.constant.VerificationCodeStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.helper.VerificationHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "check_mobile_verification_code", publicService = true)
public class CheckMobileVerificationCodeService extends ServiceBase<CheckMobileVerificationCodeRequest, CheckMobileVerificationCodeResponse>{

	private static final Logger logger = LogManager.getLogger(CheckMobileVerificationCodeService.class);
	
	public CheckMobileVerificationCodeService(CheckMobileVerificationCodeRequest request) {
		super(request);
	}

	@Override
	public CheckMobileVerificationCodeResponse perform() throws ServiceException {
		CheckMobileVerificationCodeResponse response = new CheckMobileVerificationCodeResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			db.beginTransaction();
			VerificationHelper helper = new VerificationHelper(db);
			VerificationCodeStatus status = helper.consumeMobileVerification(request.getMobileNumber(), request.getVerificationCode());
			
			// Do not commit because this is for testing validity only
			if(status == VerificationCodeStatus.VALID){
				db.rollback();
				
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			}else{
				if(status == VerificationCodeStatus.INVALID){
					throw new ServiceException(
							ResourceUtil.get("error.phone_verification_incorrect", 
									request.getLocale()), 
									APIResponse.ERROR_CODE_INVALID_INPUT);
				}else{
					throw new ServiceException(
							ResourceUtil.get("error.phone_verification_expired", 
									request.getLocale()), 
									APIResponse.ERROR_CODE_INVALID_INPUT);
				}
			}
		} catch (DatabaseFacadeException e) {
			logger.error("Error Checking Mobile Verification Code", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			logger.error("Verification code error", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} finally{
			db.close();
		}
	}
}
