package com.seb.middleware.api.services.impl.account;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.DeleteNotificationRequest;
import com.seb.middleware.api.services.response.account.DeleteNotificationResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.helper.NotificationHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "delete_notification", publicService = true, auditable = true)
public class DeleteNotificationService extends ServiceBase<DeleteNotificationRequest, DeleteNotificationResponse>  {

	private static final Logger logger = LogManager.getLogger(DeleteNotificationService.class);

	public DeleteNotificationService(DeleteNotificationRequest request) {
		super(request);
	}

	@Override
	public DeleteNotificationResponse perform() throws ServiceException {
		DeleteNotificationResponse response = new DeleteNotificationResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			NotificationHelper notificationHelper = new NotificationHelper(db);
			db.beginTransaction();
			notificationHelper.deleteNotificationHistory(request.getEmail(), request.getNotificationIds());
			db.commit();
			
			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		} catch (DatabaseFacadeException e) {
			logger.error("Error deleting notification", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("Delete push notification (ids): ");
		if(request.getNotificationIds() == null || request.getNotificationIds().length == 0){
			sb.append(" Clear-All-Notifications");
		}else{
			sb.append(Arrays.toString(request.getNotificationIds()));	
		}
		
		return sb.toString();
	}
}
