package com.seb.middleware.api.services.impl.account;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.Photo;
import com.seb.middleware.api.services.request.account.EditProfileRequest;
import com.seb.middleware.api.services.response.account.EditProfileResponse;
import com.seb.middleware.constant.PreferredCommunicationMethod;
import com.seb.middleware.constant.VerificationCodeStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.CrmUserProfile;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.VerificationHelper;
import com.seb.middleware.ldap.LDAPConnector;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.salesforce.api.CreateAccountRequestInfo;
import com.seb.middleware.salesforce.api.QueryAccountRequestInfo;
import com.seb.middleware.salesforce.api.UpdateAccountRequestInfo;
import com.seb.middleware.salesforce.auth.AuthenticationException;
import com.seb.middleware.salesforce.auth.Token;
import com.seb.middleware.salesforce.core.CompositeResponse;
import com.seb.middleware.salesforce.core.HTTPException;
import com.seb.middleware.salesforce.core.QueryAccountRequestResponseInfo;
import com.seb.middleware.salesforce.core.SFUtility;
import com.seb.middleware.salesforce.core.SalesForceCompositeConnector;
import com.seb.middleware.utility.CryptoUtil;
import com.seb.middleware.utility.ResourceUtil;
import com.seb.middleware.utility.StringUtil;

@Service(name = "edit_profile", publicService = true, auditable = true)
public class EditProfileService extends ServiceBase<EditProfileRequest, EditProfileResponse> {

	private static final Logger logger = LogManager.getLogger(EditProfileService.class);
	
	public EditProfileService(EditProfileRequest request) {
		super(request);
	}

	@Override
	public EditProfileResponse perform() throws ServiceException {
		EditProfileResponse response = new EditProfileResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		LDAPConnector ldap = new LDAPConnector();
		Person clone = null;
		try{
			
			SettingHelper settingHelper = new SettingHelper(db);
			MemberHelper memberHelper = new MemberHelper(db);
			Person person = memberHelper.getPerson(request.getEmail());
			
			clone = person.clone(); // For backup
			
			User user = memberHelper.getUser(request.getEmail());
		
			Photo profilePhoto = request.getProfilePhoto();
			
			if(profilePhoto != null && !StringUtil.isEmpty(profilePhoto.getData()) && !StringUtil.isEmpty(profilePhoto.getDataType())){
				user.setProfilePhoto(CryptoUtil.decodeBase64(profilePhoto.getData()));
				user.setProfilePhotoType(profilePhoto.getDataType());
			}
			
			if(!StringUtil.isEmpty(request.getName())){
				person.setName(request.getName().trim());
			}
			
			if(!StringUtil.isEmpty(request.getPreferredCommunicationMethod())){
				user.setPreferredCommunicationMethod(
						PreferredCommunicationMethod.valueOf(request.getPreferredCommunicationMethod()));
			}
			
			if(!StringUtil.isEmpty(request.getNricOrPassport())){
				person.setNricPassport(request.getNricOrPassport().trim());
			}
			
			// Optional Fields, allowed update to nul
			if(request.getOfficeTel() != null){
				if(!StringUtil.isEmpty(request.getOfficeTel())){
					person.setOfficePhone(request.getOfficeTel().trim());
				}else{
					person.setOfficePhone(null);
				}
			}
			
			// Optional Fields, allowed update to null
			if(request.getHomeTel() != null){
				if(!StringUtil.isEmpty(request.getHomeTel())){
					person.setHomePhone(request.getHomeTel().trim());
				}else{
					person.setHomePhone(null);
				}
			}
			
			if(!StringUtil.isEmpty(request.getLocale())){
				user.setPreferredLocale(request.getPreferredLocale());
			}
			
			if(request.getMobileNumber() != null){
				VerificationHelper helper = new VerificationHelper(db);
				logger.info("User: " + request.getEmail() + " request to update mobile number to: " + request.getMobileNumber().getNumber());
				VerificationCodeStatus valid = helper.consumeMobileVerification(
						request.getMobileNumber().getNumber().trim(),
						request.getMobileNumber().getVerificationCode());
				
				if(valid != VerificationCodeStatus.VALID){
					if(valid == VerificationCodeStatus.INVALID){
						throw new ServiceException(
								ResourceUtil.get("error.phone_verification_incorrect", 
										request.getLocale()), 
										APIResponse.ERROR_CODE_INVALID_INPUT);	
					}else{
						throw new ServiceException(
								ResourceUtil.get("error.phone_verification_expired", 
										request.getLocale()), 
										APIResponse.ERROR_CODE_INVALID_INPUT);
					}
				}
				
				person.setMobilePhone(request.getMobileNumber().getNumber());
			}
			
			
			//added by pramod for Nric manipulation
			if((person.getNricPassport()!=null)&&(person.getNricPassport().length()!=0)&&(!person.getNricPassport().isEmpty()) ) {
				String regex = "[0-9]+";
				boolean verifyNumeric =true;
				verifyNumeric=person.getNricPassport().matches(regex);
				if((person.getNricPassport().length()==12)&&(verifyNumeric==true)) {
					logger.info("NricPassport >> " + person.getNricPassport());
					String NricPassport=	this.set14CharNricNumber(person.getNricPassport());
					LDAPConnector ldapUpdation = new LDAPConnector();
					person.setNricPassport(NricPassport);
					ldapUpdation.updateUser(person);
					logger.info("NricPassport LDAP updated ");
					person= memberHelper.getPerson(request.getEmail());
					logger.info("get NricPassport LDAP updated value " +person.getNricPassport());
				}else {
					logger.info("NricPassport length not matched with 12 digit and and numric >> "+person.getNricPassport());
				}
			}
			
			
			//added by pramod for MobilePhone manipulation
			if((person.getMobilePhone()!=null)&&(person.getMobilePhone().length()!=0)&&(!person.getMobilePhone().isEmpty()) ) {				
				logger.info("phoneNumber >> " +person.getMobilePhone());
				String phoneNumber=person.getMobilePhone();
				String intitialDigit=StringUtils.substring(phoneNumber, 0, 1);
				logger.info("intitialDigit phoneNumber  >> "+intitialDigit);
				String phoneNumberFinal="";
				// number  starts 0 and  9,10 digit format 
				 if( (intitialDigit.equals("0")) &&((phoneNumber.length()==10) || (phoneNumber.length()==11))) {
					 phoneNumberFinal =this.zeroInitialPhoneNumber(phoneNumber);					
					 logger.info("phoneNumberFinal >>" +phoneNumberFinal);
					// number  starts 1 and  10,11 digit format
				 }else if((intitialDigit.equals("1")) &&((phoneNumber.length()==9) || (phoneNumber.length()==10))) {
					 phoneNumberFinal =this.oneInitialPhoneNumber(phoneNumber);					
					 logger.info("phoneNumberFinal >>" +phoneNumberFinal);
				 }	
				 //LDAP Updation
				 if((phoneNumberFinal!=null)&&(phoneNumberFinal.length()!=0)&&(!phoneNumberFinal.isEmpty())) {				 
					  LDAPConnector ldapPhoneNumber = new LDAPConnector(); 
					  person.setMobilePhone(phoneNumberFinal);
					  ldapPhoneNumber.updateUser(person); 
					  logger.info("MobilePhone LDAP updated "); 
					  person=memberHelper.getPerson(request.getEmail());
					  logger.info("get MobilePhone LDAP updated value " +person.getMobilePhone());				 
				 }else {
					 logger.info("Number validation failed  >> " +phoneNumber);
				 }
			}
			
			
			ldap.updateUser(person);
			user.setSfdcUpdatedDate(new Date());
			
			try {
				Token token = SFUtility.getToken(settingHelper);
				SalesForceCompositeConnector connector = new SalesForceCompositeConnector(token);
				
				if(!StringUtil.isEmpty(user.getSfdcId())) {
					// profile available in CRM system and not available in SEB Care  and Ldap
					logger.info("Profile  available Sales Force system with E-mail, NRIC/Passport and Mobile Number  But not available in SEB Cares system: " + request.getEmail());
					
					// need to update query  CRM  new user available at CRM to Email-1 and mobile-1
					CompositeResponse qaresp=	this.queryAccountRequestInfo(user, person);
					CrmUserProfile crmUserProfiles = memberHelper.getCrmUserProfileWithSfdcID(qaresp.getFirstObjectId());
				
						// CRM profile already having the data
						
						CompositeResponse compositeResponseUpdateAccountResponse=null;
						QueryAccountRequestResponseInfo queryAccountRequestResponseInfo=null;
						QueryAccountRequestResponseInfo	 queryAccountResponseInfo	=qaresp.getQueryAccountResponseInfo();
						//for Update Sales Force need to justify which set of pair need to update.																											
						// set new profile with numbering 
						Boolean crmProdileAvailableInd= this.getCrmAllThreeProfileExists(crmUserProfiles.getSfdcId());
						QueryAccountRequestResponseInfo queryAccountResponsePairs=qaresp.getQueryAccountResponseInfo();
						if(crmProdileAvailableInd) {
							logger.info("Already exists CRM profiles with less than three in SEB Cares and Sales Force");
							queryAccountRequestResponseInfo=this.getNotExistsAllThreeCrmProfilesRequestBody(person,queryAccountResponsePairs);
						}else {
							logger.info("Already exists CRM profiles equal or more than three in SEB Cares and Sales Force");
							queryAccountRequestResponseInfo=this.setPairToUpdateNewProfileInCrm(person,queryAccountResponseInfo,qaresp.getFirstObjectId());	
						}
						
						compositeResponseUpdateAccountResponse=	this.updateAccountRequestInfoWithExistingProfiles(person,queryAccountRequestResponseInfo,qaresp.getFirstObjectId());		
						if(compositeResponseUpdateAccountResponse.getHttpStatusCode()==204) {	
							logger.info("Update CRM  success with new profile : " + user.getUserId()+" : sfdcID : "+compositeResponseUpdateAccountResponse.getFirstObjectId());		
							logger.info("Updation of existing sfdcId with new Email : "+crmUserProfiles.getSfdcId()+" <<<<>>>>  "+user.getUserId());
							user.setSfdcId(qaresp.getFirstObjectId());
							user.setSfdcUpdatedDate(new Date());
							Boolean sfdcIdUpdation =this.userUpdation(user,qaresp.getFirstObjectId())	;
							if(sfdcIdUpdation) {
								logger.info("sfdcId Update done with new E-Mail of existing profile  : " + request.getEmail());
							}else {
								logger.info("sfdcId Update failed with new E-Mail of existing profile : " + request.getEmail());
							}								
							// update new CRM user profile table 																																			
							Boolean userCrmUpdation=this.crmUserUpdateWithNewSfdcid(crmUserProfiles.getSfdcId(),qaresp.getFirstObjectId(),queryAccountRequestResponseInfo);									
							if(userCrmUpdation) {
								logger.info("sfdcId  done Crm user updation : " + request.getEmail());
							}else {
								logger.info("sfdcId  failed Crm user updation: " + request.getEmail());
							}
						}
					
				}else {
							
						}
			}catch(Throwable t) {
				logger.error("error updating account to salesforce, however, it will be created later via cron - " + t.getMessage(), t);
			}
			
			db.beginTransaction();
			db.update(user);
			db.commit();
			
			response.getResponseStatus().setStatus(SUCCESS);
			
			return response;
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		} catch (DatabaseFacadeException e) {
			
			if(clone != null){
				
				LDAPConnector connector = new LDAPConnector();
				try {
					logger.debug("Reverting changes on LDAP for user: " + clone.getEmail());
					connector.updateUser(clone);
				} catch (NamingException e1) {
					logger.error("Error reverting changes on LDAP for user: " + clone.getEmail(), e);
				}
			}
			
			logger.error("Error update user", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			logger.error("Verification code error", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} finally{
			db.close();
		}
	}
	

		//added by pramod	
		public String set14CharNricNumber(String Nric) {
			String finalNric="";
			logger.info("NricPassport length >> " + Nric.length());
			if(Nric.length()==12) {
				String sixCharacters=StringUtils.substring(Nric, 0, 6);
				String middleTwoCharacters=StringUtils.substring(Nric, 6, 8);
				String lastFourCharacters=StringUtils.substring(Nric, 8, 12);
				finalNric=sixCharacters+"-"+middleTwoCharacters+"-"+lastFourCharacters;
				logger.info("Final NRIC Number >> "+finalNric);
				logger.info("Final NRIC Number length >> "+finalNric.length());
			}
			return finalNric;
		}
		public  String zeroInitialPhoneNumber(String phoneNumber) {
			String finaNumber=phoneNumber.replaceFirst("0", "60");
			
			return finaNumber;
			
		}
		public  String oneInitialPhoneNumber(String phoneNumber) {
			String finaNumber="60"+phoneNumber;
			return finaNumber;
			
		}

		// Crm user sfdcID Updation
		public Boolean crmUserUpdateWithNewSfdcid(String oldestSfdcID,String newSfdcid,QueryAccountRequestResponseInfo queryAccountResponseInfo) throws DatabaseFacadeException {
			logger.info("call >> crmUserUpdateWithNewSfdcid(String oldestSfdcID,String newSfdcid,QueryAccountRequestResponseInfo queryAccountResponseInfo) ");
			Boolean  userCrmUpdation=false;
			DatabaseFacade dbGetCrmUser = new DatabaseFacade();
			MemberHelper userHelper = new MemberHelper(dbGetCrmUser);
			dbGetCrmUser.beginTransaction();
			logger.info("call >> 	getCrmUserProfileWithSfdcID(String sfdcId) ");	
			
			logger.info("Get CRM table sfdcID >>> "+ newSfdcid);
			
			CrmUserProfile crmUserProfiles = userHelper.getCrmUserProfileWithSfdcID(newSfdcid);
			if(crmUserProfiles != null){	
				try {					

					if(queryAccountResponseInfo.getSebcEmailOne()!=null && queryAccountResponseInfo.getSebcMobileOne()!=null) {
						crmUserProfiles.setEmail_1(queryAccountResponseInfo.getSebcEmailOne());
						crmUserProfiles.setMobile_1(queryAccountResponseInfo.getSebcMobileOne());
						crmUserProfiles.setSfdcId(newSfdcid);
						crmUserProfiles.setSfdcUpdatedDate1(new Date());
					}else if(queryAccountResponseInfo.getSebcEmailTwo()!=null && queryAccountResponseInfo.getSebcMobileTwo()!=null) {
						crmUserProfiles.setEmail_2(queryAccountResponseInfo.getSebcEmailTwo());
						crmUserProfiles.setMobile_2(queryAccountResponseInfo.getSebcMobileTwo());
						crmUserProfiles.setSfdcId(newSfdcid);
						crmUserProfiles.setSfdcUpdatedDate2(new Date());
					}else if(queryAccountResponseInfo.getSebcEmailThree()!=null && queryAccountResponseInfo.getSebcMobileThree()!=null) {
						crmUserProfiles.setEmail_3(queryAccountResponseInfo.getSebcEmailThree());
						crmUserProfiles.setMobile_3(queryAccountResponseInfo.getSebcMobileThree());
						crmUserProfiles.setSfdcId(newSfdcid);
						crmUserProfiles.setSfdcUpdatedDate3(new Date());
					}
					
														
				
					dbGetCrmUser.update(crmUserProfiles);
					dbGetCrmUser.commit();
				} catch (DatabaseFacadeException e1) {
					logger.error("Error while reverting DB CrmUserProfiles Updation.", e1);
				} finally {
					dbGetCrmUser.close();
					userCrmUpdation=true;
				}
			}
			return userCrmUpdation;
		}
		
		public QueryAccountRequestResponseInfo setPairToUpdateNewProfileInCrm(Person person,QueryAccountRequestResponseInfo queryAccountResponseInfo,String oldSfdcId){
			logger.info("call >> setPairToUpdateNewProfileInCrm(Person person,QueryAccountRequestResponseInfo queryAccountResponseInfo,String oldSfdcId)");
			
			QueryAccountRequestResponseInfo queryAccountRequestInfo=new QueryAccountRequestResponseInfo();
			
			DatabaseFacade dbGetCrmUser = new DatabaseFacade();
			MemberHelper userHelper = new MemberHelper(dbGetCrmUser);
			
			try {
				dbGetCrmUser.beginTransaction();
				logger.info("call >> 	getCrmUserProfileWithSfdcID(String sfdcId) ");	
				CrmUserProfile crmUserProfiles = userHelper.getCrmUserProfileWithSfdcID(oldSfdcId);
				LinkedHashMap<String, String> emailPhone=new LinkedHashMap<String, String>();
				
				if(queryAccountResponseInfo.getSebcEmailOne()!=null && queryAccountResponseInfo.getSebcMobileOne()!=null) {
					emailPhone.put(queryAccountResponseInfo.getSebcEmailOne(),queryAccountResponseInfo.getSebcMobileOne() );	
				}
				if(queryAccountResponseInfo.getSebcEmailTwo()!=null && queryAccountResponseInfo.getSebcMobileTwo()!=null) {
					emailPhone.put(queryAccountResponseInfo.getSebcEmailTwo(), queryAccountResponseInfo.getSebcMobileTwo());
				}
				if(queryAccountResponseInfo.getSebcEmailThree()!=null && queryAccountResponseInfo.getSebcMobileThree()!=null) {
					emailPhone.put(queryAccountResponseInfo.getSebcEmailThree(), queryAccountResponseInfo.getSebcMobileThree());	
				}
							
										
				
				Boolean firstSet=false;
				Boolean SecondSet=false;
				Boolean ThirdSet=false;
				
				int i=1;
				for (Map.Entry<String, String> entry : emailPhone.entrySet()) {
					if(entry.getKey()!=null && entry.getValue()!=null) {								
					String k = entry.getKey();
					String v = entry.getValue();
					
					// pair 1 mapping
					if(i==1) {
						if(crmUserProfiles.getEmail_1()!=null && crmUserProfiles.getMobile_1()!=null) {
							
							if((crmUserProfiles.getEmail_1().equals(entry.getKey()) &&crmUserProfiles.getMobile_1().equals(entry.getValue()))) {
								logger.info("Matched Email-1 : " + k + ", PhoneNumber: " + v);
								 firstSet=true;
							}
						}
						i++;
					}
					
					// pair 3 mapping
					if(i==2) {			
					if(crmUserProfiles.getEmail_2()!=null && crmUserProfiles.getMobile_2()!=null) {
						if((crmUserProfiles.getEmail_2().equals(entry.getKey()) &&crmUserProfiles.getMobile_2().equals(entry.getValue()))) {
							logger.info("Matched Email-2 : " + k + ", PhoneNumber: " + v);
							SecondSet=true;						
						}
					}
					i++;
					}
					// pair 3 mapping
					if(i==3) {
						if(crmUserProfiles.getEmail_3()!=null && crmUserProfiles.getMobile_3()!=null) {
							if((crmUserProfiles.getEmail_3().equals(entry.getKey()) &&crmUserProfiles.getMobile_3().equals(entry.getValue()))) {
								logger.info("Matched Email-3 : " + k + ", PhoneNumber: " + v);
								ThirdSet=true;
							}
						}	
					}
							
				}
					if(!firstSet || !SecondSet || !ThirdSet) {
						logger.info(" need to return UI message");
						queryAccountRequestInfo.setReturnUI(false);
					}
				
				// setting the pairs for create the new profile in CRM with existing user
					if(queryAccountRequestInfo.getReturnUI()) {
						int j=1;
						
						Boolean personPairMatchedInd=false;
						
						for (Map.Entry<String, String> entryInfo : emailPhone.entrySet()) {
							if(entryInfo.getKey()!=null && entryInfo.getValue()!=null) {								
							String k = entry.getKey();
							String v = entry.getValue();
							
							// pair 1 setting for updation
							if(j==1) {
							if(person.getEmail().equals( entryInfo.getKey()) && person.getMobilePhone().equals(entryInfo.getValue()) ){
								queryAccountRequestInfo.setSebcEmailOne(person.getEmail());
								queryAccountRequestInfo.setSebcMobileOne(person.getMobilePhone());
								personPairMatchedInd=true;
								}
								j++;
							}
							
							// pair 2 setting for updation
							if(j==2) {
								if(person.getEmail().equals( entryInfo.getKey()) && person.getMobilePhone().equals(entryInfo.getValue()) ){
									queryAccountRequestInfo.setSebcEmailTwo(person.getEmail());
									queryAccountRequestInfo.setSebcMobileTwo(person.getMobilePhone());
									personPairMatchedInd=true;
									}
									j++;
							}
							// pair 3 setting for updation
							if(j==3) {
								if(person.getEmail().equals( entryInfo.getKey()) && person.getMobilePhone().equals(entryInfo.getValue()) ){
									queryAccountRequestInfo.setSebcEmailThree(person.getEmail());
									queryAccountRequestInfo.setSebcMobileThree(person.getMobilePhone());
									personPairMatchedInd=true;
									}
									j++;
							}
							
							
						}
					}
						
						if(!personPairMatchedInd) {
							// pair 2 setting for updation
							if(emailPhone.size()==1) {
								queryAccountRequestInfo.setSebcEmailTwo(person.getEmail());
								queryAccountRequestInfo.setSebcMobileTwo(person.getMobilePhone());
							}
							// pair 3 setting for updation
							if(emailPhone.size()==2) {
								queryAccountRequestInfo.setSebcEmailThree(person.getEmail());
								queryAccountRequestInfo.setSebcMobileThree(person.getMobilePhone());
							}
							// pair 1 setting for updation
							if(emailPhone.size()==3) {
								queryAccountRequestInfo.setSebcEmailOne(person.getEmail());
								queryAccountRequestInfo.setSebcMobileOne(person.getMobilePhone());
							}
							
						}
						
				}
				}
				
			} catch (DatabaseFacadeException e1) {
				logger.error("Error while reverting DB user creation.", e1);
			} finally {
				dbGetCrmUser.close();
				
			}
			
			return queryAccountRequestInfo;
		}
		// sales force  call for  QueryAccountRequest
		public CompositeResponse queryAccountRequestInfo(User user,Person person ) {
			CompositeResponse  compositeResponse=null;
			DatabaseFacade db = new DatabaseFacade();
			SettingHelper settingHelper = new SettingHelper(db);
			try {
				db.beginTransaction();
				Token token = SFUtility.getToken(settingHelper);
				SalesForceCompositeConnector connector = new SalesForceCompositeConnector(token);
				QueryAccountRequestInfo queryAccountRequest=new QueryAccountRequestInfo(user.getUserId(), person.getMobilePhone(), person.getNricPassport(), user.getUserId(), user.getUserId(), user.getUserId(), person.getMobilePhone(), person.getMobilePhone(), person.getMobilePhone());
				
				compositeResponse = connector.invoke(queryAccountRequest);
				db.commit();
				db.close();
			
			} catch (AuthenticationException|HTTPException |DatabaseFacadeException| IOException  e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return compositeResponse;
		}
		
			// updating the existig user  with CRM profiles
			public CompositeResponse updateAccountRequestInfoWithExistingProfiles(Person person,QueryAccountRequestResponseInfo updateAccountRequestResponseInfo,String newSfdcId) {
				logger.info("call >> updateAccountRequestInfoWithExistingProfiles(Person person,QueryAccountRequestResponseInfo updateAccountRequestResponseInfo,String newSfdcId)");
				
				CompositeResponse compositeResponse=null;
				DatabaseFacade db = new DatabaseFacade();
				SettingHelper settingHelper = new SettingHelper(db);
				try {
					db.beginTransaction();
					Token token = SFUtility.getToken(settingHelper);			
					SalesForceCompositeConnector connector = new SalesForceCompositeConnector(token);
					UpdateAccountRequestInfo queryAccountRequest=new UpdateAccountRequestInfo(newSfdcId, person.getName(), updateAccountRequestResponseInfo.getSebcEmailOne(), 
							
							updateAccountRequestResponseInfo.getSebcEmailTwo(), updateAccountRequestResponseInfo.getSebcEmailThree(), updateAccountRequestResponseInfo.getSebcMobileOne(), updateAccountRequestResponseInfo.getSebcMobileTwo(), updateAccountRequestResponseInfo.getSebcMobileThree());
					compositeResponse = connector.invoke(queryAccountRequest);	
					db.commit();
					db.close();
				} catch (AuthenticationException|HTTPException |DatabaseFacadeException| IOException  e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.error("error update the existing  account to salesforce which having the sfdcId, however - " + e.getMessage(), e);
				}
				return compositeResponse;
			}
			

			public Boolean getCrmAllThreeProfileExists(String sfdcId) {
				
				logger.info("cal >>> getCrmAllThreeProfileExists(String sfdcId) ");
				Boolean crmProdileInd=true;
				DatabaseFacade dbGetCrmUser = new DatabaseFacade();
				MemberHelper userHelper = new MemberHelper(dbGetCrmUser);
				
				Boolean firstSet=false;
				Boolean SecondSet=false;
				Boolean ThirdSet=false;
				try {
					dbGetCrmUser.beginTransaction();
					CrmUserProfile crmUserProfiles = userHelper.getCrmUserProfileWithSfdcID(sfdcId);
					if(crmUserProfiles!=null) {
						if(crmUserProfiles.getEmail_1()!=null && crmUserProfiles.getMobile_1()!=null) {
							firstSet=true;
						}
						
						if(crmUserProfiles.getEmail_2()!=null && crmUserProfiles.getMobile_2()!=null) {
							SecondSet=true;
						}
						
						if(crmUserProfiles.getEmail_3()!=null && crmUserProfiles.getMobile_3()!=null) {
							ThirdSet=true;
						}
					}
					
					if(firstSet &&  SecondSet &&ThirdSet) {
						crmProdileInd=false;
					}
					
				} catch (DatabaseFacadeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				logger.info("crmProdileInd >> "+crmProdileInd);
				
				return crmProdileInd;
				
			}

			
			// added by pramod for setting the getNotExistsAllThreeCrmProfilesRequestBody request parameters
			public QueryAccountRequestResponseInfo getNotExistsAllThreeCrmProfilesRequestBody(Person person,QueryAccountRequestResponseInfo queryAccountResponsePairs){
				logger.info("call >> getNotExistsAllThreeCrmProfilesRequestBody pair setting");
				QueryAccountRequestResponseInfo queryAccountRequestResponseInfo=new QueryAccountRequestResponseInfo();
				Boolean firstSet=false;
				Boolean SecondSet=false;
				Boolean ThirdSet=false;
						
						if((queryAccountResponsePairs.getSebcEmailOne()==null && queryAccountResponsePairs.getSebcMobileOne()==null)||(queryAccountResponsePairs.getSebcEmailOne().equals(person.getEmail()) &&queryAccountResponsePairs.getSebcMobileOne().equals(person.getMobilePhone()))) {
							firstSet=true;						
						}else if((queryAccountResponsePairs.getSebcEmailTwo()==null && queryAccountResponsePairs.getSebcMobileTwo()==null)||(queryAccountResponsePairs.getSebcEmailTwo().equals(person.getEmail()) &&queryAccountResponsePairs.getSebcMobileTwo().equals(person.getMobilePhone()))) {
							SecondSet=true;					
						}else if((queryAccountResponsePairs.getSebcEmailThree()==null && queryAccountResponsePairs.getSebcMobileThree()==null)||(queryAccountResponsePairs.getSebcEmailThree().equals(person.getEmail()) &&queryAccountResponsePairs.getSebcMobileThree().equals(person.getMobilePhone()))) {
							ThirdSet=true;					
						}
						if(firstSet && !SecondSet &&! ThirdSet) {
							logger.info(" First pair is matched with login profile ");
							queryAccountRequestResponseInfo.setSebcEmailOne(person.getEmail());
							queryAccountRequestResponseInfo.setSebcMobileOne(person.getMobilePhone());
						}else if(!firstSet && SecondSet && !ThirdSet) {
							logger.info("Setting pair SET-2 for updare request due to Second  pair  matched with login profile or it's having empty with CRM profile ");
							queryAccountRequestResponseInfo.setSebcEmailTwo(person.getEmail());
							queryAccountRequestResponseInfo.setSebcMobileTwo(person.getMobilePhone());
							
						}else if(!firstSet && !SecondSet && ThirdSet) {
							logger.info(" Setting pair SET-3 for updare request due to Third  pair  matched with login profile or it's having empty with CRM profile");
							queryAccountRequestResponseInfo.setSebcEmailThree(person.getEmail());
							queryAccountRequestResponseInfo.setSebcMobileThree(person.getMobilePhone());	
						}
						queryAccountRequestResponseInfo.setReturnUI(true);
				return queryAccountRequestResponseInfo;
			}
			
			
			// user sfdcID update
			public Boolean userUpdation(User userEmail,String sfdcID) {
				Boolean  userInsertion=false;
				DatabaseFacade dbUser = new DatabaseFacade();
				MemberHelper userHelper = new MemberHelper(dbUser);
				
				if(userEmail != null){
				
					
					try {
						dbUser.beginTransaction();
						logger.info("user is update started with sdfcid "+userEmail.getUserId() +","+sfdcID);
						User existingUserInfo=userHelper.getUser(userEmail.getUserId());
						logger.info("existingUserInfo >> "+existingUserInfo.getUserId());
						existingUserInfo.setSfdcId(sfdcID);
						Date now =new Date();
						existingUserInfo.setSfdcUpdatedDate(now);
						existingUserInfo.setSfdcSyncDate(now);			
						dbUser.update(existingUserInfo);
						dbUser.commit();
					} catch (DatabaseFacadeException e1) {
						logger.error("Error while reverting DB user creation.", e1);
					} finally {
						dbUser.close();
						userInsertion=true;
					}
				}

				return userInsertion;
			}
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		return "Edit profile";
	}
}
