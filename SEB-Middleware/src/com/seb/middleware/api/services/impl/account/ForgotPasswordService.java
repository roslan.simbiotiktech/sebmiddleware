package com.seb.middleware.api.services.impl.account;

import java.io.IOException;

import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.ForgotPasswordRequest;
import com.seb.middleware.api.services.response.account.ForgotPasswordResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.ldap.LDAPConnector;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.sms.SMSManager;
import com.seb.middleware.utility.PasswordUtil;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "forgot_password", publicService = true, auditable = true)
public class ForgotPasswordService extends ServiceBase<ForgotPasswordRequest, ForgotPasswordResponse> {

	private static final Logger logger = LogManager.getLogger(ForgotPasswordService.class);
	
	public ForgotPasswordService(ForgotPasswordRequest request) {
		super(request);
	}

	@Override
	public ForgotPasswordResponse perform() throws ServiceException {
		ForgotPasswordResponse response = new ForgotPasswordResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		LDAPConnector ldap = new LDAPConnector();
		try {
			
			db.beginTransaction();
			
			MemberHelper memberHelper = new MemberHelper(db);
			SettingHelper settingHelper = new SettingHelper(db);
			
			boolean testingEnvironment = settingHelper.isTestingEnvironment();
			User user = memberHelper.getUser(request.getEmail());
			user.setPasswordExpired(1);
			user.setExistingPassword(null);
			db.update(user);
			
			String password = PasswordUtil.generatePassword();

			if(testingEnvironment){
				logger.debug("New Password: " + password);	
			}
			
			try {
				Person person = memberHelper.getPerson(request.getEmail());
				SMSManager sms = new SMSManager();
				sms.sendForgotPassword(person.getMobilePhone(), password);
			} catch (IOException e) {
				logger.error("Error sending SMS password", e);
				throw new ServiceException(e, 
						ResourceUtil.get("error.general_500", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			}

			ldap.resetPassword(request.getEmail(), password);
			db.commit();
			
			logger.debug("Password reset successfully for : " + request.getEmail());
			response.getResponseStatus().setStatus(SUCCESS);
			
			return response;
			
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		} catch (DatabaseFacadeException e) {
			logger.error("Error Forgot password", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} finally {
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		return "Forgot password";
	}
}
