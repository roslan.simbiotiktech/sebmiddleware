package com.seb.middleware.api.services.impl.account;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.GenerateClientIdRequest;
import com.seb.middleware.api.services.response.account.GenerateClientIdResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.RememberMe;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "generate_client_id", publicService = true)
public class GenerateClientIdService extends ServiceBase<GenerateClientIdRequest, GenerateClientIdResponse> {

	private static final Logger logger = LogManager.getLogger(GenerateClientIdService.class);
	
	public GenerateClientIdService(GenerateClientIdRequest request) {
		super(request);
	}

	@Override
	public GenerateClientIdResponse perform() throws ServiceException {
		GenerateClientIdResponse response = new GenerateClientIdResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		try {
			int counter = 0;
			MemberHelper helper = new MemberHelper(db);
			
			String clientId = getSecureRandomId();
			RememberMe found = helper.getRememberedSession(clientId);
			
			boolean foundUnique = found == null;
			
			while(found != null && counter < 10){
				counter++;
				clientId = getSecureRandomId();
				found = helper.getRememberedSession(clientId);
				
				if(found == null){
					foundUnique = true;
				}
			}
			
			if(foundUnique){
				response.setClientId(clientId);
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			}else{
				logger.error("error generating unique client id");
				throw new ServiceException( 
						ResourceUtil.get("error.general_500", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			}
			
		} finally {
			db.close();
		}
	}
	
	private String getSecureRandomId(){
		SecureRandom random = new SecureRandom();
		return new BigInteger(130, random).toString(32);
	}
}
