package com.seb.middleware.api.services.impl.account;

import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.GetAccountDetailsRequest;
import com.seb.middleware.api.services.response.Photo;
import com.seb.middleware.api.services.response.account.GetAccountDetailsResponse;
import com.seb.middleware.constant.LocaleType;
import com.seb.middleware.constant.LoginPrompt;
import com.seb.middleware.constant.UserStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.utility.CryptoUtil;
import com.seb.middleware.utility.ResourceUtil;
import com.seb.middleware.utility.StringUtil;

@Service(name = "get_account_details", publicService = true, auditable = true)
public class GetAccountDetailsService extends ServiceBase<GetAccountDetailsRequest, GetAccountDetailsResponse>  {

	private static final Logger logger = LogManager.getLogger(GetAccountDetailsService.class);
	
	
	public GetAccountDetailsService(GetAccountDetailsRequest request) {
		super(request);
	}

	@Override
	public GetAccountDetailsResponse perform() throws ServiceException {
		GetAccountDetailsResponse response = new GetAccountDetailsResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			MemberHelper memberHelper = new MemberHelper(db);
			
			User user = memberHelper.getUser(request.getEmail()); // This could be User ID from Existing system
			Person person  =  memberHelper.getPerson(request.getEmail()); // This could be User ID from Existing system
			if(user != null && person != null){
				
				response.getResponseStatus().setStatus(SUCCESS);
				response.setName(person.getName());
				response.setNricOrPassport(person.getNricPassport());
				response.setMobileNumber(person.getMobilePhone());
				response.setOfficeTel(person.getOfficePhone());
				response.setHomeTel(person.getHomePhone());
				response.setLastLogin(user.getLastLoginAt());
				
				if(user.getPreferredLocale() != null){
					response.setPreferredLocale(user.getPreferredLocale());
				}else{
					response.setPreferredLocale(LocaleType.en.toString());
				}
				
				if(user.getProfilePhoto() != null && !StringUtil.isEmpty(user.getProfilePhotoType())){
					String profilePhotoBase64 = CryptoUtil.encodeBase64(user.getProfilePhoto());
					response.setProfilePhoto(new Photo(user.getProfilePhotoType(), profilePhotoBase64));
				}
				
				if(user.getPreferredCommunicationMethod() != null)
					response.setPreferredCommMethod(user.getPreferredCommunicationMethod().toString());
				
				if(user.getStatus() == UserStatus.PENDING){
					logger.debug("Old user getAccountDetails");
					response.setEmail(user.getExistingEmail());
					
					response.setPrompt(LoginPrompt.ACCOUNT_ACTIVATION.toString());
					
				}else if(user.getStatus() == UserStatus.DEACTIVATED){
					logger.debug("New user getAccountDetails - Deactivated account");
					response.setEmail(user.getUserId());
					
					response.setPrompt(LoginPrompt.REACTIVATION.toString());
					
				}else{
					logger.debug("New user getAccountDetails");
					response.setEmail(user.getUserId());
					
					if(user.getPasswordExpired() == 1){
						response.setPrompt(LoginPrompt.CHANGE_PASSWORD.toString());
					}
				}
				
				return response;
			}else{
				throw new ServiceException(
						ResourceUtil.get("validation.invalid_user", 
								request.getLocale(), "", request.getEmail()), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		}finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		return "Read account details";
	}
}
