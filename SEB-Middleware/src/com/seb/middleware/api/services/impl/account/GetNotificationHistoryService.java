package com.seb.middleware.api.services.impl.account;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.GetNotificationHistoryRequest;
import com.seb.middleware.api.services.response.Pagination;
import com.seb.middleware.api.services.response.account.GetNotificationHistoryResponse;
import com.seb.middleware.api.services.response.account.Notification;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.NotificationHistory;
import com.seb.middleware.jpa.helper.NotificationHelper;

@Service(name = "get_notification_history", publicService = true, auditable = true)
public class GetNotificationHistoryService extends ServiceBase<GetNotificationHistoryRequest, GetNotificationHistoryResponse>  {

	private static final Logger logger = LogManager.getLogger(GetNotificationHistoryService.class);

	public GetNotificationHistoryService(GetNotificationHistoryRequest request) {
		super(request);
	}

	@Override
	public GetNotificationHistoryResponse perform() throws ServiceException {
		GetNotificationHistoryResponse response = new GetNotificationHistoryResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		Pagination pagination = new Pagination();
		pagination.setPaging(request.getPaging());
		
		response.setPagination(pagination);
		
		try{
			NotificationHelper notificationHelper = new NotificationHelper(db);
			int unread = (int) notificationHelper.getUnreadNotificationCount(request.getEmail());
			response.setUnreadCount(unread);
			
			ArrayList<NotificationHistory> notis = notificationHelper.getNotificationHistory(request.getEmail(), pagination);
			if(notis != null && !notis.isEmpty()){
				Notification [] notifications = new Notification[notis.size()];
				for(int i = 0; i < notis.size(); i++){
					NotificationHistory noti = notis.get(i);
					Notification notification = new Notification();
					notification.setNotificationId(String.valueOf(noti.getNotificationId()));
					notification.setMessage(noti.getText());
					notification.setTitle(noti.getTitle());
					notification.setType(noti.getNotificationType().toString());
					
					if(noti.getAssociatedId() != null){
						notification.setAssociatedId(String.valueOf(noti.getAssociatedId().longValue()));	
					}
					
					notification.setNotifyDatetime(noti.getCreatedDatetime());
					notification.setStatus(noti.getStatus().toString());
					notifications[i] = notification;
				}
				response.setNotifications(notifications);
			}else{
				logger.debug("User " + request.getEmail() + " has no notification history.");
			}
			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		} finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		return "Read notification history";
	}
}
