package com.seb.middleware.api.services.impl.account;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.naming.AuthenticationException;
import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.LoginRequest;
import com.seb.middleware.api.services.response.account.LoginResponse;
import com.seb.middleware.constant.AuthenticationStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.RememberMe;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.ldap.LDAPConnector;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.utility.CryptoUtil;
import com.seb.middleware.utility.ResourceUtil;
import com.seb.middleware.utility.StringUtil;

@Service(name = "login", publicService = true, auditable = true)
public class LoginService extends ServiceBase<LoginRequest, LoginResponse> {

	private static final Logger logger = LogManager.getLogger(LoginService.class);
	
	public LoginService(LoginRequest request) {
		super(request);
	}

	@Override
	public LoginResponse perform() throws ServiceException {
		
		LoginResponse response = new LoginResponse();
		
		try {
			boolean emailProvided = request.getEmail() != null && !request.getEmail().isEmpty();
			boolean passwordProvided = request.getPassword() != null && !request.getPassword().isEmpty();
			boolean clientIdProvided = request.getClientId() != null && !request.getClientId().isEmpty();
			
			if(emailProvided && passwordProvided){
				DatabaseFacade db = new DatabaseFacade();
				MemberHelper memberHelper = new MemberHelper(db);
				SettingHelper settingHelper = new SettingHelper(db);
				
				try{
					User user = memberHelper.getUser(request.getEmail());
					
					if(user == null){
						throw new ServiceException(
								ResourceUtil.get("error.invalid_login_credential", 
										request.getLocale()), 
										APIResponse.LOGIN_ERROR_CODE_INVALID_CREDENTIAL);
					}else{
						LDAPConnector ldap = new LDAPConnector();
						
						boolean dbPassMatching = false;
						AuthenticationStatus authStatus = AuthenticationStatus.FAILED;
						
						if(StringUtil.isEmpty(user.getExistingPassword())){
							authStatus = ldap.authenticate(request.getEmail(), request.getPassword());
						}else{
							dbPassMatching = true;
							
							String hex = CryptoUtil.sha256HexadecimalLowercase(request.getPassword());
							
							if(settingHelper.isTestingEnvironment()){
								logger.debug("Hash Password: " + hex);
								logger.debug("user.getExistingPassword(): " + user.getExistingPassword());
							}
							
							if(hex.equalsIgnoreCase(user.getExistingPassword())){
								authStatus = AuthenticationStatus.SUCCESS;
							}
						}

						if(authStatus == AuthenticationStatus.FAILED){
							throw new ServiceException(
									ResourceUtil.get("error.invalid_login_credential", 
											request.getLocale()), 
											APIResponse.LOGIN_ERROR_CODE_INVALID_CREDENTIAL);
						}else{
							db.beginTransaction();

							if(clientIdProvided && 
									request.getRememberMe() != null && request.getRememberMe().booleanValue() == true){
								memberHelper.RememberClient(request.getClientId().trim(), request.getEmail());
							}
							
							Date previousLogin = user.getCurrentLoginAt();
							if(previousLogin == null){
								Date now = new Date();
								user.setLastLoginAt(now);
								user.setCurrentLoginAt(now);
							}else{
								user.setLastLoginAt(previousLogin);
								user.setCurrentLoginAt(new Date());
							}
							
							if(authStatus == AuthenticationStatus.SUCCESS_PASSWORD_EXPIRED_WARNING){
								user.setPasswordExpired(1);
							}

							if(dbPassMatching){
								user.setExistingPassword(null);
							}
							
							db.update(user);
							db.commit();
							
							if(dbPassMatching){
								// Do this after committed
								logger.info("Existing eService user login successfully with DB password, system will now import the password into LDAP");
								ldap.resetPassword(request.getEmail(), request.getPassword());
								logger.info("imported successfully");
							}
							
							response.setEmail(request.getEmail());
							response.getResponseStatus().setStatus(SUCCESS);
							return response;
						}
					}
				} finally{
					db.close();
				}
			}else if(clientIdProvided){
				DatabaseFacade db = new DatabaseFacade();
				try{
					MemberHelper memberHelper = new MemberHelper(db);
					RememberMe rememberedSession = memberHelper.getRememberedSession(request.getClientId().trim());
					if(memberHelper.isRememberedSessionValid(rememberedSession)){
						
						User user = memberHelper.getUser(rememberedSession.getUserEmail());
						Date previousLogin = user.getCurrentLoginAt();
						if(previousLogin == null){
							Date now = new Date();
							user.setLastLoginAt(now);
							user.setCurrentLoginAt(now);
						}else{
							user.setLastLoginAt(previousLogin);
							user.setCurrentLoginAt(new Date());
						}
						db.beginTransaction();
						db.update(user);
						db.commit();
						
						response.setEmail(rememberedSession.getUserEmail());
						response.getResponseStatus().setStatus(SUCCESS);
						return response;
					}else{
						if(!emailProvided){
							String field = ResourceUtil.get("field.email", request.getLocale());
							String msg = ResourceUtil.get("validation.mandatory", request.getLocale(), field);
							throw generateNonPrintingServiceException(msg, APIResponse.LOGIN_ERROR_CODE_MISSING_CREDENTIAL);
						}else{
							String field = ResourceUtil.get("field.password", request.getLocale());
							String msg = ResourceUtil.get("validation.mandatory", request.getLocale(), field);
							throw generateNonPrintingServiceException(msg, APIResponse.LOGIN_ERROR_CODE_MISSING_CREDENTIAL);
						}
					}
					
				}finally{
					db.close();
				}
			}else{
				if(!emailProvided){
					String field = ResourceUtil.get("field.email", request.getLocale());
					String msg = ResourceUtil.get("validation.mandatory", request.getLocale(), field);
					throw generateNonPrintingServiceException(msg, APIResponse.LOGIN_ERROR_CODE_MISSING_CREDENTIAL);
				}else{
					String field = ResourceUtil.get("field.password", request.getLocale());
					String msg = ResourceUtil.get("validation.mandatory", request.getLocale(), field);
					throw generateNonPrintingServiceException(msg, APIResponse.LOGIN_ERROR_CODE_MISSING_CREDENTIAL);
				}
			}
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			logger.error("Hashing error", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_500", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} catch (DatabaseFacadeException e) {
			logger.error("Error Updating User's Password expiry flag", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} catch (AuthenticationException e) {
			logger.error("Error authenticating user", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.invalid_login_credential", 
							request.getLocale()), 
							APIResponse.LOGIN_ERROR_CODE_INVALID_CREDENTIAL);
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		}
	}
	
	private ServiceException generateNonPrintingServiceException(String message, String code) {
		ServiceException se = new ServiceException(message, code);
		se.setPrint(false);
		return se;
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		return "Login";
	}
}
