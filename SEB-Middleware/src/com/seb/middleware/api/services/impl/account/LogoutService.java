package com.seb.middleware.api.services.impl.account;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.LogoutRequest;
import com.seb.middleware.api.services.response.account.LogoutResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.RememberMe;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "logout", publicService = true, auditable = true)
public class LogoutService extends ServiceBase<LogoutRequest, LogoutResponse> {

	private static final Logger logger = LogManager.getLogger(LogoutService.class);
	
	public LogoutService(LogoutRequest request) {
		super(request);
	}

	@Override
	public LogoutResponse perform() throws ServiceException {
		
		LogoutResponse response = new LogoutResponse();
		DatabaseFacade db = new DatabaseFacade();
		try {
			MemberHelper memberHelper = new MemberHelper(db);
			RememberMe rememberedSession = memberHelper.getRememberedSession(request.getClientId().trim());
			
			if(rememberedSession != null){
				db.beginTransaction();
				db.delete(rememberedSession);
				db.commit();
			}
			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		} catch (DatabaseFacadeException e) {
			logger.error("Error Logout User", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} finally {
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		return "Logout";
	}
}
