package com.seb.middleware.api.services.impl.account;

import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.PreverifyRegistrationRequest;
import com.seb.middleware.api.services.response.account.PreverifyRegistrationResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.ldap.LDAPConnector;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "preverify_registration", publicService = true, auditable = true)
public class PreverifyRegistrationService extends ServiceBase<PreverifyRegistrationRequest, PreverifyRegistrationResponse> {

	private static final Logger logger = LogManager.getLogger(PreverifyRegistrationService.class);
	
	public PreverifyRegistrationService(PreverifyRegistrationRequest request) {
		super(request);
	}

	@Override
	public PreverifyRegistrationResponse perform() throws ServiceException {
		PreverifyRegistrationResponse response = new PreverifyRegistrationResponse();

		DatabaseFacade db = new DatabaseFacade();
		try{
			
			LDAPConnector ldap = new LDAPConnector();
			MemberHelper userHelper = new MemberHelper(db);
			User user = userHelper.getUser(request.getEmail());
			
			if(user == null){
				boolean existLdap = ldap.isUserExist(request.getEmail());
				if(existLdap) {
					logger.info("Preverify registration failed - user with email: " + request.getEmail() + ", already existed in LDAP.");	
					throw new ServiceException(
							ResourceUtil.get("error.user_email_taken", 
									request.getLocale()), 
									APIResponse.ERROR_CODE_EMAIL_EXISTED);
				}
			}else{
				logger.info("Preverify registration failed - user with email: " + request.getEmail() + ", already existed in Database.");
				throw new ServiceException(
						ResourceUtil.get("error.user_email_taken", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_EMAIL_EXISTED);
			}
			
			response.getResponseStatus().setStatus(SUCCESS);
			return response;
			
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		} finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		return "Preverify Registration";
	}
}
