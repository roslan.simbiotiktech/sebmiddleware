package com.seb.middleware.api.services.impl.account;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.ReactivateAccountRequest;
import com.seb.middleware.api.services.response.account.ReactivateAccountResponse;
import com.seb.middleware.constant.SubscriptionStatus;
import com.seb.middleware.constant.UserStatus;
import com.seb.middleware.constant.VerificationCodeStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.Subscription;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.jpa.helper.VerificationHelper;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "reactivate_account", publicService = true, auditable = true)
public class ReactivateAccountService extends ServiceBase<ReactivateAccountRequest, ReactivateAccountResponse>{

	private static final Logger logger = LogManager.getLogger(ReactivateAccountService.class);
	
	public ReactivateAccountService(ReactivateAccountRequest request) {
		super(request);
	}

	@Override
	public ReactivateAccountResponse perform() throws ServiceException {
		
		ReactivateAccountResponse response = new ReactivateAccountResponse();
		
		String email = request.getEmail();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			MemberHelper memberHelper = new MemberHelper(db);
			VerificationHelper verificatonHelper = new VerificationHelper(db);
			
			User user = memberHelper.getUser(email);
			
			if(user != null){
				
				if(user.getStatus() == UserStatus.DEACTIVATED){
					
					Person person = memberHelper.getPerson(email);
					
					db.beginTransaction();
					
					VerificationCodeStatus smsVerified = 
							verificatonHelper.consumeMobileVerification(person.getMobilePhone(), request.getActivationCode());
					
					if(smsVerified == VerificationCodeStatus.VALID){
						SettingHelper settingHelper = new SettingHelper(db);
						
						// Step 1 : Change user's status
						user.setStatus(UserStatus.ACTIVE);
						
						// Step 2: Active Subscriptions
						SubscriptionHelper subscriptionHelper = new SubscriptionHelper(db);
						ArrayList<Subscription> inactiveSubscriptions = subscriptionHelper.getSubscriptions(user.getUserId(), SubscriptionStatus.INACTIVE);
						if(inactiveSubscriptions != null && !inactiveSubscriptions.isEmpty()){
							int maxSubscribeAllowed = settingHelper.getContractMaxSubscriptions();
							
							logger.info("user " + user.getUserId() + " have a total " + inactiveSubscriptions.size() + " to activate.");
							for(int i = 0; i < inactiveSubscriptions.size(); i++){
								Subscription subscription = inactiveSubscriptions.get(i);
								logger.info("Migrating " + 1 + "/" + inactiveSubscriptions.size());
								logger.info("Contract Account No: " + subscription.getContractAccNo());
								logger.info("Contract Account Name: " + subscription.getContract().getContractAccountName());
								
								// Update to new email
								subscription.setUserEmail(email);
								
								if(subscriptionHelper.isContractAccountCanSubscribe(maxSubscribeAllowed, subscription.getContractAccNo())){
									subscription.setStatus(SubscriptionStatus.ACTIVE);
									logger.info("Migration suceeded for subscription : " + subscription.getContractAccNo());
								}else{
									logger.info("Migration failed for subscription : Subscriptions max");
									logger.info(subscription);
								}
								db.update(subscription);
							}
						}else{
							logger.info("user " + user.getUserId() + " does no have any subscription to migrate.");
						}
						
						db.commit();
						
						logger.info("User re-activation completed for :" + email);
						response.getResponseStatus().setStatus(SUCCESS);
						
						return response;
					}else{
						if(smsVerified == VerificationCodeStatus.INVALID){
							logger.warn("Incorrect verification code for SMS : " + person.getMobilePhone() + ", code : " + request.getActivationCode());
							throw new ServiceException(
									ResourceUtil.get("error.phone_verification_incorrect", 
											request.getLocale()), 
											APIResponse.ERROR_CODE_INVALID_INPUT);
						}else{
							logger.warn("Expired verification code for SMS : " + person.getMobilePhone() + ", code : " + request.getActivationCode());
							throw new ServiceException(
									ResourceUtil.get("error.phone_verification_expired", 
											request.getLocale()), 
											APIResponse.ERROR_CODE_INVALID_INPUT);
						}							
					}
				}else{
					logger.error("User Id: " + email + " doesnt required activation.");
					throw new ServiceException(
							ResourceUtil.get("error.general_500", 
									request.getLocale()), 
									APIResponse.ERROR_CODE_SERVER_EXCEPTION);
				}
			}else{
				logger.error("User Id: " + email + " doesnt exist in the system.");
				throw new ServiceException(
						ResourceUtil.get("error.general_500", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			}
		} catch (DatabaseFacadeException e) {
			logger.error("Error reactivating account", e);
			
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			logger.error("Verification code error", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		} finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("Re-Activate account");
		return sb.toString();
	}
}
