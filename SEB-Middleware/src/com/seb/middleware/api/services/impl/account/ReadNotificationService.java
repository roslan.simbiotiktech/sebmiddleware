package com.seb.middleware.api.services.impl.account;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.ReadNotificationRequest;
import com.seb.middleware.api.services.response.account.ReadNotificationResponse;
import com.seb.middleware.constant.MessageStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.NotificationHistory;
import com.seb.middleware.jpa.helper.NotificationHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "read_notification", publicService = true, auditable = true)
public class ReadNotificationService extends ServiceBase<ReadNotificationRequest, ReadNotificationResponse>  {

	private static final Logger logger = LogManager.getLogger(ReadNotificationService.class);

	public ReadNotificationService(ReadNotificationRequest request) {
		super(request);
	}

	@Override
	public ReadNotificationResponse perform() throws ServiceException {
		ReadNotificationResponse response = new ReadNotificationResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			NotificationHelper notificationHelper = new NotificationHelper(db);
			NotificationHistory notis = notificationHelper.getNotificationHistory(request.getEmail(), request.getNotificationId());
			
			if(notis != null){
				if(notis.getStatus() == MessageStatus.UNREAD){
					notis.setStatus(MessageStatus.READ);
					db.beginTransaction();
					db.update(notis);
					db.commit();
				}else{
					logger.warn("User " + request.getEmail() + " with notification with id " + request.getNotificationId() + " was already read.");
				}
			}else{
				logger.warn("User " + request.getEmail() + " doesn't have notification with id " + request.getNotificationId());
			}
			
			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		} catch (DatabaseFacadeException e) {
			logger.error("Error marking notification as read", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("Read push notification (id): ");
		sb.append(request.getNotificationId());
		return sb.toString();
	}
}
