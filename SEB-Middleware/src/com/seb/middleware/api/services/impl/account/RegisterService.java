package com.seb.middleware.api.services.impl.account;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.RegisterRequest;
import com.seb.middleware.api.services.response.account.RegisterResponse;
import com.seb.middleware.constant.PreferredCommunicationMethod;
import com.seb.middleware.constant.UserStatus;
import com.seb.middleware.constant.VerificationCodeStatus;
import com.seb.middleware.email.EmailManager;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.CrmUserProfile;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.VerificationHelper;
import com.seb.middleware.ldap.LDAPConnector;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.salesforce.api.CreateAccountRequestInfo;
import com.seb.middleware.salesforce.api.QueryAccountRequestInfo;
import com.seb.middleware.salesforce.api.UpdateAccountRequestInfo;
import com.seb.middleware.salesforce.auth.AuthenticationException;
import com.seb.middleware.salesforce.auth.Token;
import com.seb.middleware.salesforce.core.CompositeResponse;
import com.seb.middleware.salesforce.core.HTTPException;
import com.seb.middleware.salesforce.core.QueryAccountRequestResponseInfo;
import com.seb.middleware.salesforce.core.SFUtility;
import com.seb.middleware.salesforce.core.SalesForceCompositeConnector;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "register", publicService = true, auditable = true)
public class RegisterService extends ServiceBase<RegisterRequest, RegisterResponse> {

	private static final Logger logger = LogManager.getLogger(RegisterService.class);
	
	public RegisterService(RegisterRequest request) {
		super(request);
	}

	@SuppressWarnings("null")
	@Override
	public RegisterResponse perform() throws ServiceException {
		RegisterResponse response = new RegisterResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		User inserted = null;
		try{
			boolean existed = true;
			
			LDAPConnector ldap = new LDAPConnector();
			MemberHelper userHelper = new MemberHelper(db);
			User user = userHelper.getUser(request.getEmail());
			VerificationHelper verificatonHelper = new VerificationHelper(db);
			SettingHelper settingHelper = new SettingHelper(db);
			
			if(user == null){
				boolean existLdap = ldap.isUserExist(request.getEmail());
				if(!existLdap) {
					existed = false;	
				}else{
					logger.info("unable to registered user with email: " + request.getEmail() + ", already existed in LDAP.");
				}
			}else{
				logger.info("unable to registered user with email: " + request.getEmail() + ", already existed in Database.");
			}

			if(!existed){
				
				db.beginTransaction();
				VerificationCodeStatus mobileVerified = verificatonHelper.consumeMobileVerification(request.getMobileNumber(), request.getVerificationCode());
				
				if(mobileVerified == VerificationCodeStatus.VALID){
					user = new User();
					user.setUserId(request.getEmail());
					user.setPreferredCommunicationMethod(PreferredCommunicationMethod.valueOf(request.getPreferredCommunicationMethod()));
					user.setStatus(UserStatus.ACTIVE);
					
					userHelper.RememberClient(request.getClientId().trim(), request.getEmail());					
					inserted = db.insert(user);
					db.commit();					
					Person person = new Person();
					person.setEmail(request.getEmail());
					person.setName(request.getName());
					person.setUserPassword(request.getPassword());
					person.setMobilePhone(request.getMobileNumber());
					person.setNricPassport(request.getNricOrPassport());
					person.setHomePhone(null);
					person.setOfficePhone(null);
					ldap.addUser(person);
					
					//added by pramod for Nric manipulation in Ldap
					if((person.getNricPassport()!=null)&&(person.getNricPassport().length()!=0)&&(!person.getNricPassport().isEmpty()) ) {
						String regex = "[0-9]+";
						boolean verifyNumeric =true;
						verifyNumeric=person.getNricPassport().matches(regex);
						if((person.getNricPassport().length()==12)&&(verifyNumeric==true)) {
							logger.info("NricPassport >> " + person.getNricPassport());
							String NricPassport=	this.set14CharNricNumber(person.getNricPassport());
							LDAPConnector ldapPerson = new LDAPConnector();
							person.setNricPassport(NricPassport);
							ldapPerson.updateUser(person);
							logger.info("NricPassport LDAP updated ");
							person= userHelper.getPerson(request.getEmail());
							logger.info("get NricPassport LDAP updated value " +person.getNricPassport());
						}else {
							logger.info("NricPassport length not matched with 12 digit and and numric >> "+person.getNricPassport());
						}
					}					
					
				
					//added by pramod for MobilePhone manipulation
					if((person.getMobilePhone()!=null)&&(person.getMobilePhone().length()!=0)&&(!person.getMobilePhone().isEmpty()) ) {				
						logger.info("phoneNumber >> " +person.getMobilePhone());
						String phoneNumber=person.getMobilePhone();
						String intitialDigit=StringUtils.substring(phoneNumber, 0, 1);
						logger.info("intitialDigit phoneNumber  >> "+intitialDigit);
						String phoneNumberFinal="";
						// number  starts 0 and  9,10 digit format 
						 if( (intitialDigit.equals("0")) &&((phoneNumber.length()==10) || (phoneNumber.length()==11))) {
							 phoneNumberFinal =this.zeroInitialPhoneNumber(phoneNumber);					
							 logger.info("phoneNumberFinal >>" +phoneNumberFinal);
							// number  starts 1 and  10,11 digit format
						 }else if((intitialDigit.equals("1")) &&((phoneNumber.length()==9) || (phoneNumber.length()==10))) {
							 phoneNumberFinal =this.oneInitialPhoneNumber(phoneNumber);					
							 logger.info("phoneNumberFinal >>" +phoneNumberFinal);
						 }	
						 //LDAP Updation
						 if((phoneNumberFinal!=null)&&(phoneNumberFinal.length()!=0)&&(!phoneNumberFinal.isEmpty())) {				 
							  LDAPConnector ldapPhoneNumber = new LDAPConnector(); 
							  person.setMobilePhone(phoneNumberFinal);
							  ldapPhoneNumber.updateUser(person); 
							  logger.info("MobilePhone LDAP updated "); 
							  person=userHelper.getPerson(request.getEmail());
							  logger.info("get MobilePhone LDAP updated value " +person.getMobilePhone());				 
						 }else {
							 logger.info("Number validation failed  >> " +phoneNumber);
						 }
					}
					
					
					
					// New CR-10 Changes
					
					CompositeResponse qaresp=	this.queryAccountRequestInfo(user, person);
					CompositeResponse caresp=null;
					if(!qaresp.getQueryAccountInfo()) {
						logger.info("Profile not available Sales Force system,SEB Cares system with E-mail, NRIC/Passport and Mobile Number : " + request.getEmail());
						logger.info("Create profile in CRM system :"+ request.getEmail());
						 caresp=	this.createAccountRequest(user, person);	
						if(caresp.isSuccess()) {								
							Boolean sfdcIdUpdation =this.userUpdation(user,caresp.getObjectId());
							if(sfdcIdUpdation) {
								logger.info("sfdcId Update done : " + request.getEmail());
							}else {
								logger.info("sfdcId Update failed : " + request.getEmail());
							}
							/// insert new CRM user profile table 
							CrmUserProfile crmUserProfile=new CrmUserProfile();
							crmUserProfile.setEmail_1(request.getEmail());
							crmUserProfile.setMobile_1(person.getMobilePhone());
							crmUserProfile.setSfdcId(caresp.getObjectId());
							crmUserProfile.setSfdcUpdatedDate1(new Date());
							Boolean crmProfileInsertion =this.crmUserInsertion(crmUserProfile);
							if(crmProfileInsertion) {
								logger.info("crm User Insertion  done : " + request.getEmail());
							}else {
								logger.info("crm User Insertion failed : " + request.getEmail());
							}								
						}else {
							logger.info("Error occured while creating the profile in CRM : " + user.getUserId());
						}
					}else {
						// profile available in CRM system and not available in SEB Care  and Ldap
						logger.info("Profile  available Sales Force system with E-mail, NRIC/Passport and Mobile Number  But not available in SEB Cares system: " + request.getEmail());
						
						// need to update query  CRM  new user available at CRM to Email-1 and mobile-1
						
						CrmUserProfile crmUserProfiles = userHelper.getCrmUserProfileWithSfdcID(qaresp.getFirstObjectId());
					
							// CRM profile already having the data
							
							CompositeResponse compositeResponseUpdateAccountResponse=null;
							QueryAccountRequestResponseInfo queryAccountRequestResponseInfo=null;
							QueryAccountRequestResponseInfo	 queryAccountResponseInfo	=qaresp.getQueryAccountResponseInfo();
							//for Update Sales Force need to justify which set of pair need to update.																											
							// set new profile with numbering 
							Boolean crmProdileAvailableInd= this.getCrmAllThreeProfileExists(crmUserProfiles.getSfdcId());
							QueryAccountRequestResponseInfo queryAccountResponsePairs=qaresp.getQueryAccountResponseInfo();
							if(crmProdileAvailableInd) {
								logger.info("Already exists CRM profiles with less than three in SEB Cares and Sales Force");
								queryAccountRequestResponseInfo=this.getNotExistsAllThreeCrmProfilesRequestBody(person,queryAccountResponsePairs);
							}else {
								logger.info("Already exists CRM profiles equal or more than three in SEB Cares and Sales Force");
								queryAccountRequestResponseInfo=this.setPairToUpdateNewProfileInCrm(person,queryAccountResponseInfo,qaresp.getFirstObjectId());	
							}
							
							compositeResponseUpdateAccountResponse=	this.updateAccountRequestInfoWithExistingProfiles(person,queryAccountRequestResponseInfo,qaresp.getFirstObjectId());		
							if(compositeResponseUpdateAccountResponse.getHttpStatusCode()==204) {	
								logger.info("Update CRM  success with new profile : " + user.getUserId()+" : sfdcID : "+compositeResponseUpdateAccountResponse.getFirstObjectId());		
								logger.info("Updation of existing sfdcId with new Email : "+crmUserProfiles.getSfdcId()+" <<<<>>>>  "+user.getUserId());
								user.setSfdcId(qaresp.getFirstObjectId());
								user.setSfdcUpdatedDate(new Date());
								Boolean sfdcIdUpdation =this.userUpdation(user,qaresp.getFirstObjectId())	;
								if(sfdcIdUpdation) {
									logger.info("sfdcId Update done with new E-Mail of existing profile  : " + request.getEmail());
								}else {
									logger.info("sfdcId Update failed with new E-Mail of existing profile : " + request.getEmail());
								}								
								// update new CRM user profile table 																																			
								Boolean userCrmUpdation=this.crmUserUpdateWithNewSfdcid(crmUserProfiles.getSfdcId(),qaresp.getFirstObjectId(),queryAccountRequestResponseInfo);									
								if(userCrmUpdation) {
									logger.info("sfdcId  done Crm user updation : " + request.getEmail());
								}else {
									logger.info("sfdcId  failed Crm user updation: " + request.getEmail());
								}
							}
						
					}
					logger.info("User registration completed: " + request.getEmail());
					response.getResponseStatus().setStatus(SUCCESS);
					
					//Send Email to user upon successful register account
					try{
						EmailManager manager = new EmailManager();
						manager.sendSuccessRegisterEmail(request.getEmail(), settingHelper.getFaqUrl());
					}catch(Exception ex){
						logger.error("Error while sending success register email to [" + request.getEmail() + "].", ex);
					}
					
					return response;
				}else{
					if(mobileVerified == VerificationCodeStatus.INVALID){
						logger.warn("Incorrect verification code for Mobile : " + request.getMobileNumber() + ", code : " + request.getVerificationCode());
						throw new ServiceException(
								ResourceUtil.get("error.phone_verification_incorrect", 
										request.getLocale()), 
										APIResponse.ERROR_CODE_INVALID_INPUT);
					}else{
						logger.warn("Expired verification code for Mobile : " + request.getMobileNumber() + ", code : " + request.getVerificationCode());
						throw new ServiceException(
								ResourceUtil.get("error.phone_verification_expired", 
										request.getLocale()), 
										APIResponse.ERROR_CODE_INVALID_INPUT);
					}
				}
			}else{
				throw new ServiceException(
						ResourceUtil.get("error.user_email_taken", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_EMAIL_EXISTED);
			}
		} catch (NamingException e) {
			if(inserted != null){
				DatabaseFacade db2 = new DatabaseFacade();
				try {
					db2.beginTransaction();
					db2.delete(inserted);
					db2.commit();
				} catch (DatabaseFacadeException e1) {
					logger.error("Error while reverting DB user creation.", e1);
				} finally {
					db2.close();
				}
			}

			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		} catch (DatabaseFacadeException e) {
			logger.error("Error create new user", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			logger.error("Verification code error", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} finally{
			db.close();
		}
	}
	
	// sales force  call for  QueryAccountRequest
	public CompositeResponse queryAccountRequestInfo(User user,Person person ) {
		CompositeResponse  compositeResponse=null;
		DatabaseFacade db = new DatabaseFacade();
		SettingHelper settingHelper = new SettingHelper(db);
		try {
			db.beginTransaction();
			Token token = SFUtility.getToken(settingHelper);
			SalesForceCompositeConnector connector = new SalesForceCompositeConnector(token);
			QueryAccountRequestInfo queryAccountRequest=new QueryAccountRequestInfo(user.getUserId(), person.getMobilePhone(), person.getNricPassport(), user.getUserId(), user.getUserId(), user.getUserId(), person.getMobilePhone(), person.getMobilePhone(), person.getMobilePhone());
			
			compositeResponse = connector.invoke(queryAccountRequest);
			db.commit();
			db.close();
		
		} catch (AuthenticationException|HTTPException |DatabaseFacadeException| IOException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return compositeResponse;
	}
	
	

	
	// updating the existig user  with CRM profiles
	public CompositeResponse updateAccountRequestInfoWithExistingProfiles(Person person,QueryAccountRequestResponseInfo updateAccountRequestResponseInfo,String newSfdcId) {
		CompositeResponse compositeResponse=null;
		DatabaseFacade db = new DatabaseFacade();
		SettingHelper settingHelper = new SettingHelper(db);
		try {
			db.beginTransaction();
			logger.info("Update Account in Sales Force system with :: "+" call >> updateAccountRequestInfoWithExistingProfiles(Person person,QueryAccountRequestResponseInfo updateAccountRequestResponseInfo,String newSfdcId)");
			Token token = SFUtility.getToken(settingHelper);			
			SalesForceCompositeConnector connector = new SalesForceCompositeConnector(token);
			UpdateAccountRequestInfo queryAccountRequest=new UpdateAccountRequestInfo(newSfdcId, person.getName(), updateAccountRequestResponseInfo.getSebcEmailOne(), 
					
					updateAccountRequestResponseInfo.getSebcEmailTwo(), updateAccountRequestResponseInfo.getSebcEmailThree(), updateAccountRequestResponseInfo.getSebcMobileOne(), updateAccountRequestResponseInfo.getSebcMobileTwo(), updateAccountRequestResponseInfo.getSebcMobileThree());
			compositeResponse = connector.invoke(queryAccountRequest);	
			db.commit();
			db.close();
		} catch (AuthenticationException|HTTPException |DatabaseFacadeException| IOException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error("error update the existing  account to salesforce which having the sfdcId, however - " + e.getMessage(), e);
		}
		return compositeResponse;
	}
	// sales force  call for  CreateAccountRequest
	public CompositeResponse createAccountRequest(User user,Person person ) {
		CompositeResponse  compositeResponse=null;
		DatabaseFacade db = new DatabaseFacade();
		SettingHelper settingHelper = new SettingHelper(db);
		try {
			db.beginTransaction();
			logger.info("createAccountRequest in Sales Force system");
			Token token = SFUtility.getToken(settingHelper);			
			SalesForceCompositeConnector connector = new SalesForceCompositeConnector(token);
			CreateAccountRequestInfo createAccountRequest = new CreateAccountRequestInfo(user.getUserId(), person.getName(),
					person.getMobilePhone(), person.getEmail(), person.getNricPassport(), 
					settingHelper.getSalesForceAccountRecordTypeId(),person.getEmail(),person.getMobilePhone());
			compositeResponse = connector.invoke(createAccountRequest);
			db.commit();
			db.close();
			
		} catch (AuthenticationException|HTTPException |DatabaseFacadeException| IOException  e) {
			// TODO Auto-generated catch block
			logger.error("error creating the profile - " + e.getMessage(), e);
			e.printStackTrace();
		}
		
		return compositeResponse;
	}
	
	// user sfdcID update
	public Boolean userUpdation(User userEmail,String sfdcID) {
		Boolean  userInsertion=false;
		DatabaseFacade dbUser = new DatabaseFacade();
		MemberHelper userHelper = new MemberHelper(dbUser);
		
		if(userEmail != null){
		
			
			try {
				dbUser.beginTransaction();
				logger.info("user is update started with sdfcid "+userEmail.getUserId() +","+sfdcID);
				User existingUserInfo=userHelper.getUser(userEmail.getUserId());
				logger.info("existingUserInfo >> "+existingUserInfo.getUserId());
				existingUserInfo.setSfdcId(sfdcID);
				Date now =new Date();
				existingUserInfo.setSfdcUpdatedDate(now);
				existingUserInfo.setSfdcSyncDate(now);			
				dbUser.update(existingUserInfo);
				dbUser.commit();
			} catch (DatabaseFacadeException e1) {
				logger.error("Error while reverting DB user creation.", e1);
			} finally {
				dbUser.close();
				userInsertion=true;
			}
		}

		return userInsertion;
	}
	// Crm user sfdcID Insert
	public Boolean crmUserInsertion(CrmUserProfile crmUserProfile) {
		Boolean  userCrmInsertion=false;
		
		if(crmUserProfile != null){
			DatabaseFacade dbCrmUser = new DatabaseFacade();
			try {
				logger.info("crmUserProfile is insertion started with sdfcid "+crmUserProfile.getSfdcId());
				dbCrmUser.beginTransaction();
				dbCrmUser.insert(crmUserProfile);
				dbCrmUser.commit();
			} catch (DatabaseFacadeException e1) {
				logger.error("Error while reverting DB user creation.", e1);
			} finally {
				dbCrmUser.close();
				userCrmInsertion=true;
			}
		}
		return userCrmInsertion;
	}
	
	
	// Crm user sfdcID Updation
		public Boolean crmUserUpdateWithNewSfdcid(String oldestSfdcID,String newSfdcid,QueryAccountRequestResponseInfo queryAccountResponseInfo) throws DatabaseFacadeException {
			logger.info("call >> crmUserUpdateWithNewSfdcid(String oldestSfdcID,String newSfdcid,QueryAccountRequestResponseInfo queryAccountResponseInfo) ");
			Boolean  userCrmUpdation=false;
			DatabaseFacade dbGetCrmUser = new DatabaseFacade();
			MemberHelper userHelper = new MemberHelper(dbGetCrmUser);
			dbGetCrmUser.beginTransaction();
			logger.info("call >> 	getCrmUserProfileWithSfdcID(String sfdcId) ");	
			
			logger.info("Get CRM table sfdcID >>> "+ newSfdcid);
			
			CrmUserProfile crmUserProfiles = userHelper.getCrmUserProfileWithSfdcID(newSfdcid);
			if(crmUserProfiles != null){	
				try {					

					if(queryAccountResponseInfo.getSebcEmailOne()!=null && queryAccountResponseInfo.getSebcMobileOne()!=null) {
						crmUserProfiles.setEmail_1(queryAccountResponseInfo.getSebcEmailOne());
						crmUserProfiles.setMobile_1(queryAccountResponseInfo.getSebcMobileOne());
						crmUserProfiles.setSfdcId(newSfdcid);
						crmUserProfiles.setSfdcUpdatedDate1(new Date());
					}else if(queryAccountResponseInfo.getSebcEmailTwo()!=null && queryAccountResponseInfo.getSebcMobileTwo()!=null) {
						crmUserProfiles.setEmail_2(queryAccountResponseInfo.getSebcEmailTwo());
						crmUserProfiles.setMobile_2(queryAccountResponseInfo.getSebcMobileTwo());
						crmUserProfiles.setSfdcId(newSfdcid);
						crmUserProfiles.setSfdcUpdatedDate2(new Date());
					}else if(queryAccountResponseInfo.getSebcEmailThree()!=null && queryAccountResponseInfo.getSebcMobileThree()!=null) {
						crmUserProfiles.setEmail_3(queryAccountResponseInfo.getSebcEmailThree());
						crmUserProfiles.setMobile_3(queryAccountResponseInfo.getSebcMobileThree());
						crmUserProfiles.setSfdcId(newSfdcid);
						crmUserProfiles.setSfdcUpdatedDate3(new Date());
					}
					
														
				
					dbGetCrmUser.update(crmUserProfiles);
					dbGetCrmUser.commit();
				} catch (DatabaseFacadeException e1) {
					logger.error("Error while reverting DB CrmUserProfiles Updation.", e1);
				} finally {
					dbGetCrmUser.close();
					userCrmUpdation=true;
				}
			}
			return userCrmUpdation;
		}
	
	public Boolean getCrmAllThreeProfileExists(String sfdcId) {
		
		logger.info("cal >>> getCrmAllThreeProfileExists(String sfdcId) ");
		Boolean crmProdileInd=true;
		DatabaseFacade dbGetCrmUser = new DatabaseFacade();
		MemberHelper userHelper = new MemberHelper(dbGetCrmUser);
		
		Boolean firstSet=false;
		Boolean SecondSet=false;
		Boolean ThirdSet=false;
		try {
			dbGetCrmUser.beginTransaction();
			CrmUserProfile crmUserProfiles = userHelper.getCrmUserProfileWithSfdcID(sfdcId);
			if(crmUserProfiles!=null) {
				if(crmUserProfiles.getEmail_1()!=null && crmUserProfiles.getMobile_1()!=null) {
					firstSet=true;
				}
				
				if(crmUserProfiles.getEmail_2()!=null && crmUserProfiles.getMobile_2()!=null) {
					SecondSet=true;
				}
				
				if(crmUserProfiles.getEmail_3()!=null && crmUserProfiles.getMobile_3()!=null) {
					ThirdSet=true;
				}
			}
			
			if(firstSet &&  SecondSet &&ThirdSet) {
				crmProdileInd=false;
			}
			
		} catch (DatabaseFacadeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		logger.info("crmProdileInd >> "+crmProdileInd);
		
		return crmProdileInd;
		
	}
	// added by pramod for setting the getNotExistsAllThreeCrmProfilesRequestBody request parameters
	public QueryAccountRequestResponseInfo getNotExistsAllThreeCrmProfilesRequestBody(Person person,QueryAccountRequestResponseInfo queryAccountResponsePairs){
		logger.info("call >> getNotExistsAllThreeCrmProfilesRequestBody pair setting");
		QueryAccountRequestResponseInfo queryAccountRequestResponseInfo=new QueryAccountRequestResponseInfo();
		Boolean firstSet=false;
		Boolean SecondSet=false;
		Boolean ThirdSet=false;
				
				if((queryAccountResponsePairs.getSebcEmailOne()==null && queryAccountResponsePairs.getSebcMobileOne()==null)||(queryAccountResponsePairs.getSebcEmailOne().equals(person.getEmail()) &&queryAccountResponsePairs.getSebcMobileOne().equals(person.getMobilePhone()))) {
					firstSet=true;						
				}else if((queryAccountResponsePairs.getSebcEmailTwo()==null && queryAccountResponsePairs.getSebcMobileTwo()==null)||(queryAccountResponsePairs.getSebcEmailTwo().equals(person.getEmail()) &&queryAccountResponsePairs.getSebcMobileTwo().equals(person.getMobilePhone()))) {
					SecondSet=true;					
				}else if((queryAccountResponsePairs.getSebcEmailThree()==null && queryAccountResponsePairs.getSebcMobileThree()==null)||(queryAccountResponsePairs.getSebcEmailThree().equals(person.getEmail()) &&queryAccountResponsePairs.getSebcMobileThree().equals(person.getMobilePhone()))) {
					ThirdSet=true;					
				}
				if(firstSet && !SecondSet &&! ThirdSet) {
					logger.info(" First pair is matched with login profile ");
					queryAccountRequestResponseInfo.setSebcEmailOne(person.getEmail());
					queryAccountRequestResponseInfo.setSebcMobileOne(person.getMobilePhone());
				}else if(!firstSet && SecondSet && !ThirdSet) {
					logger.info("Setting pair SET-2 for updare request due to Second  pair  matched with login profile or it's having empty with CRM profile ");
					queryAccountRequestResponseInfo.setSebcEmailTwo(person.getEmail());
					queryAccountRequestResponseInfo.setSebcMobileTwo(person.getMobilePhone());
					
				}else if(!firstSet && !SecondSet && ThirdSet) {
					logger.info(" Setting pair SET-3 for updare request due to Third  pair  matched with login profile or it's having empty with CRM profile");
					queryAccountRequestResponseInfo.setSebcEmailThree(person.getEmail());
					queryAccountRequestResponseInfo.setSebcMobileThree(person.getMobilePhone());	
				}
				queryAccountRequestResponseInfo.setReturnUI(true);
		return queryAccountRequestResponseInfo;
	}
	
	public QueryAccountRequestResponseInfo setPairToUpdateNewProfileInCrm(Person person,QueryAccountRequestResponseInfo queryAccountResponseInfo,String queryResSfdcId){
		logger.info("call >> setPairToUpdateNewProfileInCrm(Person person,QueryAccountRequestResponseInfo queryAccountResponseInfo,String oldSfdcId)");
		QueryAccountRequestResponseInfo queryAccountRequestInfo=new QueryAccountRequestResponseInfo();
		
		DatabaseFacade dbGetCrmUser = new DatabaseFacade();
		
		MemberHelper userHelper = new MemberHelper(dbGetCrmUser);
		
		try {
			dbGetCrmUser.beginTransaction();
			logger.info("call >> 	getCrmUserProfileWithSfdcID(String sfdcId) ");	
			CrmUserProfile crmUserProfiles = userHelper.getCrmUserProfileWithSfdcID(queryResSfdcId);
			// need to get clarification if record is not available in CRMProfile table
			
			LinkedHashMap<String, String> emailPhone=new LinkedHashMap<String, String>();
			
			if(queryAccountResponseInfo.getSebcEmailOne()!=null && queryAccountResponseInfo.getSebcMobileOne()!=null) {
				logger.info("putting first pair :"+queryAccountResponseInfo.getSebcEmailOne(),queryAccountResponseInfo.getSebcMobileOne());
				
				emailPhone.put(queryAccountResponseInfo.getSebcEmailOne(),queryAccountResponseInfo.getSebcMobileOne() );	
			}
			if(queryAccountResponseInfo.getSebcEmailTwo()!=null && queryAccountResponseInfo.getSebcMobileTwo()!=null) {
				emailPhone.put(queryAccountResponseInfo.getSebcEmailTwo(), queryAccountResponseInfo.getSebcMobileTwo());
				logger.info("putting second pair :"+queryAccountResponseInfo.getSebcEmailTwo(), queryAccountResponseInfo.getSebcMobileTwo());
			}
			if(queryAccountResponseInfo.getSebcEmailThree()!=null && queryAccountResponseInfo.getSebcMobileThree()!=null) {
				emailPhone.put(queryAccountResponseInfo.getSebcEmailThree(), queryAccountResponseInfo.getSebcMobileThree());
				logger.info("putting third pair :"+queryAccountResponseInfo.getSebcEmailThree(), queryAccountResponseInfo.getSebcMobileThree());
			}
							
			Boolean crmProfilePairMatchedIndOfQueryResponse=false;
			Boolean personPairMatchedIndOfQueryResponse=false;				
				if(person.getEmail().equals(queryAccountResponseInfo.getSebcEmailOne()) && person.getMobilePhone().equals(queryAccountResponseInfo.getSebcMobileOne()) ){
					queryAccountRequestInfo.setSebcEmailOne(person.getEmail());
					queryAccountRequestInfo.setSebcMobileOne(person.getMobilePhone());
					logger.info("Pair number one matched  Person Matched Email : " + person.getEmail() + ", PhoneNumber: " + person.getMobilePhone());
					personPairMatchedIndOfQueryResponse=true;
					}	
				else if(person.getEmail().equals(queryAccountResponseInfo.getSebcEmailTwo()) && person.getMobilePhone().equals(queryAccountResponseInfo.getSebcMobileTwo()) ){
					queryAccountRequestInfo.setSebcEmailTwo(person.getEmail());
					queryAccountRequestInfo.setSebcMobileTwo(person.getMobilePhone());
					personPairMatchedIndOfQueryResponse=true;
					logger.info("Pair  number two matched  Person Matched Email : " + person.getEmail() + ", PhoneNumber: " + person.getMobilePhone());
					}
				else if(person.getEmail().equals( queryAccountResponseInfo.getSebcEmailThree()) && person.getMobilePhone().equals(queryAccountResponseInfo.getSebcMobileThree()) ){
						queryAccountRequestInfo.setSebcEmailThree(person.getEmail());
						queryAccountRequestInfo.setSebcMobileThree(person.getMobilePhone());
						logger.info("Pair  number three matched  Person Matched Email : " + person.getEmail() + ", PhoneNumber: " + person.getMobilePhone());
						personPairMatchedIndOfQueryResponse=true;
					}
			
			
				
				
				if(!personPairMatchedIndOfQueryResponse) {
				logger.info("person Pair not Matched  with QueryResponse pair");
				
				
				// check login person matched with CRM profiletable
				
				Boolean loginProfileMatchWithCrmTable =this.getLoginProfileMatchWithCrmTable(crmUserProfiles,person);
				
				if(loginProfileMatchWithCrmTable) {
					logger.info("person Pair  Matched  with QueryResponse pair : " +loginProfileMatchWithCrmTable);
					Boolean firstSet=false;
					Boolean SecondSet=false;
					Boolean ThirdSet=false;
					
					int i=1;
					for (Map.Entry<String, String> entry : emailPhone.entrySet()) {
						if(entry.getKey()!=null && entry.getValue()!=null) {								
						String k = entry.getKey();
						String v = entry.getValue();
						
						// pair 1 mapping
						if(i==1) {
								if((crmUserProfiles.getEmail_1().equals(entry.getKey()) &&crmUserProfiles.getMobile_1().equals(entry.getValue()))) {
									logger.info("Pair number matched "+i+"CrmUserProfile Matched Email-1 : " + k + ", PhoneNumber: " + v);
									 firstSet=true;
								}
							}else if(i==2) {		
							// pair 3 mapping
							if((crmUserProfiles.getEmail_2().equals(entry.getKey()) &&crmUserProfiles.getMobile_2().equals(entry.getValue()))) {
								logger.info("Pair number matched "+i+"CrmUserProfile Matched Email-1 : " + k + ", PhoneNumber: " + v);
								SecondSet=true;						
							}
						}else if(i==3) {
							// pair 3 mapping
								if((crmUserProfiles.getEmail_3().equals(entry.getKey()) &&crmUserProfiles.getMobile_3().equals(entry.getValue()))) {
									logger.info("Pair number matched "+i+"CrmUserProfile Matched Email-1 : " + k + ", PhoneNumber: " + v);
									ThirdSet=true;
								}	
						}
								
					}
						i++;
				}
									
					if(firstSet && !SecondSet && !ThirdSet) {
						logger.info("Setting SET-1");
						queryAccountRequestInfo.setSebcEmailOne(person.getEmail());
						queryAccountRequestInfo.setSebcMobileOne(person.getMobilePhone());
						crmProfilePairMatchedIndOfQueryResponse=true;
					}else if(!firstSet && !SecondSet && ThirdSet) {
						logger.info("Setting SET-3");
						queryAccountRequestInfo.setSebcEmailThree(person.getEmail());
						queryAccountRequestInfo.setSebcMobileThree(person.getMobilePhone());
						crmProfilePairMatchedIndOfQueryResponse=true;
						
					}else if(!firstSet && SecondSet && !ThirdSet) {
						logger.info("Setting SET-2");
						queryAccountRequestInfo.setSebcEmailTwo(person.getEmail());
						queryAccountRequestInfo.setSebcMobileTwo(person.getMobilePhone());
						crmProfilePairMatchedIndOfQueryResponse=true;
						
					}									
				}
				}
				
				
			
			if(!personPairMatchedIndOfQueryResponse && ! crmProfilePairMatchedIndOfQueryResponse) {
				logger.info("person Pair , CRM Profiel table pairs not Matched  with QueryResponse pair setting pair -1");
			Integer finalPairNumber=	this.getOldestDateCrmProfile(crmUserProfiles);
			if(finalPairNumber==1) {
				logger.info("Final Pair Number to Update  CRM"+finalPairNumber);
				queryAccountRequestInfo.setSebcEmailOne(person.getEmail());
				queryAccountRequestInfo.setSebcMobileOne(person.getMobilePhone());
			}else if(finalPairNumber==2) {
				logger.info("Final Pair Number to Update  CRM"+finalPairNumber);
				queryAccountRequestInfo.setSebcEmailTwo(person.getEmail());
				queryAccountRequestInfo.setSebcMobileTwo(person.getMobilePhone());
			}else if(finalPairNumber==3) {
				logger.info("Final Pair Number to Update  CRM"+finalPairNumber);
				queryAccountRequestInfo.setSebcEmailThree(person.getEmail());
				queryAccountRequestInfo.setSebcMobileThree(person.getMobilePhone());
			}
			
				
			}
			
		} catch (DatabaseFacadeException e1) {
			logger.error("Error while reverting DB user creation.", e1);
		} finally {
			dbGetCrmUser.close();
			
		}
		
		return queryAccountRequestInfo;
	}
	public Integer getOldestDateCrmProfile(CrmUserProfile crmUserProfile) {	
		  logger.info("get oldest CRM profile number");
		int oldestProfileNumber = 0;
		Date pairone=crmUserProfile.getSfdcUpdatedDate1();
		Date pairTwo=crmUserProfile.getSfdcUpdatedDate2();
		Date pairThree=crmUserProfile.getSfdcUpdatedDate3();
		List<Date> listOfPairDates=new ArrayList<Date>();
		listOfPairDates.add(pairone); 
		listOfPairDates.add(pairTwo);
		listOfPairDates.add(pairThree); 		 
		  Collections.sort(listOfPairDates);
		  logger.info("Oldest Date desc order >> "+listOfPairDates);
		  Date oldestDate= new Date();
		  oldestDate=listOfPairDates.get(0); 
		  logger.info("Oldest Date >> "+oldestDate);
		  if (oldestDate.compareTo(pairone) == 0) {
			  oldestProfileNumber=1;
		  }else if(oldestDate.compareTo(pairTwo) == 0) {
			  oldestProfileNumber=2;
		  }else if(oldestDate.compareTo(pairThree) == 0) {
			  oldestProfileNumber=3;
		  }
		
		return oldestProfileNumber;
	}
	

	
	public Boolean getLoginProfileMatchWithCrmTable(CrmUserProfile crmProfile,Person person) {
		logger.info("person Pair  Matched  with QueryResponse pair  activated ");
		Boolean loginProfileMatchWithCrmTable =false;
		if(crmProfile.getEmail_1().equals(person.getEmail())&&crmProfile.getMobile_1().equals(person.getMobilePhone())) {
			loginProfileMatchWithCrmTable=true;
		}else if(crmProfile.getEmail_2().equals(person.getEmail())&&crmProfile.getMobile_2().equals(person.getMobilePhone())) {
			loginProfileMatchWithCrmTable=true;
		}else if(crmProfile.getEmail_3().equals(person.getEmail())&&crmProfile.getMobile_3().equals(person.getMobilePhone())) {
			loginProfileMatchWithCrmTable=true;
		}
	return loginProfileMatchWithCrmTable;
	}
	
	
	//added by pramod	
	public String set14CharNricNumber(String Nric) {
		String finalNric="";
		logger.info("NricPassport length >> " + Nric.length());
		if(Nric.length()==12) {
			String sixCharacters=StringUtils.substring(Nric, 0, 6);
			String middleTwoCharacters=StringUtils.substring(Nric, 6, 8);
			String lastFourCharacters=StringUtils.substring(Nric, 8, 12);
			finalNric=sixCharacters+"-"+middleTwoCharacters+"-"+lastFourCharacters;
			logger.info("Final NRIC Number >> "+finalNric);
			logger.info("Final NRIC Number length >> "+finalNric.length());
		}	
		return finalNric;
	}
	//added by pramod	
	public  String zeroInitialPhoneNumber(String phoneNumber) {
		String finaNumber=phoneNumber.replaceFirst("0", "60");
		
		return finaNumber;
		
	}
	//added by pramod	
	public  String oneInitialPhoneNumber(String phoneNumber) {
		String finaNumber="60"+phoneNumber;
		return finaNumber;
		
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		return "Registration";
	}
}
