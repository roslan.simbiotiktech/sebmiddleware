package com.seb.middleware.api.services.impl.account;

import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.SendEmailActivationCodeRequest;
import com.seb.middleware.api.services.response.account.SendEmailActivationCodeResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.VerificationHelper;
import com.seb.middleware.ldap.LDAPConnector;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "send_email_activation_code", publicService = true, auditable = true)
public class SendEmailActivationCodeService extends ServiceBase<SendEmailActivationCodeRequest, SendEmailActivationCodeResponse>{

	private static final Logger logger = LogManager.getLogger(SendEmailActivationCodeService.class);
	
	public SendEmailActivationCodeService(SendEmailActivationCodeRequest request) {
		super(request);
	}

	@Override
	public SendEmailActivationCodeResponse perform() throws ServiceException {
		SendEmailActivationCodeResponse response = new SendEmailActivationCodeResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			LDAPConnector ldap = new LDAPConnector();
			MemberHelper userHelper = new MemberHelper(db);
			User user = userHelper.getUser(request.getEmail());
			boolean existed = true;
			
			if(user == null){
				boolean existLdap = ldap.isUserExist(request.getEmail());
				if(!existLdap) {
					existed = false;	
				}else{
					logger.info("unable to registered user with email: " + request.getEmail() + ", already existed in LDAP.");
				}
			}else{
				logger.info("unable to registered user with email: " + request.getEmail() + ", already existed in Database.");
			}

			if(!existed){
				db.beginTransaction();
				
				VerificationHelper helper = new VerificationHelper(db);
				boolean generate = helper.generateEmailVerification(request, request.getEmail());
				db.commit();
				
				if(generate){
					response.getResponseStatus().setStatus(SUCCESS);
					return response;	
				}else{
					throw new ServiceException(ResourceUtil.get("error.email_activation_already_sent", 
									request.getLocale()), 
									APIResponse.ERROR_CODE_WARNING);
				}
			}else{
				throw new ServiceException(
						ResourceUtil.get("error.user_email_taken", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_EMAIL_EXISTED);
			}
			
		} catch (DatabaseFacadeException e) {
			logger.error("Error Sending Email Activation Code", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		} finally{
			db.close();
		}
	}

	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		return "Request to send email activation code";
	}
}
