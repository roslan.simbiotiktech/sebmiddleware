package com.seb.middleware.api.services.impl.account;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.SendMobileVerificationCodeRequest;
import com.seb.middleware.api.services.response.account.SendMobileVerificationCodeResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.helper.VerificationHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "send_mobile_verification_code", publicService = true)
public class SendMobileVerificationCodeService extends ServiceBase<SendMobileVerificationCodeRequest, SendMobileVerificationCodeResponse>{

	private static final Logger logger = LogManager.getLogger(SendMobileVerificationCodeService.class);
	
	public SendMobileVerificationCodeService(SendMobileVerificationCodeRequest request) {
		super(request);
	}

	@Override
	public SendMobileVerificationCodeResponse perform() throws ServiceException {
		SendMobileVerificationCodeResponse response = new SendMobileVerificationCodeResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			db.beginTransaction();
			
			VerificationHelper helper = new VerificationHelper(db);
			boolean generate = helper.generateMobileVerification(request, request.getMobileNumber());
			db.commit();
			
			if(generate){
				response.getResponseStatus().setStatus(SUCCESS);
				return response;	
			}else{
				throw new ServiceException(ResourceUtil.get("error.phone_verification_already_sent", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_WARNING);
			}
		} catch (DatabaseFacadeException e) {
			logger.error("Error Sending Mobile Verification Code", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		}finally{
			db.close();
		}
	}

}
