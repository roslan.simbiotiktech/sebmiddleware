package com.seb.middleware.api.services.impl.account;

import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.SendOtpRequest;
import com.seb.middleware.api.services.response.account.SendOtpResponse;
import com.seb.middleware.constant.PreferredCommunicationMethod;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.VerificationHelper;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "send_otp", publicService = true, auditable = true)
public class SendOtpService extends ServiceBase<SendOtpRequest, SendOtpResponse>{

	private static final Logger logger = LogManager.getLogger(SendOtpService.class);
	
	public SendOtpService(SendOtpRequest request) {
		super(request);
	}

	@Override
	public SendOtpResponse perform() throws ServiceException {
		SendOtpResponse response = new SendOtpResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			db.beginTransaction();
			MemberHelper memberHelper = new MemberHelper(db);
			User user = memberHelper.getUser(request.getEmail());
			Person person = memberHelper.getPerson(request.getEmail());
			
			/*PreferredCommunicationMethod method = user.getPreferredCommunicationMethod();
			if(request.getCommunicationMethod() != null && !request.getCommunicationMethod().isEmpty()){
				method = PreferredCommunicationMethod.valueOf(request.getCommunicationMethod());
			}
			
			if(method == null){
				throw new ServiceException(ResourceUtil.get("error.pref_comm_method_not_set_one_time_password_unable_send", 
						request.getLocale()), 
						APIResponse.ERROR_CODE_INVALID_INPUT);
			}else{
				
			}*/
			
			PreferredCommunicationMethod method = PreferredCommunicationMethod.MOBILE;
			
			VerificationHelper helper = new VerificationHelper(db);
			boolean generate = helper.generateOneTimePassword(request, user, person, method);
			db.commit();
			
			if(generate){
				response.setPreferredCommMethod(user.getPreferredCommunicationMethod().toString());
				response.getResponseStatus().setStatus(SUCCESS);
				return response;	
			}else{
				String methodName = null;
				if(method == PreferredCommunicationMethod.EMAIL){
					methodName = ResourceUtil.get("field.email", request.getLocale());
				}else{
					methodName = ResourceUtil.get("field.mobile_number", request.getLocale());
				}
					
				throw new ServiceException(ResourceUtil.get("error.one_time_password_already_sent", 
								request.getLocale(), methodName), 
								APIResponse.ERROR_CODE_WARNING);
			}
		} catch (DatabaseFacadeException e) {
			logger.error("Error Sending Mobile Verification Code", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		} finally{
			db.close();
		}
	}

	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		return "Request to send OTP";
	}
}
