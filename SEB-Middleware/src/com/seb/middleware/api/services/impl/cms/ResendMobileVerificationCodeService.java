package com.seb.middleware.api.services.impl.cms;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.impl.account.SendMobileVerificationCodeService;
import com.seb.middleware.api.services.request.account.SendMobileVerificationCodeRequest;
import com.seb.middleware.api.services.request.cms.res.VerificationAuthority;
import com.seb.middleware.api.services.response.cms.ResendMobileVerificationCodeResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.helper.VerificationHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "cms_resend_mobile_verification_code", publicService = true, auditable = false)
public class ResendMobileVerificationCodeService extends ServiceBase<SendMobileVerificationCodeRequest, ResendMobileVerificationCodeResponse>{

	private static final Logger logger = LogManager.getLogger(SendMobileVerificationCodeService.class);
	
	public ResendMobileVerificationCodeService(SendMobileVerificationCodeRequest request) {
		super(request);
	}

	@Override
	public ResendMobileVerificationCodeResponse perform() throws ServiceException {
		ResendMobileVerificationCodeResponse response = new ResendMobileVerificationCodeResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			db.beginTransaction();
			
			VerificationHelper helper = new VerificationHelper(db);
			VerificationAuthority va = new VerificationAuthority();
			va.setAuthorized(true);
			
			helper.generateMobileVerification(request, request.getMobileNumber(), va);
			db.commit();
			
			response.setOtp(va.getCode());
			response.getResponseStatus().setStatus(SUCCESS);
			return response;	
			
		} catch (DatabaseFacadeException e) {
			logger.error("Error Sending Mobile Verification Code", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		}finally{
			db.close();
		}
	}

}
