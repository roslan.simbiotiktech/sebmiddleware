package com.seb.middleware.api.services.impl.cms;

import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.cms.ResendOtpRequest;
import com.seb.middleware.api.services.request.cms.res.VerificationAuthority;
import com.seb.middleware.api.services.response.cms.ResendOtpResponse;
import com.seb.middleware.constant.OtpType;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.VerificationHelper;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.utility.ResourceUtil;
import com.seb.middleware.utility.StringUtil;

@Service(name = "cms_resend_otp", publicService = true, auditable = false)
public class ResendOtpService extends ServiceBase<ResendOtpRequest, ResendOtpResponse>{

	private static final Logger logger = LogManager.getLogger(ResendOtpService.class);
	
	public ResendOtpService(ResendOtpRequest request) {
		super(request);
	}

	@Override
	public ResendOtpResponse perform() throws ServiceException {
		ResendOtpResponse response = new ResendOtpResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			db.beginTransaction();
			MemberHelper memberHelper = new MemberHelper(db);
			User user = memberHelper.getUser(request.getLoginId());
			Person person = memberHelper.getPerson(user.getUserId());
			
			if(user.getPreferredCommunicationMethod() == null){
				throw new ServiceException(ResourceUtil.get("error.pref_comm_method_not_set_one_time_password_unable_send", 
						request.getLocale()), 
						APIResponse.ERROR_CODE_INVALID_INPUT);
			}else{
				VerificationHelper helper = new VerificationHelper(db);
				VerificationAuthority va = new VerificationAuthority();
				va.setAuthorized(true);
				
				OtpType type = OtpType.ALL;
				if(!StringUtil.isEmpty(request.getType())){
					type = OtpType.valueOf(request.getType());
				}
				
				helper.generateOneTimePassword(request, user, person, va, type, null);
				db.commit();
				
				response.setPreferredCommMethod(user.getPreferredCommunicationMethod().toString());
				response.setOtp(va.getCode());
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			}
		} catch (DatabaseFacadeException e) {
			logger.error("Error Sending Mobile Verification Code", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		} finally{
			db.close();
		}
	}
}
