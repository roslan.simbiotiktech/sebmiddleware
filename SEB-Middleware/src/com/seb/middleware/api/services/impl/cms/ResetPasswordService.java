package com.seb.middleware.api.services.impl.cms;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.cms.ResetPasswordRequest;
import com.seb.middleware.api.services.response.cms.ResetPasswordResponse;
import com.seb.middleware.constant.PreferredCommunicationMethod;
import com.seb.middleware.constant.UserStatus;
import com.seb.middleware.email.EmailManager;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.ldap.LDAPConnector;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.sms.SMSManager;
import com.seb.middleware.utility.PasswordUtil;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "cms_reset_customer_password", publicService = true, auditable = false)
public class ResetPasswordService extends ServiceBase<ResetPasswordRequest, ResetPasswordResponse> {

	private static final Logger logger = LogManager.getLogger(ResetPasswordService.class);
	
	public ResetPasswordService(ResetPasswordRequest request) {
		super(request);
	}

	@Override
	public ResetPasswordResponse perform() throws ServiceException {
		
		ResetPasswordResponse response = new ResetPasswordResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		LDAPConnector ldap = new LDAPConnector();
		try {
			MemberHelper memberHelper = new MemberHelper(db);
			SettingHelper settingHelper = new SettingHelper(db);
			
			boolean testingEnvironment = settingHelper.isTestingEnvironment();
			
			db.beginTransaction();
			
			User user = memberHelper.getUser(request.getLoginId());
			user.setPasswordExpired(1);
			user.setExistingPassword(null);
			db.update(user);
			
			String password = PasswordUtil.generatePassword();

			if(testingEnvironment){
				logger.debug("New Password: " + password);	
			}
			
			if(user.getStatus() == UserStatus.PENDING){
				// Existing user
				try {
					EmailManager email = new EmailManager();
					email.sendForgotPassword(user.getExistingEmail(), password);
				} catch (IOException | MessagingException e) {
					logger.error("Error sending email password", e);
					throw new ServiceException(e, 
							ResourceUtil.get("error.general_500", 
									request.getLocale()), 
									APIResponse.ERROR_CODE_SERVER_EXCEPTION);
				}
			}else if(user.getPreferredCommunicationMethod() == PreferredCommunicationMethod.EMAIL){
				try {
					EmailManager email = new EmailManager();
					email.sendForgotPassword(user.getUserId(), password);
				} catch (IOException | MessagingException e) {
					logger.error("Error sending email password", e);
					throw new ServiceException(e, 
							ResourceUtil.get("error.general_500", 
									request.getLocale()), 
									APIResponse.ERROR_CODE_SERVER_EXCEPTION);
				}
			}else if(user.getPreferredCommunicationMethod() == PreferredCommunicationMethod.MOBILE){
				try {
					Person person = memberHelper.getPerson(request.getLoginId());
					SMSManager email = new SMSManager();
					email.sendForgotPassword(person.getMobilePhone(), password);
				} catch (IOException e) {
					logger.error("Error sending SMS password", e);
					throw new ServiceException(e, 
							ResourceUtil.get("error.general_500", 
									request.getLocale()), 
									APIResponse.ERROR_CODE_SERVER_EXCEPTION);
				}
			}

			response.setNewPassword(password);
			ldap.resetPassword(request.getLoginId(), password);
			db.commit();
			
			logger.debug("Password reset successfully for : " + request.getLoginId());
			response.getResponseStatus().setStatus(SUCCESS);
			
			return response;
			
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		} catch (DatabaseFacadeException e) {
			logger.error("Error Forgot password", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} finally {
			db.close();
		}
	}
}
