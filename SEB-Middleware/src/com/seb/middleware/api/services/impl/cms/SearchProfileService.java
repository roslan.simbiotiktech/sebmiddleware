package com.seb.middleware.api.services.impl.cms;

import java.util.ArrayList;
import java.util.Collections;

import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.Paging;
import com.seb.middleware.api.services.request.cms.SearchProfileRequest;
import com.seb.middleware.api.services.response.Pagination;
import com.seb.middleware.api.services.response.cms.Contract;
import com.seb.middleware.api.services.response.cms.SearchProfileResponse;
import com.seb.middleware.constant.UserStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.Subscription;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.ldap.LDAPConnector;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.utility.StringUtil;

@Service(name = "cms_search_profile", publicService = true, auditable = false)
public class SearchProfileService extends ServiceBase<SearchProfileRequest, SearchProfileResponse> {

	private static final Logger logger = LogManager.getLogger(SearchProfileService.class);
	
	public SearchProfileService(SearchProfileRequest request) {
		super(request);
	}

	@Override
	public SearchProfileResponse perform() throws ServiceException {
		
		SearchProfileResponse response = new SearchProfileResponse();
		
		boolean loginIdSearch = !StringUtil.isEmpty(request.getLoginId());
		boolean emailSearch = !StringUtil.isEmpty(request.getEmail());
		boolean nameSearch = !StringUtil.isEmpty(request.getName());
		boolean mobileNumberSearch = !StringUtil.isEmpty(request.getMobileNumber());
		boolean contractAccountNumberSearch = !StringUtil.isEmpty(request.getContractAccountNo());
		
		boolean allSearch = !loginIdSearch && !emailSearch && !nameSearch && !mobileNumberSearch && !contractAccountNumberSearch;
		
		DatabaseFacade db = new DatabaseFacade();
		try{
			
			ArrayList<String> crossMatchedIds = new ArrayList<String>();
			
			ArrayList<String> loginIdSearchMatch = null;
			ArrayList<String> emailSearchMatch = null;
			ArrayList<String> nameAndPhoneNumberSearchMatch = null;
			ArrayList<String> contractAccountNumberSearchMatch = null;
			
			if(loginIdSearch){
				loginIdSearchMatch = new ArrayList<String>();
				loginIdSearchMatch.add(request.getLoginId());
			}
			
			if(emailSearch){
				MemberHelper memberHelper = new MemberHelper(db);
				emailSearchMatch = memberHelper.searchUserWithEmail(request.getEmail());
			}
			
			if(nameSearch || mobileNumberSearch){
				LDAPConnector ldap = new LDAPConnector();
				ArrayList<Person> persons = ldap.getUsers(request.getName(), request.getMobileNumber());
				if(persons != null){
					nameAndPhoneNumberSearchMatch = new ArrayList<String>();
					for(Person person : persons){
						nameAndPhoneNumberSearchMatch.add(person.getEmail());
					}
				}
			}
			
			if(contractAccountNumberSearch){
				SubscriptionHelper subscriptionHelper = new SubscriptionHelper(db);
				contractAccountNumberSearchMatch = subscriptionHelper.getSubscribersIds(request.getContractAccountNo());
			}
			
			boolean searchBegan = false;
			if(loginIdSearch){
				if(searchBegan){
					crossMatch(crossMatchedIds, loginIdSearchMatch);
				}else{
					addAll(crossMatchedIds, loginIdSearchMatch);
				}
				
				searchBegan = true;
			}
			
			if(emailSearch){
				if(searchBegan){
					crossMatch(crossMatchedIds, emailSearchMatch);
				}else{
					addAll(crossMatchedIds, emailSearchMatch);
				}
				
				searchBegan = true;
			}
			
			if(nameSearch || mobileNumberSearch){
				if(searchBegan){
					crossMatch(crossMatchedIds, nameAndPhoneNumberSearchMatch);
				}else{
					addAll(crossMatchedIds, nameAndPhoneNumberSearchMatch);
				}
				
				searchBegan = true;
			}
			
			if(contractAccountNumberSearch){
				if(searchBegan){
					crossMatch(crossMatchedIds, contractAccountNumberSearchMatch);
				}else{
					addAll(crossMatchedIds, contractAccountNumberSearchMatch);
				}
				
				searchBegan = true;
			}
			
			Paging paging = request.getPaging();
			if(paging == null) paging = new Paging();
			
			if(allSearch){
				// Searching all members
				MemberHelper memberHelper = new MemberHelper(db);
				SubscriptionHelper subscriptionHelper = new SubscriptionHelper(db);
				
				ArrayList<User> allUsers = memberHelper.getUser((paging.getPage() -1) * paging.getRecordsPerPage(), paging.getRecordsPerPage());
				
				com.seb.middleware.api.services.response.cms.User [] users = 
						new com.seb.middleware.api.services.response.cms.User[allUsers.size()];
				
				for(int i = 0; i < allUsers.size(); i++){
					User user = allUsers.get(i);
					Person person = memberHelper.getPerson(user.getUserId());

					users[i] = fillUser(user, person, subscriptionHelper);
				}
				
				int totalMembers = (int)memberHelper.getUserCount();
				
				Pagination pagination = new Pagination();
			    pagination.setPaging(paging);
			    pagination.setTotalRecords(totalMembers);
			    pagination.setMaxPage(pagination.calculateMaxPage(totalMembers, paging.getRecordsPerPage()));
			    
				response.setPagination(pagination);
				response.setUsers(users);
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			}else if(crossMatchedIds.isEmpty()){
				
				Pagination pagination = new Pagination();
			    pagination.setPaging(paging);
			    pagination.setTotalRecords(0);
			    pagination.setMaxPage(0);
			    
				response.setPagination(pagination);
				
				logger.debug("nothing matched against the searching criteria");
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			}else{
				Collections.sort(crossMatchedIds);
				
				ArrayList<String> pagedCrossMatchedIds = new ArrayList<String>();
				int fromIndex = (paging.getPage() - 1) * paging.getRecordsPerPage();
				int toIndex = fromIndex + paging.getRecordsPerPage();
				
				for(int i = fromIndex; i < toIndex; i++){
					if(crossMatchedIds.size() > i){
						pagedCrossMatchedIds.add(crossMatchedIds.get(i));
					}else{
						break;
					}
				}
				
				MemberHelper memberHelper = new MemberHelper(db);
				SubscriptionHelper subscriptionHelper = new SubscriptionHelper(db);
				
				com.seb.middleware.api.services.response.cms.User [] users = 
						new com.seb.middleware.api.services.response.cms.User[pagedCrossMatchedIds.size()];
				
				for(int i = 0; i < pagedCrossMatchedIds.size(); i++){
					String id = pagedCrossMatchedIds.get(i);
					
					User user = memberHelper.getUser(id);
					Person person = memberHelper.getPerson(id);

					users[i] = fillUser(user, person, subscriptionHelper);
				}
				
				Pagination pagination = new Pagination();
			    pagination.setPaging(paging);
			    pagination.setTotalRecords(crossMatchedIds.size());
			    pagination.setMaxPage(pagination.calculateMaxPage(crossMatchedIds.size(), paging.getRecordsPerPage()));
			    
				response.setPagination(pagination);
				response.setUsers(users);
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			}

		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		}finally{
			db.close();
		}
	}
	
	private com.seb.middleware.api.services.response.cms.User fillUser(User user, Person person, SubscriptionHelper subscriptionHelper){
		com.seb.middleware.api.services.response.cms.User usr = new com.seb.middleware.api.services.response.cms.User();
		usr.setLoginId(user.getUserId());
		usr.setEmail(user.getStatus() == UserStatus.PENDING ? user.getExistingEmail() : user.getUserId());
		usr.setName(person.getName());
		usr.setLastLoginAt(user.getCurrentLoginAt());
		usr.setMobileNumber(person.getMobilePhone());
		usr.setStatus(user.getStatus().toString());
		
		ArrayList<Subscription> subs = subscriptionHelper.getSubscriptions(user.getUserId(), null);
		if(subs != null && !subs.isEmpty()){
			Contract [] contracts = new Contract[subs.size()];
			for(int k = 0; k < subs.size(); k++){
				Subscription sub = subs.get(k);
				
				Contract contract = new Contract();
				contract.setAccountNumber(sub.getContractAccNo());
				contract.setAccountName(sub.getContract().getContractAccountName());
				contract.setStatus(sub.getStatus().toString());
				
				contracts[k] = contract;
			}
			
			usr.setContractSubscribed(contracts);
		}
		
		return usr;
	}
	
	private void addAll(ArrayList<String> source, ArrayList<String> additional){
		if(additional != null && !additional.isEmpty()){
			source.addAll(additional);
		}
	}
	
	private void crossMatch(ArrayList<String> source, ArrayList<String> additional){
		if(isEmpty(source) || isEmpty(additional)){
			source.clear();
		}else{
			ArrayList<String> removal = new ArrayList<String>();
			for(String so : source){
				if(!additional.contains(so)){
					removal.add(so);
				}
			}
			if(!removal.isEmpty()){
				for(String remove : removal){
					source.remove(remove);	
				}
			}
		}
	}
	
	private boolean isEmpty(ArrayList<String> list){
		return list == null || list.isEmpty();
	}
}
