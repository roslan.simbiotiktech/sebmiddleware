package com.seb.middleware.api.services.impl.cms;

import java.util.ArrayList;
import java.util.HashMap;

import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.Paging;
import com.seb.middleware.api.services.request.cms.ViewCustomerHistoryRequest;
import com.seb.middleware.api.services.response.Pagination;
import com.seb.middleware.api.services.response.cms.AuditTrail;
import com.seb.middleware.api.services.response.cms.Contract;
import com.seb.middleware.api.services.response.cms.ViewCustomerHistoryResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.CustomerAuditTrail;
import com.seb.middleware.jpa.dao.Subscription;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.ldap.LDAPConnector;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.utility.StringUtil;

@Service(name = "cms_view_customer_history", publicService = true, auditable = false)
public class ViewCustomerHistoryService extends ServiceBase<ViewCustomerHistoryRequest, ViewCustomerHistoryResponse> {

	private static final Logger logger = LogManager.getLogger(ViewCustomerHistoryService.class);
	
	public ViewCustomerHistoryService(ViewCustomerHistoryRequest request) {
		super(request);
	}

	@Override
	public ViewCustomerHistoryResponse perform() throws ServiceException {
		
		ViewCustomerHistoryResponse response = new ViewCustomerHistoryResponse();
		
		boolean loginIdSearch = !StringUtil.isEmpty(request.getLoginId());
		boolean mobileNumberSearch = !StringUtil.isEmpty(request.getMobileNumber());
		boolean contractAccountNumberSearch = !StringUtil.isEmpty(request.getContractAccountNo());
		
		DatabaseFacade db = new DatabaseFacade();
		try{
			
			ArrayList<String> crossMatchedIds = new ArrayList<String>();
			
			ArrayList<String> loginIdSearchMatch = null;
			ArrayList<String> nameAndPhoneNumberSearchMatch = null;
			ArrayList<String> contractAccountNumberSearchMatch = null;
			
			
			if(loginIdSearch){
				loginIdSearchMatch = new ArrayList<String>();
				loginIdSearchMatch.add(request.getLoginId());
			}

			if(mobileNumberSearch){
				LDAPConnector ldap = new LDAPConnector();
				ArrayList<Person> persons = ldap.getUsers(null, request.getMobileNumber());
				if(persons != null){
					nameAndPhoneNumberSearchMatch = new ArrayList<String>();
					for(Person person : persons){
						nameAndPhoneNumberSearchMatch.add(person.getEmail());
					}
				}
			}
			
			if(contractAccountNumberSearch){
				SubscriptionHelper subscriptionHelper = new SubscriptionHelper(db);
				contractAccountNumberSearchMatch = subscriptionHelper.getSubscribersIds(request.getContractAccountNo());
			}
			
			boolean searchBegan = false;
			if(loginIdSearch){
				if(searchBegan){
					crossMatch(crossMatchedIds, loginIdSearchMatch);
				}else{
					addAll(crossMatchedIds, loginIdSearchMatch);
				}
				
				searchBegan = true;
			}

			if(mobileNumberSearch){
				if(searchBegan){
					crossMatch(crossMatchedIds, nameAndPhoneNumberSearchMatch);
				}else{
					addAll(crossMatchedIds, nameAndPhoneNumberSearchMatch);
				}
				
				searchBegan = true;
			}
			
			if(contractAccountNumberSearch){
				if(searchBegan){
					crossMatch(crossMatchedIds, contractAccountNumberSearchMatch);
				}else{
					addAll(crossMatchedIds, contractAccountNumberSearchMatch);
				}
				
				searchBegan = true;
			}
			
			if(searchBegan && crossMatchedIds.isEmpty()){
				logger.debug("nothing matched against the searching criteria");
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			}else{
				Paging paging = request.getPaging();
				if(paging == null) paging = new Paging();
				
				Criteria criteria = db.getSession().createCriteria(CustomerAuditTrail.class);
				criteria.add(Restrictions.between("auditDatetime", request.getDateFrom(), request.getDateTo()));
				
				if(searchBegan){
					// This is search with ID
					criteria.add(Restrictions.in("userId", crossMatchedIds));
				}
				
			    criteria.setProjection(Projections.rowCount());	
			    
			    Long resultCount = (Long)criteria.uniqueResult();
			    logger.debug("Log count for audit trail searching: " + resultCount);
			    
			    Pagination pagination = new Pagination();
			    pagination.setPaging(paging);
			    
			    int totalRecords = resultCount == null ? 0 : resultCount.intValue();
			    int totalPages = pagination.calculateMaxPage(totalRecords, paging.getRecordsPerPage());
			    
			    if(totalRecords == 0){
			    	totalPages = 0;
			    }
			    
			    pagination.setMaxPage(totalPages);
			    pagination.setTotalRecords(totalRecords);
				response.setPagination(pagination);
				
				if(totalRecords > 0){
					criteria.setMaxResults(paging.getRecordsPerPage());
					criteria.setFirstResult((paging.getPage() -1) * paging.getRecordsPerPage());
					criteria.addOrder(Order.desc("auditDatetime"));
					criteria.setProjection(null);
					
					ArrayList<CustomerAuditTrail> audits = db.query(criteria);
					if(audits != null && !audits.isEmpty()){
						AuditTrail [] auditTrails = new AuditTrail[audits.size()];
						MemberHelper memberHelper = new MemberHelper(db);
						SubscriptionHelper subscriptionHelper = new SubscriptionHelper(db);
						
						for(int i = 0; i < audits.size(); i++){
							CustomerAuditTrail audit = audits.get(i);
							
							AuditTrail auditTrail = new AuditTrail();
							auditTrail.setLoginId(audit.getUserId());
							auditTrail.setActivity(audit.getActivity());
							auditTrail.setActivityAt(audit.getAuditDatetime());
							auditTrail.setContractSubscribed(getContractSubscribed(subscriptionHelper, audit.getUserId()));
							
							Person person = getCachePerson(memberHelper, audit.getUserId());
							if(person != null){
								auditTrail.setMobileNumber(person.getMobilePhone());
							}
							
							auditTrails[i] = auditTrail;
						}
						
						response.setAuditTrails(auditTrails);
					}
				}
				
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			}
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		}finally{
			db.close();
		}
	}
	
	private HashMap<String, Contract[]> contractStore = new HashMap<String, Contract[]>();
	private Contract[] getContractSubscribed(SubscriptionHelper helper, String loginId){
		if(contractStore.containsKey(loginId)){
			return contractStore.get(loginId);
		}else{
			ArrayList<Subscription> subscriptions = helper.getSubscriptions(loginId, null);
			Contract[] contracts = null;
			if(subscriptions != null && !subscriptions.isEmpty()){
				contracts = new Contract[subscriptions.size()];
				for(int i = 0; i < subscriptions.size(); i++){
					Subscription sub = subscriptions.get(i);
					Contract con = new Contract();
					con.setAccountNumber(sub.getContractAccNo());
					con.setAccountName(sub.getContract().getContractAccountName());
					con.setStatus(sub.getStatus().toString());
					
					contracts[i] = con;
				}
			}
			
			contractStore.put(loginId, contracts);
			
			return contracts;
		}
	}
	
	private HashMap<String, Person> personStore = new HashMap<String, Person>();
	
	private Person getCachePerson(MemberHelper helper, String loginId) throws NamingException{
		if(personStore.containsKey(loginId)){
			return personStore.get(loginId);
		}else{
			Person person = helper.getPerson(loginId);
			personStore.put(loginId, person);
			return person;
		}
	}
	
	private void addAll(ArrayList<String> source, ArrayList<String> additional){
		if(additional != null && !additional.isEmpty()){
			source.addAll(additional);
		}
	}
	
	private void crossMatch(ArrayList<String> source, ArrayList<String> additional){
		if(isEmpty(source) || isEmpty(additional)){
			source.clear();
		}else{
			ArrayList<String> removal = new ArrayList<String>();
			for(String so : source){
				if(!additional.contains(so)){
					removal.add(so);
				}
			}
			if(!removal.isEmpty()){
				for(String remove : removal){
					source.remove(remove);	
				}
			}
		}
	}
	
	private boolean isEmpty(ArrayList<String> list){
		return list == null || list.isEmpty();
	}
}
