package com.seb.middleware.api.services.impl.cms;

import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.cms.ViewProfileDetailsRequest;
import com.seb.middleware.api.services.response.cms.ContractDetail;
import com.seb.middleware.api.services.response.cms.ViewProfileDetailsResponse;
import com.seb.middleware.constant.UserStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.Subscription;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.utility.StringUtil;

@Service(name = "cms_view_profile_details", publicService = true, auditable = false)
public class ViewProfileDetailService extends ServiceBase<ViewProfileDetailsRequest, ViewProfileDetailsResponse> {

	private static final Logger logger = LogManager.getLogger(ViewProfileDetailService.class);
	
	public ViewProfileDetailService(ViewProfileDetailsRequest request) {
		super(request);
	}

	@Override
	public ViewProfileDetailsResponse perform() throws ServiceException {
		
		ViewProfileDetailsResponse response = new ViewProfileDetailsResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		try{
			MemberHelper memberHelper = new MemberHelper(db);
			
			User user = memberHelper.getUser(request.getLoginId());
			Person person = memberHelper.getPerson(request.getLoginId());
			
			response.setLoginId(user.getUserId());
			response.setName(person.getName());
			response.setNricOrPassport(person.getNricPassport());
			response.setEmail(user.getStatus() == UserStatus.PENDING ? user.getExistingEmail() : user.getUserId());
			response.setMobileNumber(person.getMobilePhone());
			response.setOfficeTel(person.getOfficePhone());
			response.setHomeTel(person.getHomePhone());
			if(user.getPreferredCommunicationMethod() != null)
				response.setPreferredCommMethod(user.getPreferredCommunicationMethod().toString());
			response.setAccountStatus(user.getStatus().toString());
			response.setLastLogin(user.getCurrentLoginAt());
			response.setRemark(user.getRemark());
			
			if(!StringUtil.isEmpty(request.getContractAccountNo())){
				SubscriptionHelper subscriptionHelper = new SubscriptionHelper(db);
				Subscription subscription = subscriptionHelper.getSubscription(request.getContractAccountNo(), request.getLoginId(), null);
				if(subscription != null){
					ContractDetail detail = new ContractDetail();
					detail.setAccountNumber(subscription.getContractAccNo());
					detail.setAccountName(subscription.getContract().getContractAccountName());
					detail.setAccountNick(subscription.getContractAccNick());
					detail.setSubsriptionType(subscription.getSubsType().toString());
					detail.setStatus(subscription.getStatus().toString());
					detail.setSubsriptionAt(subscription.getCreatedDatetime());
					detail.setRemark(subscription.getRemark());
					response.setContractDetail(detail);
				}else{
					logger.warn("user " + request.getLoginId() + " getting an invalid contract account " + request.getContractAccountNo());
				}
			}
			
			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		}finally{
			db.close();
		}
	}
}
