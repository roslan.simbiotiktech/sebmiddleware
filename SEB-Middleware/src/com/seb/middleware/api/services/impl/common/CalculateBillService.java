package com.seb.middleware.api.services.impl.common;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.common.CalculateBillRequest;
import com.seb.middleware.api.services.response.common.CalculateBillResponse;
import com.seb.middleware.api.services.response.common.KwhUsage;
import com.seb.middleware.constant.CalculatorType;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.TariffSettingDAO;
import com.seb.middleware.jpa.helper.ContentHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "calculate_bill", publicService = true)
public class CalculateBillService extends ServiceBase<CalculateBillRequest, CalculateBillResponse> {

	private static final Logger logger = LogManager.getLogger(CalculateBillService.class);
	
	private static BigDecimal gstRate = BigDecimal.valueOf(6);
	private static BigDecimal gstRateOthers = BigDecimal.valueOf(0);
	private static BigDecimal hundred = BigDecimal.valueOf(100);
	private static BigDecimal zero = BigDecimal.valueOf(0);
	
	public CalculateBillService(CalculateBillRequest request) {
		super(request);
	}

	@Override
	public CalculateBillResponse perform() throws ServiceException {
		CalculateBillResponse response = new CalculateBillResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			CalculatorType type = CalculatorType.valueOf(request.getCalculatorType());
			ContentHelper cHelper = new ContentHelper(db);
			List<TariffSettingDAO> settings = cHelper.getTarrifSetting(type);
			
			if(settings == null || settings.isEmpty()){
				throw new ServiceException(
						ResourceUtil.get("error.tarrif_setting_not_defined", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			}else{
				
				response.setKwhConsumption(request.getKwhConsumption());
				response.setKvarhConsumption(request.getKvarhConsumption());
				response.setOffpeakKwhConsumption(request.getOffpeakKwhConsumption());
				response.setOffpeakKvarhConsumption(request.getOffpeakKvarhConsumption());
				response.setKwhTotalComsumption(request.getKwhConsumption() + request.getOffpeakKwhConsumption());
				response.setKvarhTotalComsumption(request.getKvarhConsumption() + request.getOffpeakKvarhConsumption());
				
				if(type == CalculatorType.DOMESTIC){
					response.setGstRate(gstRate.doubleValue());
					calculateDomestic(response, settings, gstRate);
				}else if(type == CalculatorType.COMMERCIAL_C1){
					response.setGstRate(gstRateOthers.doubleValue());
					calculateLevel1(response, settings);
				}else if(type == CalculatorType.COMMERCIAL_C2){
					response.setGstRate(gstRateOthers.doubleValue());
					calculateLevel2(response, settings);
				}else if(type == CalculatorType.COMMERCIAL_C3){
					response.setGstRate(gstRateOthers.doubleValue());
					calculateLevel3(response, settings);
				}else if(type == CalculatorType.INDUSTRIAL_I1){
					response.setGstRate(gstRateOthers.doubleValue());
					calculateLevel1(response, settings);
				}else if(type == CalculatorType.INDUSTRIAL_I2){
					response.setGstRate(gstRateOthers.doubleValue());
					calculateLevel2(response, settings);
				}else if(type == CalculatorType.INDUSTRIAL_I3){
					response.setGstRate(gstRateOthers.doubleValue());
					calculateLevel3(response, settings);
				}else if(type == CalculatorType.PUBLIC_LIGHTING){
					response.setGstRate(gstRateOthers.doubleValue());
					calculateDomestic(response, settings, gstRateOthers);
				}
			}
		}finally{
			db.close();
		}
		
		response.getResponseStatus().setStatus(SUCCESS);
		return response;
	}

	private void calculateDomestic(CalculateBillResponse response, List<TariffSettingDAO> settings, 
			BigDecimal gstRate) throws ServiceException{
		BigDecimal usage = BigDecimal.valueOf(request.getKwhConsumption());
		
		TariffSettingDAO setting = getDesignatedSetting(settings, usage);
		if(setting == null){
			throw new ServiceException(
					ResourceUtil.get("error.tarrif_setting_not_defined", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		}else{
			
			BigDecimal kwhConsumption = BigDecimal.valueOf(request.getKwhConsumption());
			BigDecimal excemptedKwhConsumption = null;
			
			if(setting.getExemptedGstFirstKwh() == null){
				/**
				 * No exemption
				 */
				excemptedKwhConsumption = BigDecimal.valueOf(0);
				
			}else{
				
				if(kwhConsumption.compareTo(setting.getExemptedGstFirstKwh()) <= 0){
					// Full Exemption
					excemptedKwhConsumption = kwhConsumption;
					kwhConsumption = BigDecimal.valueOf(0);
				}else{
					excemptedKwhConsumption = setting.getExemptedGstFirstKwh();
					kwhConsumption = kwhConsumption.subtract(excemptedKwhConsumption);
				}
			}
			
			BigDecimal tariffRate = getTarrifRate(setting, usage);
			BigDecimal amount = kwhConsumption.multiply(tariffRate).setScale(2, RoundingMode.HALF_UP);
			BigDecimal gstExemptedAmount = excemptedKwhConsumption.multiply(tariffRate).setScale(2, RoundingMode.HALF_UP);
			BigDecimal subTotal = amount.add(gstExemptedAmount);
			BigDecimal gst = amount.multiply(gstRate.divide(hundred)).setScale(2, RoundingMode.HALF_UP);
			BigDecimal total = subTotal.add(gst);
			
			response.setTariffRate(tariffRate.doubleValue());
			
			KwhUsage gstUsage = new KwhUsage();
			gstUsage.setUnit(kwhConsumption.doubleValue());
			gstUsage.setAmount(amount.doubleValue());
			
			KwhUsage gstExemptedUsage = new KwhUsage();
			gstExemptedUsage.setUnit(excemptedKwhConsumption.doubleValue());
			gstExemptedUsage.setAmount(gstExemptedAmount.doubleValue());
			
			response.setUsageCharge(gstUsage);
			response.setTaxExemptedUsageCharge(gstExemptedUsage);
			response.setSubTotal(subTotal.doubleValue());
			response.setGstAmount(gst.doubleValue());
			response.setBillAmount(total.doubleValue());
			response.setMinMonthlytCharge(setting.getMinMonthlyChargeAmount().doubleValue());
			
		}
	}
	
	private void calculateLevel1(CalculateBillResponse response, List<TariffSettingDAO> settings) throws ServiceException{
		BigDecimal usage = BigDecimal.valueOf(request.getKwhConsumption());
		
		TariffSettingDAO setting = getDesignatedSetting(settings, usage);
		if(setting == null){
			throw new ServiceException(
					ResourceUtil.get("error.tarrif_setting_not_defined", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		}else{
			
			BigDecimal kwhConsumption = BigDecimal.valueOf(request.getKwhConsumption());
			BigDecimal excemptedKwhConsumption = null;
			
			if(setting.getExemptedGstFirstKwh() == null){
				/**
				 * No exemption
				 */
				excemptedKwhConsumption = BigDecimal.valueOf(0);
				
			}else{
				
				if(kwhConsumption.compareTo(setting.getExemptedGstFirstKwh()) <= 0){
					// Full Exemption
					excemptedKwhConsumption = kwhConsumption;
					kwhConsumption = BigDecimal.valueOf(0);
				}else{
					excemptedKwhConsumption = setting.getExemptedGstFirstKwh();
					kwhConsumption = kwhConsumption.subtract(excemptedKwhConsumption);
				}
			}
			
			BigDecimal tariffRate = getTarrifRate(setting, kwhConsumption);
			BigDecimal amount = kwhConsumption.multiply(tariffRate).setScale(2, RoundingMode.HALF_UP);
			BigDecimal gstExemptedAmount = excemptedKwhConsumption.multiply(tariffRate).setScale(2, RoundingMode.HALF_UP);
			BigDecimal lowPowerFactorRate = getLowPowerFactorRate(BigDecimal.valueOf(request.getKwhConsumption()), 
					BigDecimal.valueOf(request.getKvarhConsumption()));
			BigDecimal subTotal = amount.add(gstExemptedAmount);
			BigDecimal gst = amount.multiply(gstRateOthers.divide(hundred)).setScale(2, RoundingMode.HALF_UP);
			BigDecimal lowPowerFactorCharge = getLowerPowerFactorCharge(lowPowerFactorRate, subTotal).setScale(2, RoundingMode.HALF_UP);
			BigDecimal total = subTotal.add(lowPowerFactorCharge).add(gst);
			
			response.setTariffRate(tariffRate.doubleValue());
			
			KwhUsage gstUsage = new KwhUsage();
			gstUsage.setUnit(kwhConsumption.doubleValue());
			gstUsage.setAmount(amount.doubleValue());
			
			KwhUsage gstExemptedUsage = new KwhUsage();
			gstExemptedUsage.setUnit(excemptedKwhConsumption.doubleValue());
			gstExemptedUsage.setAmount(gstExemptedAmount.doubleValue());
			
			response.setUsageCharge(gstUsage);
			response.setTaxExemptedUsageCharge(gstExemptedUsage);
			response.setLowPowerFactorRate(lowPowerFactorRate.doubleValue());
			response.setSubTotal(subTotal.doubleValue());
			response.setGstAmount(gst.doubleValue());
			response.setLowPowerFactorCharge(lowPowerFactorCharge.doubleValue());
			response.setBillAmount(total.doubleValue());
			response.setMinMonthlytCharge(setting.getMinMonthlyChargeAmount().doubleValue());
		}
	}
	
	private void calculateLevel2(CalculateBillResponse response, List<TariffSettingDAO> settings) throws ServiceException{
		BigDecimal usage = BigDecimal.valueOf(request.getKwhConsumption());
		
		TariffSettingDAO setting = getDesignatedSetting(settings, usage);
		if(setting == null){
			throw new ServiceException(
					ResourceUtil.get("error.tarrif_setting_not_defined", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		}else{
			
			BigDecimal kwhConsumption = BigDecimal.valueOf(request.getKwhConsumption());
			BigDecimal excemptedKwhConsumption = null;
			
			if(setting.getExemptedGstFirstKwh() == null){
				/**
				 * No exemption
				 */
				excemptedKwhConsumption = BigDecimal.valueOf(0);
				
			}else{
				
				if(kwhConsumption.compareTo(setting.getExemptedGstFirstKwh()) <= 0){
					// Full Exemption
					excemptedKwhConsumption = kwhConsumption;
					kwhConsumption = BigDecimal.valueOf(0);
				}else{
					excemptedKwhConsumption = setting.getExemptedGstFirstKwh();
					kwhConsumption = kwhConsumption.subtract(excemptedKwhConsumption);
				}
			}
			
			BigDecimal tariffRate = getTarrifRate(setting, kwhConsumption);
			BigDecimal amount = kwhConsumption.multiply(tariffRate).setScale(2, RoundingMode.HALF_UP);
			BigDecimal gstExemptedAmount = excemptedKwhConsumption.multiply(tariffRate).setScale(2, RoundingMode.HALF_UP);
			BigDecimal maxDemandAmount = setting.getKwMaxDemandAmount().multiply(BigDecimal.valueOf(request.getKwMaxDemandUnit())).setScale(2, RoundingMode.HALF_UP);
			BigDecimal lowPowerFactorRate = getLowPowerFactorRate(BigDecimal.valueOf(request.getKwhConsumption()), 
					BigDecimal.valueOf(request.getKvarhConsumption()));
			BigDecimal subTotal = amount.add(gstExemptedAmount).add(maxDemandAmount);
			BigDecimal gst = amount.add(maxDemandAmount).multiply(gstRateOthers.divide(hundred)).setScale(2, RoundingMode.HALF_UP);
			BigDecimal lowPowerFactorCharge = getLowerPowerFactorChargeHighLevel(lowPowerFactorRate, subTotal).setScale(2, RoundingMode.HALF_UP);
			BigDecimal total = subTotal.add(lowPowerFactorCharge).add(gst);
			
			response.setTariffRate(tariffRate.doubleValue());
			
			KwhUsage gstUsage = new KwhUsage();
			gstUsage.setUnit(kwhConsumption.doubleValue());
			gstUsage.setAmount(amount.doubleValue());
			
			KwhUsage gstExemptedUsage = new KwhUsage();
			gstExemptedUsage.setUnit(excemptedKwhConsumption.doubleValue());
			gstExemptedUsage.setAmount(gstExemptedAmount.doubleValue());
			
			response.setUsageCharge(gstUsage);
			response.setTaxExemptedUsageCharge(gstExemptedUsage);
			response.setKwMaxDemandUnit(request.getKwMaxDemandUnit());
			response.setKwMaxDemandAmount(maxDemandAmount.doubleValue());
			response.setLowPowerFactorRate(lowPowerFactorRate.doubleValue());
			response.setSubTotal(subTotal.doubleValue());
			response.setGstAmount(gst.doubleValue());
			response.setLowPowerFactorCharge(lowPowerFactorCharge.doubleValue());
			response.setBillAmount(total.doubleValue());
			response.setMinMonthlytCharge(setting.getMinMonthlyChargeAmount().doubleValue());
		}
	}
	
	private void calculateLevel3(CalculateBillResponse response, List<TariffSettingDAO> settings) throws ServiceException{
		
		if(settings == null || settings.isEmpty()){
			throw new ServiceException(
					ResourceUtil.get("error.tarrif_setting_not_defined", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		}else{
			TariffSettingDAO setting = settings.get(0); // always first
			
			BigDecimal kwhConsumption = BigDecimal.valueOf(request.getKwhConsumption());
			BigDecimal offpeakKwhConsumption = BigDecimal.valueOf(request.getOffpeakKwhConsumption());
			
			BigDecimal tariffRate = setting.getPeakAmount();
			BigDecimal offpeakTariffRate = setting.getNonPeakAmount();
			
			BigDecimal amount = kwhConsumption.multiply(tariffRate).setScale(2, RoundingMode.HALF_UP);
			BigDecimal offpeakAmount = offpeakKwhConsumption.multiply(offpeakTariffRate).setScale(2, RoundingMode.HALF_UP);
			
			BigDecimal maxDemandAmount = setting.getKwMaxDemandAmount().multiply(BigDecimal.valueOf(request.getKwMaxDemandUnit())).setScale(2, RoundingMode.HALF_UP);
			BigDecimal lowPowerFactorRate = getLowPowerFactorRate(
					BigDecimal.valueOf(request.getKwhConsumption() + request.getOffpeakKwhConsumption()), 
					BigDecimal.valueOf(request.getKvarhConsumption() + request.getOffpeakKvarhConsumption()));
			BigDecimal subTotal = amount.add(offpeakAmount).add(maxDemandAmount);
			BigDecimal gst = amount.add(offpeakAmount).add(maxDemandAmount).multiply(gstRateOthers.divide(hundred)).setScale(2, RoundingMode.HALF_UP);
			BigDecimal lowPowerFactorCharge = getLowerPowerFactorChargeHighLevel(lowPowerFactorRate, subTotal).setScale(2, RoundingMode.HALF_UP);
			BigDecimal total = subTotal.add(lowPowerFactorCharge).add(gst);
			
			response.setTariffRate(tariffRate.doubleValue());
			response.setOffpeakTariffRate(offpeakTariffRate.doubleValue());
			
			KwhUsage gstUsage = new KwhUsage();
			gstUsage.setUnit(kwhConsumption.doubleValue());
			gstUsage.setAmount(amount.doubleValue());
			
			KwhUsage offpeakGstUsage = new KwhUsage();
			offpeakGstUsage.setUnit(offpeakKwhConsumption.doubleValue());
			offpeakGstUsage.setAmount(offpeakAmount.doubleValue());
			
			response.setUsageCharge(gstUsage);
			response.setOffpeakUsageCharge(offpeakGstUsage);
			response.setKwMaxDemandUnit(request.getKwMaxDemandUnit());
			response.setKwMaxDemandAmount(maxDemandAmount.doubleValue());
			response.setLowPowerFactorRate(lowPowerFactorRate.doubleValue());
			response.setSubTotal(subTotal.doubleValue());
			response.setGstAmount(gst.doubleValue());
			response.setLowPowerFactorCharge(lowPowerFactorCharge.doubleValue());
			response.setBillAmount(total.doubleValue());
			response.setMinMonthlytCharge(setting.getMinMonthlyChargeAmount().doubleValue());
		}
	}
	
	private TariffSettingDAO getDesignatedSetting(List<TariffSettingDAO> settings, BigDecimal kwhUsage){
		
		TariffSettingDAO designatedSetting = null;
		for(TariffSettingDAO setting : settings){
			
			BigDecimal matchRating = getMatchRating(setting, kwhUsage);
			
			if(matchRating != null){
				if(designatedSetting == null){
					designatedSetting = setting;
				}else{
					BigDecimal prevMatchRating = getMatchRating(designatedSetting, kwhUsage);
					if(matchRating.compareTo(prevMatchRating) < 0){
						designatedSetting = setting;
					}
				}
			}
		}
		
		return designatedSetting;
	}
	
	/**
	 * Lowest the better
	 */
	private BigDecimal getMatchRating(TariffSettingDAO setting, BigDecimal kwhUsage){

		if(setting.getTrFromUnit() != null && setting.getTrToUnit() != null){
			if(kwhUsage.compareTo(setting.getTrFromUnit()) >= 0 
					&& kwhUsage.compareTo(setting.getTrToUnit()) <= 0){
				// Between
				return setting.getTrToUnit().subtract(setting.getTrFromUnit());
			}
		}
		
		if(kwhUsage.compareTo(setting.getTrMoreUnit()) >= 0){
			return kwhUsage.subtract(setting.getTrMoreAmount());
		}
		
		return null;
	}
	
	private BigDecimal getTarrifRate(TariffSettingDAO setting, BigDecimal unit){
		
		if(setting.getTrFromUnit() != null && setting.getTrToUnit() != null && setting.getTrMoreUnit() == null){
			return setting.getTrAmountPerUnit();
		}else if(setting.getTrFromUnit() == null && setting.getTrToUnit() == null && setting.getTrMoreUnit() != null){
			return setting.getTrMoreAmount();
		}else if(setting.getTrFromUnit() != null && setting.getTrToUnit() != null && setting.getTrMoreUnit() != null){
			
			if(unit.compareTo(setting.getTrMoreUnit()) >= 0){
				return setting.getTrMoreAmount();
			}else{
				return setting.getTrAmountPerUnit();
			}
			
		}else{
			logger.error("Tarrif setting is invalid, all unit null");
			return null;
		}
	}
	
	private BigDecimal getLowPowerFactorRate(BigDecimal kwh, BigDecimal kvarh){
		BigDecimal kwhRatio = kwh.multiply(kwh);
		BigDecimal kvarhRatio = kvarh.multiply(kvarh);
		BigDecimal ratio = kwhRatio.add(kvarhRatio);
		
		if(ratio.compareTo(zero) == 0){
			return zero;
		}else{
			BigDecimal root = BigDecimal.valueOf(Math.sqrt(kwhRatio.doubleValue() + kvarhRatio.doubleValue())).setScale(2, RoundingMode.HALF_UP);
			return kwh.divide(root, 2, RoundingMode.FLOOR);	
		}
	}
	
	private BigDecimal dot75 = BigDecimal.valueOf(0.75);
	private BigDecimal dot85 = BigDecimal.valueOf(0.85);
	private BigDecimal onePoint5 = BigDecimal.valueOf(1.5);
	private BigDecimal three = BigDecimal.valueOf(3);
	private BigDecimal getLowerPowerFactorCharge(BigDecimal lowPowerFactorRate, BigDecimal subTotal){
		if(lowPowerFactorRate.compareTo(dot75) < 0){
			BigDecimal a = dot85.subtract(dot75).multiply(onePoint5).multiply(subTotal);
			BigDecimal b = dot75.subtract(lowPowerFactorRate).multiply(three).multiply(subTotal);
			return a.add(b);
		}else if(lowPowerFactorRate.compareTo(dot85) < 0){
			return dot85.subtract(lowPowerFactorRate).multiply(onePoint5).multiply(subTotal);
		}else{
			return zero;
		}
	}
	
	private BigDecimal getLowerPowerFactorChargeHighLevel(BigDecimal lowPowerFactorRate, BigDecimal subTotal){
		BigDecimal a = dot85.subtract(dot75).multiply(onePoint5).multiply(subTotal);
		BigDecimal firstCompare = dot75.multiply(a);
		if(lowPowerFactorRate.compareTo(firstCompare) < 0){
			if(lowPowerFactorRate.compareTo(dot85) < 0){
				return dot85.subtract(lowPowerFactorRate).multiply(onePoint5).multiply(subTotal);
			}else{
				return zero;
			}
		}else{
			return zero;
		}
	}
}
