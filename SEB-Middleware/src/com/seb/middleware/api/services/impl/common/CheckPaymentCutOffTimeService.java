package com.seb.middleware.api.services.impl.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.api.services.response.Response;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.utility.ResourceUtil;
import com.seb.middleware.utility.StringUtil;

@Service(name = "check_payment_cut_off_time", publicService = true)
public class CheckPaymentCutOffTimeService extends ServiceBase<Request, Response> {

	private static final Logger logger = LogManager.getLogger(CheckPaymentCutOffTimeService.class);
	
	public CheckPaymentCutOffTimeService(Request request) {
		super(request);
	}

	@Override
	public Response perform() throws ServiceException {
		Response response = new Response();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			SettingHelper sHelper = new SettingHelper(db);
			String start = sHelper.getPaymentCutOffStart();
			String end = sHelper.getPaymentCutOffEnd();
			
			if(!StringUtil.isEmpty(start) && !StringUtil.isEmpty(end)){
				boolean isCutOff = isCutOff(start, end, new Date());
				
				if(isCutOff){
					throw new ServiceException(
							ResourceUtil.get("error.payment_cutoff_time_now", 
									request.getLocale(), getDisplayTime(start), getDisplayTime(end)), 
									APIResponse.ERROR_CODE_INVALID_INPUT);
				}else{
					response.getResponseStatus().setStatus(SUCCESS);
				}
			}else{
				logger.info("incomplete payment cutoff time setting");
				response.getResponseStatus().setStatus(SUCCESS);
			}
		} catch (ParseException e) {
			logger.error("Error checking cut off time", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		}finally{
			db.close();
		}
		
		return response;
	}
	
	private String getDisplayTime(String hhmmss) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Date d = sdf.parse(hhmmss);
		SimpleDateFormat sdfDisplay = new SimpleDateFormat("hh:mma");
		return sdfDisplay.format(d);
	}

	private boolean isCutOff(String start, String end, Date time) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String timeNow = sdf.format(time);
		
		if((end.compareTo(start) < 0 && (timeNow.compareTo(start) >= 0 || timeNow.compareTo(end) <= 0)) 
				|| (timeNow.compareTo(start) >= 0 && timeNow.compareTo(end) <= 0)){
			    return true;
		}else{
			return false;
		}
	}
}
