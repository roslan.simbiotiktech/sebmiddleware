package com.seb.middleware.api.services.impl.common;

import java.util.List;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.common.GetMobileNumberPrefixRequest;
import com.seb.middleware.api.services.response.common.GetMobilePrefixResponse;
import com.seb.middleware.api.services.response.common.MobilePrefix;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.MobilePrefixDAO;
import com.seb.middleware.jpa.helper.ContentHelper;

@Service(name = "get_mobile_number_prefix", publicService = true)
public class GetMobileNumberPrefixService extends ServiceBase<GetMobileNumberPrefixRequest, GetMobilePrefixResponse> {

	public GetMobileNumberPrefixService(GetMobileNumberPrefixRequest request) {
		super(request);
	}

	@Override
	public GetMobilePrefixResponse perform() throws ServiceException {
		GetMobilePrefixResponse response = new GetMobilePrefixResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			
			ContentHelper cHelper = new ContentHelper(db);
			List<MobilePrefixDAO> list = cHelper.getMobilePrefix();
			
			MobilePrefix [] mobilePrefixes = new MobilePrefix[list.size()];
			
			for(int i = 0; i < list.size(); i++){
				MobilePrefixDAO pref = list.get(i);
				mobilePrefixes[i] = new MobilePrefix();
				mobilePrefixes[i].setCountryName(pref.getCountryName());
				mobilePrefixes[i].setPrefix(pref.getPrefix());
			}
			
			response.setMobilePrefixes(mobilePrefixes);
			
		}finally{
			db.close();
		}
		
		response.getResponseStatus().setStatus(SUCCESS);
		return response;
	}

}
