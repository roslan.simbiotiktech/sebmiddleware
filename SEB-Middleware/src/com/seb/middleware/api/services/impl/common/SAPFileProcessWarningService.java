package com.seb.middleware.api.services.impl.common;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.common.SAPFileProcessWarningRequest;
import com.seb.middleware.api.services.response.common.SAPFileProcessWarningResponse;
import com.seb.middleware.email.EmailManager;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.utility.ResourceUtil;
import com.seb.middleware.utility.StringUtil;

@Service(name = "sap_file_process_warning", publicService = true, auditable = true)
public class SAPFileProcessWarningService extends ServiceBase<SAPFileProcessWarningRequest, SAPFileProcessWarningResponse> {

	private static final Logger logger = LogManager.getLogger(SAPFileProcessWarningService.class);
	
	public SAPFileProcessWarningService(SAPFileProcessWarningRequest request) {
		super(request);
	}

	@Override
	public SAPFileProcessWarningResponse perform() throws ServiceException {
		SAPFileProcessWarningResponse response = new SAPFileProcessWarningResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		EmailManager manager = new EmailManager();
		try {
			SettingHelper setting = new SettingHelper(db);
			String emails = setting.getSAPFileProcessingNotifyRecipients();
			
			if(!StringUtil.isEmpty(emails)){
				String [] recipients = emails.split(",");
				ArrayList<String> list = new ArrayList<String>();
				for(String s : recipients){
					String eml = s.trim();
					if(!StringUtil.isDigit(eml)){
						list.add(eml);	
					}
				}
				String date = "";
				String fileName = "";
				String details = "";
				if(!StringUtil.isEmpty(request.getDate())){
					date = request.getDate();
				}
				if(!StringUtil.isEmpty(request.getFileName())){
					fileName = request.getFileName();
				}
				if(!StringUtil.isEmpty(request.getDetails())){
					details = request.getDetails();
				}
				if(!list.isEmpty()){
					manager.sendSAPFileProcessingWarning(list, date, fileName, details);	
				}else{
					logger.error("getSAPFileProcessingNotifyRecipients() invalid, unable to send warning. " + emails);
				}
			}else{
				logger.error("getSAPFileProcessingNotifyRecipients() null, unable to send warning");
			}
			
			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		} catch (Exception e) {
			logger.error("Error Sending Email - SAP File warning", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} finally{
			db.close();
		}
	}

	@Override
	public String getAuditUser(){
		return "SYSTEM";
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("SAP Flat file processing error. ");
		sb.append("File - ").append(request.getFileName());
		sb.append(" (").append(request.getDate()).append(")");
		return sb.toString();
	}
}
