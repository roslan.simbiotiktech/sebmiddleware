package com.seb.middleware.api.services.impl.common;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.common.VerifyContractAccountNumberRequest;
import com.seb.middleware.api.services.response.common.VerifyContractAccountNumberResponse;

@Service(name = "verify_contract_acc_no", publicService = true)
public class VerifyContractAccountNumberService extends ServiceBase<VerifyContractAccountNumberRequest, VerifyContractAccountNumberResponse> {

	public VerifyContractAccountNumberService(VerifyContractAccountNumberRequest request) {
		super(request);
	}

	@Override
	public VerifyContractAccountNumberResponse perform() throws ServiceException {
		VerifyContractAccountNumberResponse response = new VerifyContractAccountNumberResponse();
		response.getResponseStatus().setStatus(SUCCESS);
		return response;
	}

}
