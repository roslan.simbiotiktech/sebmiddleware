package com.seb.middleware.api.services.impl.content;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.content.GetAvailableStationRequest;
import com.seb.middleware.api.services.response.content.GetAvailableStationResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.helper.ContentHelper;

@Service(name = "get_available_station", publicService = true)
public class GetAvailableStationService extends ServiceBase<GetAvailableStationRequest, GetAvailableStationResponse> {

	public GetAvailableStationService(GetAvailableStationRequest request) {
		super(request);
	}

	@Override
	public GetAvailableStationResponse perform() throws ServiceException {
		GetAvailableStationResponse response = new GetAvailableStationResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			ContentHelper helper = new ContentHelper(db);
			response.setStations(helper.getStations());
			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		}finally{
			db.close();
		}
	}
}
