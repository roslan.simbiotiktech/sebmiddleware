package com.seb.middleware.api.services.impl.content;

import java.util.ArrayList;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.content.GetContactUsRequest;
import com.seb.middleware.api.services.response.content.GetContactUsResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.helper.ContentHelper;

@Service(name = "get_contactus", publicService = true)
public class GetContactUsService extends ServiceBase<GetContactUsRequest, GetContactUsResponse> {

	public GetContactUsService(GetContactUsRequest request) {
		super(request);
	}

	@Override
	public GetContactUsResponse perform() throws ServiceException {
		GetContactUsResponse response = new GetContactUsResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		ContentHelper contentHelper = new ContentHelper(db);
		
		try{
			ArrayList<com.seb.middleware.jpa.dao.ContactUs> contactUsDaoList = contentHelper.getContactUs();
						
			for(com.seb.middleware.jpa.dao.ContactUs contactUsDao : contactUsDaoList){
				
				response.setTelNumber(contactUsDao.getTelNo());
				response.setFaxNumber(contactUsDao.getFaxNo());
				response.setEmail(contactUsDao.getEmail());
				response.setAddress(contactUsDao.getAddress());
				response.setWebsiteUrl(contactUsDao.getWebUrl());
				
				break;
			}
			
			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		}finally{
			db.close();
		}
	}
	
}
