package com.seb.middleware.api.services.impl.content;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.content.GetCustomerServiceCounterRequest;
import com.seb.middleware.api.services.response.Location;
import com.seb.middleware.api.services.response.content.Country;
import com.seb.middleware.api.services.response.content.GetCustomerServiceCounterResponse;
import com.seb.middleware.api.services.response.content.Region;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.CustServiceCounter;
import com.seb.middleware.jpa.dao.CustServiceLocation;
import com.seb.middleware.jpa.helper.ContentHelper;
import com.seb.middleware.utility.StringUtil;

@Service(name = "get_customer_service_counter", publicService = true)
public class GetCustomerServiceCounterService extends ServiceBase<GetCustomerServiceCounterRequest, GetCustomerServiceCounterResponse> {

	public GetCustomerServiceCounterService(GetCustomerServiceCounterRequest request) {
		super(request);
	}

	@Override
	public GetCustomerServiceCounterResponse perform() throws ServiceException {
		GetCustomerServiceCounterResponse response = new GetCustomerServiceCounterResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		ContentHelper contentHelper = new ContentHelper(db);
		
		try{
			ArrayList<CustServiceCounter> custServiceCounters = contentHelper.getCustServiceCounters(request.getStation());
			
			HashMap<String,ArrayList<Country>> regionList = new HashMap<String,ArrayList<Country>>();
			
			for(CustServiceCounter custServiceCounter : custServiceCounters){
				
				CustServiceLocation custServiceLocation = custServiceCounter.getCustServiceLocation();
				
				String addressDelimiter = "\n";
				String stationName = custServiceLocation.getStation();
				
				/*
				 * <Address Line1> <Address Line2> <Address Line3>
				 * <Postcode> <City>
				 */
				StringBuilder address = new StringBuilder();
				
				StringBuilder line1 = new StringBuilder();
				StringBuilder line2 = new StringBuilder();
				
				ArrayList<String> workingLines = new ArrayList<String>();
				if(!StringUtil.isEmpty(custServiceCounter.getAddressLine1())){
					String line = custServiceCounter.getAddressLine1().trim();
					if(!line.isEmpty()) workingLines.add(line);
				}
				if(!StringUtil.isEmpty(custServiceCounter.getAddressLine2())){
					String line = custServiceCounter.getAddressLine2().trim();
					if(!line.isEmpty()) workingLines.add(line);
				}
				if(!StringUtil.isEmpty(custServiceCounter.getAddressLine3())){
					String line = custServiceCounter.getAddressLine3().trim();
					if(!line.isEmpty()) workingLines.add(line);
				}
				
				for(int i = 0; i < workingLines.size(); i++){
					line1.append(workingLines.get(i));
					if(i + 1 < workingLines.size()) line1.append(" ");
				}
				
				workingLines = new ArrayList<String>();
				if(!StringUtil.isEmpty(custServiceCounter.getPostCode())){
					String line = custServiceCounter.getPostCode().trim();
					if(!line.isEmpty()) workingLines.add(line);
				}
				if(!StringUtil.isEmpty(custServiceCounter.getCity())){
					String line = custServiceCounter.getCity().trim();
					if(!line.isEmpty()) workingLines.add(line);
				}
				
				for(int i = 0; i < workingLines.size(); i++){
					line2.append(workingLines.get(i));
					if(i + 1 < workingLines.size()) line2.append(" ");
				}
				
				workingLines = new ArrayList<String>();
				if(!line1.toString().trim().isEmpty()){
					workingLines.add(line1.toString().trim());
				}
				if(!line2.toString().trim().isEmpty()){
					workingLines.add(line2.toString().trim());
				}
				
				for(int i = 0; i < workingLines.size(); i++){
					address.append(workingLines.get(i));
					if(i + 1 < workingLines.size()) address.append(addressDelimiter);
				}
				
				Country country = new Country();
				country.setName(custServiceCounter.getAddressLine1());
				country.setAddress(address.toString());
				country.setOpeningHours(custServiceCounter.getOpeningHour());
				country.setLocation(new Location(custServiceCounter.getLatitude(), custServiceCounter.getLongitude(), ""));
				
				if(regionList.containsKey(stationName)){
					regionList.get(stationName).add(country);
				}else{
					ArrayList<Country> countries = new ArrayList<Country>();
					countries.add(country);
					regionList.put(stationName, countries);
				}
				
			}
			
			Region [] regions = new Region [regionList.size()];
			
			int counter = 0;
			
			Map<String,ArrayList<Country>> sortedRegionMap = new TreeMap<String,ArrayList<Country>>(regionList);
			
			for(Entry<String,ArrayList<Country>> entry : sortedRegionMap.entrySet()){
				String stationName = entry.getKey();
				ArrayList<Country> countryList = entry.getValue();
				Country[] countries = new Country[countryList.size()];
				entry.getValue().toArray(countries);
				
				regions[counter] = new Region(stationName, countries);
				
				counter++;
			}
			
			response.getResponseStatus().setStatus(SUCCESS);
			response.setRegions(regions);
			return response;
		}finally{
			db.close();
		}
	}
	
}
