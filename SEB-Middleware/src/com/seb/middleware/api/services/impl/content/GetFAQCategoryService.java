package com.seb.middleware.api.services.impl.content;

import java.util.ArrayList;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.content.GetFAQCategoryRequest;
import com.seb.middleware.api.services.response.content.FaqCategory;
import com.seb.middleware.api.services.response.content.GetFAQCategoryResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.FaqCategoryDao;
import com.seb.middleware.jpa.helper.ContentHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "get_faq_category", publicService = true)
public class GetFAQCategoryService extends ServiceBase<GetFAQCategoryRequest, GetFAQCategoryResponse> {

	public GetFAQCategoryService(GetFAQCategoryRequest request) {
		super(request);
	}

	@Override
	public GetFAQCategoryResponse perform() throws ServiceException {
		GetFAQCategoryResponse response = new GetFAQCategoryResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		ContentHelper contentHelper = new ContentHelper(db);
		
		try{
			ArrayList<FaqCategoryDao> faqCategoriesDao = contentHelper.getFaqCategory();
			
			if(faqCategoriesDao != null && !faqCategoriesDao.isEmpty()){

				FaqCategory [] faqCategories = new FaqCategory [faqCategoriesDao.size()];
				
				int counter = 0;
				
				for(FaqCategoryDao faqCategoryDao : faqCategoriesDao){
					
					FaqCategory faqCategory = new FaqCategory();
					faqCategory.setKey(faqCategoryDao.getName());
					faqCategory.setName(ResourceUtil.get(faqCategoryDao.getName(), request.getLocale()));
					faqCategories[counter] = faqCategory;
					
					counter++;
				}
				
				response.setFaqCategories(faqCategories);
			}
			
			response.getResponseStatus().setStatus(SUCCESS);
			
			return response;
		}finally{
			db.close();
		}
	}
}
