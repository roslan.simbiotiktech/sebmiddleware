package com.seb.middleware.api.services.impl.content;

import java.util.ArrayList;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.content.GetFAQRequest;
import com.seb.middleware.api.services.response.content.Faq;
import com.seb.middleware.api.services.response.content.GetFAQResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.helper.ContentHelper;

@Service(name = "get_faq", publicService = true)
public class GetFAQService extends ServiceBase<GetFAQRequest, GetFAQResponse> {

	public GetFAQService(GetFAQRequest request) {
		super(request);
	}

	@Override
	public GetFAQResponse perform() throws ServiceException {
		GetFAQResponse response = new GetFAQResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		ContentHelper contentHelper = new ContentHelper(db);
		
		try{
			String categoryKey = request.getCategoryKey();
			
			ArrayList<com.seb.middleware.jpa.dao.Faq> faqDaoList = contentHelper.getFaqs(categoryKey);
			
			Faq[] faqs = new Faq[faqDaoList.size()];
			
			int counter = 0;
			
			String locale = request.getLocale();
			
			for(com.seb.middleware.jpa.dao.Faq faqDao : faqDaoList){
				
				if(locale.equalsIgnoreCase("en")){
					faqs[counter] = new Faq(faqDao.getQuestionEn(), faqDao.getAnswerEn());
				}else if(locale.equalsIgnoreCase("zh")){
					faqs[counter] = new Faq(faqDao.getQuestionCn(), faqDao.getAnswerCn());
				}else if(locale.equalsIgnoreCase("ms")){
					faqs[counter] = new Faq(faqDao.getQuestionBm(), faqDao.getAnswerBm());
				}
				
				counter++;
			}
			
			response.getResponseStatus().setStatus(SUCCESS);
			response.setFaqs(faqs);
			return response;
		}finally{
			db.close();
		}
	}
	
}
