package com.seb.middleware.api.services.impl.content;

import java.util.ArrayList;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.content.GetPowerAlertRequest;
import com.seb.middleware.api.services.response.Pagination;
import com.seb.middleware.api.services.response.content.GetPowerAlertResponse;
import com.seb.middleware.api.services.response.content.PowerAlert;
import com.seb.middleware.constant.PowerAlertType;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.PowerAlertDao;
import com.seb.middleware.jpa.helper.ContentHelper;
import com.seb.middleware.utility.StringUtil;

@Service(name = "get_power_alert", publicService = true)
public class GetPowerAlertService extends ServiceBase<GetPowerAlertRequest, GetPowerAlertResponse> { 
	
	public GetPowerAlertService(GetPowerAlertRequest request) {
		super(request);
	}

	@Override
	public GetPowerAlertResponse perform() throws ServiceException {
		GetPowerAlertResponse response = new GetPowerAlertResponse();
				
		DatabaseFacade db = new DatabaseFacade();
		
		ContentHelper contentHelper = new ContentHelper(db);
		
		Pagination pagination = new Pagination();
		pagination.setPaging(request.getPaging());
		
		response.setPagination(pagination);
		
		try{
			
			if(request.getAlertId() > 0){
				PowerAlertDao powerAlertDao = contentHelper.getPowerAlert(request.getAlertId());
				
				if(powerAlertDao != null){
					pagination.setMaxPage(1);
					pagination.setTotalRecords(1);
					
					PowerAlert [] powerAlerts = new PowerAlert [1];
					PowerAlert powerAlert = convert(powerAlertDao);
					powerAlerts[0] = powerAlert;
					response.setPowerAlerts(powerAlerts);
				}else{
					pagination.setMaxPage(0);
					pagination.setTotalRecords(0);
				}
			}else{
				PowerAlertType type = null;
				if(!StringUtil.isEmpty(request.getAlertType())){
					type = PowerAlertType.valueOf(request.getAlertType());
				}
				
				ArrayList<PowerAlertDao> powerAlertDaos = contentHelper.getPowerAlerts(type, pagination, 
						request.getSearchKeyword(), request.getSearchDate());
				if(powerAlertDaos != null && !powerAlertDaos.isEmpty()){
					PowerAlert [] powerAlerts = new PowerAlert [powerAlertDaos.size()];
					
					int count = 0 ;
					
					for(PowerAlertDao powerAlertDao : powerAlertDaos){
						PowerAlert powerAlert = convert(powerAlertDao);
						
						powerAlerts[count] = powerAlert;
						
						count++;
					}
					
					response.setPowerAlerts(powerAlerts);
				}
			}
			
			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		}finally{
			db.close();
		}
	}
	
	private PowerAlert convert(PowerAlertDao powerAlertDao){
		PowerAlert powerAlert = new PowerAlert();
		
		PowerAlertType powerAlertType = PowerAlertType.fromValue(powerAlertDao.getPowerAlertType().getName());
		
		powerAlert.setAlertId(powerAlertDao.getId());
		powerAlert.setAlertType(powerAlertType.getValue());
		powerAlert.setStation(powerAlertDao.getCustServiceLocation().getStation());
		powerAlert.setArea(powerAlertDao.getArea());
		powerAlert.setCauses(powerAlertDao.getCauses());
		powerAlert.setTitle(powerAlertDao.getTitle());
		powerAlert.setCreatedDatetime(powerAlertDao.getCreatedDatetime());
		
		if(powerAlertType.equals(PowerAlertType.OUTAGE_ANNOUNCEMENT)){
			powerAlert.setEstimatedRestoreDatetime(powerAlertDao.getRestorationDatetime());
		}else if(powerAlertType.equals(PowerAlertType.PREVENTIVE_MAINTENANCE)){
			powerAlert.setMaintenanceEndDatetime(powerAlertDao.getMaintenanceEndDatetime());
			powerAlert.setMaintenanceStartDatetime(powerAlertDao.getMaintenanceStartDatetime());
		}
		
		return powerAlert;
	}
}
