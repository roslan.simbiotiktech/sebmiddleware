package com.seb.middleware.api.services.impl.content;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.content.GetPrivacyPolicyRequest;
import com.seb.middleware.api.services.response.content.GetPrivacyPolicyResponse;
import com.seb.middleware.core.PathManager;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "get_privacy_policy", publicService = true)
public class GetPrivacyPolicyService extends ServiceBase<GetPrivacyPolicyRequest, GetPrivacyPolicyResponse> {

	private static final Logger logger = LogManager.getLogger(GetPrivacyPolicyService.class);
	public static final String FILE = "tpl_privacy_policy.txt";
	
	public GetPrivacyPolicyService(GetPrivacyPolicyRequest request) {
		super(request);
	}

	@Override
	public GetPrivacyPolicyResponse perform() throws ServiceException {
		GetPrivacyPolicyResponse response = new GetPrivacyPolicyResponse();
		
		try{
			String privacyFilePath = PathManager.getStaticTemplatePath(FILE);
			byte[] encoded = Files.readAllBytes(Paths.get(privacyFilePath));
			String privacyPolicy = new String(encoded, "UTF-8");
			
			response.getResponseStatus().setStatus(SUCCESS);
			response.setPrivacPolicy(privacyPolicy);
			
			return response;
		}catch(IOException e){
			logger.error("Error reading privacy policy template file", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		}
	}
}
