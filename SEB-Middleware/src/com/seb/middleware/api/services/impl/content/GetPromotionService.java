package com.seb.middleware.api.services.impl.content;

import java.util.ArrayList;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.content.GetPromotionRequest;
import com.seb.middleware.api.services.response.Pagination;
import com.seb.middleware.api.services.response.Photo;
import com.seb.middleware.api.services.response.content.GetPromotionResponse;
import com.seb.middleware.api.services.response.content.Promotion;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.helper.ContentHelper;
import com.seb.middleware.utility.CryptoUtil;

@Service(name = "get_promotion", publicService = true)
public class GetPromotionService extends ServiceBase<GetPromotionRequest, GetPromotionResponse> {

	public GetPromotionService(GetPromotionRequest request) {
		super(request);
	}

	@Override
	public GetPromotionResponse perform() throws ServiceException {
		GetPromotionResponse response = new GetPromotionResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		ContentHelper contentHelper = new ContentHelper(db);
		Pagination pagination = new Pagination();
		pagination.setPaging(request.getPaging());
		
		response.setPagination(pagination);
		
		try{
			if(request.getPromoId() > 0){
				com.seb.middleware.jpa.dao.Promotion promotion = contentHelper.getPromotion(request.getPromoId());
				
				if(promotion != null){
					pagination.setMaxPage(1);
					pagination.setTotalRecords(1);
					
					Promotion [] promotions = new Promotion [1];
					Promotion promotionResponse = convert(promotion);
					promotions[0] = promotionResponse;
					response.setPromotions(promotions);
				}else{
					pagination.setMaxPage(0);
					pagination.setTotalRecords(0);
				}
			}else{
				ArrayList<com.seb.middleware.jpa.dao.Promotion> promotionList = 
						contentHelper.getPromotions(pagination);
				if(promotionList != null && !promotionList.isEmpty()){
					Promotion [] promotions = new Promotion [promotionList.size()];
					
					int counter = 0;
					
					for(com.seb.middleware.jpa.dao.Promotion promotion : promotionList){
						Promotion promotionResponse = convert(promotion);
						promotions[counter] = promotionResponse;
						
						counter++;
					}
					
					response.setPromotions(promotions);
				}
			}
			
			response.getResponseStatus().setStatus(SUCCESS);
			
			return response;
		}finally{
			db.close();
		}
	}
	
	private Promotion convert(com.seb.middleware.jpa.dao.Promotion promotion){
		
		Promotion promotionResponse = new Promotion();
		promotionResponse.setPromoId(promotion.getId());
		promotionResponse.setDescription(promotion.getDescription());
		promotionResponse.setEndDate(promotion.getEndDateTime());
		promotionResponse.setStartDate(promotion.getStartDateTime());
		promotionResponse.setTitle(promotion.getTitle());
		
		if(promotion.getImageSFileype() != null && promotion.getImageS() != null){
			Photo photo = new Photo(promotion.getImageSFileype(),CryptoUtil.encodeBase64(promotion.getImageS()));
			
			promotionResponse.setPhoto(photo);
		}
		return promotionResponse;
	}
}
