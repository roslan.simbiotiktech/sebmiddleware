package com.seb.middleware.api.services.impl.content;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.content.GetTermsAndConditionsRequest;
import com.seb.middleware.api.services.response.content.GetTermsAndConditionsResponse;
import com.seb.middleware.core.PathManager;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "get_terms_and_conditions", publicService = true)
public class GetTermsAndConditionsService extends ServiceBase<GetTermsAndConditionsRequest, GetTermsAndConditionsResponse> {

	private static final Logger logger = LogManager.getLogger(GetTermsAndConditionsService.class);
	public static final String FILE = "tpl_terms_and_conditions.txt";
	
	public GetTermsAndConditionsService(GetTermsAndConditionsRequest request) {
		super(request);
	}

	@Override
	public GetTermsAndConditionsResponse perform() throws ServiceException {
		GetTermsAndConditionsResponse response = new GetTermsAndConditionsResponse();
		
		try{
			String privacyFilePath = PathManager.getStaticTemplatePath(FILE);
			byte[] encoded = Files.readAllBytes(Paths.get(privacyFilePath));
			String TermsAndConditions = new String(encoded, "UTF-8");
			
			response.getResponseStatus().setStatus(SUCCESS);
			response.setTermsAndConditions(TermsAndConditions);
			
			return response;
		}catch(IOException e){
			logger.error("Error reading tnc template file", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		}
	}
}
