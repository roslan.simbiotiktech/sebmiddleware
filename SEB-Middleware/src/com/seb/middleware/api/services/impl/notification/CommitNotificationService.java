package com.seb.middleware.api.services.impl.notification;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.notification.CommitNotificationRequest;
import com.seb.middleware.api.services.response.notification.CommitNotificationResponse;
import com.seb.middleware.constant.MessageStatus;
import com.seb.middleware.constant.NotificationStatus;
import com.seb.middleware.constant.NotificationType;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.BillReadyNotification;
import com.seb.middleware.jpa.dao.NotificationHistory;
import com.seb.middleware.jpa.dao.PowerAlertDao;
import com.seb.middleware.jpa.dao.Promotion;
import com.seb.middleware.jpa.dao.ReportNotification;
import com.seb.middleware.jpa.dao.TagNotification;
import com.seb.middleware.jpa.dao.TagSubscriptions;
import com.seb.middleware.jpa.helper.ContentHelper;
import com.seb.middleware.jpa.helper.NotificationHelper;
import com.seb.middleware.utility.ResourceUtil;
import com.seb.middleware.utility.StringUtil;

@Service(name = "commit_notification", publicService = true)
public class CommitNotificationService extends ServiceBase<CommitNotificationRequest, CommitNotificationResponse> {

	private static final Logger logger = LogManager.getLogger(CommitNotificationService.class);
	
	public CommitNotificationService(CommitNotificationRequest request) {
		super(request);
	}

	@Override
	public CommitNotificationResponse perform() throws ServiceException {
		CommitNotificationResponse response = new CommitNotificationResponse();
		
		if(isArrayEmpty(request.getSuccessId()) && isArrayEmpty(request.getUnsuccessId())){
			logger.warn("Commit Notification request doesn't provide any IDs to process.");
			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		}else{
			DatabaseFacade db = new DatabaseFacade();
			
			ContentHelper contentHelper = new ContentHelper(db);
			
			try{
				NotificationType type = NotificationType.valueOf(request.getNotificationType());
				
				if(type == NotificationType.REPORT){
					
					db.beginTransaction();
					
					if(!isArrayEmpty(request.getSuccessId())){
						String hqlUpdate = "update ReportNotification set status = :newStatus where notificationId in (:successId)";
						int updated = db.createQuery(hqlUpdate)
								.setParameter("newStatus", NotificationStatus.SENT)
								.setParameterList("successId", toList(request.getSuccessId()))
								.executeUpdate();
						
						logger.debug("Commited success to " + request.getSuccessId().length + "/" + updated + " notifications.");
						
						NotificationHelper notificationHelper = new NotificationHelper(db);
						ArrayList<ReportNotification> notifications = notificationHelper.getSuccessReportNotification(request.getSuccessId());
						if(notifications != null && !notifications.isEmpty()){
							for(ReportNotification noti : notifications){
								
								NotificationHistory history = new NotificationHistory();
								history.setAssociatedId(noti.getTransId());
								history.setNotificationType(NotificationType.REPORT);
								history.setStatus(MessageStatus.UNREAD);
								history.setText(noti.getText());
								history.setUserEmail(noti.getUserEmail());
								db.insert(history);
							}
						}
					}
					
					if(!isArrayEmpty(request.getUnsuccessId())){
						String hqlUpdate = "update ReportNotification set status = :newStatus where notificationId in (:unsuccessId))";
						int updated = db.createQuery(hqlUpdate)
								.setParameter("newStatus", NotificationStatus.UNSENT)
								.setParameterList("unsuccessId", toList(request.getUnsuccessId()))
								.executeUpdate();
						logger.debug("Commited unsuccess to " + request.getUnsuccessId().length + "/" + updated + " notifications.");
					}

					db.commit();
				}else if(type == NotificationType.BILL){
					
					db.beginTransaction();
					
					if(!isArrayEmpty(request.getSuccessId())){
						String hqlUpdate = "update BillReadyNotification set status = :newStatus where notificationId in (:successId)";
						int updated = db.createQuery(hqlUpdate)
								.setParameter("newStatus", NotificationStatus.SENT)
								.setParameterList("successId", toList(request.getSuccessId()))
								.executeUpdate();
						
						logger.debug("Commited success to " + request.getSuccessId().length + "/" + updated + " notifications.");
						
						NotificationHelper notificationHelper = new NotificationHelper(db);
						ArrayList<BillReadyNotification> notifications = notificationHelper.getSuccessBillReadyNotification(request.getSuccessId());
						if(notifications != null && !notifications.isEmpty()){
							for(BillReadyNotification noti : notifications){
								
								NotificationHistory history = new NotificationHistory();
								history.setAssociatedId(noti.getNotificationId());
								history.setNotificationType(NotificationType.BILL);
								history.setStatus(MessageStatus.UNREAD);
								history.setText(noti.getText());
								history.setUserEmail(noti.getUserEmail());
								db.insert(history);
							}
						}
					}
					
					if(!isArrayEmpty(request.getUnsuccessId())){
						String hqlUpdate = "update BillReadyNotification set status = :newStatus where notificationId in (:unsuccessId))";
						int updated = db.createQuery(hqlUpdate)
								.setParameter("newStatus", NotificationStatus.UNSENT)
								.setParameterList("unsuccessId", toList(request.getUnsuccessId()))
								.executeUpdate();
						logger.debug("Commited unsuccess to " + request.getUnsuccessId().length + "/" + updated + " notifications.");
					}

					db.commit();
				}else if(type == NotificationType.PROMO){
					db.beginTransaction();
					
					if(!isArrayEmpty(request.getSuccessId())){
						String hqlUpdate = "update TagNotification set status = :newStatus where notificationId in (:successId) and notificationType = :notificationType";
						int updated = db.createQuery(hqlUpdate)
								.setParameter("newStatus", NotificationStatus.SENT)
								.setParameter("notificationType", NotificationType.PROMO)
								.setParameterList("successId", toList(request.getSuccessId()))
								.executeUpdate();
						
						logger.debug("Commited success to " + request.getSuccessId().length + "/" + updated + " notifications.");
						
						NotificationHelper notificationHelper = new NotificationHelper(db);
						ArrayList<TagNotification> notifications = 
								notificationHelper.getSuccessTagNotification(request.getSuccessId(), NotificationType.PROMO);
						if(notifications != null && !notifications.isEmpty()){
							
							for(TagNotification noti : notifications){
							
								ArrayList<TagSubscriptions> subscribers = notificationHelper.getTagSubscribers(noti.getTagName());
								
								Promotion promo = contentHelper.getPromotion(noti.getAssociatedId());
								
								for(TagSubscriptions subscriber : subscribers){
									NotificationHistory history = new NotificationHistory();
									history.setAssociatedId(noti.getAssociatedId());
									history.setNotificationType(NotificationType.PROMO);
									history.setStatus(MessageStatus.UNREAD);
									history.setText(noti.getText());
									history.setUserEmail(subscriber.getUserEmail());
									
									if(promo != null && !StringUtil.isEmpty(promo.getTitle())){
										history.setTitle(promo.getTitle());
									}
									
									db.insert(history);
								}
							}
						}
					}
					
					if(!isArrayEmpty(request.getUnsuccessId())){
						String hqlUpdate = "update TagNotification set status = :newStatus where notificationId in (:unsuccessId) and notificationType = :notificationType";
						int updated = db.createQuery(hqlUpdate)
								.setParameter("newStatus", NotificationStatus.UNSENT)
								.setParameter("notificationType", NotificationType.PROMO)
								.setParameterList("unsuccessId", toList(request.getUnsuccessId()))
								.executeUpdate();
						logger.debug("Commited unsuccess to " + request.getUnsuccessId().length + "/" + updated + " notifications.");
					}

					db.commit();
				}else if(type == NotificationType.POWER_ALERT){
					db.beginTransaction();
					
					if(!isArrayEmpty(request.getSuccessId())){
						String hqlUpdate = "update TagNotification set status = :newStatus where notificationId in (:successId) and notificationType = :notificationType)";
						int updated = db.createQuery(hqlUpdate)
								.setParameter("newStatus", NotificationStatus.SENT)
								.setParameter("notificationType", NotificationType.POWER_ALERT)
								.setParameterList("successId", toList(request.getSuccessId()))
								.executeUpdate();
						
						logger.debug("Commited success to " + request.getSuccessId().length + "/" + updated + " notifications.");
						
						NotificationHelper notificationHelper = new NotificationHelper(db);
						ArrayList<TagNotification> notifications = 
								notificationHelper.getSuccessTagNotification(request.getSuccessId(), NotificationType.POWER_ALERT);
						if(notifications != null && !notifications.isEmpty()){
							
							for(TagNotification noti : notifications){
							
								ArrayList<TagSubscriptions> subscribers = notificationHelper.getTagSubscribers(noti.getTagName());
								
								PowerAlertDao powerAlertDao = contentHelper.getPowerAlert(noti.getAssociatedId());
								
								for(TagSubscriptions subscriber : subscribers){
									NotificationHistory history = new NotificationHistory();
									history.setAssociatedId(noti.getAssociatedId());
									history.setNotificationType(NotificationType.POWER_ALERT);
									history.setStatus(MessageStatus.UNREAD);
									history.setText(noti.getText());
									history.setUserEmail(subscriber.getUserEmail());
									
									if(powerAlertDao != null){
										history.setTitle(powerAlertDao.getTitle());
									}
									
									db.insert(history);
								}
							}
						}
					}
					
					if(!isArrayEmpty(request.getUnsuccessId())){
						String hqlUpdate = "update TagNotification set status = :newStatus where notificationId in (:unsuccessId) and notificationType = :notificationType)";
						int updated = db.createQuery(hqlUpdate)
								.setParameter("newStatus", NotificationStatus.UNSENT)
								.setParameter("notificationType", NotificationType.POWER_ALERT)
								.setParameterList("unsuccessId", toList(request.getUnsuccessId()))
								.executeUpdate();
						logger.debug("Commited unsuccess to " + request.getUnsuccessId().length + "/" + updated + " notifications.");
					}

					db.commit();
				}else if(type == NotificationType.PLAIN){
					db.beginTransaction();
					
					if(!isArrayEmpty(request.getSuccessId())){
						String hqlUpdate = "update TagNotification set status = :newStatus where notificationId in (:successId) and notificationType = :notificationType)";
						int updated = db.createQuery(hqlUpdate)
								.setParameter("newStatus", NotificationStatus.SENT)
								.setParameter("notificationType", NotificationType.PLAIN)
								.setParameterList("successId", toList(request.getSuccessId()))
								.executeUpdate();
						
						logger.debug("Commited success to " + request.getSuccessId().length + "/" + updated + " notifications.");
						
						NotificationHelper notificationHelper = new NotificationHelper(db);
						ArrayList<TagNotification> notifications = 
								notificationHelper.getSuccessTagNotification(request.getSuccessId(), NotificationType.PLAIN);
						if(notifications != null && !notifications.isEmpty()){
							
							for(TagNotification noti : notifications){
							
								ArrayList<TagSubscriptions> subscribers = notificationHelper.getTagSubscribers(noti.getTagName());
								
								for(TagSubscriptions subscriber : subscribers){
									NotificationHistory history = new NotificationHistory();
									history.setNotificationType(NotificationType.PLAIN);
									history.setStatus(MessageStatus.UNREAD);
									history.setText(noti.getText());
									history.setUserEmail(subscriber.getUserEmail());
									db.insert(history);
								}
							}
						}
					}
					
					if(!isArrayEmpty(request.getUnsuccessId())){
						String hqlUpdate = "update TagNotification set status = :newStatus where notificationId in (:unsuccessId) and notificationType = :notificationType)";
						int updated = db.createQuery(hqlUpdate)
								.setParameter("newStatus", NotificationStatus.UNSENT)
								.setParameter("notificationType", NotificationType.PLAIN)
								.setParameterList("unsuccessId", toList(request.getUnsuccessId()))
								.executeUpdate();
						logger.debug("Commited unsuccess to " + request.getUnsuccessId().length + "/" + updated + " notifications.");
					}

					db.commit();
				}
				
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			} catch (DatabaseFacadeException e) {
				logger.error("Error commit notification", e);
				throw new ServiceException(e, 
						ResourceUtil.get("error.general_error_database", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			}finally{
				db.close();
			}
		}
	}
	
	private ArrayList<Long> toList(long [] arr){
		ArrayList<Long> list = new ArrayList<Long>();
		
		for(long l : arr) list.add(l);
		
		return list;
	}
	
	private boolean isArrayEmpty(long [] arr){
		return arr == null || arr.length == 0;
	}
}
