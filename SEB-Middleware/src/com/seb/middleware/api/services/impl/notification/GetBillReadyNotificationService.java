package com.seb.middleware.api.services.impl.notification;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.notification.GetBillReadyNotificationRequest;
import com.seb.middleware.api.services.response.notification.GetBillReadyNotificationResponse;
import com.seb.middleware.constant.NotificationStatus;
import com.seb.middleware.constant.NotificationType;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.BillReadyNotification;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "get_bill_ready_notification", publicService = true)
public class GetBillReadyNotificationService extends ServiceBase<GetBillReadyNotificationRequest, GetBillReadyNotificationResponse> {

	private static final Logger logger = LogManager.getLogger(GetBillReadyNotificationService.class);
	
	public GetBillReadyNotificationService(GetBillReadyNotificationRequest request) {
		super(request);
	}

	@Override
	public synchronized GetBillReadyNotificationResponse perform() throws ServiceException {
		GetBillReadyNotificationResponse response = new GetBillReadyNotificationResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			Date now = new Date();
			
			SettingHelper setting = new SettingHelper(db);
			int notificationSendPatientMin = setting.getNotificationSendPatientInMinutes();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(now);
			calendar.add(Calendar.MINUTE, (notificationSendPatientMin * -1));
			
			Date lastPatient = calendar.getTime();
			
			Criterion sendExpiredClause = Restrictions.and(
					Restrictions.eq("status", NotificationStatus.SENDING), 
					Restrictions.le("sendingDatetime", lastPatient));
			
			Criteria criteria = db.getSession()
					.createCriteria(BillReadyNotification.class)
					.add(Restrictions.lt("sendCounter", 3))
					.add(Restrictions.or(
							Restrictions.eq("status", NotificationStatus.UNSENT), 
							sendExpiredClause));
			
			List<BillReadyNotification> notifications = db.query(criteria);
			if(notifications != null && !notifications.isEmpty()){
				db.beginTransaction();
				
				String hqlUpdate = "update BillReadyNotification set status = :newStatus, "
						+ " sendCounter = sendCounter + 1, sendingDatetime = current_timestamp() "
						+ " where sendCounter < 3 "
						+ "   and (status = :unsentStatus or (status = :sendingStatus and sendingDatetime <= :lastPatientTime))";
				
				int updated = db.createQuery(hqlUpdate)
						.setParameter("newStatus", NotificationStatus.SENDING)
						.setParameter("unsentStatus", NotificationStatus.UNSENT)
						.setParameter("sendingStatus", NotificationStatus.SENDING)
						.setDate("lastPatientTime", lastPatient)
						.executeUpdate();
				
				db.commit();
				logger.info("Query listed : " + notifications.size() + " | Updated : " + updated);
				
				ArrayList<com.seb.middleware.api.services.response.notification.BillReadyNotification> notis = new ArrayList<com.seb.middleware.api.services.response.notification.BillReadyNotification>();
				
				for(BillReadyNotification noti : notifications){
					com.seb.middleware.api.services.response.notification.BillReadyNotification notification = 
							new com.seb.middleware.api.services.response.notification.BillReadyNotification();
					notification.setNotificationId(noti.getNotificationId());
					notification.setEmail(noti.getUserEmail());
					notification.setText(StringEscapeUtils.unescapeHtml4(noti.getText()));
					notification.setContractAccNo(noti.getContractAccountNumber());
					notification.setInvoiceNo(noti.getInvoiceNumber());
					notification.setInvoiceDate(noti.getInvoiceDate());
					notis.add(notification);
				}
				
				response.setNotifications(notis.toArray(new com.seb.middleware.api.services.response.notification.BillReadyNotification[notis.size()]));
				response.setNotificationType(NotificationType.BILL.toString());
				response.getResponseStatus().setStatus(SUCCESS);
				return response;	
			}else{
				logger.debug("No bill ready notification available at the moment.");
				response.setNotificationType(NotificationType.BILL.toString());
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			}	
		} catch (DatabaseFacadeException e) {
			logger.error("Error getting bill ready notification", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		}finally{
			db.close();
		}
	}
}
