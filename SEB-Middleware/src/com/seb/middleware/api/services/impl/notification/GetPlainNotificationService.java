package com.seb.middleware.api.services.impl.notification;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.notification.GetPlainNotificationRequest;
import com.seb.middleware.api.services.response.notification.GetPlainNotificationResponse;
import com.seb.middleware.constant.NotificationStatus;
import com.seb.middleware.constant.NotificationType;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.TagNotification;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "get_plain_notification", publicService = true)
public class GetPlainNotificationService extends ServiceBase<GetPlainNotificationRequest, GetPlainNotificationResponse> {

	private static final Logger logger = LogManager.getLogger(GetPlainNotificationService.class);
	
	public GetPlainNotificationService(GetPlainNotificationRequest request) {
		super(request);
	}

	@Override
	public synchronized GetPlainNotificationResponse perform() throws ServiceException {
		GetPlainNotificationResponse response = new GetPlainNotificationResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			Date now = new Date();
			
			SettingHelper setting = new SettingHelper(db);
			int notificationSendPatientMin = setting.getNotificationSendPatientInMinutes();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(now);
			calendar.add(Calendar.MINUTE, (notificationSendPatientMin * -1));
			
			Date lastPatient = calendar.getTime();
			
			Criterion sendExpiredClause = Restrictions.and(
					Restrictions.eq("status", NotificationStatus.SENDING), 
					Restrictions.le("sendingDatetime", lastPatient));
			
			Criteria criteria = db.getSession()
					.createCriteria(TagNotification.class)
					.add(Restrictions.eq("notificationType", NotificationType.PLAIN))
					.add(Restrictions.lt("pushDatetime", now))
					.add(Restrictions.lt("sendCounter", 3))
					.add(Restrictions.or(
							Restrictions.eq("status", NotificationStatus.UNSENT), 
							sendExpiredClause));
			
			ArrayList<TagNotification> notifications = db.query(criteria);
			if(notifications != null && !notifications.isEmpty()){
				db.beginTransaction();
				
				String hqlUpdate = "update TagNotification set status = :newStatus, sendCounter = sendCounter + 1, sendingDatetime = current_timestamp() where notificationType = :notificationType and pushDatetime <= :nowTime and sendCounter < 3 and (status = :unsentStatus or (status = :sendingStatus and sendingDatetime <= :lastPatientTime))";
				int updated = db.createQuery(hqlUpdate)
						.setParameter("newStatus", NotificationStatus.SENDING)
						.setParameter("unsentStatus", NotificationStatus.UNSENT)
						.setParameter("sendingStatus", NotificationStatus.SENDING)
						.setParameter("notificationType", NotificationType.PLAIN)
						.setDate("lastPatientTime", lastPatient)
						.setDate("nowTime", now)
						.executeUpdate();
				
				db.commit();
				logger.info("Query listed : " + notifications.size() + " | Updated : " + updated);
				
				ArrayList<com.seb.middleware.api.services.response.notification.TagNotification> notis = new ArrayList<com.seb.middleware.api.services.response.notification.TagNotification>();
				
				for(TagNotification noti : notifications){
					com.seb.middleware.api.services.response.notification.TagNotification notification = 
							new com.seb.middleware.api.services.response.notification.TagNotification();
					notification.setNotificationId(noti.getNotificationId());
					notification.setText(StringEscapeUtils.unescapeHtml4(noti.getText()));
					notification.setTag(noti.getTagName());
					notis.add(notification);
				}
				
				response.setNotifications(notis.toArray(new com.seb.middleware.api.services.response.notification.TagNotification[notis.size()]));
				response.setNotificationType(NotificationType.PLAIN.toString());
				response.getResponseStatus().setStatus(SUCCESS);
				return response;	
			}else{
				logger.debug("No plain notification available at the moment.");
				response.setNotificationType(NotificationType.PLAIN.toString());
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			}	
		} catch (DatabaseFacadeException e) {
			logger.error("Error getting plain notification", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		}finally{
			db.close();
		}
	}
}
