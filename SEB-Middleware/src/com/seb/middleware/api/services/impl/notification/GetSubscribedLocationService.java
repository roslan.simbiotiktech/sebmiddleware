package com.seb.middleware.api.services.impl.notification;

import java.util.ArrayList;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.notification.GetSubscribedLocationRequest;
import com.seb.middleware.api.services.response.notification.GetSubscribedLocationResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.TagSubscriptions;
import com.seb.middleware.jpa.helper.NotificationHelper;

@Service(name = "get_subscribed_location", publicService = true, auditable = true)
public class GetSubscribedLocationService extends ServiceBase<GetSubscribedLocationRequest, GetSubscribedLocationResponse> {

	public GetSubscribedLocationService(GetSubscribedLocationRequest request) {
		super(request);
	}

	@Override
	public GetSubscribedLocationResponse perform() throws ServiceException {
		GetSubscribedLocationResponse response = new GetSubscribedLocationResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			NotificationHelper notificationHelper = new NotificationHelper(db);
			ArrayList<TagSubscriptions> subscriptions = notificationHelper.getTagSubscriptions(request.getEmail());

			if(subscriptions != null && !subscriptions.isEmpty()){
				
				int counter = 0;
				
				String [] tags = new String[subscriptions.size()];
				
				for(TagSubscriptions tagSubscriptions : subscriptions){
					
					tags[counter] = tagSubscriptions.getTagName();
					
					counter++;
				}
				
				response.setTags(tags);
			}

			response.getResponseStatus().setStatus(SUCCESS);
			
			return response;
			
		} finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("Get Subscribed Location");
		return sb.toString();
	}
}
