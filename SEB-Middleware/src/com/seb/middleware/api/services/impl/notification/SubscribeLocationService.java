package com.seb.middleware.api.services.impl.notification;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.notification.SubscribeLocationRequest;
import com.seb.middleware.api.services.response.notification.SubscribeLocationResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.TagSubscriptions;
import com.seb.middleware.jpa.helper.NotificationHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "subscribe_location", publicService = true, auditable = true)
public class SubscribeLocationService extends ServiceBase<SubscribeLocationRequest, SubscribeLocationResponse> {

	private static final Logger logger = LogManager.getLogger(SubscribeLocationService.class);
	
	public SubscribeLocationService(SubscribeLocationRequest request) {
		super(request);
	}

	@Override
	public SubscribeLocationResponse perform() throws ServiceException {
		SubscribeLocationResponse response = new SubscribeLocationResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			NotificationHelper notificationHelper = new NotificationHelper(db);
			TagSubscriptions subscription = notificationHelper.getTagSubscriptions(request.getEmail(), request.getTag());
			if(subscription == null){
				db.beginTransaction();
				subscription = new TagSubscriptions();
				subscription.setUserEmail(request.getEmail());
				subscription.setTagName(request.getTag());
				db.insert(subscription);
				db.commit();
			}else{
				logger.warn("User: " + request.getEmail() + " subscription already exist with " + request.getTag());
			}

			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		} catch (DatabaseFacadeException e) {
			logger.error("Error subscribe to location notification", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		}finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("Subscribe to notification: ");
		sb.append(request.getTag());
		return sb.toString();
	}
}
