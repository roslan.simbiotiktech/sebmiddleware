package com.seb.middleware.api.services.impl.notification;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.notification.UnsubscribeLocationRequest;
import com.seb.middleware.api.services.response.notification.UnsubscribeLocationResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.TagSubscriptions;
import com.seb.middleware.jpa.helper.NotificationHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "unsubscribe_location", publicService = true, auditable = true)
public class UnsubscribeLocationService extends ServiceBase<UnsubscribeLocationRequest, UnsubscribeLocationResponse> {

	private static final Logger logger = LogManager.getLogger(UnsubscribeLocationService.class);
	
	public UnsubscribeLocationService(UnsubscribeLocationRequest request) {
		super(request);
	}

	@Override
	public UnsubscribeLocationResponse perform() throws ServiceException {
		UnsubscribeLocationResponse response = new UnsubscribeLocationResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			NotificationHelper notificationHelper = new NotificationHelper(db);
			TagSubscriptions subscription = notificationHelper.getTagSubscriptions(request.getEmail(), request.getTag());
			if(subscription != null){
				db.beginTransaction();
				db.delete(subscription);
				db.commit();
			}else{
				logger.warn("User: " + request.getEmail() + " subscription does not exist with " + request.getTag());
			}

			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		} catch (DatabaseFacadeException e) {
			logger.error("Error unsubscribe to location notification", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		}finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("Unsubscribe to notification: ");
		sb.append(request.getTag());
		return sb.toString();
	}
}
