package com.seb.middleware.api.services.impl.payment.fpx;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.payment.fpx.FPXBankListingRequest;
import com.seb.middleware.api.services.response.payment.fpx.FPXBank;
import com.seb.middleware.api.services.response.payment.fpx.FPXBankListingResponse;
import com.seb.middleware.payment.spg.SPGFacade;
import com.seb.middleware.payment.spg.SPGFpxBank;
import com.seb.middleware.payment.spg.SPGFpxBankType;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "fpx_bank_listing", publicService = true)
public class FPXBankListingService extends ServiceBase<FPXBankListingRequest, FPXBankListingResponse> {

	private static final Logger logger = LogManager.getLogger(FPXBankListingService.class);
	
	public FPXBankListingService(FPXBankListingRequest request) {
		super(request);
	}

	@Override
	public FPXBankListingResponse perform() throws ServiceException {
		FPXBankListingResponse response = new FPXBankListingResponse();
		
		try {
			Map<SPGFpxBankType, List<SPGFpxBank>> banks = SPGFacade.getBanks();
			
			response.setPersonalBanks(getFpxBanks(banks.get(SPGFpxBankType.B2C)));
			response.setCorporateBanks(getFpxBanks(banks.get(SPGFpxBankType.B2B1)));
			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		} catch (IOException e) {
			logger.error("error listing banks", e);
			throw new ServiceException(ResourceUtil.get("error.general_500", request.getLocale()), APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		}
	}
	
	private FPXBank[] getFpxBanks(List<SPGFpxBank> spgBanks){
		if(spgBanks == null){
			return null;
		}else{
			FPXBank[] banks = new FPXBank[spgBanks.size()];
			
			for(int i = 0; i < spgBanks.size(); i++){
				SPGFpxBank spgBank = spgBanks.get(i);
				FPXBank bank = new FPXBank();
				bank.setId(spgBank.getId());
				bank.setDescription(spgBank.getName());
				bank.setImagePath(spgBank.getLogo());
				bank.setEnabled(spgBank.isEnabled());
				
				banks[i] = bank;
			}
			
			return banks;
		}
	}
}
