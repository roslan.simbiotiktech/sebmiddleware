package com.seb.middleware.api.services.impl.payment.fpx;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.payment.fpx.FPXCheckPaymentRequest;
import com.seb.middleware.api.services.response.payment.CheckPaymentResponse;
import com.seb.middleware.api.services.response.payment.PaymentDetail;
import com.seb.middleware.api.services.response.payment.PaymentEntry;
import com.seb.middleware.constant.PaymentStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.PaymentGatewayDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceEntryDAO;
import com.seb.middleware.jpa.helper.PaymentHelper;
import com.seb.middleware.payment.spg.SPGFacade;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "fpx_check_payment", publicService = true)
public class FPXCheckPaymentService extends ServiceBase<FPXCheckPaymentRequest, CheckPaymentResponse> {

	private static final Logger logger = LogManager.getLogger(FPXCheckPaymentService.class);

	public FPXCheckPaymentService(FPXCheckPaymentRequest request) {
		super(request);
	}

	@Override
	public CheckPaymentResponse perform() throws ServiceException {
		CheckPaymentResponse response = new CheckPaymentResponse();

		DatabaseFacade db = new DatabaseFacade();

		try {
			PaymentHelper pHelper = new PaymentHelper(db);
			
			PaymentGatewayDAO gateway = pHelper.getSPGPaymentGateway();
			PaymentReferenceDAO reference = pHelper.getPaymentReference(gateway, request.getEmail(), request.getReferenceNo());
			
			if (reference == null) {
				logger.error("Payment reference does not found for " + request.getEmail() + ", " + request.getReferenceNo());
				throw new ServiceException(ResourceUtil.get("validation.invalid_payment_reference", request.getLocale()),
						APIResponse.ERROR_CODE_INVALID_INPUT);
			} else {
				List<PaymentReferenceEntryDAO> entries = pHelper.getPaymentEntries(reference.getPaymentId());
				
				if(entries == null || entries.isEmpty()){
					logger.error("Payment entries does not found for " + request.getEmail() + ", " + request.getReferenceNo());
					throw new ServiceException(ResourceUtil.get("validation.invalid_payment_reference", request.getLocale()),
							APIResponse.ERROR_CODE_INVALID_INPUT);
				}else{
					/**
					 * Try to requery if the status was Pending
					 */
					if (reference.getStatus() == PaymentStatus.PENDING) {
						boolean updated = SPGFacade.checkPaymentStatus(pHelper, gateway, reference);
						if (updated && reference.getStatus().isShouldCompletePayment()) {
							SPGFacade.completePayment(reference.getPaymentId(), request.getReferenceNo());
						}
					}
					
					if (reference.getStatus() != PaymentStatus.PENDING) {
						
						BigDecimal amount = BigDecimal.ZERO;
						
						PaymentEntry [] pes = new PaymentEntry[entries.size()];
						for(int i = 0; i < entries.size(); i++){
							
							PaymentReferenceEntryDAO entry = entries.get(i);
							PaymentEntry pe = new PaymentEntry();
							pe.setAmount(entry.getAmount().doubleValue());
							pe.setContractAccNo(entry.getContractAccountNumber());
							
							pes[i] = pe;
							
							amount = amount.add(entry.getAmount());
						}
						
						response.setEntries(pes);
						

						PaymentDetail pd = new PaymentDetail();
						pd.setPaymentDate(reference.getPaymentDate() == null ? reference.getCreatedDate() : reference.getPaymentDate());
						pd.setGatewayReference(reference.getGatewayReference());
						pd.setPaymentMethod(ResourceUtil.get("payment.fpx_method", request.getLocale()) + " - " + reference.getSpgBankName());
						pd.setAmount(amount.doubleValue());
						
						response.setPaymentDetail(pd);
					}
					
					response.setStatus(reference.getStatus());
					response.getResponseStatus().setStatus(SUCCESS);
					return response;
				}
			}
		} catch (IOException | NoSuchAlgorithmException e) {
			logger.error("error checking status", e);
			throw new ServiceException(ResourceUtil.get("error.general_500", request.getLocale()), APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} finally {
			db.close();
		}
	}
}
