package com.seb.middleware.api.services.impl.payment.fpx;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.payment.PaymentEntry;
import com.seb.middleware.api.services.request.payment.fpx.FPXPreparePaymentRequest;
import com.seb.middleware.api.services.response.payment.PaymentEntryDetail;
import com.seb.middleware.api.services.response.payment.PaymentField;
import com.seb.middleware.api.services.response.payment.PaymentForm;
import com.seb.middleware.api.services.response.payment.PreparePaymentResponse;
import com.seb.middleware.constant.PaymentStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.PaymentGatewayDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceDataDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceEntryDAO;
import com.seb.middleware.jpa.dao.Subscription;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.PaymentHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.payment.spg.SPGFacade;
import com.seb.middleware.payment.spg.SPGFpxBank;
import com.seb.middleware.payment.spg.SPGTransactionType;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "fpx_prepare_payment", publicService = true)
public class FPXPreparePaymentService extends ServiceBase<FPXPreparePaymentRequest, PreparePaymentResponse> {

	private static final Logger logger = LogManager.getLogger(FPXPreparePaymentService.class);
	
	private static final String SUBMIT_URL_NAME = "FPX_SUBMIT_URL";
	private static final String RETURN_URL_NAME = "RETURN_URL";
	
	public FPXPreparePaymentService(FPXPreparePaymentRequest request) {
		super(request);
	}

	@Override
	public PreparePaymentResponse perform() throws ServiceException {
		PreparePaymentResponse response = new PreparePaymentResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try {
			SettingHelper settingHelper = new SettingHelper(db);
			MemberHelper mHelper = new MemberHelper(db);
			PaymentHelper pHelper = new PaymentHelper(db);
			SubscriptionHelper sHelper = new SubscriptionHelper(db);
			
			Person person  =  mHelper.getPerson(request.getEmail());
			PaymentGatewayDAO gateway = pHelper.getSPGPaymentGateway();
			
			SPGFpxBank bank = SPGFacade.getBank(request.getBankId());
			if(bank == null){
				logger.error("SPG FPX with ID {} does not exist", request.getBankId());
				throw new ServiceException(
						ResourceUtil.get("validation.invalid_fpx_bank", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
			
			BigDecimal totalAmount = BigDecimal.ZERO;
			
			PaymentEntryDetail [] peds = new PaymentEntryDetail[request.getEntries().length]; 
			for(int i = 0; i < request.getEntries().length; i++){
				PaymentEntry entry = request.getEntries()[i];
				
				totalAmount = totalAmount.add(BigDecimal.valueOf(entry.getAmount()).setScale(2, RoundingMode.HALF_UP));
				
				Subscription subscription = sHelper.getSubscription(entry.getContractAccNo(), request.getEmail());
				if(subscription != null){
					PaymentEntryDetail ped = new PaymentEntryDetail();
					ped.setAmount(BigDecimal.valueOf(entry.getAmount()).setScale(2, RoundingMode.HALF_UP).doubleValue());
					ped.setContractAccNick(subscription.getContractAccNick());
					ped.setContractAccNo(subscription.getContractAccNo());
					
					peds[i] = ped;
				}else{
					logger.error("Member: " + request.getEmail() + 
							" did not subscribe to this Contract Account Yet (" + entry.getContractAccNo() + ")");
					throw new ServiceException(
							ResourceUtil.get("error.subscription_not_exist", 
									request.getLocale()), 
									APIResponse.ERROR_CODE_INVALID_INPUT);
				}
			}
			
			if(totalAmount.doubleValue() < gateway.getMinAmount()){
				DecimalFormat ddf = new DecimalFormat("#,##0.00");
				logger.error("FPX payment under minimum amount : " + totalAmount);
				throw new ServiceException(
						ResourceUtil.get("error.payment_amount_under", 
								request.getLocale(), ddf.format(totalAmount)), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}else if(totalAmount.doubleValue() > gateway.getMaxAmount()){
				DecimalFormat ddf = new DecimalFormat("#,##0.00");
				logger.error("FPX payment exceed maximum amount : " + totalAmount);
				throw new ServiceException(
						ResourceUtil.get("error.payment_amount_exceed", 
								request.getLocale(), ddf.format(totalAmount)), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
			
			
			logger.debug("Total amount RM {} from {} CA", totalAmount, request.getEntries().length);
			
			PaymentReferenceDAO reference = getPaymentReference(gateway, bank);
			
			db.beginTransaction();
			db.insert(reference);
			List<PaymentReferenceEntryDAO> entrys = getPaymentReferenceEntries(reference);
			for(PaymentReferenceEntryDAO entry : entrys){
				db.insert(entry);
			}
			
			String paymentReferenceNo = SPGFacade.getReferenceNo(reference);
			
			PaymentForm form = getForm(pHelper, person, bank, gateway, paymentReferenceNo, totalAmount);
			
			for(PaymentField field : form.getFields()){
				PaymentReferenceDataDAO dt = new PaymentReferenceDataDAO();
				dt.setPaymentId(reference.getPaymentId());
				dt.setName(field.getName());
				dt.setValue(field.getValue());
				db.insert(dt);
			}
			
			if(settingHelper.isTestingEnvironment()){
				logger.debug(form);
			}
			
			response.setForm(form);
			response.setEntries(peds);
			response.setNotificationEmail(request.getNotificationEmail() == null ? request.getEmail() : request.getNotificationEmail());
			response.setReferenceNo(paymentReferenceNo);
			response.setPaymentMethod(ResourceUtil.get("payment.fpx_method", request.getLocale()) + " - " + bank.getName());
			
			db.commit();
			
			response.getResponseStatus().setStatus(SUCCESS);
			
			return response;
			
		} catch (DatabaseFacadeException e) {
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		} catch (IOException | NoSuchAlgorithmException e) {
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} finally {
			db.close();
		}
	}
	
	private PaymentReferenceDAO getPaymentReference(PaymentGatewayDAO gateway, SPGFpxBank bank){
		PaymentReferenceDAO reference = new PaymentReferenceDAO();
		
		reference.setGatewayId(gateway.getGatewayId());
		reference.setSpgBankId(bank.getId());
		reference.setSpgBankName(bank.getName());
		reference.setSpgBankType(bank.getType().toString());
		reference.setStatus(PaymentStatus.PENDING);
		
		reference.setUserEmail(request.getEmail());
		reference.setNotificationEmail(request.getNotificationEmail() == null ? request.getEmail() : request.getNotificationEmail());
		reference.setIsMobile(request.isMobile() ? 1 : 0);
		reference.setReferencePrefix(SPGTransactionType.FPX.getPrefix());
		
		return reference;
	}
	
	private List<PaymentReferenceEntryDAO> getPaymentReferenceEntries(PaymentReferenceDAO reference){
		
		List<PaymentReferenceEntryDAO> list = new ArrayList<PaymentReferenceEntryDAO>();
		
		for(PaymentEntry entry : request.getEntries()){
			PaymentReferenceEntryDAO pre = new PaymentReferenceEntryDAO();
			
			pre.setPaymentId(reference.getPaymentId());
			pre.setContractAccountNumber(entry.getContractAccNo());
			pre.setAmount(BigDecimal.valueOf(entry.getAmount()).setScale(2, RoundingMode.HALF_UP));
			pre.setBillingAttemptCount(0);
			
			list.add(pre);
		}
		
		return list;
	}
	
	private PaymentForm getForm(PaymentHelper pHelper, Person person, 
			SPGFpxBank bank, PaymentGatewayDAO gateway, 
			String paymentReferenceNo, BigDecimal totalAmount) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		PaymentForm form = new PaymentForm();
		
		form.setAction(pHelper.getPaymentUrl(gateway.getGatewayId(), SUBMIT_URL_NAME));
		form.setFields(getFields(pHelper, person, bank, gateway, paymentReferenceNo, totalAmount));
		
		return form;
	}
	
	private PaymentField[] getFields(PaymentHelper pHelper, Person person, 
			SPGFpxBank bank, PaymentGatewayDAO gateway, 
			String paymentReferenceNo, BigDecimal totalAmount) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		
		Map<String, String> params = pHelper.getPaymentParameters(gateway.getGatewayId());
		
		DecimalFormat df = new DecimalFormat("0.00");
		
		String merchantId = gateway.getMerchantId();
		String transactionId = paymentReferenceNo;
		String amount = df.format(totalAmount);
		String productDescription = params.get("PRODUCT_DESCRIPTION");
		String customerUsername = request.getEmail();
		String customerEmail = request.getNotificationEmail() == null ? request.getEmail() : request.getNotificationEmail();
		String returnUrl = pHelper.getPaymentUrl(gateway.getGatewayId(), RETURN_URL_NAME);
		String bankId = String.valueOf(bank.getId());
		
		String signature = SPGFacade.getTransactionSignature(params.get(SPGFacade.PARAM_SIGNATURE_SECRET), 
				merchantId, transactionId, amount, productDescription, 
				customerUsername, customerEmail, returnUrl, bankId);
		
		int i = 0;
		PaymentField[] fields = new PaymentField[9];
		fields[i++] = getField("merchant_id", merchantId);
		fields[i++] = getField("transaction_id", transactionId);
		fields[i++] = getField("amount", amount);
		fields[i++] = getField("product_description", productDescription);
		fields[i++] = getField("customer_username", customerUsername);
		fields[i++] = getField("customer_email", customerEmail);
		fields[i++] = getField("return_url", returnUrl);
		fields[i++] = getField("bank_id", bankId);
		fields[i++] = getField("signature", signature);
		
		return fields;
	}
	
	private PaymentField getField(String name, String value){
		PaymentField pf = new PaymentField();
		pf.setName(name);
		pf.setValue(value);
		return pf;
	}
}
