package com.seb.middleware.api.services.impl.payment.mpg;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.payment.PaymentEntry;
import com.seb.middleware.api.services.request.payment.mpg.MPGPreparePaymentRequest;
import com.seb.middleware.api.services.response.payment.PaymentEntryDetail;
import com.seb.middleware.api.services.response.payment.PaymentField;
import com.seb.middleware.api.services.response.payment.PaymentForm;
import com.seb.middleware.api.services.response.payment.PreparePaymentResponse;
import com.seb.middleware.constant.MPGCardType;
import com.seb.middleware.constant.PaymentStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.PaymentGatewayDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceDataDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceEntryDAO;
import com.seb.middleware.jpa.dao.Subscription;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.PaymentHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.payment.spg.SPGFacade;
import com.seb.middleware.payment.spg.SPGTransactionType;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "mpg_prepare_payment", publicService = true)
public class MPGPreparePaymentService extends ServiceBase<MPGPreparePaymentRequest, PreparePaymentResponse> {

	private static final Logger logger = LogManager.getLogger(MPGPreparePaymentService.class);
	
	private static final String SUBMIT_URL_NAME = "MPG_SUBMIT_URL";
	private static final String RETURN_URL_NAME = "RETURN_URL";
	
	public MPGPreparePaymentService(MPGPreparePaymentRequest request) {
		super(request);
	}

	@Override
	public PreparePaymentResponse perform() throws ServiceException {
		PreparePaymentResponse response = new PreparePaymentResponse();
		
		validateCard();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try {
			SettingHelper settingHelper = new SettingHelper(db);
			MemberHelper mHelper = new MemberHelper(db);
			PaymentHelper pHelper = new PaymentHelper(db);
			SubscriptionHelper sHelper = new SubscriptionHelper(db);
			
			Person person  =  mHelper.getPerson(request.getEmail());
			PaymentGatewayDAO gateway = pHelper.getSPGPaymentGateway();
			
			BigDecimal totalAmount = BigDecimal.ZERO;
			
			PaymentEntryDetail [] peds = new PaymentEntryDetail[request.getEntries().length]; 
			for(int i = 0; i < request.getEntries().length; i++){
				PaymentEntry entry = request.getEntries()[i];
				
				totalAmount = totalAmount.add(BigDecimal.valueOf(entry.getAmount()).setScale(2, RoundingMode.HALF_UP));
				
				Subscription subscription = sHelper.getSubscription(entry.getContractAccNo(), request.getEmail());
				if(subscription != null){
					PaymentEntryDetail ped = new PaymentEntryDetail();
					ped.setAmount(BigDecimal.valueOf(entry.getAmount()).setScale(2, RoundingMode.HALF_UP).doubleValue());
					ped.setContractAccNick(subscription.getContractAccNick());
					ped.setContractAccNo(subscription.getContractAccNo());
					
					peds[i] = ped;
				}else{
					logger.error("Member: " + request.getEmail() + 
							" did not subscribe to this Contract Account Yet (" + entry.getContractAccNo() + ")");
					throw new ServiceException(
							ResourceUtil.get("error.subscription_not_exist", 
									request.getLocale()), 
									APIResponse.ERROR_CODE_INVALID_INPUT);
				}
			}
			
			if(totalAmount.doubleValue() < gateway.getMinAmount()){
				DecimalFormat ddf = new DecimalFormat("#,##0.00");
				logger.error("MPG payment under minimum amount : " + totalAmount);
				throw new ServiceException(
						ResourceUtil.get("error.payment_amount_under", 
								request.getLocale(), ddf.format(totalAmount)), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}else if(totalAmount.doubleValue() > gateway.getMaxAmount()){
				DecimalFormat ddf = new DecimalFormat("#,##0.00");
				logger.error("MPG payment exceed maximum amount : " + totalAmount);
				throw new ServiceException(
						ResourceUtil.get("error.payment_amount_exceed", 
								request.getLocale(), ddf.format(totalAmount)), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
			
			logger.debug("Total amount RM {} from {} CA", totalAmount, request.getEntries().length);
			
			PaymentReferenceDAO reference = getPaymentReference(gateway);
			
			db.beginTransaction();
			db.insert(reference);
			List<PaymentReferenceEntryDAO> entrys = getPaymentReferenceEntries(reference);
			for(PaymentReferenceEntryDAO entry : entrys){
				db.insert(entry);
			}
			
			String paymentReferenceNo = SPGFacade.getReferenceNo(reference);
			
			PaymentForm form = getForm(reference, pHelper, person, gateway, paymentReferenceNo, totalAmount);
			
			for(PaymentField field : form.getFields()){
				if(field.isStore()){
					PaymentReferenceDataDAO dt = new PaymentReferenceDataDAO();
					dt.setPaymentId(reference.getPaymentId());
					dt.setName(field.getName());
					dt.setValue(field.getValue());
					db.insert(dt);	
				}
			}
			
			if(settingHelper.isTestingEnvironment()){
				logger.debug(form);
			}
			
			response.setForm(form);
			response.setEntries(peds);
			response.setNotificationEmail(request.getNotificationEmail() == null ? request.getEmail() : request.getNotificationEmail());
			response.setReferenceNo(paymentReferenceNo);
			response.setPaymentMethod(ResourceUtil.get("payment.mpg_method", request.getLocale()));
			
			db.commit();
			
			response.getResponseStatus().setStatus(SUCCESS);
			
			return response;
		} catch (DatabaseFacadeException e) {
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} finally {
			db.close();
		}
	}
	
	private void validateCard() throws ServiceException{
		
		if(request.getCardType() == MPGCardType.V){
			if(request.getCardNumber().length() == 16 && request.getCardNumber().startsWith("4")){
				// Ok
			}else{
				logger.error("Member: " + request.getEmail() + " entered a invalid Visa card number - " + request.getCardNumber());
				throw new ServiceException(
						ResourceUtil.get("validation.invalid_card_number", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
			
			if(request.getCardCvc().length() != 3){
				logger.error("Member: " + request.getEmail() + " entered a invalid CVV - " + request.getCardCvc());
				throw new ServiceException(
						ResourceUtil.get("validation.invalid_card_cvv", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
		}else if(request.getCardType() == MPGCardType.M){
			if(request.getCardNumber().length() == 16 && 
					(request.getCardNumber().startsWith("5") || request.getCardNumber().startsWith("2"))){
				// Ok
			}else{
				logger.error("Member: " + request.getEmail() + " entered a invalid Master card number - " + request.getCardNumber());
				throw new ServiceException(
						ResourceUtil.get("validation.invalid_card_number", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
			
			if(request.getCardCvc().length() != 3){
				logger.error("Member: " + request.getEmail() + " entered a invalid CVV - " + request.getCardCvc());
				throw new ServiceException(
						ResourceUtil.get("validation.invalid_card_cvv", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
		}else if(request.getCardType() == MPGCardType.A){
			if(request.getCardNumber().length() == 15 && request.getCardNumber().startsWith("3")){
				// Ok
			}else{
				logger.error("Member: " + request.getEmail() + " entered a invalid Amex card number - " + request.getCardNumber());
				throw new ServiceException(
						ResourceUtil.get("validation.invalid_card_number", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
			
			if(request.getCardCvc().length() != 4){
				logger.error("Member: " + request.getEmail() + " entered a invalid CVV - " + request.getCardCvc());
				throw new ServiceException(
						ResourceUtil.get("validation.invalid_card_cvv", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
		}
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(request.getCardExpiry());
		cal.add(Calendar.MONTH, 1);
		
		if(cal.getTime().compareTo(new Date()) < 0){
			logger.error("Member: " + request.getEmail() + " card has expired - " + request.getCardExpiry().toString());
			throw new ServiceException(
					ResourceUtil.get("validation.invalid_card_expired", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_INVALID_INPUT);
		}
	}
	
	private PaymentReferenceDAO getPaymentReference(PaymentGatewayDAO gateway){
		PaymentReferenceDAO reference = new PaymentReferenceDAO();
		
		reference.setGatewayId(gateway.getGatewayId());
		reference.setSpgBankType(request.getCardType().toString());
		reference.setStatus(PaymentStatus.PENDING);
		
		reference.setUserEmail(request.getEmail());
		reference.setNotificationEmail(request.getNotificationEmail() == null ? request.getEmail() : request.getNotificationEmail());
		
		reference.setIsMobile(request.isMobile() ? 1 : 0);
		reference.setReferencePrefix(SPGTransactionType.MAYBANK.getPrefix());
		
		return reference;
	}
	
	private List<PaymentReferenceEntryDAO> getPaymentReferenceEntries(PaymentReferenceDAO reference){
		
		List<PaymentReferenceEntryDAO> list = new ArrayList<PaymentReferenceEntryDAO>();
		
		for(PaymentEntry entry : request.getEntries()){
			PaymentReferenceEntryDAO pre = new PaymentReferenceEntryDAO();
			
			pre.setPaymentId(reference.getPaymentId());
			pre.setContractAccountNumber(entry.getContractAccNo());
			pre.setAmount(BigDecimal.valueOf(entry.getAmount()).setScale(2, RoundingMode.HALF_UP));
			pre.setBillingAttemptCount(0);
			
			list.add(pre);
		}
		
		return list;
	}
	
	private PaymentForm getForm(PaymentReferenceDAO reference, PaymentHelper pHelper, 
			Person person, PaymentGatewayDAO gateway, String paymentReferenceNo,
			BigDecimal totalAmount) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		PaymentForm form = new PaymentForm();
		
		form.setAction(pHelper.getPaymentUrl(gateway.getGatewayId(), SUBMIT_URL_NAME));
		form.setFields(getFields(reference, pHelper, person, gateway, paymentReferenceNo, totalAmount));
		
		return form;
	}
	
	private PaymentField[] getFields(PaymentReferenceDAO reference, PaymentHelper pHelper, 
			Person person, PaymentGatewayDAO gateway, String paymentReferenceNo,
			BigDecimal totalAmount) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		
		Map<String, String> params = pHelper.getPaymentParameters(gateway.getGatewayId());
		
		DecimalFormat df = new DecimalFormat("0.00");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		
		String merchantId = gateway.getMerchantId();
		String transactionId = paymentReferenceNo;
		String amount = df.format(totalAmount);
		String productDescription = params.get("PRODUCT_DESCRIPTION");
		String customerUsername = request.getEmail();
		String customerEmail = request.getNotificationEmail() == null ? request.getEmail() : request.getNotificationEmail();
		String returnUrl = pHelper.getPaymentUrl(gateway.getGatewayId(), RETURN_URL_NAME);
		String cardType = request.getCardType().getSpgName();
		String cardNumber = request.getCardNumber();
		String cardName = request.getCardName();
		String cardExpiry = sdf.format(request.getCardExpiry());
		String cardCvc = request.getCardCvc();
		
		String signature = SPGFacade.getTransactionSignature(params.get(SPGFacade.PARAM_SIGNATURE_SECRET), 
				merchantId, transactionId, amount, productDescription, 
				customerUsername, customerEmail, returnUrl, 
				cardType, cardNumber, cardName, cardExpiry, cardCvc);
		
		int i = 0;
		PaymentField[] fields = new PaymentField[13];
		fields[i++] = getField("merchant_id", merchantId);
		fields[i++] = getField("transaction_id", transactionId);
		fields[i++] = getField("amount", amount);
		fields[i++] = getField("product_description", productDescription);
		fields[i++] = getField("customer_username", customerUsername);
		fields[i++] = getField("customer_email", customerEmail);
		fields[i++] = getField("return_url", returnUrl);
		fields[i++] = getField("card_type", cardType);
		fields[i++] = getField("card_number", cardNumber, false);
		fields[i++] = getField("card_name", cardName, false);
		fields[i++] = getField("card_expiry", cardExpiry, false);
		fields[i++] = getField("card_cvc", cardCvc, false);
		fields[i++] = getField("signature", signature);
		
		return fields;
	}
	
	private PaymentField getField(String name, String value, boolean store){
		PaymentField pf = new PaymentField();
		pf.setName(name);
		pf.setValue(value);
		pf.setStore(store);
		return pf;
	}
	
	private PaymentField getField(String name, String value){
		PaymentField pf = new PaymentField();
		pf.setName(name);
		pf.setValue(value);
		return pf;
	}
}
