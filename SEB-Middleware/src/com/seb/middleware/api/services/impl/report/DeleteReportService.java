package com.seb.middleware.api.services.impl.report;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.account.DeleteReportRequest;
import com.seb.middleware.api.services.response.account.DeleteReportResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.helper.ReportHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "delete_report", publicService = true, auditable = true)
public class DeleteReportService extends ServiceBase<DeleteReportRequest, DeleteReportResponse>  {

	private static final Logger logger = LogManager.getLogger(DeleteReportService.class);

	public DeleteReportService(DeleteReportRequest request) {
		super(request);
	}

	@Override
	public DeleteReportResponse perform() throws ServiceException {
		DeleteReportResponse response = new DeleteReportResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			
			List<Long> longTransId = new ArrayList<>();
			for(int i = 0; i < request.getTransIds().length; i++) {
				longTransId.add(Long.parseLong(request.getTransIds()[i]));
			}
			
			ReportHelper reportHelper = new ReportHelper(db);
			db.beginTransaction();
			reportHelper.deleteReportHistory(request.getEmail(), longTransId);
			db.commit();
			
			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		} catch (DatabaseFacadeException e) {
			logger.error("Error deleting report", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("Delete reports (ids): ");
		sb.append(Arrays.toString(request.getTransIds()));
		return sb.toString();
	}
}
