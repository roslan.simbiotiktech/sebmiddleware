package com.seb.middleware.api.services.impl.report;

import java.util.ArrayList;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.report.HistoryDetailRequest;
import com.seb.middleware.api.services.response.Location;
import com.seb.middleware.api.services.response.Photo;
import com.seb.middleware.api.services.response.report.HistoryDetailResponse;
import com.seb.middleware.api.services.response.report.ReportDetail;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.CaseReport;
import com.seb.middleware.jpa.helper.ReportHelper;
import com.seb.middleware.utility.CryptoUtil;

@Service(name = "history_detail", publicService = true, auditable = true)
public class HistoryDetailService extends ServiceBase<HistoryDetailRequest, HistoryDetailResponse> {

	public HistoryDetailService(HistoryDetailRequest request) {
		super(request);
	}

	@Override
	public HistoryDetailResponse perform() throws ServiceException {
		HistoryDetailResponse response = new HistoryDetailResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		ReportHelper reportHelper = new ReportHelper(db);
		
		try{
			CaseReport report = reportHelper.getReportDetail(request.getEmail(), Long.parseLong(request.getTransId()));
			
			if(report != null){
				ReportDetail r = new ReportDetail();
				
				r.setTransId(String.format("%06d", report.getTransId()));
				r.setTransStatus(report.getStatus().getTransactionStatus().toString());
				r.setTransDatetime(report.getCreatedDatetime());
				r.setType(report.getReportType().toString());
				r.setStation(report.getStation());
				r.setDescription(report.getDescription());
				r.setRemark(report.getRemark());
				r.setContractAccNo(report.getContractAccNo());
				
				if(report.getIssueType() != null){
					r.setIssueType(report.getIssueType().toString());
				}
				
				if(report.getCaseNumber() != null){
					r.setCaseId(String.format("%06d", report.getCaseNumber().longValue()));	
				}
				
				if(report.getReportCategory() != null){
					r.setCategory(report.getReportCategory().toString());
				}
				
				Location location = new Location();
				location.setLatitude(report.getLatitude());
				location.setLongitude(report.getLongitude());
				location.setAddress(report.getAddress());
				r.setLocation(location);
				
				ArrayList<Photo> photos = new ArrayList<Photo>();
				if(report.getPhoto1() != null && report.getPhoto1type() != null){
					Photo photo = new Photo();
					photo.setData(CryptoUtil.encodeBase64(report.getPhoto1()));
					photo.setDataType(report.getPhoto1type());
					photos.add(photo);
				}
				
				if(report.getPhoto2() != null && report.getPhoto2type() != null){
					Photo photo = new Photo();
					photo.setData(CryptoUtil.encodeBase64(report.getPhoto2()));
					photo.setDataType(report.getPhoto2type());
					photos.add(photo);
				}
				
				if(report.getPhoto3() != null && report.getPhoto3type() != null){
					Photo photo = new Photo();
					photo.setData(CryptoUtil.encodeBase64(report.getPhoto3()));
					photo.setDataType(report.getPhoto3type());
					photos.add(photo);
				}
				
				if(!photos.isEmpty()){
					r.setPhotos(photos.toArray(new Photo[photos.size()]));
				}
				
				response.setReportDetail(r);
			}
			
			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		}finally{
			db.close();
		}
	}

	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("View 'Make a Report' details - Task Id: ");
		sb.append(request.getTransId());
		return sb.toString();
	}
}
