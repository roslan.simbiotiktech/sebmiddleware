package com.seb.middleware.api.services.impl.report;

import java.util.ArrayList;

import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.report.HistoryListingRequest;
import com.seb.middleware.api.services.response.Location;
import com.seb.middleware.api.services.response.report.HistoryListingResponse;
import com.seb.middleware.api.services.response.report.Report;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.CaseReport;
import com.seb.middleware.jpa.helper.ReportHelper;

@Service(name = "history_listing", publicService = true, auditable = true)
public class HistoryListingService extends ServiceBase<HistoryListingRequest, HistoryListingResponse> {

	public HistoryListingService(HistoryListingRequest request) {
		super(request);
	}

	@Override
	public HistoryListingResponse perform() throws ServiceException {
		HistoryListingResponse response = new HistoryListingResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			ReportHelper helper = new ReportHelper(db);
			ArrayList<CaseReport> reports = helper.getReportDetail(request.getEmail());
			
			if(reports != null && !reports.isEmpty()){
				response.setReports(new Report[reports.size()]);
				int i = 0;
				
				for(CaseReport report : reports){
					Report r = new Report();
					
					r.setTransId(String.format("%06d", report.getTransId()));
					r.setTransStatus(report.getStatus().getTransactionStatus().toString());
					r.setTransDatetime(report.getCreatedDatetime());
					r.setType(report.getReportType().toString());
					if(report.getCaseNumber() != null){
						r.setCaseId(String.format("%06d", report.getCaseNumber().longValue()));	
					}
					
					r.setRemark(report.getRemark());
					
					Location location = new Location();
					location.setLatitude(report.getLatitude());
					location.setLongitude(report.getLongitude());
					location.setAddress(report.getAddress());
					r.setLocation(location);
					
					response.getReports()[i] = r;
					i++;
				}
			}
			
			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		}finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		return "Read 'Make a report' listing";
	}
}
