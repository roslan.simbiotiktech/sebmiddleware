package com.seb.middleware.api.services.impl.report;

import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.Photo;
import com.seb.middleware.api.services.request.report.MakeReportRequest;
import com.seb.middleware.api.services.response.report.MakeReportResponse;
import com.seb.middleware.constant.CRMReportStatus;
import com.seb.middleware.constant.ReportCategory;
import com.seb.middleware.constant.ReportIssueType;
import com.seb.middleware.constant.ReportType;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.CaseReport;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.utility.CryptoUtil;
import com.seb.middleware.utility.ResourceUtil;
import com.seb.middleware.utility.StringUtil;

@Service(name = "make_report", publicService = true, auditable = true)
public class MakeReportService extends ServiceBase<MakeReportRequest, MakeReportResponse> {

	private static final Logger logger = LogManager.getLogger(MakeReportService.class);
	
	private MakeReportResponse response;
	
	public MakeReportService(MakeReportRequest request) {
		super(request);
	}

	@Override
	public MakeReportResponse perform() throws ServiceException {
		response = new MakeReportResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		try{
			MemberHelper userHelper = new MemberHelper(db);
			
			ReportType reportType = ReportType.valueOf(request.getReportType());
			Person person = userHelper.getPerson(request.getEmail());
			
			boolean locationMandatory = false;
			boolean issueTypeMandatory = false;
			switch(reportType){
				case FAULTY_STREET_LIGHT:
					locationMandatory = true;
					break;
				case OUTAGE:
					locationMandatory = true;
					break;
				case TECHNICAL_ISSUE:
					break;
				case BILLING_AND_METER:
					issueTypeMandatory = true;
					if(StringUtil.isEmpty(request.getContractAccNo())){
						throw new ServiceException(
								ResourceUtil.get("error.report_billing_meter_contract_acc_no_mandatory", 
										request.getLocale()), 
										APIResponse.ERROR_CODE_INVALID_INPUT);
					}
					
					break;
				case GENERAL_INQUIRY:
					if(StringUtil.isEmpty(request.getReportCategory())){
						throw new ServiceException(
								ResourceUtil.get("validation.invalid_report_category", 
										request.getLocale()), 
										APIResponse.ERROR_CODE_INVALID_INPUT);
					}
					break;
			}

			ReportIssueType issueType = null;
			if(issueTypeMandatory){
				if(StringUtil.isEmpty(request.getIssueType())){
					String msg = ResourceUtil.get("validation.mandatory", request.getLocale(), ResourceUtil.get("field.issue_type", request.getLocale()));
					throw new ServiceException(msg, APIResponse.ERROR_CODE_INVALID_INPUT);
				}else{
					issueType = ReportIssueType.valueOf(request.getIssueType());
				}
			}
			if(locationMandatory){
				boolean locationValidated = false;
				if(request.getLocation() != null){
					if(!StringUtil.isEmpty(request.getLocation().getAddressShort())){
						locationValidated = true;
					}
				}
				
				if(!locationValidated){
					String msg = ResourceUtil.get("validation.mandatory", request.getLocale(), ResourceUtil.get("field.address", request.getLocale()));
					throw new ServiceException(msg, APIResponse.ERROR_CODE_INVALID_INPUT);
				}
			}
			
			CaseReport report = new CaseReport();
			
			report.setUserEmail(person.getEmail());
			report.setUserName(person.getName());
			report.setUserMobileNumber(person.getMobilePhone());
			report.setStatus(CRMReportStatus.NEW);
			report.setReportType(reportType);
			report.setIssueType(issueType);
			report.setChannel(request.getChannel());
			report.setClientOs(request.getClientOs());
			report.setDescription(request.getDescription());
			report.setStation(request.getStation());
			report.setContractAccNo(request.getContractAccNo());
			
			if(!StringUtil.isEmpty(request.getReportCategory())){
				report.setReportCategory(
						ReportCategory.valueOf(request.getReportCategory()));
			}
			
			if(request.getLocation() != null){
				report.setLatitude(request.getLocation().getLatitude());
				report.setLongitude(request.getLocation().getLongitude());
				report.setAddress(StringUtil.getString(request.getLocation().getAddressShort(), 120));
			}
			
			if(request.getPhotos() != null && request.getPhotos().length > 0){
				
				int count = 1;
				
				for(Photo photo : request.getPhotos()){
					
					if(count == 1){
						report.setPhoto1type(photo.getDataType());
						report.setPhoto1(CryptoUtil.decodeBase64(photo.getData()));
					}else if(count == 2){
						report.setPhoto2type(photo.getDataType());
						report.setPhoto2(CryptoUtil.decodeBase64(photo.getData()));
					}else if(count == 3){
						report.setPhoto3type(photo.getDataType());
						report.setPhoto3(CryptoUtil.decodeBase64(photo.getData()));
					}
					
					count++;
				}
			}
			
			try {
				db.beginTransaction();
				
				CaseReport insertedReport = db.insert(report);
				db.commit();
				
				response.setTransId(String.format("%06d", insertedReport.getTransId()));
				response.setTransStatus(insertedReport.getStatus().getTransactionStatus().toString());
				response.setTransDatetime(insertedReport.getCreatedDatetime());
				
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			} catch (DatabaseFacadeException e) {
				logger.error("Error create new report", e);
				ServiceException ex = new ServiceException(e, 
						ResourceUtil.get("error.general_error_database", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_SERVER_EXCEPTION);
				throw ex;
			}
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		}finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("Make a report - Task Id: ");
		if(response != null)
			sb.append(response.getTransId());
		return sb.toString();
	}
}
