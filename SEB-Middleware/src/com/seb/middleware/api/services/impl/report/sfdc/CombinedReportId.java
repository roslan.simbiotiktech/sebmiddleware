package com.seb.middleware.api.services.impl.report.sfdc;

import com.seb.middleware.constant.ReportRecordType;

public class CombinedReportId {

	private ReportRecordType type;
	
	private long transId;

	public CombinedReportId(ReportRecordType type, long transId) {
		super();
		this.type = type;
		this.transId = transId;
	}

	public ReportRecordType getType() {
		return type;
	}

	public void setType(ReportRecordType type) {
		this.type = type;
	}

	public long getTransId() {
		return transId;
	}

	public void setTransId(long transId) {
		this.transId = transId;
	}
}
