package com.seb.middleware.api.services.impl.report.sfdc;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.impl.sfdc.CaseUpdateService;
import com.seb.middleware.api.services.request.report.HistoryListingRequest;
import com.seb.middleware.api.services.response.Location;
import com.seb.middleware.api.services.response.report.HistoryListingResponse;
import com.seb.middleware.api.services.response.report.Report;
import com.seb.middleware.constant.NewTransStatus;
import com.seb.middleware.constant.ReportRecordType;
import com.seb.middleware.constant.ReportType;
import com.seb.middleware.constant.SFDCReportStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.CombinedReport;
import com.seb.middleware.jpa.helper.ReportHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.salesforce.api.QueryComplaintCaseRequest;
import com.seb.middleware.salesforce.auth.AuthenticationException;
import com.seb.middleware.salesforce.auth.Token;
import com.seb.middleware.salesforce.core.CompositeResponse;
import com.seb.middleware.salesforce.core.HTTPException;
import com.seb.middleware.salesforce.core.SFUtility;
import com.seb.middleware.salesforce.core.SalesForceCompositeConnector;
import com.seb.middleware.utility.ResourceUtil;
import com.seb.middleware.utility.StringUtil;

@Service(name = "sfdc_history_listing", publicService = true, auditable = true)
public class HistoryListingService extends SfdcReportAbstractService<HistoryListingRequest, HistoryListingResponse> {

	private static final Logger logger = LogManager.getLogger(HistoryListingService.class);
	
	public HistoryListingService(HistoryListingRequest request) {
		super(request);
	}

	@Override
	public HistoryListingResponse perform() throws ServiceException {
		HistoryListingResponse response = new HistoryListingResponse();
		
		if(StringUtil.isEmpty(request.getCaseNumber())) {
			DatabaseFacade db = new DatabaseFacade();
			
			try{
				ReportHelper helper = new ReportHelper(db);
				List<CombinedReport> reports = helper.getCombinedReport(request.getEmail());
				
				if(reports != null && !reports.isEmpty()){
					response.setReports(new Report[reports.size()]);
					int i = 0;
					
					for(CombinedReport report : reports){
						Report r = new Report();
						
						r.setTransId(getTransactionId(report));
						r.setTransStatus(getTransactionStatus(report));
						r.setTransDatetime(report.getCreatedDatetime());
						r.setType(report.getReportType().toString());
						r.setCaseId(report.getCaseNumber());
						r.setRemark(report.getRemark());
						r.setRecordType(report.getReportRecordType().toString());
						
						Location location = new Location();
						location.setLatitude(report.getLatitude());
						location.setLongitude(report.getLongitude());
						location.setAddress(report.getAddress());
						r.setLocation(location);
						
						response.getReports()[i] = r;
						i++;
					}
				}
				
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			}finally{
				db.close();
			}
		}else {
			DatabaseFacade db = new DatabaseFacade();
			
			try{
				ReportHelper helper = new ReportHelper(db);
				List<CombinedReport> reports = helper.getCombinedReportByCaseNumber(request.getCaseNumber());
				
				if(reports != null && !reports.isEmpty()){
					
					CombinedReport report = reports.get(0);
					
					Report r = new Report();
					
					r.setTransStatus(getTransactionStatus(report));
					r.setTransDatetime(report.getCreatedDatetime());
					r.setType(report.getReportType().toString());
					r.setCaseId(report.getCaseNumber());
					r.setRecordType(report.getReportRecordType().toString());
					
					if(report.getUserEmail().equalsIgnoreCase(request.getEmail())) {
						r.setTransId(getTransactionId(report));
						r.setRemark(report.getRemark());
						
						Location location = new Location();
						location.setLatitude(report.getLatitude());
						location.setLongitude(report.getLongitude());
						location.setAddress(report.getAddress());
						r.setLocation(location);
					}
					
					response.setReports(new Report[] {r});
				}else {
					SettingHelper settingHelper = new SettingHelper(db);
					
					Token token = SFUtility.getToken(settingHelper);
					
					QueryComplaintCaseRequest r = new QueryComplaintCaseRequest(request.getCaseNumber());
					SalesForceCompositeConnector connector = new SalesForceCompositeConnector(token);
					
					CompositeResponse res = connector.invoke(r);
					logger.debug("query case api sent to salesforce");
					
					int size = res.getBody().get("totalSize").getAsInt();
					logger.debug("found records {}", size);
					if(size > 0) {
						
						response.setReports(new Report[1]);
						
						JsonArray records = res.getBody().get("records").getAsJsonArray();
						JsonObject found = records.get(0).getAsJsonObject();
						
						String classification = found.get("Classification__c").getAsString();
						String category = found.get("Category__c").getAsString();
						
						ReportType rt = getConvertedReportType(classification, category);
						
						String subStatus = found.get("Sub_Status__c").isJsonNull() ? null : found.get("Sub_Status__c").getAsString();
						String status = found.get("Status").getAsString();
						SFDCReportStatus sfdcStatus = SFDCReportStatus.valueOf(status.toUpperCase().replace(" ", "_"));
						NewTransStatus convertedStatus = CaseUpdateService.convertStatus(sfdcStatus, subStatus, null);
						
						Report report = new Report();
						
						report.setTransId(null);
						report.setTransStatus(convertedStatus != null ? convertedStatus.toString() : null);
						report.setTransDatetime(r.parseSfDateFormat(found.get("CreatedDate").getAsString()));
						report.setType(rt != null ? rt.toString() : null);
						report.setCaseId(found.get("CaseNumber").getAsString());
						report.setRemark(report.getRemark());
						report.setRecordType(ReportRecordType.SFDC.toString());
						
						response.getReports()[0] = report;
					}
				}
				
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			} catch (IOException | HTTPException | AuthenticationException | ParseException e) {
				  logger.error("Error query complaint case from SFDC", e); 
				  ServiceException ex =
				  new ServiceException(e, ResourceUtil.get("error.general_500",
				  request.getLocale()), APIResponse.ERROR_CODE_SERVER_EXCEPTION); throw ex; 
			}finally {
				db.close();
			}
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		return "Read 'Make a report' listing";
	}
}
