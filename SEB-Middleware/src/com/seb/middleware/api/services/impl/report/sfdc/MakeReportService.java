package com.seb.middleware.api.services.impl.report.sfdc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.Photo;
import com.seb.middleware.api.services.request.report.sfdc.MakeReportRequest;
import com.seb.middleware.api.services.response.report.MakeReportResponse;
import com.seb.middleware.constant.NewTransStatus;
import com.seb.middleware.constant.ReportCategory;
import com.seb.middleware.constant.ReportIssueType;
import com.seb.middleware.constant.ReportRecordType;
import com.seb.middleware.constant.ReportType;
import com.seb.middleware.constant.SFDCReportStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.Contract;
import com.seb.middleware.jpa.dao.CrmUserProfile;
import com.seb.middleware.jpa.dao.SFDCReport;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.ldap.LDAPConnector;
import com.seb.middleware.ldap.LDAPErrorHandler;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.salesforce.api.CreateAccountRequestInfo;
import com.seb.middleware.salesforce.api.CreateCaseGetCaseNumberRequest;
import com.seb.middleware.salesforce.api.CreateCaseGetContactRequest;
import com.seb.middleware.salesforce.api.CreateCaseImageRequest;
import com.seb.middleware.salesforce.api.CreateCaseRequest;
import com.seb.middleware.salesforce.api.CreateContractRequest;
import com.seb.middleware.salesforce.api.QueryAccountRequestInfo;
import com.seb.middleware.salesforce.api.QueryContractRequest;
import com.seb.middleware.salesforce.api.UpdateAccountRequestInfo;
import com.seb.middleware.salesforce.auth.AuthenticationException;
import com.seb.middleware.salesforce.auth.Token;
import com.seb.middleware.salesforce.core.CompositeRequest;
import com.seb.middleware.salesforce.core.CompositeResponse;
import com.seb.middleware.salesforce.core.HTTPException;
import com.seb.middleware.salesforce.core.QueryAccountRequestResponseInfo;
import com.seb.middleware.salesforce.core.SFUtility;
import com.seb.middleware.salesforce.core.SalesForceCompositeConnector;
import com.seb.middleware.utility.CryptoUtil;
import com.seb.middleware.utility.ResourceUtil;
import com.seb.middleware.utility.StringUtil;

@Service(name = "sfdc_make_report", publicService = true, auditable = true)
public class MakeReportService extends SfdcReportAbstractService<MakeReportRequest, MakeReportResponse> {

	private static final Logger logger = LogManager.getLogger(MakeReportService.class);
	
	private MakeReportResponse response;
	
	public MakeReportService(MakeReportRequest request) {
		super(request);
	}

	@Override
	public MakeReportResponse perform() throws ServiceException {
		response = new MakeReportResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		try{
			MemberHelper userHelper = new MemberHelper(db);
			SubscriptionHelper sHelper = new SubscriptionHelper(db);
			SettingHelper settingHelper = new SettingHelper(db);
			
			ReportType reportType = ReportType.valueOf(request.getReportType());
			Person person = userHelper.getPerson(request.getEmail());
			//added by pramod for Nric manipulation
			if((person.getNricPassport()!=null)&&(person.getNricPassport().length()!=0)&&(!person.getNricPassport().isEmpty()) ) {
				String regex = "[0-9]+";
				boolean verifyNumeric =true;
				verifyNumeric=person.getNricPassport().matches(regex);
				if((person.getNricPassport().length()==12)&&(verifyNumeric==true)) {
					logger.info("NricPassport >> " + person.getNricPassport());
					String NricPassport=	this.set14CharNricNumber(person.getNricPassport());
					LDAPConnector ldap = new LDAPConnector();
					person.setNricPassport(NricPassport);
					ldap.updateUser(person);
					logger.info("NricPassport LDAP updated ");
					person= userHelper.getPerson(request.getEmail());
					logger.info("get NricPassport LDAP updated value " +person.getNricPassport());
				}else {
					logger.info("NricPassport length not matched with 12 digit and and numric >> "+person.getNricPassport());
				}
			}
			//added by pramod for MobilePhone manipulation
			if((person.getMobilePhone()!=null)&&(person.getMobilePhone().length()!=0)&&(!person.getMobilePhone().isEmpty()) ) {				
				logger.info("phoneNumber >> " +person.getMobilePhone());
				String phoneNumber=person.getMobilePhone();
				String intitialDigit=StringUtils.substring(phoneNumber, 0, 1);
				logger.info("intitialDigit phoneNumber  >> "+intitialDigit);
				String phoneNumberFinal="";
				// number  starts 0 and  9,10 digit format 
				 if( (intitialDigit.equals("0")) &&((phoneNumber.length()==10) || (phoneNumber.length()==11))) {
					 phoneNumberFinal =this.zeroInitialPhoneNumber(phoneNumber);					
					 logger.info("phoneNumberFinal >>" +phoneNumberFinal);
					// number  starts 1 and  10,11 digit format
				 }else if((intitialDigit.equals("1")) &&((phoneNumber.length()==9) || (phoneNumber.length()==10))) {
					 phoneNumberFinal =this.oneInitialPhoneNumber(phoneNumber);					
					 logger.info("phoneNumberFinal >>" +phoneNumberFinal);
				 }	
				 //LDAP Updation
				 if((phoneNumberFinal!=null)&&(phoneNumberFinal.length()!=0)&&(!phoneNumberFinal.isEmpty())) {				 
					  LDAPConnector ldap = new LDAPConnector(); 
					  person.setMobilePhone(phoneNumberFinal);
					  ldap.updateUser(person); 
					  logger.info("MobilePhone LDAP updated "); 
					  person=userHelper.getPerson(request.getEmail());
					  logger.info("get MobilePhone LDAP updated value " +person.getMobilePhone());				 
				 }else {
					 logger.info("Number validation failed  >> " +phoneNumber);
				 }
			}						
			User user = userHelper.getUser(request.getEmail());
			Contract contract = null;
			
			if(user == null) {
				logger.error("user is null in sfdc make report");
				throw new ServiceException(ResourceUtil.get("error.general_404", request.getLocale()), APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			}else if(!StringUtil.isEmpty(request.getContractAccNo())) {
				contract = sHelper.getContractAccount(request.getContractAccNo());
				if(contract == null) {
					logger.error("contract is null in sfdc make report");
					throw new ServiceException(ResourceUtil.get("error.general_404", request.getLocale()), APIResponse.ERROR_CODE_SERVER_EXCEPTION);
				}
			}
			
			boolean locationMandatory = false;
			boolean issueTypeMandatory = false;
			switch(reportType){
				case FAULTY_STREET_LIGHT:
					locationMandatory = true;
					break;
				case OUTAGE:
					locationMandatory = true;
					break;
				case TECHNICAL_ISSUE:
					break;
				case BILLING_AND_METER:
					issueTypeMandatory = true;
					if(StringUtil.isEmpty(request.getContractAccNo())){
						throw new ServiceException(
								ResourceUtil.get("error.report_billing_meter_contract_acc_no_mandatory", 
										request.getLocale()), 
										APIResponse.ERROR_CODE_INVALID_INPUT);
					}
					
					break;
				case GENERAL_INQUIRY:
					if(StringUtil.isEmpty(request.getReportCategory())){
						throw new ServiceException(
								ResourceUtil.get("validation.invalid_report_category", 
										request.getLocale()), 
										APIResponse.ERROR_CODE_INVALID_INPUT);
					}
					break;
			}

			ReportIssueType issueType = null;
			if(issueTypeMandatory){
				if(StringUtil.isEmpty(request.getIssueType())){
					String msg = ResourceUtil.get("validation.mandatory", request.getLocale(), ResourceUtil.get("field.issue_type", request.getLocale()));
					throw new ServiceException(msg, APIResponse.ERROR_CODE_INVALID_INPUT);
				}else{
					issueType = ReportIssueType.valueOf(request.getIssueType());
				}
			}
			if(locationMandatory){
				boolean locationValidated = false;
				if(request.getLocation() != null){
					if(!StringUtil.isEmpty(request.getLocation().getAddressShort())){
						locationValidated = true;
					}
				}
				
				if(!locationValidated){
					String msg = ResourceUtil.get("validation.mandatory", request.getLocale(), ResourceUtil.get("field.address", request.getLocale()));
					throw new ServiceException(msg, APIResponse.ERROR_CODE_INVALID_INPUT);
				}
			}
			
			SFDCReport report = new SFDCReport();
			
			report.setUserEmail(person.getEmail());
			report.setUserName(person.getName());
			report.setUserMobileNumber(person.getMobilePhone());
			report.setStatus(SFDCReportStatus.NEW);
			report.setSebStatus(NewTransStatus.OPEN);
			report.setReportType(reportType);
			report.setClassification(reportType.getClassification());
			report.setIssueType(issueType);
			report.setChannel(request.getChannel());
			report.setClientOs(request.getClientOs());
			report.setDescription(request.getDescription());
			report.setStation(request.getStation());
			report.setContractAccNo(request.getContractAccNo());
			
			if(!StringUtil.isEmpty(request.getReportCategory())){
				report.setReportCategory(ReportCategory.valueOf(request.getReportCategory()));
			}else {
				report.setReportCategory(ReportCategory.COMPLAINT);
			}
			
			if(request.getLocation() != null){
				report.setLatitude(request.getLocation().getLatitude());
				report.setLongitude(request.getLocation().getLongitude());
				report.setAddress(StringUtil.getString(request.getLocation().getAddressShort(), 100));
			}
			
			if(request.getPhotos() != null && request.getPhotos().length > 0){
				
				int count = 1;
				
				for(Photo photo : request.getPhotos()){
					
					if(count == 1){
						report.setPhoto1type(photo.getDataType());
						report.setPhoto1(CryptoUtil.decodeBase64(photo.getData()));
					}else if(count == 2){
						report.setPhoto2type(photo.getDataType());
						report.setPhoto2(CryptoUtil.decodeBase64(photo.getData()));
					}else if(count == 3){
						report.setPhoto3type(photo.getDataType());
						report.setPhoto3(CryptoUtil.decodeBase64(photo.getData()));
					}
					
					count++;
				}
			}
			
			try {
				
				Token token = SFUtility.getToken(settingHelper);
				SalesForceCompositeConnector connector = new SalesForceCompositeConnector(token);
				// for person account with CRM system implementation
				if(user != null) {
					QueryAccountRequestInfo queryAccountRequest=new QueryAccountRequestInfo(user.getUserId(), person.getMobilePhone(), person.getNricPassport(),
							user.getUserId(), user.getUserId(), user.getUserId(), person.getMobilePhone(), person.getMobilePhone(), person.getMobilePhone());											
					CompositeResponse queryResp = connector.invoke(queryAccountRequest);	
					
					if((!StringUtil.isEmpty(user.getSfdcId()))||(StringUtil.isEmpty(user.getSfdcId())&&queryResp.getQueryAccountInfo())) {
	
						logger.info("user already had the sfdcID :: "+user.getSfdcId());
					
								logger.info("CRM Query Responded with  Exists SEB Cares profile");
								// already profile created both sides
								QueryAccountRequestResponseInfo queryAccountRequestResponseInfo=null;
								Boolean crmProdileAvailableInd= this.getCrmAllThreeProfileExists(queryResp.getFirstObjectId());
								QueryAccountRequestResponseInfo queryAccountResponsePairs=queryResp.getQueryAccountResponseInfo();
								if(crmProdileAvailableInd) {
									logger.info("Already exists CRM profiles with less than three in SEB Cares and Sales Force");
									queryAccountRequestResponseInfo=this.getNotExistsAllThreeCrmProfilesRequestBody(person,queryAccountResponsePairs);
								}else {
									QueryAccountRequestResponseInfo	 queryAccountResponseInfo	=queryResp.getQueryAccountResponseInfo();
									logger.info("Already exists CRM profiles equal or more than three in SEB Cares and Sales Force");
									queryAccountRequestResponseInfo	=this.setPairToUpdateNewProfileInCrm(person,queryAccountResponseInfo,queryResp.getFirstObjectId());	
								}							
								//if it's not matched need through error message to UI 
								CompositeResponse compositeResponseUpdateProfileExists=null;
								
								compositeResponseUpdateProfileExists=	this.updateAccountRequestInfoWithExistingProfiles(person,queryAccountRequestResponseInfo,queryResp.getFirstObjectId());
							
								
								if(compositeResponseUpdateProfileExists.getHttpStatusCode()==204) {	
									
									Boolean userCrmUpdation= this.crmUserUpdateWithNewSfdcid(user.getSfdcId(), queryResp.getFirstObjectId(), queryAccountRequestResponseInfo);								
									if(userCrmUpdation) {
										logger.info("sfdcId  done Crm user updation : " + queryResp.getFirstObjectId());
									}else {
										logger.info("sfdcId  failed Crm user updation: " + queryResp.getFirstObjectId());
									}
									user.setSfdcUpdatedDate(new Date());
									user.setSfdcId(queryResp.getFirstObjectId());
								}else {
									logger.info("sfdcId  done Crm user updation is failed : " + compositeResponseUpdateProfileExists.getFirstObjectId());
								}													
							}else {
						/// for creating the account
							CreateAccountRequestInfo createAccountRequest = new CreateAccountRequestInfo(user.getUserId(), person.getName(),
									person.getMobilePhone(), person.getEmail(), person.getNricPassport(), 
									settingHelper.getSalesForceAccountRecordTypeId(),person.getEmail(),person.getMobilePhone());
							CompositeResponse resp = connector.invoke(createAccountRequest);
							if(resp.isSuccess()) {
								
								user.setSfdcId(resp.getObjectId());
								user.setSfdcSyncDate(new Date());
								
								CrmUserProfile crmUserProfile=new CrmUserProfile();
								crmUserProfile.setEmail_1(person.getEmail());
								crmUserProfile.setMobile_1(person.getMobilePhone());
								crmUserProfile.setSfdcId(resp.getObjectId());
								crmUserProfile.setSfdcUpdatedDate1(new Date());
								Boolean crmProfileInsertion =this.crmUserInsertion(crmUserProfile);
								if(crmProfileInsertion) {
									logger.info("crm User Insertion  done : " + request.getEmail());
								}else {
									logger.info("crm User Insertion failed : " + request.getEmail());
								}
							}
							else {
								logger.info("Error occured while creating the profile in CRM : " + user.getUserId());
							}
						}
				  
				}
				

				// for contract Account creation in CRM system
				if(contract != null && StringUtil.isEmpty(contract.getSfdcId())) {
					
					QueryContractRequest qc = new QueryContractRequest(contract.getContractAccountNumber());
					CreateContractRequest cc = new CreateContractRequest(contract.getContractAccountNumber(), 
							contract.getContractAccountName(), contract.getAgencyName());
					
					SalesForceCompositeConnector connectorContract = new SalesForceCompositeConnector(token);
					CompositeResponse qcresp = connectorContract.invoke(qc);
					
					if(qcresp.isSuccess()) {
						contract.setSfdcId(qcresp.getFirstObjectId());
					}
					
					// need to add Update CA Account number
					
					if(StringUtil.isEmpty(contract.getSfdcId())) {
						CompositeResponse resp = connectorContract.invoke(cc);
						if(resp.isSuccess()) {
							contract.setSfdcId(resp.getObjectId());
						}
					}
					
					if(!StringUtil.isEmpty(contract.getSfdcId())) {
						db.beginTransaction();
						db.update(contract);
						db.commit();
					}
				}
				
				if(user != null && StringUtil.isEmpty(user.getSfdcId())) {
					logger.error("user sfdc id is null sfdc make report");
					throw new ServiceException(ResourceUtil.get("error.general_500", request.getLocale()), APIResponse.ERROR_CODE_SERVER_EXCEPTION);
				} 
				//system should allaow create case with out CA Number(added by pramod)
				
				/*
				 * else if (contract != null && StringUtil.isEmpty(contract.getSfdcId())) {
				 * logger.error("contract sfdc id is null sfdc make report"); throw new
				 * ServiceException(ResourceUtil.get("error.general_500", request.getLocale()),
				 * APIResponse.ERROR_CODE_SERVER_EXCEPTION); }
				 */					 
				
				db.beginTransaction();
				
				SFDCReport insertedReport = db.insert(report);
				db.commit();
				// for case  creation in CRM system
				if(insertedReport!=null) {
					logger.info("inserted SFDCReport get TransId >> " +insertedReport.getTransId());
					CreateCaseGetContactRequest cr = new CreateCaseGetContactRequest(user.getSfdcId());
					
					CreateCaseRequest cc = new CreateCaseRequest(insertedReport.getTransId(), 
							user.getSfdcId(), contract == null ? null : contract.getSfdcId(), 
							insertedReport.getChannel(), insertedReport.getClassification(),
							insertedReport.getReportType(), insertedReport.getIssueType(),
							insertedReport.getReportCategory(), insertedReport.getDescription(), 
							insertedReport.getCreatedDatetime(), insertedReport.getAddress(), 
							insertedReport.getLatitude(), insertedReport.getLongitude(),
							cr.getReferenceId(), settingHelper.getSalesForceCaseRecordTypeId());
					
					CreateCaseGetCaseNumberRequest cn = new CreateCaseGetCaseNumberRequest(cc.getReferenceId());
					
					SalesForceCompositeConnector connectorReport = new SalesForceCompositeConnector(token);
					
					Map<CompositeRequest, CompositeResponse> resps = connectorReport.invoke(cr, cc, cn);
					logger.debug("create case api sent to salesforce");
					
					CompositeResponse resp = resps.get(cc);
					CompositeResponse respCaseNumber = resps.get(cn);
					
					if(resp.isSuccess() && respCaseNumber.isSuccess()) {

						String caseNumber = respCaseNumber.getBody().get("CaseNumber").getAsString();
						
						insertedReport.setCaseNumber(caseNumber);
						insertedReport.setCaseId(resp.getObjectId());
						
						if(request.getPhotos() != null && request.getPhotos().length > 0){
							
							connectorReport.setLogRequest(settingHelper.isTestingEnvironment());
							
							List<CompositeRequest> createCaseImageRequests = new ArrayList<CompositeRequest>();
							
							int index = 1;
							
							for(Photo photo : request.getPhotos()){
								
								CreateCaseImageRequest request = new CreateCaseImageRequest(
										insertedReport.getCaseId(), index, photo.getDataType(), photo.getData());
								
								CompositeRequest[] list = request.getRequests(); 
								for(CompositeRequest r : list) {
									createCaseImageRequests.add(r);
								}
								
								index++;
							}
							
							connectorReport.invoke(createCaseImageRequests);
							logger.debug("create case image api sent to salesforce");
						}
						db.beginTransaction();
						db.update(insertedReport);
						
						db.commit();
						
						response.setCaseId(insertedReport.getCaseNumber());
						response.setTransId(getTransactionId(ReportRecordType.SFDC, insertedReport.getTransId()));
						response.setTransStatus(insertedReport.getSebStatus().toString());
						response.setTransDatetime(insertedReport.getCreatedDatetime());
						
						response.getResponseStatus().setStatus(SUCCESS);
						
						}
				
			
					return response;
					
				}else {
					db.rollback();
					logger.error("Error create new report into SFDC"); 
					ServiceException ex =
							  new ServiceException(ResourceUtil.get("error.general_500",
							  request.getLocale()), APIResponse.ERROR_CODE_SERVER_EXCEPTION); 
					throw ex; 
				}		
							
				}
		
			 catch (DatabaseFacadeException e) {
				logger.error("Error create new report", e);
				ServiceException ex = new ServiceException(e, 
						ResourceUtil.get("error.general_error_database", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_SERVER_EXCEPTION);
				throw ex;
			} catch (IOException | HTTPException | AuthenticationException e) {
				  logger.error("Error create new report into SFDC", e); 
				  ServiceException ex =
				  new ServiceException(e, ResourceUtil.get("error.general_500",
				  request.getLocale()), APIResponse.ERROR_CODE_SERVER_EXCEPTION); throw ex; 
			}	 
		} catch (NamingException e) {
			ServiceException ex = LDAPErrorHandler.processException(e, request.getLocale());
			throw ex;
		}finally{
			db.close();
		}
		
}

	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("Make a SFDC report - Task Id: ");
		if(response != null)
			sb.append(response.getTransId());
		return sb.toString();
	}
	
	//added by pramod	
	public String set14CharNricNumber(String Nric) {
		String finalNric="";
		logger.info("NricPassport length >> " + Nric.length());
		if(Nric.length()==12) {
			String sixCharacters=StringUtils.substring(Nric, 0, 6);
			String middleTwoCharacters=StringUtils.substring(Nric, 6, 8);
			String lastFourCharacters=StringUtils.substring(Nric, 8, 12);
			finalNric=sixCharacters+"-"+middleTwoCharacters+"-"+lastFourCharacters;
			logger.info("Final NRIC Number >> "+finalNric);
			logger.info("Final NRIC Number length >> "+finalNric.length());
		}	
		return finalNric;
	}

	
	// Crm user sfdcID Insert
	public Boolean crmUserInsertion(CrmUserProfile crmUserProfile) {
		logger.info("call >> crmUserInsertion(CrmUserProfile crmUserProfile) ");
		Boolean  userCrmInsertion=false;
		
		if(crmUserProfile != null){
			DatabaseFacade dbCrmUser = new DatabaseFacade();
			try {
				dbCrmUser.beginTransaction();
				dbCrmUser.insert(crmUserProfile);
				dbCrmUser.commit();
			} catch (DatabaseFacadeException e1) {
				logger.error("Error while reverting DB user creation.", e1);
			} finally {
				dbCrmUser.close();
				userCrmInsertion=true;
			}
		}
		return userCrmInsertion;
	}
	
	

	
	
	// Crm user sfdcID Updation
	public Boolean crmUserUpdateWithNewSfdcid(String oldestSfdcID,String newSfdcid,QueryAccountRequestResponseInfo queryAccountResponseInfo) throws DatabaseFacadeException {
	logger.info("Updating the CRM Table");
		Boolean  userCrmUpdation=false;
		DatabaseFacade dbGetCrmUser = new DatabaseFacade();
		MemberHelper userHelper = new MemberHelper(dbGetCrmUser);
		
		logger.info("call >> 	getCrmUserProfileWithSfdcID(String sfdcId) ");	
		
		logger.info("Get CRM table sfdcID >>> "+ newSfdcid);
		
		CrmUserProfile crmUserProfiles = userHelper.getCrmUserProfileWithSfdcID(newSfdcid);
		if(crmUserProfiles != null){	
			try {					
				logger.info("Got CRM table record with sfdcID >>> "+ newSfdcid);
				if(queryAccountResponseInfo.getSebcEmailOne()!=null && queryAccountResponseInfo.getSebcMobileOne()!=null) {
					crmUserProfiles.setEmail_1(queryAccountResponseInfo.getSebcEmailOne());
					crmUserProfiles.setMobile_1(queryAccountResponseInfo.getSebcMobileOne());
					crmUserProfiles.setSfdcId(newSfdcid);
					crmUserProfiles.setSfdcUpdatedDate1(new Date());
				}else if(queryAccountResponseInfo.getSebcEmailTwo()!=null && queryAccountResponseInfo.getSebcMobileTwo()!=null) {
					crmUserProfiles.setEmail_2(queryAccountResponseInfo.getSebcEmailTwo());
					crmUserProfiles.setMobile_2(queryAccountResponseInfo.getSebcMobileTwo());
					crmUserProfiles.setSfdcId(newSfdcid);
					crmUserProfiles.setSfdcUpdatedDate2(new Date());
				}else if(queryAccountResponseInfo.getSebcEmailThree()!=null && queryAccountResponseInfo.getSebcMobileThree()!=null) {
					crmUserProfiles.setEmail_3(queryAccountResponseInfo.getSebcEmailThree());
					crmUserProfiles.setMobile_3(queryAccountResponseInfo.getSebcMobileThree());
					crmUserProfiles.setSfdcId(newSfdcid);
					crmUserProfiles.setSfdcUpdatedDate3(new Date());
				}
				dbGetCrmUser.beginTransaction();
				dbGetCrmUser.update(crmUserProfiles);
				dbGetCrmUser.commit();
			} catch (DatabaseFacadeException e1) {
				logger.error("Error while reverting DB CrmUserProfiles Updation.", e1);
				 e1.printStackTrace();
			} finally {
				dbGetCrmUser.close();
				userCrmUpdation=true;
			}
		}
		return userCrmUpdation;
	}
	

	public QueryAccountRequestResponseInfo setPairToUpdateNewProfileInCrm(Person person,QueryAccountRequestResponseInfo queryAccountResponseInfo,String queryResSfdcId){
		logger.info("call >> setPairToUpdateNewProfileInCrm(Person person,QueryAccountRequestResponseInfo queryAccountResponseInfo,String oldSfdcId)");
		QueryAccountRequestResponseInfo queryAccountRequestInfo=new QueryAccountRequestResponseInfo();
		
		DatabaseFacade dbGetCrmUser = new DatabaseFacade();
		
		MemberHelper userHelper = new MemberHelper(dbGetCrmUser);
		
		try {
			dbGetCrmUser.beginTransaction();
			logger.info("call >> 	getCrmUserProfileWithSfdcID(String sfdcId) ");	
			CrmUserProfile crmUserProfiles = userHelper.getCrmUserProfileWithSfdcID(queryResSfdcId);
			// need to get clarification if record is not available in CRMProfile table
			
			LinkedHashMap<String, String> emailPhone=new LinkedHashMap<String, String>();
			
			if(queryAccountResponseInfo.getSebcEmailOne()!=null && queryAccountResponseInfo.getSebcMobileOne()!=null) {
				logger.info("putting first pair :"+queryAccountResponseInfo.getSebcEmailOne(),queryAccountResponseInfo.getSebcMobileOne());
				
				emailPhone.put(queryAccountResponseInfo.getSebcEmailOne(),queryAccountResponseInfo.getSebcMobileOne() );	
			}
			if(queryAccountResponseInfo.getSebcEmailTwo()!=null && queryAccountResponseInfo.getSebcMobileTwo()!=null) {
				emailPhone.put(queryAccountResponseInfo.getSebcEmailTwo(), queryAccountResponseInfo.getSebcMobileTwo());
				logger.info("putting second pair :"+queryAccountResponseInfo.getSebcEmailTwo(), queryAccountResponseInfo.getSebcMobileTwo());
			}
			if(queryAccountResponseInfo.getSebcEmailThree()!=null && queryAccountResponseInfo.getSebcMobileThree()!=null) {
				emailPhone.put(queryAccountResponseInfo.getSebcEmailThree(), queryAccountResponseInfo.getSebcMobileThree());
				logger.info("putting third pair :"+queryAccountResponseInfo.getSebcEmailThree(), queryAccountResponseInfo.getSebcMobileThree());
			}
							
			Boolean crmProfilePairMatchedIndOfQueryResponse=false;
			Boolean personPairMatchedIndOfQueryResponse=false;				
				if(person.getEmail().equals(queryAccountResponseInfo.getSebcEmailOne()) && person.getMobilePhone().equals(queryAccountResponseInfo.getSebcMobileOne()) ){
					queryAccountRequestInfo.setSebcEmailOne(person.getEmail());
					queryAccountRequestInfo.setSebcMobileOne(person.getMobilePhone());
					logger.info("Pair number one matched  Person Matched Email : " + person.getEmail() + ", PhoneNumber: " + person.getMobilePhone());
					personPairMatchedIndOfQueryResponse=true;
					}	
				else if(person.getEmail().equals(queryAccountResponseInfo.getSebcEmailTwo()) && person.getMobilePhone().equals(queryAccountResponseInfo.getSebcMobileTwo()) ){
					queryAccountRequestInfo.setSebcEmailTwo(person.getEmail());
					queryAccountRequestInfo.setSebcMobileTwo(person.getMobilePhone());
					personPairMatchedIndOfQueryResponse=true;
					logger.info("Pair  number two matched  Person Matched Email : " + person.getEmail() + ", PhoneNumber: " + person.getMobilePhone());
					}
				else if(person.getEmail().equals( queryAccountResponseInfo.getSebcEmailThree()) && person.getMobilePhone().equals(queryAccountResponseInfo.getSebcMobileThree()) ){
						queryAccountRequestInfo.setSebcEmailThree(person.getEmail());
						queryAccountRequestInfo.setSebcMobileThree(person.getMobilePhone());
						logger.info("Pair  number three matched  Person Matched Email : " + person.getEmail() + ", PhoneNumber: " + person.getMobilePhone());
						personPairMatchedIndOfQueryResponse=true;
					}
			
			
				
				
				if(!personPairMatchedIndOfQueryResponse) {
				logger.info("person Pair not Matched  with QueryResponse pair");
				
				
				// check login person matched with CRM profiletable
				
				Boolean loginProfileMatchWithCrmTable =this.getLoginProfileMatchWithCrmTable(crmUserProfiles,person);
				
				if(loginProfileMatchWithCrmTable) {
					logger.info("person Pair  Matched  with QueryResponse pair : " +loginProfileMatchWithCrmTable);
					Boolean firstSet=false;
					Boolean SecondSet=false;
					Boolean ThirdSet=false;
					
					int i=1;
					for (Map.Entry<String, String> entry : emailPhone.entrySet()) {
						if(entry.getKey()!=null && entry.getValue()!=null) {								
						String k = entry.getKey();
						String v = entry.getValue();
						
						// pair 1 mapping
						if(i==1) {
								if((crmUserProfiles.getEmail_1().equals(entry.getKey()) &&crmUserProfiles.getMobile_1().equals(entry.getValue()))) {
									logger.info("Pair number matched "+i+"CrmUserProfile Matched Email-1 : " + k + ", PhoneNumber: " + v);
									 firstSet=true;
								}
							}else if(i==2) {		
							// pair 3 mapping
							if((crmUserProfiles.getEmail_2().equals(entry.getKey()) &&crmUserProfiles.getMobile_2().equals(entry.getValue()))) {
								logger.info("Pair number matched "+i+"CrmUserProfile Matched Email-1 : " + k + ", PhoneNumber: " + v);
								SecondSet=true;						
							}
						}else if(i==3) {
							// pair 3 mapping
								if((crmUserProfiles.getEmail_3().equals(entry.getKey()) &&crmUserProfiles.getMobile_3().equals(entry.getValue()))) {
									logger.info("Pair number matched "+i+"CrmUserProfile Matched Email-1 : " + k + ", PhoneNumber: " + v);
									ThirdSet=true;
								}	
						}
								
					}
						i++;
				}
									
					if(firstSet && !SecondSet && !ThirdSet) {
						logger.info("Setting SET-1");
						queryAccountRequestInfo.setSebcEmailOne(person.getEmail());
						queryAccountRequestInfo.setSebcMobileOne(person.getMobilePhone());
						crmProfilePairMatchedIndOfQueryResponse=true;
					}else if(!firstSet && !SecondSet && ThirdSet) {
						logger.info("Setting SET-3");
						queryAccountRequestInfo.setSebcEmailThree(person.getEmail());
						queryAccountRequestInfo.setSebcMobileThree(person.getMobilePhone());
						crmProfilePairMatchedIndOfQueryResponse=true;
						
					}else if(!firstSet && SecondSet && !ThirdSet) {
						logger.info("Setting SET-2");
						queryAccountRequestInfo.setSebcEmailTwo(person.getEmail());
						queryAccountRequestInfo.setSebcMobileTwo(person.getMobilePhone());
						crmProfilePairMatchedIndOfQueryResponse=true;
						
					}									
				}
				}
				
				
			
			if(!personPairMatchedIndOfQueryResponse && ! crmProfilePairMatchedIndOfQueryResponse) {
				logger.info("person Pair , CRM Profiel table pairs not Matched  with QueryResponse pair setting pair -1");
			Integer finalPairNumber=	this.getOldestDateCrmProfile(crmUserProfiles);
			if(finalPairNumber==1) {
				logger.info("Final Pair Number to Update  CRM"+finalPairNumber);
				queryAccountRequestInfo.setSebcEmailOne(person.getEmail());
				queryAccountRequestInfo.setSebcMobileOne(person.getMobilePhone());
			}else if(finalPairNumber==2) {
				logger.info("Final Pair Number to Update  CRM"+finalPairNumber);
				queryAccountRequestInfo.setSebcEmailTwo(person.getEmail());
				queryAccountRequestInfo.setSebcMobileTwo(person.getMobilePhone());
			}else if(finalPairNumber==3) {
				logger.info("Final Pair Number to Update  CRM"+finalPairNumber);
				queryAccountRequestInfo.setSebcEmailThree(person.getEmail());
				queryAccountRequestInfo.setSebcMobileThree(person.getMobilePhone());
			}
			
				
			}
			
		} catch (DatabaseFacadeException e1) {
			logger.error("Error while reverting DB user creation.", e1);
		} finally {
			dbGetCrmUser.close();
			
		}
		
		return queryAccountRequestInfo;
	}
	
	// updating the existig user  with CRM profiles
	public CompositeResponse updateAccountRequestInfoWithExistingProfiles(Person person,QueryAccountRequestResponseInfo updateAccountRequestResponseInfo,String newSfdcId) {
		logger.info("call >> updateAccountRequestInfoWithExistingProfiles(Person person,QueryAccountRequestResponseInfo updateAccountRequestResponseInfo,String newSfdcId) ");
		CompositeResponse compositeResponse=null;
		DatabaseFacade db = new DatabaseFacade();
		SettingHelper settingHelper = new SettingHelper(db);
		try {
			db.beginTransaction();
			Token token = SFUtility.getToken(settingHelper);			
			SalesForceCompositeConnector connector = new SalesForceCompositeConnector(token);
			UpdateAccountRequestInfo queryAccountRequest=new UpdateAccountRequestInfo(newSfdcId, person.getName(), updateAccountRequestResponseInfo.getSebcEmailOne(), 
					
					updateAccountRequestResponseInfo.getSebcEmailTwo(), updateAccountRequestResponseInfo.getSebcEmailThree(), updateAccountRequestResponseInfo.getSebcMobileOne(), updateAccountRequestResponseInfo.getSebcMobileTwo(), updateAccountRequestResponseInfo.getSebcMobileThree());
			compositeResponse = connector.invoke(queryAccountRequest);	
			db.commit();
			db.close();
		} catch (AuthenticationException|HTTPException |DatabaseFacadeException| IOException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error("error update the existing  account to salesforce which having the sfdcId, however - " + e.getMessage(), e);
		}
		return compositeResponse;
	}
	public  String zeroInitialPhoneNumber(String phoneNumber) {
		String finaNumber=phoneNumber.replaceFirst("0", "60");
		
		return finaNumber;
		
	}
	public  String oneInitialPhoneNumber(String phoneNumber) {
		String finaNumber="60"+phoneNumber;
		return finaNumber;
		
	}

	public Boolean getCrmAllThreeProfileExists(String sfdcId) {
		
		logger.info("cal >>> getCrmAllThreeProfileExists(String sfdcId) ");
		Boolean crmProdileInd=true;
		DatabaseFacade dbGetCrmUser = new DatabaseFacade();
		MemberHelper userHelper = new MemberHelper(dbGetCrmUser);
		
		Boolean firstSet=false;
		Boolean SecondSet=false;
		Boolean ThirdSet=false;
		try {
			dbGetCrmUser.beginTransaction();
			CrmUserProfile crmUserProfiles = userHelper.getCrmUserProfileWithSfdcID(sfdcId);
			if(crmUserProfiles!=null) {
				if(crmUserProfiles.getEmail_1()!=null && crmUserProfiles.getMobile_1()!=null) {
					firstSet=true;
				}
				
				if(crmUserProfiles.getEmail_2()!=null && crmUserProfiles.getMobile_2()!=null) {
					SecondSet=true;
				}
				
				if(crmUserProfiles.getEmail_3()!=null && crmUserProfiles.getMobile_3()!=null) {
					ThirdSet=true;
				}
			}
			
			if(firstSet &&  SecondSet &&ThirdSet) {
				crmProdileInd=false;
			}
			
		} catch (DatabaseFacadeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		logger.info("crmProdileInd >> "+crmProdileInd);
		
		return crmProdileInd;
		
	}
	// added by pramod for setting the getNotExistsAllThreeCrmProfilesRequestBody request parameters
	public QueryAccountRequestResponseInfo getNotExistsAllThreeCrmProfilesRequestBody(Person person,QueryAccountRequestResponseInfo queryAccountResponsePairs){
		logger.info("call >> getNotExistsAllThreeCrmProfilesRequestBody pair setting");
		QueryAccountRequestResponseInfo queryAccountRequestResponseInfo=new QueryAccountRequestResponseInfo();
		Boolean firstSet=false;
		Boolean SecondSet=false;
		Boolean ThirdSet=false;
				
				if((queryAccountResponsePairs.getSebcEmailOne()==null && queryAccountResponsePairs.getSebcMobileOne()==null)||(queryAccountResponsePairs.getSebcEmailOne().equals(person.getEmail()) &&queryAccountResponsePairs.getSebcMobileOne().equals(person.getMobilePhone()))) {
					firstSet=true;						
				}else if((queryAccountResponsePairs.getSebcEmailTwo()==null && queryAccountResponsePairs.getSebcMobileTwo()==null)||(queryAccountResponsePairs.getSebcEmailTwo().equals(person.getEmail()) &&queryAccountResponsePairs.getSebcMobileTwo().equals(person.getMobilePhone()))) {
					SecondSet=true;					
				}else if((queryAccountResponsePairs.getSebcEmailThree()==null && queryAccountResponsePairs.getSebcMobileThree()==null)||(queryAccountResponsePairs.getSebcEmailThree().equals(person.getEmail()) &&queryAccountResponsePairs.getSebcMobileThree().equals(person.getMobilePhone()))) {
					ThirdSet=true;					
				}
				if(firstSet && !SecondSet &&! ThirdSet) {
					logger.info(" First pair is matched with login profile ");
					queryAccountRequestResponseInfo.setSebcEmailOne(person.getEmail());
					queryAccountRequestResponseInfo.setSebcMobileOne(person.getMobilePhone());
				}else if(!firstSet && SecondSet && !ThirdSet) {
					logger.info("Setting pair SET-2 for updare request due to Second  pair  matched with login profile or it's having empty with CRM profile ");
					queryAccountRequestResponseInfo.setSebcEmailTwo(person.getEmail());
					queryAccountRequestResponseInfo.setSebcMobileTwo(person.getMobilePhone());
					
				}else if(!firstSet && !SecondSet && ThirdSet) {
					logger.info(" Setting pair SET-3 for updare request due to Third  pair  matched with login profile or it's having empty with CRM profile");
					queryAccountRequestResponseInfo.setSebcEmailThree(person.getEmail());
					queryAccountRequestResponseInfo.setSebcMobileThree(person.getMobilePhone());	
				}
				queryAccountRequestResponseInfo.setReturnUI(true);
		return queryAccountRequestResponseInfo;
	}
	
	public Boolean getLoginProfileMatchWithCrmTable(CrmUserProfile crmProfile,Person person) {
		logger.info("person Pair  Matched  with QueryResponse pair  activated ");
		Boolean loginProfileMatchWithCrmTable =false;
		if(crmProfile.getEmail_1().equals(person.getEmail())&&crmProfile.getMobile_1().equals(person.getMobilePhone())) {
			loginProfileMatchWithCrmTable=true;
		}else if(crmProfile.getEmail_2().equals(person.getEmail())&&crmProfile.getMobile_2().equals(person.getMobilePhone())) {
			loginProfileMatchWithCrmTable=true;
		}else if(crmProfile.getEmail_3().equals(person.getEmail())&&crmProfile.getMobile_3().equals(person.getMobilePhone())) {
			loginProfileMatchWithCrmTable=true;
		}
	return loginProfileMatchWithCrmTable;
	}
	
	public Integer getOldestDateCrmProfile(CrmUserProfile crmUserProfile) {	
		  logger.info("get oldest CRM profile number");
		int oldestProfileNumber = 0;
		Date pairone=crmUserProfile.getSfdcUpdatedDate1();
		Date pairTwo=crmUserProfile.getSfdcUpdatedDate2();
		Date pairThree=crmUserProfile.getSfdcUpdatedDate3();
		List<Date> listOfPairDates=new ArrayList<Date>();
		listOfPairDates.add(pairone); 
		listOfPairDates.add(pairTwo);
		listOfPairDates.add(pairThree); 		 
		  Collections.sort(listOfPairDates);
		  logger.info("Oldest Date desc order >> "+listOfPairDates);
		  Date oldestDate= new Date();
		  oldestDate=listOfPairDates.get(0); 
		  logger.info("Oldest Date >> "+oldestDate);
		  if (oldestDate.compareTo(pairone) == 0) {
			  oldestProfileNumber=1;
		  }else if(oldestDate.compareTo(pairTwo) == 0) {
			  oldestProfileNumber=2;
		  }else if(oldestDate.compareTo(pairThree) == 0) {
			  oldestProfileNumber=3;
		  }
		
		return oldestProfileNumber;
	}
	
}
