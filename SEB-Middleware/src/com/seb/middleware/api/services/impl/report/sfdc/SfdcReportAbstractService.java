package com.seb.middleware.api.services.impl.report.sfdc;

import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.api.services.response.Response;
import com.seb.middleware.constant.ReportIssueType;
import com.seb.middleware.constant.ReportRecordType;
import com.seb.middleware.constant.ReportType;
import com.seb.middleware.jpa.dao.CombinedReport;

public abstract class SfdcReportAbstractService<T extends Request, K extends Response> extends ServiceBase<T, K> {
	
	public SfdcReportAbstractService(T request) {
		super(request);
	}

	protected CombinedReportId extractTransactionId(String transactionId) {
		for(ReportRecordType type : ReportRecordType.values()) {
			if(transactionId.startsWith(type.getPrefix())) {
				
				return new CombinedReportId(type, 
						Long.parseLong(transactionId.replaceFirst(type.getPrefix(), "")));
				
			}
		}
		
		return null;
	}
	
	protected String getTransactionId(CombinedReport report) {
		return getTransactionId(report.getReportRecordType(), report.getTransId());
	}
	
	protected String getTransactionId(ReportRecordType type, long transId) {
		return type.getPrefix() + String.format("%06d", transId);
	}
	
	protected String getTransactionStatus(CombinedReport report) {
		if(report.getReportRecordType() == ReportRecordType.SFDC) {
			return report.getSebStatus().toString();
		}else {
			return report.getStatus().getNewTransactionStatus().toString();
		}
	}
	
	protected ReportType getConvertedReportType(String classification, String category) {
		
		if(category != null && !category.isEmpty()) {
			if(category.equalsIgnoreCase(ReportIssueType.BILLING.getName()) || 
					category.equalsIgnoreCase(ReportIssueType.METER_READING.getName())) {
				return ReportType.BILLING_AND_METER;
			}else {
				return ReportType.getByName(category);
			}
		}
		
		return null;
	}
}
