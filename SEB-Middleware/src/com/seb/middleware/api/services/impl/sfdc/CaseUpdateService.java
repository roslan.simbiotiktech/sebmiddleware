package com.seb.middleware.api.services.impl.sfdc;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.sfdc.CaseUpdateRequest;
import com.seb.middleware.api.services.response.sfdc.CaseUpdateResponse;
import com.seb.middleware.constant.NewTransStatus;
import com.seb.middleware.constant.SFDCReportStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.SFDCReport;
import com.seb.middleware.jpa.helper.ReportHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "sfdc_case_update", publicService = true, auditable = false)
public class CaseUpdateService extends SfdcService<CaseUpdateRequest, CaseUpdateResponse> {

	private static final Logger logger = LogManager.getLogger(CaseUpdateService.class);
	
	public CaseUpdateService(CaseUpdateRequest request) {
		super(request);
	}

	@Override
	public CaseUpdateResponse perform() throws ServiceException {

		validateCredential();
		
		CaseUpdateResponse response = new CaseUpdateResponse();

		DatabaseFacade db = new DatabaseFacade();
		try{
			
			ReportHelper rHelper = new ReportHelper(db);
			SFDCReport report = rHelper.getSfdcReportDetail(request.getCaseRecordId());
			
			if(report == null) {
				logger.error("sfdc report not found for case id {}", request.getCaseRecordId());
				throw new ServiceException( 
						"case_record_id '" + request.getCaseRecordId() + "' not found", APIResponse.ERROR_CODE_INVALID_INPUT);
			}else {
				SFDCReportStatus newStatus = SFDCReportStatus.valueOf(request.getStatus());
				NewTransStatus newSebStatus = convertStatus(newStatus, request.getSubStatus(), report.getSebStatus());
				logger.info("Case Number '{}'", report.getCaseNumber());
				logger.info("updating seb_status from {} to {}, sfdc status {} - '{}'", report.getSebStatus(), newSebStatus, newStatus, request.getSubStatus());
				
				report.setStatus(newStatus);
				report.setSubStatus(request.getSubStatus());
				report.setSebStatus(newSebStatus);
				report.setRemark(request.getRemarks());
				report.setUpdatedDatetime(new Date());
				
				db.beginTransaction();
				db.update(report);
				db.commit();
				
				response.setTransId(report.getTransId());
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			}
		} catch (DatabaseFacadeException e) {
			logger.error("Error updating case", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		}finally {
			db.close();
		}
	}
	
	public static NewTransStatus convertStatus(SFDCReportStatus newStatus, String subStatus, NewTransStatus originalStatus) {
		if(newStatus == SFDCReportStatus.NEW) {
			return NewTransStatus.OPEN;
		}else if(newStatus == SFDCReportStatus.ESCALATED) {
			return NewTransStatus.ESCALATED;
		}else if(newStatus == SFDCReportStatus.IN_PROGRESS) {

			if("Pending Customer Response".equalsIgnoreCase(subStatus)) {
				return NewTransStatus.PLEASE_CONTACT_SEB;
			}else {
				return NewTransStatus.ASSIGNED;
			}
			
		}else if(newStatus == SFDCReportStatus.REJECTED) {
			return NewTransStatus.IN_PROGRESS;
		}else if(newStatus == SFDCReportStatus.REOPEN) {
			
			if("Pending Customer Response".equalsIgnoreCase(subStatus)) {
				return NewTransStatus.PLEASE_CONTACT_SEB;
			}else {
				return NewTransStatus.IN_PROGRESS;
			}
			
		}else if(newStatus == SFDCReportStatus.REASSIGNED) {
			
			if("Pending Customer Response".equalsIgnoreCase(subStatus)) {
				return NewTransStatus.PLEASE_CONTACT_SEB;
			}else {
				return NewTransStatus.IN_PROGRESS;
			}
			
		}else if(newStatus == SFDCReportStatus.RESOLVED) {
			
			if("Pending Customer Response".equalsIgnoreCase(subStatus)) {
				return NewTransStatus.PLEASE_CONTACT_SEB;
			}else {
				return NewTransStatus.IN_PROGRESS;
			}
			
		}else if(newStatus == SFDCReportStatus.CLOSED) {
			return NewTransStatus.RESOLVED;
		}
		
		logger.error("unable to match status");
		return originalStatus;
	}
}
