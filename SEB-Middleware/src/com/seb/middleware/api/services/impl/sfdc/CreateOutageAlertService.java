package com.seb.middleware.api.services.impl.sfdc;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.sfdc.CreateOutageAlertRequest;
import com.seb.middleware.api.services.response.sfdc.CreateOutageAlertResponse;
import com.seb.middleware.constant.NotificationStatus;
import com.seb.middleware.constant.NotificationType;
import com.seb.middleware.constant.SFDCAlertType;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.CustServiceLocation;
import com.seb.middleware.jpa.dao.PowerAlertDao;
import com.seb.middleware.jpa.dao.PowerAlertTypeDao;
import com.seb.middleware.jpa.dao.TagNotification;
import com.seb.middleware.jpa.helper.ContentHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "sfdc_create_outage_alert", publicService = true, auditable = false)
public class CreateOutageAlertService extends SfdcService<CreateOutageAlertRequest, CreateOutageAlertResponse> {

	private static final Logger logger = LogManager.getLogger(CreateOutageAlertService.class);
	
	public CreateOutageAlertService(CreateOutageAlertRequest request) {
		super(request);
	}

	@Override
	public CreateOutageAlertResponse perform() throws ServiceException {
		
		validateCredential();
		
		CreateOutageAlertResponse response = new CreateOutageAlertResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		try {
			ContentHelper cHelper = new ContentHelper(db);
			
			SFDCAlertType sfdcType = SFDCAlertType.valueOf(request.getType());
			
			if(sfdcType == SFDCAlertType.UNPLANNED) {
				if(request.getUnplannedRestorationDate() == null) {
					logger.error("sfdc create outage alert missing unplanned date");
					throw new ServiceException( 
							"missing mandatory field 'unplanned_restoration_date'", APIResponse.ERROR_CODE_INVALID_INPUT);
				}
			}else {
				if(request.getScheduledMaintenanceStartDate() == null) {
					logger.error("sfdc create outage alert missing schedule maintenance start date");
					throw new ServiceException( 
							"missing mandatory field 'scheduled_maintenance_start_date'", APIResponse.ERROR_CODE_INVALID_INPUT);
				}else if(request.getScheduledMaintenanceEndDate() == null) {
					logger.error("sfdc create outage alert missing schedule maintenance end date");
					throw new ServiceException( 
							"missing mandatory field 'scheduled_maintenance_end_date'", APIResponse.ERROR_CODE_INVALID_INPUT);
				}
			}
			
			PowerAlertTypeDao alertType = cHelper.getPowerAlertType(sfdcType.getType());
			
			db.beginTransaction();
			
			Date now = new Date();
			
			CustServiceLocation location = cHelper.getCustServiceLocation(request.getStation());
			
			PowerAlertDao p = new PowerAlertDao();
			p.setSfdcId(request.getAnnouncementRecordId());
			p.setPowerAlertTypeId(alertType.getId());
			p.setCustServiceLocationId(location.getTransId());
			p.setArea(request.getArea());
			p.setCauses(request.getCauses());
			p.setTitle(request.getTitle());
			
			if(sfdcType == SFDCAlertType.SCHEDULED) {
				
				p.setRestorationDatetime(now);
				p.setMaintenanceStartDatetime(request.getScheduledMaintenanceStartDate());
				p.setMaintenanceEndDatetime(request.getScheduledMaintenanceEndDate());
			}else {
				p.setRestorationDatetime(request.getUnplannedRestorationDate());
				p.setMaintenanceStartDatetime(now);
				p.setMaintenanceEndDatetime(now);
			}
			
			p.setDeletedFlag(0);
			
			db.insert(p);
			
			if(request.isPushNotification()) {
				logger.debug("creating push notification");
				TagNotification t = new TagNotification();
				t.setText(request.getPushMessage());
				t.setNotificationType(NotificationType.POWER_ALERT);
				t.setAssociatedId(p.getId());
				t.setTagNme(getTagName(sfdcType));
				t.setStatus(NotificationStatus.UNSENT);
				// added by pramod
				t.setSendCounter(0);
				t.setPushDatetime(request.getPushDate());
				db.insert(t);
				logger.debug("created push notification");
				// added by pramod
			}
			
			db.commit();
			
			response.setAnnouncementId(p.getId());
			response.getResponseStatus().setStatus(SUCCESS);
			return response;	
		} catch (DatabaseFacadeException e) {
			logger.error("Error creating outage alert", e);
			throw new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		}finally {
			db.close();
		}
	}
	
	private String getTagName(SFDCAlertType sfdcType) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("NOTIFICATION_");
		if(sfdcType == SFDCAlertType.SCHEDULED) {
			sb.append("MAINTENANCE_");
		}else if(sfdcType == SFDCAlertType.UNPLANNED) {
			sb.append("POWER_ALERT_");
		}
		
		sb.append(request.getStation().toUpperCase());
		return sb.toString();
	}
}
