package com.seb.middleware.api.services.impl.sfdc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.sfdc.SfdcRequest;
import com.seb.middleware.api.services.response.Response;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.helper.SettingHelper;

public abstract class SfdcService<T extends SfdcRequest, K extends Response> extends ServiceBase<T, K> {

	private static final Logger logger = LogManager.getLogger(SfdcService.class);
	
	public SfdcService(T request) {
		super(request);
	}

	protected void validateCredential() throws ServiceException {
		DatabaseFacade db = new DatabaseFacade();
		
		try {
			SettingHelper sHelper = new SettingHelper(db);
			
			String clientId = sHelper.getSalesForceApiClientId();
			String secret = sHelper.getSalesForceApiClientSecret();
			
			if(clientId.equals(request.getClientId()) && secret.equals(request.getClientSecret())) {
				logger.debug("credential validated");
			}else {
				logger.error("sfdc request invalid credential");
				throw new ServiceException( 
						"invalid client_id or client_secret", APIResponse.ERROR_CODE_INVALID_INPUT);
			}
		}finally {
			db.close();
		}
	}
}
