package com.seb.middleware.api.services.impl.subscription;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.subscription.AddSubscriptionRequest;
import com.seb.middleware.api.services.response.subscription.AddSubscriptionResponse;
import com.seb.middleware.constant.SubscriptionType;
import com.seb.middleware.jco.SAP;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.Contract;
import com.seb.middleware.jpa.dao.Subscription;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.salesforce.api.CreateContractRequest;
import com.seb.middleware.salesforce.api.CreateSubscriptionRequest;
import com.seb.middleware.salesforce.api.QueryContractRequest;
import com.seb.middleware.salesforce.auth.Token;
import com.seb.middleware.salesforce.core.CompositeResponse;
import com.seb.middleware.salesforce.core.SFUtility;
import com.seb.middleware.salesforce.core.SalesForceCompositeConnector;
import com.seb.middleware.utility.ResourceUtil;
import com.seb.middleware.utility.StringUtil;

@Service(name = "add_subscription", publicService = true, auditable = true)
public class AddSubscriptionService extends ServiceBase<AddSubscriptionRequest, AddSubscriptionResponse> {

	private static final Logger logger = LogManager.getLogger(AddSubscriptionService.class);
	
	public AddSubscriptionService(AddSubscriptionRequest request) {
		super(request);
	}

	@Override
	public AddSubscriptionResponse perform() throws ServiceException {
		AddSubscriptionResponse response = new AddSubscriptionResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try {		
			SettingHelper settingHelper = new SettingHelper(db);
			SubscriptionHelper subscriptionHelper = new SubscriptionHelper(db);
			
			String contractAccNo = request.getContractAccNo().trim();
			String contractAccName = request.getContractAccName().trim();
			
			Contract contract = subscriptionHelper.getContractAccount(contractAccNo, contractAccName);
			
			if(contract == null){
				
				logger.info("Unable to find contract details from local database, now trying SAP BAPI.");
				boolean foundSAP = false;
				try {
					SAP sap = new SAP(settingHelper.getSAPTimeoutSeconds());
					String retrievedName = sap.getContractAccountName(contractAccNo);
					logger.debug("Contract account for " + contractAccNo + " is \"" + retrievedName + "\"");
					if(!StringUtil.isEmpty(retrievedName)){
						if(contractAccName.equalsIgnoreCase(retrievedName)){
							logger.info("Contract details was found in SAP");
							foundSAP = true;
							
							contract = new Contract();
							contract.setAgencyName("SEBWEBC");
							contract.setContractAccountName(retrievedName);
							contract.setContractAccountNumber(contractAccNo);
							
							try {
								Token token = SFUtility.getToken(settingHelper);
								QueryContractRequest qc = new QueryContractRequest(contract.getContractAccountNumber());
								CreateContractRequest cc = new CreateContractRequest(contract.getContractAccountNumber(), 
										contract.getContractAccountName(), contract.getAgencyName());
								
								SalesForceCompositeConnector connector = new SalesForceCompositeConnector(token);
								CompositeResponse qcresp = connector.invoke(qc);
								
								if(qcresp.isSuccess()) {
									contract.setSfdcId(qcresp.getFirstObjectId());
								}
								
								if(StringUtil.isEmpty(contract.getSfdcId())) {
									CompositeResponse resp = connector.invoke(cc);
									if(resp.isSuccess()) {
										contract.setSfdcId(resp.getObjectId());
									}
								}
							}catch(Throwable t) {
								logger.error("error creating contract to salesforce, however, it will be created later via cron - " + t.getMessage(), t);
							}
							
							db.beginTransaction();
							db.insertOrUpdate(contract);
						}
					}else{
						logger.info("Unable to find contract details SAP BAPI. CA does not exist.");
					}
					
				} catch (Throwable e) {
					logger.error("Error searching contract in SAP", e);
				}
				
				if(!foundSAP){
					throw new ServiceException(
							ResourceUtil.get("error.contract_account_number_name_pair_not_exist", 
									request.getLocale()), 
									APIResponse.ERROR_CODE_INVALID_INPUT);
				}
			}else if(StringUtil.isEmpty(contract.getSfdcId())){
				try {
					Token token = SFUtility.getToken(settingHelper);
					CreateContractRequest cc = new CreateContractRequest(contract.getContractAccountNumber(), 
							contract.getContractAccountName(), contract.getAgencyName());
					
					SalesForceCompositeConnector connector = new SalesForceCompositeConnector(token);
					CompositeResponse resp = connector.invoke(cc);
					if(resp.isSuccess()) {
						contract.setSfdcId(resp.getObjectId());
					}
					
					db.beginTransaction();
					db.insertOrUpdate(contract);
				}catch(Throwable t) {
					logger.error("error creating contract to salesforce, however, it will be created later via cron - " + t.getMessage(), t);
				}
			}
			
			if(!subscriptionHelper.isContractAccountCanSubscribe(settingHelper.getContractMaxSubscriptions(), contractAccNo)){
				throw new ServiceException(
						ResourceUtil.get("error.subscription_reach_max_allow", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
			
			if(subscriptionHelper.isSubscriptionExist(contractAccNo, request.getEmail())){
				throw new ServiceException(
						ResourceUtil.get("error.subscription_exist", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
			
			if(!db.isTransactionBegan())
				db.beginTransaction();
			
			MemberHelper mHelper = new MemberHelper(db);
			User u = mHelper.getUser(request.getEmail());
			
			Subscription subscription = new Subscription();
			subscription.setUserEmail(request.getEmail());
			subscription.setContractAccNick(request.getContractAccNick());
			subscription.setContractAccNo(contractAccNo);
			subscription.setSubsType(SubscriptionType.valueOf(request.getSubsType()));
			subscription.setNotifyByEmail(1);
			subscription.setNotifyBySMS(1);
			subscription.setNotifyByMobilePush(1);
			subscription.setSfdcUpdatedDate(new Date());
			
			if(!StringUtil.isEmpty(contract.getSfdcId()) && !StringUtil.isEmpty(u.getSfdcId())) {
				try {
					Token token = SFUtility.getToken(settingHelper);
					CreateSubscriptionRequest cs = new CreateSubscriptionRequest(
							u.getSfdcId(), contract.getSfdcId(), 
							request.getEmail(), request.getContractAccNo(),
							subscription.getContractAccNick(), subscription.getSubsType(), subscription.getStatus());
					
					SalesForceCompositeConnector connector = new SalesForceCompositeConnector(token);
					CompositeResponse resp = connector.invoke(cs);
					if(resp.isSuccess()) {
						subscription.setSfdcId(resp.getObjectId());
						subscription.setSfdcSyncDate(new Date());
					}
				}catch(Throwable t) {
					logger.error("error creating subscription to salesforce, however, it will be created later via cron - " + t.getMessage(), t);
				}
			}else {
				logger.debug("subscription master's record doesn't have sfdcId yet, waiting for cron trigger");
			}
			
			subscriptionHelper.addSubscription(subscription);
			
			db.commit();
			
			response.getResponseStatus().setStatus(SUCCESS);
			return response;
		} catch (DatabaseFacadeException e) {
			logger.error("Error create new subscription", e);
			ServiceException ex = new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			throw ex;
		}finally{
			db.close();
		} 
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("Add subscription - Contract account number: ");
		sb.append(request.getContractAccNo());
		return sb.toString();
	}
}
