package com.seb.middleware.api.services.impl.subscription;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.subscription.EditSubscriptionRequest;
import com.seb.middleware.api.services.response.subscription.EditSubscriptionResponse;
import com.seb.middleware.constant.SubscriptionType;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.Subscription;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.salesforce.api.UpdateSubscriptionRequest;
import com.seb.middleware.salesforce.auth.Token;
import com.seb.middleware.salesforce.core.CompositeResponse;
import com.seb.middleware.salesforce.core.SFUtility;
import com.seb.middleware.salesforce.core.SalesForceCompositeConnector;
import com.seb.middleware.utility.ResourceUtil;
import com.seb.middleware.utility.StringUtil;

@Service(name = "edit_subscription", publicService = true, auditable = true)
public class EditSubscriptionService extends ServiceBase<EditSubscriptionRequest, EditSubscriptionResponse> {

	private static final Logger logger = LogManager.getLogger(EditSubscriptionService.class);
	
	public EditSubscriptionService(EditSubscriptionRequest request) {
		super(request);
	}

	@Override
	public EditSubscriptionResponse perform() throws ServiceException {
		EditSubscriptionResponse response = new EditSubscriptionResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try {		
			SettingHelper settingHelper = new SettingHelper(db);
			SubscriptionHelper subscriptionHelper = new SubscriptionHelper(db);
			Subscription subscription = subscriptionHelper.getSubscription(request.getContractAccNo(), request.getEmail());
			
			if(subscription != null){
				
				if(request.getContractAccNick() != null && !request.getContractAccNick().isEmpty()){
					subscription.setContractAccNick(request.getContractAccNick());
				}
				if(request.getSubsType() != null && !request.getSubsType().isEmpty()){
					subscription.setSubsType(SubscriptionType.valueOf(request.getSubsType()));
				}
				
				if(request.getNotifyByEmail() != null) {
					subscription.setNotifyByEmail(request.getNotifyByEmail() ? 1 : 0);	
				}
				if(request.getNotifyBySMS()!= null) {
					subscription.setNotifyBySMS(request.getNotifyBySMS() ? 1 : 0);	
				}
				if(request.getNotifyByMobilePush() != null) {
					subscription.setNotifyByMobilePush(request.getNotifyByMobilePush() ? 1 : 0);	
				}
				
				subscription.setSfdcUpdatedDate(new Date());
				
				if(!StringUtil.isEmpty(subscription.getSfdcId())) {
					
					try {
						Token token = SFUtility.getToken(settingHelper);
						UpdateSubscriptionRequest us = new UpdateSubscriptionRequest(subscription.getSfdcId(), 
								subscription.getContractAccNick(), subscription.getSubsType(), subscription.getStatus());
						
						SalesForceCompositeConnector connector = new SalesForceCompositeConnector(token);
						CompositeResponse resp = connector.invoke(us);
						if(resp.isSuccess()) {
							subscription.setSfdcSyncDate(new Date());
						}
					}catch(Throwable t) {
						logger.error("error updating subscription to salesforce, however, it will be created later via cron - " + t.getMessage(), t);
					}
				}
				
				db.beginTransaction();
				db.update(subscription);
				db.commit();
				
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			}else{
				throw new ServiceException(
						ResourceUtil.get("error.subscription_not_exist", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
		} catch (DatabaseFacadeException e) {
			logger.error("Error edit subscription", e);
			ServiceException ex = new ServiceException(e, 
					ResourceUtil.get("error.general_error_database", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			throw ex;
		}finally{
			db.close();
		} 
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("Edit subscription - Contract account number: ");
		sb.append(request.getContractAccNo());
		return sb.toString();
	}
}
