package com.seb.middleware.api.services.impl.subscription;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.subscription.GetAvailableStatementRequest;
import com.seb.middleware.api.services.response.subscription.GetAvailableStatementResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "get_available_statement", publicService = true, auditable = true)
public class GetAvailableStatementService extends ServiceBase<GetAvailableStatementRequest, GetAvailableStatementResponse> {

	private static final Logger logger = LogManager.getLogger(GetAvailableStatementService.class);
	
	public GetAvailableStatementService(GetAvailableStatementRequest request) {
		super(request);
	}

	@Override
	public GetAvailableStatementResponse perform() throws ServiceException {
		GetAvailableStatementResponse response = new GetAvailableStatementResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		SettingHelper setting = new SettingHelper(db);
		
		try{
			SubscriptionHelper subscription = new SubscriptionHelper(db);
			
			boolean subscribed = subscription.isSubscriptionExist(request.getContractAccNo(), request.getEmail());
			if(subscribed){
				int maxRecord = setting.getStatemetnListingEntries();
				ArrayList<String> billingMonths = subscription.getAvailableStatementMonths(request.getContractAccNo(), maxRecord);
				//ArrayList<BillingTransaction> billings = subscription.getRecentBillingTransactions(request.getContractAccNo(), maxRecord);
				response.getResponseStatus().setStatus(SUCCESS);
				if(billingMonths != null && !billingMonths.isEmpty()){
					String [] months = new String [billingMonths.size()];
					for(int i = 0; i < billingMonths.size(); i++){
						months[i] = billingMonths.get(i);
					}
					response.setMonths(months);
				}else{
					logger.info("no billing transaction found for account: " + request.getContractAccNo());					
				}
				
				return response;
			}else{
				logger.error("Member: " + request.getEmail() + " did not subscribe to this Contract Account Yet (" + request.getContractAccNo() + ")");
				throw new ServiceException(
						ResourceUtil.get("error.subscription_not_exist", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
		} finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		return "Read subscriptions statement listing";
	}
}
