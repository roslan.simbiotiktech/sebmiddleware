package com.seb.middleware.api.services.impl.subscription;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.subscription.GetConsumptionRequest;
import com.seb.middleware.api.services.response.subscription.Consumption;
import com.seb.middleware.api.services.response.subscription.GetConsumptionResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "get_consumption", publicService = true, auditable = true)
public class GetConsumptionService extends ServiceBase<GetConsumptionRequest, GetConsumptionResponse> {

	private static final Logger logger = LogManager.getLogger(GetConsumptionService.class);
	
	public GetConsumptionService(GetConsumptionRequest request) {
		super(request);
	}

	@Override
	public GetConsumptionResponse perform() throws ServiceException {
		GetConsumptionResponse response = new GetConsumptionResponse();
		DatabaseFacade db = new DatabaseFacade();
		try{
			SubscriptionHelper subscriptionHelper = new SubscriptionHelper(db);
			
			boolean subscribed = subscriptionHelper.isSubscriptionExist(request.getContractAccNo(), request.getEmail());
			if(subscribed){
				Map<Date, BigDecimal> consumptions = subscriptionHelper.getConsumption(request.getContractAccNo());
				if(consumptions != null && !consumptions.isEmpty()){
					Consumption [] cons = new Consumption [consumptions.size()];
					
					Date[] dates = consumptions.keySet().toArray(new Date[consumptions.size()]);
					Arrays.sort(dates, Collections.reverseOrder());
					
					int i = 0;
					for(Date d : dates){
						BigDecimal amount = consumptions.get(d);
						
						Consumption con = new Consumption();
						con.setMonth(d);
						con.setAmt(amount.doubleValue());
						
						cons[i++] = con;
					}
					
					response.setConsumptions(cons);
				}

				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			}else{
				logger.error("Member: " + request.getEmail() + " did not subscribe to this Contract Account Yet (" + request.getContractAccNo() + ")");
				throw new ServiceException(
						ResourceUtil.get("error.subscription_not_exist", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
		}finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("Read subscription consumption - Contract account number: ");
		sb.append(request.getContractAccNo());
		return sb.toString();
	}
}
