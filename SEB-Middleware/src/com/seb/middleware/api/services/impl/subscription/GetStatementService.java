package com.seb.middleware.api.services.impl.subscription;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.subscription.GetStatementRequest;
import com.seb.middleware.api.services.response.subscription.GetStatementResponse;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.BillingTransaction;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.utility.DateUtil;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "get_statement", publicService = true, auditable = true)
public class GetStatementService extends ServiceBase<GetStatementRequest, GetStatementResponse> {

	private static final Logger logger = LogManager.getLogger(GetStatementService.class);
	
	public GetStatementService(GetStatementRequest request) {
		super(request);
	}

	/*
	 * File System Structure for PDF
	 * 
	 * <Root Folder>
	 *   <Year of Invoice>
	 *     <Contract Account Number 1>
	 *       <PDF Files>
	 *     <Contract Account Number 2>
	 *       <PDF Files>
	 *   <Year of Invoice, Another Year>
	 */
	@Override
	public GetStatementResponse perform() throws ServiceException {
		GetStatementResponse response = new GetStatementResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			SettingHelper setting = new SettingHelper(db);
			SubscriptionHelper sHelper = new SubscriptionHelper(db);
			
			boolean subscribed = sHelper.isSubscriptionExist(request.getContractAccNo(), request.getEmail());
			if(subscribed){
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
				SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
				String requestDate = sdf.format(request.getMonth());
				String requestYear = sdfYear.format(request.getMonth());

				Date [] fromToDate = DateUtil.getEffectiveBetweenClause(requestDate);
				
				BillingTransaction billing = sHelper.getBillingTransaction(request.getContractAccNo(), fromToDate[0], fromToDate[1]);
				
				if(billing != null){
					String fileName = billing.getPdfPrintDocumentNumber();
					String filePath = sHelper.getInvoicePath(setting.getStatementRootPath(), fileName, request.getContractAccNo(), requestYear);
					
					File file = new File(filePath);
					if(file.exists() && file.canRead()){
						
						byte[] data = Files.readAllBytes(file.toPath());
						byte[] data64 = Base64.encodeBase64(data);
						
						response.setPdf(new String(data64));
						response.getResponseStatus().setStatus(SUCCESS);
						
						return response;	
					}else{
						logger.error("Invoice File error | exist() : " + file.exists() + " | canRead() : " + file.canRead());
						throw new ServiceException(
								ResourceUtil.get("error.statement_not_found", 
										request.getLocale()), 
										APIResponse.ERROR_CODE_SERVER_EXCEPTION);
					}
				}else{
					logger.error("Statement not found in Database.");
					throw new ServiceException(
							ResourceUtil.get("error.statement_not_found", 
									request.getLocale()), 
									APIResponse.ERROR_CODE_SERVER_EXCEPTION);
				}
			}else{
				logger.error("Member: " + request.getEmail() + " did not subscribe to this Contract Account Yet (" + request.getContractAccNo() + ")");
				throw new ServiceException(
						ResourceUtil.get("error.subscription_not_exist", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
		} catch (IOException | ParseException e) {
			throw new ServiceException(
					ResourceUtil.get("error.general_500", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
		} finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("Read eStatement - Contract account number: ");
		sb.append(request.getContractAccNo());
		return sb.toString();
	}
}
