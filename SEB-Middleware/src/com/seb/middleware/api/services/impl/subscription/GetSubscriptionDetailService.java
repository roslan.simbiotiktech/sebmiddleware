package com.seb.middleware.api.services.impl.subscription;

import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.subscription.GetSubscriptionDetailRequest;
import com.seb.middleware.api.services.response.subscription.GetSubscriptionDetailResponse;
import com.seb.middleware.jco.SAP;
import com.seb.middleware.jco.data.AccountBalance;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.BillingTransaction;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "get_subscription_detail", publicService = true, auditable = true)
public class GetSubscriptionDetailService extends ServiceBase<GetSubscriptionDetailRequest, GetSubscriptionDetailResponse> {

	private static final Logger logger = LogManager.getLogger(GetSubscriptionDetailService.class);
	
	public GetSubscriptionDetailService(GetSubscriptionDetailRequest request) {
		super(request);
	}

	@Override
	public GetSubscriptionDetailResponse perform() throws ServiceException {
		GetSubscriptionDetailResponse response = new GetSubscriptionDetailResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		SubscriptionHelper subscriptionHelper = new SubscriptionHelper(db);
		SettingHelper settingHelper = new SettingHelper(db);
		
		SAP sap = new SAP(settingHelper.getSAPTimeoutSeconds());
		
		String contractAccNo = request.getContractAccNo();
		
		try{
			com.seb.middleware.jpa.dao.Subscription subscription = subscriptionHelper.getSubscription(contractAccNo, request.getEmail());
			
			if(subscription != null){
				BillingTransaction subscriptionDetail = subscriptionHelper.getSubscriptionDetailLatest(contractAccNo);
				
				AccountBalance accountBalance = null;
				
				try{
					accountBalance = sap.getAccountBalance(subscription.getContractAccNo());	
				}catch(Exception je){
					logger.error("error in sap - " + je.getMessage(), je);
					if(settingHelper.isTestingEnvironment()){
						logger.debug("using hardcode SAP data");
						accountBalance = new AccountBalance();
						accountBalance.setOutstanding(new BigDecimal("1000"));
						accountBalance.setExcessPayment(new BigDecimal("10"));
					}else{
						throw je;
					}
				}
				
				response.setContractAccNo(subscription.getContractAccNo());
				response.setContractAccNick(subscription.getContractAccNick());
				response.setContractAccName(subscription.getContract().getContractAccountName());
				response.setSubscriptionType(subscription.getSubsType().toString());
				response.setAmtDue(accountBalance.getOutstanding().doubleValue());
				response.setExcessPaymentAmount(accountBalance.getExcessPayment().doubleValue());
				response.setNotifyByEmail(subscription.getNotifyByEmail() == 1);
				response.setNotifyBySMS(subscription.getNotifyBySMS() == 1);
				response.setNotifyByMobilePush(subscription.getNotifyByMobilePush() == 1);
				
				if(subscriptionDetail != null){
					response.setCurrentBillAmtDue(subscriptionDetail.getCurrentAmount().doubleValue());
					response.setPaymentDueDate(subscriptionDetail.getDueDate());
					response.setBillPeriodFrom(subscriptionDetail.getBillStartDate());
					response.setBillPeriodTo(subscriptionDetail.getBillEndDate());
				}
				
				response.getResponseStatus().setStatus(SUCCESS);
				return response;
			}else{
				logger.error("Member: " + request.getEmail() + " did not subscribe to this Contract Account Yet (" + request.getContractAccNo() + ")");
				throw new ServiceException(
						ResourceUtil.get("error.subscription_not_exist", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}			
		} catch (Exception e) {
			logger.error("Error getting account balance from sap - " + e.getMessage(), e);
			ServiceException ex = new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			throw ex;
		}finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("Read Subscription details - Contract account number: ");
		sb.append(request.getContractAccNo());
		return sb.toString();
	}
}
