package com.seb.middleware.api.services.impl.subscription;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.subscription.GetSubscriptionPaymentHistoryRequest;
import com.seb.middleware.api.services.response.subscription.GetSubscriptionPaymentHistoryResponse;
import com.seb.middleware.api.services.response.subscription.PaymentHistory;
import com.seb.middleware.constant.PaymentStatus;
import com.seb.middleware.jco.SAP;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.PaymentHistoryDAO;
import com.seb.middleware.jpa.helper.PaymentHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.utility.ResourceUtil;
import com.seb.middleware.utility.StringUtil;

@Service(name = "get_subscription_payment_history", publicService = true, auditable = true)
public class GetSubscriptionPaymentHistoryService 
	extends ServiceBase<GetSubscriptionPaymentHistoryRequest, GetSubscriptionPaymentHistoryResponse> {

	private static final Logger logger = LogManager.getLogger(GetSubscriptionPaymentHistoryService.class);
	
	public GetSubscriptionPaymentHistoryService(GetSubscriptionPaymentHistoryRequest request) {
		super(request);
	}

	@Override
	public GetSubscriptionPaymentHistoryResponse perform() throws ServiceException {
		GetSubscriptionPaymentHistoryResponse response = new GetSubscriptionPaymentHistoryResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		try{
			SubscriptionHelper subscription = new SubscriptionHelper(db);
			
			boolean subscribed = subscription.isSubscriptionExist(request.getContractAccNo(), request.getEmail());
			if(subscribed){
				SettingHelper setting = new SettingHelper(db);
				PaymentHelper pHelper = new PaymentHelper(db);				
				
				try{
					if(StringUtil.isEmpty(request.getMode()) || request.getMode().equals("1")){
						getSuccessful(setting, response);
					}else if(request.getMode().equals("2")){
						getUnsuccessful(setting, pHelper, response);
					}else if(request.getMode().equals("3")){
						getPendingAuthorization(setting, pHelper, response);
					}else{
						getSuccessful(setting, response);
					}
					
					response.getResponseStatus().setStatus(SUCCESS);
					return response;
				}catch(Throwable ex){
					if(setting.isTestingEnvironment()){
						PaymentHistory[] histories = new PaymentHistory[5];
						Calendar calendar = Calendar.getInstance();
						calendar.setTime(new Date());
						for(int i = 0; i < 5; i++){
							calendar.add(Calendar.MONTH, -1);
							PaymentHistory p = new PaymentHistory();
							if(i == 0){
								p.setAmount(1234567.89);	
							}else if(i==1){
								p.setAmount(0.01);
							}else if(i==2){
								p.setAmount(500.00);
							}else if(i==3){
								p.setAmount(1500.50);
							}else{
								p.setAmount(999999999.99);
							}
							
							p.setPaymentDate(calendar.getTime());
							histories[i] = p;
						}
						response.setPaymentHistories(histories);
						
						response.getResponseStatus().setStatus(SUCCESS);
						return response;
					}else{
						throw ex;	
					}
				}
				
			}else{
				logger.error("Member: " + request.getEmail() + " did not subscribe to this Contract Account Yet (" + request.getContractAccNo() + ")");
				throw new ServiceException(
						ResourceUtil.get("error.subscription_not_exist", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_INVALID_INPUT);
			}
		} catch (Exception e) {
			logger.error("Error getting subscription payment history", e);
			ServiceException ex = new ServiceException(e, 
					ResourceUtil.get("error.general_error_sap", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			throw ex;
		}finally{
			db.close();
		}
	}
	
	private void getUnsuccessful(SettingHelper setting, PaymentHelper pHelper, GetSubscriptionPaymentHistoryResponse response) throws Exception{
		int totalDisplayEntries = setting.getPaymentHistoryListingEntries();
		
		List<PaymentHistoryDAO> list = pHelper.getPayment(request.getContractAccNo(), totalDisplayEntries, PaymentStatus.PENDING, PaymentStatus.FAILED);

		if(list != null && !list.isEmpty()){
			PaymentHistory[] histories = new PaymentHistory[list.size()];

			for(int i = 0; i < list.size(); i++){
				PaymentHistoryDAO paymentHistory = list.get(i);
				
				PaymentHistory his = new PaymentHistory();
				his.setPaymentDate(paymentHistory.getPaymentDate() != null ? paymentHistory.getPaymentDate() : paymentHistory.getCreatedDate());
				his.setAmount(paymentHistory.getAmount().doubleValue());
				histories[i] = his;
			}

			response.setPaymentHistories(histories);
		}
	}
	
	private void getPendingAuthorization(SettingHelper setting, 
			PaymentHelper pHelper, GetSubscriptionPaymentHistoryResponse response) throws Exception{
		int totalDisplayEntries = setting.getPaymentHistoryListingEntries();
		
		List<PaymentHistoryDAO> list = pHelper.getPayment(request.getContractAccNo(), totalDisplayEntries, PaymentStatus.PENAUTH);

		if(list != null && !list.isEmpty()){
			PaymentHistory[] histories = new PaymentHistory[list.size()];

			for(int i = 0; i < list.size(); i++){
				PaymentHistoryDAO paymentHistory = list.get(i);
				
				PaymentHistory his = new PaymentHistory();
				his.setPaymentDate(paymentHistory.getPaymentDate() != null ? paymentHistory.getPaymentDate() : paymentHistory.getCreatedDate());
				his.setAmount(paymentHistory.getAmount().doubleValue());
				histories[i] = his;
			}

			response.setPaymentHistories(histories);
		}
	}
	
	private void getSuccessful(SettingHelper setting, GetSubscriptionPaymentHistoryResponse response) throws Exception{
		int totalDisplayEntries = setting.getPaymentHistoryListingEntries();
		
		SAP sap = new SAP(setting.getSAPTimeoutSeconds());
		
		ArrayList<com.seb.middleware.jco.data.PaymentHistory> paymentHistories = 
				sap.getPaymentHistory(request.getContractAccNo(), totalDisplayEntries);
		
		if(paymentHistories != null && !paymentHistories.isEmpty()){
			PaymentHistory[] histories = new PaymentHistory[paymentHistories.size()];

			for(int i = 0; i < paymentHistories.size(); i++){
				com.seb.middleware.jco.data.PaymentHistory paymentHistory = paymentHistories.get(i);
				PaymentHistory his = new PaymentHistory();
				his.setPaymentDate(paymentHistory.getDate());
				his.setAmount(paymentHistory.getAmout() * -1);
				histories[i] = his;
			}

			response.setPaymentHistories(histories);
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		StringBuilder sb = new StringBuilder();
		sb.append("Read Subscription payment history - Contract account number: ");
		sb.append(request.getContractAccNo());
		return sb.toString();
	}
}
