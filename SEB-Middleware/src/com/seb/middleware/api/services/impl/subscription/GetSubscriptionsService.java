package com.seb.middleware.api.services.impl.subscription;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.Service;
import com.seb.middleware.api.ServiceBase;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.subscription.GetSubscriptionsRequest;
import com.seb.middleware.api.services.response.subscription.GetSubscriptionsResponse;
import com.seb.middleware.api.services.response.subscription.Subscription;
import com.seb.middleware.jco.SAP;
import com.seb.middleware.jco.data.AccountBalance;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.BillingTransaction;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.utility.ResourceUtil;

@Service(name = "get_subscriptions", publicService = true, auditable = true)
public class GetSubscriptionsService extends ServiceBase<GetSubscriptionsRequest, GetSubscriptionsResponse> {

	private static final Logger logger = LogManager.getLogger(GetSubscriptionsService.class);
	
	public GetSubscriptionsService(GetSubscriptionsRequest request) {
		super(request);
	}

	@Override
	public GetSubscriptionsResponse perform() throws ServiceException {
		GetSubscriptionsResponse response = new GetSubscriptionsResponse();
		
		DatabaseFacade db = new DatabaseFacade();
		SubscriptionHelper subscriptionHelper = new SubscriptionHelper(db);
		SettingHelper settingHelper = new SettingHelper(db);
		
		SAP sap = new SAP(settingHelper.getSAPTimeoutSeconds());
		try{
			ArrayList<com.seb.middleware.jpa.dao.Subscription> subscriptionList = subscriptionHelper.getSubscriptions(request.getEmail());
			
			if(subscriptionList != null && !subscriptionList.isEmpty()){
				Subscription [] subscriptions = new Subscription [subscriptionList.size()];
				
				int counter = 0;
				
				BigDecimal totalAmtDue = new BigDecimal(0);
				
				for(com.seb.middleware.jpa.dao.Subscription subscription : subscriptionList){
					String contractAccNo = subscription.getContractAccNo();

					BillingTransaction subscriptionDetail = subscriptionHelper.getSubscriptionDetailLatest(contractAccNo);
								
					AccountBalance accountBalance = null;
					
					try{
						accountBalance = sap.getAccountBalance(subscription.getContractAccNo());	
					}catch(Exception je){
						logger.error("error in sap - " + je.getMessage(), je);
						if(settingHelper.isTestingEnvironment()){
							logger.debug("using hardcode SAP data");
							accountBalance = new AccountBalance();
							accountBalance.setOutstanding(new BigDecimal("1000"));
							accountBalance.setExcessPayment(new BigDecimal("10"));
						}else{
							throw je;
						}
					}
					
					subscriptions[counter] = new Subscription();
					subscriptions[counter].setContractAccountNick(subscription.getContractAccNick());
					subscriptions[counter].setContractAccNo(subscription.getContractAccNo());
					subscriptions[counter].setContractAccName(subscription.getContract().getContractAccountName());
					subscriptions[counter].setSubscriptionType(subscription.getSubsType().toString());
					subscriptions[counter].setAmtDue(accountBalance.getOutstanding().doubleValue());
					subscriptions[counter].setExcessPaymentAmount(accountBalance.getExcessPayment().doubleValue());
					
					if(subscriptionDetail != null){
						subscriptions[counter].setCurrentBillAmtDue(subscriptionDetail.getCurrentAmount().doubleValue());
						subscriptions[counter].setPaymentDueDate(subscriptionDetail.getDueDate());
					}
					
					totalAmtDue = totalAmtDue.add(accountBalance.getOutstanding());
					
					counter++;
				}
				
				response.setSubscriptions(subscriptions);
				response.setTotalAmtDue(totalAmtDue.doubleValue());
			}

			response.getResponseStatus().setStatus(SUCCESS);
			
			return response;
		} catch (Exception e) {
			logger.error("Error getting account balance from sap - " + e.getMessage(), e);
			ServiceException ex = new ServiceException(e, 
					ResourceUtil.get("error.general_500", 
							request.getLocale()), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			throw ex;
		} finally{
			db.close();
		}
	}
	
	@Override
	public String getAuditUser(){
		return request.getEmail();
	}
	
	@Override
	public String getAuditActivity(){
		return "Read subscriptions listing";
	}
}
