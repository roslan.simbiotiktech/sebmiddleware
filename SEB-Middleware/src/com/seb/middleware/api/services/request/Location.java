package com.seb.middleware.api.services.request;

import com.anteater.library.json.JsonField;

public class Location {

	@JsonField(name="LATITUDE", validateLength=true, maxLength=11)
	private String latitude;
	
	@JsonField(name="LONGITUDE", validateLength=true, maxLength=11)
	private String longitude;
	
	@JsonField(name="ADDRESS_SHORT", localizationKey="address", mandatory=true, validateLength=true, maxLength=300)
	private String addressShort;

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getAddressShort() {
		return addressShort;
	}

	public void setAddressShort(String addressShort) {
		this.addressShort = addressShort;
	}

	@Override
	public String toString() {
		return "ReportLocation [latitude=" + latitude + ", longitude=" + longitude + ", addressShort=" + addressShort + "]";
	}
}
