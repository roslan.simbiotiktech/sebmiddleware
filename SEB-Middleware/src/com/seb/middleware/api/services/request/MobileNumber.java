package com.seb.middleware.api.services.request;

import com.anteater.library.json.JsonField;
import com.seb.middleware.formatter.PhoneNumberFormatter;
import com.seb.middleware.validator.PhoneNumberValidator;
import com.seb.middleware.validator.VerificationCodeValidator;

public class MobileNumber {

	@JsonField(name = "NUMBER", mandatory = true, customValidator = PhoneNumberValidator.class,
			customFormatter = PhoneNumberFormatter.class)
	private String number;
	
	@JsonField(name = "MOBILE_VERIFICATION_CODE", mandatory = true, customValidator = VerificationCodeValidator.class)
	private String verificationCode;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	@Override
	public String toString() {
		return "MobileNumber [number=" + number + ", verificationCode=" + verificationCode + "]";
	}
}
