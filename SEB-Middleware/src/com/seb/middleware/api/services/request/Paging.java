package com.seb.middleware.api.services.request;

import com.anteater.library.json.JsonField;

public class Paging {

	@JsonField(name = "PAGE", mandatory = false)
	private int page = 1;
	
	@JsonField(name = "RECORDS_PER_PAGE", mandatory = false, validateNumber = true, minNumber = 1)
	private int recordsPerPage = 50;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRecordsPerPage() {
		return recordsPerPage;
	}

	public void setRecordsPerPage(int recordsPerPage) {
		this.recordsPerPage = recordsPerPage;
	}

	@Override
	public String toString() {
		return "Paging [page=" + page + ", recordsPerPage=" + recordsPerPage + "]";
	}
}
