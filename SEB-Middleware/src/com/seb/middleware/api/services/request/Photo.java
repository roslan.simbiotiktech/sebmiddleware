package com.seb.middleware.api.services.request;

import com.anteater.library.json.JsonField;
import com.seb.middleware.utility.StringUtil;
import com.seb.middleware.validator.PhotoDataValidator;

public class Photo {

	@JsonField(name="DATA", localizationKey="photo_data", mandatory=true, validateLength=false, customValidator = PhotoDataValidator.class)
	private String data;
	
	@JsonField(name="DATA_TYPE", localizationKey="photo_data_type", mandatory=true)
	private String dataType;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	@Override
	public String toString() {
		return "Photo [dataType=" + dataType + ", data (size)=" + StringUtil.getHumanReadableFileSizeFromBase64(data) + "]";
	}
}
