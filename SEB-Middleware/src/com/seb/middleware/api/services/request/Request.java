package com.seb.middleware.api.services.request;

import com.anteater.library.json.JsonField;
import com.seb.middleware.validator.LocaleValidator;


public class Request {
	
	@JsonField(name="LOCALE", mandatory = false, customValidator = LocaleValidator.class)
	protected String locale = "en";

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	@Override
	public String toString() {
		return "Request [locale=" + locale + "]";
	}
}
