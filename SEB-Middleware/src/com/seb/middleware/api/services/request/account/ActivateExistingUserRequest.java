package com.seb.middleware.api.services.request.account;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.CommunicationMethodValidator;
import com.seb.middleware.validator.EmailValidator;
import com.seb.middleware.validator.UserValidator;
import com.seb.middleware.validator.VerificationCodeValidator;

public class ActivateExistingUserRequest extends Request {
	
	@JsonField(name = "USER_ID", mandatory = true, customValidator = UserValidator.class , customFormatter = EmailFormatter.class)
	private String userId;
	
	@JsonField(name = "EMAIL", mandatory = true, customValidator = EmailValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "EMAIL_ACTIVATION_CODE", 
			mandatory = true, exactLength = 6, customValidator = VerificationCodeValidator.class)
	private String activationCode;

	@JsonField(name = "PREFERRED_COMM_METHOD", mandatory = true, customValidator=CommunicationMethodValidator.class)
	private String preferredCommunicationMethod;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public String getPreferredCommunicationMethod() {
		return preferredCommunicationMethod;
	}

	public void setPreferredCommunicationMethod(String preferredCommunicationMethod) {
		this.preferredCommunicationMethod = preferredCommunicationMethod;
	}

	@Override
	public String toString() {
		return "ActivateExistingUserRequest [userId=" + userId + ", email=" + email + ", activationCode=" + activationCode
				+ ", preferredCommunicationMethod=" + preferredCommunicationMethod + ", locale=" + locale + "]";
	}
}
