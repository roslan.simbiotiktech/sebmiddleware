package com.seb.middleware.api.services.request.account;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.PasswordValidator;
import com.seb.middleware.validator.UserEmailWithStatusValidator;
import com.seb.middleware.validator.VerificationCodeValidator;

public class ChangePasswordRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = true, maxLength = 50, customValidator = UserEmailWithStatusValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "NEW_PASSWORD", mandatory = true, customValidator = PasswordValidator.class)
	private String newPassword;
	
	@JsonField(name = "OTP", 
			mandatory = false, 
			exactLength = 6, customValidator =  VerificationCodeValidator.class)
	private String otp;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	@Override
	public String toString() {
		return "ChangePasswordRequest [email=" + email + ", otp=" + otp
				+ ", locale=" + locale + "]";
	}	
}
