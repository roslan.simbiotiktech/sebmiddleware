package com.seb.middleware.api.services.request.account;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.PhoneNumberFormatter;
import com.seb.middleware.validator.PhoneNumberValidator;
import com.seb.middleware.validator.VerificationCodeValidator;

public class CheckMobileVerificationCodeRequest extends Request {

	@JsonField(name = "MOBILE_NUMBER", customValidator = PhoneNumberValidator.class, 
			customFormatter = PhoneNumberFormatter.class)
	private String mobileNumber;

	@JsonField(name = "MOBILE_VERIFICATION_CODE", 
			mandatory = true, 
			exactLength = 6, customValidator =  VerificationCodeValidator.class)
	private String verificationCode;

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	@Override
	public String toString() {
		return "CheckMobileVerificationCodeRequest [mobileNumber=" + mobileNumber + ", verificationCode=" + verificationCode + ", locale=" + locale
				+ "]";
	}
}
