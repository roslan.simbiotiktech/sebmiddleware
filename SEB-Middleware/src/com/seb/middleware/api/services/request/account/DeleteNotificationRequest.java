package com.seb.middleware.api.services.request.account;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.UserEmailWithStatusValidator;

public class DeleteNotificationRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = true, customValidator = UserEmailWithStatusValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "NOTIFICATION_IDS", mandatory = false)
	private long [] notificationIds;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long[] getNotificationIds() {
		return notificationIds;
	}

	public void setNotificationIds(long[] notificationIds) {
		this.notificationIds = notificationIds;
	}

	@Override
	public String toString() {
		return "DeleteNotificationRequest [email=" + email + ", notificationIds=" + Arrays.toString(notificationIds) + ", locale=" + locale + "]";
	}
}
