package com.seb.middleware.api.services.request.account;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.UserEmailWithStatusValidator;

public class DeleteReportRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = true, customValidator = UserEmailWithStatusValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "TRANS_IDS", mandatory = true)
	private String [] transIds;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String[] getTransIds() {
		return transIds;
	}

	public void setTransIds(String[] transIds) {
		this.transIds = transIds;
	}

	@Override
	public String toString() {
		return "DeleteReportRequest [email=" + email + ", transIds=" + Arrays.toString(transIds) + "]";
	}
}
