package com.seb.middleware.api.services.request.account;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.MobileNumber;
import com.seb.middleware.api.services.request.Photo;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.formatter.PhoneNumberFormatter;
import com.seb.middleware.validator.CommunicationMethodValidator;
import com.seb.middleware.validator.HomeNumberValidator;
import com.seb.middleware.validator.LocaleValidator;
import com.seb.middleware.validator.OfficeNumberValidator;
import com.seb.middleware.validator.UserEmailWithStatusValidator;

public class EditProfileRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = true, customValidator = UserEmailWithStatusValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "NAME", mandatory = false, maxLength = 160)
	private String name;
	
	@JsonField(name = "NRIC_OR_PASSPORT", mandatory = false, maxLength = 15)
	private String nricOrPassport;
	
	@JsonField(name = "MOBILE_NO", mandatory = false)
	private MobileNumber mobileNumber;
	
	@JsonField(name = "PREFERRED_COMM_METHOD", mandatory = false, customValidator=CommunicationMethodValidator.class)
	private String preferredCommunicationMethod;
	
	@JsonField(name = "HOME_TEL", mandatory = false, customValidator = HomeNumberValidator.class,
			customFormatter = PhoneNumberFormatter.class)
	private String homeTel;
	
	@JsonField(name = "OFFICE_TEL", mandatory = false, customValidator = OfficeNumberValidator.class,
			customFormatter = PhoneNumberFormatter.class)
	private String officeTel;
	
	@JsonField(name="PHOTO", mandatory = false)
	private Photo profilePhoto;

	@JsonField(name="PREFERRED_LOCALE", mandatory = false, customValidator = LocaleValidator.class)
	protected String preferredLocale;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNricOrPassport() {
		return nricOrPassport;
	}

	public void setNricOrPassport(String nricOrPassport) {
		this.nricOrPassport = nricOrPassport;
	}

	public MobileNumber getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(MobileNumber mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPreferredCommunicationMethod() {
		return preferredCommunicationMethod;
	}

	public void setPreferredCommunicationMethod(String preferredCommunicationMethod) {
		this.preferredCommunicationMethod = preferredCommunicationMethod;
	}

	public String getHomeTel() {
		return homeTel;
	}

	public void setHomeTel(String homeTel) {
		this.homeTel = homeTel;
	}

	public String getOfficeTel() {
		return officeTel;
	}

	public void setOfficeTel(String officeTel) {
		this.officeTel = officeTel;
	}

	public Photo getProfilePhoto() {
		return profilePhoto;
	}

	public void setProfilePhoto(Photo profilePhoto) {
		this.profilePhoto = profilePhoto;
	}

	public String getPreferredLocale() {
		return preferredLocale;
	}

	public void setPreferredLocale(String preferredLocale) {
		this.preferredLocale = preferredLocale;
	}

	@Override
	public String toString() {
		return "EditProfileRequest [email=" + email + ", name=" + name + ", nricOrPassport=" + nricOrPassport + ", mobileNumber=" + mobileNumber
				+ ", preferredCommunicationMethod=" + preferredCommunicationMethod + ", homeTel=" + homeTel + ", officeTel=" + officeTel
				+ ", profilePhoto=" + profilePhoto + ", preferredLocale=" + preferredLocale + ", locale=" + locale + "]";
	}
}
