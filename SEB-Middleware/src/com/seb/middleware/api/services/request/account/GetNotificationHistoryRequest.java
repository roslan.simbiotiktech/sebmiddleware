package com.seb.middleware.api.services.request.account;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Paging;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.UserEmailWithStatusValidator;

public class GetNotificationHistoryRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = true, customValidator = UserEmailWithStatusValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "PAGING", mandatory = false)
	private Paging paging;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Paging getPaging() {
		return paging;
	}

	public void setPaging(Paging paging) {
		this.paging = paging;
	}

	@Override
	public String toString() {
		return "GetNotificationHistoryRequest [email=" + email + ", paging=" + paging + ", locale=" + locale + "]";
	}
}
