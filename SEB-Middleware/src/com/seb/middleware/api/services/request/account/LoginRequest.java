package com.seb.middleware.api.services.request.account;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.UserValidator;

public class LoginRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = false, customValidator = UserValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "PASSWORD", mandatory = false)
	private String password;

	@JsonField(name = "CLIENT_ID", mandatory = false, maxLength = 100)
	private String clientId;
	
	@JsonField(name = "REMEMBER_ME", mandatory = false)
	private Boolean rememberMe;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public Boolean getRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(Boolean rememberMe) {
		this.rememberMe = rememberMe;
	}

	@Override
	public String toString() {
		return "LoginRequest [email=" + email + ", clientId=" + clientId + ", rememberMe=" + rememberMe + ", locale="
				+ locale + "]";
	}
}
