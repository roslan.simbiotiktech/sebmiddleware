package com.seb.middleware.api.services.request.account;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;

public class LogoutRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = false, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "CLIENT_ID", mandatory = false, maxLength = 100)
	private String clientId;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "LogoutRequest [email=" + email + ", clientId=" + clientId + ", locale=" + locale + "]";
	}
}
