package com.seb.middleware.api.services.request.account;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.constant.PreferredCommunicationMethod;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.formatter.PhoneNumberFormatter;
import com.seb.middleware.validator.CommunicationMethodValidator;
import com.seb.middleware.validator.EmailValidator;
import com.seb.middleware.validator.PasswordValidator;
import com.seb.middleware.validator.PhoneNumberValidator;

public class PreverifyRegistrationRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = true, maxLength = 50, customValidator = EmailValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "PASSWORD", mandatory = true, customValidator = PasswordValidator.class)
	private String password;
	
	@JsonField(name = "NAME", mandatory = true, maxLength = 160)
	private String name;
	
	@JsonField(name = "MOBILE_NUMBER", mandatory = true, customValidator = PhoneNumberValidator.class, 
			customFormatter = PhoneNumberFormatter.class)
	private String mobileNumber;
	
	@JsonField(name = "PREFERRED_COMM_METHOD", mandatory = false, customValidator=CommunicationMethodValidator.class)
	private String preferredCommunicationMethod = PreferredCommunicationMethod.MOBILE.toString();

	@JsonField(name = "NRIC_OR_PASSPORT", mandatory = true, maxLength = 15)
	private String nricOrPassport;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPreferredCommunicationMethod() {
		return preferredCommunicationMethod;
	}

	public void setPreferredCommunicationMethod(String preferredCommunicationMethod) {
		this.preferredCommunicationMethod = preferredCommunicationMethod;
	}

	public String getNricOrPassport() {
		return nricOrPassport;
	}

	public void setNricOrPassport(String nricOrPassport) {
		this.nricOrPassport = nricOrPassport;
	}

	@Override
	public String toString() {
		return "PreverifyRegistrationRequest [email=" + email + ", name=" + name + ", mobileNumber=" + mobileNumber
				+ ", preferredCommunicationMethod=" + preferredCommunicationMethod + ", nricOrPassport=" + nricOrPassport + ", locale=" + locale
				+ "]";
	}
}
