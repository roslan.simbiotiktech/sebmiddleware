package com.seb.middleware.api.services.request.account;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.UserValidator;
import com.seb.middleware.validator.VerificationCodeValidator;

public class ReactivateAccountRequest extends Request {
	
	@JsonField(name = "EMAIL", mandatory = true, customValidator = UserValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "ACTIVATION_CODE", 
			mandatory = true, exactLength = 6, customValidator = VerificationCodeValidator.class)
	private String activationCode;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	@Override
	public String toString() {
		return "ReactivateAccountRequest [email=" + email + ", activationCode=" + activationCode + ", locale=" + locale + "]";
	}
}
