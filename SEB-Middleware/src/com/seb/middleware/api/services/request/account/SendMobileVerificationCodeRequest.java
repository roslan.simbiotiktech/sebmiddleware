package com.seb.middleware.api.services.request.account;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.PhoneNumberFormatter;
import com.seb.middleware.validator.PhoneNumberValidator;

public class SendMobileVerificationCodeRequest extends Request {

	@JsonField(name = "MOBILE_NUMBER", mandatory = true, customValidator = PhoneNumberValidator.class, 
			customFormatter = PhoneNumberFormatter.class)
	private String mobileNumber;

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Override
	public String toString() {
		return "SendMobileVerificationCodeRequest [mobileNumber=" + mobileNumber + ", locale=" + locale + "]";
	}
}
