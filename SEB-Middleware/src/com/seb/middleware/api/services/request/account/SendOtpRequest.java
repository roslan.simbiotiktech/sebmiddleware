package com.seb.middleware.api.services.request.account;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.CommunicationMethodValidator;
import com.seb.middleware.validator.UserEmailWithStatusValidator;

public class SendOtpRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = true, customValidator = UserEmailWithStatusValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "COMM_METHOD", mandatory = false, customValidator=CommunicationMethodValidator.class)
	private String communicationMethod;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCommunicationMethod() {
		return communicationMethod;
	}

	public void setCommunicationMethod(String communicationMethod) {
		this.communicationMethod = communicationMethod;
	}

	@Override
	public String toString() {
		return "SendOtpRequest [email=" + email + ", communicationMethod=" + communicationMethod + ", locale=" + locale + "]";
	}
}
