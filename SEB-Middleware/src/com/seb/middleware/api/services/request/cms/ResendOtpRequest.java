package com.seb.middleware.api.services.request.cms;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.OtpTypeValidator;
import com.seb.middleware.validator.UserValidator;

public class ResendOtpRequest extends Request {

	@JsonField(name = "LOGIN_ID", mandatory = true, customValidator = UserValidator.class, customFormatter = EmailFormatter.class)
	private String loginId;
	
	@JsonField(name = "TYPE", mandatory = false, customValidator = OtpTypeValidator.class )
	private String type;

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "ResendOtpRequest [loginId=" + loginId + ", type=" + type + ", locale=" + locale + "]";
	}
}
