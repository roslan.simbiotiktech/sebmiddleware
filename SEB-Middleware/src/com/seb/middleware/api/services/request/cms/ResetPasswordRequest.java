package com.seb.middleware.api.services.request.cms;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.UserValidator;

public class ResetPasswordRequest extends Request {

	@JsonField(name = "LOGIN_ID", mandatory = true, customValidator = UserValidator.class, customFormatter = EmailFormatter.class)
	private String loginId;

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	@Override
	public String toString() {
		return "ResetPasswordRequest [loginId=" + loginId + ", locale=" + locale + "]";
	}
}
