package com.seb.middleware.api.services.request.cms;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Paging;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.formatter.PhoneNumberFormatter;
import com.seb.middleware.validator.ContractAccountNumberValidator;
import com.seb.middleware.validator.PhoneNumberValidator;
import com.seb.middleware.validator.UserValidator;

public class SearchProfileRequest extends Request {

	@JsonField(name = "PAGING", mandatory = false)
	private Paging paging;
	
	@JsonField(name = "LOGIN_ID", mandatory = false, customValidator = UserValidator.class, customFormatter = EmailFormatter.class)
	private String loginId;
	
	@JsonField(name = "EMAIL", mandatory = false, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "NAME", mandatory = false, validateLength = true, minLength = 3)
	private String name;

	@JsonField(name = "MOBILE_NUMBER", mandatory = false, customValidator = PhoneNumberValidator.class, customFormatter = PhoneNumberFormatter.class)
	private String mobileNumber;
	
	@JsonField(name = "CONTRACT_ACC_NO", mandatory = false, customValidator = ContractAccountNumberValidator.class)
	private String contractAccountNo;

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getContractAccountNo() {
		return contractAccountNo;
	}

	public void setContractAccountNo(String contractAccountNo) {
		this.contractAccountNo = contractAccountNo;
	}

	public Paging getPaging() {
		return paging;
	}

	public void setPaging(Paging paging) {
		this.paging = paging;
	}

	@Override
	public String toString() {
		return "SearchProfileRequest [paging=" + paging + ", loginId=" + loginId + ", email=" + email + ", name=" + name + ", mobileNumber="
				+ mobileNumber + ", contractAccountNo=" + contractAccountNo + ", locale=" + locale + "]";
	}
}
