package com.seb.middleware.api.services.request.cms;

import java.util.Date;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Paging;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.formatter.PhoneNumberFormatter;
import com.seb.middleware.validator.ContractAccountNumberValidator;
import com.seb.middleware.validator.PhoneNumberValidator;
import com.seb.middleware.validator.UserValidator;

public class ViewCustomerHistoryRequest extends Request {

	@JsonField(name = "PAGING", mandatory = false)
	private Paging paging;
	
	@JsonField(name = "DATE_FROM", mandatory = true)
	private Date dateFrom;
	
	@JsonField(name = "DATE_TO", mandatory = true)
	private Date dateTo;
	
	@JsonField(name = "LOGIN_ID", mandatory = false, customValidator = UserValidator.class, customFormatter = EmailFormatter.class)
	private String loginId;
	
	@JsonField(name = "MOBILE_NUMBER", mandatory = false, customValidator = PhoneNumberValidator.class, customFormatter = PhoneNumberFormatter.class)
	private String mobileNumber;
	
	@JsonField(name = "CONTRACT_ACC_NO", mandatory = false, customValidator = ContractAccountNumberValidator.class)
	private String contractAccountNo;

	public Paging getPaging() {
		return paging;
	}

	public void setPaging(Paging paging) {
		this.paging = paging;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getContractAccountNo() {
		return contractAccountNo;
	}

	public void setContractAccountNo(String contractAccountNo) {
		this.contractAccountNo = contractAccountNo;
	}

	@Override
	public String toString() {
		return "ViewCustomerHistoryRequest [paging=" + paging + ", dateFrom=" + dateFrom + ", dateTo=" + dateTo + ", loginId=" + loginId
				+ ", mobileNumber=" + mobileNumber + ", contractAccountNo=" + contractAccountNo + ", locale=" + locale + "]";
	}
}
