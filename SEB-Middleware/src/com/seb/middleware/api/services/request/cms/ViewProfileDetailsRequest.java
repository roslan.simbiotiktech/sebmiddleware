package com.seb.middleware.api.services.request.cms;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.ContractAccountNumberValidator;
import com.seb.middleware.validator.UserValidator;

public class ViewProfileDetailsRequest extends Request {

	@JsonField(name = "LOGIN_ID", mandatory = true, customValidator = UserValidator.class, customFormatter = EmailFormatter.class)
	private String loginId;

	@JsonField(name = "CONTRACT_ACC_NO", mandatory = false, customValidator = ContractAccountNumberValidator.class)
	private String contractAccountNo;

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getContractAccountNo() {
		return contractAccountNo;
	}

	public void setContractAccountNo(String contractAccountNo) {
		this.contractAccountNo = contractAccountNo;
	}

	@Override
	public String toString() {
		return "ViewProfileDetailsRequest [loginId=" + loginId + ", contractAccountNo=" + contractAccountNo + ", locale=" + locale + "]";
	}
}
