package com.seb.middleware.api.services.request.cms.res;

public class VerificationAuthority {

	private boolean authorized;
	
	private String code;

	public boolean isAuthorized() {
		return authorized;
	}

	public void setAuthorized(boolean authorized) {
		this.authorized = authorized;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "VerificationAuthority [authorized=" + authorized + ", code=" + code + "]";
	}
}
