package com.seb.middleware.api.services.request.common;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.validator.CalculatorTypeValidator;

public class CalculateBillRequest extends Request{

	@JsonField(name = "TYPE", mandatory = true, customValidator = CalculatorTypeValidator.class)
	private String calculatorType;
	
	@JsonField(name = "KWH_CONSUMPTION", mandatory = true)
	private double kwhConsumption;
	
	@JsonField(name = "KVARH_CONSUMPTION", mandatory = false)
	private double kvarhConsumption;
	
	@JsonField(name = "KW_MAX_DEMAND_UNIT", mandatory = false)
	private double kwMaxDemandUnit;
	
	@JsonField(name = "OFFPEAK_KWH_CONSUMPTION", mandatory = false)
	private double offpeakKwhConsumption;
	
	@JsonField(name = "OFFPEAK_KVARH_CONSUMPTION", mandatory = false)
	private double offpeakKvarhConsumption;

	public String getCalculatorType() {
		return calculatorType;
	}

	public void setCalculatorType(String calculatorType) {
		this.calculatorType = calculatorType;
	}

	public double getKwhConsumption() {
		return kwhConsumption;
	}

	public void setKwhConsumption(double kwhConsumption) {
		this.kwhConsumption = kwhConsumption;
	}

	public double getKvarhConsumption() {
		return kvarhConsumption;
	}

	public void setKvarhConsumption(double kvarhConsumption) {
		this.kvarhConsumption = kvarhConsumption;
	}

	public double getKwMaxDemandUnit() {
		return kwMaxDemandUnit;
	}

	public void setKwMaxDemandUnit(double kwMaxDemandUnit) {
		this.kwMaxDemandUnit = kwMaxDemandUnit;
	}

	public double getOffpeakKwhConsumption() {
		return offpeakKwhConsumption;
	}

	public void setOffpeakKwhConsumption(double offpeakKwhConsumption) {
		this.offpeakKwhConsumption = offpeakKwhConsumption;
	}

	public double getOffpeakKvarhConsumption() {
		return offpeakKvarhConsumption;
	}

	public void setOffpeakKvarhConsumption(double offpeakKvarhConsumption) {
		this.offpeakKvarhConsumption = offpeakKvarhConsumption;
	}

	@Override
	public String toString() {
		return "CalculateBillRequest [calculatorType=" + calculatorType + ", kwhConsumption=" + kwhConsumption + ", kvarhConsumption="
				+ kvarhConsumption + ", kwMaxDemandUnit=" + kwMaxDemandUnit + ", offpeakKwhConsumption=" + offpeakKwhConsumption
				+ ", offpeakKvarhConsumption=" + offpeakKvarhConsumption + ", locale=" + locale + "]";
	}
}
