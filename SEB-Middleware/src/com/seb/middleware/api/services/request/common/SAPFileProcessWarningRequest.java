package com.seb.middleware.api.services.request.common;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;

public class SAPFileProcessWarningRequest extends Request{
	
	@JsonField(name = "FILE_NAME", mandatory = false, maxLength = 256)
	private String fileName;
	
	@JsonField(name = "DATE", mandatory = false)
	private String date;
	
	@JsonField(name = "DETAILS", mandatory = false, maxLength = 1024)
	private String details;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	@Override
	public String toString() {
		return "SAPFileProcessWarningRequest [fileName=" + fileName + ", date="
				+ date + ", details=" + details + ", locale=" + locale + "]";
	}
}
