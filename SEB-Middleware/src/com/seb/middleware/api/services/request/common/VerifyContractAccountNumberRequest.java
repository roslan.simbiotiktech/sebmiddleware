package com.seb.middleware.api.services.request.common;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.validator.ContractAccountNumberValidator;

public class VerifyContractAccountNumberRequest extends Request{

	@JsonField(name = "CONTRACT_ACC_NO", mandatory = true, 
			customValidator = ContractAccountNumberValidator.class)
	private String contractAccNo;

	public String getContractAccNo() {
		return contractAccNo;
	}

	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}

	@Override
	public String toString() {
		return "VerifyContractAccountNumberRequest [contractAccNo=" + contractAccNo + "]";
	}
}
