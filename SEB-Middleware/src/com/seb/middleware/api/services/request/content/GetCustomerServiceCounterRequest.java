package com.seb.middleware.api.services.request.content;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.validator.CSMainStationValidator;

public class GetCustomerServiceCounterRequest extends Request {

	@JsonField(name = "STATION", customValidator=CSMainStationValidator.class)
	private String station;

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	@Override
	public String toString() {
		return "GetCustomerServiceCounterRequest [station=" + station + ", locale=" + locale + "]";
	}
}
