package com.seb.middleware.api.services.request.content;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;

public class GetFAQRequest extends Request {

	@JsonField(name = "CATEGORY_KEY", mandatory = true)
	private String categoryKey;

	public String getCategoryKey() {
		return categoryKey;
	}

	public void setCategoryKey(String categoryKey) {
		this.categoryKey = categoryKey;
	}

	@Override
	public String toString() {
		return "GetFAQRequest [categoryKey=" + categoryKey + "]";
	}
	
}
