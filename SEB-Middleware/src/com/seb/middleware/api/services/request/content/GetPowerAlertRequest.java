package com.seb.middleware.api.services.request.content;

import java.util.Date;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Paging;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.validator.PowerAlertTypeValidator;

public class GetPowerAlertRequest extends Request {

	@JsonField(name = "PAGING", mandatory = false)
	private Paging paging;
	
	@JsonField(name = "ALERT_ID", mandatory = false)
	private long alertId;
	
	@JsonField(name = "ALERT_TYPE", customValidator=PowerAlertTypeValidator.class)
	private String alertType;
	
	@JsonField(name = "SEARCH_KEYWORD")
	private String searchKeyword;
	
	@JsonField(name = "SEARCH_DATE", dateFormat="yyyy-MM-dd")
	private Date searchDate;
	
	public Paging getPaging() {
		return paging;
	}

	public void setPaging(Paging paging) {
		this.paging = paging;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

	public String getAlertType() {
		return alertType;
	}

	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	public String getSearchKeyword() {
		return searchKeyword;
	}

	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}

	public Date getSearchDate() {
		return searchDate;
	}

	public void setSearchDate(Date searchDate) {
		this.searchDate = searchDate;
	}

	@Override
	public String toString() {
		return "GetPowerAlertRequest [paging=" + paging + ", alertId=" + alertId + ", alertType=" + alertType + ", searchKeyword=" + searchKeyword
				+ ", searchDate=" + searchDate + ", locale=" + locale + "]";
	}	
}
