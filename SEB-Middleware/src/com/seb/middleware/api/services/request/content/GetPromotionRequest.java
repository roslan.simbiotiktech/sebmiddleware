package com.seb.middleware.api.services.request.content;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Paging;
import com.seb.middleware.api.services.request.Request;

public class GetPromotionRequest extends Request {

	@JsonField(name = "PAGING", mandatory = false)
	private Paging paging;
	
	@JsonField(name = "PROMO_ID", mandatory = false)
	private long promoId;

	public long getPromoId() {
		return promoId;
	}

	public void setPromoId(long promoId) {
		this.promoId = promoId;
	}

	public Paging getPaging() {
		return paging;
	}

	public void setPaging(Paging paging) {
		this.paging = paging;
	}

	@Override
	public String toString() {
		return "GetPromotionRequest [paging=" + paging + ", promoId=" + promoId + ", locale=" + locale + "]";
	}
}
