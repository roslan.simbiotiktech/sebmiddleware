package com.seb.middleware.api.services.request.notification;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.validator.NotificationTypeValidator;

public class CommitNotificationRequest extends Request {

	@JsonField(name = "SUCCESS_ID", mandatory = false)
	private long [] successId;
	
	@JsonField(name = "UNSUCCESS_ID", mandatory = false)
	private long [] unsuccessId;
	
	@JsonField(name = "NOTIFICATION_TYPE", customValidator = NotificationTypeValidator.class, mandatory = true)
	private String notificationType;

	public long[] getSuccessId() {
		return successId;
	}

	public void setSuccessId(long[] successId) {
		this.successId = successId;
	}

	public long[] getUnsuccessId() {
		return unsuccessId;
	}

	public void setUnsuccessId(long[] unsuccessId) {
		this.unsuccessId = unsuccessId;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	@Override
	public String toString() {
		return "CommitNotificationRequest [successId=" + Arrays.toString(successId) + ", unsuccessId=" + Arrays.toString(unsuccessId)
				+ ", notificationType=" + notificationType + ", locale=" + locale + "]";
	}
}
