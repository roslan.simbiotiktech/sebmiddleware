package com.seb.middleware.api.services.request.notification;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.UserEmailValidator;

public class UnsubscribeLocationRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = true, maxLength = 50, customValidator = UserEmailValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "TAG", maxLength = 50)
	private String tag;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	@Override
	public String toString() {
		return "UnsubscribeLocationRequest [email=" + email + ", tag=" + tag + ", locale=" + locale + "]";
	}
}
