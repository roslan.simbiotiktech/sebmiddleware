package com.seb.middleware.api.services.request.payment;

import com.anteater.library.json.JsonField;
import com.seb.middleware.validator.ContractAccountNumberValidator;

public class PaymentEntry {

	@JsonField(name = "CONTRACT_ACC_NO", 
			mandatory = true, 
			customValidator = ContractAccountNumberValidator.class)
	protected String contractAccNo;
	
	@JsonField(name = "AMOUNT", mandatory = true)
	protected double amount;

	public String getContractAccNo() {
		return contractAccNo;
	}

	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "PaymentEntry [contractAccNo=" + contractAccNo + ", amount=" + amount + "]";
	}
}
