package com.seb.middleware.api.services.request.payment;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.EmailValidator;
import com.seb.middleware.validator.UserEmailWithStatusValidator;

public class PreparePaymentRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = true, maxLength = 50, 
			customValidator = UserEmailWithStatusValidator.class, customFormatter = EmailFormatter.class)
	protected String email;
	
	@JsonField(name = "NOTIFICATION_EMAIL", mandatory = false, maxLength = 50, 
			customValidator = EmailValidator.class, customFormatter = EmailFormatter.class)
	protected String notificationEmail;
	
	@JsonField(name = "ENTRIES", mandatory = true, minArrayLength = 1)
	protected PaymentEntry [] entries;
	
	@JsonField(name = "IS_MOBILE", mandatory = false)
	protected boolean isMobile;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNotificationEmail() {
		return notificationEmail;
	}

	public void setNotificationEmail(String notificationEmail) {
		this.notificationEmail = notificationEmail;
	}

	public PaymentEntry[] getEntries() {
		return entries;
	}

	public void setEntries(PaymentEntry[] entries) {
		this.entries = entries;
	}

	public boolean isMobile() {
		return isMobile;
	}

	public void setMobile(boolean isMobile) {
		this.isMobile = isMobile;
	}

	@Override
	public String toString() {
		return "PreparePaymentRequest [email=" + email + ", notificationEmail=" + notificationEmail + ", entries=" + Arrays.toString(entries)
				+ ", isMobile=" + isMobile + ", locale=" + locale + "]";
	}
}
