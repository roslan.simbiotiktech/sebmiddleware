package com.seb.middleware.api.services.request.payment.fpx;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.FPXReferenceValidator;
import com.seb.middleware.validator.UserEmailWithStatusValidator;

public class FPXCheckPaymentRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = true, maxLength = 50, customValidator = UserEmailWithStatusValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "REFERENCE_NO", mandatory = true, customValidator = FPXReferenceValidator.class)
	private String referenceNo;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	@Override
	public String toString() {
		return "FPXCheckPaymentRequest [email=" + email + ", referenceNo=" + referenceNo + ", locale=" + locale + "]";
	}
}
