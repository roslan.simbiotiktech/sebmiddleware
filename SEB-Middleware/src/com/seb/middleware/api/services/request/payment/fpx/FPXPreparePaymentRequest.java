package com.seb.middleware.api.services.request.payment.fpx;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.payment.PreparePaymentRequest;

public class FPXPreparePaymentRequest extends PreparePaymentRequest {

	@JsonField(name = "BANK_ID", mandatory = true)
	protected int bankId;

	public int getBankId() {
		return bankId;
	}

	public void setBankId(int bankId) {
		this.bankId = bankId;
	}

	@Override
	public String toString() {
		return "FPXPreparePaymentRequest [bankId=" + bankId + ", email=" + email + ", notificationEmail=" + notificationEmail + ", entries="
				+ Arrays.toString(entries) + ", isMobile=" + isMobile + ", locale=" + locale + "]";
	}
}
