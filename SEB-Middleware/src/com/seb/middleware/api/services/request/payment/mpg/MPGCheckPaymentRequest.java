package com.seb.middleware.api.services.request.payment.mpg;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.MPGReferenceValidator;
import com.seb.middleware.validator.UserEmailWithStatusValidator;

public class MPGCheckPaymentRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = true, maxLength = 50, customValidator = UserEmailWithStatusValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "REFERENCE_NO", mandatory = true, customValidator = MPGReferenceValidator.class)
	private String referenceNo;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	@Override
	public String toString() {
		return "MPGCheckPaymentRequest [email=" + email + ", referenceNo=" + referenceNo + ", locale=" + locale + "]";
	}
}
