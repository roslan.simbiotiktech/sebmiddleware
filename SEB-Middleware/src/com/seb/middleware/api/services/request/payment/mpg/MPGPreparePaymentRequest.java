package com.seb.middleware.api.services.request.payment.mpg;

import java.util.Arrays;
import java.util.Date;

import com.anteater.library.json.JsonField;
import com.anteater.library.json.validator.OnlyNumberValidator;
import com.seb.middleware.api.services.request.payment.PreparePaymentRequest;
import com.seb.middleware.constant.MPGCardType;

public class MPGPreparePaymentRequest extends PreparePaymentRequest {

	@JsonField(name = "CARD_TYPE", mandatory = true)
	protected MPGCardType cardType;
	
	@JsonField(name = "CARD_NO", mandatory = true, minLength = 15, maxLength = 16, customValidator = OnlyNumberValidator.class)
	protected String cardNumber;
	
	@JsonField(name = "CARD_NAME", mandatory = true, minLength = 1, maxLength = 50)
	protected String cardName;
	
	@JsonField(name = "CARD_EXPIRY", mandatory = true, dateFormat = "yyyyMM")
	protected Date cardExpiry;
	
	@JsonField(name = "CARD_CVC", mandatory = true, customValidator = OnlyNumberValidator.class)
	protected String cardCvc;

	public MPGCardType getCardType() {
		return cardType;
	}

	public void setCardType(MPGCardType cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public Date getCardExpiry() {
		return cardExpiry;
	}

	public void setCardExpiry(Date cardExpiry) {
		this.cardExpiry = cardExpiry;
	}

	public String getCardCvc() {
		return cardCvc;
	}

	public void setCardCvc(String cardCvc) {
		this.cardCvc = cardCvc;
	}

	@Override
	public String toString() {
		return "MPGPreparePaymentRequest [cardType=" + cardType + ", cardNumber=" + cardNumber + ", cardName=" + cardName + ", cardExpiry="
				+ cardExpiry + ", cardCvc=" + cardCvc + ", email=" + email + ", notificationEmail=" + notificationEmail + ", entries="
				+ Arrays.toString(entries) + ", isMobile=" + isMobile + ", locale=" + locale + "]";
	}
}
