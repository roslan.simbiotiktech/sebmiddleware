package com.seb.middleware.api.services.request.report;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.UserEmailWithStatusValidator;

public class HistoryDetailRequest extends Request {

	@JsonField(name="EMAIL", mandatory=true, maxLength=50, customValidator=UserEmailWithStatusValidator.class, customFormatter=EmailFormatter.class)
	private String email;
	
	@JsonField(name="TRANS_ID", mandatory=true, maxLength=10)
	private String transId;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	@Override
	public String toString() {
		return "HistoryDetailRequest [email=" + email + ", transId=" + transId + ", locale=" + locale + "]";
	}
}
