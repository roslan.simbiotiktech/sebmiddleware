package com.seb.middleware.api.services.request.report;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.UserEmailWithStatusValidator;

public class HistoryListingRequest extends Request {
	
	@JsonField(name="EMAIL", mandatory=true, maxLength=50, customValidator=UserEmailWithStatusValidator.class, customFormatter=EmailFormatter.class)
	private String email;
	
	@JsonField(name="CASE_NUMBER", mandatory = false, 
			maxLength=50)
	private String caseNumber;
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	@Override
	public String toString() {
		return "HistoryListingRequest [email=" + email + ", caseNumber=" + caseNumber + ", locale=" + locale + "]";
	}
}
