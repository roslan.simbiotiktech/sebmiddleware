package com.seb.middleware.api.services.request.report;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Location;
import com.seb.middleware.api.services.request.Photo;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.ContractAccountNumberValidator;
import com.seb.middleware.validator.IssueTypeValidator;
import com.seb.middleware.validator.ReportCategoryValidator;
import com.seb.middleware.validator.ReportTypeValidator;
import com.seb.middleware.validator.StationValidator;
import com.seb.middleware.validator.UserEmailWithStatusValidator;

public class MakeReportRequest extends Request {

	@JsonField(name="EMAIL", mandatory=true, maxLength=50, customValidator=UserEmailWithStatusValidator.class, customFormatter=EmailFormatter.class)
	private String email;
	
	@JsonField(name="CHANNEL", mandatory=true, maxLength=20)
	private String channel;
	
	@JsonField(name="CLIENT_OS", mandatory=true, maxLength=255)
	private String clientOs;
	
	@JsonField(name="TYPE", localizationKey="report_type", mandatory=true, customValidator=ReportTypeValidator.class)
	private String reportType;
	
	@JsonField(name="ISSUE_TYPE", localizationKey="issue_type", mandatory=false, customValidator=IssueTypeValidator.class)
	private String issueType;
	
	@JsonField(name="CATEGORY", customValidator=ReportCategoryValidator.class)
	private String reportCategory;
	
	@JsonField(name="STATION", customValidator=StationValidator.class)
	private String station;
	
	@JsonField(name="DESCRIPTION", mandatory=true, maxLength=300)
	private String description;
	
	@JsonField(name="LOCATION")
	private Location location;
	
	@JsonField(name="CONTRACT_ACC_NO", customValidator=ContractAccountNumberValidator.class)
	private String contractAccNo;
	
	@JsonField(name="PHOTOS", validateArrayLength=true, minArrayLength=0, maxArrayLength=3)
	private Photo[] photos;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getClientOs() {
		return clientOs;
	}

	public void setClientOs(String clientOs) {
		this.clientOs = clientOs;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getIssueType() {
		return issueType;
	}

	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}

	public String getReportCategory() {
		return reportCategory;
	}

	public void setReportCategory(String reportCategory) {
		this.reportCategory = reportCategory;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getContractAccNo() {
		return contractAccNo;
	}

	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}

	public Photo[] getPhotos() {
		return photos;
	}

	public void setPhotos(Photo[] photos) {
		this.photos = photos;
	}

	@Override
	public String toString() {
		return "MakeReportRequest [email=" + email + ", channel=" + channel + ", clientOs=" + clientOs + ", reportType="
				+ reportType + ", issueType=" + issueType + ", reportCategory=" + reportCategory + ", station="
				+ station + ", description=" + description + ", location=" + location + ", contractAccNo="
				+ contractAccNo + ", photos=" + Arrays.toString(photos) + ", locale=" + locale + "]";
	}
}
