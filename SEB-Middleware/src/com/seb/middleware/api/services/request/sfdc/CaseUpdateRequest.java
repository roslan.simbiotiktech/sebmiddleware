package com.seb.middleware.api.services.request.sfdc;

import com.anteater.library.json.JsonField;
import com.seb.middleware.validator.SFDCReportStatusValidator;

public class CaseUpdateRequest extends SfdcRequest {

	@JsonField(name = "case_record_id", mandatory = true, validateLength = true, maxLength = 50)
	protected String caseRecordId;
	
	@JsonField(name = "status", mandatory = true, customValidator = SFDCReportStatusValidator.class,
			validateLength = true, maxLength = 50)
	protected String status;
	
	@JsonField(name = "sub_status", mandatory = false, validateLength = true, maxLength = 50)
	protected String subStatus;
	
	@JsonField(name = "remarks", mandatory = false, maxLength = 500)
	protected String remarks;

	public String getCaseRecordId() {
		return caseRecordId;
	}

	public void setCaseRecordId(String caseRecordId) {
		this.caseRecordId = caseRecordId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "CaseUpdateRequest [caseRecordId=" + caseRecordId + ", status=" + status + ", subStatus=" + subStatus
				+ ", remarks=" + remarks + "]";
	}
}
