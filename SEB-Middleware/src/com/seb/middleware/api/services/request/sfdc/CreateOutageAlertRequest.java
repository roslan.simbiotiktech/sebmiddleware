package com.seb.middleware.api.services.request.sfdc;

import java.util.Date;

import com.anteater.library.json.JsonField;
import com.seb.middleware.validator.SFDCAlertTypeValidator;
import com.seb.middleware.validator.StationValidator;

public class CreateOutageAlertRequest extends SfdcRequest {

	@JsonField(name = "announcement_record_id", mandatory = true, validateLength = true, maxLength = 50)
	protected String announcementRecordId;
	
	@JsonField(name = "type", mandatory = true, customValidator = SFDCAlertTypeValidator.class,
			validateLength = true, maxLength = 50)
	protected String type;
	
	@JsonField(name = "station", mandatory = true, validateLength = true, maxLength = 50, customValidator = StationValidator.class)
	protected String station;
	
	@JsonField(name = "area", mandatory = true, maxLength = 500)
	protected String area;
	
	@JsonField(name = "title", mandatory = true, maxLength = 200)
	protected String title;
	
	@JsonField(name = "causes", mandatory = true, maxLength = 500)
	protected String causes;
	
	@JsonField(name = "unplanned_restoration_date", mandatory = false, dateTimeFormat = "yyyy/MM/dd HH:mm:ss", dateFormat = "yyyy/MM/dd")
	protected Date unplannedRestorationDate;
	
	@JsonField(name = "scheduled_maintenance_start_date", mandatory = false, dateTimeFormat = "yyyy/MM/dd HH:mm:ss", dateFormat = "yyyy/MM/dd")
	protected Date scheduledMaintenanceStartDate;
	
	@JsonField(name = "scheduled_maintenance_end_date", mandatory = false, dateTimeFormat = "yyyy/MM/dd HH:mm:ss", dateFormat = "yyyy/MM/dd")
	protected Date scheduledMaintenanceEndDate;
	
	@JsonField(name = "push_notification", mandatory = true)
	protected boolean pushNotification;
	
	@JsonField(name = "push_date", mandatory = false, dateTimeFormat = "yyyy/MM/dd HH:mm:ss", dateFormat = "yyyy/MM/dd")
	protected Date pushDate;
	
	@JsonField(name = "push_message", mandatory = false, maxLength = 120)
	protected String pushMessage;

	public String getAnnouncementRecordId() {
		return announcementRecordId;
	}

	public void setAnnouncementRecordId(String announcementRecordId) {
		this.announcementRecordId = announcementRecordId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCauses() {
		return causes;
	}

	public void setCauses(String causes) {
		this.causes = causes;
	}

	public Date getUnplannedRestorationDate() {
		return unplannedRestorationDate;
	}

	public void setUnplannedRestorationDate(Date unplannedRestorationDate) {
		this.unplannedRestorationDate = unplannedRestorationDate;
	}

	public Date getScheduledMaintenanceStartDate() {
		return scheduledMaintenanceStartDate;
	}

	public void setScheduledMaintenanceStartDate(Date scheduledMaintenanceStartDate) {
		this.scheduledMaintenanceStartDate = scheduledMaintenanceStartDate;
	}

	public Date getScheduledMaintenanceEndDate() {
		return scheduledMaintenanceEndDate;
	}

	public void setScheduledMaintenanceEndDate(Date scheduledMaintenanceEndDate) {
		this.scheduledMaintenanceEndDate = scheduledMaintenanceEndDate;
	}

	public boolean isPushNotification() {
		return pushNotification;
	}

	public void setPushNotification(boolean pushNotification) {
		this.pushNotification = pushNotification;
	}

	public Date getPushDate() {
		return pushDate;
	}

	public void setPushDate(Date pushDate) {
		this.pushDate = pushDate;
	}

	public String getPushMessage() {
		return pushMessage;
	}

	public void setPushMessage(String pushMessage) {
		this.pushMessage = pushMessage;
	}

	@Override
	public String toString() {
		return "CreateOutageAlertRequest [announcementRecordId=" + announcementRecordId + ", type=" + type
				+ ", station=" + station + ", area=" + area + ", title=" + title + ", causes=" + causes
				+ ", unplannedRestorationDate=" + unplannedRestorationDate + ", scheduledMaintenanceStartDate="
				+ scheduledMaintenanceStartDate + ", scheduledMaintenanceEndDate=" + scheduledMaintenanceEndDate
				+ ", pushNotification=" + pushNotification + ", pushDate=" + pushDate + ", pushMessage=" + pushMessage
				+ ", clientId=" + clientId + ", clientSecret=" + clientSecret + ", locale=" + locale + "]";
	}
}
