package com.seb.middleware.api.services.request.sfdc;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;

public class SfdcRequest extends Request {

	@JsonField(name = "client_id", mandatory = true, maxLength = 50)
	protected String clientId;

	@JsonField(name = "client_secret", mandatory = true, maxLength = 50)
	protected String clientSecret;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}

	@Override
	public String toString() {
		return "SfdcRequest [clientId=" + clientId + ", clientSecret=" + clientSecret + "]";
	}
}
