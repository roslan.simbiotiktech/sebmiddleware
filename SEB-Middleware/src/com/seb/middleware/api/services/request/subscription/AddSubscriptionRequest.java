package com.seb.middleware.api.services.request.subscription;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.ContractAccountNumberValidator;
import com.seb.middleware.validator.SubscriptionTypeValidator;
import com.seb.middleware.validator.UserEmailWithStatusValidator;

public class AddSubscriptionRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = true, maxLength = 50, customValidator = UserEmailWithStatusValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "CONTRACT_ACC_NO", mandatory = true, customValidator = ContractAccountNumberValidator.class)
	private String contractAccNo;
	
	@JsonField(name = "CONTRACT_ACC_NAME", mandatory = true, validateLength = true, maxLength = 100)
	private String contractAccName;
	
	@JsonField(name = "CONTRACT_ACC_NICK", mandatory = true, validateLength = true, maxLength = 30)
	private String contractAccNick;
	
	@JsonField(name = "SUBS_TYPE", mandatory = true, customValidator = SubscriptionTypeValidator.class)
	private String subsType;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContractAccNo() {
		return contractAccNo;
	}

	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}

	public String getContractAccName() {
		return contractAccName;
	}

	public void setContractAccName(String contractAccName) {
		this.contractAccName = contractAccName;
	}

	public String getContractAccNick() {
		return contractAccNick;
	}

	public void setContractAccNick(String contractAccNick) {
		this.contractAccNick = contractAccNick;
	}

	public String getSubsType() {
		return subsType;
	}

	public void setSubsType(String subsType) {
		this.subsType = subsType;
	}

	@Override
	public String toString() {
		return "AddSubscriptionRequest [email=" + email + ", contractAccNo=" + contractAccNo + ", contractAccName=" + contractAccName
				+ ", contractAccNick=" + contractAccNick + ", subsType=" + subsType + ", locale=" + locale + "]";
	}
}
