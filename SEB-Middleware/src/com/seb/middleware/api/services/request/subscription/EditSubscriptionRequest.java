package com.seb.middleware.api.services.request.subscription;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.ContractAccountNumberValidator;
import com.seb.middleware.validator.SubscriptionTypeValidator;
import com.seb.middleware.validator.UserEmailWithStatusValidator;

public class EditSubscriptionRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = true, maxLength = 50, customValidator = UserEmailWithStatusValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "CONTRACT_ACC_NO", mandatory = true, customValidator = ContractAccountNumberValidator.class)
	private String contractAccNo;
	
	@JsonField(name = "CONTRACT_ACC_NICK", mandatory = true, validateLength = true, maxLength = 30)
	private String contractAccNick;
	
	@JsonField(name = "SUBS_TYPE", customValidator = SubscriptionTypeValidator.class)
	private String subsType;
	
	@JsonField(name = "NOTIFY_BY_EMAIL", mandatory = false)
	private Boolean notifyByEmail;
	
	@JsonField(name = "NOTIFY_BY_SMS", mandatory = false)
	private Boolean notifyBySMS;
	
	@JsonField(name = "NOTIFY_BY_MOBILE_PUSH", mandatory = false)
	private Boolean notifyByMobilePush;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContractAccNo() {
		return contractAccNo;
	}

	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}

	public String getContractAccNick() {
		return contractAccNick;
	}

	public void setContractAccNick(String contractAccNick) {
		this.contractAccNick = contractAccNick;
	}

	public String getSubsType() {
		return subsType;
	}

	public void setSubsType(String subsType) {
		this.subsType = subsType;
	}

	public Boolean getNotifyByEmail() {
		return notifyByEmail;
	}

	public void setNotifyByEmail(Boolean notifyByEmail) {
		this.notifyByEmail = notifyByEmail;
	}

	public Boolean getNotifyBySMS() {
		return notifyBySMS;
	}

	public void setNotifyBySMS(Boolean notifyBySMS) {
		this.notifyBySMS = notifyBySMS;
	}

	public Boolean getNotifyByMobilePush() {
		return notifyByMobilePush;
	}

	public void setNotifyByMobilePush(Boolean notifyByMobilePush) {
		this.notifyByMobilePush = notifyByMobilePush;
	}

	@Override
	public String toString() {
		return "EditSubscriptionRequest [email=" + email + ", contractAccNo=" + contractAccNo + ", contractAccNick="
				+ contractAccNick + ", subsType=" + subsType + ", notifyByEmail=" + notifyByEmail + ", notifyBySMS="
				+ notifyBySMS + ", notifyByMobilePush=" + notifyByMobilePush + ", locale=" + locale + "]";
	}
}
