package com.seb.middleware.api.services.request.subscription;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.ContractAccountNumberValidator;
import com.seb.middleware.validator.UserEmailWithStatusValidator;

public class GetConsumptionRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = true, customValidator = UserEmailWithStatusValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "CONTRACT_ACC_NO", mandatory = true, customValidator = ContractAccountNumberValidator.class)
	private String contractAccNo;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContractAccNo() {
		return contractAccNo;
	}

	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}

	@Override
	public String toString() {
		return "GetConsumptionRequest [email=" + email + ", contractAccNo=" + contractAccNo + ", locale=" + locale + "]";
	}	
}
