package com.seb.middleware.api.services.request.subscription;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.ContractAccountNumberValidator;
import com.seb.middleware.validator.UserEmailWithStatusValidator;

public class GetSubscriptionPaymentHistoryRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = true, maxLength = 50, customValidator = UserEmailWithStatusValidator.class, customFormatter = EmailFormatter.class)
	private String email;
	
	@JsonField(name = "CONTRACT_ACC_NO", mandatory = true, customValidator = ContractAccountNumberValidator.class)
	private String contractAccNo;
	
	@JsonField(name = "MODE", mandatory = false, description = "1 = Successful, 2 = Failed, 3 = Pending Auth")
	public String mode;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContractAccNo() {
		return contractAccNo;
	}

	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	@Override
	public String toString() {
		return "GetSubscriptionPaymentHistoryRequest [email=" + email + ", contractAccNo=" + contractAccNo + ", mode=" + mode + ", locale=" + locale
				+ "]";
	}
}
