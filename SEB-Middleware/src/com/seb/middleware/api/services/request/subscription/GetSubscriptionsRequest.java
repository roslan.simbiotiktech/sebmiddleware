package com.seb.middleware.api.services.request.subscription;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.formatter.EmailFormatter;
import com.seb.middleware.validator.EmailValidator;

public class GetSubscriptionsRequest extends Request {

	@JsonField(name = "EMAIL", mandatory = true, maxLength = 50, customValidator = EmailValidator.class, customFormatter = EmailFormatter.class)
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "GetSubscriptionsRequest [email=" + email + ", locale=" + locale
				+ "]";
	}
	
}
