package com.seb.middleware.api.services.response;

import java.util.Arrays;

import com.anteater.library.json.JsonField;

public class Error {

	@JsonField(name = "ERR_CODE")
	private String code;
	
	@JsonField(name = "ERR_DESC")
	private String [] messages;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String[] getMessages() {
		return messages;
	}

	public void setMessages(String[] messages) {
		this.messages = messages;
	}

	@Override
	public String toString() {
		return "Error [code=" + code + ", messages=" + Arrays.toString(messages) + "]";
	}	
}
