package com.seb.middleware.api.services.response;

import com.anteater.library.json.JsonField;

public class Location {
	
	@JsonField(name = "LATITUDE")
	private String latitude;
	
	@JsonField(name = "LONGITUDE")
	private String longitude;
	
	@JsonField(name = "ADDRESS_SHORT")
	private String address;
	
	public Location() {
		super();
	}

	public Location(String latitude, String longitude, String address) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.address = address;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Location [latitude=" + latitude + ", longitude=" + longitude + ", address=" + address + "]";
	}
}
