package com.seb.middleware.api.services.response;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.request.Paging;

public class Pagination {

	@JsonField(name = "PAGING")
	private Paging paging;
	
	@JsonField(name = "MAX_PAGE")
	private int maxPage;
	
	@JsonField(name = "TOTAL_RECORDS")
	private int totalRecords;

	public int calculateMaxPage(int totalRecord, int recordPerPage){
		double tr = totalRecord;
		double rpp = recordPerPage;
		return (int)Math.ceil(tr/rpp);
	}
	
	public Paging getPaging() {
		return paging;
	}

	public void setPaging(Paging paging) {
		this.paging = paging;
	}

	public int getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(int maxPage) {
		this.maxPage = maxPage;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	@Override
	public String toString() {
		return "Pagination [paging=" + paging + ", maxPage=" + maxPage + ", totalRecords=" + totalRecords + "]";
	}	
}
