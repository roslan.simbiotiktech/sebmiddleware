package com.seb.middleware.api.services.response;

import com.anteater.library.json.JsonField;
import com.seb.middleware.utility.StringUtil;

public class Photo {
	
	@JsonField(name = "DATA_TYPE")
	private String dataType;
	
	@JsonField(name = "DATA")
	private String data;

	public Photo(){
		
	}
	
	public Photo(String dataType, String data) {
		super();
		this.dataType = dataType;
		this.data = data;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Photo [dataType=" + dataType + ", data (size)=" + StringUtil.getHumanReadableFileSizeFromBase64(data) + "]";
	}
}
