package com.seb.middleware.api.services.response;

import com.anteater.library.json.JsonField;

public class Response {

	@JsonField(name = "STATUS")
	protected ResponseStatus responseStatus;

	public Response(){
		responseStatus = new ResponseStatus();
	}
	
	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}

	@Override
	public String toString() {
		return "Response [responseStatus=" + responseStatus + "]";
	}
}
