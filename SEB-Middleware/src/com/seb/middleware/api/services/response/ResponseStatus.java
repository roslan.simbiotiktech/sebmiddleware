package com.seb.middleware.api.services.response;

import com.anteater.library.json.JsonField;

public class ResponseStatus {

	@JsonField(name = "RESP_STAT")
	private int status = -1;
	
	@JsonField(name = "ERROR")
	private Error error;

	public ResponseStatus(boolean success){
		if(success) status = 0;
		else status = -1;
	}
	
	public ResponseStatus(){
		status = -1;
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return "ResponseStatus [status=" + status + ", error=" + error + "]";
	}
}
