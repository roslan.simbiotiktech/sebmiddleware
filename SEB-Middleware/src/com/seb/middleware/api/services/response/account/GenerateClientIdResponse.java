package com.seb.middleware.api.services.response.account;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GenerateClientIdResponse extends Response{

	@JsonField(name = "CLIENT_ID")
	private String clientId;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@Override
	public String toString() {
		return "GenerateClientIdResponse [clientId=" + clientId + ", responseStatus=" + responseStatus + "]";
	}
}
