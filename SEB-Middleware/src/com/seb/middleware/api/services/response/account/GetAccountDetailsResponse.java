package com.seb.middleware.api.services.response.account;

import java.util.Date;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Photo;
import com.seb.middleware.api.services.response.Response;

public class GetAccountDetailsResponse extends Response{

	@JsonField(name = "EMAIL")
	private String email;
	
	@JsonField(name = "NAME")
	private String name;
	
	@JsonField(name = "NRIC_OR_PASSPORT")
	private String nricOrPassport;
	
	@JsonField(name = "MOBILE_NUMBER")
	private String mobileNumber;
	
	@JsonField(name = "PREFERRED_COMM_METHOD")
	private String preferredCommMethod;
	
	@JsonField(name = "HOME_TEL")
	private String homeTel;
	
	@JsonField(name = "OFFICE_TEL")
	private String officeTel;
	
	@JsonField(name = "PREFERRED_LOCALE")
	private String preferredLocale;
	
	@JsonField(name = "PROMPT")
	private String prompt;
	
	@JsonField(name = "PHOTO")
	private Photo profilePhoto;
	
	@JsonField(name = "LAST_LOGIN_AT")
	private Date lastLogin;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNricOrPassport() {
		return nricOrPassport;
	}

	public void setNricOrPassport(String nricOrPassport) {
		this.nricOrPassport = nricOrPassport;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPreferredCommMethod() {
		return preferredCommMethod;
	}

	public void setPreferredCommMethod(String preferredCommMethod) {
		this.preferredCommMethod = preferredCommMethod;
	}

	public String getHomeTel() {
		return homeTel;
	}

	public void setHomeTel(String homeTel) {
		this.homeTel = homeTel;
	}

	public String getOfficeTel() {
		return officeTel;
	}

	public void setOfficeTel(String officeTel) {
		this.officeTel = officeTel;
	}

	public String getPreferredLocale() {
		return preferredLocale;
	}

	public void setPreferredLocale(String preferredLocale) {
		this.preferredLocale = preferredLocale;
	}

	public String getPrompt() {
		return prompt;
	}

	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}

	public Photo getProfilePhoto() {
		return profilePhoto;
	}

	public void setProfilePhoto(Photo profilePhoto) {
		this.profilePhoto = profilePhoto;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	@Override
	public String toString() {
		return "GetAccountDetailsResponse [email=" + email + ", name=" + name + ", nricOrPassport=" + nricOrPassport + ", mobileNumber="
				+ mobileNumber + ", preferredCommMethod=" + preferredCommMethod + ", homeTel=" + homeTel + ", officeTel=" + officeTel
				+ ", preferredLocale=" + preferredLocale + ", prompt=" + prompt + ", profilePhoto=" + profilePhoto + ", lastLogin=" + lastLogin
				+ ", responseStatus=" + responseStatus + "]";
	}
}
