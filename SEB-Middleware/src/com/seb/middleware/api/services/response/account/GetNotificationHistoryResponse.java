package com.seb.middleware.api.services.response.account;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Pagination;
import com.seb.middleware.api.services.response.Response;

public class GetNotificationHistoryResponse extends Response{

	@JsonField(name="NOTIFICATIONS")
	private Notification [] notifications;

	@JsonField(name = "PAGINATION")
	private Pagination pagination;
	
	@JsonField(name = "UNREAD_COUNT")
	private int unreadCount;
	
	public Notification[] getNotifications() {
		return notifications;
	}

	public void setNotifications(Notification[] notifications) {
		this.notifications = notifications;
	}

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

	public int getUnreadCount() {
		return unreadCount;
	}

	public void setUnreadCount(int unreadCount) {
		this.unreadCount = unreadCount;
	}

	@Override
	public String toString() {
		return "GetNotificationHistoryResponse [notifications=" + Arrays.toString(notifications) + ", pagination=" + pagination + ", unreadCount="
				+ unreadCount + ", responseStatus=" + responseStatus + "]";
	}
}
