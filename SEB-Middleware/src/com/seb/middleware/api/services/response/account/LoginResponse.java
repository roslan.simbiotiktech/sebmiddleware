package com.seb.middleware.api.services.response.account;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class LoginResponse extends Response{

	@JsonField(name = "EMAIL")
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "LoginResponse [email=" + email + ", responseStatus=" + responseStatus + "]";
	}
}
