package com.seb.middleware.api.services.response.account;

import java.util.Date;

import com.anteater.library.json.JsonField;

public class Notification {
	
	@JsonField(name = "NOTIFICATION_ID")
	private String notificationId;
	
	@JsonField(name = "NOTIFY_DATETIME")
	private Date notifyDatetime;
	
	@JsonField(name = "MESSAGE")
	private String message;
	
	@JsonField(name = "TITLE")
	private String title;
	
	@JsonField(name = "TYPE")
	private String type;
	
	@JsonField(name = "ASSOCIATED_ID")
	private String associatedId;
	
	@JsonField(name = "STATUS")
	private String status;

	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}

	public Date getNotifyDatetime() {
		return notifyDatetime;
	}

	public void setNotifyDatetime(Date notifyDatetime) {
		this.notifyDatetime = notifyDatetime;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAssociatedId() {
		return associatedId;
	}

	public void setAssociatedId(String associatedId) {
		this.associatedId = associatedId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Notification [notificationId=" + notificationId
				+ ", notifyDatetime=" + notifyDatetime + ", message=" + message
				+ ", title=" + title + ", type=" + type + ", associatedId="
				+ associatedId + ", status=" + status + "]";
	}
}
