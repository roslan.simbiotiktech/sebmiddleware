package com.seb.middleware.api.services.response.account;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class SendOtpResponse extends Response{
	@JsonField(name = "PREFERRED_COMM_METHOD")
	private String preferredCommMethod;

	public String getPreferredCommMethod() {
		return preferredCommMethod;
	}

	public void setPreferredCommMethod(String preferredCommMethod) {
		this.preferredCommMethod = preferredCommMethod;
	}

	@Override
	public String toString() {
		return "SendOtpResponse [preferredCommMethod=" + preferredCommMethod + ", responseStatus=" + responseStatus + "]";
	}
}
