package com.seb.middleware.api.services.response.cms;

import java.util.Arrays;
import java.util.Date;

import com.anteater.library.json.JsonField;

public class AuditTrail {

	@JsonField(name = "LOGIN_ID")
	private String loginId;
	
	@JsonField(name = "MOBILE_NUMBER")
	private String mobileNumber;
	
	@JsonField(name = "ACTIVITY")
	private String activity;
	
	@JsonField(name = "ACITIVTY_AT")
	private Date activityAt;
	
	@JsonField(name = "CONTRACT_SUBSCRIBED")
	private Contract[] contractSubscribed;

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Date getActivityAt() {
		return activityAt;
	}

	public void setActivityAt(Date activityAt) {
		this.activityAt = activityAt;
	}

	public Contract[] getContractSubscribed() {
		return contractSubscribed;
	}

	public void setContractSubscribed(Contract[] contractSubscribed) {
		this.contractSubscribed = contractSubscribed;
	}

	@Override
	public String toString() {
		return "AuditTrail [loginId=" + loginId + ", mobileNumber=" + mobileNumber + ", activity=" + activity + ", activityAt=" + activityAt
				+ ", contractSubscribed=" + Arrays.toString(contractSubscribed) + "]";
	}
}
