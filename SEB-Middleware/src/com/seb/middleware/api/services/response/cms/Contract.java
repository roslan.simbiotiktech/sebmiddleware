package com.seb.middleware.api.services.response.cms;

import com.anteater.library.json.JsonField;

public class Contract {

	@JsonField(name = "ACCOUNT_NUMBER")
	protected String accountNumber;
	
	@JsonField(name = "ACCOUNT_NAME")
	protected String accountName;
	
	@JsonField(name = "SUBSCRIPTION_STATUS")
	protected String status;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Contract [accountNumber=" + accountNumber + ", accountName=" + accountName + ", status=" + status + "]";
	}
}
