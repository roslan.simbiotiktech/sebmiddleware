package com.seb.middleware.api.services.response.cms;

import java.util.Date;

import com.anteater.library.json.JsonField;

public class ContractDetail extends Contract {

	@JsonField(name = "ACCOUNT_NICK")
	private String accountNick;
	
	@JsonField(name = "SUBSCRIPTION_TYPE")
	private String subsriptionType;
	
	@JsonField(name = "SUBSCRIBED_AT")
	private Date subsriptionAt;

	@JsonField(name = "REMARK")
	private String remark;
	
	public String getAccountNick() {
		return accountNick;
	}

	public void setAccountNick(String accountNick) {
		this.accountNick = accountNick;
	}

	public String getSubsriptionType() {
		return subsriptionType;
	}

	public void setSubsriptionType(String subsriptionType) {
		this.subsriptionType = subsriptionType;
	}

	public Date getSubsriptionAt() {
		return subsriptionAt;
	}

	public void setSubsriptionAt(Date subsriptionAt) {
		this.subsriptionAt = subsriptionAt;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "ContractDetail [accountNick=" + accountNick
				+ ", subsriptionType=" + subsriptionType + ", subsriptionAt="
				+ subsriptionAt + ", remark=" + remark + ", accountNumber="
				+ accountNumber + ", accountName=" + accountName + ", status="
				+ status + "]";
	}
}
