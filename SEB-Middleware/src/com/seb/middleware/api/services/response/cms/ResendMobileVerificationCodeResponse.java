package com.seb.middleware.api.services.response.cms;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class ResendMobileVerificationCodeResponse extends Response{
	
	@JsonField(name = "OTP")
	private String otp;

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	@Override
	public String toString() {
		return "ResendMobileVerificationCodeResponse [otp=" + otp + ", responseStatus=" + responseStatus + "]";
	}
}
