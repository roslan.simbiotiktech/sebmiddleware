package com.seb.middleware.api.services.response.cms;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class ResendOtpResponse extends Response{
	@JsonField(name = "PREFERRED_COMM_METHOD")
	private String preferredCommMethod;
	
	@JsonField(name = "OTP")
	private String otp;

	public String getPreferredCommMethod() {
		return preferredCommMethod;
	}

	public void setPreferredCommMethod(String preferredCommMethod) {
		this.preferredCommMethod = preferredCommMethod;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	@Override
	public String toString() {
		return "ResendOtpResponse [preferredCommMethod=" + preferredCommMethod + ", otp=" + otp + ", responseStatus=" + responseStatus + "]";
	}
}
