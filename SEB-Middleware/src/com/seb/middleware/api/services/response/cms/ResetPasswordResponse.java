package com.seb.middleware.api.services.response.cms;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class ResetPasswordResponse extends Response{
	
	@JsonField(name = "NEW_PASSWORD")
	private String newPassword;

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	@Override
	public String toString() {
		return "ResetPasswordResponse [newPassword=" + newPassword + ", responseStatus=" + responseStatus + "]";
	}	
}
