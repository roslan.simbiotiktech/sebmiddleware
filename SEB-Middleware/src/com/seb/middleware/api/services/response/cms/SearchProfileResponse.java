package com.seb.middleware.api.services.response.cms;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Pagination;
import com.seb.middleware.api.services.response.Response;

public class SearchProfileResponse extends Response{

	@JsonField(name = "PAGINATION")
	private Pagination pagination;
	
	@JsonField(name = "USERS")
	private User [] users;

	public User[] getUsers() {
		return users;
	}

	public void setUsers(User[] users) {
		this.users = users;
	}

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

	@Override
	public String toString() {
		return "SearchProfileResponse [pagination=" + pagination + ", users=" + Arrays.toString(users) + ", responseStatus=" + responseStatus + "]";
	}
}
