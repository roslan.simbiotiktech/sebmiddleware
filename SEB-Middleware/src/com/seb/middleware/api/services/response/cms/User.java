package com.seb.middleware.api.services.response.cms;

import java.util.Arrays;
import java.util.Date;

import com.anteater.library.json.JsonField;

public class User {

	@JsonField(name = "LOGIN_ID")
	private String loginId;
	
	@JsonField(name = "EMAIL")
	private String email;
	
	@JsonField(name = "NAME")
	private String name;
	
	@JsonField(name = "MOBILE_NUMBER")
	private String mobileNumber;
	
	@JsonField(name = "STATUS")
	private String status;
	
	@JsonField(name = "LAST_LOGIN_AT")
	private Date lastLoginAt;
	
	@JsonField(name = "CONTRACT_SUBSCRIBED")
	private Contract[] contractSubscribed;

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getLastLoginAt() {
		return lastLoginAt;
	}

	public void setLastLoginAt(Date lastLoginAt) {
		this.lastLoginAt = lastLoginAt;
	}

	public Contract[] getContractSubscribed() {
		return contractSubscribed;
	}

	public void setContractSubscribed(Contract[] contractSubscribed) {
		this.contractSubscribed = contractSubscribed;
	}

	@Override
	public String toString() {
		return "User [loginId=" + loginId + ", email=" + email + ", name=" + name + ", mobileNumber=" + mobileNumber + ", status=" + status
				+ ", lastLoginAt=" + lastLoginAt + ", contractSubscribed=" + Arrays.toString(contractSubscribed) + "]";
	}
}
