package com.seb.middleware.api.services.response.cms;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Pagination;
import com.seb.middleware.api.services.response.Response;

public class ViewCustomerHistoryResponse extends Response{

	@JsonField(name = "PAGINATION")
	private Pagination pagination;
	
	@JsonField(name = "AUDIT_TRAILS")
	private AuditTrail [] auditTrails;

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

	public AuditTrail[] getAuditTrails() {
		return auditTrails;
	}

	public void setAuditTrails(AuditTrail[] auditTrails) {
		this.auditTrails = auditTrails;
	}

	@Override
	public String toString() {
		return "ViewCustomerHistoryResponse [pagination=" + pagination + ", auditTrails=" + Arrays.toString(auditTrails) + ", responseStatus="
				+ responseStatus + "]";
	}
}
