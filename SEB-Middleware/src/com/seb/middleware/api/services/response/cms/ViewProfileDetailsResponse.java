package com.seb.middleware.api.services.response.cms;

import java.util.Date;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class ViewProfileDetailsResponse extends Response{

	@JsonField(name = "LOGIN_ID")
	private String loginId;
	
	@JsonField(name = "EMAIL")
	private String email;
	
	@JsonField(name = "NAME")
	private String name;
	
	@JsonField(name = "NRIC_OR_PASSPORT")
	private String nricOrPassport;
	
	@JsonField(name = "MOBILE_NUMBER")
	private String mobileNumber;
	
	@JsonField(name = "PREFERRED_COMM_METHOD")
	private String preferredCommMethod;
	
	@JsonField(name = "HOME_TEL")
	private String homeTel;
	
	@JsonField(name = "OFFICE_TEL")
	private String officeTel;
	
	@JsonField(name = "ACCOUNT_STATUS")
	private String accountStatus;
	
	@JsonField(name = "LAST_LOGIN_AT")
	private Date lastLogin;
	
	@JsonField(name = "CONTRACT_DETAIL")
	private ContractDetail contractDetail;
	
	@JsonField(name = "REMARK")
	private String remark;

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNricOrPassport() {
		return nricOrPassport;
	}

	public void setNricOrPassport(String nricOrPassport) {
		this.nricOrPassport = nricOrPassport;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPreferredCommMethod() {
		return preferredCommMethod;
	}

	public void setPreferredCommMethod(String preferredCommMethod) {
		this.preferredCommMethod = preferredCommMethod;
	}

	public String getHomeTel() {
		return homeTel;
	}

	public void setHomeTel(String homeTel) {
		this.homeTel = homeTel;
	}

	public String getOfficeTel() {
		return officeTel;
	}

	public void setOfficeTel(String officeTel) {
		this.officeTel = officeTel;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public ContractDetail getContractDetail() {
		return contractDetail;
	}

	public void setContractDetail(ContractDetail contractDetail) {
		this.contractDetail = contractDetail;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "ViewProfileDetailsResponse [loginId=" + loginId + ", email=" + email + ", name=" + name + ", nricOrPassport=" + nricOrPassport
				+ ", mobileNumber=" + mobileNumber + ", preferredCommMethod=" + preferredCommMethod + ", homeTel=" + homeTel + ", officeTel="
				+ officeTel + ", accountStatus=" + accountStatus + ", lastLogin=" + lastLogin + ", contractDetail=" + contractDetail + ", remark="
				+ remark + ", responseStatus=" + responseStatus + "]";
	}
}
