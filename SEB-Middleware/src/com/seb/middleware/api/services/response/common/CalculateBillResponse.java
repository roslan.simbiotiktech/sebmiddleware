package com.seb.middleware.api.services.response.common;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class CalculateBillResponse extends Response{

	@JsonField(name = "KWH_CONSUMPTION")
	private double kwhConsumption;
	
	@JsonField(name = "KVARH_CONSUMPTION")
	private double kvarhConsumption;
	
	@JsonField(name = "KWH_TOTAL_CONSUMPTION")
	private double kwhTotalComsumption;
	
	@JsonField(name = "KVARH_TOTAL_CONSUMPTION")
	private double kvarhTotalComsumption;
	
	@JsonField(name = "TARIFF_RATE")
	private double tariffRate;
	
	@JsonField(name = "OFFPEAK_TARIFF_RATE")
	private double offpeakTariffRate;
	
	@JsonField(name = "KW_MAX_DEMAND_UNIT")
	private double kwMaxDemandUnit;
	
	@JsonField(name = "KW_MAX_DEMAND_AMOUNT")
	private double kwMaxDemandAmount;
	
	@JsonField(name = "LOW_POWER_FACTOR_RATE")
	private double lowPowerFactorRate;
	
	@JsonField(name = "LOW_POWER_FACTOR_CHARGE")
	private double lowPowerFactorCharge;
	
	@JsonField(name = "USAGE_CHARGE")
	private KwhUsage usageCharge;
	
	@JsonField(name = "TAX_EXEMPTED_USAGE_CHARGE")
	private KwhUsage taxExemptedUsageCharge;
	
	@JsonField(name = "OFFPEAK_USAGE_CHARGE")
	private KwhUsage offpeakUsageCharge;
	
	@JsonField(name = "OFFPEAK_KWH_CONSUMPTION")
	private double offpeakKwhConsumption;
	
	@JsonField(name = "OFFPEAK_KVARH_CONSUMPTION")
	private double offpeakKvarhConsumption;
	
	@JsonField(name = "GST_RATE")
	private double gstRate;
	
	@JsonField(name = "GST_AMOUNT")
	private double gstAmount;
	
	@JsonField(name = "SUB_TOTAL")
	private double subTotal;
	
	@JsonField(name = "BILL_AMOUNT")
	private double billAmount;
	
	@JsonField(name = "MIN_MONTHLY_CHARGE")
	private double minMonthlytCharge;

	public double getKwhConsumption() {
		return kwhConsumption;
	}

	public void setKwhConsumption(double kwhConsumption) {
		this.kwhConsumption = kwhConsumption;
	}

	public double getKvarhConsumption() {
		return kvarhConsumption;
	}

	public void setKvarhConsumption(double kvarhConsumption) {
		this.kvarhConsumption = kvarhConsumption;
	}

	public double getKwhTotalComsumption() {
		return kwhTotalComsumption;
	}

	public void setKwhTotalComsumption(double kwhTotalComsumption) {
		this.kwhTotalComsumption = kwhTotalComsumption;
	}

	public double getKvarhTotalComsumption() {
		return kvarhTotalComsumption;
	}

	public void setKvarhTotalComsumption(double kvarhTotalComsumption) {
		this.kvarhTotalComsumption = kvarhTotalComsumption;
	}

	public double getTariffRate() {
		return tariffRate;
	}

	public void setTariffRate(double tariffRate) {
		this.tariffRate = tariffRate;
	}

	public double getOffpeakTariffRate() {
		return offpeakTariffRate;
	}

	public void setOffpeakTariffRate(double offpeakTariffRate) {
		this.offpeakTariffRate = offpeakTariffRate;
	}

	public double getKwMaxDemandUnit() {
		return kwMaxDemandUnit;
	}

	public void setKwMaxDemandUnit(double kwMaxDemandUnit) {
		this.kwMaxDemandUnit = kwMaxDemandUnit;
	}

	public double getKwMaxDemandAmount() {
		return kwMaxDemandAmount;
	}

	public void setKwMaxDemandAmount(double kwMaxDemandAmount) {
		this.kwMaxDemandAmount = kwMaxDemandAmount;
	}

	public double getLowPowerFactorRate() {
		return lowPowerFactorRate;
	}

	public void setLowPowerFactorRate(double lowPowerFactorRate) {
		this.lowPowerFactorRate = lowPowerFactorRate;
	}

	public double getLowPowerFactorCharge() {
		return lowPowerFactorCharge;
	}

	public void setLowPowerFactorCharge(double lowPowerFactorCharge) {
		this.lowPowerFactorCharge = lowPowerFactorCharge;
	}

	public KwhUsage getUsageCharge() {
		return usageCharge;
	}

	public void setUsageCharge(KwhUsage usageCharge) {
		this.usageCharge = usageCharge;
	}


	public KwhUsage getTaxExemptedUsageCharge() {
		return taxExemptedUsageCharge;
	}

	public void setTaxExemptedUsageCharge(KwhUsage taxExemptedUsageCharge) {
		this.taxExemptedUsageCharge = taxExemptedUsageCharge;
	}

	public KwhUsage getOffpeakUsageCharge() {
		return offpeakUsageCharge;
	}

	public void setOffpeakUsageCharge(KwhUsage offpeakUsageCharge) {
		this.offpeakUsageCharge = offpeakUsageCharge;
	}

	public double getOffpeakKwhConsumption() {
		return offpeakKwhConsumption;
	}

	public void setOffpeakKwhConsumption(double offpeakKwhConsumption) {
		this.offpeakKwhConsumption = offpeakKwhConsumption;
	}

	public double getOffpeakKvarhConsumption() {
		return offpeakKvarhConsumption;
	}

	public void setOffpeakKvarhConsumption(double offpeakKvarhConsumption) {
		this.offpeakKvarhConsumption = offpeakKvarhConsumption;
	}

	public double getGstRate() {
		return gstRate;
	}

	public void setGstRate(double gstRate) {
		this.gstRate = gstRate;
	}

	public double getGstAmount() {
		return gstAmount;
	}

	public void setGstAmount(double gstAmount) {
		this.gstAmount = gstAmount;
	}

	public double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public double getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(double billAmount) {
		this.billAmount = billAmount;
	}

	public double getMinMonthlytCharge() {
		return minMonthlytCharge;
	}

	public void setMinMonthlytCharge(double minMonthlytCharge) {
		this.minMonthlytCharge = minMonthlytCharge;
	}

	@Override
	public String toString() {
		return "CalculateBillResponse [kwhConsumption=" + kwhConsumption + ", kvarhConsumption=" + kvarhConsumption + ", kwhTotalComsumption="
				+ kwhTotalComsumption + ", kvarhTotalComsumption=" + kvarhTotalComsumption + ", tariffRate=" + tariffRate + ", offpeakTariffRate="
				+ offpeakTariffRate + ", kwMaxDemandUnit=" + kwMaxDemandUnit + ", kwMaxDemandAmount=" + kwMaxDemandAmount + ", lowPowerFactorRate="
				+ lowPowerFactorRate + ", lowPowerFactorCharge=" + lowPowerFactorCharge + ", usageCharge=" + usageCharge + ", taxExemptedUsageCharge="
				+ taxExemptedUsageCharge + ", offpeakUsageCharge=" + offpeakUsageCharge + ", offpeakKwhConsumption=" + offpeakKwhConsumption
				+ ", offpeakKvarhConsumption=" + offpeakKvarhConsumption + ", gstRate=" + gstRate + ", gstAmount=" + gstAmount + ", subTotal="
				+ subTotal + ", billAmount=" + billAmount + ", minMonthlytCharge=" + minMonthlytCharge + ", responseStatus=" + responseStatus + "]";
	}
}
