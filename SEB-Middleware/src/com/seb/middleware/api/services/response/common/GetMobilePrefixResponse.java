package com.seb.middleware.api.services.response.common;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GetMobilePrefixResponse extends Response{

	@JsonField(name = "MOBILE_PREFIXES")
	private MobilePrefix [] mobilePrefixes;

	public MobilePrefix[] getMobilePrefixes() {
		return mobilePrefixes;
	}

	public void setMobilePrefixes(MobilePrefix[] mobilePrefixes) {
		this.mobilePrefixes = mobilePrefixes;
	}

	@Override
	public String toString() {
		return "GetMobilePrefixResponse [mobilePrefixes=" + Arrays.toString(mobilePrefixes) + ", responseStatus=" + responseStatus + "]";
	}	
}
