package com.seb.middleware.api.services.response.common;

import com.anteater.library.json.JsonField;

public class KwhUsage {

	@JsonField(name = "UNIT")
	private double unit;
	
	@JsonField(name = "AMOUNT")
	private double amount;

	public double getUnit() {
		return unit;
	}

	public void setUnit(double unit) {
		this.unit = unit;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "KwhUsage [unit=" + unit + ", amount=" + amount + "]";
	}	
}
