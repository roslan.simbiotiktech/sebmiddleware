package com.seb.middleware.api.services.response.common;

import com.anteater.library.json.JsonField;

public class MobilePrefix {

	@JsonField(name = "COUNTRY_NAME")
	private String countryName;
	
	@JsonField(name = "PREFIX")
	private String prefix;

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	@Override
	public String toString() {
		return "MobilePrefix [countryName=" + countryName + ", prefix=" + prefix + "]";
	}
}
