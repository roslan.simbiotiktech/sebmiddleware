package com.seb.middleware.api.services.response.content;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Location;

public class Country {
	
	@JsonField(name = "NAME")
	private String name;
	
	@JsonField(name = "ADDRESS")
	private String address;
	
	@JsonField(name = "OPENING_HOURS")
	private String openingHours;
	
	@JsonField(name = "LOCATION")
	private Location location;
	
	public Country() {
		super();
	}

	public Country(String name, String address, String openingHours,
			Location location) {
		super();
		this.name = name;
		this.address = address;
		this.openingHours = openingHours;
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOpeningHours() {
		return openingHours;
	}

	public void setOpeningHours(String openingHours) {
		this.openingHours = openingHours;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "Country [name=" + name + ", address=" + address
				+ ", openingHours=" + openingHours + ", location=" + location
				+ "]";
	}
	
}
