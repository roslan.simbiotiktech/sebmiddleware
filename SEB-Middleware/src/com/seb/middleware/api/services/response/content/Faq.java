package com.seb.middleware.api.services.response.content;

import com.anteater.library.json.JsonField;

public class Faq {
	
	@JsonField(name = "QUESTION")
	private String question;
	
	@JsonField(name = "ANSWER")
	private String answer;

	public Faq(String question, String answer) {
		super();
		this.question = question;
		this.answer = answer;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@Override
	public String toString() {
		return "Faq [question=" + question + ", answer=" + answer + "]";
	}
	
}
