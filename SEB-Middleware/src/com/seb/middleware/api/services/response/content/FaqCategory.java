package com.seb.middleware.api.services.response.content;

import com.anteater.library.json.JsonField;

public class FaqCategory {
	
	@JsonField(name = "KEY")
	private String key;
	
	@JsonField(name = "DESCRIPTION")
	private String name;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "FaqCategory [key=" + key + ", name=" + name + "]";
	}

}
