package com.seb.middleware.api.services.response.content;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GetAvailableStationResponse extends Response{
	
	@JsonField(name = "STATIONS")
	private Station[] stations;

	public Station[] getStations() {
		return stations;
	}

	public void setStations(Station[] stations) {
		this.stations = stations;
	}

	@Override
	public String toString() {
		return "GetAvailableStationResponse [stations="
				+ Arrays.toString(stations) + "]";
	}

}
