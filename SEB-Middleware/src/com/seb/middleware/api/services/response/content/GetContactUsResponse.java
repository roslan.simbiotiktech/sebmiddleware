package com.seb.middleware.api.services.response.content;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GetContactUsResponse extends Response{
	
	@JsonField(name = "TEL_NUMBER")
	private String telNumber;
	
	@JsonField(name = "FAX_NUMBER")
	private String faxNumber;
	
	@JsonField(name = "EMAIL")
	private String email;
	
	@JsonField(name = "ADDRESS")
	private String address;
	
	@JsonField(name = "WEBSITE_URL")
	private String websiteUrl;
	
	@JsonField(name = "TWITTER")
	private String twitter;

	public String getTelNumber() {
		return telNumber;
	}

	public void setTelNumber(String telNumber) {
		this.telNumber = telNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	@Override
	public String toString() {
		return "GetContactUsResponse [telNumber=" + telNumber + ", faxNumber="
				+ faxNumber + ", email=" + email + ", address=" + address
				+ ", websiteUrl=" + websiteUrl + ", twitter=" + twitter + "]";
	}

}
