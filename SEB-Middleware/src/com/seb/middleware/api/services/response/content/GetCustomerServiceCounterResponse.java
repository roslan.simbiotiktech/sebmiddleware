package com.seb.middleware.api.services.response.content;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GetCustomerServiceCounterResponse extends Response{
	
	@JsonField(name = "REGIONS")
	private Region[] regions;

	public Region[] getRegions() {
		return regions;
	}

	public void setRegions(Region[] regions) {
		this.regions = regions;
	}

	@Override
	public String toString() {
		return "GetCustomerServiceCounterResponse [regions="
				+ Arrays.toString(regions) + "]";
	}

}
