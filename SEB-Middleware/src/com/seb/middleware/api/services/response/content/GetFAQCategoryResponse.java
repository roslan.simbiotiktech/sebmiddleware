package com.seb.middleware.api.services.response.content;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GetFAQCategoryResponse extends Response{
	
	@JsonField(name = "FAQ_CATEGORIES")
	private FaqCategory[] faqCategories;

	public FaqCategory[] getFaqCategories() {
		return faqCategories;
	}

	public void setFaqCategories(FaqCategory[] faqCategories) {
		this.faqCategories = faqCategories;
	}

	@Override
	public String toString() {
		return "GetFAQCategoryResponse [faqCategories="
				+ Arrays.toString(faqCategories) + ", responseStatus="
				+ responseStatus + "]";
	}

}
