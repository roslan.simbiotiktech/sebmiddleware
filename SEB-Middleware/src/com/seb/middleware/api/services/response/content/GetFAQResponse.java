package com.seb.middleware.api.services.response.content;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GetFAQResponse extends Response{
	
	@JsonField(name = "FAQs")
	private Faq[] faqs;

	public Faq[] getFaqs() {
		return faqs;
	}

	public void setFaqs(Faq[] faqs) {
		this.faqs = faqs;
	}

	@Override
	public String toString() {
		return "GetFAQResponse [faqs=" + Arrays.toString(faqs)
				+ ", responseStatus=" + responseStatus + "]";
	}

}
