package com.seb.middleware.api.services.response.content;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Pagination;
import com.seb.middleware.api.services.response.Response;

public class GetPowerAlertResponse extends Response{
	
	@JsonField(name = "PAGINATION")
	private Pagination pagination;
	
	@JsonField(name = "POWER_ALERTS")
	private PowerAlert[] powerAlerts;

	public PowerAlert[] getPowerAlerts() {
		return powerAlerts;
	}

	public void setPowerAlerts(PowerAlert[] powerAlerts) {
		this.powerAlerts = powerAlerts;
	}

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

	@Override
	public String toString() {
		return "GetPowerAlertResponse [pagination=" + pagination + ", powerAlerts=" + Arrays.toString(powerAlerts) + ", responseStatus="
				+ responseStatus + "]";
	}
}
