package com.seb.middleware.api.services.response.content;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GetPrivacyPolicyResponse extends Response{
	
	@JsonField(name = "PRIVACY_POLICY")
	private String privacPolicy;

	public String getPrivacPolicy() {
		return privacPolicy;
	}

	public void setPrivacPolicy(String privacPolicy) {
		this.privacPolicy = privacPolicy;
	}

	@Override
	public String toString() {
		return "GetPrivacyPolicyResponse [responseStatus=" + responseStatus + "]";
	}

}
