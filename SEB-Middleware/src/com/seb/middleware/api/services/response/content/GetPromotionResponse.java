package com.seb.middleware.api.services.response.content;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Pagination;
import com.seb.middleware.api.services.response.Response;

public class GetPromotionResponse extends Response{
	
	@JsonField(name = "PROMOTIONS")
	private Promotion[] promotions;
	
	@JsonField(name = "PAGINATION")
	private Pagination pagination;

	public Promotion[] getPromotions() {
		return promotions;
	}

	public void setPromotions(Promotion[] promotions) {
		this.promotions = promotions;
	}

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

	@Override
	public String toString() {
		return "GetPromotionResponse [promotions=" + Arrays.toString(promotions) + ", pagination=" + pagination + ", responseStatus=" + responseStatus
				+ "]";
	}
}
