package com.seb.middleware.api.services.response.content;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GetTermsAndConditionsResponse extends Response{
	
	@JsonField(name = "TERMS_AND_CONDITIONS")
	private String termsAndConditions;

	public String getTermsAndConditions() {
		return termsAndConditions;
	}

	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	@Override
	public String toString() {
		return "GetTermsAndConditionsResponse [responseStatus=" + responseStatus + "]";
	}
}
