package com.seb.middleware.api.services.response.content;

import java.util.Date;

import com.anteater.library.json.JsonField;

public class PowerAlert {
	
	@JsonField(name = "ALERT_ID")
	private long alertId;
	
	@JsonField(name = "ALERT_TYPE")
	private String alertType;
	
	@JsonField(name = "STATION")
	private String station;
	
	@JsonField(name = "AREA")
	private String area;
	
	@JsonField(name = "CAUSES")
	private String causes;
	
	@JsonField(name = "TITLE")
	private String title;
	
	@JsonField(name = "ESTIMATED_RESTORE_DATETIME")
	private Date estimatedRestoreDatetime;
	
	@JsonField(name = "MAINTENANCE_START_DATETIME")
	private Date maintenanceStartDatetime;
	
	@JsonField(name = "MAINTENANCE_END_DATETIME")
	private Date maintenanceEndDatetime;
	
	@JsonField(name = "CREATED_DATETIME")
	private Date createdDatetime;

	public PowerAlert() {
		super();
	}

	public PowerAlert(long alertId, String alertType, String station,
			String area, String causes, Date maintenanceStartDatetime,
			Date maintenanceEndDatetime, Date createdDatetime) {
		super();
		this.alertId = alertId;
		this.alertType = alertType;
		this.station = station;
		this.area = area;
		this.causes = causes;
		this.maintenanceStartDatetime = maintenanceStartDatetime;
		this.maintenanceEndDatetime = maintenanceEndDatetime;
		this.createdDatetime = createdDatetime;
	}

	public PowerAlert(long alertId, String alertType, String station,
			String area, String causes, Date estimatedRestoreDatetime, Date createdDatetime) {
		super();
		this.alertId = alertId;
		this.alertType = alertType;
		this.station = station;
		this.area = area;
		this.causes = causes;
		this.estimatedRestoreDatetime = estimatedRestoreDatetime;
		this.createdDatetime = createdDatetime;
	}

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

	public String getAlertType() {
		return alertType;
	}

	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCauses() {
		return causes;
	}

	public void setCauses(String causes) {
		this.causes = causes;
	}

	public Date getEstimatedRestoreDatetime() {
		return estimatedRestoreDatetime;
	}

	public void setEstimatedRestoreDatetime(Date estimatedRestoreDatetime) {
		this.estimatedRestoreDatetime = estimatedRestoreDatetime;
	}

	public Date getMaintenanceStartDatetime() {
		return maintenanceStartDatetime;
	}

	public void setMaintenanceStartDatetime(Date maintenanceStartDatetime) {
		this.maintenanceStartDatetime = maintenanceStartDatetime;
	}

	public Date getMaintenanceEndDatetime() {
		return maintenanceEndDatetime;
	}

	public void setMaintenanceEndDatetime(Date maintenanceEndDatetime) {
		this.maintenanceEndDatetime = maintenanceEndDatetime;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "PowerAlert [alertId=" + alertId + ", alertType=" + alertType
				+ ", station=" + station + ", area=" + area + ", causes="
				+ causes + ", title=" + title + ", estimatedRestoreDatetime="
				+ estimatedRestoreDatetime + ", maintenanceStartDatetime="
				+ maintenanceStartDatetime + ", maintenanceEndDatetime="
				+ maintenanceEndDatetime + ", createdDatetime="
				+ createdDatetime + "]";
	}
}
