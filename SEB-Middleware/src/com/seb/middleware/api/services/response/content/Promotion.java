package com.seb.middleware.api.services.response.content;

import java.util.Date;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Photo;

public class Promotion {
	
	@JsonField(name = "PROMO_ID")
	private long promoId;
	
	@JsonField(name = "TITLE")
	private String title;
	
	@JsonField(name = "DESCRIPTION")
	private String description;
	
	@JsonField(name = "START_DATE")
	private Date startDate;
	
	@JsonField(name = "END_DATE")
	private Date endDate;
	
	@JsonField(name = "PHOTO")
	private Photo photo;

	public Promotion() {
	}

	public Promotion(long promoId, String title, String description,
			Date startDate, Date endDate, Photo photo) {
		this.promoId = promoId;
		this.title = title;
		this.description = description;
		this.startDate = startDate;
		this.endDate = endDate;
		this.photo = photo;
	}

	public long getPromoId() {
		return promoId;
	}

	public void setPromoId(long promoId) {
		this.promoId = promoId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Photo getPhoto() {
		return photo;
	}

	public void setPhoto(Photo photo) {
		this.photo = photo;
	}

	@Override
	public String toString() {
		return "Promotion [promoId=" + promoId + ", title=" + title
				+ ", startDate=" + startDate
				+ ", endDate=" + endDate + ", photo=" + photo + "]";
	}

}
