package com.seb.middleware.api.services.response.content;

import java.util.Arrays;

import com.anteater.library.json.JsonField;

public class Region {
	
	@JsonField(name = "NAME")
	private String name;
	
	@JsonField(name = "CS_COUNTERS")
	private Country[] countries;

	public Region(String name, Country[] countries) {
		super();
		this.name = name;
		this.countries = countries;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Country[] getCountries() {
		return countries;
	}

	public void setCountries(Country[] countries) {
		this.countries = countries;
	}

	@Override
	public String toString() {
		return "Region [name=" + name + ", countries="
				+ Arrays.toString(countries) + "]";
	}

}
