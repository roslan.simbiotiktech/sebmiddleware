package com.seb.middleware.api.services.response.notification;

import java.util.Date;

import com.anteater.library.json.JsonField;

public class BillReadyNotification extends Notification{
	
	@JsonField(name = "EMAIL")
	protected String email;

	@JsonField(name = "CONTRACT_ACC_NO")
	protected String contractAccNo;
	
	@JsonField(name = "INVOICE_NO")
	protected String invoiceNo;
	
	@JsonField(name = "INVOICE_DATE", outputDateFormat = "yyyyMM")
	protected Date invoiceDate;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContractAccNo() {
		return contractAccNo;
	}

	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	@Override
	public String toString() {
		return "BillReadyNotification [email=" + email + ", contractAccNo=" + contractAccNo + ", invoiceNo=" + invoiceNo
				+ ", invoiceDate=" + invoiceDate + ", notificationId=" + notificationId + ", text=" + text + "]";
	}
}
