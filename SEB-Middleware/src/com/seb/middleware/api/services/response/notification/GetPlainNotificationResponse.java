package com.seb.middleware.api.services.response.notification;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GetPlainNotificationResponse extends Response{

	@JsonField(name = "NOTIFICATION")
	private TagNotification [] notifications; 
	
	@JsonField(name = "NOTIFICATION_TYPE")
	private String notificationType;

	public TagNotification[] getNotifications() {
		return notifications;
	}

	public void setNotifications(TagNotification[] notifications) {
		this.notifications = notifications;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	@Override
	public String toString() {
		return "GetPlainNotificationResponse [notifications=" + Arrays.toString(notifications) + ", notificationType=" + notificationType
				+ ", responseStatus=" + responseStatus + "]";
	}
}
