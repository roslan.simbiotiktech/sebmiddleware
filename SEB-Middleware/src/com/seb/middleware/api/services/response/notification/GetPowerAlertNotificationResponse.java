package com.seb.middleware.api.services.response.notification;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GetPowerAlertNotificationResponse extends Response{

	@JsonField(name = "NOTIFICATION")
	private PowerAlertNotification [] notifications; 
	
	@JsonField(name = "NOTIFICATION_TYPE")
	private String notificationType;

	public PowerAlertNotification[] getNotifications() {
		return notifications;
	}

	public void setNotifications(PowerAlertNotification[] notifications) {
		this.notifications = notifications;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	@Override
	public String toString() {
		return "GetPowerAlertNotificationResponse [notifications=" + Arrays.toString(notifications) + ", notificationType=" + notificationType
				+ ", responseStatus=" + responseStatus + "]";
	}
}
