package com.seb.middleware.api.services.response.notification;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GetSubscribedLocationResponse extends Response{
	
	@JsonField(name = "TAGS")
	private String[] tags;

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return "GetSubscribedLocationResponse [tags=" + Arrays.toString(tags)
				+ ", responseStatus=" + responseStatus + "]";
	}
	
}
