package com.seb.middleware.api.services.response.notification;

import com.anteater.library.json.JsonField;

public class Notification {

	@JsonField(name = "ID")
	protected long notificationId;

	@JsonField(name = "TEXT")
	protected String text;

	public long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(long notificationId) {
		this.notificationId = notificationId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "Notification [notificationId=" + notificationId + ", text=" + text + "]";
	}
}
