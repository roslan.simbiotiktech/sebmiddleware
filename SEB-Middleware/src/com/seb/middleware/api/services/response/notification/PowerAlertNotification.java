package com.seb.middleware.api.services.response.notification;

import com.anteater.library.json.JsonField;

public class PowerAlertNotification extends TagNotification{
	
	@JsonField(name = "ALERT_ID")
	protected long alertId;

	public long getAlertId() {
		return alertId;
	}

	public void setAlertId(long alertId) {
		this.alertId = alertId;
	}

	@Override
	public String toString() {
		return "PowerAlertNotification [alertId=" + alertId + ", tag=" + tag + ", notificationId=" + notificationId + ", text=" + text + "]";
	}
}
