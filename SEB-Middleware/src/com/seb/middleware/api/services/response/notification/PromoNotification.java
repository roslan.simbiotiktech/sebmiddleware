package com.seb.middleware.api.services.response.notification;

import com.anteater.library.json.JsonField;

public class PromoNotification extends TagNotification{
	
	@JsonField(name = "PROMO_ID")
	protected long promoId;

	public long getPromoId() {
		return promoId;
	}

	public void setPromoId(long promoId) {
		this.promoId = promoId;
	}

	@Override
	public String toString() {
		return "PromoNotification [promoId=" + promoId + ", tag=" + tag + ", notificationId=" + notificationId + ", text=" + text + "]";
	}
}
