package com.seb.middleware.api.services.response.notification;

import com.anteater.library.json.JsonField;

public class ReportNotification extends Notification{
	
	@JsonField(name = "EMAIL")
	private String email;

	@JsonField(name = "TRANS_ID")
	private long transId;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getTransId() {
		return transId;
	}

	public void setTransId(long transId) {
		this.transId = transId;
	}

	@Override
	public String toString() {
		return "ReportNotification [email=" + email + ", transId=" + transId + ", notificationId=" + notificationId + ", text=" + text + "]";
	}
}
