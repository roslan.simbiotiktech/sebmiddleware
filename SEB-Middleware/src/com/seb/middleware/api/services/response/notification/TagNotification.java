package com.seb.middleware.api.services.response.notification;

import com.anteater.library.json.JsonField;

public class TagNotification extends Notification{
	
	@JsonField(name = "TAG")
	protected String tag;

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	@Override
	public String toString() {
		return "TagNotification [tag=" + tag + ", notificationId=" + notificationId + ", text=" + text + "]";
	}
}
