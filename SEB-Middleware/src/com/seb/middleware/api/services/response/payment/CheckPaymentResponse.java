package com.seb.middleware.api.services.response.payment;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;
import com.seb.middleware.constant.PaymentStatus;

public class CheckPaymentResponse extends Response{
	
	@JsonField(name = "PAYMENT_STATUS")
	protected PaymentStatus status;
	
	@JsonField(name = "PAYMENT_DETAILS")
	protected PaymentDetail paymentDetail;
	
	@JsonField(name = "PAYMENT_ENTRIES")
	protected PaymentEntry [] entries;
	
	public PaymentStatus getStatus() {
		return status;
	}

	public void setStatus(PaymentStatus status) {
		this.status = status;
	}

	public PaymentDetail getPaymentDetail() {
		return paymentDetail;
	}

	public void setPaymentDetail(PaymentDetail paymentDetail) {
		this.paymentDetail = paymentDetail;
	}

	public PaymentEntry[] getEntries() {
		return entries;
	}

	public void setEntries(PaymentEntry[] entries) {
		this.entries = entries;
	}

	@Override
	public String toString() {
		return "CheckPaymentResponse [status=" + status + ", paymentDetail=" + paymentDetail + ", entries=" + Arrays.toString(entries)
				+ ", responseStatus=" + responseStatus + "]";
	}
}
