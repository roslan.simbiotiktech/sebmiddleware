package com.seb.middleware.api.services.response.payment;

import java.util.Date;

import com.anteater.library.json.JsonField;

public class PaymentDetail {

	@JsonField(name = "PAYMENT_DATE", outputDateFormat = "dd/MMM/yyyy HH:mm:ss")
	private Date paymentDate;
	
	@JsonField(name = "AMOUNT")
	private double amount;
	
	@JsonField(name = "PAYMENT_METHOD")
	private String paymentMethod;
	
	@JsonField(name = "GATEWAY_REFERENCE")
	private String gatewayReference;

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getGatewayReference() {
		return gatewayReference;
	}

	public void setGatewayReference(String gatewayReference) {
		this.gatewayReference = gatewayReference;
	}

	@Override
	public String toString() {
		return "PaymentDetail [paymentDate=" + paymentDate + ", amount=" + amount + ", paymentMethod=" + paymentMethod + ", gatewayReference="
				+ gatewayReference + "]";
	}
}
