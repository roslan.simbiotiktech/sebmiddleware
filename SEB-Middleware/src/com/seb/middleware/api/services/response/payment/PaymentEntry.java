package com.seb.middleware.api.services.response.payment;

import com.anteater.library.json.JsonField;

public class PaymentEntry {

	@JsonField(name = "CONTRACT_ACC_NO")
	protected String contractAccNo;

	@JsonField(name = "AMOUNT")
	private double amount;

	public String getContractAccNo() {
		return contractAccNo;
	}

	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "PaymentEntry [contractAccNo=" + contractAccNo + ", amount=" + amount + "]";
	}
}
