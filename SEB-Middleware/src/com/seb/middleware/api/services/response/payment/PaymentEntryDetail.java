package com.seb.middleware.api.services.response.payment;

import com.anteater.library.json.JsonField;

public class PaymentEntryDetail extends PaymentEntry {

	@JsonField(name = "CONTRACT_ACC_NICK")
	private String contractAccNick;

	public String getContractAccNick() {
		return contractAccNick;
	}

	public void setContractAccNick(String contractAccNick) {
		this.contractAccNick = contractAccNick;
	}

	@Override
	public String toString() {
		return "PaymentEntryDetail [contractAccNick=" + contractAccNick + ", contractAccNo=" + contractAccNo + "]";
	}
}
