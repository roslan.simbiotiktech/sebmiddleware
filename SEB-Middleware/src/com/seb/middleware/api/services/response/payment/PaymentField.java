package com.seb.middleware.api.services.response.payment;

import com.anteater.library.json.JsonField;

public class PaymentField {
	
	@JsonField(name = "NAME")
	private String name;
	
	@JsonField(name = "VALUE")
	private String value;
	
	private boolean store = true;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isStore() {
		return store;
	}

	public void setStore(boolean store) {
		this.store = store;
	}

	@Override
	public String toString() {
		return "PostParameter [name=" + name + ", value=" + value + "]";
	}
}
