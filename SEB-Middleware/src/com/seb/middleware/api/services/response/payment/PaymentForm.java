package com.seb.middleware.api.services.response.payment;

import java.util.Arrays;

import com.anteater.library.json.JsonField;

public class PaymentForm {

	@JsonField(name = "ACTION")
	private String action;
	
	@JsonField(name = "FIELDS")
	private PaymentField [] fields;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public PaymentField[] getFields() {
		return fields;
	}

	public void setFields(PaymentField[] fields) {
		this.fields = fields;
	}

	@Override
	public String toString() {
		return "PaymentForm [action=" + action + ", fields=" + Arrays.toString(fields) + "]";
	}
}
