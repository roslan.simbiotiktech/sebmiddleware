package com.seb.middleware.api.services.response.payment;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class PreparePaymentResponse extends Response{
	
	@JsonField(name = "FORM")
	private PaymentForm form;

	@JsonField(name = "ENTRIES")
	private PaymentEntryDetail [] entries;
	
	@JsonField(name = "PAYMENT_METHOD")
	private String paymentMethod;
	
	@JsonField(name = "NOTIFICATION_EMAIL")
	protected String notificationEmail;
	
	@JsonField(name = "REFERENCE_NO")
	private String referenceNo;

	public PaymentForm getForm() {
		return form;
	}

	public void setForm(PaymentForm form) {
		this.form = form;
	}

	public PaymentEntryDetail[] getEntries() {
		return entries;
	}

	public void setEntries(PaymentEntryDetail[] entries) {
		this.entries = entries;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getNotificationEmail() {
		return notificationEmail;
	}

	public void setNotificationEmail(String notificationEmail) {
		this.notificationEmail = notificationEmail;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	@Override
	public String toString() {
		return "PreparePaymentResponse [form=" + form + ", entries=" + Arrays.toString(entries) + ", paymentMethod=" + paymentMethod
				+ ", notificationEmail=" + notificationEmail + ", referenceNo=" + referenceNo + ", responseStatus=" + responseStatus + "]";
	}
}
