package com.seb.middleware.api.services.response.payment.fpx;

import com.anteater.library.json.JsonField;

public class FPXBank {
	
	@JsonField(name = "ID")
	private int id;
	
	@JsonField(name = "DESCRIPTION")
	private String description;
	
	@JsonField(name = "IMAGE_PATH")
	private String imagePath;
	
	@JsonField(name = "ENABLED")
	private boolean enabled;
	
	public FPXBank() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "FPXBank [id=" + id + ", description=" + description + ", imagePath=" + imagePath + ", enabled=" + enabled + "]";
	}
}
