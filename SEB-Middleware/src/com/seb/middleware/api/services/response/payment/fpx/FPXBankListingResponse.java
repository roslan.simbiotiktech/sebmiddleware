package com.seb.middleware.api.services.response.payment.fpx;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class FPXBankListingResponse extends Response{
	
	@JsonField(name = "PERSONAL_BANKS")
	private FPXBank[] personalBanks;
	
	@JsonField(name = "CORPORATE_BANKS")
	private FPXBank[] corporateBanks;

	public FPXBank[] getPersonalBanks() {
		return personalBanks;
	}

	public void setPersonalBanks(FPXBank[] personalBanks) {
		this.personalBanks = personalBanks;
	}

	public FPXBank[] getCorporateBanks() {
		return corporateBanks;
	}

	public void setCorporateBanks(FPXBank[] corporateBanks) {
		this.corporateBanks = corporateBanks;
	}

	@Override
	public String toString() {
		return "FPXBankListingResponse [personalBanks=" + Arrays.toString(personalBanks) + ", corporateBanks=" + Arrays.toString(corporateBanks)
				+ ", responseStatus=" + responseStatus + "]";
	}
}
