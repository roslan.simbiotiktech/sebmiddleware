package com.seb.middleware.api.services.response.report;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class HistoryDetailResponse extends Response{

	@JsonField(name="REPORT_DETAIL")
	private ReportDetail reportDetail;

	public ReportDetail getReportDetail() {
		return reportDetail;
	}

	public void setReportDetail(ReportDetail reportDetail) {
		this.reportDetail = reportDetail;
	}

	@Override
	public String toString() {
		return "HistoryDetailResponse [reportDetail=" + reportDetail
				+ ", responseStatus=" + responseStatus + "]";
	}
	
}
