package com.seb.middleware.api.services.response.report;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class HistoryListingResponse extends Response{

	@JsonField(name="REPORT")
	private Report [] reports;

	public Report[] getReports() {
		return reports;
	}

	public void setReports(Report[] reports) {
		this.reports = reports;
	}

	@Override
	public String toString() {
		return "HistoryListingResponse [reports=" + Arrays.toString(reports)
				+ ", responseStatus=" + responseStatus + "]";
	}
}
