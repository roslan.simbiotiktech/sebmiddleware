package com.seb.middleware.api.services.response.report;

import java.util.Date;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class MakeReportResponse extends Response{

	@JsonField(name="TRANS_ID")
	private String transId;
	
	@JsonField(name="CASE_ID")
	private String caseId;
	
	@JsonField(name="TRANS_DATETIME")
	private Date transDatetime;
	
	@JsonField(name="TRANS_STATUS")
	private String transStatus;

	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public Date getTransDatetime() {
		return transDatetime;
	}

	public void setTransDatetime(Date transDatetime) {
		this.transDatetime = transDatetime;
	}

	public String getTransStatus() {
		return transStatus;
	}

	public void setTransStatus(String transStatus) {
		this.transStatus = transStatus;
	}

	@Override
	public String toString() {
		return "MakeReportResponse [transId=" + transId + ", caseId=" + caseId + ", transDatetime=" + transDatetime
				+ ", transStatus=" + transStatus + "]";
	}
}
