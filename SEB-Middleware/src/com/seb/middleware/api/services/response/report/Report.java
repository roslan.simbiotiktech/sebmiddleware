package com.seb.middleware.api.services.response.report;

import java.util.Date;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Location;

public class Report {
	
	@JsonField(name="TRANS_ID")
	private String transId;

	@JsonField(name="CASE_ID")
	private String caseId;
	
	@JsonField(name="TRANS_DATETIME")
	private Date transDatetime;
	
	@JsonField(name="TRANS_STATUS")
	private String transStatus;
	
	@JsonField(name="TYPE")
	private String type;
	
	@JsonField(name="REMARK")
	private String remark;
	
	@JsonField(name="LOCATION")
	private Location location;
	
	@JsonField(name="RECORD_TYPE")
	private String recordType;
	
	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public Date getTransDatetime() {
		return transDatetime;
	}

	public void setTransDatetime(Date transDatetime) {
		this.transDatetime = transDatetime;
	}

	public String getTransStatus() {
		return transStatus;
	}

	public void setTransStatus(String transStatus) {
		this.transStatus = transStatus;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Report [transId=").append(transId).append(", caseId=").append(caseId).append(", transDatetime=")
				.append(transDatetime).append(", transStatus=").append(transStatus).append(", type=").append(type)
				.append(", remark=").append(remark).append(", location=").append(location).append(", recordType=")
				.append(recordType).append("]");
		return builder.toString();
	}
}
