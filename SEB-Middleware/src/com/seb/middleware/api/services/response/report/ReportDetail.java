package com.seb.middleware.api.services.response.report;

import java.util.Arrays;
import java.util.Date;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Location;
import com.seb.middleware.api.services.response.Photo;

public class ReportDetail {
	
	@JsonField(name="TRANS_ID")
	private String transId;

	@JsonField(name="CASE_ID")
	private String caseId;
	
	@JsonField(name="TRANS_DATETIME")
	private Date transDatetime;
	
	@JsonField(name="TRANS_STATUS")
	private String transStatus;
	
	@JsonField(name="TYPE")
	private String type;
	
	@JsonField(name="ISSUE_TYPE")
	private String issueType;
	
	@JsonField(name="CATEGORY")
	private String category;
	
	@JsonField(name="STATION")
	private String station;
	
	@JsonField(name="DESCRIPTION")
	private String description;
	
	@JsonField(name="REMARK")
	private String remark;
	
	@JsonField(name="LOCATION")
	private Location location;

	@JsonField(name="CONTRACT_ACC_NO")
	private String contractAccNo;
	
	@JsonField(name="PHOTOS")
	private Photo [] photos;

	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public Date getTransDatetime() {
		return transDatetime;
	}

	public void setTransDatetime(Date transDatetime) {
		this.transDatetime = transDatetime;
	}

	public String getTransStatus() {
		return transStatus;
	}

	public void setTransStatus(String transStatus) {
		this.transStatus = transStatus;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIssueType() {
		return issueType;
	}

	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getContractAccNo() {
		return contractAccNo;
	}

	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}

	public Photo[] getPhotos() {
		return photos;
	}

	public void setPhotos(Photo[] photos) {
		this.photos = photos;
	}

	@Override
	public String toString() {
		return "ReportDetail [transId=" + transId + ", caseId=" + caseId + ", transDatetime=" + transDatetime + ", transStatus=" + transStatus
				+ ", type=" + type + ", issueType=" + issueType + ", category=" + category + ", station=" + station + ", description=" + description
				+ ", remark=" + remark + ", location=" + location + ", contractAccNo=" + contractAccNo + ", photos=" + Arrays.toString(photos) + "]";
	}
}
