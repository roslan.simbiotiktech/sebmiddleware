package com.seb.middleware.api.services.response.sfdc;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class CaseUpdateResponse extends Response{

	@JsonField(name="trans_id", outputWhenNull = false)
	private Long transId;

	public Long getTransId() {
		return transId;
	}

	public void setTransId(Long transId) {
		this.transId = transId;
	}

	@Override
	public String toString() {
		return "CaseUpdateResponse [transId=" + transId + ", responseStatus=" + responseStatus + "]";
	}
}
