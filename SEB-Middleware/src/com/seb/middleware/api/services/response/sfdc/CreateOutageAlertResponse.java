package com.seb.middleware.api.services.response.sfdc;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class CreateOutageAlertResponse extends Response{

	@JsonField(name="announcement_id", outputWhenNull = false)
	private Long announcementId;

	public Long getAnnouncementId() {
		return announcementId;
	}

	public void setAnnouncementId(Long announcementId) {
		this.announcementId = announcementId;
	}

	@Override
	public String toString() {
		return "CreateOutageAlertResponse [announcementId=" + announcementId + ", responseStatus=" + responseStatus
				+ "]";
	}
}
