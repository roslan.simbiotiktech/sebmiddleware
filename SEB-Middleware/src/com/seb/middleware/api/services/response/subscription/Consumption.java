package com.seb.middleware.api.services.response.subscription;

import java.util.Date;

import com.anteater.library.json.JsonField;

public class Consumption {
	
	@JsonField(name = "MONTH", outputDateFormat = "yyyyMM")
	private Date month;
	
	@JsonField(name = "AMT")
	private double amt;

	public Date getMonth() {
		return month;
	}

	public void setMonth(Date month) {
		this.month = month;
	}

	public double getAmt() {
		return amt;
	}

	public void setAmt(double amt) {
		this.amt = amt;
	}

	@Override
	public String toString() {
		return "Consumption [month=" + month + ", amt=" + amt + "]";
	}
}
