package com.seb.middleware.api.services.response.subscription;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GetAvailableStatementResponse extends Response{
	
	@JsonField(name = "MONTH")
	private String[] months;

	public String[] getMonths() {
		return months;
	}

	public void setMonths(String[] months) {
		this.months = months;
	}

	@Override
	public String toString() {
		return "GetAvailableStatementResponse [months=" + Arrays.toString(months) + ", responseStatus=" + responseStatus + "]";
	}
}
