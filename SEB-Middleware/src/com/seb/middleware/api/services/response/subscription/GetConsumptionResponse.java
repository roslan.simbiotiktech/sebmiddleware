package com.seb.middleware.api.services.response.subscription;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GetConsumptionResponse extends Response{
	
	@JsonField(name = "CONSUMPTION")
	private Consumption[] consumptions;

	public Consumption[] getConsumptions() {
		return consumptions;
	}

	public void setConsumptions(Consumption[] consumptions) {
		this.consumptions = consumptions;
	}

	@Override
	public String toString() {
		return "GetConsumptionResponse [consumptions="
				+ Arrays.toString(consumptions) + "]";
	}

}
