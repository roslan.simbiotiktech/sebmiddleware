package com.seb.middleware.api.services.response.subscription;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;
import com.seb.middleware.utility.StringUtil;

public class GetStatementResponse extends Response{
	
	@JsonField(name = "PDF")
	private String pdf;

	public String getPdf() {
		return pdf;
	}

	public void setPdf(String pdf) {
		this.pdf = pdf;
	}

	

	@Override
	public String toString() {
		return "GetStatementResponse [pdf (size)=" + StringUtil.getHumanReadableFileSizeFromBase64(pdf) + ", responseStatus=" + responseStatus + "]";
	}
}
