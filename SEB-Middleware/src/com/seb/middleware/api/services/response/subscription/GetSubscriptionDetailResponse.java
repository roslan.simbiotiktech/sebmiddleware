package com.seb.middleware.api.services.response.subscription;

import java.util.Date;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GetSubscriptionDetailResponse extends Response{
	
	@JsonField(name = "CONTRACT_ACC_NO")
	private String contractAccNo;
	
	@JsonField(name = "CONTRACT_ACC_NICK")
	private String contractAccNick;
	
	@JsonField(name = "CONTRACT_ACC_NAME")
	private String contractAccName;
	
	@JsonField(name = "CURRENT_BILL_AMT_DUE")
	private double currentBillAmtDue;
	
	@JsonField(name = "AMT_DUE")
	private double amtDue;
	
	@JsonField(name = "EXCESS_PAYMENT_AMT")
	private double excessPaymentAmount;
	
	@JsonField(name = "BILL_PERIOD_FROM", outputDateFormat = "dd/MM/yyyy")
	private Date billPeriodFrom;
	
	@JsonField(name = "BILL_PERIOD_TO", outputDateFormat = "dd/MM/yyyy")
	private Date billPeriodTo;
	
	@JsonField(name = "PAYMENT_DUE_DATE", outputDateFormat = "dd/MM/yyyy")
	private Date paymentDueDate;
	
	@JsonField(name = "SUBS_TYPE")
	private String subscriptionType;
	
	@JsonField(name = "NOTIFY_BY_EMAIL")
	private boolean notifyByEmail;
	
	@JsonField(name = "NOTIFY_BY_SMS")
	private boolean notifyBySMS;
	
	@JsonField(name = "NOTIFY_BY_MOBILE_PUSH")
	private boolean notifyByMobilePush;

	public String getContractAccNo() {
		return contractAccNo;
	}

	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}

	public String getContractAccNick() {
		return contractAccNick;
	}

	public void setContractAccNick(String contractAccNick) {
		this.contractAccNick = contractAccNick;
	}

	public String getContractAccName() {
		return contractAccName;
	}

	public void setContractAccName(String contractAccName) {
		this.contractAccName = contractAccName;
	}

	public double getCurrentBillAmtDue() {
		return currentBillAmtDue;
	}

	public void setCurrentBillAmtDue(double currentBillAmtDue) {
		this.currentBillAmtDue = currentBillAmtDue;
	}

	public double getAmtDue() {
		return amtDue;
	}

	public void setAmtDue(double amtDue) {
		this.amtDue = amtDue;
	}

	public double getExcessPaymentAmount() {
		return excessPaymentAmount;
	}

	public void setExcessPaymentAmount(double excessPaymentAmount) {
		this.excessPaymentAmount = excessPaymentAmount;
	}

	public Date getBillPeriodFrom() {
		return billPeriodFrom;
	}

	public void setBillPeriodFrom(Date billPeriodFrom) {
		this.billPeriodFrom = billPeriodFrom;
	}

	public Date getBillPeriodTo() {
		return billPeriodTo;
	}

	public void setBillPeriodTo(Date billPeriodTo) {
		this.billPeriodTo = billPeriodTo;
	}

	public Date getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	public String getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public boolean isNotifyByEmail() {
		return notifyByEmail;
	}

	public void setNotifyByEmail(boolean notifyByEmail) {
		this.notifyByEmail = notifyByEmail;
	}

	public boolean isNotifyBySMS() {
		return notifyBySMS;
	}

	public void setNotifyBySMS(boolean notifyBySMS) {
		this.notifyBySMS = notifyBySMS;
	}

	public boolean isNotifyByMobilePush() {
		return notifyByMobilePush;
	}

	public void setNotifyByMobilePush(boolean notifyByMobilePush) {
		this.notifyByMobilePush = notifyByMobilePush;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GetSubscriptionDetailResponse [contractAccNo=").append(contractAccNo)
				.append(", contractAccNick=").append(contractAccNick).append(", contractAccName=")
				.append(contractAccName).append(", currentBillAmtDue=").append(currentBillAmtDue).append(", amtDue=")
				.append(amtDue).append(", excessPaymentAmount=").append(excessPaymentAmount).append(", billPeriodFrom=")
				.append(billPeriodFrom).append(", billPeriodTo=").append(billPeriodTo).append(", paymentDueDate=")
				.append(paymentDueDate).append(", subscriptionType=").append(subscriptionType)
				.append(", notifyByEmail=").append(notifyByEmail).append(", notifyBySMS=").append(notifyBySMS)
				.append(", notifyByMobilePush=").append(notifyByMobilePush).append("]");
		return builder.toString();
	}
}
