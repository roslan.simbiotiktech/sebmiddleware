package com.seb.middleware.api.services.response.subscription;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GetSubscriptionPaymentHistoryResponse extends Response{
	
	@JsonField(name = "PAYMENT_HISTORY")
	private PaymentHistory[] paymentHistories;

	public PaymentHistory[] getPaymentHistories() {
		return paymentHistories;
	}

	public void setPaymentHistories(PaymentHistory[] paymentHistories) {
		this.paymentHistories = paymentHistories;
	}

	@Override
	public String toString() {
		return "GetSubscriptionPaymentHistoryResponse [paymentHistories="
				+ Arrays.toString(paymentHistories) + "]";
	}

}
