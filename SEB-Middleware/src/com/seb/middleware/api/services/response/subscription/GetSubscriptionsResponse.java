package com.seb.middleware.api.services.response.subscription;

import java.util.Arrays;

import com.anteater.library.json.JsonField;
import com.seb.middleware.api.services.response.Response;

public class GetSubscriptionsResponse extends Response{
	
	@JsonField(name = "SUBSCRIPTION")
	private Subscription [] subscriptions;
	
	@JsonField(name = "TOTAL_AMT_DUE")
	private double totalAmtDue;

	public Subscription[] getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(Subscription[] subscriptions) {
		this.subscriptions = subscriptions;
	}

	public double getTotalAmtDue() {
		return totalAmtDue;
	}

	public void setTotalAmtDue(double totalAmtDue) {
		this.totalAmtDue = totalAmtDue;
	}

	@Override
	public String toString() {
		return "GetSubscriptionsResponse [subscriptions=" + Arrays.toString(subscriptions) + ", totalAmtDue=" + totalAmtDue + ", responseStatus="
				+ responseStatus + "]";
	}	
}
