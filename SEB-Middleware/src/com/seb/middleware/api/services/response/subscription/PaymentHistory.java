package com.seb.middleware.api.services.response.subscription;

import java.util.Date;

import com.anteater.library.json.JsonField;

public class PaymentHistory {
	
	@JsonField(name = "PAYMENT_DATE", outputDateFormat = "dd/MM/yyyy")
	private Date paymentDate;
	
	@JsonField(name = "AMOUNT")
	private double amount;

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "PaymentHistory [paymentDate=" + paymentDate + ", amount=" + amount + "]";
	}
}
