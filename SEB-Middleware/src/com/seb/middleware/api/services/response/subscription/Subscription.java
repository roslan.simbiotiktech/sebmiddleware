package com.seb.middleware.api.services.response.subscription;

import java.util.Date;

import com.anteater.library.json.JsonField;

public class Subscription {
	
	@JsonField(name = "CONTRACT_ACC_NICK")
	private String contractAccountNick;
	
	@JsonField(name = "CONTRACT_ACC_NO")
	private String contractAccNo;
	
	@JsonField(name = "CONTRACT_ACC_NAME")
	private String contractAccName;
	
	@JsonField(name = "SUBS_TYPE")
	private String subscriptionType;
	
	@JsonField(name = "AMT_DUE")
	private double amtDue;

	@JsonField(name = "CURRENT_BILL_AMT_DUE")
	private double currentBillAmtDue;
	
	@JsonField(name = "EXCESS_PAYMENT_AMT")
	private double excessPaymentAmount;
	
	@JsonField(name = "PAYMENT_DUE_DATE", outputDateFormat = "dd/MM/yyyy")
	private Date paymentDueDate;
	
	public String getContractAccountNick() {
		return contractAccountNick;
	}

	public void setContractAccountNick(String contractAccountNick) {
		this.contractAccountNick = contractAccountNick;
	}

	public String getContractAccNo() {
		return contractAccNo;
	}

	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}

	public String getContractAccName() {
		return contractAccName;
	}

	public void setContractAccName(String contractAccName) {
		this.contractAccName = contractAccName;
	}

	public String getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public double getAmtDue() {
		return amtDue;
	}

	public void setAmtDue(double amtDue) {
		this.amtDue = amtDue;
	}

	public double getCurrentBillAmtDue() {
		return currentBillAmtDue;
	}

	public void setCurrentBillAmtDue(double currentBillAmtDue) {
		this.currentBillAmtDue = currentBillAmtDue;
	}

	public double getExcessPaymentAmount() {
		return excessPaymentAmount;
	}

	public void setExcessPaymentAmount(double excessPaymentAmount) {
		this.excessPaymentAmount = excessPaymentAmount;
	}

	public Date getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	@Override
	public String toString() {
		return "Subscription [contractAccountNick=" + contractAccountNick + ", contractAccNo=" + contractAccNo + ", contractAccName="
				+ contractAccName + ", subscriptionType=" + subscriptionType + ", amtDue=" + amtDue + ", currentBillAmtDue=" + currentBillAmtDue
				+ ", excessPaymentAmount=" + excessPaymentAmount + ", paymentDueDate=" + paymentDueDate + "]";
	}
}
