package com.seb.middleware.constant;

public enum AuthenticationStatus {

	SUCCESS,
	SUCCESS_PASSWORD_EXPIRED_WARNING,
	FAILED
	
}
