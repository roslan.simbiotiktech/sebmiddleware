package com.seb.middleware.constant;

public enum CRMReportStatus {

	NEW(TransStatus.OPEN, NewTransStatus.OPEN),
	OPEN(TransStatus.ASSIGNED, NewTransStatus.OPEN),
	ESCALATED(TransStatus.IN_PROGRESS, NewTransStatus.ESCALATED),
	ASSIGNED(TransStatus.IN_PROGRESS, NewTransStatus.ASSIGNED),
	RESOLVED(TransStatus.RESOLVED, NewTransStatus.RESOLVED),
	CLOSED(TransStatus.RESOLVED, NewTransStatus.RESOLVED);
	
	TransStatus transactionStatus;
	NewTransStatus newTransactionStatus;
	
	CRMReportStatus(TransStatus transStatus, NewTransStatus newStatus){
		this.transactionStatus = transStatus;
		this.newTransactionStatus = newStatus;
	}
	
	public TransStatus getTransactionStatus(){
		return transactionStatus;
	}
	
	public NewTransStatus getNewTransactionStatus() {
		return newTransactionStatus;
	}
}
