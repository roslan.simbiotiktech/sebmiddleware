package com.seb.middleware.constant;

public enum CSMainStation {

	KUCHING,
	SERIAN,
	SRI_AMAN,
	SIBU,
	SARIKEI,
	MIRI,
	BINTULU,
	LIMBANG
}
