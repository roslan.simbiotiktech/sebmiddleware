package com.seb.middleware.constant;

public enum CalculatorType {

	DOMESTIC,
	COMMERCIAL_C1,
	COMMERCIAL_C2,
	COMMERCIAL_C3,
	INDUSTRIAL_I1,
	INDUSTRIAL_I2,
	INDUSTRIAL_I3,
	PUBLIC_LIGHTING;

}
