package com.seb.middleware.constant;

public enum FPXBankType {

	B2C("01", true),
	B2B1("02", true),
	B2B2("03", false);
	
	private String msgToken;
	private boolean personal;
	
	FPXBankType(String msgToken, boolean personal) {
		this.msgToken = msgToken;
		this.personal = personal;
	}
	
	public String getMessageToken(){
		return msgToken;
	}

	public boolean isPersonal() {
		return personal;
	}
}
