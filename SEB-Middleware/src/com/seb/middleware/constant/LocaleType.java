package com.seb.middleware.constant;

public enum LocaleType {
	en,
	zh,
	ms
}
