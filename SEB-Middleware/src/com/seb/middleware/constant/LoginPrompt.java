package com.seb.middleware.constant;

public enum LoginPrompt {

	CHANGE_PASSWORD,
	ACCOUNT_ACTIVATION,
	REACTIVATION
	
}
