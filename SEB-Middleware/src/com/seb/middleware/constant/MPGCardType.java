package com.seb.middleware.constant;

public enum MPGCardType {

	V("VISA"),
	M("MASTER"),
	A("AMEX");
	
	private String spgName;
	
	MPGCardType(String spgName){
		this.spgName = spgName;
	}

	public String getSpgName() {
		return spgName;
	}
}
