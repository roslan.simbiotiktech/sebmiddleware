package com.seb.middleware.constant;

public enum MessageStatus {
	READ,
	UNREAD
}
