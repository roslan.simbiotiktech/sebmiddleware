package com.seb.middleware.constant;

public enum NewTransStatus {

	OPEN,
	ESCALATED,
	PLEASE_CONTACT_SEB,
	ASSIGNED,
	IN_PROGRESS,
	RESOLVED
}
