package com.seb.middleware.constant;

public enum NotificationStatus {
	
	UNSENT,
	SENT,
	SENDING,
	CANCELLED
	
}
