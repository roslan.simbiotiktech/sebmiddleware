package com.seb.middleware.constant;

public enum NotificationType {
	PLAIN,
	PROMO,
	POWER_ALERT,
	REPORT,
	BILL
}
