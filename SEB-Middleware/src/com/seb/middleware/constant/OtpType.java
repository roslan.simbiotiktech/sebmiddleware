package com.seb.middleware.constant;

public enum OtpType {
	
	ALL,
	FORGOT_PASS,
	CHANGE_PASS;
	
}
