package com.seb.middleware.constant;

public enum PaymentStatus {

	PENDING(false),  // upon payment start
	FAILED(true), // failed
	RECEIVED(true), // upon received confirmation from gateway
	BILLED(false), // after update into SAP,
	PENAUTH(false);
	
	private boolean shouldCompletePayment;
	
	PaymentStatus(boolean shouldCompletePayment){
		this.shouldCompletePayment = shouldCompletePayment;
	}

	public boolean isShouldCompletePayment() {
		return shouldCompletePayment;
	}
}
