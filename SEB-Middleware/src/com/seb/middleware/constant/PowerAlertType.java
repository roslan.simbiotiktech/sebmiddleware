package com.seb.middleware.constant;

public enum PowerAlertType {

	OUTAGE_ANNOUNCEMENT("Outage Announcement"), 
	PREVENTIVE_MAINTENANCE("Preventive Maintenance");

	private String value;

	private PowerAlertType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static PowerAlertType fromValue(String text) {
		if (text != null) {
			for (PowerAlertType powerAlertType : PowerAlertType.values()) {
				if (text.equalsIgnoreCase(powerAlertType.getValue())) {
					return powerAlertType;
				}
			}
		}
		return null;
	}
}
