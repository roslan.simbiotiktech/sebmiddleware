package com.seb.middleware.constant;

public enum PreferredCommunicationMethod {

	EMAIL,
	MOBILE
	
}
