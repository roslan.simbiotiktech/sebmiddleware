package com.seb.middleware.constant;

public enum ReportCategory {

	ENQUIRIES("Enquiry/Request"),
	COMPLAINT("Complaint"),
	COMPLIMENT("Compliment"),
	SUGGESTION("Suggestion/Feedback");
	
	private String name;
	
	ReportCategory(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
