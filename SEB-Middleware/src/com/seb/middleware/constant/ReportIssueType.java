package com.seb.middleware.constant;

public enum ReportIssueType {

	BILLING("Bill"),
	METER_READING("Meter");
	
	private String name;
	ReportIssueType(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
