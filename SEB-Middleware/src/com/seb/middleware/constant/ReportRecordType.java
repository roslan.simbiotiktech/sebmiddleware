package com.seb.middleware.constant;

public enum ReportRecordType {
	
	SFDC("S"),
	CRM("C");
	
	String prefix;
	
	ReportRecordType(String prefix) {
		this.prefix = prefix;
	}
	
	public String getPrefix() {
		return prefix;
	}
}
