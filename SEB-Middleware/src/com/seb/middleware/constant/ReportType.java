package com.seb.middleware.constant;

public enum ReportType {
	
	FAULTY_STREET_LIGHT("Street Lighting", SFDCClassification.TECHNICAL_ISSUES),
	OUTAGE("Outage", SFDCClassification.TECHNICAL_ISSUES),
	TECHNICAL_ISSUE("Technical Others", SFDCClassification.TECHNICAL_ISSUES),
	BILLING_AND_METER("Bill", SFDCClassification.CUSTOMER_SERVICE),
	GENERAL_INQUIRY("General Enquiry", SFDCClassification.CUSTOMER_SERVICE);
	
	private String name;
	private SFDCClassification classification;
	
	ReportType(String name, SFDCClassification classification) {
		this.name = name;
		this.classification = classification;
	}
	
	public String getName() {
		return name;
	}
	
	public SFDCClassification getClassification() {
		return classification;
	}
	
	public static ReportType getByName(String name) {
		for(ReportType rt : ReportType.values()) {
			if(rt.getName().equalsIgnoreCase(name)) {
				return rt;
			}
		}
		return null;
	}
}
