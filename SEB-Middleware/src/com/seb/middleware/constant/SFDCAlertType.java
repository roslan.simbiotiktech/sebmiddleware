package com.seb.middleware.constant;

public enum SFDCAlertType {

	UNPLANNED(PowerAlertType.OUTAGE_ANNOUNCEMENT), 
	SCHEDULED(PowerAlertType.PREVENTIVE_MAINTENANCE);

	private PowerAlertType type;

	private SFDCAlertType(PowerAlertType type) {
		this.type = type;
	}

	public PowerAlertType getType() {
		return type;
	}
}
