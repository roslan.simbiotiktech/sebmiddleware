package com.seb.middleware.constant;

public enum SFDCClassification {
	
	CUSTOMER_SERVICE("Customer Service"),
	TECHNICAL_ISSUES("Technical Issues");
	
	private String name;
	
	SFDCClassification(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
