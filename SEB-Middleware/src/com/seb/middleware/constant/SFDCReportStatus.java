package com.seb.middleware.constant;

public enum SFDCReportStatus {

	NEW,
	ESCALATED,
	IN_PROGRESS,
	REJECTED,
	REOPEN,
	REASSIGNED,
	RESOLVED,
	CLOSED;
	
}
