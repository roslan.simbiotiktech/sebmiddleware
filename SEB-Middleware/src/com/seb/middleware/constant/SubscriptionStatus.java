package com.seb.middleware.constant;

public enum SubscriptionStatus {
	
	ACTIVE,
	INACTIVE,
	DELETED;
	
}
