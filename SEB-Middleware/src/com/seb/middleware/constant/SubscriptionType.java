package com.seb.middleware.constant;

public enum SubscriptionType {
	
	CHILDREN,
	EMPLOYEE,
	EMPLOYER,
	PARENT,
	RELATIVE,
	SIBLING,
	SPOUSE,
	TENANT,
	OWNER
}
