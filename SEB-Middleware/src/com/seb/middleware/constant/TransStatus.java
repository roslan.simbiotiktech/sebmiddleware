package com.seb.middleware.constant;

public enum TransStatus {

	OPEN,
	ASSIGNED,
	IN_PROGRESS,
	RESOLVED
	
}
