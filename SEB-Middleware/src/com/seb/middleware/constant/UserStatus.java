package com.seb.middleware.constant;

public enum UserStatus {

	ACTIVE, // ACTIVATED
	PENDING, // PENDING ACTIVATION
	SUSPENDED,
	DEACTIVATED,
	TERMINATE
	
}
