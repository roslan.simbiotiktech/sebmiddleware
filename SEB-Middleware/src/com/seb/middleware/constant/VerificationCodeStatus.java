package com.seb.middleware.constant;

public enum VerificationCodeStatus {

	VALID,
	INVALID,
	EXPIRED
	
}
