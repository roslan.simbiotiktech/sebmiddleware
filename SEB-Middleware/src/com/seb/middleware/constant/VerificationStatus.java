package com.seb.middleware.constant;

public enum VerificationStatus {

	UNUSED,
	USED,
	INVALIDATE;
	
}
