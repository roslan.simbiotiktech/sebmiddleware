package com.seb.middleware.core;

public interface BaseService {

	public void startService();
	public void stopService();
	
}
