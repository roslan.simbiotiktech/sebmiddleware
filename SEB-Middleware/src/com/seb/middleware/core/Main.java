package com.seb.middleware.core;

import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.RoutingService;
import com.seb.middleware.jco.SAPService;
import com.seb.middleware.jpa.JPA;
import com.seb.middleware.ldap.LDAP;
import com.seb.middleware.scheduler.BillReadinessNotifier;
import com.seb.middleware.scheduler.CaseReportChecker;

public class Main extends HttpServlet {

	private static final Logger logger = LogManager.getLogger(Main.class);
	private static final long serialVersionUID = -1453690395805617138L;

	private ArrayList<BaseService> services;
	
	public Main(){
		services = new ArrayList<BaseService>();
	}
	
	private void initServices(){
		
		logger.info("Initializing Services");
		
		RoutingService routing = new RoutingService();
		JPA jpa = new JPA();
		LDAP ldap = new LDAP();
		SAPService sap = new SAPService();
		CaseReportChecker reportChecker = new CaseReportChecker();
		BillReadinessNotifier billReadinessNotifier = new BillReadinessNotifier();
		
		services.add(routing);
		services.add(jpa);
		services.add(ldap);
		services.add(sap);
		services.add(reportChecker);
		services.add(billReadinessNotifier);
	}
	
	public void startServices(){
		for(BaseService service : services){
			try{
				service.startService();
			}catch(Throwable ex){
				logger.error("Error starting service. ", ex);
			}
		}
	}
	
	public void stopServices(){
		for(BaseService service : services){
			try{
				service.stopService();
			}catch(Throwable ex){
				logger.error("Error Stopping service. ", ex);
			}
		}
	}
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		initServices();
		startServices();
		logger.info("System initialized complete.");
	}

	@Override
    public void destroy(){
		stopServices();
		logger.info("System destroyed.");
    }
}
