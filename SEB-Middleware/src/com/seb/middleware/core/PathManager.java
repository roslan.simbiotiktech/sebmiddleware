package com.seb.middleware.core;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PathManager {

	private static final Logger logger = LogManager.getLogger(PathManager.class);
	
	private static final String CONFIG_BASE = "seb.conf.base";
	private static final String TEMPLATES_BASE = "seb.templates.base";
	
	private static final String EMAIL = "email";
	private static final String SMS = "sms";
	private static final String STATIC = "static";
	
	public static String getConfigPath(String configFileName){
		StringBuilder sb = new StringBuilder();
		sb.append(getConfigurationBase());
		sb.append(File.separator);
		sb.append(configFileName);
		
		String path = sb.toString();
		logger.info(path);
		
		return path;
	}
	
	public static File getConfigFile(String configFileName){
		return new File(getConfigPath(configFileName));
	}
	
	private static String getConfigurationBase(){
		return System.getProperty(CONFIG_BASE);
	}
	
	private static String getTemplatesBase(){
		return System.getProperty(TEMPLATES_BASE);
	}
	
	public static String getEmailTemplatePath(String templateFileName){
		StringBuilder sb = new StringBuilder();
		sb.append(getTemplatesBase());
		sb.append(File.separator);
		sb.append(EMAIL);
		sb.append(File.separator);
		sb.append(templateFileName);
		return sb.toString();
	}
	
	public static File getEmailTemplateFile(String templateFileName){
		return new File(getEmailTemplatePath(templateFileName));
	}
	
	public static String getSmsTemplatePath(String templateFileName){
		StringBuilder sb = new StringBuilder();
		sb.append(getTemplatesBase());
		sb.append(File.separator);
		sb.append(SMS);
		sb.append(File.separator);
		sb.append(templateFileName);
		return sb.toString();
	}
	
	public static File getSmsTemplateFile(String templateFileName){
		return new File(getSmsTemplatePath(templateFileName));
	}

	public static String getStaticTemplatePath(String templateFileName){
		StringBuilder sb = new StringBuilder();
		sb.append(getTemplatesBase());
		sb.append(File.separator);
		sb.append(STATIC);
		sb.append(File.separator);
		sb.append(templateFileName);
		return sb.toString();
	}
	
	public static File getStaticTemplateFile(String templateFileName){
		return new File(getStaticTemplatePath(templateFileName));
	}
}
