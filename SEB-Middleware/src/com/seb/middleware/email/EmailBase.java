package com.seb.middleware.email;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.core.PathManager;

public abstract class EmailBase {
	
	private static final Logger logger = LogManager.getLogger(EmailBase.class);
	
	private static final String HEADER = "cid:Header";
	private static final String FOOTER = "cid:Footer";
	private static final String PAYNOW = "cid:Paynow";
	
	private static final String LOOP_START = "loop_start";
	private static final String LOOP_END = "loop_end";
	
	protected void send(String from, String [] to, String [] cc, String [] bcc, String subject, String text, String type, String [] attachments) throws MessagingException, NamingException{

		Context initCtx = new InitialContext();
		Context envCtx = (Context) initCtx.lookup("java:comp/env");
		Session session = (Session) envCtx.lookup("mail/Session");
		
		Message message = new MimeMessage(session);
		message.setSubject(subject);
		message.setFrom(new InternetAddress(from));
		
		InternetAddress toAddr [] = convert(to);
		InternetAddress ccAddr [] = convert(cc);
		InternetAddress bccAddr [] = convert(bcc);

		if(toAddr != null){
			message.setRecipients(Message.RecipientType.TO, toAddr);	
		}
		if(ccAddr != null){
			message.setRecipients(Message.RecipientType.CC, ccAddr);	
		}
		if(bccAddr != null){
			message.setRecipients(Message.RecipientType.BCC, bccAddr);	
		}
		
		MimeMultipart multipart = new MimeMultipart("related");
		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(text, type);
		multipart.addBodyPart(messageBodyPart);
		embededBranding(multipart, text);
		embededAttachment(multipart, attachments);
		
		message.setContent(multipart);
		Transport.send(message);
    }
	
	private void embededAttachment(MimeMultipart multipart, String [] attachments) throws MessagingException{

		if(attachments != null && attachments.length > 0){
			
			for(String attach : attachments){
				File f = new File(attach);
				
				BodyPart attachmentPart = new MimeBodyPart();
				DataSource fds = new FileDataSource(attach);
				attachmentPart.setDataHandler(new DataHandler(fds));
				attachmentPart.setFileName(f.getName());
				multipart.addBodyPart(attachmentPart);
				
				logger.debug("Embeded attachment");
			}
		}
	}
	
	private void embededBranding(MimeMultipart multipart, String text) throws MessagingException{

		if(text.indexOf(HEADER) != -1){
			// Add Header
			BodyPart headerImagePart = new MimeBodyPart();
			DataSource fds = new FileDataSource(PathManager.getEmailTemplatePath("email_banner_top.jpg"));
			headerImagePart.setDataHandler(new DataHandler(fds));
			headerImagePart.setHeader("Content-ID", "<Header>");
			multipart.addBodyPart(headerImagePart);
			
			logger.debug("Embeded sesco header image");
		}
		
		if(text.indexOf(FOOTER) != -1){
			// Add Footer
			BodyPart footerImagePart = new MimeBodyPart();
			DataSource fds = new FileDataSource(PathManager.getEmailTemplatePath("email_banner_bottom.jpg"));
			footerImagePart.setDataHandler(new DataHandler(fds));
			footerImagePart.setHeader("Content-ID", "<Footer>");
			multipart.addBodyPart(footerImagePart);
			logger.debug("Embeded sesco footer image");
		}
		
		if(text.indexOf(PAYNOW) != -1){
			// Add Paynow
			BodyPart paynowImagePart = new MimeBodyPart();
			DataSource fds = new FileDataSource(PathManager.getEmailTemplatePath("paynow_logo.jpg"));
			paynowImagePart.setDataHandler(new DataHandler(fds));
			paynowImagePart.setHeader("Content-ID", "<Paynow>");
			multipart.addBodyPart(paynowImagePart);
			logger.debug("Embeded paynow image");
		}
	}
	
	private InternetAddress[] convert(String [] address) throws AddressException{
		if(address != null && address.length > 0){
			
			InternetAddress[] addr = new InternetAddress[address.length];
			for(int i = 0; i < address.length; i++){
				addr[i] = new InternetAddress(address[i]);
			}
			
			return addr;
		}else{
			return null;
		}
	}
	
	protected String processTemplateFile(String filePath, 
			Map<String, String> properties) throws IOException{
		return processTemplateFile(filePath, properties, null);
	}
	
	protected String processTemplateFile(String filePath, 
			Map<String, String> properties,
			Map<String, List<Map<String, String>>> loops) throws IOException{
		
		String content = new String(Files.readAllBytes(Paths.get(filePath)));
		StringBuilder sb = new StringBuilder(content);
		
		if(loops != null && !loops.isEmpty()){
			processTemplateLoops(sb, loops);
		}

		if(properties != null && !properties.isEmpty()){
			processTemplateParams(sb, properties);
		}
		
		return sb.toString();
	}
	
	protected void processTemplateParams(StringBuilder content, Map<String, String> properties){
		
		Set<Entry<String, String>> set = properties.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		
		while(iterator.hasNext()){
			Entry<String, String> next = iterator.next();
			replaceAll(content, "{" + next.getKey() + "}", next.getValue());
		}
	}
	
	protected void processTemplateLoops(StringBuilder content, Map<String, List<Map<String, String>>> loops){
		
		Set<Entry<String, List<Map<String, String>>>> set = loops.entrySet();
		Iterator<Entry<String, List<Map<String, String>>>> iterator = set.iterator();
		
		while(iterator.hasNext()){
			Entry<String, List<Map<String, String>>> next = iterator.next();
			String loopStart = "{" + LOOP_START + ":" + next.getKey() + "}";
			String loopEnd = "{" + LOOP_END + ":" + next.getKey() + "}";
			
			int loopStartIndex = content.indexOf(loopStart);
			int loopEndIndex = content.indexOf(loopEnd);
			
			if(loopStartIndex == -1 || loopEndIndex == -1){
				logger.error("can't find loop start/end tag {} {}, {}", next.getKey(), loopStartIndex, loopEndIndex);
				continue;
			}else if(loopEndIndex < loopStartIndex){
				logger.error("loop end tag must be after start tag {} {}, {}", next.getKey(), loopStartIndex, loopEndIndex);
				continue;
			}
			
			String loopTemplate = content.substring(loopStartIndex + loopStart.length(), loopEndIndex);
			List<Map<String, String>> list = next.getValue();
			
			StringBuilder sb = new StringBuilder();
			for(Map<String, String> entry : list){
				StringBuilder tpl = new StringBuilder(loopTemplate);
				processTemplateParams(tpl, entry);
				
				sb.append(tpl);
			}
			
			content.replace(loopStartIndex, loopEndIndex + loopEnd.length(), sb.toString());
		}
	}
	
	private void replaceAll(StringBuilder builder, String from, String to){
	    int index = builder.indexOf(from);
	    while (index != -1)
	    {
	        builder.replace(index, index + from.length(), to);
	        index += to.length(); // Move to the end of the replacement
	        index = builder.indexOf(from, index);
	    }
	}
	
	protected String getSubject(String file, StringBuilder outContent) {
		int lineNumber = 0;
		String subject = null;
		Scanner scanner = new Scanner(file);
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			
			if(lineNumber == 0){
				subject = line.trim();
			}else{
				outContent.append(line);
				if(scanner.hasNextLine()){
					outContent.append("\n");
				}
			}
			
			lineNumber++;
		}
		scanner.close();
		
		return subject;
	}
}
