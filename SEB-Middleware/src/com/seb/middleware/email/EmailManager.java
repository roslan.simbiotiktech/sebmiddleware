package com.seb.middleware.email;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.mail.MessagingException;
import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.core.PathManager;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.helper.SettingHelper;

public class EmailManager extends EmailBase {

	private static final Logger logger = LogManager.getLogger(EmailManager.class);

	private String senderEmail;
	private boolean testingEnvironment;

	public EmailManager() {
		DatabaseFacade db = new DatabaseFacade();

		try {
			SettingHelper setting = new SettingHelper(db);
			senderEmail = setting.getEmailSenderAddress();
			testingEnvironment = setting.isTestingEnvironment();
		} finally {
			db.close();
		}
	}

	public void sendSuccessPaymentEmail(
			String email, String name, String referenceNo, 
			Date transactionDate, Map<String, BigDecimal> entries, String paymentMethod,
			String gatewayReferenceLabel, String gatewaytReference)
			throws IOException, MessagingException, NamingException {

		DecimalFormat df = new DecimalFormat("#,###.00");
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("name", name);
		properties.put("reference_no", referenceNo);
		properties.put("date", transactionDate != null ? sdf.format(transactionDate) : "N/A");
		properties.put("payment_method", paymentMethod);
		properties.put("gateway_reference_label", gatewayReferenceLabel);
		properties.put("gateway_reference", gatewaytReference);
		
		Map<String, List<Map<String, String>>> loops = new HashMap<String, List<Map<String,String>>>();
		List<Map<String, String>> list = new ArrayList<Map<String,String>>();
		loops.put("cas", list);
		
		for(Entry<String, BigDecimal> entry : entries.entrySet()){
			
			Map<String, String> ca = new HashMap<String, String>();
			ca.put("amount", df.format(entry.getValue()));
			ca.put("contract_acc_no", entry.getKey());
			
			list.add(ca);
		}

		String template = PathManager.getEmailTemplatePath("tpl_payment_successful.txt");
		String content = processTemplateFile(template, properties, loops);

		StringBuilder actualContent = new StringBuilder();
		String title = getSubject(content, actualContent);

		send(new String[] { email }, title, actualContent.toString());
	}
	
	public void sendUnsuccessPaymentEmail(
			String email, String name, String referenceNo, 
			Date transactionDate, Map<String, BigDecimal> entries, 
			String paymentMethod,
			String gatewayReferenceLabel, String gatewaytReference)
			throws IOException, MessagingException, NamingException {

		DecimalFormat df = new DecimalFormat("#,###.00");
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("name", name);
		properties.put("reference_no", referenceNo);
		properties.put("date", transactionDate != null ? sdf.format(transactionDate) : "N/A");
		properties.put("payment_method", paymentMethod);
		properties.put("gateway_reference_label", gatewayReferenceLabel);
		properties.put("gateway_reference", gatewaytReference);

		Map<String, List<Map<String, String>>> loops = new HashMap<String, List<Map<String,String>>>();
		List<Map<String, String>> list = new ArrayList<Map<String,String>>();
		loops.put("cas", list);
		
		for(Entry<String, BigDecimal> entry : entries.entrySet()){
			
			Map<String, String> ca = new HashMap<String, String>();
			ca.put("amount", df.format(entry.getValue()));
			ca.put("contract_acc_no", entry.getKey());
			
			list.add(ca);
		}
		
		String template = PathManager.getEmailTemplatePath("tpl_payment_unsuccessful.txt");
		String content = processTemplateFile(template, properties, loops);

		StringBuilder actualContent = new StringBuilder();
		String title = getSubject(content, actualContent);

		send(new String[] { email }, title, actualContent.toString());
	}
	
	public void sendPendingAuthorizationPaymentEmail(
			String email, String name, String referenceNo, 
			Date transactionDate, Map<String, BigDecimal> entries, 
			String paymentMethod,
			String gatewayReferenceLabel, String gatewaytReference)
			throws IOException, MessagingException, NamingException {

		DecimalFormat df = new DecimalFormat("#,###.00");
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("name", name);
		properties.put("reference_no", referenceNo);
		properties.put("date", transactionDate != null ? sdf.format(transactionDate) : "N/A");
		properties.put("payment_method", paymentMethod);
		properties.put("gateway_reference_label", gatewayReferenceLabel);
		properties.put("gateway_reference", gatewaytReference);

		Map<String, List<Map<String, String>>> loops = new HashMap<String, List<Map<String,String>>>();
		List<Map<String, String>> list = new ArrayList<Map<String,String>>();
		loops.put("cas", list);
		
		for(Entry<String, BigDecimal> entry : entries.entrySet()){
			
			Map<String, String> ca = new HashMap<String, String>();
			ca.put("amount", df.format(entry.getValue()));
			ca.put("contract_acc_no", entry.getKey());
			
			list.add(ca);
		}
		
		String template = PathManager.getEmailTemplatePath("tpl_payment_pen_auth.txt");
		String content = processTemplateFile(template, properties, loops);

		StringBuilder actualContent = new StringBuilder();
		String title = getSubject(content, actualContent);

		send(new String[] { email }, title, actualContent.toString());
	}

	public void sendSuccessRegisterEmail(String email, String faqUrl) throws IOException, MessagingException, NamingException {
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("email", email);
		properties.put("faqUrl", faqUrl);

		String template = PathManager.getEmailTemplatePath("tpl_success_registration.txt");
		String content = processTemplateFile(template, properties);

		StringBuilder actualContent = new StringBuilder();
		String title = getSubject(content, actualContent);

		send(new String[] { email }, title, actualContent.toString());
	}

	public void sendOtp(String email, String otp, int expirationMinutes) throws IOException, MessagingException, NamingException {
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("otp", otp);
		properties.put("minutes", String.valueOf(expirationMinutes));

		String template = PathManager.getEmailTemplatePath("tpl_one_time_password.txt");
		String content = processTemplateFile(template, properties);

		StringBuilder actualContent = new StringBuilder();
		String title = getSubject(content, actualContent);

		send(new String[] { email }, title, actualContent.toString());
	}

	public void sendEmailVerificationCode(String email, String verificationCode, int expirationMinutes)
			throws IOException, MessagingException, NamingException {
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("activation_code", verificationCode);
		properties.put("minutes", String.valueOf(expirationMinutes));

		String template = PathManager.getEmailTemplatePath("tpl_email_verification.txt");
		String content = processTemplateFile(template, properties);

		StringBuilder actualContent = new StringBuilder();
		String title = getSubject(content, actualContent);

		send(new String[] { email }, title, actualContent.toString());
	}

	public void sendForgotPassword(String email, String password) throws IOException, MessagingException, NamingException {
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("password", password);

		String template = PathManager.getEmailTemplatePath("tpl_forgot_password.txt");
		String content = processTemplateFile(template, properties);

		StringBuilder actualContent = new StringBuilder();
		String title = getSubject(content, actualContent);

		send(new String[] { email }, title, actualContent.toString());
	}

	public void sendSAPFileProcessingWarning(ArrayList<String> emails, String date, String fileName, String details)
			throws IOException, MessagingException, NamingException {
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("date", date);
		properties.put("filename", fileName);
		properties.put("details", details);

		String template = PathManager.getEmailTemplatePath("tpl_sap_file_process_warning.txt");
		String content = processTemplateFile(template, properties);

		StringBuilder actualContent = new StringBuilder();
		String title = getSubject(content, actualContent);

		String[] email = new String[emails.size()];
		for (int i = 0; i < emails.size(); i++) {
			email[i] = emails.get(i);
		}

		send(email, title, actualContent.toString());
	}
	
	public void sendSAPUpdatePaymentErrorReport(ArrayList<String> emails, String records)
			throws IOException, MessagingException, NamingException {
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("records", records);

		String template = PathManager.getEmailTemplatePath("tpl_billing_exception_report.txt");
		String content = processTemplateFile(template, properties);

		StringBuilder actualContent = new StringBuilder();
		String title = getSubject(content, actualContent);

		String[] email = new String[emails.size()];
		for (int i = 0; i < emails.size(); i++) {
			email[i] = emails.get(i);
		}

		send(email, title, actualContent.toString());
	}
	
	public void sendBillReadinessNotification(String email, String name, 
			String ca, BigDecimal currentTotalAmount, BigDecimal currentAmount, Date dueDate, String payUrl, 
			Date invoiceDate, String filePath) throws IOException, MessagingException, NamingException{
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		DecimalFormat df = new DecimalFormat("#,##0.00");
		
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("name", name);
		properties.put("ca", ca);
		properties.put("current_amount", df.format(currentAmount));
		properties.put("amount", df.format(currentTotalAmount));
		properties.put("due_date", sdf.format(dueDate));
		properties.put("pay_url", payUrl);
		properties.put("invoice_date", sdf.format(invoiceDate));
		
		String template = PathManager.getEmailTemplatePath("tpl_bill_ready.txt");
		String content = processTemplateFile(template, properties);

		StringBuilder actualContent = new StringBuilder();
		String title = getSubject(content, actualContent);

		send(new String[] { email }, title, actualContent.toString(), new String[] {filePath});
	}

	private void send(final String[] receiver, final String subject, final String body) throws MessagingException, NamingException {
		send(receiver, subject, body, null);
	}
	
	private void send(final String[] receiver, final String subject, final String body, String [] attachmentsPath) throws MessagingException, NamingException {
		logger.info("Sending email to " + Arrays.toString(receiver) + " for subject: " + subject);

		if (testingEnvironment) {
			logger.info("Content: " + body);
		}

		if (testingEnvironment) {
			// Testing env does not trigger error
			try {
				send(senderEmail, receiver, null, null, subject, body, "text/html", attachmentsPath);
				logger.info("Email sent successfully");
			} catch (MessagingException | NamingException e) {
				logger.error("Error sending mail (testing env).", e);
			}
		} else {
			send(senderEmail, receiver, null, null, subject, body, "text/html", attachmentsPath);
			logger.info("Email sent successfully");
		}
	}
}
