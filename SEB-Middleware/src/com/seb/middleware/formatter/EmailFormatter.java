package com.seb.middleware.formatter;

import com.anteater.library.json.CustomFormatter;

public class EmailFormatter extends CustomFormatter<String>{

	@Override
	public String format(String obj) {
		if(obj != null) return obj.trim().toLowerCase();
		else return obj;
	}

}
