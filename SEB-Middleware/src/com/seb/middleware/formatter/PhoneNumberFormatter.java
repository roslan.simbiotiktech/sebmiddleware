package com.seb.middleware.formatter;

import com.anteater.library.json.CustomFormatter;

public class PhoneNumberFormatter extends CustomFormatter<String>{

	private static String SYSTEM_COUNTRY_CODE = "6";
		
	@Override
	public String format(String obj) {
		if(obj != null){
			return formatString(obj);
		}
		
		return obj;
	}
	
	public String formatString(String msisdn){
		String phone = msisdn.trim();
		
		while(phone.startsWith("+")){
			phone = phone.substring(1);
		}
		
		if(phone.startsWith("0")){
			phone = SYSTEM_COUNTRY_CODE + phone;
		}
		
		return phone;
	}

	

}
