package com.seb.middleware.jco;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sap.conn.jco.ext.DataProviderException;
import com.sap.conn.jco.ext.DestinationDataEventListener;
import com.sap.conn.jco.ext.DestinationDataProvider;
import com.seb.middleware.core.PathManager;

public class CustomDestinationDataProvider implements DestinationDataProvider{
	private static final Logger logger = LogManager.getLogger(CustomDestinationDataProvider.class);

	@Override
	public Properties getDestinationProperties(String destinationName) {
		try {
            //read the destination from DB
            Properties properties = new Properties();
            properties.load(new FileInputStream(PathManager.getConfigFile(destinationName + ".properties")));
            
            if(properties != null){
                if(properties.isEmpty())
                    throw new DataProviderException(DataProviderException.Reason.INVALID_CONFIGURATION, "destination configuration is empty", null);

                return properties;
            }
        } catch (FileNotFoundException e) {
			logger.error("unable to load sap config file", e);
		} catch (IOException e) {
			logger.error("unable to load sap config file", e);
		}
		
		return null;
	}

	@Override
	public void setDestinationDataEventListener(DestinationDataEventListener destinationDataEventListener) {
		
	}

	@Override
	public boolean supportsEvents() {
		return true;
	}
}
