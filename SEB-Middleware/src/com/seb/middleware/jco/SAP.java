package com.seb.middleware.jco;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.constant.PaymentType;
import com.seb.middleware.jco.data.AccountBalance;
import com.seb.middleware.jco.data.PaymentErrorMessage;
import com.seb.middleware.jco.data.PaymentHistory;
import com.seb.middleware.jco.process.BAPIAccountBalance;
import com.seb.middleware.jco.process.BAPIGetDetail;
import com.seb.middleware.jco.process.BAPIPaymentDataUpdate;
import com.seb.middleware.jco.process.BAPIPaymentHistory;
import com.seb.middleware.jpa.dao.PaymentReferenceDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceEntryDAO;

public class SAP {
	private static final Logger logger = LogManager.getLogger(SAP.class);

	private static final String DESTINATION = "abap_as";
	private int timeoutSeconds;
	
	public SAP(int timeoutSeconds){
		this.timeoutSeconds = timeoutSeconds;
	}

	public String getContractAccountName(String contractAccountNumber) throws Exception {
		
		logger.info("getContractAccountName() - " +  contractAccountNumber);
		
		BAPIGetDetail getDetail = new BAPIGetDetail();
    	getDetail.setDestination(DESTINATION);
    	getDetail.setContractAccountNumber(contractAccountNumber);
    	
		ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<String> future = executor.submit(getDetail);

        try {
        	return future.get(timeoutSeconds, TimeUnit.SECONDS);
        } catch (TimeoutException e) {
            future.cancel(true);
            logger.error("Timeout calling bapi", e);
            return null;
        } catch (Exception e) {
        	future.cancel(true);
        	throw e;
		} finally{
        	executor.shutdownNow();	
        	logger.info("getContractAccountName() ended");
        }   
	}
	
	public ArrayList<PaymentHistory> getPaymentHistory(String contractAccountNumber, int numberOfRecords) throws Exception {
		
		logger.info("getPaymentHistory() - " +  contractAccountNumber);
		
		BAPIPaymentHistory paymentHist = new BAPIPaymentHistory();
    	paymentHist.setDestination(DESTINATION);
    	paymentHist.setContractAccountNumber(contractAccountNumber);
    	paymentHist.setNumberOfRecords(numberOfRecords);
		
		ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<ArrayList<PaymentHistory>> future = executor.submit(paymentHist);

        try {
        	return future.get(timeoutSeconds, TimeUnit.SECONDS);
        } catch (TimeoutException e) {
            future.cancel(true);
            logger.error("Timeout calling bapi", e);
            return null;
        } catch (Exception e) {
        	future.cancel(true);
        	throw e;
		} finally{
        	executor.shutdownNow();	
        	logger.info("getPaymentHistory() ended");
        }   
	}
	
	public AccountBalance getAccountBalance(String contractAccountNumber) throws Exception {
		
		logger.info("getAccountBalance() - " +  contractAccountNumber);
		
		BAPIAccountBalance accountBalance = new BAPIAccountBalance();
		accountBalance.setDestination(DESTINATION);
		accountBalance.setContractAccountNumber(contractAccountNumber);
		
		ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<AccountBalance> future = executor.submit(accountBalance);

        try {
        	return future.get(timeoutSeconds, TimeUnit.SECONDS);
        } catch (TimeoutException e) {
            future.cancel(true);
            logger.error("Timeout calling bapi", e);
            return null;
        } catch (Exception e) {
        	future.cancel(true);
        	throw e;
		} finally{
        	executor.shutdownNow();	
        	logger.info("getAccountBalance() ended");
        }  
	}
	
	public PaymentErrorMessage updatePaymentStatus(PaymentReferenceDAO reference, String paymentReference, 
			PaymentReferenceEntryDAO entry, PaymentType paymentType, String fpxReconKey, String otherReconKey) throws Exception{
		
		logger.info("updatePaymentStatus() - " +  paymentReference);
		
		BAPIPaymentDataUpdate paymentUpdate = new BAPIPaymentDataUpdate(fpxReconKey, otherReconKey);
		
		paymentUpdate.setDestination(DESTINATION);
		paymentUpdate.setContractAccountNumber(entry.getContractAccountNumber());
		paymentUpdate.setAmount(entry.getAmount());
		paymentUpdate.setPaymentDate(reference.getPaymentDate());
		paymentUpdate.setReferenceNumber(paymentReference);
		paymentUpdate.setPaymentType(paymentType);
		
		ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<PaymentErrorMessage> future = executor.submit(paymentUpdate);

        try {
        	return future.get(timeoutSeconds, TimeUnit.SECONDS);
        } catch (TimeoutException e) {
            future.cancel(true);
            logger.error("Timeout calling bapi", e);
            return new PaymentErrorMessage("Timeout calling bapi");
        } catch (Exception e) {
        	future.cancel(true);
        	throw e;
		} finally{
        	executor.shutdownNow();	
        	logger.info("updatePaymentStatus() ended");
        }  
	}
}
