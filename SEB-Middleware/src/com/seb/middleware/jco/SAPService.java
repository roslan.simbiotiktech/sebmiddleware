package com.seb.middleware.jco;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sap.conn.jco.ext.Environment;
import com.seb.middleware.core.BaseService;
import com.seb.middleware.core.Main;

public class SAPService implements BaseService {

	private static final Logger logger = LogManager.getLogger(Main.class);
	private CustomDestinationDataProvider dataProvider = null;
	
	public SAPService(){
		dataProvider = new CustomDestinationDataProvider();
	}
	
	@Override
	public void startService() {
		Environment.registerDestinationDataProvider(dataProvider);
		logger.info("Registered jco provider");
	}

	@Override
	public void stopService() {
		Environment.unregisterDestinationDataProvider(dataProvider);
		logger.info("jco provider destroyed");
	}

}
