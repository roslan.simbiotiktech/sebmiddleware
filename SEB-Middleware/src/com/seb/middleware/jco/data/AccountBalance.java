package com.seb.middleware.jco.data;

import java.math.BigDecimal;

public class AccountBalance {

	private BigDecimal outstanding;
	
	private BigDecimal excessPayment;

	public BigDecimal getOutstanding() {
		return outstanding;
	}

	public void setOutstanding(BigDecimal outstanding) {
		this.outstanding = outstanding;
	}

	public BigDecimal getExcessPayment() {
		return excessPayment;
	}

	public void setExcessPayment(BigDecimal excessPayment) {
		this.excessPayment = excessPayment;
	}

	@Override
	public String toString() {
		return "AccountBalance [outstanding=" + outstanding + ", excessPayment=" + excessPayment + "]";
	}
}
