package com.seb.middleware.jco.data;

import java.util.ArrayList;

import com.seb.middleware.utility.StringUtil;

public class PartnerData {

	private String lastName;
	
	private String firstName;
	
	private String middleName;
	
	private String organizationName1;
	
	private String organizationName2;
	
	private String organizationName3;
	
	private String organizationName4;
	
	private String groupName1;
	
	private String groupName2;

	public String getContractAccountName(){
		ArrayList<String> nameParts = new ArrayList<String>();
		
		if(!StringUtil.isEmpty(lastName) || !StringUtil.isEmpty(firstName) || !StringUtil.isEmpty(middleName)){
			if(!StringUtil.isEmpty(lastName.trim())){
				nameParts.add(lastName.trim());
			}
			
			if(!StringUtil.isEmpty(firstName.trim())){
				nameParts.add(firstName.trim());
			}
			
			if(!StringUtil.isEmpty(middleName.trim())){
				nameParts.add(middleName.trim());
			}
		}else if(!StringUtil.isEmpty(organizationName1) 
				|| !StringUtil.isEmpty(organizationName2) 
				|| !StringUtil.isEmpty(organizationName3)
				|| !StringUtil.isEmpty(organizationName4)){
			if(!StringUtil.isEmpty(organizationName1.trim())){
				nameParts.add(organizationName1.trim());
			}
			
			if(!StringUtil.isEmpty(organizationName2.trim())){
				nameParts.add(organizationName2.trim());
			}
			
			if(!StringUtil.isEmpty(organizationName3.trim())){
				nameParts.add(organizationName3.trim());
			}
			
			if(!StringUtil.isEmpty(organizationName4.trim())){
				nameParts.add(organizationName4.trim());
			}
		}else if(!StringUtil.isEmpty(groupName1) || !StringUtil.isEmpty(groupName1)){
			if(!StringUtil.isEmpty(groupName1.trim())){
				nameParts.add(groupName1.trim());
			}
			
			if(!StringUtil.isEmpty(groupName1.trim())){
				nameParts.add(groupName1.trim());
			}
		}
		
		String name = mergeName(nameParts);
		if(!StringUtil.isEmpty(name)) name = name.toUpperCase();
		return name;
	}
	
	private String mergeName(ArrayList<String> nameParts){
		if(nameParts == null || nameParts.isEmpty()) return null;
		
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < nameParts.size(); i++){
			sb.append(nameParts.get(i));
			if((i + 1) < nameParts.size()){
				sb.append(" ");
			}
		}
		return sb.toString();
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getOrganizationName1() {
		return organizationName1;
	}

	public void setOrganizationName1(String organizationName1) {
		this.organizationName1 = organizationName1;
	}

	public String getOrganizationName2() {
		return organizationName2;
	}

	public void setOrganizationName2(String organizationName2) {
		this.organizationName2 = organizationName2;
	}

	public String getOrganizationName3() {
		return organizationName3;
	}

	public void setOrganizationName3(String organizationName3) {
		this.organizationName3 = organizationName3;
	}

	public String getOrganizationName4() {
		return organizationName4;
	}

	public void setOrganizationName4(String organizationName4) {
		this.organizationName4 = organizationName4;
	}

	public String getGroupName1() {
		return groupName1;
	}

	public void setGroupName1(String groupName1) {
		this.groupName1 = groupName1;
	}

	public String getGroupName2() {
		return groupName2;
	}

	public void setGroupName2(String groupName2) {
		this.groupName2 = groupName2;
	}

	@Override
	public String toString() {
		return "PartnerData [lastName=" + lastName + ", firstName=" + firstName + ", middleName=" + middleName + ", organizationName1="
				+ organizationName1 + ", organizationName2=" + organizationName2 + ", organizationName3=" + organizationName3
				+ ", organizationName4=" + organizationName4 + ", groupName1=" + groupName1 + ", groupName2=" + groupName2 + "]";
	}
}
