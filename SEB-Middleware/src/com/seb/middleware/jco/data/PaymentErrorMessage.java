package com.seb.middleware.jco.data;

import com.seb.middleware.utility.StringUtil;

public class PaymentErrorMessage {

	private String messageType;
	
	private String messageId;
	
	private String messageNumber;
	
	private String messageText;

	public PaymentErrorMessage(){
		
	}
	
	public PaymentErrorMessage(String messageText){
		this.messageText = messageText;
	}
	
	public boolean isError(){
		if(!StringUtil.isEmpty(messageNumber) && messageNumber.equals("217")){
			return false;
		}else{
			return true;
		}
	}
	
	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getMessageNumber() {
		return messageNumber;
	}

	public void setMessageNumber(String messageNumber) {
		this.messageNumber = messageNumber;
	}

	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	@Override
	public String toString() {
		return "PaymentErrorMessage [messageType=" + messageType + ", messageId=" + messageId + ", messageNumber=" + messageNumber + ", messageText="
				+ messageText + "]";
	}
}
