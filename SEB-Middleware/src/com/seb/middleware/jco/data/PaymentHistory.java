package com.seb.middleware.jco.data;

import java.util.Date;

public class PaymentHistory {

	private Date date;
	private double amout;
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public double getAmout() {
		return amout;
	}
	
	public void setAmout(double amout) {
		this.amout = amout;
	}

	@Override
	public String toString() {
		return "PaymentHistory [date=" + date + ", amout=" + amout + "]";
	}	
}
