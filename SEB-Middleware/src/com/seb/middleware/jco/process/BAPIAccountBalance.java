package com.seb.middleware.jco.process;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoTable;
import com.seb.middleware.jco.data.AccountBalance;

public class BAPIAccountBalance implements Callable<AccountBalance>{

	private static final Logger logger = LogManager.getLogger(BAPIAccountBalance.class);
	
	private String destination;
	private String contractAccountNumber;
	
	@Override
    public AccountBalance call() throws Exception {
		String bapiName = "ZBAPI_ISUACCNT_GETACCTBAL_CRM";
        JCoDestination dest = JCoDestinationManager.getDestination(destination);
        JCoFunction function = dest.getRepository().getFunction(bapiName);
        
        if(function == null)
            throw new RuntimeException(bapiName + " not found in SAP.");
        
        function.getImportParameterList().setValue("CONTRACT_ACCOUNT", contractAccountNumber);
        function.execute(dest);
        
        JCoTable table = function.getTableParameterList().getTable("ACCOUNT_BALANCES");
        
        HashMap<String, BigDecimal> map = new HashMap<String, BigDecimal>();
        
        logger.debug("AccountBalance for " + contractAccountNumber);
        
        for (int i = 0; i < table.getNumRows(); i++){
            table.setRow(i);
            
            try{
            	String description = table.getString("TEXT");
                BigDecimal amount = table.getBigDecimal("WITHD_VAL");
                
                logger.debug("TEXT: " + description + " | WITHD_VAL: " + amount);
                
                if(description != null && amount != null){
                	description = description.trim().toLowerCase();
                	map.put(description, amount);
                }	
            }catch(Exception e){
            	logger.error("error reading sap table - " + e.getMessage(), e);
            }
        }
        
        AccountBalance balance = new AccountBalance();
        
        BigDecimal openAmount = map.get("open");
        BigDecimal creditAmount = map.get("credit");
        
        logger.debug("Open Amount: " + openAmount);
        logger.debug("Credit Amount: " + creditAmount);
        
        BigDecimal bal = getOrDefault(openAmount, BigDecimal.ZERO).subtract(getOrDefault(creditAmount, BigDecimal.ZERO));
        
        if(bal.compareTo(BigDecimal.ZERO) >= 0){
        	balance.setExcessPayment(BigDecimal.ZERO);
        	balance.setOutstanding(bal);
        }else{
        	balance.setExcessPayment(bal.negate());
        	balance.setOutstanding(BigDecimal.ZERO);
        }
        /*
        
        if(!isEmptyCredit(openAmount) && !isEmptyCredit(creditAmount)){
        	*//**
        	 * both not empty
        	 *//*
        	balance.setOutstanding(openAmount);
        	balance.setExcessPayment(creditAmount);
        }else if(isEmptyCredit(openAmount) && isEmptyCredit(creditAmount)){
        	*//**
        	 * both empty
        	 *//*
        	balance.setOutstanding(zero);
        	balance.setExcessPayment(zero);
        }else{
        	*//**
        	 * either one is empty
        	 *//*
        	if(!isEmptyCredit(openAmount)){
        		balance.setOutstanding(openAmount);
        		balance.setExcessPayment(zero);
        	}else{
        		balance.setOutstanding(zero);
        		balance.setExcessPayment(creditAmount);
        	}
        }*/
        
        logger.debug(balance);
        
        return balance;
    }
	
	private BigDecimal getOrDefault(BigDecimal value, BigDecimal def){
		if(value == null) return def;
		else return value;
	}
	
	/*private boolean isEmptyCredit(BigDecimal value){
		BigDecimal zero = BigDecimal.valueOf(0).setScale(2);
		
		if(value == null || value.compareTo(zero) == 0){
			return true;
		}else{
			return false;
		}
	}*/

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getContractAccountNumber() {
		return contractAccountNumber;
	}

	public void setContractAccountNumber(String contractAccountNumber) {
		this.contractAccountNumber = contractAccountNumber;
	}
}
