package com.seb.middleware.jco.process;

import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoParameterList;
import com.sap.conn.jco.JCoStructure;
import com.seb.middleware.jco.data.PartnerData;

public class BAPIGetDetail implements Callable<String>{

	private static final Logger logger = LogManager.getLogger(BAPIGetDetail.class);
	
	private String destination;
	private String contractAccountNumber;
	
	@Override
    public String call() throws Exception {
		String bapiName = "ZBAPI_ISUPARTNER_GETDETAIL";
        JCoDestination dest = JCoDestinationManager.getDestination(destination);
        JCoFunction function = dest.getRepository().getFunction(bapiName);
        
        if(function == null)
            throw new RuntimeException(bapiName + " not found in SAP.");
        
        function.getImportParameterList().setValue("CONTRACTACCOUNT", contractAccountNumber);
        function.execute(dest);
        
        JCoParameterList exportParameters = function.getExportParameterList();
        if(exportParameters == null){
        	logger.warn("export parameter is null for contract: " + contractAccountNumber);
        	return null;
        }else{
        	PartnerData pd = new PartnerData();
        	
        	JCoStructure structure = exportParameters.getStructure("PARTNERDATA");
        	for(int i = 0; i < structure.getMetaData().getFieldCount(); i++){
        		String name = structure.getMetaData().getName(i);
        		String value = structure.getString(i);
        		
        		if(name.equalsIgnoreCase("NAME_LAST")){
        			pd.setLastName(value);
        		}else if(name.equalsIgnoreCase("NAME_FIRST")){
        			pd.setFirstName(value);
        		}else if(name.equalsIgnoreCase("NAMEMIDDLE")){
        			pd.setMiddleName(value);
        		}else if(name.equalsIgnoreCase("NAME_ORG1")){
        			pd.setOrganizationName1(value);
        		}else if(name.equalsIgnoreCase("NAME_ORG2")){
        			pd.setOrganizationName2(value);
        		}else if(name.equalsIgnoreCase("NAME_ORG3")){
        			pd.setOrganizationName3(value);
        		}else if(name.equalsIgnoreCase("NAME_ORG4")){
        			pd.setOrganizationName4(value);
        		}else if(name.equalsIgnoreCase("NAME_GRP1")){
        			pd.setGroupName1(value);
        		}else if(name.equalsIgnoreCase("NAME_GRP2")){
        			pd.setGroupName2(value);
        		}
        	}
        	
        	logger.debug(pd);
        	
        	return pd.getContractAccountName();
        }
    }

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getContractAccountNumber() {
		return contractAccountNumber;
	}

	public void setContractAccountNumber(String contractAccountNumber) {
		this.contractAccountNumber = contractAccountNumber;
	}
}
