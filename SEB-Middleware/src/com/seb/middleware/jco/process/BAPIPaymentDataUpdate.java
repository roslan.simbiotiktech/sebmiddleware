package com.seb.middleware.jco.process;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoParameterList;
import com.sap.conn.jco.JCoStructure;
import com.seb.middleware.constant.PaymentType;
import com.seb.middleware.jco.data.PaymentErrorMessage;
import com.seb.middleware.utility.StringUtil;

public class BAPIPaymentDataUpdate implements Callable<PaymentErrorMessage>{

	private static final Logger logger = LogManager.getLogger(BAPIPaymentDataUpdate.class);
	
	private String destination;
	
	private PaymentType paymentType;
	private Date paymentDate;
	private String contractAccountNumber;
	private String referenceNumber;
	private BigDecimal amount;
	
	private String fpxReconKey;
	private String otherReconKey;
	
	public BAPIPaymentDataUpdate(String fpxReconKey, String otherReconKey){
		this.fpxReconKey = fpxReconKey;
		this.otherReconKey = otherReconKey;
	}
	
	@Override
    public PaymentErrorMessage call() throws Exception {
		String bapiName = "ZBAPI_ISUACCNT_PYMTUPD_SEBCARE";
        JCoDestination dest = JCoDestinationManager.getDestination(destination);
        JCoFunction function = dest.getRepository().getFunction(bapiName);
        
        if(function == null)
            throw new RuntimeException(bapiName + " not found in SAP.");
        
        String reconKey = getReconKey();
        String paymentAmount = String.valueOf(amount.doubleValue());
        
        logger.info("RECONKEY = " + reconKey);
        logger.info("DOCDATE = " + paymentDate);
        logger.info("CONTRACTACCT = " + contractAccountNumber);
        logger.info("REFNO = " + referenceNumber);
        logger.info("PAYMENTAMT = " + paymentAmount);
        
        JCoParameterList parameterList = function.getImportParameterList();
        parameterList.setValue("RECONKEY", reconKey);
        parameterList.setValue("DOCDATE", paymentDate);
        parameterList.setValue("CONTRACTACCT", contractAccountNumber);
        parameterList.setValue("REFNO", referenceNumber);
        parameterList.setValue("PAYMENTAMT", paymentAmount);
        
        function.execute(dest);
        
        JCoParameterList exportParameters = function.getExportParameterList();
        if(exportParameters == null){
        	logger.warn("export parameter is null for contract: " + contractAccountNumber);
        	return null;
        }else{
        	
        	JCoStructure structure = exportParameters.getStructure("ERR_MSG");
        	if(structure != null && structure.getMetaData() != null 
        			&& structure.getMetaData().getFieldCount() > 0){
        		PaymentErrorMessage er = new PaymentErrorMessage();
        		
        		for(int i = 0; i < structure.getMetaData().getFieldCount(); i++){
            		String name = structure.getMetaData().getName(i);
            		String value = structure.getString(i);
            		
            		if(!StringUtil.isEmpty(name) && !StringUtil.isEmpty(value)){
            			if(name.equalsIgnoreCase("MSGTY")){
                			er.setMessageType(value);
                		}else if(name.equalsIgnoreCase("MSGID")){
                			er.setMessageId(value);
                		}else if(name.equalsIgnoreCase("MSGNO")){
                			er.setMessageNumber(value);
                		}else if(name.equalsIgnoreCase("MSGTX")){
                			er.setMessageText(value);
                		}	
            		}
            	}
        		
        		logger.debug(er);
        		
        		if(er.isError()){
        			logger.error("ERROR updating payment status to SAP BAPI");
            		return er;	
        		}else{
        			logger.info("Payment update successful to SAP BAPI");
            		return null;
        		}
        	}else{
        		logger.info("Payment update successful to SAP BAPI");
        		return null;
        	}
        }
    }
	
	private String getReconKey(){
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyy");
		String reconKey = "10123" + sdf.format(new Date());
		
		if(paymentType == PaymentType.FPX){
			reconKey += fpxReconKey;
		}else{
			reconKey += otherReconKey;
		}
		
		return reconKey;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getContractAccountNumber() {
		return contractAccountNumber;
	}

	public void setContractAccountNumber(String contractAccountNumber) {
		this.contractAccountNumber = contractAccountNumber;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
