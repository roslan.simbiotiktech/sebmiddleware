package com.seb.middleware.jco.process;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoTable;
import com.seb.middleware.jco.data.PaymentHistory;

public class BAPIPaymentHistory implements Callable<ArrayList<PaymentHistory>>{

	private static final Logger logger = LogManager.getLogger(BAPIPaymentHistory.class);
	
	private String destination;
	private int numberOfRecords;
	private String contractAccountNumber;
	
	@Override
    public ArrayList<PaymentHistory> call() throws Exception {
		String bapiName = "ZBAPI_ISUACCNT_HLIST_CMA";
        JCoDestination dest = JCoDestinationManager.getDestination(destination);
        JCoFunction function = dest.getRepository().getFunction(bapiName);
        
        if(function == null)
            throw new RuntimeException(bapiName + " not found in SAP.");
        
        function.getImportParameterList().setValue("X_VKONT", contractAccountNumber);
        function.execute(dest);
        
        JCoTable table = function.getTableParameterList().getTable("T_AUGTAB");
        
        ArrayList<String> paymentRows = new ArrayList<String>();
        paymentRows.add("incoming payment");
        paymentRows.add("pmnt on acct");
        paymentRows.add("payment on account");
        
        HashMap<String, PaymentHistory> paymentDetails = new HashMap<String, PaymentHistory>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        for (int i = 0; i < table.getNumRows(); i++){
            table.setRow(i);
            String description = table.getString("VOTXT").trim().toLowerCase();
            String documentDate = table.getString("BLDAT"); // yyyy-MM-dd
            double amount = table.getDouble("BETRH");
            
            logger.debug("VOTXT: " + description + " | BLDAT: " + documentDate + " | BETRH: " + amount);
            
            // Only process wanted rows
            if(paymentRows.contains(description) && (documentDate != null && !documentDate.isEmpty())){
            	
				try {
					Date docDate = sdf.parse(documentDate);
					
            		PaymentHistory accumulated = paymentDetails.get(documentDate);
                	if(accumulated == null){
                		accumulated = new PaymentHistory();
                		accumulated.setDate(docDate);
                		paymentDetails.put(documentDate, accumulated);
                	}
                	
                	accumulated.setAmout(accumulated.getAmout() + amount);
				} catch (ParseException e) {
					logger.error("error parsing sap date " + documentDate);
				}
            }
        }
        
        Set<String> keyset = paymentDetails.keySet();
        List<String> sorted = asSortedList(keyset);

        int i = 0;
        ArrayList<PaymentHistory> histories = new ArrayList<PaymentHistory>();
        for(String date : sorted){
        	if(i >= numberOfRecords) break;
        	
        	PaymentHistory paymentHistory = paymentDetails.get(date);
        	histories.add(paymentHistory);
        	i++;
        }
        
        return histories;
    }

	private List<String> asSortedList(Collection<String> c) {
		List<String> list = new ArrayList<String>(c);
		Collections.sort(list, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return o2.compareTo(o1);
			}
		});
		
		return list;
	}
	
	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getContractAccountNumber() {
		return contractAccountNumber;
	}

	public void setContractAccountNumber(String contractAccountNumber) {
		this.contractAccountNumber = contractAccountNumber;
	}

	public int getNumberOfRecords() {
		return numberOfRecords;
	}

	public void setNumberOfRecords(int numberOfRecords) {
		this.numberOfRecords = numberOfRecords;
	}
}
