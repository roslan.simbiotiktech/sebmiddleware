package com.seb.middleware.jpa;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DatabaseFacade {

	private int timeout = 60; // Seconds 
	private Session session;
	private Transaction transaction;
	
	public DatabaseFacade(){
		session = JPA.getInstance().openSession();
	}
	
	public boolean isTransactionBegan(){
		return transaction != null;
	}
	
	public void beginTransaction() throws DatabaseFacadeException {
		if(transaction == null){
			transaction = session.beginTransaction();
			transaction.setTimeout(timeout);
		}else{
			throw new DatabaseFacadeException("Transaction already began.");
		}
	}
	
	public void commit() throws DatabaseFacadeException {
		if(transaction != null){
			if(transaction.isActive()){
				if(transaction.wasCommitted()){
					throw new DatabaseFacadeException("Transaction was already been committed.");
				}else if(transaction.wasRolledBack()){
					throw new DatabaseFacadeException("Transaction was already been rollback.");
				}else{
					transaction.commit();
					transaction = null;
				}
			}else{
				throw new DatabaseFacadeException("Transaction is not alived.");
			}
		}else{
			throw new DatabaseFacadeException("Transaction has not been initialized.");
		}
	}
	
	public void rollback() throws DatabaseFacadeException {
		if(transaction != null){
			if(transaction.isActive()){
				if(transaction.wasCommitted()){
					throw new DatabaseFacadeException("Transaction was already been committed.");
				}else if(transaction.wasRolledBack()){
					throw new DatabaseFacadeException("Transaction was already been rollback.");
				}else{
					transaction.rollback();
				}
			}else{
				throw new DatabaseFacadeException("Transaction is not alived.");
			}
		}else{
			throw new DatabaseFacadeException("Transaction has not been initialized.");
		}
	}

	public <T> T insert(T t) throws DatabaseFacadeException {
		if(transaction != null){
			if(transaction.wasRolledBack()){
				throw new DatabaseFacadeException("Transaction was already been rollback.");
			}else if(transaction.wasCommitted()){
				throw new DatabaseFacadeException("Transaction was already been committed.");
			}else{
				session.save(t);
				return t;	
			}
		}else{
			throw new DatabaseFacadeException("Transaction has not been initialized.");
		}
	}
	
	public <T> T update(T t) throws DatabaseFacadeException {
		if(transaction != null){
			if(transaction.wasRolledBack()){
				throw new DatabaseFacadeException("Transaction was already been rollback.");
			}else if(transaction.wasCommitted()){
				throw new DatabaseFacadeException("Transaction was already been committed.");
			}else{
				session.save(t);
				return t;	
			}
		}else{
			throw new DatabaseFacadeException("Transaction has not been initialized.");
		}
	}
	
	public <T> T insertOrUpdate(T t) throws DatabaseFacadeException {
		if(transaction != null){
			if(transaction.wasRolledBack()){
				throw new DatabaseFacadeException("Transaction was already been rollback.");
			}else if(transaction.wasCommitted()){
				throw new DatabaseFacadeException("Transaction was already been committed.");
			}else{
				session.saveOrUpdate(t);
				return t;	
			}
		}else{
			throw new DatabaseFacadeException("Transaction has not been initialized.");
		}
	}
	
	public <T> void delete(T t) throws DatabaseFacadeException {
		if(transaction != null){
			if(transaction.wasRolledBack()){
				throw new DatabaseFacadeException("Transaction was already been rollback.");
			}else if(transaction.wasCommitted()){
				throw new DatabaseFacadeException("Transaction was already been committed.");
			}else{
				session.delete(t);
			}
		}else{
			throw new DatabaseFacadeException("Transaction has not been initialized.");
		}
	}
	
	public void flush(){
		session.flush();
	}
	
	public Query createQuery(String hql){
		return session.createQuery(hql);
	}
	
	public boolean wasCommitted(){
		return transaction.wasCommitted();
	}
	
	public boolean wasRolledBack(){
		return transaction.wasRolledBack();
	}
	
	@SuppressWarnings("unchecked")
	public <T> ArrayList<T> query(Class<T> cls){
		Criteria criteria = session.createCriteria(cls);
		return (ArrayList<T>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public <T> ArrayList<T> query(Query query){
		return (ArrayList<T>) query.list();
	}
	
	@SuppressWarnings("unchecked")
	public <T> ArrayList<T> query(String arg0){
		Criteria criteria = session.createCriteria(arg0);
		return (ArrayList<T>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public <T> ArrayList<T> query(Class<T> cls, String arg0){
		Criteria criteria = session.createCriteria(cls, arg0);
		return (ArrayList<T>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	public <T> ArrayList<T> query(String arg0, String arg1){
		Criteria criteria = session.createCriteria(arg0, arg1);
		return (ArrayList<T>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public <T> ArrayList<T> query(Criteria criteria){
		return (ArrayList<T>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public <T> T queryUnique(Criteria criteria){
		return (T) criteria.uniqueResult();
	}
	
	public Session getSession(){
		return session;
	}
	
	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public void close(){
		if(this.session != null && this.session.isOpen()){
			this.session.close();
		}
	}
}
