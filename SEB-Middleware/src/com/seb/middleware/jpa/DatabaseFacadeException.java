package com.seb.middleware.jpa;

public class DatabaseFacadeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4324054797494898234L;

	public DatabaseFacadeException(String errorMsg){
		super(errorMsg);
	}
}
