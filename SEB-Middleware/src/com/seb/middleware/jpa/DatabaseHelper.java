package com.seb.middleware.jpa;

import java.util.ArrayList;

import org.hibernate.Criteria;

import com.seb.middleware.api.services.request.Paging;
import com.seb.middleware.api.services.response.Pagination;



public class DatabaseHelper {

	protected DatabaseFacade db;
	
	public DatabaseHelper(DatabaseFacade db){
		this.db = db;
	}
	
	/**
	 * Assume the named query has the Sorting attribute
	 */
	protected <T> ArrayList<T> query(Criteria criteria, Pagination paging){
		criteria.setFirstResult(getFirstResult(paging.getPaging()));
		criteria.setMaxResults(getMaxResult(paging.getPaging()));
		return db.query(criteria);
	}
	
	protected int getFirstResult(Paging paging){
		return (paging.getPage() -1) * paging.getRecordsPerPage();
	}
	
	protected int getMaxResult(Paging paging){
		return paging.getRecordsPerPage();
	}
}
