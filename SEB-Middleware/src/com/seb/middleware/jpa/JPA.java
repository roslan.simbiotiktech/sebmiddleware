package com.seb.middleware.jpa;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.seb.middleware.core.BaseService;

public class JPA implements BaseService {

	private static final Logger logger = LogManager.getLogger(JPA.class);
	private static SessionFactory sessionFactory;
	
	//private static final String CONFIG_FILE = "hibernate.cfg.xml";
	
	@Override
	public void startService() {
		logger.info("Starting JPA Service.");
		Configuration configuration = new Configuration(); 
    	configuration.configure();

    	StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder()
    			.applySettings(configuration.getProperties());

    	ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();

    	sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    	logger.info("JPA Service Started.");
	}

	@Override
	public void stopService() {
		logger.info("Stopping JPA Service.");
		sessionFactory.close();
		logger.info("JPA Service Stopped.");
	}

	public static SessionFactory getInstance(){
		return sessionFactory;
	}
	
}
