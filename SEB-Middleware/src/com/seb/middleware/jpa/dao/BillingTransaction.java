package com.seb.middleware.jpa.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seb.billing_transaction")
public class BillingTransaction implements Serializable {

	private static final long serialVersionUID = -5813811229559747851L;

	@Id
	@Column(name="billing_id")
	private long bilingId;
	
	@Id
	@Column(name="invoice_number")
	private String invoiceNumber;
	
	@Column(name="contract_account_number")
	private String contractAccountNumber;
	
	@Column(name="business_partner_number")
	private String businessPartnerNumber;
	
	@Column(name="invoice_date")
	private Date invoiceDate;
	
	@Column(name="due_date")
	private Date dueDate;
	
	@Column(name="current_amount")
	private BigDecimal currentAmount;
	
	@Column(name="current_total_amount")
	private BigDecimal currentTotalAmount;
	
	@Column(name="bill_start_date")
	private Date billStartDate;
	
	@Column(name="bill_end_date")
	private Date billEndDate;
	
	@Column(name="pdf_print_document_number")
	private String pdfPrintDocumentNumber;

	@Column(name="created_datetime")
	private Date createdDate;
	
	public long getBilingId() {
		return bilingId;
	}

	public void setBilingId(long bilingId) {
		this.bilingId = bilingId;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getContractAccountNumber() {
		return contractAccountNumber;
	}

	public void setContractAccountNumber(String contractAccountNumber) {
		this.contractAccountNumber = contractAccountNumber;
	}

	public String getBusinessPartnerNumber() {
		return businessPartnerNumber;
	}

	public void setBusinessPartnerNumber(String businessPartnerNumber) {
		this.businessPartnerNumber = businessPartnerNumber;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public BigDecimal getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(BigDecimal currentAmount) {
		this.currentAmount = currentAmount;
	}

	public BigDecimal getCurrentTotalAmount() {
		return currentTotalAmount;
	}

	public void setCurrentTotalAmount(BigDecimal currentTotalAmount) {
		this.currentTotalAmount = currentTotalAmount;
	}

	public Date getBillStartDate() {
		return billStartDate;
	}

	public void setBillStartDate(Date billStartDate) {
		this.billStartDate = billStartDate;
	}

	public Date getBillEndDate() {
		return billEndDate;
	}

	public void setBillEndDate(Date billEndDate) {
		this.billEndDate = billEndDate;
	}

	public String getPdfPrintDocumentNumber() {
		return pdfPrintDocumentNumber;
	}

	public void setPdfPrintDocumentNumber(String pdfPrintDocumentNumber) {
		this.pdfPrintDocumentNumber = pdfPrintDocumentNumber;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return "BillingTransaction [bilingId=" + bilingId + ", invoiceNumber=" + invoiceNumber + ", contractAccountNumber=" + contractAccountNumber
				+ ", businessPartnerNumber=" + businessPartnerNumber + ", invoiceDate=" + invoiceDate + ", dueDate=" + dueDate + ", currentAmount="
				+ currentAmount + ", currentTotalAmount=" + currentTotalAmount + ", billStartDate=" + billStartDate + ", billEndDate=" + billEndDate
				+ ", pdfPrintDocumentNumber=" + pdfPrintDocumentNumber + ", createdDate=" + createdDate + "]";
	}

	@Override
	public boolean equals(Object obj){
		if(obj == null) return false;
		
		if(obj instanceof BillingTransaction){
			BillingTransaction bt = (BillingTransaction) obj;
			if(bt.getBilingId() == getBilingId()){
				
				if(bt.getInvoiceNumber() != null && getInvoiceNumber() != null){
					if(bt.getInvoiceNumber().equals(getInvoiceNumber())){
						return true;
					}
				}else if(bt.getInvoiceNumber() == null && getInvoiceNumber() == null){
					return true;
				}
			}
		}
		
		return false;
	}
}
