package com.seb.middleware.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.seb.middleware.constant.CRMReportStatus;
import com.seb.middleware.constant.NewTransStatus;
import com.seb.middleware.constant.ReportRecordType;
import com.seb.middleware.constant.ReportType;

@Entity
@Table(name = "seb.combined_report")
public class CombinedReport {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="trans_id")
	private long transId;
	
	@Column(name="case_number")
	private String caseNumber;
	
	@Column(name="user_email")
	private String userEmail;
	
	@Enumerated(EnumType.STRING)
	@Column(name="report_type")
	private ReportType reportType;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private CRMReportStatus status;
	
	@Enumerated(EnumType.STRING)
	@Column(name="seb_status")
	private NewTransStatus sebStatus;
	
	@Column(name="remark")
	private String remark;
	
	@Column(name="loc_longitude")
	private String longitude;
	
	@Column(name="loc_latitude")
	private String latitude;
	
	@Column(name="loc_address")
	private String address;
	
	@Column(name="updated_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.ALWAYS)
	private Date updatedDatetime;
	
	@Column(name="created_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;
	
	@Column(name="deleted_flag")
	private int deletedFlag;
	
	@Enumerated(EnumType.STRING)
	@Column(name="report_record_type")
	private ReportRecordType reportRecordType;

	public long getTransId() {
		return transId;
	}

	public void setTransId(long transId) {
		this.transId = transId;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public ReportType getReportType() {
		return reportType;
	}

	public void setReportType(ReportType reportType) {
		this.reportType = reportType;
	}

	public CRMReportStatus getStatus() {
		return status;
	}

	public void setStatus(CRMReportStatus status) {
		this.status = status;
	}

	public NewTransStatus getSebStatus() {
		return sebStatus;
	}

	public void setSebStatus(NewTransStatus sebStatus) {
		this.sebStatus = sebStatus;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public ReportRecordType getReportRecordType() {
		return reportRecordType;
	}

	public void setReportRecordType(ReportRecordType reportRecordType) {
		this.reportRecordType = reportRecordType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CombinedReport [transId=").append(transId).append(", caseNumber=").append(caseNumber)
				.append(", userEmail=").append(userEmail).append(", reportType=").append(reportType).append(", status=")
				.append(status).append(", sebStatus=").append(sebStatus).append(", remark=").append(remark)
				.append(", longitude=").append(longitude).append(", latitude=").append(latitude).append(", address=")
				.append(address).append(", updatedDatetime=").append(updatedDatetime).append(", createdDatetime=")
				.append(createdDatetime).append(", deletedFlag=").append(deletedFlag).append(", reportRecordType=")
				.append(reportRecordType).append("]");
		return builder.toString();
	}
}
