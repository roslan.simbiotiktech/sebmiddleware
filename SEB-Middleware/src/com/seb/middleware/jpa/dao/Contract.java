package com.seb.middleware.jpa.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seb.contract")
public class Contract {
	
	@Id
	@Column(name="contract_account_number")
	private String contractAccountNumber;
	
	@Column(name="contract_account_name")
	private String contractAccountName;

	@Column(name="agency_name")
	private String agencyName;
	
	@Column(name="sfdc_id")
	private String sfdcId;

	public String getContractAccountNumber() {
		return contractAccountNumber;
	}

	public void setContractAccountNumber(String contractAccountNumber) {
		this.contractAccountNumber = contractAccountNumber;
	}

	public String getContractAccountName() {
		return contractAccountName;
	}

	public void setContractAccountName(String contractAccountName) {
		this.contractAccountName = contractAccountName;
	}

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public String getSfdcId() {
		return sfdcId;
	}

	public void setSfdcId(String sfdcId) {
		this.sfdcId = sfdcId;
	}

	@Override
	public String toString() {
		return "Contract [contractAccountNumber=" + contractAccountNumber + ", contractAccountName="
				+ contractAccountName + ", agencyName=" + agencyName + ", sfdcId=" + sfdcId + "]";
	}
}
