package com.seb.middleware.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seb.CRM_USER_PROFILE")
public class CrmUserProfile {

	@Id
	@Column(name="sfdc_id")
	private String sfdcId;
	
	@Column(name="Email_1")
	private String email_1;
	
	@Column(name="Email_2")
	private String email_2;
	
	@Column(name="Email_3")
	private String email_3;
	
	@Column(name="Mobile_1")
	private String mobile_1;
	
	@Column(name="Mobile_2")
	private String mobile_2;
	
	@Column(name="Mobile_3")
	private String mobile_3;	
	
	@Column(name="sfdc_updated_datetime1")
	private Date sfdcUpdatedDate1;
	
	@Column(name="sfdc_updated_datetime2")
	private Date sfdcUpdatedDate2;
	
	@Column(name="sfdc_updated_datetime3")
	private Date sfdcUpdatedDate3;

	public String getEmail_1() {
		return email_1;
	}

	public void setEmail_1(String email_1) {
		this.email_1 = email_1;
	}

	public String getEmail_2() {
		return email_2;
	}

	public void setEmail_2(String email_2) {
		this.email_2 = email_2;
	}

	public String getEmail_3() {
		return email_3;
	}

	public void setEmail_3(String email_3) {
		this.email_3 = email_3;
	}

	public String getMobile_1() {
		return mobile_1;
	}

	public void setMobile_1(String mobile_1) {
		this.mobile_1 = mobile_1;
	}

	public String getMobile_2() {
		return mobile_2;
	}

	public void setMobile_2(String mobile_2) {
		this.mobile_2 = mobile_2;
	}

	public String getMobile_3() {
		return mobile_3;
	}

	public void setMobile_3(String mobile_3) {
		this.mobile_3 = mobile_3;
	}

	public String getSfdcId() {
		return sfdcId;
	}

	public void setSfdcId(String sfdcId) {
		this.sfdcId = sfdcId;
	}

	public Date getSfdcUpdatedDate1() {
		return sfdcUpdatedDate1;
	}

	public void setSfdcUpdatedDate1(Date sfdcUpdatedDate1) {
		this.sfdcUpdatedDate1 = sfdcUpdatedDate1;
	}

	public Date getSfdcUpdatedDate2() {
		return sfdcUpdatedDate2;
	}

	public void setSfdcUpdatedDate2(Date sfdcUpdatedDate2) {
		this.sfdcUpdatedDate2 = sfdcUpdatedDate2;
	}

	public Date getSfdcUpdatedDate3() {
		return sfdcUpdatedDate3;
	}

	public void setSfdcUpdatedDate3(Date sfdcUpdatedDate3) {
		this.sfdcUpdatedDate3 = sfdcUpdatedDate3;
	}

	@Override
	public String toString() {
		return "CrmUserProfile [email_1=" + email_1 + ", email_2=" + email_2 + ", email_3=" + email_3 + ", mobile_1="
				+ mobile_1 + ", mobile_2=" + mobile_2 + ", mobile_3=" + mobile_3 + ", sfdcId=" + sfdcId
				+ ", sfdcUpdatedDate1=" + sfdcUpdatedDate1 + ", sfdcUpdatedDate2=" + sfdcUpdatedDate2
				+ ", sfdcUpdatedDate3=" + sfdcUpdatedDate3 + "]";
	}
	
}
