package com.seb.middleware.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "seb.cust_service_counter")
public class CustServiceCounter {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	
	@Column(name="cust_service_location_id")
	private int custServiceLocationId;
	
	@Column(name="admin_user_id")
	private int adminUserId;
	
	@Column(name="address_line1")
	private String addressLine1;
	
	@Column(name="address_line2")
	private String addressLine2;
	
	@Column(name="address_line3")
	private String addressLine3;
	
	@Column(name="postcode")
	private String postCode;
	
	@Column(name="city")
	private String city;
	
	@Column(name="opening_hour")
	private String openingHour;
	
	@Column(name="latitude")
	private String latitude;
	
	@Column(name="longitude")
	private String longitude;
	
	@Column(name="remark")
	private String remark;
	
	@Column(name="status")
	private String status;
	
	@Column(name="approver_id")
	private int approverId;
	
	@Column(name="reject_reason")
	private String rejectReason;
	
	@Column(name="updated_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.ALWAYS)
	private Date updatedDatetime;
	
	@Column(name="created_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;
	
	@Column(name="deleted_flag")
	private int deletedFlag;

	@OneToOne()
    @JoinColumn(name = "cust_service_location_id", insertable = false, updatable = false)
	private CustServiceLocation custServiceLocation;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getCustServiceLocationId() {
		return custServiceLocationId;
	}

	public void setCustServiceLocationId(int custServiceLocationId) {
		this.custServiceLocationId = custServiceLocationId;
	}

	public int getAdminUserId() {
		return adminUserId;
	}

	public void setAdminUserId(int adminUserId) {
		this.adminUserId = adminUserId;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getOpeningHour() {
		return openingHour;
	}

	public void setOpeningHour(String openingHour) {
		this.openingHour = openingHour;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getApproverId() {
		return approverId;
	}

	public void setApproverId(int approverId) {
		this.approverId = approverId;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public Date getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public CustServiceLocation getCustServiceLocation() {
		return custServiceLocation;
	}

	public void setCustServiceLocation(CustServiceLocation custServiceLocation) {
		this.custServiceLocation = custServiceLocation;
	}

	@Override
	public String toString() {
		return "CustServiceCounter [id=" + id + ", custServiceLocationId="
				+ custServiceLocationId + ", adminUserId=" + adminUserId
				+ ", addressLine1=" + addressLine1 + ", addressLine2="
				+ addressLine2 + ", addressLine3=" + addressLine3
				+ ", postCode=" + postCode + ", city=" + city
				+ ", openingHour=" + openingHour + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", remark=" + remark
				+ ", status=" + status + ", approverId=" + approverId
				+ ", rejectReason=" + rejectReason + ", updatedDatetime="
				+ updatedDatetime + ", createdDatetime=" + createdDatetime
				+ ", deletedFlag=" + deletedFlag + ", custServiceLocation="
				+ custServiceLocation + "]";
	}
}
