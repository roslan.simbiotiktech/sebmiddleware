package com.seb.middleware.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "seb.customer_audit_trail")
public class CustomerAuditTrail {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="audit_id")
	private long auditId;
	
	@Column(name="user_id")
	private String userId;
	
	@Column(name="activity")
	private String activity;
	
	@Column(name="success")
	private int success;
	
	@Column(name="audit_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.INSERT)
	private Date auditDatetime;

	public long getAuditId() {
		return auditId;
	}

	public void setAuditId(long auditId) {
		this.auditId = auditId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public int isSuccess() {
		return success;
	}

	public void setSuccess(int success) {
		this.success = success;
	}

	public Date getAuditDatetime() {
		return auditDatetime;
	}

	public void setAuditDatetime(Date auditDatetime) {
		this.auditDatetime = auditDatetime;
	}

	@Override
	public String toString() {
		return "CustomerAuditTrail [auditId=" + auditId + ", userId=" + userId + ", activity=" + activity + ", success=" + success
				+ ", auditDatetime=" + auditDatetime + "]";
	}
}
