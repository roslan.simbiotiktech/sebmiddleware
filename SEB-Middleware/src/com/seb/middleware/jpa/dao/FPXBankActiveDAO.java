package com.seb.middleware.jpa.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seb.fpx_bank_active_list")
public class FPXBankActiveDAO {
	
	@Id
	@Column(name="bank_id")
	private String bankId;
	
	@Column(name="status")
	private String status;

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "FPXBankActiveDAO [bankId=" + bankId + ", status=" + status + "]";
	}
}
