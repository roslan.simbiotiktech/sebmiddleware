package com.seb.middleware.jpa.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.seb.middleware.constant.FPXBankType;

@Entity
@Table(name = "seb.fpx_bank_view")
public class FPXBankDAO {
	
	@Id
	@Column(name="bank_id")
	private String bankId;
	
	@Column(name="description")
	private String description;

	@Column(name="image_path")
	private String imagePath;
	
	@Enumerated(EnumType.STRING)
	@Column(name="bank_type")
	private FPXBankType bankType;

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public FPXBankType getBankType() {
		return bankType;
	}

	public void setBankType(FPXBankType bankType) {
		this.bankType = bankType;
	}

	@Override
	public String toString() {
		return "FPXBankDAO [bankId=" + bankId + ", description=" + description + ", imagePath=" + imagePath + ", bankType=" + bankType + "]";
	}
}
