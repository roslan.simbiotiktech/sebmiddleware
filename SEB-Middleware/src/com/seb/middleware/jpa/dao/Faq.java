package com.seb.middleware.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.seb.middleware.constant.FaqStatus;

@Entity
@Table(name = "seb.faq")
public class Faq {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="faq_category_id")
	private int faqCategoryId;
	
	@Column(name="question_en")
	private String questionEn;
	
	@Column(name="answer_en")
	private String answerEn;
	
	@Column(name="question_bm")
	private String questionBm;
	
	@Column(name="answer_bm")
	private String answerBm;
	
	@Column(name="question_cn")
	private String questionCn;
	
	@Column(name="answer_cn")
	private String answerCn;
	
	@Column(name="deleted_flag")
	private int deletedFlag;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private FaqStatus status;
	
	@Column(name="faq_sequence")
	private int faqSequence;
	
	@Column(name="updated_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.ALWAYS)
	private Date updatedDatetime;
	
	@Column(name="created_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;

	@OneToOne()
    @JoinColumn(name = "faq_category_id", insertable = false, updatable = false)
	private FaqCategoryDao faqCategory;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFaqCategoryId() {
		return faqCategoryId;
	}

	public void setFaqCategoryId(int faqCategoryId) {
		this.faqCategoryId = faqCategoryId;
	}

	public String getQuestionEn() {
		return questionEn;
	}

	public void setQuestionEn(String questionEn) {
		this.questionEn = questionEn;
	}

	public String getAnswerEn() {
		return answerEn;
	}

	public void setAnswerEn(String answerEn) {
		this.answerEn = answerEn;
	}

	public String getQuestionBm() {
		return questionBm;
	}

	public void setQuestionBm(String questionBm) {
		this.questionBm = questionBm;
	}

	public String getAnswerBm() {
		return answerBm;
	}

	public void setAnswerBm(String answerBm) {
		this.answerBm = answerBm;
	}

	public String getQuestionCn() {
		return questionCn;
	}

	public void setQuestionCn(String questionCn) {
		this.questionCn = questionCn;
	}

	public String getAnswerCn() {
		return answerCn;
	}

	public void setAnswerCn(String answerCn) {
		this.answerCn = answerCn;
	}

	public int getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public FaqStatus getStatus() {
		return status;
	}

	public void setStatus(FaqStatus status) {
		this.status = status;
	}

	public int getFaqSequence() {
		return faqSequence;
	}

	public void setFaqSequence(int faqSequence) {
		this.faqSequence = faqSequence;
	}

	public Date getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public FaqCategoryDao getFaqCategory() {
		return faqCategory;
	}

	public void setFaqCategory(FaqCategoryDao faqCategory) {
		this.faqCategory = faqCategory;
	}

	@Override
	public String toString() {
		return "Faq [id=" + id + ", faqCategoryId=" + faqCategoryId
				+ ", questionEn=" + questionEn + ", answerEn=" + answerEn
				+ ", questionBm=" + questionBm + ", answerBm=" + answerBm
				+ ", questionCn=" + questionCn + ", answerCn=" + answerCn
				+ ", deletedFlag=" + deletedFlag + ", faqSequence="
				+ faqSequence + ", updatedDatetime=" + updatedDatetime
				+ ", createdDatetime=" + createdDatetime + ", faqCategory="
				+ faqCategory + "]";
	}

}
