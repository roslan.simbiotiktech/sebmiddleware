package com.seb.middleware.jpa.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seb.faq_category")
public class FaqCategoryDao {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="description")
	private String description;
	
	@Column(name="category_sequence")
	private int categorySequence;
	
	@Column(name="deleted_flag")
	private int deletedFlag;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public int getCategorySequence() {
		return categorySequence;
	}

	public void setCategorySequence(int categorySequence) {
		this.categorySequence = categorySequence;
	}

	@Override
	public String toString() {
		return "FaqCategoryDao [id=" + id + ", name=" + name + ", description=" + description + ", categorySequence=" + categorySequence
				+ ", deletedFlag=" + deletedFlag + "]";
	}	
}
