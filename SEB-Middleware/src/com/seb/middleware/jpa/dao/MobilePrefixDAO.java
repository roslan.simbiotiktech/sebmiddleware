package com.seb.middleware.jpa.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seb.mobile_prefix")
public class MobilePrefixDAO {
	
	@Id
	@Column(name="country_name")
	private String countryName;
	
	@Column(name="prefix")
	private String prefix;

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	@Override
	public String toString() {
		return "MobilePrefixDAO [countryName=" + countryName + ", prefix=" + prefix + "]";
	}
}
