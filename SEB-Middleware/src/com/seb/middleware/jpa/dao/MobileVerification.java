package com.seb.middleware.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.seb.middleware.constant.VerificationStatus;

@Entity
@Table(name = "seb.mobile_verification")
public class MobileVerification extends VerificationBase {

	private String mobileNumber;

	public MobileVerification(){
		super();
	}
	
	public MobileVerification(String mobileNumber){
		super();
		setMobileNumber(mobileNumber);
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="verification_id")
	public long getVerificationId() {
		return verificationId;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	public VerificationStatus getStatus() {
		return status;
	}
	
	@Column(name="code")
	public String getCode() {
		return code;
	}

	@Generated(value=GenerationTime.INSERT)
	@Column(name="created_datetime", updatable=false, insertable=false)
	public Date getCreatedDatetime() {
		return createdDatetime;
	}
	
	@Column(name="mobile_number")
	public String getMobileNumber() {
		return mobileNumber;
	}
	
	public void setVerificationId(long verificationId) {
		this.verificationId = verificationId;
	}

	public void setStatus(VerificationStatus status) {
		this.status = status;
	}
	
	public void setCode(String code) {
		this.code = code;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Override
	public String toString() {
		return "MobileVerification [mobileNumber=" + mobileNumber + ", getVerificationId()=" + getVerificationId() + ", getStatus()=" + getStatus()
				+ ", getCreatedDatetime()=" + getCreatedDatetime() + ", maskedCode()=" + maskedCode() + "]";
	}
}
