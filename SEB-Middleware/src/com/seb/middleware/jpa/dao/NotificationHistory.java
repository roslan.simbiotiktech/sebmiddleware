package com.seb.middleware.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.seb.middleware.constant.MessageStatus;
import com.seb.middleware.constant.NotificationType;

@Entity
@Table(name = "seb.notification_history")
public class NotificationHistory {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="notification_id")
	private long notificationId;
	
	@Column(name="user_email")
	private String userEmail;
	
	@Column(name="title")
	private String title;
	
	@Column(name="text")
	private String text;
	
	@Enumerated(EnumType.STRING)
	@Column(name="notification_type")
	private NotificationType notificationType;
	
	@Column(name="associated_id")
	private Long associatedId;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private MessageStatus status;
	
	@Column(name="created_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;

	public long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(long notificationId) {
		this.notificationId = notificationId;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public NotificationType getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public Long getAssociatedId() {
		return associatedId;
	}

	public void setAssociatedId(Long associatedId) {
		this.associatedId = associatedId;
	}

	public MessageStatus getStatus() {
		return status;
	}

	public void setStatus(MessageStatus status) {
		this.status = status;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	@Override
	public String toString() {
		return "NotificationHistory [notificationId=" + notificationId
				+ ", userEmail=" + userEmail + ", title=" + title + ", text="
				+ text + ", notificationType=" + notificationType
				+ ", associatedId=" + associatedId + ", status=" + status
				+ ", createdDatetime=" + createdDatetime + "]";
	}
}
