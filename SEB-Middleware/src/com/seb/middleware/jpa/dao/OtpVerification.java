package com.seb.middleware.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.seb.middleware.constant.OtpType;
import com.seb.middleware.constant.VerificationStatus;

@Entity
@Table(name = "seb.otp_verification")  
public class OtpVerification extends VerificationBase {
	
	private String userEmail;
	
	private OtpType type;

	public OtpVerification(){
		super();
	}
	
	public OtpVerification(String userEmail, OtpType type){
		super();
		setUserEmail(userEmail);
		setType(type);
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="verification_id")
	public long getVerificationId() {
		return verificationId;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	public VerificationStatus getStatus() {
		return status;
	}
	
	@Column(name="code")
	public String getCode() {
		return code;
	}

	@Generated(value=GenerationTime.INSERT)
	@Column(name="created_datetime", updatable=false, insertable=false)
	public Date getCreatedDatetime() {
		return createdDatetime;
	}
	
	@Column(name="user_email")
	public String getUserEmail() {
		return userEmail;
	}
	
	public void setVerificationId(long verificationId) {
		this.verificationId = verificationId;
	}

	public void setStatus(VerificationStatus status) {
		this.status = status;
	}
	
	public void setCode(String code) {
		this.code = code;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	@Enumerated(EnumType.STRING)
	@Column(name="type")
	public OtpType getType() {
		return type;
	}

	public void setType(OtpType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "OtpVerification [userEmail=" + userEmail + ", type=" + type + ", verificationId=" + verificationId + ", status=" + status + ", code="
				+ code + ", createdDatetime=" + createdDatetime + "]";
	}
}
