package com.seb.middleware.jpa.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seb.payment_gateway")
public class PaymentGatewayDAO {

	@Id
	@Column(name="gateway_id")
	private String gatewayId;
	
	@Column(name="merchant_id")
	private String merchantId;

	@Column(name="reference_prefix")
	private String referencePrefix;

	@Column(name="min_amount")
	private double minAmount;
	
	@Column(name="max_amount")
	private double maxAmount;
	
	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getReferencePrefix() {
		return referencePrefix;
	}

	public void setReferencePrefix(String referencePrefix) {
		this.referencePrefix = referencePrefix;
	}

	public double getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(double minAmount) {
		this.minAmount = minAmount;
	}

	public double getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(double maxAmount) {
		this.maxAmount = maxAmount;
	}

	@Override
	public String toString() {
		return "PaymentGatewayDAO [gatewayId=" + gatewayId + ", merchantId=" + merchantId + ", referencePrefix=" + referencePrefix + ", minAmount="
				+ minAmount + ", maxAmount=" + maxAmount + "]";
	}
}
