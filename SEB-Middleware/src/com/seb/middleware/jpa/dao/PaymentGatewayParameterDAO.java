package com.seb.middleware.jpa.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seb.payment_gateway_parameter")
public class PaymentGatewayParameterDAO implements Serializable {
	
	private static final long serialVersionUID = -2382654763635521818L;

	@Id
	@Column(name="gateway_id")
	private String gatewayId;
	
	@Id
	@Column(name="param_name")
	private String paramName;

	@Column(name="param_value")
	private String paramValue;

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	@Override
	public String toString() {
		return "PaymentGatewayParameterDAO [gatewayId=" + gatewayId + ", paramName=" + paramName + ", paramValue=" + paramValue + "]";
	}
}
