package com.seb.middleware.jpa.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seb.payment_gateway_url")
public class PaymentGatewayUrlDAO implements Serializable {

	private static final long serialVersionUID = -7519686873021636976L;

	@Id
	@Column(name="gateway_id")
	private String gatewayId;
	
	@Id
	@Column(name="url_name")
	private String urlName;

	@Column(name="url_value")
	private String urlValue;

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getUrlName() {
		return urlName;
	}

	public void setUrlName(String urlName) {
		this.urlName = urlName;
	}

	public String getUrlValue() {
		return urlValue;
	}

	public void setUrlValue(String urlValue) {
		this.urlValue = urlValue;
	}

	@Override
	public String toString() {
		return "PaymentGatewayUrlDAO [gatewayId=" + gatewayId + ", urlName=" + urlName + ", urlValue=" + urlValue + "]";
	}
}
