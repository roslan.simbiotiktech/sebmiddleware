package com.seb.middleware.jpa.dao;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import com.seb.middleware.constant.PaymentStatus;

@Entity
public class PaymentHistoryDAO {

	@Id
	@Column(name="payment_entry_id")
	private long paymentEntryId;

	@Column(name="payment_id")
	private long paymentId;
	
	@Column(name="gateway_id")
	private String gatewayId;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private PaymentStatus status;
	
	@Column(name="user_email")
	private String userEmail;
	
	@Column(name="notification_email")
	private String notificationEmail;
	
	@Column(name="is_mobile")
	private int isMobile;
	
	@Column(name="gateway_ref")
	private String gatewayReference;
	
	@Column(name="gateway_status")
	private String gatewayStatus;
	
	@Column(name="external_gateway_status")
	private String externalGatewayStatus;
	
	@Column(name="payment_datetime")
	private Date paymentDate;

	@Column(name="created_datetime")
	private Date createdDate;
	
	@Column(name="contract_account_number")
	private String contractAccountNumber;
	
	@Column(name="amount")
	private BigDecimal amount;

	public long getPaymentEntryId() {
		return paymentEntryId;
	}

	public void setPaymentEntryId(long paymentEntryId) {
		this.paymentEntryId = paymentEntryId;
	}

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public PaymentStatus getStatus() {
		return status;
	}

	public void setStatus(PaymentStatus status) {
		this.status = status;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getNotificationEmail() {
		return notificationEmail;
	}

	public void setNotificationEmail(String notificationEmail) {
		this.notificationEmail = notificationEmail;
	}

	public int getIsMobile() {
		return isMobile;
	}

	public void setIsMobile(int isMobile) {
		this.isMobile = isMobile;
	}

	public String getGatewayReference() {
		return gatewayReference;
	}

	public void setGatewayReference(String gatewayReference) {
		this.gatewayReference = gatewayReference;
	}

	public String getGatewayStatus() {
		return gatewayStatus;
	}

	public void setGatewayStatus(String gatewayStatus) {
		this.gatewayStatus = gatewayStatus;
	}

	public String getExternalGatewayStatus() {
		return externalGatewayStatus;
	}

	public void setExternalGatewayStatus(String externalGatewayStatus) {
		this.externalGatewayStatus = externalGatewayStatus;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getContractAccountNumber() {
		return contractAccountNumber;
	}

	public void setContractAccountNumber(String contractAccountNumber) {
		this.contractAccountNumber = contractAccountNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "PaymentHistoryDAO [paymentEntryId=" + paymentEntryId + ", paymentId=" + paymentId + ", gatewayId="
				+ gatewayId + ", status=" + status + ", userEmail=" + userEmail + ", notificationEmail="
				+ notificationEmail + ", isMobile=" + isMobile + ", gatewayReference=" + gatewayReference
				+ ", gatewayStatus=" + gatewayStatus + ", externalGatewayStatus=" + externalGatewayStatus
				+ ", paymentDate=" + paymentDate + ", createdDate=" + createdDate + ", contractAccountNumber="
				+ contractAccountNumber + ", amount=" + amount + "]";
	}
}
