package com.seb.middleware.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.seb.middleware.constant.PaymentStatus;

@Entity
@Table(name = "seb.payment_reference")
public class PaymentReferenceDAO {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="payment_id")
	private long paymentId;
	
	@Column(name="gateway_id")
	private String gatewayId;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private PaymentStatus status;
	
	@Column(name="user_email")
	private String userEmail;
	
	@Column(name="notification_email")
	private String notificationEmail;
	
	@Column(name="is_mobile")
	private int isMobile;
	
	@Column(name="gateway_ref")
	private String gatewayReference;
	
	@Column(name="gateway_status")
	private String gatewayStatus;
	
	@Column(name="external_gateway_status")
	private String externalGatewayStatus;
	
	@Column(name="payment_datetime")
	private Date paymentDate;
	
	@Column(name="spg_bank_id")
	private Integer spgBankId;
	
	@Column(name="spg_bank_name")
	private String spgBankName;
	
	@Column(name="spg_bank_type")
	private String spgBankType;
	
	@Column(name="spg_card_type")
	private String spgCardType;
	
	@Column(name="reference_prefix")
	private String referencePrefix;
	
	@Column(name="updated_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.ALWAYS)
	private Date updatedDatetime;
	
	@Column(name="created_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDate;

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public PaymentStatus getStatus() {
		return status;
	}

	public void setStatus(PaymentStatus status) {
		this.status = status;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getNotificationEmail() {
		return notificationEmail;
	}

	public void setNotificationEmail(String notificationEmail) {
		this.notificationEmail = notificationEmail;
	}

	public int getIsMobile() {
		return isMobile;
	}

	public void setIsMobile(int isMobile) {
		this.isMobile = isMobile;
	}

	public String getGatewayReference() {
		return gatewayReference;
	}

	public void setGatewayReference(String gatewayReference) {
		this.gatewayReference = gatewayReference;
	}

	public String getGatewayStatus() {
		return gatewayStatus;
	}

	public void setGatewayStatus(String gatewayStatus) {
		this.gatewayStatus = gatewayStatus;
	}

	public String getExternalGatewayStatus() {
		return externalGatewayStatus;
	}

	public void setExternalGatewayStatus(String externalGatewayStatus) {
		this.externalGatewayStatus = externalGatewayStatus;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Integer getSpgBankId() {
		return spgBankId;
	}

	public void setSpgBankId(Integer spgBankId) {
		this.spgBankId = spgBankId;
	}

	public String getSpgBankName() {
		return spgBankName;
	}

	public void setSpgBankName(String spgBankName) {
		this.spgBankName = spgBankName;
	}

	public String getSpgBankType() {
		return spgBankType;
	}

	public void setSpgBankType(String spgBankType) {
		this.spgBankType = spgBankType;
	}

	public String getSpgCardType() {
		return spgCardType;
	}

	public void setSpgCardType(String spgCardType) {
		this.spgCardType = spgCardType;
	}

	public String getReferencePrefix() {
		return referencePrefix;
	}

	public void setReferencePrefix(String referencePrefix) {
		this.referencePrefix = referencePrefix;
	}

	public Date getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return "PaymentReferenceDAO [paymentId=" + paymentId + ", gatewayId=" + gatewayId + ", status=" + status + ", userEmail=" + userEmail
				+ ", notificationEmail=" + notificationEmail + ", isMobile=" + isMobile + ", gatewayReference=" + gatewayReference
				+ ", gatewayStatus=" + gatewayStatus + ", externalGatewayStatus=" + externalGatewayStatus + ", paymentDate=" + paymentDate
				+ ", spgBankId=" + spgBankId + ", spgBankName=" + spgBankName + ", spgBankType=" + spgBankType + ", spgCardType=" + spgCardType
				+ ", referencePrefix=" + referencePrefix + ", updatedDatetime=" + updatedDatetime + ", createdDate=" + createdDate + "]";
	}
}
