package com.seb.middleware.jpa.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seb.payment_reference_data")
public class PaymentReferenceDataDAO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -648960933457038046L;

	@Id
	@Column(name="payment_id")
	private long paymentId;
	
	@Id
	@Column(name="name")
	private String name;

	@Column(name="value")
	private String value;

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "PaymentReferenceDataDAO [paymentId=" + paymentId + ", name=" + name + ", value=" + value + "]";
	}
}
