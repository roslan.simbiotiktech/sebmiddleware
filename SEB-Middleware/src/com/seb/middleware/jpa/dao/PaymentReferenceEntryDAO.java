package com.seb.middleware.jpa.dao;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seb.payment_reference_entry")
public class PaymentReferenceEntryDAO {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="payment_entry_id")
	private long paymentEntryId;

	@Column(name="payment_id")
	private long paymentId;
	
	@Column(name="contract_account_number")
	private String contractAccountNumber;
	
	@Column(name="amount")
	private BigDecimal amount;
	
	@Column(name="billed")
	private boolean billed;
	
	@Column(name="billing_attempt_count")
	private int billingAttemptCount;
	
	@Column(name="billing_attempt_last_datetime")
	private Date billingAttemptLastDatetime;
	
	@Column(name="billing_attempt_last_error")
	private String billingAttemptLastError;

	public long getPaymentEntryId() {
		return paymentEntryId;
	}

	public void setPaymentEntryId(long paymentEntryId) {
		this.paymentEntryId = paymentEntryId;
	}

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public String getContractAccountNumber() {
		return contractAccountNumber;
	}

	public void setContractAccountNumber(String contractAccountNumber) {
		this.contractAccountNumber = contractAccountNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public boolean isBilled() {
		return billed;
	}

	public void setBilled(boolean billed) {
		this.billed = billed;
	}

	public int getBillingAttemptCount() {
		return billingAttemptCount;
	}

	public void setBillingAttemptCount(int billingAttemptCount) {
		this.billingAttemptCount = billingAttemptCount;
	}

	public Date getBillingAttemptLastDatetime() {
		return billingAttemptLastDatetime;
	}

	public void setBillingAttemptLastDatetime(Date billingAttemptLastDatetime) {
		this.billingAttemptLastDatetime = billingAttemptLastDatetime;
	}

	public String getBillingAttemptLastError() {
		return billingAttemptLastError;
	}

	public void setBillingAttemptLastError(String billingAttemptLastError) {
		this.billingAttemptLastError = billingAttemptLastError;
	}

	@Override
	public String toString() {
		return "PaymentReferenceEntryDAO [paymentEntryId=" + paymentEntryId + ", paymentId=" + paymentId + ", contractAccountNumber="
				+ contractAccountNumber + ", amount=" + amount + ", billed=" + billed + ", billingAttemptCount=" + billingAttemptCount
				+ ", billingAttemptLastDatetime=" + billingAttemptLastDatetime + ", billingAttemptLastError=" + billingAttemptLastError + "]";
	}
}
