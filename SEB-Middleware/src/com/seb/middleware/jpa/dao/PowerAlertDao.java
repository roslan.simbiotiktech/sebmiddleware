package com.seb.middleware.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "seb.power_alert")
public class PowerAlertDao {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	
	@Column(name="power_alert_type_id")
	private long powerAlertTypeId;
	
	@Column(name="cust_service_location_id")
	private long custServiceLocationId;
	
	@Column(name="area")
	private String area;
	
	@Column(name="causes")
	private String causes;
	
	@Column(name="title")
	private String title;
	
	@Column(name="restoration_datetime")
	private Date restorationDatetime;
	
	@Column(name="maintenance_start_datetime")
	private Date maintenanceStartDatetime;
	
	@Column(name="maintenance_end_datetime")
	private Date maintenanceEndDatetime;
	
	@Column(name="sfdc_id")
	private String sfdcId;
	
	@Column(name="deleted_flag")
	private int deletedFlag;
	
	@Column(name="updated_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.ALWAYS)
	private Date updatedDatetime;
	
	@Column(name="created_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;
	
	@OneToOne()
	@JoinColumn(name = "cust_service_location_id", insertable = false, updatable = false)
	private CustServiceLocation custServiceLocation;

	@OneToOne()
    @JoinColumn(name = "power_alert_type_id", insertable = false, updatable = false)
	private PowerAlertTypeDao powerAlertType;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPowerAlertTypeId() {
		return powerAlertTypeId;
	}

	public void setPowerAlertTypeId(long powerAlertTypeId) {
		this.powerAlertTypeId = powerAlertTypeId;
	}

	public long getCustServiceLocationId() {
		return custServiceLocationId;
	}

	public void setCustServiceLocationId(long custServiceLocationId) {
		this.custServiceLocationId = custServiceLocationId;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCauses() {
		return causes;
	}

	public void setCauses(String causes) {
		this.causes = causes;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getRestorationDatetime() {
		return restorationDatetime;
	}

	public void setRestorationDatetime(Date restorationDatetime) {
		this.restorationDatetime = restorationDatetime;
	}

	public Date getMaintenanceStartDatetime() {
		return maintenanceStartDatetime;
	}

	public void setMaintenanceStartDatetime(Date maintenanceStartDatetime) {
		this.maintenanceStartDatetime = maintenanceStartDatetime;
	}

	public Date getMaintenanceEndDatetime() {
		return maintenanceEndDatetime;
	}

	public void setMaintenanceEndDatetime(Date maintenanceEndDatetime) {
		this.maintenanceEndDatetime = maintenanceEndDatetime;
	}

	public String getSfdcId() {
		return sfdcId;
	}

	public void setSfdcId(String sfdcId) {
		this.sfdcId = sfdcId;
	}

	public int getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public Date getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public CustServiceLocation getCustServiceLocation() {
		return custServiceLocation;
	}

	public void setCustServiceLocation(CustServiceLocation custServiceLocation) {
		this.custServiceLocation = custServiceLocation;
	}

	public PowerAlertTypeDao getPowerAlertType() {
		return powerAlertType;
	}

	public void setPowerAlertType(PowerAlertTypeDao powerAlertType) {
		this.powerAlertType = powerAlertType;
	}

	@Override
	public String toString() {
		return "PowerAlertDao [id=" + id + ", powerAlertTypeId=" + powerAlertTypeId + ", custServiceLocationId="
				+ custServiceLocationId + ", area=" + area + ", causes=" + causes + ", title=" + title
				+ ", restorationDatetime=" + restorationDatetime + ", maintenanceStartDatetime="
				+ maintenanceStartDatetime + ", maintenanceEndDatetime=" + maintenanceEndDatetime + ", sfdcId=" + sfdcId
				+ ", deletedFlag=" + deletedFlag + ", updatedDatetime=" + updatedDatetime + ", createdDatetime="
				+ createdDatetime + "]";
	}
}
