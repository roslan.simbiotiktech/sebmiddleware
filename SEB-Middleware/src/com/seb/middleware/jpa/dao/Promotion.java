package com.seb.middleware.jpa.dao;

import java.util.Arrays;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "seb.news")
public class Promotion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	
	@Column(name="title")
	private String title;
	
	@Column(name="description")
	private String description;
	
	@Column(name="start_datetime")
	private Date startDateTime;
	
	@Column(name="end_datetime")
	private Date endDateTime;
	
	@Column(name="publish_datetime")
	private Date publishDateTime;

	@Column(name="deleted_flag")
	private int deletedFlag;
	
	@Column(name="image_s")
	private byte[] imageS;
	
	@Column(name="image_s_file_type")
	private String imageSFileype;
	
	@Column(name="image_l")
	private byte[] imageL;
	
	@Column(name="image_l_file_type")
	private String imageLFileype;
	
	@Column(name="updated_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.ALWAYS)
	private Date updatedDatetime;
	
	@Column(name="created_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}

	public Date getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}

	public Date getPublishDateTime() {
		return publishDateTime;
	}

	public void setPublishDateTime(Date publishDateTime) {
		this.publishDateTime = publishDateTime;
	}

	public int getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public byte[] getImageS() {
		return imageS;
	}

	public void setImageS(byte[] imageS) {
		this.imageS = imageS;
	}

	public String getImageSFileype() {
		return imageSFileype;
	}

	public void setImageSFileype(String imageSFileype) {
		this.imageSFileype = imageSFileype;
	}

	public byte[] getImageL() {
		return imageL;
	}

	public void setImageL(byte[] imageL) {
		this.imageL = imageL;
	}

	public String getImageLFileype() {
		return imageLFileype;
	}

	public void setImageLFileype(String imageLFileype) {
		this.imageLFileype = imageLFileype;
	}

	public Date getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	@Override
	public String toString() {
		return "Promotion [id=" + id + ", title=" + title + ", description=" + description + ", startDateTime=" + startDateTime + ", endDateTime="
				+ endDateTime + ", publishDateTime=" + publishDateTime + ", deletedFlag=" + deletedFlag + ", imageS=" + Arrays.toString(imageS)
				+ ", imageSFileype=" + imageSFileype + ", imageL=" + Arrays.toString(imageL) + ", imageLFileype=" + imageLFileype
				+ ", updatedDatetime=" + updatedDatetime + ", createdDatetime=" + createdDatetime + "]";
	}
}
