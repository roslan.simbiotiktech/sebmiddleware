package com.seb.middleware.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seb.remember_me")
public class RememberMe {

	@Id
	@Column(name="client_id")
	private String clientId;
	
	@Column(name="user_email")
	private String userEmail;
	
	@Column(name="expiry_date")
	private Date expiryDate;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	@Override
	public String toString() {
		return "RememberMe [clientId=" + clientId + ", userEmail=" + userEmail + ", expiryDate=" + expiryDate + "]";
	}
}
