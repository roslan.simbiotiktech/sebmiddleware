package com.seb.middleware.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.seb.middleware.constant.NotificationStatus;

@Entity
@Table(name = "seb.report_notification")
public class ReportNotification {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="notification_id")
	private long notificationId;
	
	@Column(name="user_email")
	private String userEmail;
	
	@Column(name="text")
	private String text;
	
	@Column(name="trans_id")
	private long transId;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private NotificationStatus status;
	
	@Column(name="sending_datetime")
	private Date sendingDatetime;
	
	@Column(name="send_counter")
	private int sendCounter;
	
	@Column(name="created_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;

	public long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(long notificationId) {
		this.notificationId = notificationId;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public long getTransId() {
		return transId;
	}

	public void setTransId(long transId) {
		this.transId = transId;
	}

	public NotificationStatus getStatus() {
		return status;
	}

	public void setStatus(NotificationStatus status) {
		this.status = status;
	}

	public Date getSendingDatetime() {
		return sendingDatetime;
	}

	public void setSendingDatetime(Date sendingDatetime) {
		this.sendingDatetime = sendingDatetime;
	}

	public int getSendCounter() {
		return sendCounter;
	}

	public void setSendCounter(int sendCounter) {
		this.sendCounter = sendCounter;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	@Override
	public String toString() {
		return "ReportNotification [notificationId=" + notificationId + ", userEmail=" + userEmail + ", text=" + text + ", transId=" + transId
				+ ", status=" + status + ", sendingDatetime=" + sendingDatetime + ", sendCounter=" + sendCounter + ", createdDatetime="
				+ createdDatetime + "]";
	}
}
