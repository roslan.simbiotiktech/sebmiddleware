package com.seb.middleware.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.seb.middleware.constant.NewTransStatus;
import com.seb.middleware.constant.ReportCategory;
import com.seb.middleware.constant.ReportIssueType;
import com.seb.middleware.constant.ReportType;
import com.seb.middleware.constant.SFDCClassification;
import com.seb.middleware.constant.SFDCReportStatus;

@Entity
@Table(name = "seb.sfdc_report")
public class SFDCReport {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="trans_id")
	private long transId;
	
	@Column(name="case_number")
	private String caseNumber;

	@Column(name="case_id")
	private String caseId;
	
	@Enumerated(EnumType.STRING)
	@Column(name="classification")
	private SFDCClassification classification;
	
	@Enumerated(EnumType.STRING)
	@Column(name="report_type")
	private ReportType reportType;

	@Enumerated(EnumType.STRING)
	@Column(name="category")
	private ReportCategory reportCategory;
	
	@Enumerated(EnumType.STRING)
	@Column(name="issue_type")
	private ReportIssueType issueType;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private SFDCReportStatus status;
	
	@Column(name="sub_status")
	private String subStatus;
	
	@Enumerated(EnumType.STRING)
	@Column(name="previous_notified_status")
	private SFDCReportStatus previousNotifiedStatus;
	
	@Enumerated(EnumType.STRING)
	@Column(name="seb_status")
	private NewTransStatus sebStatus;
	
	@Column(name="contract_account_number")
	private String contractAccNo;
	
	@Column(name="remark")
	private String remark;
	
	@Column(name="channel")
	private String channel;
	
	@Column(name="client_os")
	private String clientOs;
	
	@Column(name="user_email")
	private String userEmail;
	
	@Column(name="user_name")
	private String userName;
	
	@Column(name="user_mobile_number")
	private String userMobileNumber;
	
	@Column(name="station")
	private String station;
	
	@Column(name="description")
	private String description;
	
	@Column(name="loc_longitude")
	private String longitude;
	
	@Column(name="loc_latitude")
	private String latitude;
	
	@Column(name="loc_address")
	private String address;
	
	@Column(name="photo_1")
	private byte[] photo1;
	
	@Column(name="photo_1_type")
	private String photo1type;
	
	@Column(name="photo_2")
	private byte[] photo2;
	
	@Column(name="photo_2_type")
	private String photo2type;
	
	@Column(name="photo_3")
	private byte[] photo3;
	
	@Column(name="photo_3_type")
	private String photo3type;
	
	@Column(name="updated_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.ALWAYS)
	private Date updatedDatetime;
	
	@Column(name="created_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;
	
	@Column(name="deleted_flag")
	private int deletedFlag;

	public long getTransId() {
		return transId;
	}

	public void setTransId(long transId) {
		this.transId = transId;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public ReportCategory getReportCategory() {
		return reportCategory;
	}

	public void setReportCategory(ReportCategory reportCategory) {
		this.reportCategory = reportCategory;
	}

	public SFDCClassification getClassification() {
		return classification;
	}

	public void setClassification(SFDCClassification classification) {
		this.classification = classification;
	}

	public ReportType getReportType() {
		return reportType;
	}

	public void setReportType(ReportType reportType) {
		this.reportType = reportType;
	}

	public ReportIssueType getIssueType() {
		return issueType;
	}

	public void setIssueType(ReportIssueType issueType) {
		this.issueType = issueType;
	}

	public SFDCReportStatus getStatus() {
		return status;
	}

	public void setStatus(SFDCReportStatus status) {
		this.status = status;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public SFDCReportStatus getPreviousNotifiedStatus() {
		return previousNotifiedStatus;
	}

	public void setPreviousNotifiedStatus(SFDCReportStatus previousNotifiedStatus) {
		this.previousNotifiedStatus = previousNotifiedStatus;
	}

	public NewTransStatus getSebStatus() {
		return sebStatus;
	}

	public void setSebStatus(NewTransStatus sebStatus) {
		this.sebStatus = sebStatus;
	}

	public String getContractAccNo() {
		return contractAccNo;
	}

	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getClientOs() {
		return clientOs;
	}

	public void setClientOs(String clientOs) {
		this.clientOs = clientOs;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserMobileNumber() {
		return userMobileNumber;
	}

	public void setUserMobileNumber(String userMobileNumber) {
		this.userMobileNumber = userMobileNumber;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public byte[] getPhoto1() {
		return photo1;
	}

	public void setPhoto1(byte[] photo1) {
		this.photo1 = photo1;
	}

	public String getPhoto1type() {
		return photo1type;
	}

	public void setPhoto1type(String photo1type) {
		this.photo1type = photo1type;
	}

	public byte[] getPhoto2() {
		return photo2;
	}

	public void setPhoto2(byte[] photo2) {
		this.photo2 = photo2;
	}

	public String getPhoto2type() {
		return photo2type;
	}

	public void setPhoto2type(String photo2type) {
		this.photo2type = photo2type;
	}

	public byte[] getPhoto3() {
		return photo3;
	}

	public void setPhoto3(byte[] photo3) {
		this.photo3 = photo3;
	}

	public String getPhoto3type() {
		return photo3type;
	}

	public void setPhoto3type(String photo3type) {
		this.photo3type = photo3type;
	}

	public Date getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SFDCReport [transId=").append(transId).append(", caseNumber=").append(caseNumber)
				.append(", caseId=").append(caseId).append(", reportCategory=").append(reportCategory)
				.append(", classification=").append(classification).append(", reportType=").append(reportType)
				.append(", issueType=").append(issueType).append(", status=").append(status).append(", subStatus=")
				.append(subStatus).append(", previousNotifiedStatus=").append(previousNotifiedStatus)
				.append(", sebStatus=").append(sebStatus).append(", contractAccNo=").append(contractAccNo)
				.append(", remark=").append(remark).append(", channel=").append(channel).append(", clientOs=")
				.append(clientOs).append(", userEmail=").append(userEmail).append(", userName=").append(userName)
				.append(", userMobileNumber=").append(userMobileNumber).append(", station=").append(station)
				.append(", description=").append(description).append(", longitude=").append(longitude)
				.append(", latitude=").append(latitude).append(", address=").append(address).append(", photo1type=")
				.append(photo1type).append(", photo2type=").append(photo2type).append(", photo3type=")
				.append(photo3type).append(", updatedDatetime=").append(updatedDatetime).append(", createdDatetime=")
				.append(createdDatetime).append(", deletedFlag=").append(deletedFlag).append("]");
		return builder.toString();
	}
}
