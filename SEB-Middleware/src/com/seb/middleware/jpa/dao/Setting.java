package com.seb.middleware.jpa.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seb.setting")
public class Setting {

	@Id
	@Column(name="setting_key")
	private String settingKey;
	
	@Column(name="setting_value")
	private String settingValue;

	public String getSettingKey() {
		return settingKey;
	}

	public void setSettingKey(String settingKey) {
		this.settingKey = settingKey;
	}

	public String getSettingValue() {
		return settingValue;
	}

	public void setSettingValue(String settingValue) {
		this.settingValue = settingValue;
	}

	@Override
	public String toString() {
		return "Setting [settingKey=" + settingKey + ", settingValue=" + settingValue + "]";
	}
}
