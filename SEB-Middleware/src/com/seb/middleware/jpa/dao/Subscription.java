package com.seb.middleware.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.seb.middleware.constant.SubscriptionStatus;
import com.seb.middleware.constant.SubscriptionType;

@Entity
@Table(name = "seb.subscription")
public class Subscription {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="subs_id")
	private long subsId;
	
	@Column(name="user_email")
	private String userEmail;
	
	@Column(name="contract_account_number")
	private String contractAccNo;
	
	@Column(name="contract_account_nick")
	private String contractAccNick;
	
	@Enumerated(EnumType.STRING)
	@Column(name="subs_type")
	private SubscriptionType subsType;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private SubscriptionStatus status = SubscriptionStatus.ACTIVE;
	
	@Column(name="updated_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.ALWAYS)
	private Date updatedDatetime;
	
	@Column(name="created_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;

	@OneToOne()
    @JoinColumn(name = "contract_account_number", insertable = false, updatable = false)
	private Contract contract;
	
	@Column(name="remark")
	private String remark;
	
	@Column(name="notify_by_email")
	private int notifyByEmail;
	
	@Column(name="notify_by_sms")
	private int notifyBySMS;
	
	@Column(name="notify_by_mobile_push")
	private int notifyByMobilePush;
	
	@Column(name="sfdc_id")
	private String sfdcId;
	
	@Column(name="sfdc_updated_datetime")
	private Date sfdcUpdatedDate;
	
	@Column(name="sfdc_sync_datetime")
	private Date sfdcSyncDate;
	
	public long getSubsId() {
		return subsId;
	}

	public void setSubsId(long subsId) {
		this.subsId = subsId;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getContractAccNo() {
		return contractAccNo;
	}

	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}

	public String getContractAccNick() {
		return contractAccNick;
	}

	public void setContractAccNick(String contractAccNick) {
		this.contractAccNick = contractAccNick;
	}

	public SubscriptionType getSubsType() {
		return subsType;
	}

	public void setSubsType(SubscriptionType subsType) {
		this.subsType = subsType;
	}

	public SubscriptionStatus getStatus() {
		return status;
	}

	public void setStatus(SubscriptionStatus status) {
		this.status = status;
	}

	public Date getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public int getNotifyByEmail() {
		return notifyByEmail;
	}

	public void setNotifyByEmail(int notifyByEmail) {
		this.notifyByEmail = notifyByEmail;
	}

	public int getNotifyBySMS() {
		return notifyBySMS;
	}

	public void setNotifyBySMS(int notifyBySMS) {
		this.notifyBySMS = notifyBySMS;
	}

	public int getNotifyByMobilePush() {
		return notifyByMobilePush;
	}

	public void setNotifyByMobilePush(int notifyByMobilePush) {
		this.notifyByMobilePush = notifyByMobilePush;
	}

	public String getSfdcId() {
		return sfdcId;
	}

	public void setSfdcId(String sfdcId) {
		this.sfdcId = sfdcId;
	}

	public Date getSfdcUpdatedDate() {
		return sfdcUpdatedDate;
	}

	public void setSfdcUpdatedDate(Date sfdcUpdatedDate) {
		this.sfdcUpdatedDate = sfdcUpdatedDate;
	}

	public Date getSfdcSyncDate() {
		return sfdcSyncDate;
	}

	public void setSfdcSyncDate(Date sfdcSyncDate) {
		this.sfdcSyncDate = sfdcSyncDate;
	}

	@Override
	public String toString() {
		return "Subscription [subsId=" + subsId + ", userEmail=" + userEmail + ", contractAccNo=" + contractAccNo
				+ ", contractAccNick=" + contractAccNick + ", subsType=" + subsType + ", status=" + status
				+ ", updatedDatetime=" + updatedDatetime + ", createdDatetime=" + createdDatetime + ", contract="
				+ contract + ", remark=" + remark + ", notifyByEmail=" + notifyByEmail + ", notifyBySMS=" + notifyBySMS
				+ ", notifyByMobilePush=" + notifyByMobilePush + ", sfdcId=" + sfdcId + ", sfdcUpdatedDate="
				+ sfdcUpdatedDate + ", sfdcSyncDate=" + sfdcSyncDate + "]";
	}
}