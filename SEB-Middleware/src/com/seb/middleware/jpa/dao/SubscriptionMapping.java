package com.seb.middleware.jpa.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SubscriptionMapping {

	@Id
	@Column(name="subs_id")
	private long subsId;
	
	@Column(name="account_sfdc_id")
	private String accountSfdcId;
	
	@Column(name="contract_sfdc_id")
	private String contractSfdcId;

	public long getSubsId() {
		return subsId;
	}

	public void setSubsId(long subsId) {
		this.subsId = subsId;
	}

	public String getAccountSfdcId() {
		return accountSfdcId;
	}

	public void setAccountSfdcId(String accountSfdcId) {
		this.accountSfdcId = accountSfdcId;
	}

	public String getContractSfdcId() {
		return contractSfdcId;
	}

	public void setContractSfdcId(String contractSfdcId) {
		this.contractSfdcId = contractSfdcId;
	}

	@Override
	public String toString() {
		return "SubscriptionMapping [subsId=" + subsId + ", accountSfdcId=" + accountSfdcId + ", contractSfdcId="
				+ contractSfdcId + "]";
	}
}