package com.seb.middleware.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.seb.middleware.constant.NotificationStatus;
import com.seb.middleware.constant.NotificationType;

@Entity
@Table(name = "seb.tag_notification")
public class TagNotification {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="notification_id")
	private long notificationId;
	
	@Column(name="text")
	private String text;
	
	@Enumerated(EnumType.STRING)
	@Column(name="notification_type")
	private NotificationType notificationType;
	
	@Column(name="associated_id")
	private Long associatedId;
	
	@Column(name="tag_name")
	private String tagName;
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private NotificationStatus status;
	
	@Column(name="push_datetime")
	private Date pushDatetime;
	
	@Column(name="sending_datetime")
	private Date sendingDatetime;
	
	@Column(name="send_counter")
	private int sendCounter;
	
	@Column(name="created_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;

	public long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(long notificationId) {
		this.notificationId = notificationId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public NotificationType getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public Long getAssociatedId() {
		return associatedId;
	}

	public void setAssociatedId(Long associatedId) {
		this.associatedId = associatedId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagNme(String tagName) {
		this.tagName = tagName;
	}

	public NotificationStatus getStatus() {
		return status;
	}

	public void setStatus(NotificationStatus status) {
		this.status = status;
	}

	public Date getPushDatetime() {
		return pushDatetime;
	}

	public void setPushDatetime(Date pushDatetime) {
		this.pushDatetime = pushDatetime;
	}

	public Date getSendingDatetime() {
		return sendingDatetime;
	}

	public void setSendingDatetime(Date sendingDatetime) {
		this.sendingDatetime = sendingDatetime;
	}

	public int getSendCounter() {
		return sendCounter;
	}

	public void setSendCounter(int sendCounter) {
		this.sendCounter = sendCounter;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	@Override
	public String toString() {
		return "TagNotification [notificationId=" + notificationId + ", text=" + text + ", notificationType=" + notificationType + ", associatedId="
				+ associatedId + ", tagName=" + tagName + ", status=" + status + ", pushDatetime=" + pushDatetime + ", sendingDatetime="
				+ sendingDatetime + ", sendCounter=" + sendCounter + ", createdDatetime=" + createdDatetime + "]";
	}
}
