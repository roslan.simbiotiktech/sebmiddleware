package com.seb.middleware.jpa.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "seb.tag_subscriptions")
public class TagSubscriptions implements Serializable {

	private static final long serialVersionUID = -7405784539485818009L;

	@Id
	@Column(name="user_email")
	private String userEmail;
	
	@Id
	@Column(name="tag_name")
	private String tagName;
	
	@Column(name="created_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	@Override
	public String toString() {
		return "TagSubscriptions [userEmail=" + userEmail + ", tagName=" + tagName + ", createdDatetime=" + createdDatetime + "]";
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj == null) return false;
		
		if(obj instanceof TagSubscriptions){
			TagSubscriptions ts = (TagSubscriptions) obj;
			return emailEquals(ts.getUserEmail()) && tagNameEquals(ts.getTagName());
		}
		
		return false;
	}
	
	private boolean emailEquals(String email){
		if(email != null && getUserEmail() != null){
			if(email.equals(getUserEmail())){
				return true;	
			}
		}else if(email == null && getUserEmail() == null){
			return true;
		}
		
		return false;
	}
	
	private boolean tagNameEquals(String tagName){
		if(tagName != null && getTagName() != null){
			if(tagName.equals(getTagName())){
				return true;	
			}
		}else if(tagName == null && getTagName() == null){
			return true;
		}
		
		return false;
	}
}
