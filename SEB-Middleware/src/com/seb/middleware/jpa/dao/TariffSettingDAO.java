package com.seb.middleware.jpa.dao;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

@Entity
@Table(name = "seb.tariff_setting")
public class TariffSettingDAO {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private long id;
	
	@Column(name="tariff_category_id")
	private int tariffCategoryId;
	
	@Column(name="tr_from_unit")
	private BigDecimal trFromUnit;
	
	@Column(name="tr_to_unit")
	private BigDecimal trToUnit;
	
	@Column(name="tr_amount_per_unit")
	private BigDecimal trAmountPerUnit;
	
	@Column(name="tr_more_unit")
	private BigDecimal trMoreUnit;
	
	@Column(name="tr_more_amount")
	private BigDecimal trMoreAmount;
	
	@Column(name="exempted_gst_first_kwh")
	private BigDecimal exemptedGstFirstKwh;
	
	@Column(name="kw_max_demand_amount")
	private BigDecimal kwMaxDemandAmount;
	
	@Column(name="min_monthly_charge_amount")
	private BigDecimal minMonthlyChargeAmount;
	
	@Column(name="low_power_factor")
	private boolean lowPowerFactor;
	
	@Column(name="peak")
	private boolean peak;
	
	@Column(name="peak_amount")
	private BigDecimal peakAmount;
	
	@Column(name="non_peak_amount")
	private BigDecimal nonPeakAmount;
	
	@Column(name="updated_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.ALWAYS)
	private Date updatedDatetime;
	
	@Column(name="created_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDatetime;
	
	@Column(name="deleted_flag")
	private int deletedFlag;

	@OneToOne()
    @JoinColumn(name = "tariff_category_id", insertable = false, updatable = false)
	private TariffCategoryDAO tariffCategory;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getTariffCategoryId() {
		return tariffCategoryId;
	}

	public void setTariffCategoryId(int tariffCategoryId) {
		this.tariffCategoryId = tariffCategoryId;
	}

	public BigDecimal getTrFromUnit() {
		return trFromUnit;
	}

	public void setTrFromUnit(BigDecimal trFromUnit) {
		this.trFromUnit = trFromUnit;
	}

	public BigDecimal getTrToUnit() {
		return trToUnit;
	}

	public void setTrToUnit(BigDecimal trToUnit) {
		this.trToUnit = trToUnit;
	}

	public BigDecimal getTrAmountPerUnit() {
		return trAmountPerUnit;
	}

	public void setTrAmountPerUnit(BigDecimal trAmountPerUnit) {
		this.trAmountPerUnit = trAmountPerUnit;
	}

	public BigDecimal getTrMoreUnit() {
		return trMoreUnit;
	}

	public void setTrMoreUnit(BigDecimal trMoreUnit) {
		this.trMoreUnit = trMoreUnit;
	}

	public BigDecimal getTrMoreAmount() {
		return trMoreAmount;
	}

	public void setTrMoreAmount(BigDecimal trMoreAmount) {
		this.trMoreAmount = trMoreAmount;
	}

	public BigDecimal getExemptedGstFirstKwh() {
		return exemptedGstFirstKwh;
	}

	public void setExemptedGstFirstKwh(BigDecimal exemptedGstFirstKwh) {
		this.exemptedGstFirstKwh = exemptedGstFirstKwh;
	}

	public BigDecimal getKwMaxDemandAmount() {
		return kwMaxDemandAmount;
	}

	public void setKwMaxDemandAmount(BigDecimal kwMaxDemandAmount) {
		this.kwMaxDemandAmount = kwMaxDemandAmount;
	}

	public BigDecimal getMinMonthlyChargeAmount() {
		return minMonthlyChargeAmount;
	}

	public void setMinMonthlyChargeAmount(BigDecimal minMonthlyChargeAmount) {
		this.minMonthlyChargeAmount = minMonthlyChargeAmount;
	}

	public boolean isLowPowerFactor() {
		return lowPowerFactor;
	}

	public void setLowPowerFactor(boolean lowPowerFactor) {
		this.lowPowerFactor = lowPowerFactor;
	}

	public boolean isPeak() {
		return peak;
	}

	public void setPeak(boolean peak) {
		this.peak = peak;
	}

	public BigDecimal getPeakAmount() {
		return peakAmount;
	}

	public void setPeakAmount(BigDecimal peakAmount) {
		this.peakAmount = peakAmount;
	}

	public BigDecimal getNonPeakAmount() {
		return nonPeakAmount;
	}

	public void setNonPeakAmount(BigDecimal nonPeakAmount) {
		this.nonPeakAmount = nonPeakAmount;
	}

	public Date getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public TariffCategoryDAO getTariffCategory() {
		return tariffCategory;
	}

	public void setTariffCategory(TariffCategoryDAO tariffCategory) {
		this.tariffCategory = tariffCategory;
	}

	@Override
	public String toString() {
		return "TariffSettingDAO [id=" + id + ", tariffCategoryId=" + tariffCategoryId + ", trFromUnit=" + trFromUnit + ", trToUnit=" + trToUnit
				+ ", trAmountPerUnit=" + trAmountPerUnit + ", trMoreUnit=" + trMoreUnit + ", trMoreAmount=" + trMoreAmount + ", exemptedGstFirstKwh="
				+ exemptedGstFirstKwh + ", kwMaxDemandAmount=" + kwMaxDemandAmount + ", minMonthlyChargeAmount=" + minMonthlyChargeAmount
				+ ", lowPowerFactor=" + lowPowerFactor + ", peak=" + peak + ", peakAmount=" + peakAmount + ", nonPeakAmount=" + nonPeakAmount
				+ ", updatedDatetime=" + updatedDatetime + ", createdDatetime=" + createdDatetime + ", deletedFlag=" + deletedFlag
				+ ", tariffCategory=" + tariffCategory + "]";
	}
}
