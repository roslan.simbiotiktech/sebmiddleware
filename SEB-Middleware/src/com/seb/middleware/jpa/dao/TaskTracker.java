package com.seb.middleware.jpa.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seb.task_tracker")
public class TaskTracker {
	
	@Id
	@Column(name="task_key")
	private String taskKey;
	
	@Column(name="task_datetime")
	private Date taskDatetime;

	public String getTaskKey() {
		return taskKey;
	}

	public void setTaskKey(String taskKey) {
		this.taskKey = taskKey;
	}

	public Date getTaskDatetime() {
		return taskDatetime;
	}

	public void setTaskDatetime(Date taskDatetime) {
		this.taskDatetime = taskDatetime;
	}

	@Override
	public String toString() {
		return "TaskTracker [taskKey=" + taskKey + ", taskDatetime=" + taskDatetime + "]";
	}
}
