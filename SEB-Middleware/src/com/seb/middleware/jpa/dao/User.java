package com.seb.middleware.jpa.dao;

import java.util.Arrays;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import com.seb.middleware.constant.PreferredCommunicationMethod;
import com.seb.middleware.constant.UserStatus;

@Entity
@Table(name = "seb.user")
public class User {

	@Id
	@Column(name="user_id")
	private String userId;

	@Enumerated(EnumType.STRING)
	@Column(name="status")
	private UserStatus status;
	
	@Enumerated(EnumType.STRING)
	@Column(name="preferred_communication_method")
	private PreferredCommunicationMethod preferredCommunicationMethod;
	
	@Column(name="existing_email")
	private String existingEmail;
	
	@Column(name="existing_password")
	private String existingPassword;
	
	@Column(name="preferred_locale")
	private String preferredLocale;
	
	@Column(name="flag_password_expired")
	private int passwordExpired;
	
	@Column(name="created_datetime", updatable=false, insertable=false)
	@Generated(value=GenerationTime.INSERT)
	private Date createdDate;
	
	@Column(name="profile_photo")
	private byte[] profilePhoto;

	@Column(name="profile_photo_type")
	private String profilePhotoType;
	
	@Column(name="current_login_at")
	private Date currentLoginAt;
	
	@Column(name="last_login_at")
	private Date lastLoginAt;
	
	@Column(name="remark")
	private String remark;
	
	@Column(name="sfdc_id")
	private String sfdcId;
	
	@Column(name="sfdc_updated_datetime")
	private Date sfdcUpdatedDate;
	
	@Column(name="sfdc_sync_datetime")
	private Date sfdcSyncDate;
	
	public String getUserId() {
		return userId;
	}

	public UserStatus getStatus() {
		return status;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}

	public PreferredCommunicationMethod getPreferredCommunicationMethod() {
		return preferredCommunicationMethod;
	}

	public void setPreferredCommunicationMethod(PreferredCommunicationMethod preferredCommunicationMethod) {
		this.preferredCommunicationMethod = preferredCommunicationMethod;
	}

	public String getExistingEmail() {
		return existingEmail;
	}

	public void setExistingEmail(String existingEmail) {
		this.existingEmail = existingEmail;
	}

	public String getExistingPassword() {
		return existingPassword;
	}

	public void setExistingPassword(String existingPassword) {
		this.existingPassword = existingPassword;
	}

	public String getPreferredLocale() {
		return preferredLocale;
	}

	public void setPreferredLocale(String preferredLocale) {
		this.preferredLocale = preferredLocale;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getPasswordExpired() {
		return passwordExpired;
	}

	public void setPasswordExpired(int passwordExpired) {
		this.passwordExpired = passwordExpired;
	}

	public byte[] getProfilePhoto() {
		return profilePhoto;
	}

	public void setProfilePhoto(byte[] profilePhoto) {
		this.profilePhoto = profilePhoto;
	}

	public String getProfilePhotoType() {
		return profilePhotoType;
	}

	public void setProfilePhotoType(String profilePhotoType) {
		this.profilePhotoType = profilePhotoType;
	}

	public Date getCurrentLoginAt() {
		return currentLoginAt;
	}

	public void setCurrentLoginAt(Date currentLoginAt) {
		this.currentLoginAt = currentLoginAt;
	}

	public Date getLastLoginAt() {
		return lastLoginAt;
	}

	public void setLastLoginAt(Date lastLoginAt) {
		this.lastLoginAt = lastLoginAt;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSfdcId() {
		return sfdcId;
	}

	public void setSfdcId(String sfdcId) {
		this.sfdcId = sfdcId;
	}

	public Date getSfdcUpdatedDate() {
		return sfdcUpdatedDate;
	}

	public void setSfdcUpdatedDate(Date sfdcUpdatedDate) {
		this.sfdcUpdatedDate = sfdcUpdatedDate;
	}

	public Date getSfdcSyncDate() {
		return sfdcSyncDate;
	}

	public void setSfdcSyncDate(Date sfdcSyncDate) {
		this.sfdcSyncDate = sfdcSyncDate;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", status=" + status + ", preferredCommunicationMethod="
				+ preferredCommunicationMethod + ", existingEmail=" + existingEmail + ", existingPassword="
				+ existingPassword + ", preferredLocale=" + preferredLocale + ", passwordExpired=" + passwordExpired
				+ ", createdDate=" + createdDate + ", profilePhoto=" + Arrays.toString(profilePhoto)
				+ ", profilePhotoType=" + profilePhotoType + ", currentLoginAt=" + currentLoginAt + ", lastLoginAt="
				+ lastLoginAt + ", remark=" + remark + ", sfdcId=" + sfdcId + ", sfdcUpdatedDate=" + sfdcUpdatedDate
				+ ", sfdcSyncDate=" + sfdcSyncDate + "]";
	}
}
