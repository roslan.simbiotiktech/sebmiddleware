package com.seb.middleware.jpa.dao;

import java.util.Date;

import com.seb.middleware.constant.VerificationStatus;
import com.seb.middleware.utility.StringUtil;

public class VerificationBase {

	protected long verificationId;
	
	protected VerificationStatus status;
	
	protected String code;
	
	protected Date createdDatetime;
	
	private boolean expired = true;
	
	
	public long getVerificationId() {
		return verificationId;
	}

	public void setVerificationId(long verificationId) {
		this.verificationId = verificationId;
	}

	public VerificationStatus getStatus() {
		return status;
	}

	public void setStatus(VerificationStatus status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	protected String maskedCode(){
		if(!StringUtil.isEmpty(code)){
			return code.substring(0, 3) + "***";	
		}else{
			return code;
		}
	}
}
