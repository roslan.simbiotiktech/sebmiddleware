package com.seb.middleware.jpa.helper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.seb.middleware.api.services.response.Pagination;
import com.seb.middleware.api.services.response.content.Station;
import com.seb.middleware.constant.CalculatorType;
import com.seb.middleware.constant.FaqStatus;
import com.seb.middleware.constant.PowerAlertType;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseHelper;
import com.seb.middleware.jpa.dao.ContactUs;
import com.seb.middleware.jpa.dao.CustServiceCounter;
import com.seb.middleware.jpa.dao.CustServiceLocation;
import com.seb.middleware.jpa.dao.Faq;
import com.seb.middleware.jpa.dao.FaqCategoryDao;
import com.seb.middleware.jpa.dao.MobilePrefixDAO;
import com.seb.middleware.jpa.dao.PowerAlertDao;
import com.seb.middleware.jpa.dao.PowerAlertTypeDao;
import com.seb.middleware.jpa.dao.Promotion;
import com.seb.middleware.jpa.dao.TariffSettingDAO;
import com.seb.middleware.utility.DateUtil;
import com.seb.middleware.utility.StringUtil;

public class ContentHelper extends DatabaseHelper{

	private int DELETED_FLAG_ACTIVE = 0;
	
	public ContentHelper(DatabaseFacade db) {
		super(db);
	}
	
	public Station[] getStations(){
		return getStations(DELETED_FLAG_ACTIVE);
	}
	
	public Station[] getStations(int deletedFlag){

		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(CustServiceLocation.class);
		criteria.add(Restrictions.eq("deletedFlag", deletedFlag));
		criteria.addOrder(Order.asc("station"));
		
		ArrayList<CustServiceLocation> custServiceLocations = db.query(criteria);
		
		Station [] stations = new Station [custServiceLocations.size()];

		for(int i = 0; i < custServiceLocations.size(); i++){
			CustServiceLocation location = custServiceLocations.get(i);
			
			String stationName = location.getStation().trim();
			
			Station station = new Station();
			station.setName(stationName);
			station.setKey(stationName.replace(" ", "_").toUpperCase());
			stations[i] = station;
		}
		
		return stations;
	}

	public CustServiceLocation getCustServiceLocation(String station){

		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(CustServiceLocation.class);
		criteria.add(Restrictions.eq("deletedFlag", DELETED_FLAG_ACTIVE));
		criteria.addOrder(Order.asc("station"));
		
		ArrayList<CustServiceLocation> custServiceLocations = db.query(criteria);

		for(int i = 0; i < custServiceLocations.size(); i++){
			CustServiceLocation location = custServiceLocations.get(i);
			
			String stationName = location.getStation().trim().replace(" ", "_").toUpperCase();
			if(stationName.equals(station)) {
				return location;
			}
		}
		
		return null;
	}
	
	public ArrayList<PowerAlertDao> getPowerAlerts(PowerAlertType type, Pagination pagination, String keyword, Date eventDate){
		Session session = db.getSession();
		
		SettingHelper settingHelper = new SettingHelper(db);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		Date unscheduledRemove = cal.getTime();
		
		cal.setTime(new Date());
		cal.add(Calendar.HOUR, (-1 * settingHelper.getPowerAlertScheduleRemoveHours()));
		Date scheduledRemove = cal.getTime();
		
		Criteria criteria = session.createCriteria(PowerAlertDao.class, "pa");
		criteria.createAlias("powerAlertType", "paType");
		
		criteria.add(Restrictions.eq("pa.deletedFlag", DELETED_FLAG_ACTIVE));
		
		if(type != null){
			criteria.add(Restrictions.eq("paType.name", type.getValue()).ignoreCase());
		}
		
		if(!StringUtil.isEmpty(keyword)){
			String match = "%" + keyword.trim() + "%";
			Criterion cArea = Restrictions.like("pa.area", match).ignoreCase();
			Criterion cCauses = Restrictions.like("pa.causes", match).ignoreCase();
			Criterion cTitle = Restrictions.like("pa.title", match).ignoreCase();
			
			Disjunction cOr = Restrictions.disjunction();
			cOr.add(cArea);
			cOr.add(cCauses);
			cOr.add(cTitle);
			
			criteria.add(cOr);
		}

		// Unscheduled
		Criterion conditionA = Restrictions.and(
				   Restrictions.eq("paType.name", PowerAlertType.OUTAGE_ANNOUNCEMENT.getValue()).ignoreCase(), 
				   Restrictions.ge("pa.restorationDatetime", unscheduledRemove));
		
		// Schedule
		Criterion conditionB = Restrictions.and(
				   Restrictions.eq("paType.name", PowerAlertType.PREVENTIVE_MAINTENANCE.getValue()).ignoreCase(),
				   Restrictions.ge("pa.maintenanceEndDatetime", scheduledRemove));
		
		Disjunction or = Restrictions.disjunction();
		or.add(conditionA);
		or.add(conditionB);
		
		criteria.add(or);
		
		if(eventDate != null){
			Date[] dates = DateUtil.getEffectiveBetweenClause(eventDate);
			Criterion cDatesA = Restrictions.and(
					   Restrictions.eq("paType.name", PowerAlertType.OUTAGE_ANNOUNCEMENT.getValue()).ignoreCase(), 
					   Restrictions.between("pa.restorationDatetime", dates[0], dates[1]));
			
			Criterion cDatesB = Restrictions.and(
					   Restrictions.eq("paType.name", PowerAlertType.PREVENTIVE_MAINTENANCE.getValue()).ignoreCase(),
					   Restrictions.between("pa.maintenanceStartDatetime", dates[0], dates[1]));
			
			Disjunction cDateOr = Restrictions.disjunction();
			cDateOr.add(cDatesA);
			cDateOr.add(cDatesB);
			
			criteria.add(cDateOr);
		}
		
		criteria.setProjection(Projections.count("pa.id"));
		
		long count = db.queryUnique(criteria);
		
		if(count > 0){
			
			pagination.setTotalRecords((int)count);
			criteria.setProjection(null);
			criteria.setResultTransformer(Criteria.ROOT_ENTITY);
			
			if(type != null){
				if(type == PowerAlertType.OUTAGE_ANNOUNCEMENT){
					criteria.addOrder(Order.desc("pa.restorationDatetime"));
				}else if(type == PowerAlertType.PREVENTIVE_MAINTENANCE){
					criteria.addOrder(Order.desc("pa.maintenanceStartDatetime"));
				}
			}else{
				criteria.addOrder(Order.desc("pa.id"));
			}
			
			if(pagination.getPaging() != null){
				pagination.setMaxPage(pagination.calculateMaxPage((int)count, pagination.getPaging().getRecordsPerPage()));
				return query(criteria, pagination);
			}else{
				pagination.setMaxPage(1);
				return db.query(criteria);
			}
		}else{
			return null;
		}
	}
	
	public PowerAlertDao getPowerAlert(long id){
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(PowerAlertDao.class);
		criteria.add(Restrictions.eq("deletedFlag", DELETED_FLAG_ACTIVE));
		criteria.add(Restrictions.eq("id", id));
		
		return db.queryUnique(criteria);
	}
	
	public PowerAlertTypeDao getPowerAlertType(PowerAlertType type){
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(PowerAlertTypeDao.class);
		criteria.add(Restrictions.eq("name", type.getValue()).ignoreCase());
		
		return db.queryUnique(criteria);
	}
	
	public ArrayList<Promotion> getPromotions(Pagination pagination){
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(Promotion.class);
		criteria.add(Restrictions.eq("deletedFlag", DELETED_FLAG_ACTIVE));
		criteria.add(Restrictions.le("publishDateTime", new Date()));
		criteria.add(Restrictions.ge("endDateTime", new Date()));
		
		criteria.setProjection(Projections.count("id"));
		
		long count = db.queryUnique(criteria);
		
		if(count > 0){
			
			pagination.setTotalRecords((int)count);
			criteria.setProjection(null);
			criteria.setResultTransformer(Criteria.ROOT_ENTITY);
			criteria.addOrder(Order.desc("id"));
			if(pagination.getPaging() != null){
				pagination.setMaxPage(pagination.calculateMaxPage((int)count, pagination.getPaging().getRecordsPerPage()));
				return query(criteria, pagination);
			}else{
				pagination.setMaxPage(1);
				return db.query(criteria);
			}
		}else{
			return null;
		}
	}
	
	public Promotion getPromotion(long id){
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(Promotion.class);
		criteria.add(Restrictions.eq("deletedFlag", DELETED_FLAG_ACTIVE));
		criteria.add(Restrictions.eq("id", id));
		
		return db.queryUnique(criteria);
	}
	
	public ArrayList<Faq> getFaqs(String categoryKey){
		return getFaqs(DELETED_FLAG_ACTIVE, categoryKey);
	}

	public ArrayList<Faq> getFaqs(int deletedFlag, String categoryKey){
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(Faq.class, "faq");
		criteria.createAlias("faqCategory", "faqCat");
		
		criteria.add(Restrictions.eq("faq.deletedFlag", deletedFlag));
		criteria.add(Restrictions.eq("faqCat.name", categoryKey));
		criteria.add(Restrictions.eq("faq.status", FaqStatus.ACTIVE));
		criteria.addOrder(Order.asc("faq.faqSequence"));
		criteria.addOrder(Order.asc("faq.id"));
		
		return db.query(criteria);
	}

	public ArrayList<CustServiceCounter> getCustServiceCounters(String mainStation){
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(CustServiceCounter.class, "custC");
		criteria.createAlias("custServiceLocation", "custSL");
		criteria.add(Restrictions.eq("custC.deletedFlag", DELETED_FLAG_ACTIVE));
		criteria.add(Restrictions.eq("custC.status", "a").ignoreCase());
		
		if(!StringUtil.isEmpty(mainStation)){
			criteria.add(Restrictions.eq("custSL.region", mainStation).ignoreCase());
		}
		
		criteria.addOrder(Order.asc("custSL.station"));
		
		return db.query(criteria);
	}
	
	public ArrayList<ContactUs> getContactUs(){
		return getContactUs(DELETED_FLAG_ACTIVE);
	}
	
	public ArrayList<ContactUs> getContactUs(int deletedFlag){
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(ContactUs.class);
		criteria.add(Restrictions.eq("deletedFlag", deletedFlag));
		
		return db.query(criteria);
	}
	
	public ArrayList<FaqCategoryDao> getFaqCategory(){
		return getFaqCategory(DELETED_FLAG_ACTIVE);
	}
	
	public ArrayList<FaqCategoryDao> getFaqCategory(int deletedFlag){

		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(FaqCategoryDao.class);
		criteria.add(Restrictions.eq("deletedFlag", deletedFlag));
		criteria.addOrder(Order.asc("categorySequence"));
		
		return db.query(criteria);
	}
	
	public List<MobilePrefixDAO> getMobilePrefix(){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(MobilePrefixDAO.class);
		return db.query(criteria);
	}
	
	public List<TariffSettingDAO> getTarrifSetting(CalculatorType type){
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(TariffSettingDAO.class, "set");
		criteria.createAlias("tariffCategory", "cat");
		criteria.add(Restrictions.eq("cat.deletedFlag", DELETED_FLAG_ACTIVE));
		criteria.add(Restrictions.eq("cat.name", type.toString()));
		criteria.add(Restrictions.eq("set.deletedFlag", DELETED_FLAG_ACTIVE));

		criteria.addOrder(Order.asc("set.trToUnit"));
		
		return db.query(criteria);
	}
}
