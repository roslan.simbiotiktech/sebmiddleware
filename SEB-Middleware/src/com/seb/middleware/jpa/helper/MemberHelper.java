package com.seb.middleware.jpa.helper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.naming.NamingException;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.DatabaseHelper;
import com.seb.middleware.jpa.dao.CrmUserProfile;
import com.seb.middleware.jpa.dao.RememberMe;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.ldap.LDAPConnector;
import com.seb.middleware.ldap.Person;

public class MemberHelper extends DatabaseHelper{

	public MemberHelper(DatabaseFacade db) {
		super(db);
	}

	public User getUser(String userEmail){
		Criteria criteria = db.getSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("userId", userEmail));
		
		ArrayList<User> users = db.query(criteria);
		if(users != null && !users.isEmpty()){
			return users.get(0);
		}else{
			return null;
		}
	}
	// added by pramod   get CrmUserProfile table data With SfdcID
	
	public CrmUserProfile getCrmUserProfileWithSfdcID(String sfdcId) {		
		Criteria criteria = db.getSession()
				.createCriteria(CrmUserProfile.class)
				.add(Restrictions.eq("sfdcId", sfdcId));
		
		ArrayList<CrmUserProfile> crmUserProfiles = db.query(criteria);
		if(crmUserProfiles != null && !crmUserProfiles.isEmpty()){
			return crmUserProfiles.get(0);
		}else{
			return null;
		}		
	}
	
	public ArrayList<User> getUser(int firstResult, int maxResult){
		Criteria criteria = db.getSession()
				.createCriteria(User.class)
				.addOrder(Order.asc("userId"));
		criteria.setFirstResult(firstResult);
		criteria.setMaxResults(maxResult);
		
		ArrayList<User> users = db.query(criteria);
		return users;
	}
	
	public long getUserCount(){
		Criteria criteria = db.getSession()
				.createCriteria(User.class);
		criteria.setProjection(Projections.rowCount());	
		Long resultCount = (Long)criteria.uniqueResult();
		return resultCount;
	}
	
	public Person getPerson(String userEmail) throws NamingException{
		LDAPConnector ldap = new LDAPConnector();
		return ldap.getUser(userEmail);
	}
	
	public RememberMe getRememberedSession(String clientId){
		Criteria criteria = db.getSession()
				.createCriteria(RememberMe.class)
				.add(Restrictions.eq("clientId", clientId));
		
		ArrayList<RememberMe> remembers = db.query(criteria);
		if(remembers != null && !remembers.isEmpty()){
			return remembers.get(0);
		}else{
			return null;
		}
	}
	
	// added by pramod 
	//get oldeest acount details as per creation date
	public User getEarlestUserAccount(List<String> eamils) {
		User u= new User();
		
		Criteria criteria = db.getSession()
				.createCriteria(User.class)
				.add(Restrictions.in("userId", eamils));
		criteria.setFirstResult(0);
		criteria.setMaxResults(1);
		criteria.addOrder(Order.asc("createdDate"));
		ArrayList<User> earlestUser = db.query(criteria);
		u=earlestUser.get(0);		
		return u;
	}

	

	
	
	public void RememberClient(String clientId, String userEmail) throws DatabaseFacadeException{
		
		RememberMe existingSession = getRememberedSession(clientId);
		
		if(existingSession != null){
			// Delete first
			db.delete(existingSession);
		}
		
		SettingHelper setting = new SettingHelper(db);
		
		RememberMe rmb = new RememberMe();
		rmb.setClientId(clientId);
		rmb.setUserEmail(userEmail);
		if(setting.getRememberMePeriodInMonths() > 0){
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.MONTH, setting.getRememberMePeriodInMonths());
			rmb.setExpiryDate(cal.getTime());
		}else{
			// Unlimited
			rmb.setExpiryDate(null);
		}
		
		db.insert(rmb);
	}
	
	public boolean isRememberedSessionValid(RememberMe rememberMe){
		if(rememberMe == null) return false;
		if(rememberMe.getExpiryDate() == null) return true;
		
		Date now = new Date();
		if(now.compareTo(rememberMe.getExpiryDate()) >= 0){
			return false;
		}else{
			return true;
		}
	}
	
	public ArrayList<String> searchUserWithEmail(String userEmail){
		Criterion existingUserCriterion = Restrictions.and(
				Restrictions.isNotNull("existingEmail"), 
				Restrictions.eq("existingEmail", userEmail).ignoreCase());
		
		Criteria criteria = db.getSession()
				.createCriteria(User.class)
				.setProjection(Projections.property("userId"))
				.add(Restrictions.or(
						Restrictions.eq("userId", userEmail), 
						existingUserCriterion));
		
		ArrayList<String> users = db.query(criteria);
		return users;
	}
}
