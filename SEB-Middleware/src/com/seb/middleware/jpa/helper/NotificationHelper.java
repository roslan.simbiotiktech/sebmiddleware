package com.seb.middleware.jpa.helper;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.seb.middleware.api.services.response.Pagination;
import com.seb.middleware.constant.MessageStatus;
import com.seb.middleware.constant.NotificationType;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.DatabaseHelper;
import com.seb.middleware.jpa.dao.BillReadyNotification;
import com.seb.middleware.jpa.dao.NotificationHistory;
import com.seb.middleware.jpa.dao.ReportNotification;
import com.seb.middleware.jpa.dao.TagNotification;
import com.seb.middleware.jpa.dao.TagSubscriptions;

public class NotificationHelper extends DatabaseHelper{

	public NotificationHelper(DatabaseFacade db) {
		super(db);
	}

	public TagSubscriptions getTagSubscriptions(String email, String tag){
		Criteria criteria = db.getSession()
				.createCriteria(TagSubscriptions.class)
				.add(Restrictions.eq("userEmail", email))
				.add(Restrictions.eq("tagName", tag));
		
		ArrayList<TagSubscriptions> subs = db.query(criteria);
		if(subs == null || subs.isEmpty()){
			return null;
		}else{
			return subs.get(0);
		}
	}
	
	public ArrayList<TagSubscriptions> getTagSubscriptions(String email){
		Criteria criteria = db.getSession()
				.createCriteria(TagSubscriptions.class)
				.add(Restrictions.eq("userEmail", email));
		
		ArrayList<TagSubscriptions> subs = db.query(criteria);
		if(subs == null || subs.isEmpty()){
			return null;
		}else{
			return subs;
		}
	}
	
	public long getUnreadNotificationCount(String email){
		
		Criteria criteria = db.getSession()
				.createCriteria(NotificationHistory.class)
				.add(Restrictions.eq("userEmail", email))
				.add(Restrictions.eq("status", MessageStatus.UNREAD));
		
		criteria.setProjection(Projections.count("notificationId"));
		
		long count = db.queryUnique(criteria);
		return count;
	}
	
	public ArrayList<NotificationHistory> getNotificationHistory(String email, Pagination pagination){
		Criteria criteria = db.getSession()
				.createCriteria(NotificationHistory.class)
				.add(Restrictions.eq("userEmail", email));
		
		criteria.setProjection(Projections.count("notificationId"));
		
		long count = db.queryUnique(criteria);
		if(count > 0){
			pagination.setTotalRecords((int)count);
			criteria.setProjection(null);
			criteria.setResultTransformer(Criteria.ROOT_ENTITY);
			criteria.addOrder(Order.desc("createdDatetime"));
			if(pagination.getPaging() != null){
				pagination.setMaxPage(pagination.calculateMaxPage((int)count, pagination.getPaging().getRecordsPerPage()));
				return query(criteria, pagination);
			}else{
				pagination.setMaxPage(1);
				return db.query(criteria);
			}
		}else{
			return null;
		}
	}
	
	public NotificationHistory getNotificationHistory(String email, long notificationId){
		Criteria criteria = db.getSession()
				.createCriteria(NotificationHistory.class)
				.add(Restrictions.eq("userEmail", email))
				.add(Restrictions.eq("notificationId", notificationId));
		
		ArrayList<NotificationHistory> hist = db.query(criteria);
		if(hist == null || hist.isEmpty()){
			return null;
		}else{
			return hist.get(0);
		}
	}
	
	public void deleteNotificationHistory(String email, long [] notificationIds) throws DatabaseFacadeException{
		
		Criterion ids = null;
		
		if(notificationIds != null && notificationIds.length > 0){
			ArrayList<Long> list = new ArrayList<Long>();
			for(long l : notificationIds){
				list.add(l);
			}
			ids = Restrictions.in("notificationId", list);
		}
		
		
		Criteria criteria = db.getSession()
				.createCriteria(NotificationHistory.class);
		criteria.add(Restrictions.eq("userEmail", email));
		if(ids != null) criteria.add(ids);
		
		ArrayList<NotificationHistory> notifications = db.query(criteria);
		if(notifications != null && !notifications.isEmpty()){
			for(NotificationHistory noti : notifications){
				db.delete(noti);
			}
		}
	}
	
	public ArrayList<ReportNotification> getSuccessReportNotification(long [] notificationIds){
		ArrayList<Long> list = new ArrayList<Long>();
		for(long l : notificationIds){
			list.add(l);
		}
		
		Criteria criteria = db.getSession()
				.createCriteria(ReportNotification.class)
				.add(Restrictions.in("notificationId", list));
		
		ArrayList<ReportNotification> notifications = db.query(criteria);
		return notifications;
	}
	
	public ArrayList<BillReadyNotification> getSuccessBillReadyNotification(long [] notificationIds){
		ArrayList<Long> list = new ArrayList<Long>();
		for(long l : notificationIds){
			list.add(l);
		}
		
		Criteria criteria = db.getSession()
				.createCriteria(BillReadyNotification.class)
				.add(Restrictions.in("notificationId", list));
		
		return db.query(criteria);
	}
	
	public ArrayList<TagNotification> getSuccessTagNotification(long [] notificationIds, NotificationType type){
		ArrayList<Long> list = new ArrayList<Long>();
		for(long l : notificationIds){
			list.add(l);
		}
		
		Criteria criteria = db.getSession()
				.createCriteria(TagNotification.class)
				.add(Restrictions.in("notificationId", list))
				.add(Restrictions.eq("notificationType", type));
		
		ArrayList<TagNotification> notifications = db.query(criteria);
		return notifications;
	}
	
	public ArrayList<TagSubscriptions> getTagSubscribers(String tagName){
		Criteria criteria = db.getSession()
				.createCriteria(TagSubscriptions.class)
				.add(Restrictions.eq("tagName", tagName));
		
		ArrayList<TagSubscriptions> subs = db.query(criteria);
		return subs;
	}
}
