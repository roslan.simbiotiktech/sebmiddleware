package com.seb.middleware.jpa.helper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.seb.middleware.constant.PaymentStatus;
import com.seb.middleware.constant.PaymentType;
import com.seb.middleware.jco.SAP;
import com.seb.middleware.jco.data.PaymentErrorMessage;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.DatabaseHelper;
import com.seb.middleware.jpa.dao.FPXBankDAO;
import com.seb.middleware.jpa.dao.PaymentGatewayDAO;
import com.seb.middleware.jpa.dao.PaymentGatewayParameterDAO;
import com.seb.middleware.jpa.dao.PaymentGatewayUrlDAO;
import com.seb.middleware.jpa.dao.PaymentHistoryDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceDataDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceEntryDAO;
import com.seb.middleware.payment.spg.SPGFacade;
import com.seb.middleware.payment.spg.SPGTransactionType;
import com.seb.middleware.utility.StringUtil;

public class PaymentHelper extends DatabaseHelper {

	private static final Logger logger = LogManager.getLogger(PaymentHelper.class);
	public static final String FPX_GATEWAY_ID = "FPX";
	public static final String MPG_GATEWAY_ID = "MPG";
	public static final String SPG_GATEWAY_ID = "SPG";

	public PaymentHelper(DatabaseFacade db) {
		super(db);
	}

	public PaymentGatewayDAO getSPGPaymentGateway() {
		return getPaymentGateway(SPG_GATEWAY_ID);
	}
	
	public PaymentGatewayDAO getFPXPaymentGateway() {
		return getPaymentGateway(FPX_GATEWAY_ID);
	}
	
	public PaymentGatewayDAO getMPGPaymentGateway() {
		return getPaymentGateway(MPG_GATEWAY_ID);
	}

	public PaymentGatewayDAO getPaymentGateway(String gatewayId) {
		Session session = db.getSession();

		Criteria criteria = session.createCriteria(PaymentGatewayDAO.class).add(Restrictions.eq("gatewayId", gatewayId));

		ArrayList<PaymentGatewayDAO> gateways = db.query(criteria);
		if (gateways != null && !gateways.isEmpty()) {
			return gateways.get(0);
		} else {
			return null;
		}
	}
	
	public PaymentReferenceDAO getPaymentReference(long paymentId) {
		Session session = db.getSession();

		Criteria criteria = session.createCriteria(PaymentReferenceDAO.class);
		criteria.add(Restrictions.eq("paymentId", paymentId));

		ArrayList<PaymentReferenceDAO> gateways = db.query(criteria);
		if (gateways != null && !gateways.isEmpty()) {
			return gateways.get(0);
		} else {
			return null;
		}
	}

	public PaymentReferenceDAO getPaymentReference(PaymentGatewayDAO gateway, String prefixedReferenceNo) {
		return getPaymentReference(gateway, null, prefixedReferenceNo);
	}
	
	public PaymentReferenceDAO getPaymentReference(PaymentGatewayDAO gateway, String email, String prefixedReferenceNo) {

		String referenceNo = StringUtil.getNumericFromRight(prefixedReferenceNo);
		
		logger.debug("reference no :" + referenceNo);
		
		try{
			Session session = db.getSession();
			
			long ref = Long.parseLong(referenceNo);
			Criteria criteria = session.createCriteria(PaymentReferenceDAO.class);
			criteria.add(Restrictions.eq("gatewayId", gateway.getGatewayId()));
			criteria.add(Restrictions.eq("paymentId", ref));
			
			if(!StringUtil.isEmpty(email)){
				criteria.add(Restrictions.eq("userEmail", email));	
			}
			
			ArrayList<PaymentReferenceDAO> references = db.query(criteria);
			if(references != null && !references.isEmpty()){
				return references.get(0);
			}else{
				return null;
			}
			
		}catch(NumberFormatException e){
			logger.error("error converting reference no - " + e.getMessage(), e);
			return null;
		}
	}
	
	public List<PaymentReferenceEntryDAO> getPaymentEntries(long paymentId) {

		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(PaymentReferenceEntryDAO.class);
		criteria.add(Restrictions.eq("paymentId", paymentId));
					
		return db.query(criteria);
	}

	public Map<String, String> getPaymentParameters(String gatewayId) {
		Session session = db.getSession();

		Criteria criteria = session.createCriteria(PaymentGatewayParameterDAO.class).add(Restrictions.eq("gatewayId", gatewayId));

		ArrayList<PaymentGatewayParameterDAO> params = db.query(criteria);

		Map<String, String> param = new HashMap<>();

		for (PaymentGatewayParameterDAO p : params) {
			param.put(p.getParamName(), p.getParamValue());
		}

		return param;
	}

	public String getPaymentUrl(String gatewayId, String urlName) {
		Session session = db.getSession();

		Criteria criteria = session.createCriteria(PaymentGatewayUrlDAO.class)
				.add(Restrictions.eq("gatewayId", gatewayId))
				.add(Restrictions.eq("urlName", urlName));

		ArrayList<PaymentGatewayUrlDAO> urls = db.query(criteria);
		return urls.get(0).getUrlValue();
	}

	public boolean isFPXBankExist(String bankId) {
		return getFPXBank(bankId) != null;
	}

	public FPXBankDAO getFPXBank(String bankId) {

		Session session = db.getSession();

		Criteria criteria = session.createCriteria(FPXBankDAO.class).add(Restrictions.eq("bankId", bankId));

		ArrayList<FPXBankDAO> fpxBanks = db.query(criteria);
		if (fpxBanks != null && !fpxBanks.isEmpty()) {
			return fpxBanks.get(0);
		} else {
			return null;
		}
	}
	
	public Map<String, String> getPaymentReferenceData(long referenceNo){
		Session session = db.getSession();

		Criteria criteria = session.createCriteria(PaymentReferenceDataDAO.class)
				.add(Restrictions.eq("paymentId", referenceNo));
		
		ArrayList<PaymentReferenceDataDAO> datas = db.query(criteria);
		
		Map<String, String> map = new HashMap<>();
		for(PaymentReferenceDataDAO data : datas){
			map.put(data.getName(), data.getValue());
		}
		return map;
	}
	
	public void billTransaction(PaymentReferenceDAO reference, 
			List<PaymentReferenceEntryDAO> entries, SettingHelper settingHelper) throws DatabaseFacadeException{

		String referenceNumber = SPGFacade.getReferenceNo(reference);
		
		boolean allBilled = true;
		
		String fpxReconKey = settingHelper.getPaymentReconKeyFPX();
		String otherReconKey = settingHelper.getPaymentReconKeyOther();
		
		for(PaymentReferenceEntryDAO entry : entries){
			
			if(!entry.isBilled()){
				
				String entryReferenceNumber = referenceNumber + "_" + entry.getPaymentEntryId();
				
				entry.setBillingAttemptCount(entry.getBillingAttemptCount() + 1);
				entry.setBillingAttemptLastDatetime(new Date());
				
				try{
					SAP sap = new SAP(settingHelper.getSAPTimeoutSeconds());
					PaymentErrorMessage error = sap.updatePaymentStatus(reference, 
							entryReferenceNumber, entry, getPaymentType(reference),
							fpxReconKey, otherReconKey);
					
					if(error == null){
						entry.setBilled(true);
					}else{
						allBilled = false;
						entry.setBillingAttemptLastError(error.toString());
					}	
				}catch(Exception e){
					allBilled = false;
					entry.setBillingAttemptLastError(e.getMessage());
					logger.error("error updating status into SAP - " + e.getMessage(), e);
				}
			}
		}
		
		if(allBilled){
			reference.setStatus(PaymentStatus.BILLED);
		}
	}
	
	public List<PaymentHistoryDAO> getPayment(String contractAccountNumber, int limit, PaymentStatus ... statuses){
		
		Session session = db.getSession();
		
		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append("  pre.payment_entry_id, pr.payment_id, pr.gateway_id, pr.status, pr.user_email, ");
		sb.append("  pr.notification_email, pr.is_mobile, pr.gateway_ref, pr.gateway_status, pr.external_gateway_status, ");
		sb.append("  pr.payment_datetime, pr.created_datetime, pre.contract_account_number, pre.amount ");
		sb.append("from ");
		sb.append("  seb.payment_reference pr join ");
		sb.append("  seb.payment_reference_entry pre on pr.payment_id = pre.payment_id ");
		sb.append("where ");
		sb.append("  pre.contract_account_number = :contractAccountNumber and ");
		sb.append("  pr.status in (");
		for(int i = 0; i < statuses.length; i++){
			PaymentStatus ps = statuses[i];
			sb.append("'").append(ps.toString()).append("'");
			
			if(i+1 < statuses.length){
				sb.append(",");
			}
		}
		sb.append(" ) ");
		
		sb.append("order by pr.created_datetime desc");
		
		SQLQuery query = session.createSQLQuery(sb.toString());
		query.setParameter("contractAccountNumber", contractAccountNumber);
		query.addEntity(PaymentHistoryDAO.class);
		query.setFirstResult(0);
		query.setMaxResults(limit);
		
		return db.query(query);
	}
	
	public List<PaymentReferenceDAO> getPendingPayment(int minutes){
		
		Session session = db.getSession();
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.MINUTE, (minutes * -1));
		Date start = cal.getTime();
		cal.setTime(new Date());
		cal.add(Calendar.MINUTE, -10);
		Date end = cal.getTime();
		
		logger.debug("Start : " + start);
		logger.debug("End : " + end);
		
		Criterion c1 = Restrictions.eq("status", PaymentStatus.PENDING);
		Criterion c2 = Restrictions.between("createdDate", start, end);
		
		Criterion pending = Restrictions.and(c1, c2);
		Criterion pendingOrPenAuth = Restrictions.or(pending, Restrictions.eq("status", PaymentStatus.PENAUTH));
		
		Criteria criteria = session.createCriteria(PaymentReferenceDAO.class)
				.add(pendingOrPenAuth);
		
		return db.query(criteria);
	}
	
	public List<PaymentReferenceDAO> getUnbillPayment(int sapTimeout){
		
		/*
		 * Added sap timeout in query to avoid double post transaction. 
		 * (E.g. when primary transaction is running concurrently with Biller thread)
		 */
		int sapTimeoutThreshold = sapTimeout * 2;
		
		Session session = db.getSession();
		
		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append("  distinct pr.* ");
		sb.append("from ");
		sb.append("  seb.payment_reference pr join seb.payment_reference_entry pre ");
		sb.append("    on pr.payment_id = pre.payment_id join ");
		sb.append("  seb.billing_attempt_matrix bam ");
		sb.append("    on pre.billing_attempt_count between bam.attempt_from and bam.attempt_to ");
		sb.append("where ");
		sb.append("  pr.status = 'RECEIVED' and ");
		sb.append("  (");
		sb.append("    (");
		sb.append("      pre.billing_attempt_last_datetime IS NULL ");
		sb.append("      and ");
		sb.append("      pr.payment_datetime IS NOT NULL ");
		sb.append("      and ");
		sb.append("      timestampdiff(2, current_timestamp-pr.payment_datetime) > " + sapTimeoutThreshold);
		sb.append("    )");
		sb.append("    or ");
		sb.append("    timestampdiff(4, current_timestamp-pre.billing_attempt_last_datetime) >= bam.retry_minutes");
		sb.append("  )");
		
		SQLQuery query = session.createSQLQuery(sb.toString());
		query.addEntity(PaymentReferenceDAO.class);
		return db.query(query);
	}
	
	public List<PaymentReferenceDAO> getAllUnbillPayment(int sapTimeout){
		
		/*
		 * Added sap timeout in query to avoid double post transaction. 
		 * (E.g. when primary transaction is running concurrently with Biller thread)
		 */
		int sapTimeoutThreshold = sapTimeout * (-2);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.SECOND, sapTimeoutThreshold);
		
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(PaymentReferenceDAO.class)
				.add(Restrictions.eq("status", PaymentStatus.RECEIVED))
				.add(Restrictions.isNotNull("paymentDate"))
				.add(Restrictions.gt("paymentDate", cal.getTime()));
		
		return db.query(criteria);
	}
	
	private PaymentType getPaymentType(PaymentReferenceDAO reference){
		if(reference.getGatewayId().equals(FPX_GATEWAY_ID)){
			return PaymentType.FPX;
		}else if(reference.getGatewayId().equals(MPG_GATEWAY_ID)){
			return PaymentType.CARD;
		}else if(reference.getGatewayId().equals(SPG_GATEWAY_ID)){
			
			SPGTransactionType spgTransactionType = SPGFacade.getTransactionType(reference);
			if(spgTransactionType == SPGTransactionType.FPX){
				return PaymentType.FPX;
			}else{
				return PaymentType.CARD;
			}
		}else{
			logger.error("invalid gateway id - " + reference.getGatewayId());
			return null;
		}
	}
}
