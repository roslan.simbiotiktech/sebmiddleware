package com.seb.middleware.jpa.helper;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.DatabaseHelper;
import com.seb.middleware.jpa.dao.CaseReport;
import com.seb.middleware.jpa.dao.CombinedReport;
import com.seb.middleware.jpa.dao.SFDCReport;
import com.seb.middleware.utility.StringUtil;

public class ReportHelper extends DatabaseHelper{

	private int DELETED_FLAG_ACTIVE = 0;
	
	public ReportHelper(DatabaseFacade db) {
		super(db);
	}	

	public void deleteReportHistory(String email, List<Long> transIds) throws DatabaseFacadeException{
		
		Criteria criteria = db.getSession()
				.createCriteria(CaseReport.class)
				.add(Restrictions.eq("userEmail", email))
				.add(Restrictions.in("transId", transIds))
				.add(Restrictions.eq("deletedFlag", DELETED_FLAG_ACTIVE));;
		
		ArrayList<CaseReport> reports = db.query(criteria);
		if(reports != null && !reports.isEmpty()){
			for(CaseReport report : reports){
				report.setDeletedFlag(1);
				db.update(report);
			}
		}
	}
	
	public CaseReport getReportDetail(String email, long transId){
		Criteria criteria = db.getSession()
				.createCriteria(CaseReport.class)
				.add(Restrictions.eq("userEmail", email))
				.add(Restrictions.eq("transId", transId))
				.add(Restrictions.eq("deletedFlag", DELETED_FLAG_ACTIVE));
		
		ArrayList<CaseReport> caseReport = db.query(criteria);
		if(caseReport != null && !caseReport.isEmpty()){
			return caseReport.get(0);
		}else{
			return null;
		}
	}
	
	public ArrayList<CaseReport> getReportDetail(String email){
		Criteria criteria = db.getSession()
				.createCriteria(CaseReport.class)
				.add(Restrictions.eq("userEmail", email))
				.add(Restrictions.eq("deletedFlag", DELETED_FLAG_ACTIVE))
				.addOrder(Order.desc("createdDatetime"));
		
		return db.query(criteria);
	}
	
	
	/*
	 * SFDC Report
	 */
	public void deleteSfdcReportHistory(String email, List<Long> transIds) throws DatabaseFacadeException{
		
		Criteria criteria = db.getSession()
				.createCriteria(SFDCReport.class)
				.add(Restrictions.eq("userEmail", email))
				.add(Restrictions.in("transId", transIds))
				.add(Restrictions.eq("deletedFlag", DELETED_FLAG_ACTIVE));;
		
		ArrayList<SFDCReport> reports = db.query(criteria);
		if(reports != null && !reports.isEmpty()){
			for(SFDCReport report : reports){
				report.setDeletedFlag(1);
				db.update(report);
			}
		}
	}
	
	public SFDCReport getSfdcReportDetail(String email, long transId){
		Criteria criteria = db.getSession()
				.createCriteria(SFDCReport.class)
				.add(Restrictions.eq("userEmail", email))
				.add(Restrictions.eq("transId", transId))
				.add(Restrictions.eq("deletedFlag", DELETED_FLAG_ACTIVE));
		
		ArrayList<SFDCReport> caseReport = db.query(criteria);
		if(caseReport != null && !caseReport.isEmpty()){
			return caseReport.get(0);
		}else{
			return null;
		}
	}
	
	public ArrayList<SFDCReport> getSfdcReportDetail(String email, String caseNumber){
		Criteria criteria = db.getSession()
				.createCriteria(SFDCReport.class)
				.add(Restrictions.eq("userEmail", email))
				.add(Restrictions.eq("deletedFlag", DELETED_FLAG_ACTIVE))
				.addOrder(Order.desc("createdDatetime"));
		
		if(!StringUtil.isEmpty(caseNumber)) {
			criteria.add(Restrictions.like("caseNumber", "%" + caseNumber + "%"));
		}
		
		return db.query(criteria);
	}
	
	public List<CombinedReport> getCombinedReport(String email){
		Criteria criteria = db.getSession()
				.createCriteria(CombinedReport.class)
				.add(Restrictions.eq("userEmail", email))
				.add(Restrictions.eq("deletedFlag", DELETED_FLAG_ACTIVE))
				.addOrder(Order.desc("createdDatetime"));
			
		return db.query(criteria);
	}
	
	public List<CombinedReport> getCombinedReportByCaseNumber(String caseNumber){
		Criteria criteria = db.getSession()
				.createCriteria(CombinedReport.class)
				.add(Restrictions.eq("caseNumber", caseNumber))
				.add(Restrictions.eq("deletedFlag", DELETED_FLAG_ACTIVE))
				.addOrder(Order.desc("createdDatetime"));
			
		return db.query(criteria);
	}
	
	public SFDCReport getSfdcReportDetail(String caseId){
		Criteria criteria = db.getSession()
				.createCriteria(SFDCReport.class)
				.add(Restrictions.eq("caseId", caseId));
		
		return db.queryUnique(criteria);
	}
	
}
