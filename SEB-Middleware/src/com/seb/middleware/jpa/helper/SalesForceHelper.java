package com.seb.middleware.jpa.helper;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseHelper;
import com.seb.middleware.jpa.dao.Contract;
import com.seb.middleware.jpa.dao.SubscriptionMapping;
import com.seb.middleware.jpa.dao.User;

public class SalesForceHelper extends DatabaseHelper {

	public SalesForceHelper(DatabaseFacade db) {
		super(db);
	}	

	public List<User> getUnsyncedUsers(){
		Criteria criteria = db.getSession()
				.createCriteria(User.class);
		
		Criterion notUpToDate = Restrictions.and(
				Restrictions.isNotNull("sfdcId"), 
				Restrictions.gtProperty("sfdcUpdatedDate", "sfdcSyncDate"));
		
		criteria.add(Restrictions.or(Restrictions.isNull("sfdcId"), notUpToDate));
		
		return db.query(criteria);
	}
	
	public List<Contract> getUnsyncedContracts(){
		Criteria criteria = db.getSession()
				.createCriteria(Contract.class);
		
		criteria.add(Restrictions.isNull("sfdcId"));
		
		return db.query(criteria);
	}
	
	public List<SubscriptionMapping> getUnsyncedSubscriptions(){
		Session session = db.getSession();
		
		StringBuilder sb = new StringBuilder();
		sb.append("select ");
		sb.append("  s.subs_id, u.sfdc_id as account_sfdc_id, c.sfdc_id as contract_sfdc_id ");
		sb.append("from ");
		sb.append("  seb.subscription s join ");
		sb.append("  seb.contract c on s.contract_account_number = c.contract_account_number join ");
		sb.append("  seb.user u on s.user_email = u.user_id ");
		sb.append("where ");
		sb.append("  c.sfdc_id is not null and u.sfdc_id is not null and ");
		sb.append("  (s.sfdc_id is null or (s.sfdc_id is not null and s.sfdc_updated_datetime > s.sfdc_sync_datetime) )");
		
		String sql = sb.toString();
		
		SQLQuery query = session.createSQLQuery(sql);
		query.addEntity(SubscriptionMapping.class);
		return db.query(query);
	}
}

