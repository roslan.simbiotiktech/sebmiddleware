package com.seb.middleware.jpa.helper;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseHelper;
import com.seb.middleware.jpa.dao.Setting;

public class SettingHelper extends DatabaseHelper {

	public SettingHelper(DatabaseFacade db) {
		super(db);
	}

	public String get(String settingKey){
		Criteria criteria = db.getSession()
				.createCriteria(Setting.class)
				.add(Restrictions.eq("settingKey", settingKey));
		
		ArrayList<Setting> setting = db.query(criteria);
		if(setting == null || setting.isEmpty()){
			throw new RuntimeException("The requested setting '" + settingKey + "' was not found.");
		}else{
			return setting.get(0).getSettingValue();
		}
	}
	
	private static final String TESTING_ENVIRONMENT = "TESTING_ENVIRONMENT";
	public boolean isTestingEnvironment(){
		String value = get(TESTING_ENVIRONMENT);
		return Boolean.parseBoolean(value);
	}
	
	private static final String EMAIL_SENDER_ADDRESS = "EMAIL_SENDER_ADDRESS";
	public String getEmailSenderAddress(){
		return get(EMAIL_SENDER_ADDRESS);
	}
	
	private static final String ACTIVATION_CODE_EXPIRE_MINUTES = "ACTIVATION_CODE_EXPIRE_MINUTES"; // Combine all Activation Code
	public int getActivationCodeExpirationInMinutes(){
		String value = get(ACTIVATION_CODE_EXPIRE_MINUTES);
		return Integer.parseInt(value);
	}

	public int getEmailActivationCodeExpirationInMinutes(){
		String value = get(ACTIVATION_CODE_EXPIRE_MINUTES);
		return Integer.parseInt(value);
	}

	public int getMobileActivationCodeExpirationInMinutes(){
		String value = get(ACTIVATION_CODE_EXPIRE_MINUTES);
		return Integer.parseInt(value);
	}

	private static final String ACTIVATION_CODE_REQUEST_WAIT_MINUTES = "ACTIVATION_CODE_REQUEST_WAIT_MINUTES";
	public int getActivationCodeRequestWaitInMinutes(){
		String value = get(ACTIVATION_CODE_REQUEST_WAIT_MINUTES);
		return Integer.parseInt(value);
	}
	
	private static final String SESSION_REMEMBER_ME_MONTHS = "SESSION_REMEMBER_ME_MONTHS";
	public int getRememberMePeriodInMonths(){
		String value = get(SESSION_REMEMBER_ME_MONTHS);
		return Integer.parseInt(value);
	}
	
	private static final String STATEMENT_ROOT_PATH = "STATEMENT_ROOT_PATH";
	public String getStatementRootPath(){
		return get(STATEMENT_ROOT_PATH);
	}
	
	private static final String NOTIFICATION_SEND_PATIENT_MINUTES = "NOTIFICATION_SEND_PATIENT_MINUTES";
	public int getNotificationSendPatientInMinutes(){
		String value = get(NOTIFICATION_SEND_PATIENT_MINUTES);
		return Integer.parseInt(value);
	}
	
	private static final String SMS_USER_ID = "SMS_USER_ID";
	public String getSmsUserId(){
		return get(SMS_USER_ID);
	}
	
	private static final String SMS_USER_PASSWORD = "SMS_USER_PASSWORD";
	public String getSmsUserPassword(){
		return get(SMS_USER_PASSWORD);
	}
	
	private static final String SMS_USER_ID_ISENTRIC = "SMS_USER_ID_ISENTRIC";
	public String getSmsUserIdIsentric(){
		return get(SMS_USER_ID_ISENTRIC);
	}
	
	private static final String SMS_USER_ID_ISENTRIC_TYPE_BILL = "SMS_USER_ID_ISENTRIC_TYPE_BILL";
	public String getSmsUserIdIsentricTypeBIll(){
		return get(SMS_USER_ID_ISENTRIC_TYPE_BILL);
	}
	
	private static final String CASE_REPORT_CHECK_SECOND = "CASE_REPORT_CHECK_SECOND";
	public int getCaseReportCheckInSeconds(){
		String value = get(CASE_REPORT_CHECK_SECOND);
		return Integer.parseInt(value);
	}
	
	private static final String PAYMENT_HISTORY_LISTING_ENTRIES = "PAYMENT_HISTORY_LISTING_ENTRIES";
	public int getPaymentHistoryListingEntries(){
		String value = get(PAYMENT_HISTORY_LISTING_ENTRIES);
		return Integer.parseInt(value);
	}
	
	private static final String STATEMENT_LISTING_ENTRIES = "STATEMENT_LISTING_ENTRIES";
	public int getStatemetnListingEntries(){
		String value = get(STATEMENT_LISTING_ENTRIES);
		return Integer.parseInt(value);
	}
	
	private static final String CONTRACT_MAX_SUBSCRIPTION = "CONTRACT_MAX_SUBSCRIPTION";
	public int getContractMaxSubscriptions(){
		String value = get(CONTRACT_MAX_SUBSCRIPTION);
		return Integer.parseInt(value);
	}
	
	private static final String POWER_ALERT_UNSCHEDULE_REMOVE_HOURS = "POWER_ALERT_UNSCHEDULE_REMOVE_HOURS";
	@Deprecated
	public int getPowerAlertUnscheduleRemoveHours(){
		String value = get(POWER_ALERT_UNSCHEDULE_REMOVE_HOURS);
		return Integer.parseInt(value);
	}
	
	private static final String POWER_ALERT_SCHEDULE_REMOVE_HOURS = "POWER_ALERT_SCHEDULE_REMOVE_HOURS";
	public int getPowerAlertScheduleRemoveHours(){
		String value = get(POWER_ALERT_SCHEDULE_REMOVE_HOURS);
		return Integer.parseInt(value);
	}
	
	private static final String SECRET = "SECRET";
	public String getSecret(){
		return get(SECRET);
	}
	
	private static final String SAP_FILE_PROCESSING_NOTIFY_RECIPIENTS = "SAP_FILE_PROCESSING_NOTIFY_RECIPIENTS";
	public String getSAPFileProcessingNotifyRecipients(){
		return get(SAP_FILE_PROCESSING_NOTIFY_RECIPIENTS);
	}
	
	private static final String IMAGE_MAX_BYTES = "IMAGE_MAX_BYTES";
	public long getImageMaxAllowedBytes(){
		String value = get(IMAGE_MAX_BYTES);
		return Long.parseLong(value);
	}
	
	private static final String FAQ_URL = "FAQ_URL";
	public String getFaqUrl(){
		return get(FAQ_URL);
	}
	
	private static final String SAP_TIMEOUT_SECONDS = "SAP_TIMEOUT_SECONDS";
	public int getSAPTimeoutSeconds(){
		String value = get(SAP_TIMEOUT_SECONDS);
		return Integer.parseInt(value);
	}
	
	private static final String CHECK_PENDING_PAYMENT_EXPIRE_MINUTE = "CHECK_PENDING_PAYMENT_EXPIRE_MINUTE";
	public int getCheckPendingPaymentExpireMinute(){
		String value = get(CHECK_PENDING_PAYMENT_EXPIRE_MINUTE);
		return Integer.parseInt(value);
	}
	
	private static final String SMS_0_ENABLE = "SMS_0_ENABLE";
	public boolean isSMS0Enabled(){
		String value = get(SMS_0_ENABLE);
		return Boolean.parseBoolean(value);
	}
	
	private static final String SMS_1_ENABLE = "SMS_1_ENABLE";
	public boolean isSMS1Enabled(){
		String value = get(SMS_1_ENABLE);
		return Boolean.parseBoolean(value);
	}
	
	private static final String PAYNOW_URL = "PAYNOW_URL";
	public String getPaynowUrl(){
		return get(PAYNOW_URL);
	}
	
	private static final String PAYMENY_RECON_KEY_FPX = "PAYMENY_RECON_KEY_FPX";
	public String getPaymentReconKeyFPX(){
		String key = get(PAYMENY_RECON_KEY_FPX); 
		if(key == null) return "";
		else return key;
	}
	
	private static final String PAYMENY_RECON_KEY_OTHER = "PAYMENY_RECON_KEY_OTHER";
	public String getPaymentReconKeyOther(){
		String key = get(PAYMENY_RECON_KEY_OTHER); 
		if(key == null) return "";
		else return key;
	}
	
	private static final String PAYMENT_CUTOFF_START = "PAYMENT_CUTOFF_START";
	public String getPaymentCutOffStart(){
		return get(PAYMENT_CUTOFF_START);
	}
	
	private static final String PAYMENT_CUTOFF_END = "PAYMENT_CUTOFF_END";
	public String getPaymentCutOffEnd(){
		return get(PAYMENT_CUTOFF_END);
	}
	
	private static final String SF_INSTANCE_URL = "SF_INSTANCE_URL";
	public String getSalesForceInstanceUrl(){
		return get(SF_INSTANCE_URL);
	}
	
	private static final String SF_CLIENT_ID = "SF_CLIENT_ID";
	public String getSalesForceClientId(){
		return get(SF_CLIENT_ID);
	}
	
	private static final String SF_CLIENT_SECRET = "SF_CLIENT_SECRET";
	public String getSalesForceClientSecret(){
		return get(SF_CLIENT_SECRET);
	}
	
	private static final String SF_USERNAME = "SF_USERNAME";
	public String getSalesForceUsername(){
		return get(SF_USERNAME);
	}
	
	private static final String SF_PASSWORD = "SF_PASSWORD";
	public String getSalesForcePassword(){
		return get(SF_PASSWORD);
	}
	
	private static final String SF_API_CLIENT_ID = "SF_API_CLIENT_ID";
	public String getSalesForceApiClientId(){
		return get(SF_API_CLIENT_ID);
	}
	
	private static final String SF_API_CLIENT_SECRET = "SF_API_CLIENT_SECRET";
	public String getSalesForceApiClientSecret(){
		return get(SF_API_CLIENT_SECRET);
	}
	
	private static final String SF_ACCOUNT_RECORDTYPEID = "SF_ACCOUNT_RECORDTYPEID";
	public String getSalesForceAccountRecordTypeId(){
		return get(SF_ACCOUNT_RECORDTYPEID);
	}
	
	private static final String SF_CASE_RECORDTYPEID = "SF_CASE_RECORDTYPEID";
	public String getSalesForceCaseRecordTypeId(){
		return get(SF_CASE_RECORDTYPEID);
	}
}
