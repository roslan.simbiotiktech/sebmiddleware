package com.seb.middleware.jpa.helper;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.seb.middleware.constant.SubscriptionStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.DatabaseHelper;
import com.seb.middleware.jpa.dao.BillingTransaction;
import com.seb.middleware.jpa.dao.Subscription;

public class SubscriptionHelper extends DatabaseHelper{

	private static final Logger logger = LogManager.getLogger(SubscriptionHelper.class);
	
	public SubscriptionHelper(DatabaseFacade db) {
		super(db);
	}
	
	public Subscription addSubscription(Subscription subscription) throws DatabaseFacadeException{
		Subscription existingSub = getSubscription(subscription.getContractAccNo(), subscription.getUserEmail(), null);
		if(existingSub != null){
			logger.info("Deleting a duplicate subscription. Posibiliy due to subscribing an unsubbed contract");
			db.delete(existingSub);
			db.flush();
		}else{
			logger.debug("no duplicate, proceed to insert.");
		}
		
		Subscription sub = db.insert(subscription);
		return sub;
	}
	
	public ArrayList<Subscription> getSubscriptions(String userEmail){
		return getSubscriptions(userEmail, SubscriptionStatus.ACTIVE);
	}

	public ArrayList<Subscription> getSubscriptions(String userEmail, SubscriptionStatus status){
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(com.seb.middleware.jpa.dao.Subscription.class);
		criteria.add(Restrictions.eq("userEmail", userEmail));
		if(status != null){
			criteria.add(Restrictions.eq("status", status));	
		}
		return db.query(criteria);
	}
	
	public ArrayList<Subscription> getSubscriptionsByContract(String contractAccountNo){
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(com.seb.middleware.jpa.dao.Subscription.class);
		criteria.add(Restrictions.eq("contractAccNo", contractAccountNo));
		criteria.add(Restrictions.eq("status", SubscriptionStatus.ACTIVE));	
		
		return db.query(criteria);
	}
	
	public com.seb.middleware.jpa.dao.Contract getContractAccount(String contractAccNo){
		
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(com.seb.middleware.jpa.dao.Contract.class);
		criteria.add(Restrictions.eq("contractAccountNumber", contractAccNo));
		
		return db.queryUnique(criteria);
	}
	
	public com.seb.middleware.jpa.dao.Contract getContractAccount(String contractAccNo, String contractAccName){
		
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(com.seb.middleware.jpa.dao.Contract.class);
		criteria.add(Restrictions.eq("contractAccountNumber", contractAccNo));
		criteria.add(Restrictions.eq("contractAccountName", contractAccName).ignoreCase());
		
		return db.queryUnique(criteria);
	}
	
	public boolean isContractAccountCanSubscribe(int maxSubscribe, String contractAccNo){
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(com.seb.middleware.jpa.dao.Subscription.class);
		criteria.add(Restrictions.eq("contractAccNo", contractAccNo));
		criteria.add(Restrictions.eq("status", SubscriptionStatus.ACTIVE));
		
		ArrayList<com.seb.middleware.jpa.dao.Subscription> subscriptions = db.query(criteria);
		
		if(subscriptions.size() >= maxSubscribe)
			return false;
		
		return true;
	}
	
	public boolean isSubscriptionExist(String contractAccNo, String userEmail){
		
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(com.seb.middleware.jpa.dao.Subscription.class);
		criteria.add(Restrictions.eq("contractAccNo", contractAccNo));
		criteria.add(Restrictions.eq("userEmail", userEmail));
		criteria.add(Restrictions.eq("status", SubscriptionStatus.ACTIVE));
		
		ArrayList<com.seb.middleware.jpa.dao.Subscription> subscriptions = db.query(criteria);
		
		if(subscriptions.size() > 0)
			return true;
		
		return false;
	}
	
	@Deprecated
	public BigDecimal getSubscriptionLatestAmtDue(String contractAccNo){
		
		BigDecimal amtDue = new BigDecimal(0);
		
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(BillingTransaction.class);
		criteria.add(Restrictions.eq("contractAccountNumber", contractAccNo));
		criteria.addOrder(Order.desc("bilingId"));
		criteria.setMaxResults(1);
		
		ArrayList<BillingTransaction> billingTransactions = db.query(criteria);
		
		if(billingTransactions.size() > 0){
			amtDue = billingTransactions.get(0).getCurrentAmount();
		}
		
		return amtDue;
	}
	
	public BillingTransaction getBillingTransaction(String contractAccNo, Date from, Date to){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(BillingTransaction.class);
		criteria.add(Restrictions.eq("contractAccountNumber", contractAccNo));
		criteria.add(Restrictions.between("invoiceDate", from, to));
		criteria.addOrder(Order.desc("bilingId"));
		
		ArrayList<BillingTransaction> billingTransactions = db.query(criteria);
		
		if(billingTransactions.size() > 0){
			return billingTransactions.get(0);
		}else{
			return null;
		}
	}
	
	public List<BillingTransaction> getNewBillingTransaction(Date createdDate){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(BillingTransaction.class);
		criteria.add(Restrictions.gt("createdDate", createdDate));
		
		return db.query(criteria);
	}
	
	public ArrayList<String> getAvailableStatementMonths(String contractAccNo, int maxRecord){
		Session session = db.getSession();
		StringBuilder sql = new StringBuilder();
		sql.append("select max(year(invoiceDate)) as Year, max(month(invoiceDate)) as Month from ");
		sql.append(BillingTransaction.class.getSimpleName());
		sql.append(" where contractAccountNumber = :contractAccNo");
		sql.append(" group by year(invoiceDate), month(invoiceDate) order by Year desc, Month desc");
		
		Query query = session.createQuery(sql.toString());
		query.setParameter("contractAccNo", contractAccNo);
		query.setMaxResults(maxRecord);
		
		@SuppressWarnings("unchecked")
		List<Object[]> rows = query.list();
		
		ArrayList<String> months = new ArrayList<>();
		for (Object[] row: rows) {
			Object year = row[0];
			Object month = row[1];
			
			if(year != null && month != null){
				int y = (int)year;
				int m = (int)month;
				String date = String.format("%04d%02d", y, m);
				months.add(date);
			}
		}
		
		return months;
	}
	
	@Deprecated
	public ArrayList<BillingTransaction> getRecentBillingTransactions(String contractAccNo, int maxRecords){
		Session session = db.getSession();
		Criteria criteria = session.createCriteria(BillingTransaction.class);
		criteria.add(Restrictions.eq("contractAccountNumber", contractAccNo));
		criteria.addOrder(Order.desc("bilingId"));
		
		if(maxRecords > 0){
			criteria.setMaxResults(maxRecords);	
		}
		
		ArrayList<BillingTransaction> billingTransactions = db.query(criteria);
		
		return billingTransactions;
	}
	
	
	public com.seb.middleware.jpa.dao.Subscription getSubscription(String contractAccNo, String userEmail, SubscriptionStatus status){
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(com.seb.middleware.jpa.dao.Subscription.class);
		criteria.add(Restrictions.eq("contractAccNo", contractAccNo));
		criteria.add(Restrictions.eq("userEmail", userEmail));
		if(status != null)
			criteria.add(Restrictions.eq("status", status));
		
		return db.queryUnique(criteria);
	}
	
	public com.seb.middleware.jpa.dao.Subscription getSubscription(String contractAccNo, String userEmail){
		return getSubscription(contractAccNo, userEmail, SubscriptionStatus.ACTIVE);
	}
	
	public BillingTransaction getSubscriptionDetailLatest(String contractAccNo){
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(BillingTransaction.class);
		criteria.add(Restrictions.eq("contractAccountNumber", contractAccNo));
		criteria.addOrder(Order.desc("bilingId"));
		criteria.setMaxResults(1);
		
		return db.queryUnique(criteria);
	}
	
	public Map<Date, BigDecimal> getConsumption(String contractAccNo){
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(BillingTransaction.class);
		criteria.add(Restrictions.eq("contractAccountNumber", contractAccNo));
		criteria.addOrder(Order.desc("bilingId"));
		
		Map<Date, BigDecimal> map = new HashMap<Date, BigDecimal>();
		
		List<BillingTransaction> list = db.query(criteria);
		if(list != null && !list.isEmpty()){
			
			for(BillingTransaction t : list){
				if(!map.containsKey(t.getInvoiceDate())){
					map.put(t.getInvoiceDate(), t.getCurrentAmount());
				}
			}
		}
		
		return map;
	}
	
	public ArrayList<String> getSubscribersIds(String contractAccNo){
		Criteria criteria = db.getSession()
				.createCriteria(Subscription.class)
				.setProjection(Projections.distinct(Projections.property("userEmail")))
				.add(Restrictions.eq("contractAccNo", contractAccNo));
		
		return db.query(criteria);
	}
	
	public String getInvoicePath(String rootPath, String fileName, String contractAccNumber, String invoiceYear){
		StringBuilder sb = new StringBuilder();
		sb.append(rootPath);
		sb.append(File.separator);
		sb.append(invoiceYear);
		sb.append(File.separator);
		sb.append(contractAccNumber);
		sb.append(File.separator);
		sb.append(fileName);
		return sb.toString();
	}
	
	public List<com.seb.middleware.jpa.dao.Subscription> getSubscriptions(Long [] subIds){
		Session session = db.getSession();
		
		Criteria criteria = session.createCriteria(com.seb.middleware.jpa.dao.Subscription.class);
		criteria.add(Restrictions.in("subsId", subIds));
		
		return db.query(criteria);
	}
}
