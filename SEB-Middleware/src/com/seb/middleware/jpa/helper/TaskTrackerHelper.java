package com.seb.middleware.jpa.helper;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseHelper;
import com.seb.middleware.jpa.dao.TaskTracker;

public class TaskTrackerHelper extends DatabaseHelper{

	public TaskTrackerHelper(DatabaseFacade db) {
		super(db);
	}

	public TaskTracker get(String taskKey){
		Criteria criteria = db.getSession()
				.createCriteria(TaskTracker.class)
				.add(Restrictions.eq("taskKey", taskKey));
		
		ArrayList<TaskTracker> setting = db.query(criteria);
		if(setting == null || setting.isEmpty()){
			throw new RuntimeException("The requested time tracker '" + taskKey + "' was not found.");
		}else{
			return setting.get(0);
		}
	}
	
	public int commitTask(TaskTracker taskTracker){
		String hqlUpdate = "update TaskTracker set taskDatetime = current_timestamp where taskKey = :taskKey";
		int updated = db.createQuery(hqlUpdate)
				.setParameter("taskKey", taskTracker.getTaskKey())
				.executeUpdate();
		return updated;
	}
}
