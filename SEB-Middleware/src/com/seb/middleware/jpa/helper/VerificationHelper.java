package com.seb.middleware.jpa.helper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import javax.mail.MessagingException;
import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.api.services.request.Request;
import com.seb.middleware.api.services.request.cms.res.VerificationAuthority;
import com.seb.middleware.constant.OtpType;
import com.seb.middleware.constant.PreferredCommunicationMethod;
import com.seb.middleware.constant.VerificationCodeStatus;
import com.seb.middleware.constant.VerificationStatus;
import com.seb.middleware.email.EmailManager;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.DatabaseHelper;
import com.seb.middleware.jpa.dao.EmailVerification;
import com.seb.middleware.jpa.dao.MobileVerification;
import com.seb.middleware.jpa.dao.OtpVerification;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.dao.VerificationBase;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.sms.SMSManager;
import com.seb.middleware.utility.CryptoUtil;
import com.seb.middleware.utility.ResourceUtil;

public class VerificationHelper extends DatabaseHelper{

	private static final Logger logger = LogManager.getLogger(VerificationHelper.class);
	
	public VerificationHelper(DatabaseFacade db) {
		super(db);
	}
	
	public VerificationCodeStatus consumeEmailVerification(String userEmail, String code) throws DatabaseFacadeException, NoSuchAlgorithmException, UnsupportedEncodingException{
		
		ArrayList<VerificationBase> verifications = getValidVerifications(userEmail, null, VerificationType.EMAIL);
		logger.debug(verifications);
		
		boolean hasExpired = false;
		if(verifications != null){
			for(VerificationBase verify : verifications){
				boolean valid = CryptoUtil.ssha256Equals(verify.getCode(), code);
				if(valid){
					
					if(!verify.isExpired()){
						verify.setStatus(VerificationStatus.USED);
						logger.info("VerificationBase updation is stopped");
						//db.update(verify);
						
						return VerificationCodeStatus.VALID;	
					}else{
						hasExpired = true;
					}
				}
			}
		}
		
		if(hasExpired){
			return VerificationCodeStatus.EXPIRED;
		}else{
			return VerificationCodeStatus.INVALID;
		}
	}
	
	public VerificationCodeStatus consumeMobileVerification(String mobileNumber, String code) throws DatabaseFacadeException, NoSuchAlgorithmException, UnsupportedEncodingException{

		ArrayList<VerificationBase> verifications = getValidVerifications(null, mobileNumber, VerificationType.MOBILE);
		logger.debug(verifications);
		boolean hasExpired = false;
		if(verifications != null){
			for(VerificationBase verify : verifications){
				boolean valid = CryptoUtil.ssha256Equals(verify.getCode(), code);
				if(valid){
					if(!verify.isExpired()){
						verify.setStatus(VerificationStatus.USED);
						logger.info("VerificationBase updation is stopped");
						//db.update(verify);
						
						return VerificationCodeStatus.VALID;	
					}else{
						hasExpired = true;
					}
				}
			}
		}
		
		if(hasExpired){
			return VerificationCodeStatus.EXPIRED;
		}else{
			return VerificationCodeStatus.INVALID;
		}
	}
	
	public VerificationCodeStatus consumeOTPVerification(String userEmail, String code, OtpType type) throws DatabaseFacadeException, NoSuchAlgorithmException, UnsupportedEncodingException{

		ArrayList<VerificationBase> verifications = getValidVerifications(userEmail, null, VerificationType.OTP);
		logger.debug(verifications);
		boolean hasExpired = false;
		if(verifications != null){
			for(VerificationBase verify : verifications){
				OtpVerification otpVerify = (OtpVerification) verify;
				if(otpVerify.getType() == OtpType.ALL || otpVerify.getType() == type){
					boolean valid = CryptoUtil.ssha256Equals(verify.getCode(), code);
					if(valid){
						if(!verify.isExpired()){
							verify.setStatus(VerificationStatus.USED);
							logger.info("VerificationBase updation is stopped");
							//db.update(verify);
							
							return VerificationCodeStatus.VALID;	
						}else{
							hasExpired = true;
						}
					}	
				}
			}
		}
		
		if(hasExpired){
			return VerificationCodeStatus.EXPIRED;
		}else{
			return VerificationCodeStatus.INVALID;
		}
	}

	public boolean generateEmailVerification(Request request, String userEmail) throws DatabaseFacadeException, ServiceException{
		
		if(shouldGenerate(userEmail, null, VerificationType.EMAIL)){
			
			try {
				String code = generateVerification(userEmail, null, VerificationType.EMAIL);
				sendEmailVerification(userEmail, code);
				return true;
			} catch (IOException e) {
				logger.error("Error Sending Email Activation Code (Template file error)", e);
				throw new ServiceException(e, 
						ResourceUtil.get("error.general_500", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			} catch (MessagingException | NamingException e) {
				logger.error("Error Sending Email Activation Code (SMTP error)", e);
				throw new ServiceException(e, 
						ResourceUtil.get("error.general_500", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			} catch (NoSuchAlgorithmException e) {
				logger.error("Error Generating Email Activation Code", e);
				throw new ServiceException(e, 
						ResourceUtil.get("error.general_500", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			}
		}else{
			return false;
		}
	}
	
	public boolean generateMobileVerification(Request request, String mobileNumber, VerificationAuthority verifyAuth) throws DatabaseFacadeException, ServiceException{

		if((verifyAuth != null && verifyAuth.isAuthorized())
				|| shouldGenerate(null, mobileNumber, VerificationType.MOBILE)){
			try {
				String code = generateVerification(null, mobileNumber, VerificationType.MOBILE);
				
				if(verifyAuth != null){
					verifyAuth.setCode(code);
				}
				
				sendMobileVerification(mobileNumber, code);
				return true;
			} catch (IOException e) {
				logger.error("Error Sending SMS Verification Code (Template file error)", e);
				throw new ServiceException(e, 
						ResourceUtil.get("error.general_500", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			} catch (NoSuchAlgorithmException e) {
				logger.error("Error Generating Mobile Verification Code", e);
				throw new ServiceException(e, 
						ResourceUtil.get("error.general_500", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			}
		}else{
			return false;
		}
	}
	
	public boolean generateMobileVerification(Request request, String mobileNumber) throws DatabaseFacadeException, ServiceException{
		return generateMobileVerification(request, mobileNumber, null);
	}
	
	public boolean generateOneTimePassword(Request request, User user, Person person, VerificationAuthority verifyAuth, 
			OtpType type, PreferredCommunicationMethod forceMethod) throws DatabaseFacadeException, ServiceException{
		String userEmail = user.getUserId();
		String mobileNumber = person.getMobilePhone();
		
		if((verifyAuth != null && verifyAuth.isAuthorized())
				|| shouldGenerate(userEmail, mobileNumber, VerificationType.OTP)){
			
			try{
				String code = generateVerification(userEmail, mobileNumber, VerificationType.OTP, type);
				
				if(verifyAuth != null){
					verifyAuth.setCode(code);
				}
				
				PreferredCommunicationMethod method = user.getPreferredCommunicationMethod();
				if(forceMethod != null) method = forceMethod;
				
				if(method == PreferredCommunicationMethod.EMAIL){
					try {
						sendEmailOtp(userEmail, code);
						return true;
					} catch (IOException e) {
						logger.error("Error Sending Email OTP (Template file error)", e);
						throw new ServiceException(e, 
								ResourceUtil.get("error.general_500", 
										request.getLocale()), 
										APIResponse.ERROR_CODE_SERVER_EXCEPTION);
					} catch (MessagingException | NamingException e) {
						logger.error("Error Sending Email Activation Code (SMTP error)", e);
						throw new ServiceException(e, 
								ResourceUtil.get("error.general_500", 
										request.getLocale()), 
										APIResponse.ERROR_CODE_SERVER_EXCEPTION);
					} 
				}else if(method == PreferredCommunicationMethod.MOBILE){
					
					try {
						sendMobileOtp(mobileNumber, code);
						return true;
					} catch (IOException e) {
						logger.error("Error Sending SMS OTP (Template file error)", e);
						throw new ServiceException(e, 
								ResourceUtil.get("error.general_500", 
										request.getLocale()), 
										APIResponse.ERROR_CODE_SERVER_EXCEPTION);
					}
				}else{
					logger.error("Preferred communication method not implemented: " + user.getPreferredCommunicationMethod());
					throw new ServiceException( 
							ResourceUtil.get("error.general_500", 
									request.getLocale()), 
									APIResponse.ERROR_CODE_SERVER_EXCEPTION);
				}
			} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
				logger.error("Error Generating OTP Verification Code", e);
				throw new ServiceException(e, 
						ResourceUtil.get("error.general_500", 
								request.getLocale()), 
								APIResponse.ERROR_CODE_SERVER_EXCEPTION);
			}
		}else{
			return false;
		}
	}
	
	public boolean generateOneTimePassword(Request request, User user, Person person, PreferredCommunicationMethod forceMethod) throws DatabaseFacadeException, ServiceException{
		return generateOneTimePassword(request, user, person, null, OtpType.ALL, forceMethod);
	}
	
	private boolean shouldGenerate(String userEmail, String mobileNumber, VerificationType type){
		boolean shouldGenerate = true;
		SettingHelper helper = new SettingHelper(db);
		
		ArrayList<VerificationBase> validVerification = getValidVerifications(userEmail, mobileNumber, type);
		
		logger.debug("Valid Verification: " + validVerification);
		
		int minutesMustWait = helper.getActivationCodeRequestWaitInMinutes();
		
		logger.debug("Minutes must wait: " + minutesMustWait);
		
		if(minutesMustWait != -1){
			if(validVerification != null && !validVerification.isEmpty()){
				Date now = new Date();
				for(VerificationBase ver : validVerification){
					double minutesDifferent = (now.getTime() - ver.getCreatedDatetime().getTime()) / 1000 / 60;
					if(minutesDifferent < minutesMustWait){
						logger.info("Unable to generate " + type.toString() + " Verification for " 
								+ userEmail + " | " + mobileNumber + " because there's another Verification sent " + minutesDifferent + " minutes ago.");
						shouldGenerate = false;
						break;
					}
				}
			}	
		}
		
		return shouldGenerate;
	}
	
	private ArrayList<VerificationBase> getValidVerifications(String userEmail, String mobileNumber, VerificationType type){
		SettingHelper helper = new SettingHelper(db);
		
		Date lastValidDate = null;
		Date lastExpiredDate = null; // Date older than expired will not be retrieved at all

		int expiryMinute = 0;
		if(type == VerificationType.OTP){
			expiryMinute = helper.getActivationCodeExpirationInMinutes();
		}else if(type == VerificationType.EMAIL){
			expiryMinute = helper.getEmailActivationCodeExpirationInMinutes();
		}else if(type == VerificationType.MOBILE){
			expiryMinute = helper.getMobileActivationCodeExpirationInMinutes();
		}
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MINUTE, (expiryMinute * -1));
		lastValidDate = calendar.getTime();
		
		calendar.setTime(new Date());
		calendar.add(Calendar.MINUTE, (expiryMinute * -10));
		lastExpiredDate = calendar.getTime();
		
		logger.debug("getValidVerification(): " + lastExpiredDate.toString() + " | " + type + " | " + userEmail);
		
		Criteria criteria = null;
		
		if(type == VerificationType.EMAIL){
			criteria = db.getSession().
					createCriteria(EmailVerification.class).
					add(Restrictions.eq("userEmail", userEmail));
		}else if(type == VerificationType.MOBILE){
			criteria = db.getSession().
					createCriteria(MobileVerification.class).
					add(Restrictions.eq("mobileNumber", mobileNumber));
		}else if(type == VerificationType.OTP){
			criteria = db.getSession().
					createCriteria(OtpVerification.class)
					.add(Restrictions.eq("userEmail", userEmail));
		}
		
		criteria.add(Restrictions.eq("status", VerificationStatus.UNUSED))
			.add(Restrictions.gt("createdDatetime", lastExpiredDate));
		
		ArrayList<VerificationBase> list = db.query(criteria);
		if(list != null && !list.isEmpty()){
			for(VerificationBase vb : list){
				if(vb.getCreatedDatetime().compareTo(lastValidDate) >= 0){
					vb.setExpired(false);
				}
			}
		}
		
		return list;
	}
	
	private String generateVerification(String userEmail, String mobileNumber, VerificationType type, OtpType otpType) throws DatabaseFacadeException, NoSuchAlgorithmException, UnsupportedEncodingException{
		VerificationBase verify = null;
		if(type == VerificationType.EMAIL){
			verify = new EmailVerification(userEmail);
		}else if(type == VerificationType.MOBILE){
			verify = new MobileVerification(mobileNumber);
		}else if(type == VerificationType.OTP){
			verify = new OtpVerification(userEmail, otpType);
		}

		String code = generateVerificationCode();
		verify.setStatus(VerificationStatus.UNUSED);
		verify.setCode(CryptoUtil.ssha256HexadecimalLowercase(code));
		
		if(type == VerificationType.MOBILE){
			// Invalidate all other Code
			String hqlUpdate = "update MobileVerification set status = :newStatus where mobileNumber = :theNumber and status = :unusedStatus";
			int updated = db.createQuery(hqlUpdate)
					.setParameter("newStatus", VerificationStatus.INVALIDATE)
					.setParameter("unusedStatus", VerificationStatus.UNUSED)
					.setParameter("theNumber", mobileNumber)
					.executeUpdate();
			
			logger.info("Total " + updated + " mobile verification of " + mobileNumber + " were invalidated");
		}
		
		VerificationBase insertedVeri = db.insert(verify);
		logger.info("Verification Generated: " + insertedVeri.toString());
		
		return code;
	}
	
	private String generateVerification(String userEmail, String mobileNumber, VerificationType type) throws DatabaseFacadeException, NoSuchAlgorithmException, UnsupportedEncodingException{
		return generateVerification(userEmail, mobileNumber, type, null);
	}
	
	private void sendEmailVerification(String email, String code) throws IOException, MessagingException, NamingException{
		SettingHelper setting = new SettingHelper(db);
		EmailManager manager = new EmailManager();
		manager.sendEmailVerificationCode(email, code, setting.getEmailActivationCodeExpirationInMinutes());
	}
	
	private void sendMobileVerification(String mobileNumber, String code) throws IOException{
		SettingHelper setting = new SettingHelper(db);
		SMSManager manager = new SMSManager();
		manager.sendMobileVerificationCode(mobileNumber, code, setting.getMobileActivationCodeExpirationInMinutes());
	}
	
	private void sendEmailOtp(String email, String code) throws IOException, MessagingException, NamingException{
		SettingHelper setting = new SettingHelper(db);
		EmailManager manager = new EmailManager();
		manager.sendOtp(email, code, setting.getActivationCodeExpirationInMinutes());
	}
	
	private void sendMobileOtp(String mobileNumber, String code) throws IOException{
		SettingHelper setting = new SettingHelper(db);
		SMSManager manager = new SMSManager();
		manager.sendOtp(mobileNumber, code, setting.getActivationCodeExpirationInMinutes());
	}
	
	private String generateVerificationCode(){
		Random rand = new Random();
		int n = rand.nextInt(999999) + 1;
		return String.format("%06d", n);
	}
	
	private enum VerificationType {
		OTP,
		EMAIL,
		MOBILE
	}
}
