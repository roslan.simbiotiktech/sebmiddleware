package com.seb.middleware.ldap;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.anteater.library.json.Json;
import com.seb.middleware.core.BaseService;
import com.seb.middleware.core.PathManager;
import com.seb.middleware.utility.CryptoUtil;

public class LDAP implements BaseService{

	private static final Logger logger = LogManager.getLogger(LDAP.class);
		
	public static String DOMAIN_URL;
	public static String DOMAIN_PARTITION;
	public static String DOMAIN_BASE_DN;

	private static Hashtable<String, String> ldapEnvironmentParameters;
	
	@Override
	public void startService() {
		logger.info("Starting LDAP Service.");
		try{
			byte[] encoded = Files.readAllBytes(Paths.get(PathManager.getConfigPath("LDAPProperties.txt")));
			String s = new String(encoded);
			
			Json json = new Json();
			LDAPProperties ldapProperties = json.convert(LDAPProperties.class, s);
			logger.info(ldapProperties.toString());
			
			StringBuilder domain = new StringBuilder();
			domain.append(ldapProperties.getProtocol());
			if(!ldapProperties.getProtocol().endsWith("://")){
				domain.append("://");
			}
			domain.append(ldapProperties.getHost());
			domain.append(":");
			domain.append(ldapProperties.getPort());
			
			DOMAIN_PARTITION = ldapProperties.getDomainPartition();
			DOMAIN_BASE_DN = ldapProperties.getDomainBaseDn();
			
			DOMAIN_URL = domain.toString();
			logger.info("LDAP: " + DOMAIN_URL);
			
			ldapEnvironmentParameters = new Hashtable<String, String>();
			ldapEnvironmentParameters.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			ldapEnvironmentParameters.put(Context.PROVIDER_URL, DOMAIN_URL);
			ldapEnvironmentParameters.put(Context.SECURITY_AUTHENTICATION, "simple");
			ldapEnvironmentParameters.put(Context.SECURITY_PRINCIPAL, ldapProperties.getAdminUserdn());
			ldapEnvironmentParameters.put(Context.SECURITY_CREDENTIALS, CryptoUtil.decrypt(ldapProperties.getAdminPassword()));
			
		} catch (IOException e) {
			logger.error("Error reading config file", e);
		} catch (Exception e) {
			logger.error("Error Starting LDAP Service", e);
		}
		
		logger.info("LDAP Service Started.");
	}

	@Override
	public void stopService() {
		logger.info("Stopping LDAP Service.");
		
		logger.info("LDAP Service Stopped.");		
	}
	
	public static LdapContext getAdminInstance() throws NamingException{
		InitialLdapContext context = new InitialLdapContext(ldapEnvironmentParameters, null);
		return context;
	}
	
	public static void closeContext(LdapContext context){
		try{
			if(context != null) context.close();
		}catch(Exception e){
			logger.error("error closing ldap context", e);
		}
	}
}
