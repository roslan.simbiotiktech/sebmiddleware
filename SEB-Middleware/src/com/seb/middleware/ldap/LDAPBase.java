package com.seb.middleware.ldap;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.LdapContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.utility.CryptoUtil;
import com.seb.middleware.utility.StringUtil;

public class LDAPBase {
	
	private static final Logger logger = LogManager.getLogger(LDAPBase.class);
	
	protected static String ERR_ENCRYPTION = "Unable to encrypt string..........";
	protected static String ERR_DECRYPTION = "Unable to decrypt string..........";
	
	protected String getUserNameSpaceByEmail(LdapContext ctx, String email) throws NamingException{

		SearchControls ctls = new SearchControls();
	    ctls.setCountLimit(1);
	    
	    String filters = "(email="+email+")";
	    
	    NamingEnumeration<SearchResult> answer = ctx.search(LDAP.DOMAIN_BASE_DN + "," + LDAP.DOMAIN_PARTITION, filters, ctls);

	    if(answer.hasMore()) {
	        SearchResult sr = answer.next();
	        return sr.getNameInNamespace();
	    }
	    
		return "";
	}
	
	protected LdapContext getLoginContext(String user, String password) throws NamingException{
		
		NamingException namingException = null;
		LdapContext ctx = LDAP.getAdminInstance();
		try {
			ctx.addToEnvironment(Context.SECURITY_PRINCIPAL, getUserNameSpaceByEmail(ctx, user));
			ctx.addToEnvironment(Context.SECURITY_CREDENTIALS, password);
			ctx.reconnect(new Control[]{new PasswordRequestControl(true)});
		} catch (NamingException e) {
			namingException = e;
		} finally {
			String error = "";
			if (ctx != null) {
				PasswordPolicyResponse passwordPolicyResponse = getPasswordPolicyResponse(ctx);

				if (passwordPolicyResponse != null && passwordPolicyResponse.getError() != null)
					error = passwordPolicyResponse.getError().toString();

				if (!StringUtil.isEmpty(error)
						&& !error.equalsIgnoreCase("none")) {
					throw new LDAPPasswordPolicyException(
							passwordPolicyResponse, namingException);
				}
			}
			
			if (namingException != null)
				throw new NamingException(namingException.getMessage());
		}
        
		return ctx;
	}
	
	protected String generateUserDN(String email) {
		String dn = "";
		
		if(!StringUtil.isEmpty(email))
			dn = "email="+email+",";
		
    	return dn+LDAP.DOMAIN_BASE_DN+","+LDAP.DOMAIN_PARTITION;
    }
	
	protected String getAttributeValue(Attribute attribute){
    	return attribute != null ? attribute.toString().split(": ")[1] : "";
    }
    
	protected Person getPersonFromAttributes(Attributes attributes){
    	Person person = new Person();
    	
    	Attribute email = attributes.get("email");
    	Attribute name = attributes.get("cn");
        Attribute mobile = attributes.get("mobile");
        Attribute homePhone = attributes.get("homePhone");
        Attribute officePhone = attributes.get("officePhone");
        Attribute nricPassport = attributes.get("nricPassport");
        
        String nameString = getAttributeValue(name);
        String mobileString = getAttributeValue(mobile);
        String homePhoneString = getAttributeValue(homePhone);
        String officePhoneString = getAttributeValue(officePhone);
        String nricPassportString = getAttributeValue(nricPassport);
        
        try{
        	person.setEmail(getAttributeValue(email));
            person.setName(nameString);
            person.setMobilePhone(StringUtil.isEmpty(mobileString) ? "" : CryptoUtil.decrypt(mobileString));
            person.setHomePhone(StringUtil.isEmpty(homePhoneString) ? "" : CryptoUtil.decrypt(homePhoneString));
            person.setOfficePhone(StringUtil.isEmpty(officePhoneString) ? "" : CryptoUtil.decrypt(officePhoneString));
            person.setNricPassport(StringUtil.isEmpty(nricPassportString) ? "" : CryptoUtil.decrypt(nricPassportString));
        }catch(Exception ex){
        	logger.error("Error decryption......", ex);
        	throw new RuntimeException(ERR_DECRYPTION, ex);
        }        
    	
    	return person;
    }
	
	protected PasswordPolicyResponse getPasswordPolicyResponse(LdapContext ctx){
		PasswordPolicyResponse passwordPolicyResponse = new PasswordPolicyResponse();
		if (ctx != null) {
	          Control[] controls;
	          
				try {
					controls = ctx.getResponseControls();
					if (controls != null) {
						PasswordResponseControlFactory passwordResponseControlFactory = new PasswordResponseControlFactory();
						PasswordResponseControl passwordResponseControl =  (PasswordResponseControl) passwordResponseControlFactory.getControlInstance(controls);
						
						if(passwordResponseControl != null){
							passwordPolicyResponse = passwordResponseControl.getPasswordPolicyResponse();
						}
			          }
				} catch (NamingException ex) {
				}
	     }
		
		return passwordPolicyResponse;
	}
}
