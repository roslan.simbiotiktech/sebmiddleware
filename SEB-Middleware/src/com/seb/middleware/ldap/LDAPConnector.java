package com.seb.middleware.ldap;

import java.util.ArrayList;
import java.util.HashMap;

import javax.naming.AuthenticationException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.constant.AuthenticationStatus;
import com.seb.middleware.utility.CryptoUtil;
import com.seb.middleware.utility.StringUtil;

public class LDAPConnector extends LDAPBase{
	
	private static final Logger logger = LogManager.getLogger(LDAPConnector.class);
	
	public LDAPConnector() {
	}
	
	public AuthenticationStatus authenticate(String email, String userpassword) throws NamingException{
		LdapContext ctx = getLoginContext(email, userpassword);
		try{
			PasswordPolicyResponse passwordPolicyResponse = getPasswordPolicyResponse(ctx);
			
			if(passwordPolicyResponse != null && passwordPolicyResponse.getWarningCode() < 2 )
				return AuthenticationStatus.SUCCESS_PASSWORD_EXPIRED_WARNING;
			
			return AuthenticationStatus.SUCCESS;	
		}finally{
			LDAP.closeContext(ctx);
		}
	}

	public ArrayList<Person> getAllUsers() throws NamingException{
		ArrayList<Person> persons = new ArrayList<Person>();
		
	    Attributes matchAttrs = new BasicAttributes(true); // ignore case
	    matchAttrs.put(new BasicAttribute("email"));

	    LdapContext ctx = LDAP.getAdminInstance();
	    try{
	    	NamingEnumeration<SearchResult> answer = ctx.search(LDAP.DOMAIN_BASE_DN + "," + LDAP.DOMAIN_PARTITION, matchAttrs);

		    while (answer.hasMore()) {
		        SearchResult sr = answer.next();
		        Attributes attributes = sr.getAttributes();
		        persons.add(getPersonFromAttributes(attributes));
		    }		    
		    
			return persons;
	    }finally{
	    	LDAP.closeContext(ctx);
	    }
	}

	public ArrayList<Person> getUsers(String name, String mobilePhone) throws NamingException{
		ArrayList<Person> persons = new ArrayList<Person>();
		
		StringBuilder searchFilter = new StringBuilder();
		searchFilter.append("(&(objectClass=customPerson)");
		
		if(!StringUtil.isEmpty(name)){
			searchFilter.append("(cn=*").append(name).append("*)");
		}
		
		if(!StringUtil.isEmpty(mobilePhone)){
			try{
				mobilePhone = CryptoUtil.encrypt(mobilePhone);
				searchFilter.append("(mobile=").append(mobilePhone).append(")");
			}catch(Exception ex){
				logger.error("Error encryption.", ex);
	        	throw new RuntimeException(ERR_ENCRYPTION, ex);
			}
		}
		
		searchFilter.append(")");
		
		String searchCriteria = searchFilter.toString();
		logger.debug("search: " + searchCriteria);
		
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        LdapContext ctx = LDAP.getAdminInstance();
        
        try{
        	NamingEnumeration<SearchResult> answer = ctx.search(LDAP.DOMAIN_BASE_DN + "," + LDAP.DOMAIN_PARTITION, searchCriteria, searchControls);
     	    
     	    while (answer.hasMore()) {
     	        SearchResult sr = answer.next();
     	        Attributes attributes = sr.getAttributes();
     	        persons.add(getPersonFromAttributes(attributes));
     	    }		    
     	    
     		return persons;
        }finally{
        	LDAP.closeContext(ctx);
        }
	}
	
	public Person getUser(String email) throws NamingException{
		Person person = null;
		
	    Attributes matchAttrs = new BasicAttributes(true); // ignore case
	    matchAttrs.put(new BasicAttribute("email", email));

	    LdapContext ctx = LDAP.getAdminInstance();
	    
	    try{
	    	NamingEnumeration<SearchResult> answer = ctx.search(LDAP.DOMAIN_BASE_DN + "," + LDAP.DOMAIN_PARTITION, matchAttrs);

		    person = new Person();
		    
		    while (answer.hasMore()) {
		        SearchResult sr = answer.next();
		        Attributes attributes = sr.getAttributes();
		        person = getPersonFromAttributes(attributes);
		    }		    
		    
			return person;
	    }finally{
	    	LDAP.closeContext(ctx);
	    }
	}
	
	// added by pramod 
	// get list of account which have same mobile and NRIC/Passport 
	public ArrayList<Person> getUsersMobileNumNric(String mobilePhone,String NricPassportNum) throws Exception{
		ArrayList<Person> persons = new ArrayList<Person>();
		logger.info("Second Ldap request  mobile :: " + mobilePhone+" NricPassportNum  :: "+NricPassportNum);	
	//    Attributes matchAttrs = new BasicAttributes(true); // ignore case
	
	    StringBuilder searchFilter = new StringBuilder();
		searchFilter.append("(&(objectClass=customPerson)");	
		
	
		
		  if(!StringUtil.isEmpty(NricPassportNum)){ 
			  NricPassportNum = CryptoUtil.encrypt(NricPassportNum);
			  searchFilter.append("(nricPassport=").append(NricPassportNum).append(")");
		 }
		 
		
		  if(!StringUtil.isEmpty(mobilePhone)){
			  try{ 
				mobilePhone = CryptoUtil.encrypt(mobilePhone);
			  	searchFilter.append("(mobile=").append(mobilePhone).append(")");
		  }catch(Exception ex){
			  logger.error("Error encryption.", ex); throw new
		  RuntimeException(ERR_ENCRYPTION, ex);
			  } 
			  }
		 
		
		searchFilter.append(")");
		
		String searchCriteria = searchFilter.toString();
		logger.debug("search: " + searchCriteria);
		
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        LdapContext ctx = LDAP.getAdminInstance();
	    
	    try{
	    
	    	NamingEnumeration<SearchResult> answer = ctx.search(LDAP.DOMAIN_BASE_DN + "," + LDAP.DOMAIN_PARTITION,  searchCriteria, searchControls);
		    while (answer.hasMore()) {
		        SearchResult sr = answer.next();
		        Attributes attributes = sr.getAttributes();
		        persons.add(getPersonFromAttributes(attributes));
		    }
	    }
		    catch (Exception e) {
				// TODO: handle exception
		    	e.printStackTrace();
		    	logger.error("Ldap  second Error >>");
			}finally{
		    	LDAP.closeContext(ctx);
		    }
		    logger.info("second ldap users list  size is : >>>>>  "+persons.size());
		    if(persons.size()!=0) {
		    	for (Person person : persons) {
		    		logger.info("person info from Ldap :"+": E-mail "+person.getEmail() +" Mobile : "+person.getMobilePhone()+" NRIC/Passport : "+person.getNricPassport());
				}
		    	
		    }
		    
		    
			return persons;
	    
	
	}
	
	
	
	
	public boolean isUserExist(String email) throws NamingException{
		logger.info("isUserExist ? " + email);	
		LdapContext ctx = LDAP.getAdminInstance();
		
		try{
			boolean result = false;

		    Attributes matchAttrs = new BasicAttributes(true); // ignore case
		    matchAttrs.put(new BasicAttribute("email", email));

		    NamingEnumeration<SearchResult> answer = ctx.search(LDAP.DOMAIN_BASE_DN + "," + LDAP.DOMAIN_PARTITION, matchAttrs);

		    while (answer.hasMore()) {
		    	result = true;
		    	break;
		    }		    
			
			return result;
		}finally{
			LDAP.closeContext(ctx);
		}
	}
	
	public boolean addUser(Person person) throws NamingException{
		Attributes container = new BasicAttributes();

        Attribute objClasses = new BasicAttribute("objectClass");
        objClasses.add("customPerson");

        String emailValue = StringUtil.isEmpty(person.getEmail()) ? "" : person.getEmail();
        Attribute cn = null;
        Attribute mobile = null;
        Attribute homePhone = null;
        Attribute officePhone = null;
        Attribute nric = null;
        Attribute userPassword = null;
        
        try{
        	cn = new BasicAttribute("cn", person.getName());
        	mobile = new BasicAttribute("mobile", StringUtil.isEmpty(person.getMobilePhone()) ? "" : CryptoUtil.encrypt(person.getMobilePhone()) );
        	homePhone = new BasicAttribute("homePhone", StringUtil.isEmpty(person.getHomePhone()) ? "" : CryptoUtil.encrypt(person.getHomePhone()));
        	officePhone = new BasicAttribute("officePhone", StringUtil.isEmpty(person.getOfficePhone()) ? "" : CryptoUtil.encrypt(person.getOfficePhone()));
        	nric = new BasicAttribute("nricPassport", StringUtil.isEmpty(person.getNricPassport()) ? "" : CryptoUtil.encrypt(person.getNricPassport()));
        	userPassword = new BasicAttribute("userpassword", person.getUserPassword());
        }catch(Exception ex){
        	logger.error("Error encryption......", ex);
        	throw new RuntimeException(ERR_ENCRYPTION, ex);
        }

        container.put(objClasses);
        
        if(!StringUtil.isEmpty(person.getName()))
        	container.put(cn);
        
        if(!StringUtil.isEmpty(person.getUserPassword()))
        	container.put(userPassword);
        
        //Optional attribute
        if(!StringUtil.isEmpty(person.getMobilePhone()))
        	container.put(mobile);
        
        if(!StringUtil.isEmpty(person.getHomePhone()))
        	container.put(homePhone);
        
        if(!StringUtil.isEmpty(person.getOfficePhone()))
        	container.put(officePhone);
        
        if(!StringUtil.isEmpty(person.getNricPassport()))
        	container.put(nric);
        
        LdapContext ctx = LDAP.getAdminInstance();
        
        try{
        	ctx.createSubcontext(generateUserDN(emailValue), container);
            return true;
        }finally{
        	if(ctx != null){
				try{
					ctx.close();
				}catch(Exception e){
					logger.error("error closing ldap context", e);
				}
			}
        }
    }
    
	public boolean changePassword(String email, String oldPass, String newPass) throws AuthenticationException, NamingException{		
		
		LdapContext ctx = getLoginContext(email, oldPass);
    	
		try{
			boolean result = false;
			
	    	email = getUserNameSpaceByEmail(ctx, email);
	    	
	    	if(ctx != null){    		
	            ModificationItem[] mods = new ModificationItem[2];
	            
	            //Supply old password to remove attribute
	            mods[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE,
	            		new BasicAttribute("userpassword", oldPass));

	            mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
	            		new BasicAttribute("userpassword", newPass));

	            // Perform the update
	            ctx.modifyAttributes(email, mods);
	            
	            logger.info("Changed Password successfully");
	            
	            result = true;
	    	}else
	    		logger.info("Invalid credential with old password.");
	    	
			return result;
		}finally{
			LDAP.closeContext(ctx);
		}
	}
    
    public void resetPassword(String email, String newPass) throws AuthenticationException, NamingException{		
		
		LdapContext ctx = LDAP.getAdminInstance();
		
		try{
			email = getUserNameSpaceByEmail(ctx, email);
			
			ModificationItem[] mods = new ModificationItem[1];

	        mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
	                      new BasicAttribute("userpassword", newPass));

	        // Perform the update
	        ctx.modifyAttributes(email, mods);
	        
	        logger.info("Reset Password successfully");
		}finally{
			LDAP.closeContext(ctx);
		}
	}
    
    public boolean updateUser(Person person) throws AuthenticationException, NamingException{
    	
    	LdapContext ctx = LDAP.getAdminInstance();
    	
    	String userDn = getUserNameSpaceByEmail(ctx, person.getEmail());
		
    	try{
    		HashMap<String,String> listToUpdate = new HashMap<String,String>();
            
            if(!StringUtil.isEmpty(person.getName()))
            	listToUpdate.put("cn", person.getName());
            
            try{
            	if(!StringUtil.isEmpty(person.getHomePhone()))
                	listToUpdate.put("homePhone", CryptoUtil.encrypt(person.getHomePhone()));
            	else
            		listToUpdate.put("homePhone", null);
            	
                if(!StringUtil.isEmpty(person.getOfficePhone()))
                	listToUpdate.put("officePhone", CryptoUtil.encrypt(person.getOfficePhone()));
                else
                	listToUpdate.put("officePhone", null);
                
                if(!StringUtil.isEmpty(person.getMobilePhone()))
                	listToUpdate.put("mobile", CryptoUtil.encrypt(person.getMobilePhone()));
                
                if(!StringUtil.isEmpty(person.getNricPassport()))
                	listToUpdate.put("nricPassport", CryptoUtil.encrypt(person.getNricPassport()));
            }catch(Exception ex){
            	logger.error("Error encryption......", ex);
            	throw new RuntimeException(ERR_ENCRYPTION, ex);
            }   
            
            int count = 0;
            
            ModificationItem[] mods = new ModificationItem[listToUpdate.size()];
            
            for(java.util.Map.Entry<String, String> entry : listToUpdate.entrySet()){
            	//Will create new if attribute not exist
            	//Will delete attribute if value not exist
            	int action = DirContext.REPLACE_ATTRIBUTE;
            	
            	String key = entry.getKey();
            	String value = entry.getValue();
            	
            	mods[count] = new ModificationItem(action,
                		new BasicAttribute(key, value));
            	
            	count++;
            }
            
            // Perform the update
            ctx.modifyAttributes(userDn, mods);
            
            logger.info("Update User successfully");
            
            return true;  
    	}finally{
    		LDAP.closeContext(ctx);
    	}
	}
    
    public boolean updateUserToNewEmail(Person person, String emailNew) throws AuthenticationException, NamingException{		
		
		LdapContext ctx = LDAP.getAdminInstance();
		
		try{
			boolean result = false;
			
	    	String userDn = getUserNameSpaceByEmail(ctx, person.getEmail());
	    	
	    	if(ctx != null){    		
	            if(!StringUtil.isEmpty(emailNew)){
	            	//Update userId/email from old to new
	            	String userDnNew = userDn.replace(person.getEmail(), emailNew);
	                ctx.rename(userDn, userDnNew);
	                
	                logger.info("Update User To New Email successfully!");
	                
	                result = true;
	            }else
	            	logger.error("User New Email is empty!");
	            
	    	}else
	    		logger.info("Invalid credential.");
	    	
			return result;
		}finally{
			LDAP.closeContext(ctx);
		}
	}
    
    public boolean deleteUser(String email) throws NamingException{
		logger.info("deleteUser - " + email);	
		
		LdapContext ctx = LDAP.getAdminInstance();
		
		try{
			email = getUserNameSpaceByEmail(ctx, email);
			ctx.unbind(email);
			return true;
		}finally{
			LDAP.closeContext(ctx);
		}
	}
}
