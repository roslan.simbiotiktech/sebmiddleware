package com.seb.middleware.ldap;

import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.api.APIResponse;
import com.seb.middleware.api.ServiceException;
import com.seb.middleware.utility.ResourceUtil;

public class LDAPErrorHandler {

	private static final Logger logger = LogManager.getLogger(LDAPErrorHandler.class);
	
	private static final int LDAP_INVALID_CREDENTIAL = 49;
	private static final int LDAP_NEW_PASSWORD_IN_HISTORY = 19;
	//private static final int LDAP_INSUFFICIENT_PERMISSION = 50;
	
	public static ServiceException processException(NamingException ex, String locale){
		logger.error("processException", ex);
		if(ex instanceof LDAPPasswordPolicyException){
			LDAPPasswordPolicyException policy = (LDAPPasswordPolicyException) ex;
			logger.error("Possword Policy Exception: " + policy.getPasswordPolicyResponse());
			PasswordPolicyResponse.Error err = PasswordPolicyResponse.Error.none;
			
			if(policy.getPasswordPolicyResponse() != null && policy.getPasswordPolicyResponse().isErrorCode()){
				err = policy.getPasswordPolicyResponse().getError();
			}
			
			logger.error("LDAP Password Policy Error Type: " + err);
			
			ServiceException se = null;
			
			switch(err){
				case passwordExpired:
					se = new ServiceException(
							ResourceUtil.get("error.ldap_password_expired", locale), 
							APIResponse.ERROR_CODE_INVALID_INPUT);
					break;
				case accountLocked:
					se = new ServiceException(
							ResourceUtil.get("error.ldap_account_locked", locale), 
							APIResponse.ERROR_CODE_INVALID_INPUT);
					break;
				case changeAfterReset:
					se = new ServiceException(
							ResourceUtil.get("error.ldap_change_after_reset", locale), 
							APIResponse.ERROR_CODE_INVALID_INPUT);
					break;
				case passwordModNotAllowed:
					se = new ServiceException(
							ResourceUtil.get("error.general_error_ldap", locale), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
					break;
				case mustSupplyOldPassword:
					se = new ServiceException(
							ResourceUtil.get("error.general_error_ldap", locale), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
					break;
				case insufficientPasswordQuality:
					se = new ServiceException(ResourceUtil.get("validation.invalid_password", locale, 
							ResourceUtil.get("field.password", locale)), 
							APIResponse.ERROR_CODE_INVALID_INPUT);
					break;
				case passwordTooShort:
					se = new ServiceException(ResourceUtil.get("validation.invalid_password", locale, 
							ResourceUtil.get("field.password", locale)), 
							APIResponse.ERROR_CODE_INVALID_INPUT);
					break;
				case passwordTooYoung:
					se = new ServiceException(ResourceUtil.get("validation.invalid_password", locale, 
							ResourceUtil.get("field.password", locale)), 
							APIResponse.ERROR_CODE_INVALID_INPUT);
					break;
				case passwordInHistory:
					se = new ServiceException(
							ResourceUtil.get("error.ldap_password_in_history", locale), 
							APIResponse.ERROR_CODE_INVALID_INPUT);
					break;
				case none:
					se = new ServiceException(
							ResourceUtil.get("error.invalid_login_credential", locale), 
							APIResponse.LOGIN_ERROR_CODE_INVALID_CREDENTIAL);
					break;
				default:
					se = new ServiceException(
							ResourceUtil.get("error.general_error_ldap", locale), 
							APIResponse.ERROR_CODE_SERVER_EXCEPTION);
					break;
			}
			
			return se;
			
		}else{
			LDAPError error = extractError(ex);
			logger.error(error);
			
			if(error.getErrorCode() == LDAP_INVALID_CREDENTIAL){
				ServiceException se = new ServiceException(
						ResourceUtil.get("error.invalid_login_credential", locale), 
						APIResponse.LOGIN_ERROR_CODE_INVALID_CREDENTIAL);
				return se;
			}else if(error.getErrorCode() == LDAP_NEW_PASSWORD_IN_HISTORY){
				ServiceException se = new ServiceException(
						ResourceUtil.get("error.ldap_password_in_history", locale), 
						APIResponse.ERROR_CODE_INVALID_INPUT);
				return se;
			}else{
				ServiceException se = new ServiceException(
						ResourceUtil.get("error.general_error_ldap", locale), 
						APIResponse.ERROR_CODE_SERVER_EXCEPTION);
				return se;
			}
		}
	}
	
	private static final String ERROR_CODE = "error code";
	private static final String DASH = "-";
	/*
	 * e.g. [LDAP: error code 49 - Invalid Credentials]
	 */
	private static LDAPError extractError(NamingException exception){
		
		LDAPError error = new LDAPError();
		
		String explanation = exception.getExplanation();

		if(explanation != null && !explanation.isEmpty()){
			explanation = explanation.toLowerCase();
			explanation = explanation.replace("[", "");
			explanation = explanation.replace("]", "");
			int indexErrCode = explanation.indexOf("error code");
			int indexDash = explanation.indexOf("-");
			
			if(indexErrCode != -1 && indexDash != -1){
			
				try{
					String errCode = explanation.substring(indexErrCode + ERROR_CODE.length(), indexDash).trim();
					String errMsg = explanation.substring(indexDash + DASH.length()).trim();
					
					error.setErrorCode(Integer.parseInt(errCode));
					error.setErrorMessage(errMsg);
					
					return error;
				}catch(Throwable ex){
					logger.error("Error parsing ldap error " + explanation, ex);
				}
			}
		}
		
		error.setErrorCode(99);
		error.setErrorMessage("LDAP Error is empty");
		
		return error;
	}
}
