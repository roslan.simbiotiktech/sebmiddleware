package com.seb.middleware.ldap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import com.sap.conn.jco.util.Codecs.Hex;

public class LDAPExtractor {

	private static final String PASSWORD = "sar@w@k.3nergy^@*%";
	
	public static void main(String [] args) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, FileNotFoundException, IOException{
		
		String input = "C:\\Users\\PeirHwa\\Desktop\\Projects\\SEB\\SEB\\Deploy\\customer.ld";
		String output = "C:\\Users\\PeirHwa\\Desktop\\Projects\\SEB\\SEB\\Deploy\\customer.sql";
		
		List<Person> persons = new ArrayList<>();
		
		try (BufferedReader br = new BufferedReader(new FileReader(new File(input)))) {
		    String line;
		    StringBuilder sb = new StringBuilder();
		    while ((line = br.readLine()) != null) {
		    	if(line.isEmpty()){
		    		if(sb.length() > 0){
				    	Person per = getPerson(sb.toString());
			    		if(per != null){
			    			persons.add(per);
			    		}	
				    }
		    		sb = new StringBuilder();
		    	}else{
		    		sb.append("\n").append(line);
		    	}
		    }
		    
		    if(sb.length() > 0){
		    	Person per = getPerson(sb.toString());
	    		if(per != null){
	    			persons.add(per);
	    		}	
		    }
		}
		
		System.out.println("-- Total " + persons.size() + " loaded");
		
		try(PrintWriter writer = new PrintWriter(output, "UTF-8")){
			for(int i = 0; i < persons.size(); i++){
				
				Person p = persons.get(i);
				
				StringBuilder line = new StringBuilder();
				line.append("insert into seb.user_mobile (user_id, mobile, email_flag) values (");
				line.append("'").append(p.getEmail().replace("'", "''")).append("', ");
				
				if(p.getMobilePhone() == null){
					line.append("null");
				}else{
					line.append("'").append(p.getMobilePhone()).append("'");
				}
				
				line.append(", ");
				if(p.getEmail().indexOf("@") != -1){
					line.append("1);");
				}else{
					line.append("0);");
				}
				
				writer.println(line.toString());
			}	
		}
		
		System.out.println("Done printing into " + output);
	}
	
	private static Pattern emailPattern = Pattern.compile(Pattern.quote("email=") + "(.*?)" + Pattern.quote(","));
	private static Pattern mobilePattern = Pattern.compile(Pattern.quote("mobile: ") + "(.*?)" + Pattern.quote("\n")); 
	
	private static Person getPerson(String lines) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException{
		
		String email = matchGroup1(lines, emailPattern);
		if(email != null){
			String encMobile = matchGroup1(lines, mobilePattern);
			
			String mobile = null;
			if(encMobile != null){
				mobile = decrypt(encMobile, PASSWORD);
			}
			
			Person p = new Person();
			p.setEmail(email);
			p.setMobilePhone(mobile);
			
			return p;
		}
		
		return null;
		
	}
	
	private static String matchGroup1(String text, Pattern pattern){
		Matcher matcher = pattern.matcher(text);
		
		if(matcher.find()){
			String email = matcher.group(1);
			
			if(email != null && !email.isEmpty()){
				return email.trim();
			}
		}
		
		return null;
	}
	
	private static final String ALGORITHM = "AES";
	private static final String CIPHER = "AES/ECB/PKCS5Padding";
	private static final String ENCODING = "UTF-8";
	private static final int KEY_LENGTH = 128;

	private static byte[] getSecret(String password) throws UnsupportedEncodingException, NoSuchAlgorithmException{
		MessageDigest sha = MessageDigest.getInstance("SHA-1");
		byte [] secret = sha.digest(password.getBytes(ENCODING));
		secret = Arrays.copyOf(secret, KEY_LENGTH / Byte.SIZE);
		return secret;
	}
	
	public static String decrypt(String encrypted, String password) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException{

		byte[] encryptedCipher = Base64.decodeBase64(encrypted);
	     
		SecretKey secret = new SecretKeySpec(getSecret(password), ALGORITHM);

		Cipher cipher = Cipher.getInstance(CIPHER);

		cipher.init(Cipher.DECRYPT_MODE, secret);
		String plain = new String(cipher.doFinal(encryptedCipher), ENCODING);
	    return plain;
	}
	
	public static byte[] decodeBase64(String base64){
		byte[] decoded= Base64.decodeBase64(base64.getBytes());
		return decoded;
	}
	
	/*
	 * Return string's length = input length * 2 
	 */
	public static String getHexSalt(int length) {
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[length];
		random.nextBytes(bytes);
		return Hex.encode(bytes).toLowerCase();
	}
}
