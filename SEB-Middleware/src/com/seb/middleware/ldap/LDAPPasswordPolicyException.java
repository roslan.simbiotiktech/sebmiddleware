package com.seb.middleware.ldap;

import javax.naming.NamingException;

public class LDAPPasswordPolicyException extends NamingException
{
	private PasswordPolicyResponse passwordPolicyResponse;
	
	private static final long serialVersionUID = -5072093197316256178L;

	public LDAPPasswordPolicyException(String msg)
    {
        super(msg);
    }

    public LDAPPasswordPolicyException(PasswordPolicyResponse passwordPolicyResponse, Exception exc)
    {
        super(exc.getMessage());
        super.setRootCause(exc);
        super.setStackTrace(exc.getStackTrace());
        this.passwordPolicyResponse = passwordPolicyResponse;
    }

	public PasswordPolicyResponse getPasswordPolicyResponse() {
		return passwordPolicyResponse;
	}

	public void setPasswordPolicyResponse(
			PasswordPolicyResponse passwordPolicyResponse) {
		this.passwordPolicyResponse = passwordPolicyResponse;
	}

}