package com.seb.middleware.ldap;

import com.anteater.library.json.JsonField;


public class LDAPProperties {

	@JsonField(name="HOST", validateLength = false)
	private String host;
	
	@JsonField(name="PORT")
	private String port;
	
	@JsonField(name="PROTOCOL")
	private String protocol;
	
	@JsonField(name="DOMAIN_PARTITION", validateLength = false)
	private String domainPartition;
	
	@JsonField(name="DOMAIN_BASE_DN", validateLength = false)
	private String domainBaseDn;
	
	@JsonField(name="ADMIN_USERDN", validateLength = false)
	private String adminUserdn;
	
	@JsonField(name="ADMIN_PASSWORD", validateLength = false)
	private String adminPassword;
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getDomainPartition() {
		return domainPartition;
	}

	public void setDomainPartition(String domainPartition) {
		this.domainPartition = domainPartition;
	}

	public String getDomainBaseDn() {
		return domainBaseDn;
	}

	public void setDomainBaseDn(String domainBaseDn) {
		this.domainBaseDn = domainBaseDn;
	}

	public String getAdminUserdn() {
		return adminUserdn;
	}

	public void setAdminUserdn(String adminUserdn) {
		this.adminUserdn = adminUserdn;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}

	@Override
	public String toString() {
		return "LDAPProperties [host=" + host + ", port=" + port + ", protocol=" + protocol + ", domainPartition=" + domainPartition
				+ ", domainBaseDn=" + domainBaseDn + ", adminUserdn=" + adminUserdn + "]";
	}
}
