package com.seb.middleware.ldap;


public class PasswordPolicyResponse {
	/** Warning codes. */
    public enum Warning
    {
        timeBeforeExpiration,
        graceAuthNsRemaining,
        none;
    }
    /** Error codes. */
    public enum Error
    {
        /** The password has expired.*/
        passwordExpired,
        accountLocked,
        changeAfterReset,
        passwordModNotAllowed,
        mustSupplyOldPassword,
        insufficientPasswordQuality,
        passwordTooShort,
        passwordTooYoung,
        passwordInHistory,
        /**
         * No info, e.g. user has simply used the wrong password.
         */
        none;
    };
    private int warningCode = Warning.none.ordinal();
    private int timeBeforeExpiration;
    private int graceLoginsRemaining;
    private int errorCode = Error.none.ordinal();
    
    /**
     * @return true if this response has a Warning.
     */
    public boolean isWarningCode()
    {
        return warningCode != Warning.none.ordinal();
    }
    /**
     * Only meaningful if isWarningCode() returns true.
     * @return the Warning
     */
    public Warning getWarning()
    {
        return Warning.values()[warningCode];
    }
    /**
     * Only meaningful if isWarningCode() returns true
     * and getWarning() returns <code>Warning.graceAuthNsRemaining</code>.
     * @return the number of grace logins remaining.
     */
    public int getGraceLoginsRemaining()
    {
        return graceLoginsRemaining;
    }
    /**
     * Only meaningful if isWarningCode() returns true
     * and getWarning() returns <code>Warning.timeBeforeExpiration</code>.
     * @return the time to expiration,
     * measured in seconds since the response that created this object was sent.
     */
    public int getTimeBeforeExpiration()
    {
        return timeBeforeExpiration;
    }
    /**
     * @return true if this response has an Error.
     */
    public boolean isErrorCode()
    {
        return errorCode != Error.none.ordinal();
    }
    /**
     * Only meaningful if isErrorCode() returns true.
     * @return the Error.
     */
    public Error getError()
    {
        return Error.values()[errorCode];
    }
    
	public void setWarningCode(int warningCode) {
		this.warningCode = warningCode;
	}
	
	public void setTimeBeforeExpiration(int timeBeforeExpiration) {
		this.timeBeforeExpiration = timeBeforeExpiration;
	}
	
	public void setGraceLoginsRemaining(int graceLoginsRemaining) {
		this.graceLoginsRemaining = graceLoginsRemaining;
	}
	
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	public int getWarningCode() {
		return warningCode;
	}
	
	public int getErrorCode() {
		return errorCode;
	}
	
	@Override
	public String toString() {
		return "PasswordPolicyResponse [warningCode=" + warningCode
				+ ", timeBeforeExpiration=" + timeBeforeExpiration
				+ ", graceLoginsRemaining=" + graceLoginsRemaining
				+ ", errorCode=" + errorCode + "]";
	}
    
}
