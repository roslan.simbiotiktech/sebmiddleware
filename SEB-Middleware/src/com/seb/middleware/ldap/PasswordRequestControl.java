package com.seb.middleware.ldap;

import javax.naming.ldap.BasicControl;

public class PasswordRequestControl extends BasicControl
{
    public static final long    serialVersionUID = 5342767500282457716L;

    // @see http://tools.ietf.org/html/draft-behera-ldap-password-policy-10#page-24

    public PasswordRequestControl()
    {
        super(PasswordResponseControl.OID);
    }

    public PasswordRequestControl(boolean isCritical)
    {
        super(PasswordResponseControl.OID, isCritical, null);
    }
}