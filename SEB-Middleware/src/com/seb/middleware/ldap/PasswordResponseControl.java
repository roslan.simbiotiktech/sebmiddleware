package com.seb.middleware.ldap;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.naming.NamingException;
import javax.naming.ldap.BasicControl;

import netscape.ldap.ber.stream.BERChoice;
import netscape.ldap.ber.stream.BERElement;
import netscape.ldap.ber.stream.BEREnumerated;
import netscape.ldap.ber.stream.BERInteger;
import netscape.ldap.ber.stream.BERSequence;
import netscape.ldap.ber.stream.BERTag;
import netscape.ldap.ber.stream.BERTagDecoder;
import netscape.ldap.client.JDAPBERTagDecoder;

public class PasswordResponseControl extends BasicControl
{
	private static final long serialVersionUID = -777564737891801749L;
	/** PasswordResponse OID. */
    public static final String OID = "1.3.6.1.4.1.42.2.27.8.5.1";

    public PasswordPolicyResponse passwordPolicyResponse = new PasswordPolicyResponse();

    /**
     * Construct an instance. Called by the PasswordResponseControlFactory.
     * @param isCritical true iff critical
     * @param ber  BER encoded data
     * @throws NamingException Decoding problem.
     */
    public PasswordResponseControl(boolean isCritical, byte[] ber)
        throws NamingException
    {
        super(OID, isCritical, ber);
        try
        {
            new NetscapePasswordResponseControlDecoder().decode(ber);
        }
        catch (IOException exc)
        {
            throw new LDAPPasswordPolicyException("Failed to parse control value "+java.util.Arrays.toString(ber));
        }
    }
    
    public PasswordPolicyResponse getPasswordPolicyResponse() {
		return passwordPolicyResponse;
	}

	public void setPasswordPolicyResponse(
			PasswordPolicyResponse passwordPolicyResponse) {
		this.passwordPolicyResponse = passwordPolicyResponse;
	}

	/**
     * Decoder based on Netscape ldapsdk library.
     */
    private class NetscapePasswordResponseControlDecoder
    {
        public void decode(byte[] encodedValue) throws IOException
        {
            int[] bytesRead = {0};
            BERSequence seq = (BERSequence)BERElement.getElement(new ApplicationTagDecoder(),
                new ByteArrayInputStream(encodedValue), bytesRead);
            
            for (int i = 0; i < seq.size(); i++)
            {
                BERTag tag = (BERTag)seq.elementAt(i);
                BERElement  element = tag.getValue();
                if (element instanceof BERChoice)
                {
                    // ASN.1 Choice: the warning data.
                    BERChoice warning = (BERChoice)element;
                    BERTag content = (BERTag)warning.getValue();
                    int value = ((BERInteger)content.getValue()).getValue();
                    int warningCode = content.getTag();
                    
                    passwordPolicyResponse.setWarningCode(warningCode);
                    switch (passwordPolicyResponse.getWarningCode())
                    {
                    case 0x80:
                    	passwordPolicyResponse.setTimeBeforeExpiration(value);
                        break;
                    case 0x81:
                    	passwordPolicyResponse.setGraceLoginsRemaining(value);
                        break;
                    default:
                    }
                    passwordPolicyResponse.setWarningCode(warningCode &= ~0x80); // Clear junk
                }
                else if (element instanceof BEREnumerated)
                {
                    // ASN.1 Enumerated: the error code.
                    BEREnumerated error = (BEREnumerated)element;
                    passwordPolicyResponse.setErrorCode(error.getValue());
                }
            }
        }
        /**
         * Application-specific decoder.
         */
        class ApplicationTagDecoder extends JDAPBERTagDecoder
        {
            private boolean inChoice;

            @Override
            public BERElement getElement(BERTagDecoder decoder, int tag, InputStream stream, int[] bytesRead,
                boolean[] implicit) throws IOException
            {
                switch (tag)
                {
                case 0xa0:
                    implicit[0] = false;
                    inChoice = true;
                    BERElement.readLengthOctets(stream, bytesRead);
                    int[] componentLength = new int[1];
                    BERChoice choice = new BERChoice(decoder, stream, componentLength);
                    bytesRead[0] += componentLength[0];
                    return choice;
                case 0x80:
                case 0x81:
                    if (inChoice)
                    {
                        // Must be time before expiry (0x80)
                        // or graceLogins (0x81)
                        inChoice = false;
                        return new BERInteger(stream, bytesRead);
                    }
                    return new BEREnumerated(stream, bytesRead);
                default:
                    return super.getElement(decoder, tag, stream, bytesRead, implicit);
                }
            }
        }
    }
}