package com.seb.middleware.ldap;

import javax.naming.NamingException;
import javax.naming.ldap.Control;
import javax.naming.ldap.ControlFactory;

public class PasswordResponseControlFactory extends ControlFactory
{
    public PasswordResponseControlFactory()
    {
    }

    public Control getControlInstance(Control[] controls)
        throws NamingException
    {

    	for(Control control : controls){
    		String id = control.getID();

            if (id.equals(PasswordResponseControl.OID))
            {
                return new PasswordResponseControl(control.isCritical(), control.getEncodedValue());
            }
    	}
        
        return null;
    }

	@Override
	public Control getControlInstance(Control ctl) throws NamingException {
		return null;
	}
}