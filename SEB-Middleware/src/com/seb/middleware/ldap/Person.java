package com.seb.middleware.ldap;

public class Person implements Cloneable {
	private String email;
	private String name;
	private String userPassword;
	private String homePhone;
	private String officePhone;
	private String mobilePhone;
	private String nricPassport;
	
	public Person() {
		super();
	}
	public Person(String email, String name, String userPassword,
			String homePhone, String officePhone, String mobilePhone,
			String nricPassport) {
		super();
		this.email = email;
		this.name = name;
		this.userPassword = userPassword;
		this.homePhone = homePhone;
		this.officePhone = officePhone;
		this.mobilePhone = mobilePhone;
		this.nricPassport = nricPassport;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	public String getOfficePhone() {
		return officePhone;
	}
	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getNricPassport() {
		return nricPassport;
	}
	public void setNricPassport(String nricPassport) {
		this.nricPassport = nricPassport;
	}
	
	@Override
	public Person clone(){
		Person clone = new Person(
				getEmail(), getName(), 
				getUserPassword(), getHomePhone(), 
				getOfficePhone(), getMobilePhone(), 
				getNricPassport());
		
		return clone;
	}
	
	@Override
	public String toString() {
		return "Person [email=" + email + ", name=" + name + ", homePhone="
				+ homePhone + ", officePhone=" + officePhone + ", mobilePhone="
				+ mobilePhone + ", nricPassport=" + nricPassport + "]";
	}	
}
