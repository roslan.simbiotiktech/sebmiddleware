package com.seb.middleware.payment;

import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.PaymentReferenceDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceEntryDAO;
import com.seb.middleware.jpa.helper.PaymentHelper;
import com.seb.middleware.jpa.helper.SettingHelper;

public class PaymentBillerServlet extends HttpServlet {

	private static final long serialVersionUID = -8085095457683376492L;
	private static final Logger logger = LogManager.getLogger(PaymentBillerServlet.class);

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		doPost(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) {

		logger.info("Received Biller checking request.");
		DatabaseFacade db = new DatabaseFacade();
		try {

			PaymentHelper pHelper = new PaymentHelper(db);
			SettingHelper sHelper = new SettingHelper(db);
			int sapTimeout = sHelper.getSAPTimeoutSeconds();
			List<PaymentReferenceDAO> unbillTransaction = pHelper.getUnbillPayment(sapTimeout);
			if(unbillTransaction != null && !unbillTransaction.isEmpty()){
				logger.debug("total " + unbillTransaction.size() + " unbill transaction");
				for(PaymentReferenceDAO payment : unbillTransaction){
					bill(payment);
				}
			}
			
			logger.info("End Process Biller checking request.");
		} finally {
			db.close();
		}
	}
	
	private void bill(PaymentReferenceDAO payment){
		
		logger.debug(payment);
		
		DatabaseFacade db = new DatabaseFacade();
		try {
			PaymentHelper pHelper = new PaymentHelper(db);
			SettingHelper settingHelper = new SettingHelper(db);
			
			PaymentReferenceDAO pay = pHelper.getPaymentReference(payment.getPaymentId());
			List<PaymentReferenceEntryDAO> entries = pHelper.getPaymentEntries(pay.getPaymentId());
			
			db.beginTransaction();
			pHelper.billTransaction(pay, entries, settingHelper);
			db.commit();
		} catch (DatabaseFacadeException e) {
			logger.error("error billing, unrecoverable error");
			logger.error(payment);
			logger.error(e.getMessage(), e);
		} finally {
			db.close();
		}
	}
}
