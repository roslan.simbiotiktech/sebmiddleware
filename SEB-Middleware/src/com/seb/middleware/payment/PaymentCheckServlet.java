package com.seb.middleware.payment;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.PaymentGatewayDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceEntryDAO;
import com.seb.middleware.jpa.helper.PaymentHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.payment.fpx.FPXFacade;
import com.seb.middleware.payment.fpx.FPXPkiException;
import com.seb.middleware.payment.mpg.MPGFacade;
import com.seb.middleware.payment.spg.SPGFacade;

public class PaymentCheckServlet extends HttpServlet {

	private static final long serialVersionUID = -1564136513719356794L;
	private static final Logger logger = LogManager.getLogger(PaymentCheckServlet.class);

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		doPost(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) {

		logger.info("Received Payment Check request.");
		DatabaseFacade db = new DatabaseFacade();
		try {

			PaymentHelper pHelper = new PaymentHelper(db);
			SettingHelper sHelper = new SettingHelper(db);
			
			List<PaymentReferenceDAO> pendingTransaction = pHelper.getPendingPayment(sHelper.getCheckPendingPaymentExpireMinute());
			if(pendingTransaction != null && !pendingTransaction.isEmpty()){
				logger.debug("total " + pendingTransaction.size() + " pending transaction");
				for(PaymentReferenceDAO payment : pendingTransaction){
					checkPayment(payment);
				}
			}
			
			logger.info("End Process Payment Check request.");
		} finally {
			db.close();
		}
	}
	
	private void checkPayment(PaymentReferenceDAO payment){
		
		logger.debug(payment);
		
		try {
			if(payment.getGatewayId().equals(PaymentHelper.FPX_GATEWAY_ID)){
				checkFpxPayment(payment);
			}else if(payment.getGatewayId().equals(PaymentHelper.MPG_GATEWAY_ID)){
				checkMpgPayment(payment);
			}else if(payment.getGatewayId().equals(PaymentHelper.SPG_GATEWAY_ID)){
				checkSpgPayment(payment);
			}
		} catch (Exception e) {
			logger.error("error checking payment - " + e.getMessage(), e);
		}
	}
	
	private void checkMpgPayment(PaymentReferenceDAO reference) throws NoSuchAlgorithmException, IOException, ParseException, DatabaseFacadeException{
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			PaymentHelper pHelper = new PaymentHelper(db);
			List<PaymentReferenceEntryDAO> entries = pHelper.getPaymentEntries(reference.getPaymentId());
			PaymentGatewayDAO gateway = pHelper.getMPGPaymentGateway();
			
			boolean updated = MPGFacade.checkPaymentStatus(pHelper, gateway, reference, entries);
			if (updated) {
				String referenceNo = MPGFacade.getReferenceNo(reference.getReferencePrefix(), reference.getPaymentId());
				MPGFacade.completePayment(reference.getPaymentId(), referenceNo);
			}
		}finally{
			db.close();
		}
	}
	
	private void checkFpxPayment(PaymentReferenceDAO reference) throws DatabaseFacadeException, IOException, FPXPkiException, ParseException{
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			PaymentHelper pHelper = new PaymentHelper(db);
			PaymentGatewayDAO gateway = pHelper.getFPXPaymentGateway();
			
			boolean updated = FPXFacade.checkPaymentStatus(pHelper, gateway, reference);
			if (updated) {
				String referenceNo = FPXFacade.getReferenceNo(reference.getReferencePrefix(), reference.getPaymentId());
				FPXFacade.completePayment(reference.getPaymentId(), referenceNo);
			}
		}finally{
			db.close();
		}
	}
	
	private void checkSpgPayment(PaymentReferenceDAO reference) throws NoSuchAlgorithmException, IOException{
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			PaymentHelper pHelper = new PaymentHelper(db);
			PaymentGatewayDAO gateway = pHelper.getSPGPaymentGateway();
			
			boolean updated = SPGFacade.checkPaymentStatus(pHelper, gateway, reference);
			if (updated && reference.getStatus().isShouldCompletePayment()) {
				String referenceNo = SPGFacade.getReferenceNo(reference);
				SPGFacade.completePayment(reference.getPaymentId(), referenceNo);
			}
		}finally{
			db.close();
		}
	}
}
