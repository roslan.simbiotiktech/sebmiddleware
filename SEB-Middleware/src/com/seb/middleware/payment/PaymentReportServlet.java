package com.seb.middleware.payment;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.email.EmailManager;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.PaymentGatewayDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceEntryDAO;
import com.seb.middleware.jpa.helper.PaymentHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.payment.spg.SPGFacade;
import com.seb.middleware.utility.StringUtil;

/**
 * This servlet sends exceptional report for all the unsuccessful billing records
 */
public class PaymentReportServlet extends HttpServlet {
	
	private static final long serialVersionUID = -3690087604848905487L;
	private static final Logger logger = LogManager.getLogger(PaymentReportServlet.class);
	private static DecimalFormat df = new DecimalFormat("#,##0.00");
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		doPost(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) {

		logger.info("Received Billing Report request.");
		DatabaseFacade db = new DatabaseFacade();
		try {
			
			PaymentHelper pHelper = new PaymentHelper(db);
			SettingHelper sHelper = new SettingHelper(db);
			List<PaymentReferenceDAO> unbillTransaction = pHelper.getAllUnbillPayment(sHelper.getSAPTimeoutSeconds());
			if(unbillTransaction != null && !unbillTransaction.isEmpty()){
				logger.debug("total " + unbillTransaction.size() + " unbill transaction");
				
				StringBuilder sb = new StringBuilder();
				for(int i = 0; i < unbillTransaction.size(); i++){
					
					PaymentReferenceDAO transaction = unbillTransaction.get(i);
					List<PaymentReferenceEntryDAO> entries = pHelper.getPaymentEntries(transaction.getPaymentId());
					
					boolean billingStarted = false;
					for(PaymentReferenceEntryDAO entry : entries){
						if(entry.getBillingAttemptCount() > 0){
							billingStarted = true;
							break;
						}
					}
					
					if(billingStarted){
						sb.append(getRow(i+1, transaction, entries, pHelper));	
					}
				}
				
				if(sb.length() > 0){
					SettingHelper setting = new SettingHelper(db);
					String emails = setting.getSAPFileProcessingNotifyRecipients();
					
					if(!StringUtil.isEmpty(emails)){
						String [] recipients = emails.split(",");
						ArrayList<String> list = new ArrayList<String>();
						for(String s : recipients){
							String eml = s.trim();
							if(!StringUtil.isDigit(eml)){
								list.add(eml);	
							}
						}
						
						if(!list.isEmpty()){
							EmailManager manager = new EmailManager();
							manager.sendSAPUpdatePaymentErrorReport(list, sb.toString());
						}else{
							logger.error("PaymentReportServlet() invalid, unable to send report. " + emails);
						}
					}else{
						logger.error("PaymentReportServlet() null email recipients, unable to send report");
					}
				}else{
					logger.info("PaymentReportServlet() all skipped due to billing not started");
				}
			}
			
			logger.info("End Process Billing Report request.");
		} catch (IOException e) {
			logger.error("error - " + e.getMessage(), e);
		} catch (MessagingException e) {
			logger.error("error - " + e.getMessage(), e);
		} catch (NamingException e) {
			logger.error("error - " + e.getMessage(), e);
		} finally {
			db.close();
		}
	}
	
	private String getRow(int counter, PaymentReferenceDAO reference, List<PaymentReferenceEntryDAO> entries, PaymentHelper pHelper){
		
		PaymentGatewayDAO gateway = pHelper.getPaymentGateway(reference.getGatewayId());
		
		StringBuilder sb = new StringBuilder();
		
		PaymentReferenceEntryDAO firstEntry = entries.get(0);
		int colSpan = entries.size();
		
		sb.append("<tr>");
		sb.append(getCell(String.valueOf(counter), colSpan));
		sb.append(getCell(SPGFacade.getReferenceNo(reference), colSpan));
		sb.append(getCell(gateway.getGatewayId(), colSpan));
		sb.append(getCell(reference.getGatewayReference(), colSpan));
		sb.append(getCell(reference.getUserEmail(), colSpan));
		sb.append(getCell(sdf.format(reference.getPaymentDate()), colSpan));
		sb.append(getCell(firstEntry.getContractAccountNumber()));
		sb.append(getCell(df.format(firstEntry.getAmount())));
		sb.append(getCell(String.valueOf(firstEntry.getBillingAttemptCount())));
		sb.append(getCell(firstEntry.getBillingAttemptLastError()));
		sb.append("</tr>");
		
		if(entries.size() > 1){
			// Start from 2nd
			for(int i = 1; i < entries.size(); i++){
				PaymentReferenceEntryDAO entry = entries.get(i);
				sb.append("<tr>");
				sb.append(getCell(entry.getContractAccountNumber()));
				sb.append(getCell(df.format(entry.getAmount())));
				sb.append(getCell(String.valueOf(entry.getBillingAttemptCount())));
				sb.append(getCell(entry.getBillingAttemptLastError()));
				sb.append("</tr>");		
			}
		}
		
		return sb.toString();
	}
	
	private String getCell(String content){
		return getCell(content, 0);
	}
	
	private String getCell(String content, int rowSpan){
		StringBuilder sb = new StringBuilder();
		
		sb.append("<td style='padding:10px'");
		if(rowSpan > 0){
			sb.append(" rowspan='").append(rowSpan).append("'");
		}
		sb.append(">");
		sb.append(content);
		sb.append("</td>");
		
		return sb.toString();
	}
}
