package com.seb.middleware.payment.fpx;

public class Bank {

	private String id;
	
	private String status;

	public Bank(String id, String status) {
		super();
		this.id = id;
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Bank [id=" + id + ", status=" + status + "]";
	}	
}
