package com.seb.middleware.payment.fpx;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.utility.StringUtil;

public class BankListQuery {

	private static final Logger logger = LogManager.getLogger(BankListQuery.class);

	public static void main(String[] args) throws KeyManagementException, NoSuchAlgorithmException {
		//new BankListQuery().test();
		List<Bank> bankList = new BankListQuery().get();
		System.out.println(bankList);
	}

	private String keyFile;

	private String exchangeId;

	private String url;

	boolean isTesting;

	public BankListQuery() {
		keyFile = "C:\\apache\\apache-tomcat-9.0.0.M1\\webapps\\Java_SMI_SampleSourceCode\\certandkey\\EX00004669.key";
		exchangeId = "EX00004669";
		url = "https://uat.mepsfpx.com.my/FPXMain/RetrieveBankList";
		isTesting = true;

	}

	public void test() throws KeyManagementException, NoSuchAlgorithmException{
		enableSelfSignedCertification();
		String message = "fpx_msgType=AE&fpx_msgToken=01&fpx_sellerExId=EX00004669&fpx_sellerExOrderNo=20160613122830&fpx_sellerTxnTime=20160613122830&fpx_sellerOrderNo=20160613122830&fpx_sellerId=SE00005254&fpx_sellerBankCode=01&fpx_txnCurrency=MYR&fpx_txnAmount=100.00&fpx_buyerEmail=peirhwa87%40gmail.com&fpx_buyerName=SOo+peir+hwa&fpx_buyerBankId=TEST0021&fpx_buyerBankBranch=&fpx_buyerAccNo=&fpx_buyerId=&fpx_makerName=&fpx_buyerIban=&fpx_productDesc=Product+description&fpx_version=5.0&fpx_checkSum=93727963E3A30B4D59CE0FC00143FED23C8FEC10A74A12BEECB2217709F4B2391A3BBCF0D613F0FD44136D1E78A69DE5C839E61B97CE2E9290F3219C9DAD07C52F715E72B7733531CEAA63CF915491C5E08A90CB1CFF696484DA6EBE2A2F00B5206D46C3AF33EA544FFB49BEA2559496B34BFB687E1FFFCF7659A1BFB2976E7E25767390FB940A08CD17E6D1DE69690640C8E65430173A9A6C7C2148564B7FA90875811F5FBAB313E517278810B6501E131D2D37FAAC96B4F7F0A9A117E9782EE6E9DE9F8E0259B7E37856FEC0B516672F95E70F8BCEBD4D903274B1F051115F0368A75553BA44199A1C5F5B74878D0556AA9CC56C2E010F4ACBD8478C71EF84";
		String result = post("https://uat.mepsfpx.com.my/FPXMain/sellerNVPTxnStatus.jsp", message);
		System.out.println(result);
		
	}
	
	public List<Bank> get() {

		try {
			String messageType = "BE";
			String messageToken = "01";
			String version = "5.0";

			String checkSumStr = messageToken + "|" + messageType + "|" + exchangeId + "|" + version;
			String checksum = FPXPkiImplementation.signData(keyFile, checkSumStr, "SHA1withRSA");

			StringBuilder request = new StringBuilder();

			request.append("fpx_msgType=").append(messageType).append("&");
			request.append("fpx_msgToken=").append(messageToken).append("&");
			request.append("fpx_sellerExId=").append(exchangeId).append("&");
			request.append("fpx_version=").append(version).append("&");
			request.append("fpx_checkSum=").append(checksum);

			if (isTesting) {
				enableSelfSignedCertification();
			}

			String response = post(url, request.toString());

			Map<String, String> map = getResponseMap(response);
			String bankList = map.get("fpx_bankList");
			return getBanks(bankList);
		} catch (FPXPkiException e) {
			logger.error("error query fpx banks", e);
			throw new RuntimeException("error query fpx banks - " + e.getMessage());
		} catch (KeyManagementException e) {
			logger.error("error query fpx banks", e);
			throw new RuntimeException("error query fpx banks - " + e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			logger.error("error query fpx banks", e);
			throw new RuntimeException("error query fpx banks - " + e.getMessage());
		} catch (UnsupportedEncodingException e) {
			logger.error("error query fpx banks", e);
			throw new RuntimeException("error query fpx banks - " + e.getMessage());
		}
	}

	private List<Bank> getBanks(String bankList) {
		List<Bank> banks = new ArrayList<>();

		if (!StringUtil.isEmpty(bankList)) {
			String[] bks = bankList.split(",");

			for (String bk : bks) {
				String[] bank = bk.split("~");
				Bank b = new Bank(bank[0], bank[1]);
				banks.add(b);
			}
		}

		return banks;
	}

	private Map<String, String> getResponseMap(String response) throws UnsupportedEncodingException {

		Map<String, String> map = new HashMap<>();

		if (!StringUtil.isEmpty(response)) {
			String[] valuePairs = response.split("&");

			for (String vps : valuePairs) {

				String[] vp = vps.split("=");
				String name = URLDecoder.decode(vp[0], "UTF-8");
				String value = URLDecoder.decode(vp[1], "UTF-8");

				map.put(name, value);
			}
		}

		return map;
	}

	private void enableSelfSignedCertification() throws NoSuchAlgorithmException, KeyManagementException {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
			}
		} };

		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		});
	}

	private String post(String url, String body) {

		HttpURLConnection connection = null;
		try {
			URL u = new URL(url);
			connection = (HttpURLConnection) u.openConnection();

			connection.setRequestMethod("POST");
			connection.setUseCaches(false);
			connection.setDoOutput(true);
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);

			try (OutputStream os = connection.getOutputStream()) {
				os.write(body.getBytes());
			}

			try (InputStream is = connection.getInputStream()) {
				return IOUtils.toString(is, "UTF-8");
			}
		} catch (IOException e) {
			logger.error("error connecting to the remote server - " + e.getMessage(), e);
			return null;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}
}
