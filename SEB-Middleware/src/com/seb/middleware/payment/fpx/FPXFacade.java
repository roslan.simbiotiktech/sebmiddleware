package com.seb.middleware.payment.fpx;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.naming.NamingException;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.constant.PaymentStatus;
import com.seb.middleware.email.EmailManager;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.FPXBankActiveDAO;
import com.seb.middleware.jpa.dao.PaymentGatewayDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceEntryDAO;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.PaymentHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.utility.ResourceUtil;
import com.seb.middleware.utility.StringUtil;

public class FPXFacade {

	private static final Logger logger = LogManager.getLogger(FPXFacade.class);

	private static final String QUERY_URL_NAME = "QUERY_URL";
	private static final String BANK_LIST_URL_NAME = "BANK_LIST_URL";

	public static void updateBankList() throws FPXPkiException, IOException {

		DatabaseFacade db = new DatabaseFacade();

		try {
			PaymentHelper pHelper = new PaymentHelper(db);
			PaymentGatewayDAO gateway = pHelper.getFPXPaymentGateway();
			Map<String, String> params = pHelper.getPaymentParameters(gateway.getGatewayId());
			String queryUrl = pHelper.getPaymentUrl(gateway.getGatewayId(), BANK_LIST_URL_NAME);

			String msgType = "BE";
			String msgToken = "01";
			String sellerExId = params.get("EXCHANGE_ID");
			String version = params.get("VERSION");

			List<String> checksumFields = new ArrayList<>();
			checksumFields.add(msgToken);
			checksumFields.add(msgType);
			checksumFields.add(sellerExId);
			checksumFields.add(version);

			String checksum = FPXPkiImplementation.signData(params.get("KEY_PATH"), getChecksum(checksumFields), "SHA1withRSA");

			StringBuilder sb = new StringBuilder();
			sb.append("fpx_msgType=").append(URLEncoder.encode(msgType, "UTF-8")).append("&");
			sb.append("fpx_msgToken=").append(URLEncoder.encode(msgToken, "UTF-8")).append("&");
			sb.append("fpx_sellerExId=").append(URLEncoder.encode(sellerExId, "UTF-8")).append("&");
			sb.append("fpx_checkSum=").append(URLEncoder.encode(checksum, "UTF-8")).append("&");
			sb.append("fpx_version=").append(URLEncoder.encode(version, "UTF-8")).append("&");

			String body = sb.toString();
			logger.debug(queryUrl);
			logger.debug(body);

			String response = post(queryUrl, body);
			Map<String, String> map = getResponseMap(response);
			
			logger.debug(response);
			
			String bankList = map.get("fpx_bankList");
			List<FPXBankActiveDAO> activeBanks = getBanks(bankList);
			if (activeBanks.isEmpty()) {
				logger.debug("Active bank list is empty");
			} else {
				db.beginTransaction();
				
				db.createQuery("delete from FPXBankActiveDAO").executeUpdate();
				
				for(FPXBankActiveDAO b : activeBanks){
					db.insert(b);
					logger.debug("FPX Bank: "+ b.toString());
				}
				
				db.commit();
				logger.debug("Update successfully - " + activeBanks.size());
			}
 
		} catch (DatabaseFacadeException e) {
			logger.error("error updating fpx active bank list");
		} finally {
			db.close();
		}
	}

	public static void completePayment(final long paymentId, final String referenceNo) {

		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {

				logger.debug("FPX Billing Starting");
				logger.debug(referenceNo);
				DatabaseFacade db = new DatabaseFacade();
				try {
					PaymentHelper pHelper = new PaymentHelper(db);
					MemberHelper mHelper = new MemberHelper(db);
					SettingHelper settingHelper = new SettingHelper(db);
					
					PaymentReferenceDAO reference = pHelper.getPaymentReference(paymentId);
					List<PaymentReferenceEntryDAO> entries = pHelper.getPaymentEntries(reference.getPaymentId());
					
					PaymentGatewayDAO gateway = pHelper.getFPXPaymentGateway();

					Person person = mHelper.getPerson(reference.getUserEmail());
					
					logger.debug("Billing : " + referenceNo);
					
					if (reference.getStatus() == PaymentStatus.FAILED) {
						// Notify failed email
						notifyPaymentStatus(gateway, reference, entries, person);
					} else if (reference.getStatus() == PaymentStatus.RECEIVED) {
						// Notify success email and update sap
						notifyPaymentStatus(gateway, reference, entries, person);
						
						pHelper.billTransaction(reference, entries, settingHelper);
						
						db.beginTransaction();
						db.update(reference);
						db.commit();
					} else {
						logger.error("completePayment with unexpected payment status reached - " + reference.getStatus() + ", "
								+ reference.getPaymentId());
					}
				} catch (NamingException e) {
					logger.error("error getting Person from LDAP - " + e.getMessage(), e);
				} catch (DatabaseFacadeException e) {
					logger.error("unrecover error occured, payment status updated to SAP but failed to update to local db");
					logger.error(e.getMessage(), e);
					logger.error(referenceNo);
				} finally {
					db.close();
				}
			}
		});
		t.start();
	}

	/**
	 * This sends an AE message
	 */
	public static boolean checkPaymentStatus(PaymentHelper pHelper, PaymentGatewayDAO gateway, PaymentReferenceDAO reference)
			throws IOException, FPXPkiException, ParseException {

		Map<String, String> params = pHelper.getPaymentParameters(gateway.getGatewayId());
		String queryUrl = pHelper.getPaymentUrl(gateway.getGatewayId(), QUERY_URL_NAME);
		Map<String, String> datas = pHelper.getPaymentReferenceData(reference.getPaymentId());

		String msgType = "AE";
		String msgToken = datas.get("fpx_msgToken");
		String sellerExId = datas.get("fpx_sellerExId");
		String sellerExOrderNo = datas.get("fpx_sellerExOrderNo");
		String sellerTxnTime = datas.get("fpx_sellerTxnTime");
		String sellerOrderNo = datas.get("fpx_sellerOrderNo");
		String sellerId = datas.get("fpx_sellerId");
		String sellerBankCode = datas.get("fpx_sellerBankCode");
		String txnCurrency = datas.get("fpx_txnCurrency");
		String txnAmount = datas.get("fpx_txnAmount");
		String buyerEmail = datas.get("fpx_buyerEmail");
		String buyerName = datas.get("fpx_buyerName");
		String buyerBankId = datas.get("fpx_buyerBankId");
		String buyerBankBranch = datas.get("fpx_buyerBankBranch");
		String buyerAccNo = datas.get("fpx_buyerAccNo");
		String buyerId = datas.get("fpx_buyerId");
		String makerName = datas.get("fpx_makerName");
		String buyerIban = datas.get("fpx_buyerIban");
		String productDesc = datas.get("fpx_productDesc");
		String version = datas.get("fpx_version");

		List<String> checksumFields = new ArrayList<>();
		checksumFields.add(buyerAccNo);
		checksumFields.add(buyerBankBranch);
		checksumFields.add(buyerBankId);
		checksumFields.add(buyerEmail);
		checksumFields.add(buyerIban);
		checksumFields.add(buyerId);
		checksumFields.add(buyerName);
		checksumFields.add(makerName);
		checksumFields.add(msgToken);
		checksumFields.add(msgType);
		checksumFields.add(productDesc);
		checksumFields.add(sellerBankCode);
		checksumFields.add(sellerExId);
		checksumFields.add(sellerExOrderNo);
		checksumFields.add(sellerId);
		checksumFields.add(sellerOrderNo);
		checksumFields.add(sellerTxnTime);
		checksumFields.add(txnAmount);
		checksumFields.add(txnCurrency);
		checksumFields.add(version);

		String checksum = FPXPkiImplementation.signData(params.get("KEY_PATH"), getChecksum(checksumFields), "SHA1withRSA");

		StringBuilder sb = new StringBuilder();
		sb.append("fpx_msgType=").append(URLEncoder.encode(msgType, "UTF-8")).append("&");
		sb.append("fpx_msgToken=").append(URLEncoder.encode(msgToken, "UTF-8")).append("&");
		sb.append("fpx_sellerExId=").append(URLEncoder.encode(sellerExId, "UTF-8")).append("&");
		sb.append("fpx_sellerExOrderNo=").append(URLEncoder.encode(sellerExOrderNo, "UTF-8")).append("&");
		sb.append("fpx_sellerTxnTime=").append(URLEncoder.encode(sellerTxnTime, "UTF-8")).append("&");
		sb.append("fpx_sellerOrderNo=").append(URLEncoder.encode(sellerOrderNo, "UTF-8")).append("&");
		sb.append("fpx_sellerBankCode=").append(URLEncoder.encode(sellerBankCode, "UTF-8")).append("&");
		sb.append("fpx_txnCurrency=").append(URLEncoder.encode(txnCurrency, "UTF-8")).append("&");
		sb.append("fpx_txnAmount=").append(URLEncoder.encode(txnAmount, "UTF-8")).append("&");
		sb.append("fpx_buyerEmail=").append(URLEncoder.encode(buyerEmail, "UTF-8")).append("&");
		sb.append("fpx_checkSum=").append(URLEncoder.encode(checksum, "UTF-8")).append("&");
		sb.append("fpx_buyerName=").append(URLEncoder.encode(buyerName, "UTF-8")).append("&");
		sb.append("fpx_buyerBankId=").append(URLEncoder.encode(buyerBankId, "UTF-8")).append("&");
		sb.append("fpx_buyerBankBranch=").append(URLEncoder.encode(buyerBankBranch, "UTF-8")).append("&");
		sb.append("fpx_buyerAccNo=").append(URLEncoder.encode(buyerAccNo, "UTF-8")).append("&");
		sb.append("fpx_buyerId=").append(URLEncoder.encode(buyerId, "UTF-8")).append("&");
		sb.append("fpx_makerName=").append(URLEncoder.encode(makerName, "UTF-8")).append("&");
		sb.append("fpx_buyerIban=").append(URLEncoder.encode(buyerIban, "UTF-8")).append("&");
		sb.append("fpx_productDesc=").append(URLEncoder.encode(productDesc, "UTF-8")).append("&");
		sb.append("fpx_version=").append(URLEncoder.encode(version, "UTF-8")).append("&");
		sb.append("fpx_sellerId=").append(URLEncoder.encode(sellerId, "UTF-8"));

		String body = sb.toString();

		logger.debug(queryUrl);
		logger.debug(body);

		String paymentStatus = post(queryUrl, body);
		logger.debug(paymentStatus);
		if (paymentStatus != null) {
			String fpxResponse = paymentStatus.trim();
			if (fpxResponse.equals("msgfromfpx= PROSESSING ERROR") || fpxResponse.equals("ERROR")) {
				logger.warn("error response from FPX - " + fpxResponse);
				return false;
			} else {
				return processFpxQueryResponse(fpxResponse, reference);
			}
		} else {
			logger.warn("error response from FPX - " + paymentStatus);
			return false;
		}
	}

	/**
	 * Manual parsing FPX broken HTML format
	 * @throws UnsupportedEncodingException 
	 */
	public static Map<String, String> processACResponse(PaymentHelper pHelper, String response) throws UnsupportedEncodingException {

		if (StringUtil.isEmpty(response)) {
			logger.warn("AC response is empty");
			return null;
		} else {
			Map<String, String> map = getResponseMap(response);
			if (!map.isEmpty()) {
				return map;
			} else {
				logger.error("empty parsing result");
				return null;
			}
		}
	}
	
	public synchronized static boolean processFPXMessage(PaymentReferenceDAO reference, Map<String, String> map){
		
		DatabaseFacade db = new DatabaseFacade();
		try{
			
			PaymentHelper pHelper = new PaymentHelper(db);
			PaymentReferenceDAO synchronizedReference = pHelper.getPaymentReference(reference.getPaymentId());
			
			boolean updated = processFPXMessage(pHelper, synchronizedReference, map);
			
			if (updated) {
				db.beginTransaction();
				db.update(synchronizedReference);
				db.commit();
				
				logger.info("FPX Status updated. " + synchronizedReference.toString());
				
				reference.setPaymentDate(synchronizedReference.getPaymentDate());
				reference.setStatus(synchronizedReference.getStatus());
				reference.setGatewayReference(synchronizedReference.getGatewayReference());
				reference.setGatewayStatus(synchronizedReference.getGatewayStatus());
			}
			
			return updated;
			
		} catch (ParseException e) {
			logger.error("error parsing data - " + e.getMessage(), e);
			return false;
		} catch (DatabaseFacadeException e) {
			logger.error("error update FPX transaction status - " + e.getMessage(), e);
			return false;
		}finally{
			db.close();
		}
	}
	
	private static boolean processFPXMessage(PaymentHelper pHelper, PaymentReferenceDAO reference, Map<String, String> map) throws ParseException {
		if (verifyData(pHelper, map)) {
			String fpxDebitAuthCode = map.get("fpx_debitAuthCode");
			if (fpxDebitAuthCode != null) {
				if(fpxDebitAuthCode.equals(reference.getGatewayStatus())){
					logger.debug("Transaction status already received previously");
					return false;
				}else if (fpxDebitAuthCode.equals("00")) {
					if (reference.getStatus() == PaymentStatus.PENDING || reference.getStatus() == PaymentStatus.FAILED) {

						SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

						String fpxTransactionId = map.get("fpx_fpxTxnId");
						String fpxTransactionTime = map.get("fpx_fpxTxnTime");

						reference.setPaymentDate(sdf.parse(fpxTransactionTime));
						reference.setGatewayReference(fpxTransactionId);
						reference.setStatus(PaymentStatus.RECEIVED);
						
						logger.debug("payment successful " + fpxTransactionId);
					}else if(reference.getStatus() == PaymentStatus.RECEIVED || reference.getStatus() == PaymentStatus.BILLED){
						return false;
					}
				} else if (fpxDebitAuthCode.equals("99")) {
					logger.debug("Pending authorization");
				} /*else if (fpxDebitAuthCode.equals("76")) {
					logger.debug("No transaction found");
				}*/ else if (fpxDebitAuthCode.equals("09")) {
					logger.debug("Transaction in progress");
				} else {
					logger.debug("Transaction error - " + fpxDebitAuthCode);
					if (reference.getStatus() == PaymentStatus.FAILED) {
						return false;
					}else{
						if (reference.getStatus() == PaymentStatus.RECEIVED 
								|| reference.getStatus() == PaymentStatus.BILLED) {
							
							logger.debug("Duplicated update received");
							if(fpxDebitAuthCode != null && 
									(fpxDebitAuthCode.equals("72") || fpxDebitAuthCode.equalsIgnoreCase("XO"))){
								logger.debug("Ignored 72/XO duplicate exchange id response");
								return false;
							}
						}
						
						String fpxTransactionId = map.get("fpx_fpxTxnId");
						String fpxTransactionTime = map.get("fpx_fpxTxnTime");

						if(!StringUtil.isEmpty(fpxTransactionId)){
							reference.setGatewayReference(fpxTransactionId);
						}
						
						if(!StringUtil.isEmpty(fpxTransactionTime)){
							SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
							reference.setPaymentDate(sdf.parse(fpxTransactionTime));	
						}
						
						reference.setStatus(PaymentStatus.FAILED);
					}
				}
				
				reference.setGatewayStatus(fpxDebitAuthCode);
				return true;
			}
		}

		return false;
	}

	private static boolean processFpxQueryResponse(String paymentStatus, PaymentReferenceDAO reference)
			throws UnsupportedEncodingException, ParseException {
		
		Map<String, String> map = getResponseMap(paymentStatus);
		return processFPXMessage(reference, map);
	}

	private static boolean verifyData(PaymentHelper pHelper, Map<String, String> map) {

		String fpxChecksumString = map.get("fpx_checkSum");
		if (StringUtil.isEmpty(fpxChecksumString)) {
			logger.debug("checksum is empty");
			return false;
		} else {

			Map<String, String> parameters = pHelper.getPaymentParameters(PaymentHelper.FPX_GATEWAY_ID);

			List<String> checksumFields = new ArrayList<>();
			checksumFields.add(map.get("fpx_buyerBankBranch"));
			checksumFields.add(map.get("fpx_buyerBankId"));
			checksumFields.add(map.get("fpx_buyerIban"));
			checksumFields.add(map.get("fpx_buyerId"));
			checksumFields.add(map.get("fpx_buyerName"));
			checksumFields.add(map.get("fpx_creditAuthCode"));
			checksumFields.add(map.get("fpx_creditAuthNo"));
			checksumFields.add(map.get("fpx_debitAuthCode"));
			checksumFields.add(map.get("fpx_debitAuthNo"));
			checksumFields.add(map.get("fpx_fpxTxnId"));
			checksumFields.add(map.get("fpx_fpxTxnTime"));
			checksumFields.add(map.get("fpx_makerName"));
			checksumFields.add(map.get("fpx_msgToken"));
			checksumFields.add(map.get("fpx_msgType"));
			checksumFields.add(map.get("fpx_sellerExId"));
			checksumFields.add(map.get("fpx_sellerExOrderNo"));
			checksumFields.add(map.get("fpx_sellerId"));
			checksumFields.add(map.get("fpx_sellerOrderNo"));
			checksumFields.add(map.get("fpx_sellerTxnTime"));
			checksumFields.add(map.get("fpx_txnAmount"));
			checksumFields.add(map.get("fpx_txnCurrency"));

			String checksumString = getChecksum(checksumFields);

			String certificatePath = parameters.get("PUBLIC_KEY_PATH");
			String mainCert = parameters.get("MAIN_CERTIFICATE");
			String currentCert = parameters.get("CURRENT_CERTIFICATE");
			
			String pkivarification = FPXPkiImplementation.verifyData(certificatePath, checksumString, fpxChecksumString,
					"SHA1withRSA", mainCert, currentCert);
			if (pkivarification != null && pkivarification.equals("00")) {
				return true;
			} else {
				logger.debug("1: " + checksumString);
				logger.debug("2: " + fpxChecksumString);
				logger.error("verify fpx data error - " + pkivarification);
				return false;
			}
		}
	}

	private static String post(String url, String body) throws IOException {

		HttpURLConnection connection = null;
		try {
			URL u = new URL(url);
			connection = (HttpURLConnection) u.openConnection();

			connection.setRequestMethod("POST");
			connection.setUseCaches(false);
			connection.setDoOutput(true);
			connection.setConnectTimeout(30000);
			connection.setReadTimeout(30000);

			try (OutputStream os = connection.getOutputStream()) {
				os.write(body.getBytes());
			}

			try (InputStream is = connection.getInputStream()) {
				return IOUtils.toString(is, "UTF-8");
			}
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	private static String getChecksum(List<String> checksumFields) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < checksumFields.size(); i++) {

			if (checksumFields.get(i) != null) {
				sb.append(checksumFields.get(i));
			}

			if (i + 1 < checksumFields.size()) {
				sb.append("|");
			}
		}

		return sb.toString();
	}

	public static String getReferenceNo(String prefix, long referenceNo){
		return String.format("%s%08d", prefix, referenceNo);
	}
	
	private static void notifyPaymentStatus(PaymentGatewayDAO gateway, PaymentReferenceDAO reference, 
			List<PaymentReferenceEntryDAO> entries, Person person) {

		try {
			EmailManager manager = new EmailManager();

			String refNo = getReferenceNo(reference.getReferencePrefix(), reference.getPaymentId());
			
			String paymentMethod = ResourceUtil.get("payment.fpx_method", "en") + " - " + reference.getSpgBankName();
			String gatewayReferenceLabel = "FPX Transaction Id";
			
			Map<String, BigDecimal> entriesMap = getEntriesAmountMap(entries);
			
			if (reference.getStatus() == PaymentStatus.FAILED) {
				manager.sendUnsuccessPaymentEmail(
						reference.getUserEmail(), person.getName(), refNo, 
						reference.getCreatedDate(), entriesMap, paymentMethod, 
						gatewayReferenceLabel, reference.getGatewayReference());
			} else {
				
				manager.sendSuccessPaymentEmail(
						reference.getUserEmail(), person.getName(), refNo, 
						reference.getPaymentDate(), entriesMap, paymentMethod, 
						gatewayReferenceLabel, reference.getGatewayReference());
			}
		} catch (IOException | MessagingException | NamingException e) {
			logger.error("error sending payment notification - " + e.getMessage(), e);
		}
	}
	
	private static Map<String, BigDecimal> getEntriesAmountMap(List<PaymentReferenceEntryDAO> entries){
		
		Map<String, BigDecimal> map = new LinkedHashMap<>();
		
		for(PaymentReferenceEntryDAO entry : entries){
			map.put(entry.getContractAccountNumber(), entry.getAmount());
		}
		
		return map;
	}
	
	private static Map<String, String> getResponseMap(String response) throws UnsupportedEncodingException {

		Map<String, String> map = new HashMap<>();

		if (!StringUtil.isEmpty(response)) {
			String[] valuePairs = response.split("&");

			for (String vps : valuePairs) {

				String[] vp = vps.split("=");
				String name = vp.length >= 1 ? URLDecoder.decode(vp[0], "UTF-8") : null;
				String value = vp.length >= 2 ? URLDecoder.decode(vp[1], "UTF-8") : null;

				map.put(name, value);
			}
		}

		return map;
	}

	private static List<FPXBankActiveDAO> getBanks(String bankList) {
		List<FPXBankActiveDAO> banks = new ArrayList<>();

		if (!StringUtil.isEmpty(bankList)) {
			String[] bks = bankList.split(",");

			for (String bk : bks) {
				String[] bank = bk.split("~");
				FPXBankActiveDAO b = new FPXBankActiveDAO();
				b.setBankId(bank[0]);
				b.setStatus(bank[1]);
				banks.add(b);
			}
		}

		return banks;
	}
}
