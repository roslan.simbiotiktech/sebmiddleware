package com.seb.middleware.payment.fpx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMReader;

public class FPXPkiImplementation {

	private static final Logger logger = LogManager.getLogger(FPXPkiImplementation.class);

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	static String errorCode = "ErrorCode : [03]";
	static int cerExpiryCount = 0;

	public static void main(String [] args) throws FPXPkiException{
		String signature = signData("C:\\Users\\PeirHwa\\Desktop\\SPG Test\\EX00006342.key", "01BEEX000063425.0", "SHA1withRSA");
		System.out.println(signature);
	}
	
	public static String signData(String pvtKeyFileName, String dataToSign, String signatureAlg) throws FPXPkiException {

		try {
			PrivateKey privateKey = getPrivateKey(pvtKeyFileName);
			Signature signature = Signature.getInstance(signatureAlg, "BC");
			signature.initSign(privateKey);

			signature.update(dataToSign.getBytes());
			byte[] signatureBytes = signature.sign();

			return byteArrayToHexString(signatureBytes);
		} catch (Exception ex) {

			logger.error("error signing data", ex);
			throw new FPXPkiException();
		}
	}

	public static String verifyData(String pubKeyFileName, String calcCheckSum, String checkSumFromMsg, String signatureAlg, 
			String mainCert, String currentCert) {

		boolean result = false;
		try {
			ArrayList<PublicKey> pubKeys = getFPXPublicKey(pubKeyFileName, mainCert, currentCert);
			Signature verifier = Signature.getInstance(signatureAlg, "BC");
			logger.debug("public keys:" + pubKeys.size());
			for (PublicKey pubKey : pubKeys) {
				verifier.initVerify(pubKey);
				verifier.update(calcCheckSum.getBytes());
				result = verifier.verify(HexStringToByteArray(checkSumFromMsg));
				logger.debug("result [" + result + "]");
				if (result)
					return "00";
				else
					return "Your Data cannot be verified against the Signature. ErrorCode :[09]";

			}

			if (pubKeys.size() == 0 && cerExpiryCount == 1) {
				return "One Certificate Found and Expired. ErrorCode : [07]";
			} else if (pubKeys.size() == 0 && cerExpiryCount == 2) {
				return "Both Certificates Expired . ErrorCode : [08]";
			}
			if (pubKeys.size() == 0) {
				return "Invalid Certificates. ErrorCode : [06]";
			}
		} catch (Exception e) {

			logger.error("error verifyData", e);
			return "ErrorCode : [03]" + e.getMessage();
		} finally {
			cerExpiryCount = 0;
		}

		return errorCode;
	}

	private static PublicKey getPublicKey(X509Certificate X509Cert) {
		return (RSAPublicKey) X509Cert.getPublicKey();
	}

	private static PrivateKey getPrivateKey(String pvtKeyFileName) throws IOException {
		FileReader pvtFileReader = getPVTKeyFile(new File(pvtKeyFileName));
		PEMReader pvtPemReader = getPvtPemReader(pvtFileReader);
		KeyPair keyPair = (KeyPair) pvtPemReader.readObject();

		pvtFileReader.close();
		pvtFileReader = null;
		pvtPemReader.close();
		pvtPemReader = null;
		return keyPair.getPrivate();
	}

	private static FileReader getPVTKeyFile(File pvtFile) {
		FileReader pvtFileReader = null;
		try {
			pvtFileReader = new FileReader(pvtFile);
		} catch (FileNotFoundException e) {

			e.printStackTrace();
			pvtFileReader = null;
		}

		return pvtFileReader;
	}

	private static PEMReader getPvtPemReader(Reader pvtFile) {
		return new PEMReader(pvtFile);
	}

	static char[] hexChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static String byteArrayToHexString(byte b[]) {
		StringBuffer sb = new StringBuffer(b.length * 2);
		for (int i = 0; i < b.length; i++) {
			sb.append(hexChar[(b[i] & 0xf0) >>> 4]);
			sb.append(hexChar[b[i] & 0x0f]);
		}
		return sb.toString();
	}

	public static byte[] HexStringToByteArray(String strHex) {
		byte bytKey[] = new byte[(strHex.length() / 2)];
		int y = 0;
		String strbyte;
		for (int x = 0; x < bytKey.length; x++) {
			strbyte = strHex.substring(y, (y + 2));
			if (strbyte.equals("FF")) {
				bytKey[x] = (byte) 0xFF;
			} else {
				bytKey[x] = (byte) Integer.parseInt(strbyte, 16);
			}
			y = y + 2;
		}
		return bytKey;
	}

	private static X509Certificate getX509Certificate(String pubKeyFileName) throws CertificateException, IOException {
		InputStream inStream = new FileInputStream(pubKeyFileName);
		CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
		X509Certificate cert = (X509Certificate) certFactory.generateCertificate(inStream);
		inStream.close();
		return cert;
	}

	private static ArrayList<PublicKey> getFPXPublicKey(String path, String mainCert, String currentCert) throws CertificateException, IOException, NumberFormatException, ParseException {
		// int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(new
		// Date()));
		ArrayList<String> certFiles = new ArrayList<String>();
		certFiles.add(path + File.separator + currentCert);// Old
																	// Certificate
		certFiles.add(path + File.separator + mainCert); // New Certificate
		ArrayList<PublicKey> publicKeys = null;

		for (String file : certFiles) {
			publicKeys = checkCertExpiry(file, mainCert, currentCert);
			logger.debug(file + "<--->" + publicKeys.size());
			if (publicKeys.size() > 0)
				return publicKeys;
		}

		return publicKeys;
	}

	private static ArrayList<PublicKey> checkCertExpiry(String file, String mainCert, String currentCert) throws NumberFormatException, CertificateException, IOException, ParseException {
		ArrayList<PublicKey> publicKey = new ArrayList<PublicKey>();
		X509Certificate x509Cert = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		int renamestatus;
		try {
			x509Cert = getX509Certificate(file);
		} catch (FileNotFoundException e) {
			logger.debug("*****" + e);
			return publicKey;
		}

		Calendar currentDate = Calendar.getInstance();
		currentDate.setTime(sdf.parse(sdf.format(new Date())));

		Calendar certExpiryDate = Calendar.getInstance();
		certExpiryDate.setTime(sdf.parse(sdf.format(x509Cert.getNotAfter())));
		certExpiryDate.add(Calendar.DAY_OF_MONTH, -1);

		SimpleDateFormat settleSdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS");

		logger.debug(settleSdf.format(certExpiryDate.getTime()) + "<-->" + settleSdf.format(currentDate.getTime()));
		logger.debug(certExpiryDate.getTime() + "<-->" + currentDate.getTime() + "<->" + certExpiryDate.compareTo(currentDate));

		if (certExpiryDate.compareTo(currentDate) == 0) // cert expiry and
														// current date is same
														// day so check is both
														// cert
		{
			logger.debug("Same day do check with both cert");
			String nextFile = getNextCertFile(file, mainCert);
			File nextFileCheck = new File(nextFile);

			if (!file.contains(currentCert) && nextFileCheck.exists()) {
				renamestatus = certRollOver(nextFile, currentCert);
				logger.debug("renstatus [" + renamestatus + "]");

			}
			logger.debug("cert1 [" + nextFile + "] cert2[" + file + "]");
			if (nextFileCheck.exists())
				publicKey.add(getPublicKey(getX509Certificate(nextFile)));

			publicKey.add(getPublicKey(x509Cert));

		} else if (certExpiryDate.compareTo(currentDate) == 1) // Not
																// Expired(Still
																// valid)
		{
			if (file.contains(mainCert)) {

				renamestatus = certRollOver(file, currentCert);
				logger.debug("renstatus [" + renamestatus + "]");

			}

			logger.debug("Still valid  [" + file + "]");
			publicKey.add(getPublicKey(x509Cert));
		} else if (certExpiryDate.compareTo(currentDate) == -1) // Expired
		{

			cerExpiryCount = cerExpiryCount + 1;

			logger.debug("Expired [" + file + "]");
		}

		return publicKey;

	}

	private static int certRollOver(String file, String currentCert) throws NumberFormatException, IOException {
		File old_crt = new File(file);

		File new_crt = new File(old_crt.getParent() + "\\" + currentCert);
		String timestamp = new java.text.SimpleDateFormat("yyyyMMddhmmss").format(new Date());

		File newfile = new File(old_crt.getParent() + "\\" + currentCert + timestamp);

		if (new_crt.exists()) {
			// FPX_CURRENT.cer to FPX_CURRENT.cer_<CURRENT TIMESTAMP>
			logger.debug(new_crt + "old_crt is" + newfile);

			if (new_crt.renameTo(newfile)) {
				logger.debug("File renamed");
			} else {
				logger.debug("Sorry! the file can't be renamed");
				return 01;
			}
		}
		/*
		 * if (!new_crt.exists() && old_crt.exists()) { // FPX.cer to
		 * FPX_CURRENT.cer logger.debug(new_cer + "old_cer is" + old_cer);
		 * if (old_cer.renameTo(new_cer)) {
		 * 
		 * logger.debug("File renamed");
		 * 
		 * } else { logger.debug("Sorry! the file can't be renamed");
		 * return 01; } }
		 */

		return 00;
	}

	private static String getNextCertFile(String strFile, String mainCert) throws NumberFormatException, IOException {
		File file = new File(strFile);
		// String fileName = file.getName();
		// String strYear = fileName.substring(fileName.lastIndexOf(".") - 4,
		// fileName.lastIndexOf("."));
		return file.getParentFile() + File.separator + mainCert;
	}

}
