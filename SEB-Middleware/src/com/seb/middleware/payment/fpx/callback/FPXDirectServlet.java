package com.seb.middleware.payment.fpx.callback;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.PaymentGatewayDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceDAO;
import com.seb.middleware.jpa.helper.PaymentHelper;
import com.seb.middleware.payment.fpx.FPXFacade;
import com.seb.middleware.utility.StringUtil;

public class FPXDirectServlet extends HttpServlet {

	private static final long serialVersionUID = 4417399489594199288L;
	private static final Logger logger = LogManager.getLogger(FPXDirectServlet.class);

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		doPost(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) {

		logger.info("Received FPX Direct Message.");
		String body = getRequestBody(request);

		if (StringUtil.isEmpty(body)) {
			logger.error("FPX Direct message is empty");
		} else {
			logger.debug(body);
			DatabaseFacade db = new DatabaseFacade();

			try {
				PaymentHelper pHelper = new PaymentHelper(db);
				PaymentGatewayDAO gateway = pHelper.getFPXPaymentGateway();
				
				Map<String, String> map = FPXFacade.processACResponse(pHelper, body);

				if(map != null && !map.isEmpty()){
					String referenceNo = map.get("fpx_sellerOrderNo");

					if (!StringUtil.isEmpty(referenceNo)) {
						PaymentReferenceDAO reference = pHelper.getPaymentReference(gateway, referenceNo);

						if (reference == null) {
							logger.error("Payment reference does not found for " + referenceNo);
						} else {

							boolean updated = FPXFacade.processFPXMessage(reference, map);
							if (updated) {
								FPXFacade.completePayment(reference.getPaymentId(), referenceNo);
							}
							
							PrintWriter out = response.getWriter();
							out.write("OK");
						}
					} else {
						logger.error("unable to obtain reference no from AC message");
						logger.error(body);
					}
				}else{
					logger.error("unable to decode input from AC message");
					logger.error(body);
				}
			} catch (IOException e) {
				logger.error("error writing response - " + e.getMessage(), e);
			} finally {
				db.close();
			}
		}

		logger.info("End Proecess FPX Direct Message.");
	}

	protected String getRequestBody(HttpServletRequest request) {
		if (request.getContentLength() > 0) {
			try {
				return IOUtils.toString(request.getInputStream(), "UTF-8");
			} catch (IOException e) {
				logger.error("Error reading request body", e);
			}
		}

		return null;
	}
}
