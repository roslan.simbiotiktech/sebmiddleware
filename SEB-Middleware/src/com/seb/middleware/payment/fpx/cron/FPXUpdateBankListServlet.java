package com.seb.middleware.payment.fpx.cron;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.payment.fpx.FPXFacade;
import com.seb.middleware.payment.fpx.FPXPkiException;

public class FPXUpdateBankListServlet extends HttpServlet {

	private static final long serialVersionUID = 4417399489594199288L;
	private static final Logger logger = LogManager.getLogger(FPXUpdateBankListServlet.class);

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		doPost(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) {

		logger.info("Received Update bank list request.");
		try {
			FPXFacade.updateBankList();
			logger.info("End Proecess Update Bank list request.");
		} catch (FPXPkiException | IOException e) {
			logger.error("Error updating fpx bank list - " + e.getMessage(), e);
		}
	}
}
