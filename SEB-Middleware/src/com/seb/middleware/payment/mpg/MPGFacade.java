package com.seb.middleware.payment.mpg;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.naming.NamingException;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.constant.MPGCardType;
import com.seb.middleware.constant.PaymentStatus;
import com.seb.middleware.email.EmailManager;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.PaymentGatewayDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceEntryDAO;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.PaymentHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.utility.CryptoUtil;
import com.seb.middleware.utility.ResourceUtil;
import com.seb.middleware.utility.StringUtil;

public class MPGFacade {

	private static final Logger logger = LogManager.getLogger(MPGFacade.class);

	private static final String QUERY_URL_NAME = "QUERY_URL";

	public static void completePayment(final long paymentId, final String referenceNo) {

		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {

				logger.debug("MPG Billing Starting");
				
				DatabaseFacade db = new DatabaseFacade();
				try {
					PaymentHelper pHelper = new PaymentHelper(db);
					MemberHelper mHelper = new MemberHelper(db);
					SettingHelper settingHelper = new SettingHelper(db);

					PaymentReferenceDAO reference = pHelper.getPaymentReference(paymentId);
					List<PaymentReferenceEntryDAO> entries = pHelper.getPaymentEntries(reference.getPaymentId());
					
					PaymentGatewayDAO gateway = pHelper.getMPGPaymentGateway();
					Person person = mHelper.getPerson(reference.getUserEmail());

					if (reference.getStatus() == PaymentStatus.FAILED) {
						// Notify failed email
						notifyPaymentStatus(gateway, reference, entries, person);
					} else if (reference.getStatus() == PaymentStatus.RECEIVED) {
						// Notify success email and update sap
						notifyPaymentStatus(gateway, reference, entries, person);
						
						db.beginTransaction();
						pHelper.billTransaction(reference, entries, settingHelper);
						db.commit();
					} else {
						logger.error("completePayment with unexpected payment status reached - " + reference.getStatus() + ", "
								+ reference.getPaymentId());
					}
				} catch (NamingException e) {
					logger.error("error getting Person from LDAP - " + e.getMessage(), e);
				} catch (DatabaseFacadeException e) {
					logger.error("unrecover error occured, payment status updated to SAP but failed to update to local db");
					logger.error(e.getMessage(), e);
					logger.error(referenceNo);
				} finally {
					db.close();
				}
			}
		});
		t.start();
	}

	public static boolean checkPaymentStatus(PaymentHelper pHelper, PaymentGatewayDAO gateway, 
			PaymentReferenceDAO reference, List<PaymentReferenceEntryDAO> entries)
			throws IOException, ParseException, NoSuchAlgorithmException {

		Map<String, String> params = pHelper.getPaymentParameters(gateway.getGatewayId());
		String queryUrl = pHelper.getPaymentUrl(gateway.getGatewayId(), QUERY_URL_NAME);
		DecimalFormat df = new DecimalFormat("0.00");
		
		String merchantAccountNo = getMerchantId(gateway, reference, params);
		String hashKey = getHashKey(reference, params);
		String merchantTransId = getReferenceNo(reference.getReferencePrefix(), reference.getPaymentId());
		String amount = df.format(getTotalAmount(entries));
		String transactionType = "1";
		String responseType = "PLAIN";
		String rawSign = hashKey + merchantAccountNo + merchantTransId + amount;
		String signature = CryptoUtil.sha512HexadecimalLowercase(rawSign);
		
		StringBuilder sb = new StringBuilder();
		sb.append("MERCHANT_ACC_NO=").append(URLEncoder.encode(merchantAccountNo, "UTF-8")).append("&");
		sb.append("MERCHANT_TRANID=").append(URLEncoder.encode(merchantTransId, "UTF-8")).append("&");
		sb.append("AMOUNT=").append(URLEncoder.encode(amount, "UTF-8")).append("&");
		sb.append("TRANSACTION_TYPE=").append(URLEncoder.encode(transactionType, "UTF-8")).append("&");
		sb.append("TXN_SIGNATURE=").append(URLEncoder.encode(signature, "UTF-8")).append("&");
		sb.append("RESPONSE_TYPE=").append(URLEncoder.encode(responseType, "UTF-8"));

		String body = sb.toString();

		logger.debug(queryUrl);
		logger.debug(body);

		String paymentStatus = post(queryUrl, body);
		logger.debug(paymentStatus);
		if (paymentStatus != null) {
			String mpgResponse = paymentStatus.trim().replace("<BR>", "&");
			return processMpgQueryResponse(mpgResponse, reference, entries);
		} else {
			logger.warn("error response from MPG - " + paymentStatus);
			return false;
		}
	}
	
	public static BigDecimal getTotalAmount(List<PaymentReferenceEntryDAO> entries){
		BigDecimal total = BigDecimal.ZERO;
		
		for(PaymentReferenceEntryDAO entry : entries){
			total = total.add(entry.getAmount());
		}
		
		return total;
	}
	
	public static String getHashKey(PaymentReferenceDAO reference, Map<String, String> params){
		MPGCardType cardType = MPGCardType.valueOf(reference.getSpgCardType());
		if(cardType == MPGCardType.A){
			return params.get("HASH_KEY_AMEX");
		}else{
			return params.get("HASH_KEY");
		}
	}
	
	public static String getMerchantId(PaymentGatewayDAO gateway, PaymentReferenceDAO reference, Map<String, String> params){
		MPGCardType cardType = MPGCardType.valueOf(reference.getSpgCardType());
		if(cardType == MPGCardType.A){
			return params.get("MERCHANT_ID_AMEX");
		}else{
			return gateway.getMerchantId();
		}
	}

	public static Map<String, String> processResponse(PaymentHelper pHelper, String response) throws UnsupportedEncodingException {

		if (StringUtil.isEmpty(response)) {
			logger.warn("AC response is empty");
			return null;
		} else {
			Map<String, String> map = getResponseMap(response);
			if (!map.isEmpty()) {
				return map;
			} else {
				logger.error("empty parsing result");
				return null;
			}
		}
	}
	
	public synchronized static boolean processMPGMessage(PaymentReferenceDAO reference, 
			List<PaymentReferenceEntryDAO> entries, Map<String, String> map){
		
		DatabaseFacade db = new DatabaseFacade();
		try{
			
			PaymentHelper pHelper = new PaymentHelper(db);
			PaymentReferenceDAO synchronizedReference = pHelper.getPaymentReference(reference.getPaymentId());
			
			boolean updated = processMPGMessage(pHelper, synchronizedReference, entries, map);
			
			if (updated) {
				db.beginTransaction();
				db.update(synchronizedReference);
				db.commit();
				
				logger.info("MPG Status updated. " + synchronizedReference.toString());
				
				reference.setPaymentDate(synchronizedReference.getPaymentDate());
				reference.setStatus(synchronizedReference.getStatus());
				reference.setGatewayReference(synchronizedReference.getGatewayReference());
				reference.setGatewayStatus(synchronizedReference.getGatewayStatus());
			}
			
			return updated;
			
		} catch (ParseException | NoSuchAlgorithmException | UnsupportedEncodingException e) {
			logger.error("error parsing data - " + e.getMessage(), e);
			return false;
		} catch (DatabaseFacadeException e) {
			logger.error("error update MPG transaction status - " + e.getMessage(), e);
			return false;
		}finally{
			db.close();
		}
	}
	
	private static boolean processMPGMessage(PaymentHelper pHelper, 
			PaymentReferenceDAO reference, List<PaymentReferenceEntryDAO> entries, 
			Map<String, String> map) throws ParseException, NoSuchAlgorithmException, UnsupportedEncodingException {
		if (verifyData(reference, entries, pHelper, map)) {
			String transactionStatus = map.get("TXN_STATUS");
			String responseCode = map.get("RESPONSE_CODE");
			String authorizationId = map.get("AUTH_ID");
			
			logger.debug("transactionStatus = " + transactionStatus);
			logger.debug("responseCode = " + responseCode);
			logger.debug("authorizationId = " + authorizationId);
			
			if (!StringUtil.isEmpty(transactionStatus)) {
				if(transactionStatus.equals(reference.getGatewayStatus())){
					logger.debug("Transaction status already received previously");
					return false;
				}else if (!StringUtil.isEmpty(responseCode) && responseCode.equals("0") &&
						!StringUtil.isEmpty(authorizationId) &&
						transactionStatus.equals("A") || transactionStatus.equals("C") || transactionStatus.equals("S")) {
					if(reference.getGatewayReference() == null || 
							!reference.getGatewayReference().equals(transactionStatus)){
						
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

						String transactionId = map.get("TRANSACTION_ID");
						String transactionDate = map.get("TRAN_DATE");

						reference.setPaymentDate(sdf.parse(transactionDate));
						reference.setGatewayReference(transactionId);
						reference.setStatus(PaymentStatus.RECEIVED);
						
						logger.debug("payment successful " + transactionStatus);
					}else{
						return false;
					}
				} else if (transactionStatus.equals("N")) {
					logger.debug("Pending authorization");
				} else {
					logger.debug("Transaction error");
					if (reference.getStatus() == PaymentStatus.FAILED) {
						return false;
					}else{
						if (reference.getStatus() == PaymentStatus.RECEIVED 
								|| reference.getStatus() == PaymentStatus.BILLED) {
							
							logger.debug("Duplicated update received");
							if(responseCode != null && responseCode.equals("4003")){
								logger.debug("Ignored 4003 duplicate trans id response");
								return false;
							}
						}
						
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

						String transactionId = map.get("TRANSACTION_ID");
						String transactionDate = map.get("TRAN_DATE");
						
						if(!StringUtil.isEmpty(transactionId)){
							reference.setGatewayReference(transactionId);	
						}
						
						if(!StringUtil.isEmpty(transactionDate)){
							reference.setPaymentDate(sdf.parse(transactionDate));
						}
						
						reference.setStatus(PaymentStatus.FAILED);
					}
				}
				
				reference.setGatewayStatus(transactionStatus);
				return true;
			}
		}

		return false;
	}
	
	private static boolean isResponseSignatureValid(
			String signature, String hashKey, String merchantAccountNo, 
			String merchantTransactionId, String transactionAmount, String transactionId,
			String transactionStatus, String responseCode) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		
		StringBuilder sb = new StringBuilder();
		sb.append(getStringOrDefault(hashKey, "").toUpperCase());
		sb.append(getStringOrDefault(merchantAccountNo, ""));
		sb.append(getStringOrDefault(merchantTransactionId, "").toUpperCase());
		sb.append(getStringOrDefault(transactionAmount, ""));
		sb.append(getStringOrDefault(transactionId, ""));
		sb.append(getStringOrDefault(transactionStatus, "").toUpperCase());
		sb.append(getStringOrDefault(responseCode, ""));
		
		String raw = sb.toString();
		String hex = CryptoUtil.sha512HexadecimalLowercase(raw);
		boolean match = hex.equalsIgnoreCase(signature);
		
		if(!match){
			logger.error("Hash does not match!");
			logger.error("Plain Sign : " + raw);
			logger.error("compute hash : " + hex);
			logger.error("maybank hash : " + signature);
			
			return false;
		}else{
			return true;
		}
	}

	private static String getStringOrDefault(String str, String def){
		if(str != null) return str;
		else return def;
	}
	
	private static boolean processMpgQueryResponse(String paymentStatus, 
			PaymentReferenceDAO reference, List<PaymentReferenceEntryDAO> entries)
			throws UnsupportedEncodingException, ParseException {
		
		Map<String, String> map = getResponseMap(paymentStatus);
		return processMPGMessage(reference, entries, map);
	}

	private static boolean verifyData(PaymentReferenceDAO reference, List<PaymentReferenceEntryDAO> entries,
			PaymentHelper pHelper, Map<String, String> map) throws NoSuchAlgorithmException, UnsupportedEncodingException {

		String signature = map.get("TXN_SIGNATURE2");
		if (StringUtil.isEmpty(signature)) {
			logger.debug("TXN_SIGNATURE2 is empty");
			return false;
		} else {

			DecimalFormat df = new DecimalFormat("0.00");
			
			Map<String, String> parameters = pHelper.getPaymentParameters(PaymentHelper.MPG_GATEWAY_ID);
			String hashKey = getHashKey(reference, parameters);
			String merchantAccountNo = map.get("MERCHANT_ACC_NO");
			String merchantTransactionId = map.get("MERCHANT_TRANID");
			String transactionAmount = df.format(getTotalAmount(entries));
			String transactionId = map.get("TRANSACTION_ID");
			String transactionStatus = map.get("TXN_STATUS");
			String responseCode = map.get("RESPONSE_CODE");
			
			return isResponseSignatureValid(signature, hashKey, merchantAccountNo, merchantTransactionId, 
					transactionAmount, transactionId, transactionStatus, responseCode);
		}
	}

	private static String post(String url, String body) throws IOException {

		HttpURLConnection connection = null;
		try {
			URL u = new URL(url);
			connection = (HttpURLConnection) u.openConnection();

			connection.setRequestMethod("POST");
			connection.setUseCaches(false);
			connection.setDoOutput(true);
			connection.setConnectTimeout(30000);
			connection.setReadTimeout(30000);

			try (OutputStream os = connection.getOutputStream()) {
				os.write(body.getBytes());
			}

			try (InputStream is = connection.getInputStream()) {
				return IOUtils.toString(is, "UTF-8");
			}
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	private static void notifyPaymentStatus(PaymentGatewayDAO gateway, PaymentReferenceDAO reference, 
			List<PaymentReferenceEntryDAO> entries, Person person) {

		try {
			EmailManager manager = new EmailManager();

			String refNo = getReferenceNo(reference.getReferencePrefix(), reference.getPaymentId());
			String paymentMethod = ResourceUtil.get("payment.mpg_method", "en");
			String gatewayReferenceLabel = "Bank Reference No";
			
			Date date = reference.getPaymentDate() == null ? reference.getCreatedDate() : reference.getPaymentDate();
			String gatewayRef = reference.getGatewayReference() == null ? "N/A" : reference.getGatewayReference();
			
			Map<String, BigDecimal> entriesMap = getEntriesAmountMap(entries);
			
			if (reference.getStatus() == PaymentStatus.FAILED) {
				manager.sendUnsuccessPaymentEmail(
						reference.getUserEmail(), person.getName(), refNo, 
						date, entriesMap, paymentMethod,
						gatewayReferenceLabel, gatewayRef);
			} else {
				
				manager.sendSuccessPaymentEmail(
						reference.getUserEmail(), person.getName(), refNo, 
						date, entriesMap, paymentMethod,
						gatewayReferenceLabel, gatewayRef);
			}
		} catch (IOException | MessagingException | NamingException e) {
			logger.error("error sending payment notification - " + e.getMessage(), e);
		}
	}
	
	public static String getReferenceNo(String prefix, long referenceNo){
		return String.format("%s%08d", prefix, referenceNo);
	}
	
	private static Map<String, BigDecimal> getEntriesAmountMap(List<PaymentReferenceEntryDAO> entries){
		
		Map<String, BigDecimal> map = new LinkedHashMap<>();
		
		for(PaymentReferenceEntryDAO entry : entries){
			map.put(entry.getContractAccountNumber(), entry.getAmount());
		}
		
		return map;
	}
	
	private static Map<String, String> getResponseMap(String response) throws UnsupportedEncodingException {

		Map<String, String> map = new HashMap<>();

		if (!StringUtil.isEmpty(response)) {
			String[] valuePairs = response.split("&");

			for (String vps : valuePairs) {

				String[] vp = vps.split("=");
				String name = vp.length >= 1 ? URLDecoder.decode(vp[0], "UTF-8") : null;
				String value = vp.length >= 2 ? URLDecoder.decode(vp[1], "UTF-8") : null;

				map.put(name, value);
			}
		}

		return map;
	}
}
