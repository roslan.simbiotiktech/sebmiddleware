package com.seb.middleware.payment.spg;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.mail.MessagingException;
import javax.naming.NamingException;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.seb.middleware.constant.PaymentStatus;
import com.seb.middleware.email.EmailManager;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.PaymentGatewayDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceDAO;
import com.seb.middleware.jpa.dao.PaymentReferenceEntryDAO;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.PaymentHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.utility.CryptoUtil;
import com.seb.middleware.utility.ResourceUtil;
import com.seb.middleware.utility.StringUtil;

public class SPGFacade {

	private static final Logger logger = LogManager.getLogger(SPGFacade.class);

	public static final String BANK_LIST_URL_NAME = "BANK_LIST_URL";
	public static final String BANK_QUERY_URL_NAME = "BANK_QUERY_URL";
	
	public static final String PARAM_SIGNATURE_SECRET = "SIGNATURE_SECRET";

	public static void completePayment(final long paymentId, final String referenceNo) {

		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {

				logger.debug("SPG Billing Starting");
				logger.debug(referenceNo);
				DatabaseFacade db = new DatabaseFacade();
				try {
					PaymentHelper pHelper = new PaymentHelper(db);
					MemberHelper mHelper = new MemberHelper(db);
					SettingHelper settingHelper = new SettingHelper(db);
					
					PaymentReferenceDAO reference = pHelper.getPaymentReference(paymentId);
					List<PaymentReferenceEntryDAO> entries = pHelper.getPaymentEntries(reference.getPaymentId());
					
					PaymentGatewayDAO gateway = pHelper.getSPGPaymentGateway();
					Person person = mHelper.getPerson(reference.getUserEmail());
					
					logger.debug("Billing : " + referenceNo);
					
					if (reference.getStatus() == PaymentStatus.FAILED) {
						// Notify failed email
						notifyPaymentStatus(gateway, reference, entries, person);
					} else if (reference.getStatus() == PaymentStatus.RECEIVED) {
						// Notify success email and update sap
						notifyPaymentStatus(gateway, reference, entries, person);
						
						pHelper.billTransaction(reference, entries, settingHelper);
						
						db.beginTransaction();
						db.update(reference);
						db.commit();
					} else {
						logger.error("completePayment with unexpected payment status reached - " + reference.getStatus() + ", "
								+ reference.getPaymentId());
					}
				} catch (NamingException e) {
					logger.error("error getting Person from LDAP - " + e.getMessage(), e);
				} catch (DatabaseFacadeException e) {
					logger.error("unrecover error occured, payment status updated to SAP but failed to update to local db");
					logger.error(e.getMessage(), e);
					logger.error(referenceNo);
				} finally {
					db.close();
				}
			}
		});
		t.start();
	}

	/**
	 * This sends a Query message
	 */
	public static boolean checkPaymentStatus(
			PaymentHelper pHelper, PaymentGatewayDAO gateway, PaymentReferenceDAO reference)
			throws IOException, NoSuchAlgorithmException {

		SPGTransactionType transactionType = getTransactionType(reference);
		
		Map<String, String> gatewayParams = pHelper.getPaymentParameters(gateway.getGatewayId());
		String queryUrl = pHelper.getPaymentUrl(gateway.getGatewayId(), transactionType.getQueryUrlName());
		List<PaymentReferenceEntryDAO> entries = pHelper.getPaymentEntries(reference.getPaymentId());
		
		DecimalFormat df = new DecimalFormat("0.00");
		String merchantId = gateway.getMerchantId();
		String referenceNo = getReferenceNo(reference);
		String amount = df.format(getTotalAmount(entries));
		
		String signature = getTransactionSignature(gatewayParams.get(PARAM_SIGNATURE_SECRET), 
				merchantId, referenceNo, amount);
		
		Map<String, String> posts = new HashMap<>();
		posts.put("merchant_id", merchantId);
		posts.put("transaction_id", referenceNo);
		posts.put("amount", amount);
		posts.put("signature", signature);
		
		Map<String, String> paymentStatus = sendForMap(queryUrl, posts);
		logger.debug(paymentStatus);
		
		if (paymentStatus != null && !paymentStatus.isEmpty()) {
			
			return processSPGMessage(reference, paymentStatus);
		} else {
			logger.warn("error response from SPG");
			return false;
		}
	}
	
	public synchronized static boolean processSPGMessage(
			PaymentReferenceDAO reference, Map<String, String> map) 
					throws NoSuchAlgorithmException, UnsupportedEncodingException{
		
		DatabaseFacade db = new DatabaseFacade();
		try{
			
			PaymentHelper pHelper = new PaymentHelper(db);
			PaymentReferenceDAO synchronizedReference = pHelper.getPaymentReference(reference.getPaymentId());
			
			boolean updated = processSPGMessage(pHelper, synchronizedReference, map);
			
			if (updated) {
				db.beginTransaction();
				db.update(synchronizedReference);
				db.commit();
				
				logger.info("SPG Status updated. " + synchronizedReference.toString());
				
				reference.setPaymentDate(synchronizedReference.getPaymentDate());
				reference.setStatus(synchronizedReference.getStatus());
				reference.setGatewayReference(synchronizedReference.getGatewayReference());
				reference.setGatewayStatus(synchronizedReference.getGatewayStatus());
				reference.setExternalGatewayStatus(synchronizedReference.getExternalGatewayStatus());
			}
			
			return updated;
			
		} catch (ParseException e) {
			logger.error("error parsing data - " + e.getMessage(), e);
			return false;
		} catch (DatabaseFacadeException e) {
			logger.error("error update SPG transaction status - " + e.getMessage(), e);
			return false;
		}finally{
			db.close();
		}
	}
	
	private static boolean processSPGMessage(PaymentHelper pHelper, PaymentReferenceDAO reference, Map<String, String> map) 
			throws ParseException, NoSuchAlgorithmException, UnsupportedEncodingException {
		
		if (verifyData(pHelper, map)) {
			String statusCode = map.get("status_code");
			String externalGatewayStatusCode = map.get("gateway_transaction_status");
			logger.info("Status code '{}', '{}'", statusCode, externalGatewayStatusCode);
			
			if (statusCode != null) {
				if(statusCode.equals(reference.getGatewayStatus())){
					logger.debug("Transaction status already received previously");
					return false;
				}else if (statusCode.equals("00")) {
					if (reference.getStatus() == PaymentStatus.PENAUTH ||
							reference.getStatus() == PaymentStatus.PENDING ||
							reference.getStatus() == PaymentStatus.FAILED) {

						SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

						String spgTransactionId = map.get("spg_transaction_id");
						String gatewayTransactionId = map.get("gateway_transaction_id");
						String spgTransactionTime = map.get("transaction_auth_date");

						if(!StringUtil.isEmpty(spgTransactionTime)){
							reference.setPaymentDate(sdf.parse(spgTransactionTime));	
						}
						
						reference.setGatewayReference(gatewayTransactionId);
						reference.setStatus(PaymentStatus.RECEIVED);
						
						logger.debug("Payment Successful '{}', '{}'", spgTransactionId, gatewayTransactionId);
					}else if(reference.getStatus() == PaymentStatus.RECEIVED || reference.getStatus() == PaymentStatus.BILLED){
						return false;
					}
				} else if (statusCode.equals("02")) {
					logger.debug("Transaction in progress");
					return false;
				} else if (statusCode.equals("03")) {
					
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
					
					String gatewayTransactionId = map.get("gateway_transaction_id");
					String spgTransactionTime = map.get("transaction_auth_date");
					
					if(!StringUtil.isEmpty(spgTransactionTime)){
						reference.setPaymentDate(sdf.parse(spgTransactionTime));	
					}
					
					reference.setGatewayReference(gatewayTransactionId);
					reference.setStatus(PaymentStatus.PENAUTH);
					
					notifyPenAuth(reference);
					
					logger.debug("Payment Pending authorization - {} {}", reference.getSpgBankName(), reference.getSpgBankType());
				}else {
					logger.debug("Transaction error - " + statusCode);
					if (reference.getStatus() == PaymentStatus.FAILED) {
						return false;
					}else{
						if (reference.getStatus() == PaymentStatus.RECEIVED 
								|| reference.getStatus() == PaymentStatus.BILLED) {
							
							logger.debug("Duplicated update received");
							if(statusCode.equals("11")){
								logger.debug("Ignored 11 duplicate exchange id response");
								return false;
							}
						}
						
						SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

						String gatewayTransactionId = map.get("gateway_transaction_id");
						String spgTransactionTime = map.get("transaction_auth_date");

						if(!StringUtil.isEmpty(spgTransactionTime)){
							reference.setPaymentDate(sdf.parse(spgTransactionTime));	
						}
						
						if(!StringUtil.isEmpty(gatewayTransactionId)){
							reference.setGatewayReference(gatewayTransactionId);
						}
						
						reference.setStatus(PaymentStatus.FAILED);
					}
				}
				
				reference.setGatewayStatus(statusCode);
				if(externalGatewayStatusCode != null) reference.setExternalGatewayStatus(externalGatewayStatusCode);
				return true;
			}
		}

		return false;
	}

	private static boolean verifyData(PaymentHelper pHelper, Map<String, String> map) 
			throws NoSuchAlgorithmException, UnsupportedEncodingException {

		String signature = map.get("signature");
		if (StringUtil.isEmpty(signature)) {
			logger.debug("signature is empty");
			return false;
		} else {

			Map<String, String> parameters = pHelper.getPaymentParameters(PaymentHelper.SPG_GATEWAY_ID);
			String computeSignature = getTransactionSignature(parameters.get(PARAM_SIGNATURE_SECRET), 
					map.get("transaction_id"), map.get("spg_transaction_id"), 
					map.get("gateway_transaction_id"), map.get("gateway_transaction_status"),   
					map.get("status_code"), map.get("amount"), map.get("message"), map.get("transaction_auth_date"));
			
			boolean match = computeSignature.equals(signature);
			
			if(!match){
				logger.error("signature mismatch SPG - {}", signature);
				logger.error("signature mismatch Local - {}", computeSignature);
			}
			
			return match;
		}
	}
	
	public static SPGFpxBank getBank(int bankId) throws IOException{

		DatabaseFacade db = new DatabaseFacade();

		try {
			PaymentHelper pHelper = new PaymentHelper(db);
			PaymentGatewayDAO gateway = pHelper.getSPGPaymentGateway();
			String queryUrl = pHelper.getPaymentUrl(gateway.getGatewayId(), BANK_QUERY_URL_NAME);

			Map<String, String> params = new HashMap<String, String>();
			params.put("id", String.valueOf(bankId));
			
			JsonObject result = sendForJson(queryUrl, params);
			
			if(result != null && result.has("id")){
				SPGFpxBank bank = getSPGFpxBank(result);
				logger.debug("Bank : {}", bank);
				return bank;
			}else{
				logger.error("unrecognized json {}", result);
				return null;
			}
		} finally {
			db.close();
		}
	}
	
	public static Map<SPGFpxBankType, List<SPGFpxBank>> getBanks() throws IOException{

		DatabaseFacade db = new DatabaseFacade();

		try {
			PaymentHelper pHelper = new PaymentHelper(db);
			PaymentGatewayDAO gateway = pHelper.getSPGPaymentGateway();
			String queryUrl = pHelper.getPaymentUrl(gateway.getGatewayId(), BANK_LIST_URL_NAME);

			JsonObject result = sendForJson(queryUrl);
			
			Map<SPGFpxBankType, List<SPGFpxBank>> map = new LinkedHashMap<>();
			int count = 0;
			if(result != null){
				
				Iterator<Entry<String, JsonElement>> iterator = result.entrySet().iterator();;
				while(iterator.hasNext()){
					
					Entry<String, JsonElement> next = iterator.next();
					
					try{
						SPGFpxBankType type = SPGFpxBankType.valueOf(next.getKey());
						JsonArray arr = next.getValue().getAsJsonArray();
						
						if(arr.size() > 0){
							List<SPGFpxBank> banks = new ArrayList<>();
							map.put(type, banks);
							
							for(int i = 0; i < arr.size(); i++){
								JsonObject o = arr.get(i).getAsJsonObject();
								banks.add(getSPGFpxBank(o));
								count++;
							}
						}
					}catch(IllegalArgumentException e){
						logger.error("error casting '{}' to SPGFpxBankType", next.getKey());
					}
				}
			}else{
				logger.error("response json is null");
			}
			
			logger.debug("total {} types and {} banks found", map.size(), count);
			return map;
		} finally {
			db.close();
		}
	}
	
	public static SPGTransactionType getTransactionType(PaymentReferenceDAO reference){
		if(reference.getSpgBankId() != null){
			return SPGTransactionType.FPX;
		}else{
			return SPGTransactionType.MAYBANK;
		}
	}
	
	public static String getReferenceNo(PaymentReferenceDAO reference){
		// SPGTransactionType type = getTransactionType(reference);
		return getReferenceNo(reference.getReferencePrefix(), reference.getPaymentId());
	}
	
	private static String getReferenceNo(String prefix, long referenceNo){
		return String.format("%s%08d", prefix, referenceNo);
	}
	
	private static BigDecimal getTotalAmount(List<PaymentReferenceEntryDAO> entries){
		BigDecimal amount = BigDecimal.ZERO;
		
		for(PaymentReferenceEntryDAO entry : entries){
			amount = amount.add(entry.getAmount());
		}
		
		return amount.setScale(2, RoundingMode.FLOOR);
	}
	
	public static String getTransactionSignature(String secret, String ... fields ) 
			throws NoSuchAlgorithmException, UnsupportedEncodingException{
		StringBuilder sb = new StringBuilder();
		
		for(String field : fields){
			if(field != null)
				sb.append(field);
		}
		
		sb.append(secret);
		
		return CryptoUtil.sha512HexadecimalLowercase(sb.toString());
	}
	
	protected static JsonObject sendForJson(String url) throws IOException {
		return sendForJson(url, null);
	}
	
	protected static JsonObject sendForJson(String url, Map<String, String> gets) throws IOException {
		String resp = send(url, gets, null);
		if(!StringUtil.isEmpty(resp)){
			JsonObject json = new JsonParser().parse(resp).getAsJsonObject();
			return json;
		}else{
			return null;
		}
	}
	
	protected static Map<String, String> sendForMap(String url, Map<String, String> posts) throws IOException {
		String resp = send(url, null, posts);
		return getParamsFromURIEncodedString(resp);
	}

	protected static String send(String url, Map<String, String> gets, Map<String, String> posts) throws IOException {

		HttpURLConnection connection = null;
		String responseString = null;
		
		try {
			String body = getURIEncodedParams(posts);
			String urlParams = getURIEncodedParams(gets);
			
			if(!StringUtil.isEmpty(urlParams)){
				url = url + "?" + urlParams;
			}
			logger.debug("connecting to {}", url);
			String method = StringUtil.isEmpty(body) ? "GET" : "POST";
			
			URL u = new URL(url);
			connection = (HttpURLConnection) u.openConnection();

			connection.setRequestMethod(method);
			connection.setUseCaches(false);
			connection.setDoOutput(!StringUtil.isEmpty(body));
			connection.setConnectTimeout(30000);
			connection.setReadTimeout(30000);

			if(!StringUtil.isEmpty(body)){
				try (OutputStream os = connection.getOutputStream()) {
					os.write(body.getBytes());
				}	
			}
			
			int code = connection.getResponseCode();
			if (code >= 200 && code < 400){
				try (InputStream is = connection.getInputStream()) {
					responseString = IOUtils.toString(is, "UTF-8");
				}
			}else{
				try (InputStream is = connection.getErrorStream()) {
					responseString = IOUtils.toString(is, "UTF-8");
				}
			}
			
			if(code == HttpURLConnection.HTTP_OK){
				return responseString;
			}else{
				logger.error("HTTP Error response code {}", code);
				throw new IOException(responseString);
			}
		} catch(IOException e){
			logger.error("error response - {}", responseString, e);
			throw e;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	private static String getURIEncodedParams(Map<String, String> values) throws UnsupportedEncodingException{
		StringBuilder sb = new StringBuilder();
		
		if(values != null && !values.isEmpty()){
			Iterator<Entry<String, String>> iterator = values.entrySet().iterator();
			while(iterator.hasNext()){
				Entry<String, String> next = iterator.next();
				sb.append(URLEncoder.encode(next.getKey(), "UTF-8"));
				sb.append("=");
				sb.append(URLEncoder.encode(next.getValue(), "UTF-8"));
				
				if(iterator.hasNext()){
					sb.append("&");
				}
			}
		}
		
		return sb.toString();
	}

	public static Map<String, String> getParamsFromURIEncodedString(String string) throws UnsupportedEncodingException{
		
		Map<String, String> map = new HashMap<>();
		
		if(!StringUtil.isEmpty(string)){
			
			String[] pairs = string.split("&");
			
			for(String pair : pairs){
				String[] sets = pair.split("=");
				if(sets.length >= 2){
					map.put(URLDecoder.decode(sets[0], "UTF-8"), URLDecoder.decode(sets[1], "UTF-8"));
				}
			}
			
			return map;
		}
		
		return map;
	}
	
	private static void notifyPaymentStatus(PaymentGatewayDAO gateway, PaymentReferenceDAO reference, 
			List<PaymentReferenceEntryDAO> entries, Person person) {

		SPGTransactionType type = getTransactionType(reference);
		
		try {
			EmailManager manager = new EmailManager();
			String refNo = getReferenceNo(reference);
			
			String paymentMethod;
			String gatewayReferenceLabel;
			
			if(type == SPGTransactionType.FPX){
				paymentMethod = ResourceUtil.get("payment.fpx_method", "en") + " - " + reference.getSpgBankName();
				gatewayReferenceLabel = "FPX Transaction Id";
			}else{
				paymentMethod = ResourceUtil.get("payment.mpg_method", "en");
				gatewayReferenceLabel = "Bank Reference No";
			}
			
			Map<String, BigDecimal> entriesMap = getEntriesAmountMap(entries);
			
			if (reference.getStatus() == PaymentStatus.RECEIVED || reference.getStatus() == PaymentStatus.BILLED) {
				manager.sendSuccessPaymentEmail(
						reference.getUserEmail(), person.getName(), refNo, 
						reference.getPaymentDate(), entriesMap, paymentMethod, 
						gatewayReferenceLabel, reference.getGatewayReference());
			} else if (reference.getStatus() == PaymentStatus.FAILED) {
				manager.sendUnsuccessPaymentEmail(
						reference.getUserEmail(), person.getName(), refNo, 
						reference.getPaymentDate() != null ? reference.getPaymentDate() : reference.getCreatedDate(), 
						entriesMap, paymentMethod, 
						gatewayReferenceLabel, reference.getGatewayReference());
			} else if (reference.getStatus() == PaymentStatus.PENAUTH) {
				manager.sendPendingAuthorizationPaymentEmail(
						reference.getUserEmail(), person.getName(), refNo, 
						reference.getPaymentDate() != null ? reference.getPaymentDate() : reference.getCreatedDate(), 
						entriesMap, paymentMethod, 
						gatewayReferenceLabel, reference.getGatewayReference());
			} else {
				logger.error("unknown status for status notification - " + reference.getStatus());
			}
		} catch (IOException | MessagingException | NamingException e) {
			logger.error("error sending payment notification - " + e.getMessage(), e);
		}
	}
	
	private static Map<String, BigDecimal> getEntriesAmountMap(List<PaymentReferenceEntryDAO> entries){
		
		Map<String, BigDecimal> map = new LinkedHashMap<>();
		
		for(PaymentReferenceEntryDAO entry : entries){
			map.put(entry.getContractAccountNumber(), entry.getAmount());
		}
		
		return map;
	}
	
	private static SPGFpxBank getSPGFpxBank(JsonObject result){
		SPGFpxBank bank = new SPGFpxBank();
		
		if(result.has("id")) bank.setId(result.get("id").getAsInt());
		if(result.has("name")) bank.setName(result.get("name").getAsString());
		if(result.has("type")) bank.setType(SPGFpxBankType.valueOf(result.get("type").getAsString()));
		if(result.has("logo")) bank.setLogo(result.get("logo").getAsString());
		if(result.has("enabled")) bank.setEnabled(result.get("enabled").getAsBoolean());
		
		return bank;
	}
	
	private static void notifyPenAuth(PaymentReferenceDAO reference){
		
		DatabaseFacade db = new DatabaseFacade();
		try {
			PaymentHelper pHelper = new PaymentHelper(db);
			MemberHelper mHelper = new MemberHelper(db);
			
			List<PaymentReferenceEntryDAO> entries = pHelper.getPaymentEntries(reference.getPaymentId());
			
			PaymentGatewayDAO gateway = pHelper.getSPGPaymentGateway();
			Person person = mHelper.getPerson(reference.getUserEmail());
			
			notifyPaymentStatus(gateway, reference, entries, person);
			
		} catch (NamingException e) {
			logger.error("error getting Person from LDAP - " + e.getMessage(), e);
		} finally {
			db.close();
		}
	}
}
