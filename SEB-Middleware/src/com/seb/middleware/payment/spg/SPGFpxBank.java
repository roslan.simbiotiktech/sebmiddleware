package com.seb.middleware.payment.spg;

public class SPGFpxBank {

	private int id;
	
	private String name;
	
	private SPGFpxBankType type;
	
	private String logo;
	
	private boolean enabled;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SPGFpxBankType getType() {
		return type;
	}

	public void setType(SPGFpxBankType type) {
		this.type = type;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "SPGFpxBank [id=" + id + ", name=" + name + ", type=" + type + ", logo=" + logo + ", enabled=" + enabled + "]";
	}
}
