package com.seb.middleware.payment.spg;

public enum SPGTransactionType {

	FPX("FPX_QUERY_URL", "FPX_SUBMIT_URL", "F"),
	MAYBANK("MPG_QUERY_URL", "MPG_SUBMIT_URL", "M");
	
	private String queryUrlName;
	private String submitUrlName;
	private String prefix;
	
	SPGTransactionType(String queryUrlName, String submitUrlName, String prefix){
		this.queryUrlName = queryUrlName;
		this.submitUrlName = submitUrlName;
		this.prefix = prefix;
	}

	public String getQueryUrlName() {
		return queryUrlName;
	}

	public String getSubmitUrlName() {
		return submitUrlName;
	}

	public String getPrefix() {
		return prefix;
	}
}
