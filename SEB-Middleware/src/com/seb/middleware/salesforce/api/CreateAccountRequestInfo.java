package com.seb.middleware.salesforce.api;

import com.google.gson.JsonObject;
import com.seb.middleware.salesforce.core.CompositeRequest;

public class CreateAccountRequestInfo extends CompositeRequest {

	private static final String REF_PREFIX = "CA_";
	
	private String userId;
	
	private String email;
	
	private String lastName;
	
	private String mobilePhone;
	
	private String nricPassport;
	
	private String recordTypeId;
	
	private String sebcEmailOne;
	
	private String sebcMobileOne;
	
	
	public CreateAccountRequestInfo(String userId, String lastName, String mobilePhone, 
			String email, String nricPassport, String recordTypeId,String sebcEmailOne,String sebcMobileOne) {
		super(REF_PREFIX + getPurgedString(userId));
		this.userId = userId;
		this.email = email;
		this.lastName = lastName;
		this.mobilePhone = mobilePhone;
		this.nricPassport = nricPassport;
		this.recordTypeId = recordTypeId;
		this.sebcEmailOne=sebcEmailOne;
		this.sebcMobileOne=sebcMobileOne;
		setEntityName("Account");
	}

	@Override
	public JsonObject getRequestBody() {
		JsonObject j = new JsonObject();
		
		j.addProperty("LastName", lastName);
		j.addProperty("RecordTypeId", recordTypeId);
		/*
		 * j.addProperty("PersonMobilePhone", mobilePhone); j.addProperty("PersonEmail",
		 * email);
		 */
		
		if((nricPassport.length() == 12) || (nricPassport.length() == 14)) {
			j.addProperty("ID_Type__pc", "NRIC");
		}else {
			j.addProperty("ID_Type__pc", "Passport");
		}
		j.addProperty("NRIC_Passport_No__pc", nricPassport);
		j.addProperty("SEBC_User_ID__c", userId);
		j.addProperty("SEBC_Email_1__c", sebcEmailOne);
		j.addProperty("SEBC_Mobile_1__c", sebcMobileOne);
		return j;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	
	public String getSebcEmailOne() {
		return sebcEmailOne;
	}

	public void setSebcEmailOne(String sebcEmailOne) {
		this.sebcEmailOne = sebcEmailOne;
	}

	public String getSebcMobileOne() {
		return sebcMobileOne;
	}

	public void setSebcMobileOne(String sebcMobileOne) {
		this.sebcMobileOne = sebcMobileOne;
	}

	@Override
	public String toString() {
		return "CreateAccountRequestInfo [userId=" + userId + ", email=" + email + ", lastName=" + lastName
				+ ", mobilePhone=" + mobilePhone + ", nricPassport=" + nricPassport + ", recordTypeId=" + recordTypeId
				+ ", sebcEmailOne=" + sebcEmailOne + ", sebcMobileOne=" + sebcMobileOne + "]";
	}

	
}
