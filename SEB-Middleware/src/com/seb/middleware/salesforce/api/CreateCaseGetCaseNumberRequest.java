package com.seb.middleware.salesforce.api;

import com.google.gson.JsonObject;
import com.seb.middleware.salesforce.core.CompositeRequest;
import com.seb.middleware.salesforce.core.SFMethod;

public class CreateCaseGetCaseNumberRequest extends CompositeRequest {

	public CreateCaseGetCaseNumberRequest(String caseReferenceId) {
		super("refCaseDetail");
		
		setMethod(SFMethod.GET);
		setOverrideUrl("/services/data/v43.0/sobjects/Case/@{" + caseReferenceId + ".id}?fields=CaseNumber");
	}
	
	@Override
	public JsonObject getRequestBody() {
		return null;
	}
}
