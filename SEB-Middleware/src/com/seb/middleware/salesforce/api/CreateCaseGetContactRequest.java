package com.seb.middleware.salesforce.api;

import com.google.gson.JsonObject;
import com.seb.middleware.salesforce.core.CompositeRequest;
import com.seb.middleware.salesforce.core.SFMethod;

public class CreateCaseGetContactRequest extends CompositeRequest {

	public CreateCaseGetContactRequest(String accountEntityId) {
		super("contactRef");
		
		setMethod(SFMethod.GET);
		setOverrideUrl("/services/data/v43.0/query/?q=select+id+from+Contact+where+AccountId+=+'"+accountEntityId+"'");
	}
	
	@Override
	public JsonObject getRequestBody() {
		return null;
	}
}
