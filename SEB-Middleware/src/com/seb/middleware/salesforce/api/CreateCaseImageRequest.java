package com.seb.middleware.salesforce.api;

import com.google.gson.JsonObject;
import com.seb.middleware.salesforce.core.CompositeRequest;
import com.seb.middleware.salesforce.core.SFMethod;

public class CreateCaseImageRequest {

	private String caseId;
	
	private int index;
	
	private String dataType;
	
	private String fileData; // base64

	public CreateCaseImageRequest(String caseId, int index, String dataType, String fileData) {
		this.caseId = caseId;
		this.index = index;
		this.dataType = dataType;
		this.fileData = fileData;
	}

	public CompositeRequest [] getRequests() {
		
		CompositeRequest file = new CompositeRequest("file" + index) {
			
			@Override
			public JsonObject getRequestBody() {
				JsonObject j = new JsonObject();
				
				j.addProperty("Title", getFileName());
				j.addProperty("PathOnClient", getFileName());
				j.addProperty("VersionData", fileData);
				
				return j;
			}
		};
		
		file.setEntityName("ContentVersion");
		file.setMethod(SFMethod.POST);
		
		CompositeRequest q = new CompositeRequest("q" + index) {
			
			@Override
			public JsonObject getRequestBody() {
				return null;
			}
		};
		q.setOverrideUrl("/services/data/v43.0/query/?q=select+id+,+ContentDocumentId+from+ContentVersion+where+id+=+'@{file" 
				+ index + ".id}'");
		q.setMethod(SFMethod.GET);
		
		CompositeRequest link = new CompositeRequest("link" + index) {
			
			@Override
			public JsonObject getRequestBody() {
				JsonObject j = new JsonObject();
				
				j.addProperty("ContentDocumentId", "@{q" + index + ".records[0].ContentDocumentId}");
				j.addProperty("LinkedEntityId", caseId);
				j.addProperty("ShareType", "v");
				
				return j;
			}
		};
		
		link.setEntityName("ContentDocumentLink");
		link.setMethod(SFMethod.POST);
		
		return new CompositeRequest[] {file, q, link};
	}
	
	public String getFileName() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("photo").append(index).append(".").append(getExtension());
		
		return sb.toString();
	}
	
	private String getExtension() {
		if(dataType == null) {
			return "jpg";
		}else {
			String dt = dataType.toLowerCase();
			if(dt.indexOf("jpg") != -1 || dt.indexOf("jpeg") != -1) {
				return "jpg";
			}else if(dt.indexOf("png") != -1) {
				return "png";
			}else if(dt.indexOf("gif") != -1) {
				return "gif";
			}else {
				return "jpg";
			}
		}
	}

	public String getCaseId() {
		return caseId;
	}

	public int getIndex() {
		return index;
	}

	public String getDataType() {
		return dataType;
	}

	public String getFileData() {
		return fileData;
	}

	@Override
	public String toString() {
		return "CreateCaseImageRequest [caseId=" + caseId + ", index=" + index + ", dataType=" + dataType + "]";
	}
}
