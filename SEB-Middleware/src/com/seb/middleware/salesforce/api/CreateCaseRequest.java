package com.seb.middleware.salesforce.api;

import java.util.Date;

import com.google.gson.JsonObject;
import com.seb.middleware.constant.ReportCategory;
import com.seb.middleware.constant.ReportIssueType;
import com.seb.middleware.constant.ReportType;
import com.seb.middleware.constant.SFDCClassification;
import com.seb.middleware.salesforce.core.CompositeRequest;

public class CreateCaseRequest extends CompositeRequest {

	private static final String REF_PREFIX = "CCE_";
	
	private long transId;
	
	private String accountEntityId;
	
	private String contractEntityId;
	
	private String channel;
	
	private SFDCClassification classification;
	
	private ReportType type;

	private ReportIssueType issueType;
	
	private ReportCategory category;

	private String description;
	
	private Date createdDatetime;

	private String address;
	
	private String latitude;
	
	private String longitude;
	
	private String contactReference;
	
	private String recordTypeId;
	
	public CreateCaseRequest(long transId, String accountEntityId, String contractEntityId,
			String channel, SFDCClassification classification, ReportType type,
			ReportIssueType issueType, ReportCategory category, String description, Date createdDatetime,
			String address, String latitude, String longitude, String contactReference, String recordTypeId) {
		super(REF_PREFIX + transId);
		
		setEntityName("Case");
		
		this.transId = transId;
		this.accountEntityId = accountEntityId;
		this.contractEntityId = contractEntityId;
		this.channel = channel;
		this.type = type;
		this.classification = classification;
		this.category = category;
		this.issueType = issueType;
		this.description = description;
		this.createdDatetime = createdDatetime;
		this.address = address;
		this.latitude = latitude;
		this.longitude = longitude;
		this.contactReference = contactReference;
		this.recordTypeId = recordTypeId;
	}
	
	@Override
	public JsonObject getRequestBody() {
		JsonObject j = new JsonObject();
		
		j.addProperty("RecordTypeId", recordTypeId);
		j.addProperty("AccountId", accountEntityId);
		j.addProperty("ContactId", "@{" + contactReference + ".records[0].Id}");
		
		if("APPS".equalsIgnoreCase(channel)) {
			j.addProperty("Origin", "Mobile App");
		}else {
			j.addProperty("Origin", "Web");
		}
		
		if(classification != null) j.addProperty("Classification__c", classification.getName());
		
		if(type == ReportType.BILLING_AND_METER) {
			if(ReportIssueType.BILLING == issueType) {
				j.addProperty("Category__c", issueType.getName());	
			}else {
				j.addProperty("Category__c", ReportIssueType.METER_READING.getName());	
			}
		}else {
			j.addProperty("Category__c", type.getName());	
		}
		
		if(category != null) {
			j.addProperty("Type", category.getName());
		}
		
		j.addProperty("Contract_Account_No__c", contractEntityId);
		j.addProperty("Status", "New");
		j.addProperty("Description", description);
		j.addProperty("SEBC_Trans_ID__c", transId);
		j.addProperty("SEBC_Created_DateTime__c", getSfDateFormat(createdDatetime));
		j.addProperty("SEBC_Doc_ID__c", String.valueOf(transId));
		j.addProperty("Incident_Address__c", address == null ? "" : address);
		j.addProperty("Incident_Coordinate__Latitude__s", latitude == null ? "" : latitude);
		j.addProperty("Incident_Coordinate__Longitude__s", longitude == null ? "" : longitude);
		j.addProperty("Incident_State__c", "Sarawak");
		j.addProperty("Incident_Country__c", "Malaysia");
		
		if(issueType != null)
			j.addProperty("SEBC_Issue_Type__c", issueType.getName());
		j.addProperty("SEBC_Deleted_Flag__c", 0);
		
		return j;
	}
}
