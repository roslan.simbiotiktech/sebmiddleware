package com.seb.middleware.salesforce.api;

import com.google.gson.JsonObject;
import com.seb.middleware.salesforce.core.CompositeRequest;

public class CreateContractRequest extends CompositeRequest {

	private static final String REF_PREFIX = "CC_";
	
	private String contractAccountNumber;
	
	private String contractAccountName;
	
	private String agencyName;
	
	public CreateContractRequest(String contractAccountNumber, String contractAccountName, String agencyName) {
		super(REF_PREFIX + getPurgedString(contractAccountNumber));
		this.contractAccountNumber = contractAccountNumber;
		this.contractAccountName = contractAccountName;
		this.agencyName = agencyName;
		
		setEntityName("Contract_Account__c");
	}

	@Override
	public JsonObject getRequestBody() {
		JsonObject j = new JsonObject();
		j.addProperty("Name", contractAccountNumber);
		j.addProperty("Contract_Account_No__c", contractAccountNumber);
		return j;
	}
	
	public String getContractAccountNumber() {
		return contractAccountNumber;
	}

	public String getContractAccountName() {
		return contractAccountName;
	}

	public String getAgencyName() {
		return agencyName;
	}

	@Override
	public String toString() {
		return "CreateContractRequest [contractAccountNumber=" + contractAccountNumber + ", contractAccountName="
				+ contractAccountName + ", agencyName=" + agencyName + "]";
	}
}
