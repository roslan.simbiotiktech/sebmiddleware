package com.seb.middleware.salesforce.api;

import com.google.gson.JsonObject;
import com.seb.middleware.constant.SubscriptionStatus;
import com.seb.middleware.constant.SubscriptionType;
import com.seb.middleware.salesforce.core.CompositeRequest;

public class CreateSubscriptionRequest extends CompositeRequest {

	private static final String REF_PREFIX = "CS_";
	
	private String accountEntityId;
	
	private String contractEntityId;
	
	private String email;
	
	private String contractAccountNumber;
	
	private String nickname;
	
	private SubscriptionType type;
	
	private SubscriptionStatus status;
	
	public CreateSubscriptionRequest(String accountEntityId, String contractEntityId, 
			String email, String contractAccountNumber, String nickname, 
			SubscriptionType type, SubscriptionStatus status) {
		
		super(REF_PREFIX + getPurgedString(accountEntityId + contractEntityId));
		
		this.accountEntityId = accountEntityId;
		this.contractEntityId = contractEntityId;
		this.email = email;
		this.contractAccountNumber = contractAccountNumber;
		this.nickname = nickname;
		this.type = type;
		this.status = status;
		
		setEntityName("Contract_Subscription__c");
	}

	@Override
	public JsonObject getRequestBody() {
		JsonObject j = new JsonObject();
		
		j.addProperty("Account__c", accountEntityId);
		j.addProperty("Contract_Account__c", contractEntityId);
		j.addProperty("SEBC_Subscription_Nickname__c", nickname); 
		j.addProperty("SEBC_Subscription_Type__c", type.toString());
		j.addProperty("SEBC_Subscription_Status__c", status.toString());
		j.addProperty("SEBC_SUBS_ID__c", email + "_" + contractAccountNumber);
		
		return j;
	}

	public String getAccountEntityId() {
		return accountEntityId;
	}

	public String getContractEntityId() {
		return contractEntityId;
	}

	public String getNickname() {
		return nickname;
	}

	public SubscriptionType getType() {
		return type;
	}

	public SubscriptionStatus getStatus() {
		return status;
	}

	@Override
	public String toString() {
		return "CreateSubscriptionRequest [accountEntityId=" + accountEntityId + ", contractEntityId="
				+ contractEntityId + ", nickname=" + nickname + ", type=" + type + ", status=" + status + "]";
	}
}
