package com.seb.middleware.salesforce.api;

import com.google.gson.JsonObject;
import com.seb.middleware.salesforce.core.CompositeRequest;
import com.seb.middleware.salesforce.core.SFMethod;

public class QueryAccountRequest extends CompositeRequest {

	public QueryAccountRequest(String email, String mobilePhone, String nricPassport) {
		super("acc");
		
		StringBuilder overrideUrl = new StringBuilder();
		overrideUrl.append("/services/data/v43.0/query/?q=SELECT+Id+,+PersonEmail+,+PersonMobilePhone+FROM+Account+WHERE+PersonEmail+=+'");
		overrideUrl.append(email).append("'+OR+PersonMobilePhone+=+'");
		overrideUrl.append(mobilePhone).append("'+OR+NRIC_Passport_No__pc+=+'");
		overrideUrl.append(nricPassport).append("'+limit+1");
		
		setMethod(SFMethod.GET);
		setOverrideUrl(overrideUrl.toString());
	}
	
	@Override
	public JsonObject getRequestBody() {
		return null;
	}
}
