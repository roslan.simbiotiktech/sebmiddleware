package com.seb.middleware.salesforce.api;

import com.google.gson.JsonObject;
import com.seb.middleware.salesforce.core.CompositeRequest;
import com.seb.middleware.salesforce.core.SFMethod;

public class QueryAccountRequestInfo extends CompositeRequest {

	public QueryAccountRequestInfo(String email, String mobilePhone, String nricPassport,String SEBC_Email_1__c,String SEBC_Email_2__c,
			String SEBC_Email_3__c,String SEBC_Mobile_1__c,String SEBC_Mobile_2__c,String SEBC_Mobile_3__c) {
		super("acc");
		StringBuilder overrideUrl = new StringBuilder();
		overrideUrl.append("/services/data/v43.0/query/?q=SELECT+Id+,+PersonEmail+,+PersonMobilePhone+,+NRIC_Passport_No__pc+,+SEBC_Email_1__c+,+SEBC_Email_2__c+,+SEBC_Email_3__c+,+SEBC_Mobile_1__c+,+SEBC_Mobile_2__c+,+SEBC_Mobile_3__c+FROM+Account+WHERE+PersonEmail+=+'");
		overrideUrl.append(email).append("'+OR+PersonMobilePhone+=+'");
		overrideUrl.append(mobilePhone).append("'+OR+NRIC_Passport_No__pc+=+'");
		overrideUrl.append(nricPassport).append("'+OR+SEBC_Email_1__c+=+'");
		overrideUrl.append(SEBC_Email_1__c).append("'+OR+SEBC_Email_2__c+=+'");
		overrideUrl.append(SEBC_Email_2__c).append("'+OR+SEBC_Email_3__c+=+'");
		overrideUrl.append(SEBC_Email_3__c).append("'+OR+SEBC_Mobile_1__c+=+'");
		overrideUrl.append(SEBC_Mobile_1__c).append("'+OR+SEBC_Mobile_2__c+=+'");
		overrideUrl.append(SEBC_Mobile_2__c).append("'+OR+SEBC_Mobile_3__c+=+'");
		overrideUrl.append(SEBC_Mobile_3__c).append("'+order+by+createddate+asc").append("+limit+1");	
		setMethod(SFMethod.GET);
		setOverrideUrl(overrideUrl.toString());
	}
	
	@Override
	public JsonObject getRequestBody() {
		return null;
	}
}
