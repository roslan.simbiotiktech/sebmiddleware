package com.seb.middleware.salesforce.api;

import com.google.gson.JsonObject;
import com.seb.middleware.salesforce.core.CompositeRequest;
import com.seb.middleware.salesforce.core.SFMethod;

public class QueryComplaintCaseRequest extends CompositeRequest {

	public QueryComplaintCaseRequest(String caseReferenceId) {
		super("q1");
		
		setMethod(SFMethod.GET);
		setOverrideUrl("/services/data/v43.0/query/?q=select+id+,+CaseNumber+,+CreatedDate+,+Status+,+Sub_Status__c+,+Classification__c+,+Category__c+from+Case+where+CaseNumber+=+'" + caseReferenceId + "'");
	}
	
	@Override
	public JsonObject getRequestBody() {
		return null;
	}
}
