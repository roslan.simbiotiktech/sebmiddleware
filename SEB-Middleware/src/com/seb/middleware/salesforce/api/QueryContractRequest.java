package com.seb.middleware.salesforce.api;

import com.google.gson.JsonObject;
import com.seb.middleware.salesforce.core.CompositeRequest;
import com.seb.middleware.salesforce.core.SFMethod;

public class QueryContractRequest extends CompositeRequest {

	public QueryContractRequest(String contractAccountNumber) {
		super("conAcc");
		
		StringBuilder overrideUrl = new StringBuilder();
		overrideUrl.append("/services/data/v43.0/query/?q=select+Id+from+Contract_Account__c+where+Contract_Account_No__c+=+'");
		overrideUrl.append(contractAccountNumber).append("'+limit+1");
		
		setMethod(SFMethod.GET);
		setOverrideUrl(overrideUrl.toString());
	}
	
	@Override
	public JsonObject getRequestBody() {
		return null;
	}
}
