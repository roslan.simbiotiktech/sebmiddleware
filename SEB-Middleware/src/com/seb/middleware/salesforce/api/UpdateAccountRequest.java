package com.seb.middleware.salesforce.api;

import com.google.gson.JsonObject;
import com.seb.middleware.salesforce.core.CompositeRequest;
import com.seb.middleware.salesforce.core.SFMethod;

public class UpdateAccountRequest extends CompositeRequest {

	private static final String REF_PREFIX = "UA_";
	
	private String lastName;
	
	private String mobilePhone;
	
	private String email;
	
	private String nricPassport;
	
	public UpdateAccountRequest(String entityId, String lastName, String mobilePhone, String email, String nricPassport) {
		super(REF_PREFIX + entityId);
		this.lastName = lastName;
		this.mobilePhone = mobilePhone;
		this.email = email;
		this.nricPassport = nricPassport;
		
		setMethod(SFMethod.PATCH);
		setEntityName("Account");
		setEntityId(entityId);
	}

	@Override
	public JsonObject getRequestBody() {
		JsonObject j = new JsonObject();
		
		j.addProperty("LastName", lastName);
		j.addProperty("PersonMobilePhone", mobilePhone);
		j.addProperty("PersonEmail", email);
		j.addProperty("NRIC_Passport_No__pc", nricPassport);
		
		return j;
	}
	
	public String getLastName() {
		return lastName;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public String getEmail() {
		return email;
	}
	
	@Override
	public String toString() {
		return "UpdateAccountRequest [lastName=" + lastName + ", mobilePhone=" + mobilePhone + "]";
	}
}
