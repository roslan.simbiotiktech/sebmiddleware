package com.seb.middleware.salesforce.api;

import com.google.gson.JsonObject;
import com.seb.middleware.salesforce.core.CompositeRequest;
import com.seb.middleware.salesforce.core.SFMethod;

public class UpdateAccountRequestInfo extends CompositeRequest {

	private static final String REF_PREFIX = "UA_";
	
	private String lastName;
	

	
	private String SEBC_Email_1__c;	
	private String SEBC_Email_2__c;
	private String SEBC_Email_3__c;
	
	private String SEBC_Mobile_1__c;
	private String SEBC_Mobile_2__c;
	private String SEBC_Mobile_3__c;

	
	public UpdateAccountRequestInfo(String entityId, String lastName,String SEBC_Email_1__c,String SEBC_Email_2__c,
			String SEBC_Email_3__c,String SEBC_Mobile_1__c,String SEBC_Mobile_2__c,String SEBC_Mobile_3__c) {
		super(REF_PREFIX + entityId);
		this.lastName = lastName;
		
		this.SEBC_Email_1__c=SEBC_Email_1__c;
		this.SEBC_Email_2__c=SEBC_Email_2__c;
		this.SEBC_Email_3__c=SEBC_Email_3__c;
		this.SEBC_Mobile_1__c=SEBC_Mobile_1__c;
		this.SEBC_Mobile_2__c=SEBC_Mobile_2__c;
		this.SEBC_Mobile_3__c=SEBC_Mobile_3__c;
		
		
		setMethod(SFMethod.PATCH);
		setEntityName("Account");
		setEntityId(entityId);
	}

	@Override
	public JsonObject getRequestBody() {
		JsonObject j = new JsonObject();
		
		j.addProperty("LastName", lastName);
		if(this.SEBC_Email_1__c!=null && this.SEBC_Mobile_1__c!=null) {
			j.addProperty("SEBC_Email_1__c", SEBC_Email_1__c);
			j.addProperty("SEBC_Mobile_1__c", SEBC_Mobile_1__c);
		}				
		if(this.SEBC_Email_2__c!=null && this.SEBC_Mobile_2__c!=null) {
			j.addProperty("SEBC_Email_2__c", SEBC_Email_2__c);
			j.addProperty("SEBC_Mobile_2__c", SEBC_Mobile_2__c);
		}				
		if(this.SEBC_Email_3__c!=null && this.SEBC_Mobile_3__c!=null) {
			j.addProperty("SEBC_Email_3__c", SEBC_Email_3__c);
			j.addProperty("SEBC_Mobile_3__c", SEBC_Mobile_3__c);
		}
		return j;
	}
	
	public String getLastName() {
		return lastName;
	}

	
	
	public String getSEBC_Email_1__c() {
		return SEBC_Email_1__c;
	}

	public String getSEBC_Email_2__c() {
		return SEBC_Email_2__c;
	}

	public String getSEBC_Email_3__c() {
		return SEBC_Email_3__c;
	}

	public String getSEBC_Mobile_1__c() {
		return SEBC_Mobile_1__c;
	}

	public String getSEBC_Mobile_2__c() {
		return SEBC_Mobile_2__c;
	}

	public String getSEBC_Mobile_3__c() {
		return SEBC_Mobile_3__c;
	}

	@Override
	public String toString() {
		return "UpdateAccountRequestInfo [lastName=" + lastName + ", SEBC_Email_1__c=" + SEBC_Email_1__c
				+ ", SEBC_Email_2__c=" + SEBC_Email_2__c + ", SEBC_Email_3__c=" + SEBC_Email_3__c
				+ ", SEBC_Mobile_1__c=" + SEBC_Mobile_1__c + ", SEBC_Mobile_2__c=" + SEBC_Mobile_2__c
				+ ", SEBC_Mobile_3__c=" + SEBC_Mobile_3__c + "]";
	}

}
