package com.seb.middleware.salesforce.api;

import com.google.gson.JsonObject;
import com.seb.middleware.constant.SubscriptionStatus;
import com.seb.middleware.constant.SubscriptionType;
import com.seb.middleware.salesforce.core.CompositeRequest;
import com.seb.middleware.salesforce.core.SFMethod;

public class UpdateSubscriptionRequest extends CompositeRequest {

	private static final String REF_PREFIX = "US_";
	
	private String subscriptionEntityId;
	
	private String nickname;
	
	private SubscriptionType type;
	
	private SubscriptionStatus status;
	
	public UpdateSubscriptionRequest(String subscriptionEntityId, String nickname, 
			SubscriptionType type, SubscriptionStatus status) {
		
		super(REF_PREFIX + getPurgedString(subscriptionEntityId));
		
		this.subscriptionEntityId = subscriptionEntityId;
		this.nickname = nickname;
		this.type = type;
		this.status = status;
		
		
		setMethod(SFMethod.PATCH);
		setEntityName("Contract_Subscription__c");
		setEntityId(subscriptionEntityId);
	}

	@Override
	public JsonObject getRequestBody() {
		JsonObject j = new JsonObject();

		j.addProperty("SEBC_Subscription_Nickname__c", nickname); 
		j.addProperty("SEBC_Subscription_Type__c", type.toString());
		j.addProperty("SEBC_Subscription_Status__c", status.toString());
		
		return j;
	}

	public static String getRefPrefix() {
		return REF_PREFIX;
	}

	public String getSubscriptionEntityId() {
		return subscriptionEntityId;
	}

	public String getNickname() {
		return nickname;
	}

	public SubscriptionType getType() {
		return type;
	}

	public SubscriptionStatus getStatus() {
		return status;
	}

	@Override
	public String toString() {
		return "UpdateSubscriptionRequest [subscriptionEntityId=" + subscriptionEntityId + ", nickname=" + nickname
				+ ", type=" + type + ", status=" + status + "]";
	}
}
