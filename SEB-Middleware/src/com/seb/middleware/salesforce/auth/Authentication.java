package com.seb.middleware.salesforce.auth;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonObject;
import com.seb.middleware.salesforce.core.HTTPException;
import com.seb.middleware.salesforce.core.SalesForceConnector;

public class Authentication extends SalesForceConnector {
	
	protected static final Logger logger = LogManager.getLogger(Authentication.class);
	
	private static String BASE_URL = "/services/oauth2/token";
	
	private static String GRANT_TYPE = "password";
	
	private String instanceUrl;
	
	private String clientId;
	
	private String clientSecret;
	
	public Authentication(String instanceUrl, String clientId, String clientSecret) {
		super();
		this.instanceUrl = instanceUrl;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
	}
	
	public Token getToken(String username, String password) throws AuthenticationException, IOException {
	
		Map<String, String> request = new LinkedHashMap<>();
		request.put("grant_type", GRANT_TYPE);
		request.put("client_id", clientId);
		request.put("client_secret", clientSecret);
		request.put("username", username);
		request.put("password", password);

		try {
			JsonObject json = invokeFormPost(instanceUrl + BASE_URL, request);
			return new Token(json);
		}catch(HTTPException e) {
			throw new AuthenticationException("unable to authenticate with remote server, http error " + e.getResponseCode() + " - " + e.getMessage());
		}
	}
}