package com.seb.middleware.salesforce.auth;

public class AuthenticationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2721292315530944911L;

	public AuthenticationException(String message) {
		super(message);
	}
}
