package com.seb.middleware.salesforce.auth;

public class InvalidTokenException extends AuthenticationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7617595683798291388L;

	public InvalidTokenException(String message) {
		super(message);
	}
}
