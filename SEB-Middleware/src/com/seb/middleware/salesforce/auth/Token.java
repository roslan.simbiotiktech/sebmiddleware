package com.seb.middleware.salesforce.auth;

import java.util.Date;

import com.google.gson.JsonObject;

public class Token {

	private String id;
	
	private Date issuedAt;
	
	private String instanceUrl;
	
	private String signature;
	
	private String accessToken;
	
	private String tokenType;

	public Token(JsonObject json) throws InvalidTokenException {
		
		if(json == null) {
			throw new InvalidTokenException("Json response is null");
		}else {
			
			this.id = getString(json, "id");
			this.instanceUrl = getString(json, "instance_url");
			this.signature = getString(json, "signature");
			this.accessToken = getString(json, "access_token");
			this.tokenType = getString(json, "token_type");
			
			if(json.has("issued_at")) {
				this.issuedAt = new Date(json.getAsJsonPrimitive("issued_at").getAsLong());
			}
			
			if(accessToken == null || accessToken.isEmpty()) {
				throw new InvalidTokenException("Json response doesn't contain mandatory keys id");
			}
		}
	}
	
	public String getAuthorization() {
		return tokenType + " " + accessToken;
	}
	
	private String getString(JsonObject json, String memberName) {
		
		if(json.has(memberName)) {
			return json.get(memberName).getAsString();
		}else {
			return null;
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getIssuedAt() {
		return issuedAt;
	}

	public void setIssuedAt(Date issuedAt) {
		this.issuedAt = issuedAt;
	}

	public String getInstanceUrl() {
		return instanceUrl;
	}

	public void setInstanceUrl(String instanceUrl) {
		this.instanceUrl = instanceUrl;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	@Override
	public String toString() {
		return "Token [id=" + id + ", issuedAt=" + issuedAt + ", instanceUrl=" + instanceUrl + ", signature="
				+ signature + ", accessToken=" + accessToken + ", tokenType=" + tokenType + "]";
	}
}
