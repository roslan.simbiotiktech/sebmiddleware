package com.seb.middleware.salesforce.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonObject;

public abstract class CompositeRequest {
	
	private static String BASE_URL = "/services/data/v43.0/sobjects";
	
	private SFMethod method = SFMethod.POST;
	
	private String entityName;

	private String entityId;
	
	private String referenceId;
	
	private String overrideUrl;

	public CompositeRequest(String referenceId) {
		this.referenceId = referenceId;
	}
	
	public abstract JsonObject getRequestBody();
	
	public SFMethod getMethod() {
		return method;
	}

	public void setMethod(SFMethod method) {
		this.method = method;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public String getOverrideUrl() {
		return overrideUrl;
	}

	public void setOverrideUrl(String overrideUrl) {
		this.overrideUrl = overrideUrl;
	}

	@Override
	public String toString() {
		return "CompositeRequest [method=" + method + ", entityName=" + entityName + ", entityId=" + entityId
				+ ", referenceId=" + referenceId + ", overrideUrl=" + overrideUrl + "]";
	}

	public JsonObject toJsonObject() {
		JsonObject j = new JsonObject();
		
		j.addProperty("method", method.toString());
		j.addProperty("url", getUrl());
		j.addProperty("referenceId", referenceId);
		
		JsonObject body = getRequestBody();
		if(body != null)
			j.add("body", getRequestBody());
		
		return j;
	}

	private String getUrl() {
		
		if(overrideUrl != null) {
			return overrideUrl;
		}else {
			StringBuilder sb = new StringBuilder();
			
			sb.append(BASE_URL);
			if(entityName != null) {
				sb.append("/").append(entityName);
			}
			if(entityId != null) {
				sb.append("/").append(entityId);
			}
			
			return sb.toString();
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((referenceId == null) ? 0 : referenceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompositeRequest other = (CompositeRequest) obj;
		if (referenceId == null) {
			if (other.referenceId != null)
				return false;
		} else if (!referenceId.equals(other.referenceId))
			return false;
		return true;
	}
	
	protected static String getPurgedString(String str) {
		return str.replace("@", "").replace(".", "").toUpperCase();
	}
	
	public String getSfDateFormat(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		return sdf.format(date);
	}
	
	public Date parseSfDateFormat(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		return sdf.parse(date);
	}
}

