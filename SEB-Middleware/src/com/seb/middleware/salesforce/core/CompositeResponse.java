package com.seb.middleware.salesforce.core;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class CompositeResponse {

	private Map<String, String> httpHeaders;
	
	private int httpStatusCode;
	
	private String referenceId;
	
	private JsonObject body;
	
	private boolean success;
	
	private String [] messages;
	
	private String [] errors;
	
	private String [] errorCode;
	
	public CompositeResponse(JsonObject json) {
		
		httpStatusCode = json.has("httpStatusCode") ? json.get("httpStatusCode").getAsInt() : 0;
		referenceId = json.has("referenceId") ? json.get("referenceId").getAsString() : null;
		
		httpHeaders = new HashMap<String, String>();
		
		if(json.has("httpHeaders")) {
			JsonObject headers = json.get("httpHeaders").getAsJsonObject();
			
			Iterator<Entry<String, JsonElement>> headerIterator = headers.entrySet().iterator();
			while(headerIterator.hasNext()){
				Entry<String, JsonElement> next = headerIterator.next();
				httpHeaders.put(next.getKey(), next.getValue().getAsString());
			}
		}
		
		if(json.has("body")) {
			
			JsonElement bodyElement = json.get("body");
			
			if(bodyElement.isJsonObject()) {
				
				body = json.get("body").getAsJsonObject();
				
				if(body.has("success")) {
					success = body.get("success").getAsBoolean();
				}else if(body.has("errors")) {
					JsonArray bodyErrors = body.get("errors").getAsJsonArray();
					String [] errs = new String[bodyErrors.size()];
					
					for(int i = 0; i < errs.length; i++) {
						errs[i] = bodyErrors.get(i).getAsString();
					}
				}else if(httpStatusCode < 400){
					success = true;
				}
			}else if(bodyElement.isJsonArray()) {
				
				JsonArray bodies = bodyElement.getAsJsonArray();
				
				messages = new String[bodies.size()];
				errorCode = new String[bodies.size()];
				
				for(int i = 0; i < bodies.size(); i++) {
					JsonObject bo = bodies.get(i).getAsJsonObject();
					
					if(bo.has("message")) {
						messages[i] = bo.get("message").getAsString();
					}
					
					if(bo.has("errorCode")) {
						errorCode[i] = bo.get("errorCode").getAsString();
					}
				}
			}else {
				if(httpStatusCode < 400){
					success = true;
				}
			}
		}else {
			if(httpStatusCode < 400){
				success = true;
			}
		}
	}
	
	public Map<String, String> getHttpHeaders() {
		return httpHeaders;
	}
	
	public int getHttpStatusCode() {
		return httpStatusCode;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public JsonObject getBody() {
		return body;
	}

	public boolean isSuccess() {
		return success;
	}

	public String[] getMessages() {
		return messages;
	}

	public String[] getErrors() {
		return errors;
	}

	public String[] getErrorCode() {
		return errorCode;
	}

	public String getObjectId() {
		if(body != null) {
			if(body.has("id")) {
				return body.get("id").getAsString();
			}
		}
		return null;
	}
	
	public String getFirstObjectId() {
		if(body != null) {
			if(body.has("records") && body.get("records").isJsonArray()) {
				JsonArray records = body.get("records").getAsJsonArray();
				if(records.size() > 0) {
					JsonObject record = records.get(0).getAsJsonObject();
					if(record.has("Id")) {
						return record.get("Id").getAsString();
					}
				}
			}
		}
		return null;
	}
	
	//added by pramod
	public Boolean getQueryAccountInfo() {
		Boolean accountAvailable=false;
		if(body != null) {
			if(body.has("records") && body.get("records").isJsonArray()) {
				JsonArray records = body.get("records").getAsJsonArray();
				if(records.size() > 0) {
					accountAvailable=true;
				}else {
					accountAvailable=false;
				}
			}
		}
		return accountAvailable;
	}
	
	//added by pramod
	public QueryAccountRequestResponseInfo getQueryAccountResponseInfo() {
		QueryAccountRequestResponseInfo queryAccountResponseInfo=new QueryAccountRequestResponseInfo();
		if(body != null) {
			if(body.has("records") && body.get("records").isJsonArray()) {
				JsonArray records = body.get("records").getAsJsonArray();
				if(records.size() > 0) {
					JsonObject record = records.get(0).getAsJsonObject();
					if(record.has("Id")) {
						queryAccountResponseInfo.setSfdcId(record.get("Id").getAsString());
						if(record.has("PersonEmail") && !record.get("PersonEmail").isJsonNull()) {
							queryAccountResponseInfo.setPersonEmail(record.get("PersonEmail").getAsString());
						}
						if(record.has("PersonMobilePhone") && !record.get("PersonMobilePhone").isJsonNull()) {
							queryAccountResponseInfo.setPersonMobilePhone(record.get("PersonMobilePhone").getAsString());
						}												
						if(record.has("NRIC_Passport_No__pc") && !record.get("NRIC_Passport_No__pc").isJsonNull()) {
							queryAccountResponseInfo.setNricPassportNoPc(record.get("NRIC_Passport_No__pc").getAsString());
						}
						if(record.has("SEBC_Email_1__c") && !record.get("SEBC_Email_1__c").isJsonNull()) {
							queryAccountResponseInfo.setSebcEmailOne(record.get("SEBC_Email_1__c").getAsString());
						}
						if(record.has("SEBC_Email_2__c") && !record.get("SEBC_Email_2__c").isJsonNull()) {
							queryAccountResponseInfo.setSebcEmailTwo(record.get("SEBC_Email_2__c").getAsString());
						}
						if(record.has("SEBC_Email_3__c") && !record.get("SEBC_Email_3__c").isJsonNull()) {
							queryAccountResponseInfo.setSebcEmailThree(record.get("SEBC_Email_3__c").getAsString());
						}
						if(record.has("SEBC_Mobile_1__c") && !record.get("SEBC_Mobile_1__c").isJsonNull()) {
							queryAccountResponseInfo.setSebcMobileOne(record.get("SEBC_Mobile_1__c").getAsString());
						}
						if(record.has("SEBC_Mobile_2__c") && !record.get("SEBC_Mobile_2__c").isJsonNull()) {
							queryAccountResponseInfo.setSebcMobileTwo(record.get("SEBC_Mobile_2__c").getAsString());
						}
						if(record.has("SEBC_Mobile_2__c")&& !record.get("SEBC_Mobile_3__c").isJsonNull()) {
							queryAccountResponseInfo.setSebcMobileThree(record.get("SEBC_Mobile_3__c").getAsString());				
						}	
														
					}
				}
			}
		}	
		return queryAccountResponseInfo;		
	}

	public void setHttpHeaders(Map<String, String> httpHeaders) {
		this.httpHeaders = httpHeaders;
	}

	public void setHttpStatusCode(int httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public void setBody(JsonObject body) {
		this.body = body;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public void setMessages(String[] messages) {
		this.messages = messages;
	}

	public void setErrors(String[] errors) {
		this.errors = errors;
	}

	public void setErrorCode(String[] errorCode) {
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return "CompositeResponse [httpHeaders=" + httpHeaders + ", httpStatusCode=" + httpStatusCode + ", referenceId="
				+ referenceId + ", body=" + body + ", success=" + success + ", messages=" + Arrays.toString(messages)
				+ ", errors=" + Arrays.toString(errors) + ", errorCode=" + Arrays.toString(errorCode) + "]";
	}
}