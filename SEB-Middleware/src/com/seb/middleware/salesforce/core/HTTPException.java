package com.seb.middleware.salesforce.core;

public class HTTPException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8156265653593021807L;
	
	private int responseCode;
	
	public HTTPException(int responseCode, String message) {
		super(message);
		
		this.responseCode = responseCode;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
}
