package com.seb.middleware.salesforce.core;

public class QueryAccountRequestResponseInfo {

	private String sfdcId;
	private String personEmail;
	private String personMobilePhone;
	private String nricPassportNoPc;
	private String sebcEmailOne;
	private String sebcEmailTwo;
	private String sebcEmailThree;
	private String sebcMobileOne;
	private String sebcMobileTwo;
	private String sebcMobileThree;
	private Boolean returnUI;
	public String getSfdcId() {
		return sfdcId;
	}
	public void setSfdcId(String sfdcId) {
		this.sfdcId = sfdcId;
	}
	public String getPersonEmail() {
		return personEmail;
	}
	public void setPersonEmail(String personEmail) {
		this.personEmail = personEmail;
	}
	public String getPersonMobilePhone() {
		return personMobilePhone;
	}
	public void setPersonMobilePhone(String personMobilePhone) {
		this.personMobilePhone = personMobilePhone;
	}
	public String getNricPassportNoPc() {
		return nricPassportNoPc;
	}
	public void setNricPassportNoPc(String nricPassportNoPc) {
		this.nricPassportNoPc = nricPassportNoPc;
	}
	public String getSebcEmailOne() {
		return sebcEmailOne;
	}
	public void setSebcEmailOne(String sebcEmailOne) {
		this.sebcEmailOne = sebcEmailOne;
	}
	public String getSebcEmailTwo() {
		return sebcEmailTwo;
	}
	public void setSebcEmailTwo(String sebcEmailTwo) {
		this.sebcEmailTwo = sebcEmailTwo;
	}
	public String getSebcEmailThree() {
		return sebcEmailThree;
	}
	public void setSebcEmailThree(String sebcEmailThree) {
		this.sebcEmailThree = sebcEmailThree;
	}
	public String getSebcMobileOne() {
		return sebcMobileOne;
	}
	public void setSebcMobileOne(String sebcMobileOne) {
		this.sebcMobileOne = sebcMobileOne;
	}
	public String getSebcMobileTwo() {
		return sebcMobileTwo;
	}
	public void setSebcMobileTwo(String sebcMobileTwo) {
		this.sebcMobileTwo = sebcMobileTwo;
	}
	public String getSebcMobileThree() {
		return sebcMobileThree;
	}
	public void setSebcMobileThree(String sebcMobileThree) {
		this.sebcMobileThree = sebcMobileThree;
	}
	public Boolean getReturnUI() {
		return returnUI;
	}
	public void setReturnUI(Boolean returnUI) {
		this.returnUI = returnUI;
	}
	
}
