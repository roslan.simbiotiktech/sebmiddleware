package com.seb.middleware.salesforce.core;

public enum SFMethod {

	POST,
	GET,
	PATCH
	
}
