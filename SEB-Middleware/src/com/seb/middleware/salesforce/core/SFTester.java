package com.seb.middleware.salesforce.core;

import java.io.IOException;

import com.seb.middleware.salesforce.auth.Authentication;
import com.seb.middleware.salesforce.auth.AuthenticationException;
import com.seb.middleware.salesforce.auth.Token;

public class SFTester {

	public static void main(String [] args) throws AuthenticationException, IOException, HTTPException {
		
		Authentication auth = new Authentication("https://test.salesforce.com", 
				"3MVG9e2mBbZnmM6nTNqVlELiUikb8p2HRge5g_g5oB_N2plyzmHjsSesn07qx51wcBKOzzqFBlaQFY45Qmyj6", 
				"4348439042887395801");
		
		Token token = auth.getToken("sebc@lavaprotocols.com", "S@bC@repass123YjBnZF5HqH0XWMWrOV1p04FNB");
		System.out.println(token);
		/*
		 * SalesForceCompositeConnector connector = new
		 * SalesForceCompositeConnector(token); CreateAccountRequest request1 = new
		 * CreateAccountRequest("xxxxtest01@test01mail.com", "john", "0123456789");
		 * CreateAccountRequest request2 = new
		 * CreateAccountRequest("xxxxtest01@test02mail.com", "john", "0123456789");
		 * 
		 * Map<CompositeRequest, CompositeResponse> response =
		 * connector.invoke(request1, request2);
		 * 
		 * System.out.println(response);
		 */
		/*
		 * 
		 * UpdateAccountRequest uRequest = new
		 * UpdateAccountRequest(response.getObjectId(), "New Name ", "01123232323");
		 * CompositeResponse uResponse = connector.invoke(uRequest);
		 * 
		 * System.out.println(uResponse);
		 */
	}
}
