package com.seb.middleware.salesforce.core;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.salesforce.auth.Authentication;
import com.seb.middleware.salesforce.auth.AuthenticationException;
import com.seb.middleware.salesforce.auth.Token;

public class SFUtility {

	private static final Logger logger = LogManager.getLogger(SFUtility.class);

	
	public static Token getToken(SettingHelper sHelper) throws AuthenticationException, IOException {
		Authentication auth = new Authentication(
				sHelper.getSalesForceInstanceUrl(), 
				sHelper.getSalesForceClientId(), 
				sHelper.getSalesForceClientSecret());
		
		Token token = auth.getToken(
				sHelper.getSalesForceUsername(), 
				sHelper.getSalesForcePassword());
		logger.info("SF token {}", token);
		return token;
	}
}
