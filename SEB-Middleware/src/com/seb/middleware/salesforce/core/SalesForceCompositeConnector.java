package com.seb.middleware.salesforce.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.seb.middleware.salesforce.auth.Token;

public class SalesForceCompositeConnector extends SalesForceConnector {

	private static final String INSTANCE_URL_COMMON = "/services/data/v43.0/composite";
	
	private String instanceUrl;
	
	private String authorization;
	
	private boolean logRequest = true;
	
	public SalesForceCompositeConnector(Token token) {
		this.instanceUrl = token.getInstanceUrl();
		this.authorization = token.getAuthorization();
	}
	
	public CompositeResponse invoke(CompositeRequest request) throws IOException, HTTPException {
		Map<CompositeRequest, CompositeResponse> responses = invoke(new CompositeRequest[] { request });
		return responses.get(request);
	}
	
	public Map<CompositeRequest, CompositeResponse> invoke(Collection<CompositeRequest> requests) throws IOException, HTTPException {
		return invoke(requests.toArray(new CompositeRequest[requests.size()]));
	}
	
	public Map<CompositeRequest, CompositeResponse> invoke(CompositeRequest ... requests) throws IOException, HTTPException {
		
		JsonObject requestJson = getCompositedRequest(requests);
		JsonObject responseJson = invokeJsonPost(instanceUrl + INSTANCE_URL_COMMON, authorization, requestJson, logRequest);
		
		return decomposition(responseJson, requests);
	}
	
	private Map<CompositeRequest, CompositeResponse> decomposition(JsonObject responseJson, CompositeRequest ... requests) {
		
		List<CompositeResponse> responses = getDecompositedResponse(responseJson);
		
		Map<CompositeRequest, CompositeResponse> map = new HashMap<CompositeRequest, CompositeResponse>();
		
		for(CompositeResponse response : responses) {
			for(CompositeRequest request : requests) {
				if(isReferenceIdEquals(request.getReferenceId(), response.getReferenceId())) {
					map.put(request, response);
					break;
				}
			}
		}
		
		return map;
	}
	
	private List<CompositeResponse> getDecompositedResponse(JsonObject responseJson){
		
		List<CompositeResponse> responses = new ArrayList<CompositeResponse>();
		
		if(responseJson != null && responseJson.has("compositeResponse")) {
			JsonArray arr = responseJson.get("compositeResponse").getAsJsonArray();
			
			for(int i = 0; i < arr.size(); i++) {
				
				JsonObject j = arr.get(i).getAsJsonObject();
				CompositeResponse response = new CompositeResponse(j);
				responses.add(response);
			}
		}
		
		return responses;
	}
	
	private JsonObject getCompositedRequest(CompositeRequest ... requests ) {
		
		JsonArray arr = new JsonArray();
		
		for(CompositeRequest request : requests) {
			arr.add(request.toJsonObject());
		}
		
		JsonObject j = new JsonObject();
		j.add("compositeRequest", arr);
		return j;
	}
	
	/**
	 * Null = Null
	 */
	private boolean isReferenceIdEquals(String referenceId, String responseReferenceId) {
		if(referenceId == null && responseReferenceId == null) {
			return true;
		}else if(referenceId != null && responseReferenceId != null) {
			return referenceId.equals(responseReferenceId);
		}else {
			return false;
		}
	}
	
	public void setLogRequest(boolean logRequest) {
		this.logRequest = logRequest;
	}
	
}