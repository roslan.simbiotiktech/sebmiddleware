package com.seb.middleware.salesforce.core;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class SalesForceConnector {

	protected static final Logger logger = LogManager.getLogger(SalesForceConnector.class);
	
	protected int connectTimeout = 5000;
	
	protected int readTimeout = 30000;
	
	protected JsonObject invokeJsonPost(String apiUrl, String authorization, JsonObject json, boolean logRequest) throws IOException, HTTPException {
		
		long start = System.currentTimeMillis();
		try {

			String request = json.toString();
			
			logger.info("SalesForce ::sending request to " + apiUrl);
			if(logRequest)
				logger.debug(request);
			
			URL url = new URL(apiUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("Content-Length", String.valueOf(request.length()));
			connection.setRequestProperty("Authorization", authorization);
			connection.setRequestProperty("Accept", "application/json");
			connection.setConnectTimeout(connectTimeout);
			connection.setReadTimeout(readTimeout);
			
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			
			OutputStream os = connection.getOutputStream();
			os.write(request.getBytes());
			os.close();
			
			if(connection.getResponseCode() >= 400){
				InputStream is = connection.getErrorStream();
				StringBuilder sb = new StringBuilder();
	            int chr;
	            while ((chr = is.read()) != -1) {
	                sb.append((char) chr);
	            }
	            is.close();
	            
	            logger.error("http error - " + connection.getResponseCode());
	            logger.error(sb.toString());

	            throw new HTTPException(connection.getResponseCode(), sb.toString());
			}else{
				InputStream is = connection.getInputStream();
				StringBuilder sb = new StringBuilder();
	            int chr;
	            while ((chr = is.read()) != -1) {
	                sb.append((char) chr);
	            }
	            is.close();
	            
	            String jsonResponse = sb.toString();
	            logger.debug(jsonResponse);
	            JsonObject response = getJsonResponse(jsonResponse);
	            return response;
			}
		}finally {
			logger.debug("Time taken " + (System.currentTimeMillis() - start) + "ms");
		}
	}
	
	protected JsonObject invokeFormPost(String apiUrl, Map<String, String> params) throws IOException, HTTPException {
		
		long start = System.currentTimeMillis();
		try {

			String request = generatePostRequest(params);
			
			logger.info("SalesForce ::sending request to " + apiUrl);
			
			URL url = new URL(apiUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-Length", String.valueOf(request.length()));
			connection.setRequestProperty("Accept", "application/json");
			connection.setConnectTimeout(connectTimeout);
			connection.setReadTimeout(readTimeout);
			
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			
			OutputStream os = connection.getOutputStream();
			os.write(request.getBytes());
			os.close();
			
			if(connection.getResponseCode() >= 400){
				InputStream is = connection.getErrorStream();
				StringBuilder sb = new StringBuilder();
	            int chr;
	            while ((chr = is.read()) != -1) {
	                sb.append((char) chr);
	            }
	            is.close();
	            
	            logger.error("http error - " + connection.getResponseCode());
	            logger.error(sb.toString());

	            throw new HTTPException(connection.getResponseCode(), sb.toString());
			}else{
				InputStream is = connection.getInputStream();
				StringBuilder sb = new StringBuilder();
	            int chr;
	            while ((chr = is.read()) != -1) {
	                sb.append((char) chr);
	            }
	            is.close();
	            
	            String json = sb.toString();
	            JsonObject response = getJsonResponse(json);
	            logger.debug(response);
	            return response;
			}
		}finally {
			logger.debug("Time taken " + (System.currentTimeMillis() - start) + "ms");
		}
	}
	
	private String generatePostRequest(Map<String, String> params) throws UnsupportedEncodingException{
		
		StringBuilder sb = new StringBuilder();
		
		Iterator<String> iterator = params.keySet().iterator();
		while(iterator.hasNext()){
			String next = iterator.next();
			String value = params.get(next);
			
			sb.append(URLEncoder.encode(next, "UTF-8"));
			sb.append("=");
			sb.append(URLEncoder.encode(value, "UTF-8"));
			
			if(iterator.hasNext()){
				sb.append("&");
			}
		}
				
		return sb.toString();
	}

	private JsonObject getJsonResponse(String rawJson){
		JsonObject json = null;
		if(rawJson != null && !rawJson.isEmpty() && !rawJson.trim().equals("\"\"")){
			JsonParser parser = new JsonParser();
			JsonElement jsonElement = parser.parse(rawJson);
			json = jsonElement.getAsJsonObject();
		}else{
			json = new JsonObject();
		}
		
		return json;
	}
	
}
