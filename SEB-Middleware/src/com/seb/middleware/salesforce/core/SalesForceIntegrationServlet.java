package com.seb.middleware.salesforce.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.constant.UserStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.Contract;
import com.seb.middleware.jpa.dao.Subscription;
import com.seb.middleware.jpa.dao.SubscriptionMapping;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.SalesForceHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.salesforce.api.CreateAccountRequest;
import com.seb.middleware.salesforce.api.CreateContractRequest;
import com.seb.middleware.salesforce.api.CreateSubscriptionRequest;
import com.seb.middleware.salesforce.api.QueryAccountRequest;
import com.seb.middleware.salesforce.api.QueryContractRequest;
import com.seb.middleware.salesforce.api.UpdateAccountRequest;
import com.seb.middleware.salesforce.api.UpdateSubscriptionRequest;
import com.seb.middleware.salesforce.auth.AuthenticationException;
import com.seb.middleware.salesforce.auth.Token;
import com.seb.middleware.utility.StringUtil;

public class SalesForceIntegrationServlet extends HttpServlet {
	
	private static final long serialVersionUID = -1627061403537432195L;
	private static final Logger logger = LogManager.getLogger(SalesForceIntegrationServlet.class);

	private static final int BATCH_SIZE = 25;
	
	/**
	 * e.g. sync=member
	 */
	private static final String PARAM = "sync";
	
	private static final String SYNC_MEMBER = "member";
	private static final String SYNC_CONTRACT = "contract";
	private static final String SYNC_SUBSCRIPTION = "subscription";
	
	private static boolean RUNNING = false;
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		doPost(request, response);
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) {

		logger.info("Received SalesForce Integration request.");

		if(RUNNING) {
			logger.error("Integrator already running, skip processing");
		}else {
			
			RUNNING = true;
			
			try {
				String syncType = request.getParameter(PARAM);
				if(StringUtil.isEmpty(syncType)) {
					doMemberSync();
					doContractSync();
					doSubscriptionSync();
				}else {
					logger.info("Syncing distinct type '{}'.", syncType);
					if(syncType.equalsIgnoreCase(SYNC_MEMBER)) {
						doMemberSync();
					}else if(syncType.equalsIgnoreCase(SYNC_CONTRACT)) {
						doContractSync();
					}else if(syncType.equalsIgnoreCase(SYNC_SUBSCRIPTION)) {
						doSubscriptionSync();
					}
				}
			}finally {
				RUNNING = false;
			}
		}
		
		logger.info("End Process SalesForce Integration request.");
	}
	
	private void doMemberSync() {
		logger.info("doMemberSync() Started");
		
		DatabaseFacade db = new DatabaseFacade();
		try{
			
			SettingHelper settingHelper = new SettingHelper(db);
			SalesForceHelper sHelper = new SalesForceHelper(db);
			MemberHelper mHelper = new MemberHelper(db);
			
			List<User> list = sHelper.getUnsyncedUsers();
			if(list != null && !list.isEmpty()) {
				
				logger.info("syncing total {} users into salesforce", list.size());

				Token token = SFUtility.getToken(settingHelper);
				
				List<User> subList = new ArrayList<User>();
				for(int i = 0; i < list.size(); i++) {
					
					User u = list.get(i);
					
					if(subList.size() < BATCH_SIZE) {
						subList.add(u);
					}else {
						
						doMemberSync(db, subList, token, mHelper);
						
						subList.clear();
						subList.add(u);
					}
				}
				
				if(!subList.isEmpty()) {
					doMemberSync(db, subList, token, mHelper);
				}
			}
		} catch (NamingException e) {
			logger.error("error getting person object from ldap", e);
		} catch (AuthenticationException e) {
			logger.error("error getting auth token from sfdc", e);
		} catch (IOException | HTTPException e) {
			logger.error("error general connection error", e);
		}catch (DatabaseFacadeException e) {
			logger.error("database exception - " + e.getMessage(), e);
		} finally{
			db.close();
		}
		
		logger.info("doMemberSync() Completed");
	}
	
	private void doMemberSync(DatabaseFacade db, List<User> list, Token token, MemberHelper mHelper) throws NamingException, IOException, HTTPException, DatabaseFacadeException {
		
		SalesForceCompositeConnector connector = new SalesForceCompositeConnector(token);
		SettingHelper settingHelper = new SettingHelper(db);
		
		String accountRecordTypeId = settingHelper.getSalesForceAccountRecordTypeId();
		
		db.beginTransaction();
		int successCount = 0;
		
		Map<CompositeRequest, User> requests = new HashMap<CompositeRequest, User>();
		for(User u : list) {
			Person p = mHelper.getPerson(u.getUserId());
			
			if(StringUtil.isEmpty(u.getSfdcId())) {
				QueryAccountRequest qa = new QueryAccountRequest(u.getUserId(), p.getMobilePhone(), p.getNricPassport());
				CompositeResponse qcr = connector.invoke(qa);
				
				if(qcr.isSuccess()) {
					u.setSfdcId(qcr.getFirstObjectId());
				}
				
				if(!StringUtil.isEmpty(u.getSfdcId())) {
					successCount++;
					u.setSfdcSyncDate(new Date());
					db.update(u);
				}else {
					CreateAccountRequest ca = new CreateAccountRequest(u.getUserId(), p.getName(), 
							p.getMobilePhone(), getUserEmail(u), p.getNricPassport(), accountRecordTypeId);
					requests.put(ca, u);
				}
			}else {
				UpdateAccountRequest ua = new UpdateAccountRequest(u.getSfdcId(), p.getName(), p.getMobilePhone(), getUserEmail(u), p.getNricPassport());
				requests.put(ua, u);
			}
		}
		
		Map<CompositeRequest, CompositeResponse> responses = connector.invoke(requests.keySet());
		
		for(CompositeRequest r : responses.keySet()) {
			
			CompositeResponse response = responses.get(r);
			User u = requests.get(r);
			
			if(response.isSuccess()) {
				successCount++;
				Date now = new Date();
				
				if(StringUtil.isEmpty(u.getSfdcId())) {
					u.setSfdcId(response.getObjectId());
				}
				u.setSfdcSyncDate(now);
				db.update(u);
				
			}else {
				logger.error("error syncing users {} into salesforce, error response {}", u.getUserId(), response);
			}
		}
		
		db.commit();
		logger.info("completed processed {} records, successful {} records", list.size(), successCount);
	}
	
	private String getUserEmail(User u) {
		if(u.getExistingEmail() == null) {
			return u.getUserId();
		}else if(u.getStatus() == UserStatus.PENDING || u.getStatus() == UserStatus.SUSPENDED) {
			return u.getExistingEmail();
		}else {
			return u.getUserId();
		}
	}
	
	private void doContractSync() {
		logger.info("doContractSync() Started");
		
		DatabaseFacade db = new DatabaseFacade();
		try{
			
			SettingHelper settingHelper = new SettingHelper(db);
			SalesForceHelper sHelper = new SalesForceHelper(db);
			
			List<Contract> list = sHelper.getUnsyncedContracts();
			if(list != null && !list.isEmpty()) {
				
				logger.info("syncing total {} contracts into salesforce", list.size());
				
				Token token = SFUtility.getToken(settingHelper);
				
				List<Contract> subList = new ArrayList<Contract>();
				for(int i = 0; i < list.size(); i++) {
					
					Contract u = list.get(i);
					
					if(subList.size() < BATCH_SIZE) {
						subList.add(u);
					}else {
						
						doContractSync(db, subList, token);
						
						subList.clear();
						subList.add(u);
					}
				}
				
				if(!subList.isEmpty()) {
					doContractSync(db, subList, token);
				}
			}
		} catch (AuthenticationException e) {
			logger.error("error getting auth token from sfdc", e);
		} catch (IOException | HTTPException e) {
			logger.error("error general connection error", e);
		}catch (DatabaseFacadeException e) {
			logger.error("database exception - " + e.getMessage(), e);
		} finally{
			db.close();
		}
		
		logger.info("doContractSync() Completed");
	}
	
	private void doContractSync(DatabaseFacade db, List<Contract> list, Token token) throws DatabaseFacadeException, IOException, HTTPException {
		
		SalesForceCompositeConnector connector = new SalesForceCompositeConnector(token);
		
		db.beginTransaction();
		int successCount = 0;
		
		Map<CompositeRequest, Contract> requests = new HashMap<CompositeRequest, Contract>();
		for(Contract c : list) {
			QueryContractRequest qc = new QueryContractRequest(c.getContractAccountNumber());
			CompositeResponse qcr = connector.invoke(qc);
			
			if(qcr.isSuccess()) {
				c.setSfdcId(qcr.getFirstObjectId());
			}
			
			if(!StringUtil.isEmpty(c.getSfdcId())) {
				successCount++;
				db.update(c);
			}else {
				CreateContractRequest ca = new CreateContractRequest(c.getContractAccountNumber(), c.getContractAccountName(), c.getAgencyName());
				requests.put(ca, c);
			}
		}
		
		
		Map<CompositeRequest, CompositeResponse> responses = connector.invoke(requests.keySet());
		
		
		for(CompositeRequest r : responses.keySet()) {
			
			CompositeResponse response = responses.get(r);
			Contract c = requests.get(r);
			
			if(response.isSuccess()) {
				successCount++;
				c.setSfdcId(response.getObjectId());
				db.update(c);
			}else {
				logger.error("error syncing contracts {} into salesforce, error response {}", c.getContractAccountNumber(), response);
			}
		}
		
		db.commit();
		logger.info("completed processed {} records, successful {} records", list.size(), successCount);
	}
	
	private void doSubscriptionSync() {
		logger.info("doSubscriptionSync() Started");
		
		DatabaseFacade db = new DatabaseFacade();
		try{
			
			SettingHelper settingHelper = new SettingHelper(db);
			SalesForceHelper sHelper = new SalesForceHelper(db);
			SubscriptionHelper subHelper = new SubscriptionHelper(db);
			
			List<SubscriptionMapping> list = sHelper.getUnsyncedSubscriptions();
			if(list != null && !list.isEmpty()) {
				
				logger.info("syncing total {} subscriptions into salesforce", list.size());
				
				Token token = SFUtility.getToken(settingHelper);
				
				List<SubscriptionMapping> subList = new ArrayList<SubscriptionMapping>();
				for(int i = 0; i < list.size(); i++) {
					
					SubscriptionMapping u = list.get(i);
					
					if(subList.size() < BATCH_SIZE) {
						subList.add(u);
					}else {
						
						doSubscriptionsSync(db, subList, token, subHelper);
						
						subList.clear();
						subList.add(u);
					}
				}
				
				if(!subList.isEmpty()) {
					doSubscriptionsSync(db, subList, token, subHelper);
				}
			}
		} catch (AuthenticationException e) {
			logger.error("error getting auth token from sfdc", e);
		} catch (IOException | HTTPException e) {
			logger.error("error general connection error", e);
		}catch (DatabaseFacadeException e) {
			logger.error("database exception - " + e.getMessage(), e);
		} finally{
			db.close();
		}
		
		
		logger.info("doSubscriptionSync() Completed");
	}
	
	private void doSubscriptionsSync(DatabaseFacade db, List<SubscriptionMapping> list, Token token, SubscriptionHelper sHelper) throws DatabaseFacadeException, IOException, HTTPException {
		
		Long [] subIds = new Long[list.size()];
		
		for(int i = 0; i < list.size(); i++) {
			subIds[i] = list.get(i).getSubsId();
		}
		
		List<Subscription> subscriptions = sHelper.getSubscriptions(subIds);
		Map<Long, Subscription> subMap = new HashMap<Long, Subscription>();
		for(Subscription s : subscriptions) {
			subMap.put(s.getSubsId(), s);
		}
		
		Map<CompositeRequest, Subscription> requests = new HashMap<CompositeRequest, Subscription>();
		for(SubscriptionMapping c : list) {
			Subscription subscription = subMap.get(c.getSubsId());
			
			if(StringUtil.isEmpty(subscription.getSfdcId())) {
				CreateSubscriptionRequest cs = new CreateSubscriptionRequest(c.getAccountSfdcId(), c.getContractSfdcId(), 
						subscription.getUserEmail(), subscription.getContractAccNo(),
						subscription.getContractAccNick(), subscription.getSubsType(), subscription.getStatus());
				
				requests.put(cs, subscription);
			}else {
				UpdateSubscriptionRequest us = new UpdateSubscriptionRequest(subscription.getSfdcId(), 
						subscription.getContractAccNick(), subscription.getSubsType(), subscription.getStatus());
				
				requests.put(us, subscription);
			}
		}
		
		SalesForceCompositeConnector connector = new SalesForceCompositeConnector(token);
		Map<CompositeRequest, CompositeResponse> responses = connector.invoke(requests.keySet());
		
		db.beginTransaction();
		int successCount = 0;
		for(CompositeRequest r : responses.keySet()) {
			
			CompositeResponse response = responses.get(r);
			Subscription s = requests.get(r);
			
			if(response.isSuccess()) {
				successCount++;
				Date now = new Date();
				
				if(StringUtil.isEmpty(s.getSfdcId())) {
					s.setSfdcId(response.getObjectId());
				}
				s.setSfdcSyncDate(now);
				db.update(s);
			}else {
				logger.error("error syncing subscriptions {} into salesforce, error response {}", s.getSubsId(), response);
			}
		}
		
		db.commit();
		logger.info("completed processed {} records, successful {} records", list.size(), successCount);
	}
}
