package com.seb.middleware.scheduler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.naming.NamingException;
import javax.swing.Timer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.seb.middleware.constant.NotificationStatus;
import com.seb.middleware.core.BaseService;
import com.seb.middleware.email.EmailManager;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.BillReadyNotification;
import com.seb.middleware.jpa.dao.BillingTransaction;
import com.seb.middleware.jpa.dao.Subscription;
import com.seb.middleware.jpa.dao.TaskTracker;
import com.seb.middleware.jpa.helper.MemberHelper;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.SubscriptionHelper;
import com.seb.middleware.jpa.helper.TaskTrackerHelper;
import com.seb.middleware.ldap.Person;
import com.seb.middleware.sms.SMSManager;

public class BillReadinessNotifier implements BaseService, ActionListener {

	private static final Logger logger = LogManager.getLogger(BillReadinessNotifier.class);
	
	private static final String TASK_KEY = "BILL_READINESS_NOTIFICATION";
	
	private Timer timer = null;
	
	public BillReadinessNotifier(){
		
	}
	
	@Override
	public void startService() {
		timer = new Timer(30 * 60 * 1000, this); // 30 minutes
		timer.start();
		logger.info("Bill Readiness Notifier started");
	}

	@Override
	public void stopService() {
		if(timer != null){
			timer.stop();
			timer = null;
		}
		logger.info("Bill Readiness Notifier stopped");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		DatabaseFacade db = new DatabaseFacade();
		try{
			db.beginTransaction();
			
			TaskTrackerHelper helper = new TaskTrackerHelper(db);
			TaskTracker taskTracker = helper.get(TASK_KEY);
			
			Date previousProcessingTime = taskTracker.getTaskDatetime(); 
			helper.commitTask(taskTracker);
			
			SettingHelper setting = new SettingHelper(db);
			SubscriptionHelper sHelper = new SubscriptionHelper(db);
			MemberHelper mHelper = new MemberHelper(db);
			
			String statementRootPath = setting.getStatementRootPath();
			String paynowUrl = setting.getPaynowUrl();
			
			List<BillingTransaction> list = sHelper.getNewBillingTransaction(previousProcessingTime);
			
			if(list != null && !list.isEmpty()){
				logger.debug("Total " + list.size() + " bill to notify");
				
				SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
				
				for(BillingTransaction bill : list){
					ArrayList<Subscription> subs = sHelper.getSubscriptionsByContract(bill.getContractAccountNumber());
					
					if(subs != null && !subs.isEmpty()){
						for(Subscription sub : subs){
							if(sub.getNotifyByEmail() == 1 || sub.getNotifyBySMS() == 1){
								
								try{
									Person person = mHelper.getPerson(sub.getUserEmail());
									
									if(sub.getNotifyByEmail() == 1){
										String invoiceYear = sdfYear.format(bill.getInvoiceDate());
										String filePath = sHelper.getInvoicePath(statementRootPath, bill.getPdfPrintDocumentNumber(), 
												sub.getContractAccNo(), invoiceYear);
										String url = paynowUrl.replace("{ca}", sub.getContractAccNo());
										
										File file = new File(filePath);
										if(file.exists() && file.canRead()){
											notifyByEmail(person, filePath, sub, 
													bill.getCurrentAmount(), bill.getCurrentTotalAmount(), bill.getDueDate(), 
													bill.getInvoiceDate(), url);	
										}else{
											logger.error("unable to read invoice file - " + filePath);
										}
									}
									
									if(sub.getNotifyBySMS() == 1){
										notifyBySMS(person, sub);
									}
									
									if(sub.getNotifyByMobilePush() == 1) {
										BillReadyNotification noti = new BillReadyNotification();
										noti.setStatus(NotificationStatus.UNSENT);
										noti.setUserEmail(sub.getUserEmail());
										noti.setText("Your latest SESCO eStatement is now available for download.");
										noti.setContractAccountNumber(bill.getContractAccountNumber());
										noti.setInvoiceNumber(bill.getInvoiceNumber());
										noti.setInvoiceDate(bill.getInvoiceDate());
										
										db.insert(noti);
									}
								}catch(NamingException ee){
									logger.error("Error getting member", ee);
								}
							}
						}
					}
				}
			}
			
			db.commit();
		} catch (DatabaseFacadeException ex) {
			logger.error("Error checking bill readiness", ex);
		} catch (Throwable ex) {
			logger.error("General Error checking bill readiness", ex);
		} finally{
			db.close();
		}
	}
	
	private void notifyBySMS(Person person, Subscription sub) {
		try {
			SMSManager manager = new SMSManager();
			manager.sendBillReady(person.getMobilePhone());
		} catch (IOException e) {
			logger.error("Error sending sms bill readiness", e);
		}
	}

	private void notifyByEmail(Person person, String filePath, Subscription sub, 
			BigDecimal currentAmount, BigDecimal currentTotalAmount, Date dueDate, 
			Date invoiceDate, String payUrl){
		
		try {
			EmailManager manager = new EmailManager();
			manager.sendBillReadinessNotification(sub.getUserEmail(), person.getName(), sub.getContractAccNo(), 
					currentAmount, currentTotalAmount, dueDate, payUrl, invoiceDate, filePath);
		} catch (IOException | MessagingException | NamingException e) {
			logger.error("Error sending sms bill readiness", e);
		}
	}
}
