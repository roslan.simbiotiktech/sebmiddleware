package com.seb.middleware.scheduler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.Timer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import com.seb.middleware.constant.NotificationStatus;
import com.seb.middleware.core.BaseService;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.DatabaseFacadeException;
import com.seb.middleware.jpa.dao.CaseReport;
import com.seb.middleware.jpa.dao.ReportNotification;
import com.seb.middleware.jpa.dao.TaskTracker;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.jpa.helper.TaskTrackerHelper;

public class CaseReportChecker implements BaseService, ActionListener {

	private static final Logger logger = LogManager.getLogger(CaseReportChecker.class);
	
	private static final String TASK_KEY = "REPORT_NOTIFICATION_CHECK";
	
	private Timer timer = null;
	
	public CaseReportChecker(){
		
	}
	
	@Override
	public void startService() {
		DatabaseFacade db = new DatabaseFacade();
		try{
			SettingHelper setting = new SettingHelper(db);
			int seconds = setting.getCaseReportCheckInSeconds();
			timer = new Timer(seconds * 1000, this);
			timer.start();
			logger.info("Case Report checker started");
		}finally{
			db.close();
		}
	}

	@Override
	public void stopService() {
		if(timer != null){
			timer.stop();
			timer = null;
		}
		logger.info("Case Report checker stopped");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		DatabaseFacade db = new DatabaseFacade();
		try{
			db.beginTransaction();
			
			TaskTrackerHelper helper = new TaskTrackerHelper(db);
			
			TaskTracker taskTracker = helper.get(TASK_KEY);
			
			Date previousProcessingTime = taskTracker.getTaskDatetime(); 
			helper.commitTask(taskTracker);
			
			Criterion statusCheck = Restrictions.or(
					Restrictions.isNull("previousNotifiedStatus"),
					Restrictions.neProperty("status", "previousNotifiedStatus"));
			
			Criteria criteria = db.getSession()
					.createCriteria(CaseReport.class)
					.add(Restrictions.eq("deletedFlag", 0))
					.add(Restrictions.gt("updatedDatetime", previousProcessingTime)) // Updated since last Notification
					.add(statusCheck);
			
			ArrayList<CaseReport> updatedReports = db.query(criteria);
			if(updatedReports != null && !updatedReports.isEmpty()){
				logger.info("Adding notification to send queue ["+updatedReports.size()+"]");

				for(CaseReport report : updatedReports){
					
					String notificationText = null;
					String caseId = null;
					
					switch (report.getStatus()) {
					case NEW:
						notificationText = "SEB: Your report has been received. A case number will be assigned. Thank you.";
						break;
					case OPEN:
						caseId = String.format("%06d", report.getCaseNumber());
						notificationText = String.format("SEB: Your report %s has been assigned for further action. For further enquiry, do call us at 1300883111.", caseId);
						break;
					case ESCALATED:
						break;
					case ASSIGNED:
						break;
					case RESOLVED:
						caseId = String.format("%06d", report.getCaseNumber());
						notificationText = String.format("SEB: Your report %s has been resolved. For enquiry, call us at 1300883111.", 
								caseId);
						break;
					case CLOSED:
						break;
					default:
						break;
					}
					
					if(notificationText != null){
						ReportNotification notification = new ReportNotification();
						notification.setUserEmail(report.getUserEmail());
						notification.setText(notificationText);
						notification.setStatus(NotificationStatus.UNSENT);
						notification.setTransId(report.getTransId());
						db.insert(notification);
					}
					
					// With or without notification, we update the status
					String hqlUpdateUser = "update CaseReport set previousNotifiedStatus = :prevNotifiedStatus where transId = :tId";
					db.createQuery(hqlUpdateUser)
							.setParameter("prevNotifiedStatus", report.getStatus())
							.setParameter("tId", report.getTransId())
							.executeUpdate();
				}
			}
			
			db.commit();
		} catch (DatabaseFacadeException ex) {
			logger.error("Error checking case report", ex);
		}finally{
			db.close();
		}
	}
}
