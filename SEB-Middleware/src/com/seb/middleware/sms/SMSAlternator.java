package com.seb.middleware.sms;

public class SMSAlternator {

	private int senderIndex;

	public int getSenderIndex() {
		return senderIndex;
	}

	public void setSenderIndex(int senderIndex) {
		this.senderIndex = senderIndex;
	}

	@Override
	public String toString() {
		return "SMSAlternator [senderIndex=" + senderIndex + "]";
	}
}
