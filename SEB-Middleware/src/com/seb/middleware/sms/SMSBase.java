package com.seb.middleware.sms;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.sms.sender.IsentricSMSSender;
import com.seb.middleware.sms.sender.MacroKioskSMSSender;
import com.seb.middleware.sms.sender.SMSSender;

public class SMSBase {

	protected static final Logger logger = LogManager.getLogger(SMSBase.class);

	private static boolean roundRobinInitialized = false;
	private static LoadingCache<String, SMSAlternator> roundRobinCache;

	protected boolean testingEnvironment;
	
	private boolean sms0Enabled;
	private boolean sms1Enabled;
	
	private static void initializeRoundRobin(int activationCodeTimeout){
		roundRobinCache = 
				CacheBuilder.newBuilder()
					.expireAfterAccess(activationCodeTimeout, TimeUnit.MINUTES)
					.build(new CacheLoader<String, SMSAlternator>() {
			public SMSAlternator load(String userId) {
				SMSAlternator alt = new SMSAlternator();
				alt.setSenderIndex(1);
				return alt;
			}
		});
		
		roundRobinInitialized = true;
	}
	
	public SMSBase(){
		DatabaseFacade db = new DatabaseFacade();
		try{
			SettingHelper sHelper = new SettingHelper(db);
			testingEnvironment = sHelper.isTestingEnvironment();
			sms0Enabled = sHelper.isSMS0Enabled();
			sms1Enabled = sHelper.isSMS1Enabled();
			
			if(!roundRobinInitialized){
				int activationCodeTimeout = sHelper.getActivationCodeExpirationInMinutes();
				initializeRoundRobin(activationCodeTimeout);
			}
		}finally{
			db.close();
		}
	}
	
	/*
	 * Expected response:
	 *  {MsgID}, {Msisdn}, {Status}
	 */
	protected boolean send(String msisdn, String text, SMSType type) throws IOException{
		SMSSender sender = getSender(msisdn, type);
		if(sender != null){
			return sender.send(msisdn, text);	
		}else{
			logger.info("No sms sender available");
			return false;
		}
		
	}
	
	protected String processTemplateFile(String filePath, Map<String, String> properties) throws IOException{
		String content = new String(Files.readAllBytes(Paths.get(filePath)));
		
		Set<Entry<String, String>> set = properties.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		
		while(iterator.hasNext()){
			Entry<String, String> next = iterator.next();
			content = content.replace("{" + next.getKey() + "}", next.getValue());
		}
		
		return content;
	}
	
	private SMSSender getSender(String userId, SMSType type){
		
		if(sms0Enabled && sms1Enabled){
			int nextSenderIndex = getNextSender(userId);
			if(nextSenderIndex == 0){
				// MicroKiosk
				logger.debug("using macro kiosk sms");
				return getMacroKiosk();
			}else{
				// iSentric
				logger.debug("using iSentric sms");
				return getIsentric(type);
			}	
		}else if(sms0Enabled){
			logger.debug("using macro kiosk sms");
			return getMacroKiosk();
		}else if(sms1Enabled){
			logger.debug("using iSentric sms");
			return getIsentric(type);
		}else{
			return null;
		}
	}
	
	private int getNextSender(String msisdn) {
		try {
			SMSAlternator alternator = roundRobinCache.get(msisdn);
			int next = alternator.getSenderIndex() % 2;
			alternator.setSenderIndex(alternator.getSenderIndex() + 1);
			return next;
		} catch (ExecutionException e) {
			logger.error("Error getting cache - " + e.getMessage(), e);
			return 0;
		}
	}

	private MacroKioskSMSSender getMacroKiosk(){
		DatabaseFacade db = new DatabaseFacade();
		
		try{	
			SettingHelper setting = new SettingHelper(db);
			String username = setting.getSmsUserId();
			String password = setting.getSmsUserPassword();
			
			MacroKioskSMSSender sender = new MacroKioskSMSSender(username, password);
			return sender;
			
		}finally{
			db.close();
		}
	}
	
	private IsentricSMSSender getIsentric(SMSType type){
		DatabaseFacade db = new DatabaseFacade();
		
		try{	
			SettingHelper setting = new SettingHelper(db);
			String username = null;
			
			if(SMSType.BILL == type) {
				username = setting.getSmsUserIdIsentricTypeBIll();
				
				if(username == null) {
					logger.error("error isentric billing type sms user not found");
					username = setting.getSmsUserIdIsentric();
				}
			}else {
				username = setting.getSmsUserIdIsentric();
			}
			
			IsentricSMSSender sender = new IsentricSMSSender(username);
			return sender;
			
		}finally{
			db.close();
		}
	}
}
