package com.seb.middleware.sms;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.seb.middleware.core.PathManager;
import com.seb.middleware.formatter.PhoneNumberFormatter;

public class SMSManager extends SMSBase {

	public void sendOtp(String msisdn, String otp, int expirationMinutes) throws IOException {
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("otp", otp);
		properties.put("minutes", String.valueOf(expirationMinutes));
		
		String template = PathManager.getSmsTemplatePath("tpl_one_time_password.txt");
		String content = processTemplateFile(template, properties);
		
		sendMessage(msisdn, content, SMSType.OTHER);
	}
	
	public void sendMobileVerificationCode(String msisdn, String verificationCode, int expirationMinutes) throws IOException {
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("verification_code", verificationCode);
		properties.put("minutes", String.valueOf(expirationMinutes));
		
		String template = PathManager.getSmsTemplatePath("tpl_mobile_verification.txt");
		String content = processTemplateFile(template, properties);
		
		sendMessage(msisdn, content, SMSType.OTHER);
	}
	
	public void sendForgotPassword(String msisdn, String password) throws IOException  {
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("password", password);
		
		String template = PathManager.getSmsTemplatePath("tpl_forgot_password.txt");
		String content = processTemplateFile(template, properties);
		
		sendMessage(msisdn, content, SMSType.OTHER);
	}
	
	public void sendBillReady(String msisdn) throws IOException  {
		Map<String, String> properties = new HashMap<String, String>();
		
		String template = PathManager.getSmsTemplatePath("tpl_bill_readiness.txt");
		String content = processTemplateFile(template, properties);
		
		sendMessage(msisdn, content, SMSType.BILL);
	}
	
	private boolean sendMessage(String msisdn, String text, SMSType type) throws IOException{
		
		String formattedMsisdn = new PhoneNumberFormatter().formatString(msisdn);
		
		logger.info("Sending SMS to " + formattedMsisdn + ".");
		
		if(testingEnvironment){
			logger.info("Content: " + text);	
		}
		
		if(testingEnvironment){
			try{
				send(formattedMsisdn, text, type);
				logger.info("SMS sent successfully.");
			}catch(Exception e){
				logger.error("error sending sms to " + formattedMsisdn, e);
			}
			
			return true;
		}else{
			boolean sendStatus = send(formattedMsisdn, text, type);
			logger.info("SMS sent successfully.");
			return sendStatus;
		}
	}
}
