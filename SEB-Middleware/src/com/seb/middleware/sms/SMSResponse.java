package com.seb.middleware.sms;


public class SMSResponse {

	private String messageId;
	
	private String msisdn;

	private String status;
	
	public SMSResponse(String raw){
		if(raw != null && !raw.isEmpty()){
			String [] split = raw.split(",");
			if(split.length >= 3){
				msisdn = split[0].trim();
				messageId = split[1].trim();
				status = split[2].trim();
			}
		}
	}
	
	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SMSResponse [messageId=" + messageId + ", msisdn=" + msisdn + ", status=" + status + "]";
	}
}
