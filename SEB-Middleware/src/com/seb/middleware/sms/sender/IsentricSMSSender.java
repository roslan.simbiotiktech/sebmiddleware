package com.seb.middleware.sms.sender;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;

import com.seb.middleware.utility.StringUtil;

public class IsentricSMSSender extends SMSSender {

	public IsentricSMSSender(String username) {
		super(username, null);
	}

	@Override
	public boolean send(String msisdn, String text) throws IOException {
		String smsUrl = buildUrl(msisdn, text);
		URL endpoint = new URL(smsUrl);

		BufferedReader in = null;
		
		logger.debug(smsUrl);
		
		try{
			URLConnection connection = endpoint.openConnection();
			connection.setConnectTimeout(15000);
			connection.setReadTimeout(15000);
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Content-Type", "application/json");
			
			in = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));

			StringBuilder responseLines = new StringBuilder();
			String inputLine;
			
			while ((inputLine = in.readLine()) != null) {
				responseLines.append(inputLine);
			}
			
			String response = responseLines.toString().trim();
			logger.debug(response);

			if(!StringUtil.isEmpty(response) && response.toLowerCase().indexOf("returncode = 0") != -1){
				return true;
			}else{
				logger.error("Error sending SMS");
				return false;
			}
		}finally{
			if(in != null)
				in.close();
		}
	}

	private String buildUrl(String msisdn, String text) throws UnsupportedEncodingException{
		StringBuilder sb = new StringBuilder();
		sb.append("http://203.223.130.115/ExtMTPush/extmtpush?");
		sb.append("shortcode=39398&");
		sb.append("custid=").append(username).append("&");
		sb.append("rmsisdn=").append(msisdn).append("&");
		sb.append("smsisdn=62003&");
		sb.append("mtid=").append(System.currentTimeMillis()).append("&");
		sb.append("mtprice=000&");
		sb.append("productCode=&");
		sb.append("productType=4&");
		sb.append("keyword=&");
		sb.append("dataEncoding=0&");
		sb.append("dataStr=").append(encode(text)).append("&");
		sb.append("dataUrl=&");
		sb.append("dnRep=0&");
		sb.append("groupTag=10");
		return sb.toString();
	}
}
