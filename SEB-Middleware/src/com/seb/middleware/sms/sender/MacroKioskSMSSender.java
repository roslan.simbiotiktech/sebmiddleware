package com.seb.middleware.sms.sender;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;

import com.seb.middleware.sms.SMSResponse;

public class MacroKioskSMSSender extends SMSSender {

	public MacroKioskSMSSender(String username, String password) {
		super(username, password);
	}

	@Override
	public boolean send(String msisdn, String text) throws IOException {
		String smsUrl = buildUrl(msisdn, text);
		URL endpoint = new URL(smsUrl);

		BufferedReader in = null;
		
		try{
			URLConnection connection = endpoint.openConnection();
			connection.setConnectTimeout(15000);
			connection.setReadTimeout(15000);
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Content-Type", "application/json");
			
			in = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));

			StringBuilder responseLines = new StringBuilder();
			String inputLine;
			
			while ((inputLine = in.readLine()) != null) {
				responseLines.append(inputLine);
			}
			
			String response = responseLines.toString().trim();
			logger.debug(response);
			
			SMSResponse smsResp = new SMSResponse(response);
			logger.debug(smsResp);
			
			if(smsResp.getStatus() != null && smsResp.getStatus().equals("200")){
				return true;
			}else{
				logger.error("Error sending SMS");
				return false;
			}
		}finally{
			if(in != null)
				in.close();
		}
	}

	private String buildUrl(String msisdn, String text) throws UnsupportedEncodingException{
		StringBuilder sb = new StringBuilder();
		sb.append("http://www.etracker.cc/bulksms/mesapi.aspx?");
		sb.append("user=").append(username).append("&");
		sb.append("pass=").append(password).append("&");
		sb.append("to=").append(msisdn).append("&");
		sb.append("text=").append(encode(text)).append("&");
		sb.append("from=").append(encode("SESCO")).append("&");
		sb.append("type=0&");
		sb.append("servid=MES01");
		return sb.toString();
	}
}
