package com.seb.middleware.sms.sender;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class SMSSender {

	protected static final Logger logger = LogManager.getLogger(SMSSender.class);
	
	protected String username;
	
	protected String password;
	
	public SMSSender(String username, String password){
		this.username = username;
		this.password = password;
	}
	
	public abstract boolean send(String msisdn, String text) throws IOException;
	
	protected String encode(String string) throws UnsupportedEncodingException{
		return URLEncoder.encode(string, "UTF-8");
	}
}
