package com.seb.middleware.utility;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sap.conn.jco.util.Codecs.Hex;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.helper.SettingHelper;

public class CryptoUtil {

	private static final Logger logger = LogManager.getLogger(CryptoUtil.class);
	private static final String ALGORITHM = "AES";
	private static final String CIPHER = "AES/ECB/PKCS5Padding";
	private static final String ENCODING = "UTF-8";
	private static final int KEY_LENGTH = 128;
	private static String PASSWORD = null;
	private static final int SSHA_SALT_LENGTH = 8;
	
	static {
		DatabaseFacade db = new DatabaseFacade();
		try{
			SettingHelper setting = new SettingHelper(db);
			PASSWORD = setting.getSecret();
		}finally{
			db.close();
		}
	}

	public static String sha512HexadecimalLowercase(String plain) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		MessageDigest digest = MessageDigest.getInstance("SHA-512");
		byte[] hash = digest.digest(plain.getBytes("UTF-8"));
		String hex = Hex.encode(hash);
		return hex.toLowerCase();
	}
	
	public static String sha256HexadecimalLowercase(String plain) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(plain.getBytes("UTF-8"));
		String hex = Hex.encode(hash);
		return hex.toLowerCase();
	}
	
	public static String ssha256HexadecimalLowercase(String plain) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		String hexSalt = getHexSalt(SSHA_SALT_LENGTH); //16 in length
		String saltedPlain = hexSalt + plain;
		String sha = sha256HexadecimalLowercase(saltedPlain);
		String ssha = hexSalt + sha;
		return ssha;
	}
	
	public static boolean ssha256Equals(String ssha, String plain){
		
		try{
			int index = SSHA_SALT_LENGTH * 2;
			String salt = ssha.substring(0, index);
			String sha = ssha.substring(index);
			
			String inputSha = sha256HexadecimalLowercase(salt + plain);
			return inputSha.equals(sha);
		}catch(Exception ex){
			logger.error("error comparing ssha", ex);
			return false;
		}
	}
	
	public static String encrypt(String plain) throws IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, InvalidAlgorithmParameterException, NoSuchPaddingException, InvalidParameterSpecException{
		return encrypt(plain, PASSWORD);
	}
	
	public static String encrypt(String plain, String password) throws IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, InvalidAlgorithmParameterException, NoSuchPaddingException, InvalidParameterSpecException{

		SecretKey secret = new SecretKeySpec(getSecret(password), ALGORITHM);

		Cipher cipher = Cipher.getInstance(CIPHER);
		cipher.init(Cipher.ENCRYPT_MODE, secret);

		byte[] cipherText = cipher.doFinal(plain.getBytes(ENCODING));
	
		byte[] cipher64 = Base64.encodeBase64(cipherText);
	    return new String(cipher64);
	}

	public static String decrypt(String encrypted) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException{
		return decrypt(encrypted, PASSWORD);
	}
	
	public static String decrypt(String encrypted, String password) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException{

		byte[] encryptedCipher = Base64.decodeBase64(encrypted);
	     
		SecretKey secret = new SecretKeySpec(getSecret(password), ALGORITHM);

		Cipher cipher = Cipher.getInstance(CIPHER);

		cipher.init(Cipher.DECRYPT_MODE, secret);
		String plain = new String(cipher.doFinal(encryptedCipher), ENCODING);
	    return plain;
	}
	
	private static byte[] getSecret(String password) throws UnsupportedEncodingException, NoSuchAlgorithmException{
		MessageDigest sha = MessageDigest.getInstance("SHA-1");
		byte [] secret = sha.digest(password.getBytes(ENCODING));
		secret = Arrays.copyOf(secret, KEY_LENGTH / Byte.SIZE);
		return secret;
	}
	
	public static byte[] decodeBase64(String base64){
		byte[] decoded= Base64.decodeBase64(base64.getBytes());
		return decoded;
	}
	
	public static String encodeBase64(byte[] bytes){
		byte[] encoded = Base64.encodeBase64(bytes);
		return new String(encoded);
	}
	
	/*
	 * Return string's length = input length * 2 
	 */
	public static String getHexSalt(int length) {
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[length];
		random.nextBytes(bytes);
		return Hex.encode(bytes).toLowerCase();
	}
	
	public static void main(String [] args) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		
		/*String ssha1 = ssha256HexadecimalLowercase("123456");
		String ssha2 = ssha256HexadecimalLowercase("123456");
		
		System.out.println(ssha1);
		System.out.println(ssha2);
		
		System.out.println(ssha256Equals(ssha1, "123456"));
		System.out.println(ssha256Equals(ssha1, "123457"));
		System.out.println(ssha256Equals(ssha2, "123456"));
		System.out.println(ssha256Equals(ssha2, "123457"));*/
		
		System.out.println(sha512HexadecimalLowercase("gstcE12V110000000000000000011000000620140414_17465450.00413718xxxxxx59181120xx3"));
	}
}
