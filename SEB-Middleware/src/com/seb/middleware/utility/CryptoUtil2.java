package com.seb.middleware.utility;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import com.sap.conn.jco.util.Codecs.Hex;

public class CryptoUtil2 {

	public static byte[] decodeBase64(String base64){
		byte[] decoded= Base64.decodeBase64(base64.getBytes());
		return decoded;
	}
	
	public static String encodeBase64(byte[] bytes){
		byte[] encoded = Base64.encodeBase64(bytes);
		return new String(encoded);
	}
	
	private static final String KEY_FACTORY = "PBKDF2WithHmacSHA1";
	private static final String ALGORITHM = "AES";
	private static final String CIPHER = "AES/CBC/PKCS5Padding";
	private static final String ENCODING = "UTF-8";
	
	private static final char [] PASSWORD = new char [] {'s', 'r', 'w', 'k', 'n', 'g', 'y'};
	private static final int ITERATION_COUNT = 16;
	private static final int KEY_LENGTH = 128;
	
	public static String encrypt(String plain) throws IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, InvalidAlgorithmParameterException, NoSuchPaddingException, InvalidParameterSpecException{
		return encrypt(plain, PASSWORD);
	}
	
	public static String encrypt(String plain, char [] password) throws IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, InvalidAlgorithmParameterException, NoSuchPaddingException, InvalidParameterSpecException{

		byte[] salt = getSalt();
		
		SecretKeyFactory factory = SecretKeyFactory.getInstance(KEY_FACTORY);

		KeySpec spec = new PBEKeySpec(password, salt, ITERATION_COUNT, KEY_LENGTH);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKey secret = new SecretKeySpec(tmp.getEncoded(), ALGORITHM);

		Cipher cipher = Cipher.getInstance(CIPHER);
		cipher.init(Cipher.ENCRYPT_MODE, secret, new IvParameterSpec(getIV()));

		byte[] cipherText = cipher.doFinal(plain.getBytes(ENCODING));
	
		byte[] cipher64 = Base64.encodeBase64(cipherText);
	    return new String(cipher64);
	}

	public static String decrypt(String encrypted) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException{
		return decrypt(encrypted, PASSWORD);
	}
	
	public static String decrypt(String encrypted, char [] password) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException{

		byte[] encryptedCipher = Base64.decodeBase64(encrypted);
	     
	    SecretKeyFactory factory = SecretKeyFactory.getInstance(KEY_FACTORY);

		KeySpec spec = new PBEKeySpec(password, getSalt(), ITERATION_COUNT, KEY_LENGTH);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKey secret = new SecretKeySpec(tmp.getEncoded(), ALGORITHM);

		Cipher cipher = Cipher.getInstance(CIPHER);

		cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(getIV()));
		String plain = new String(cipher.doFinal(encryptedCipher), ENCODING);
	    return plain;
	}
	
	private static byte[] getSalt(){
		byte[] salt = new byte[]{0,0,0,0,0,0,0,0};
		return salt;
	}
	
	private static byte[] getIV(){
		byte[] iv = new byte[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		return iv;
	}
	
	
	public static void main(String [] args) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidAlgorithmParameterException, NoSuchPaddingException, InvalidParameterSpecException{
		
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest("Superduke990R".getBytes("UTF-8"));
		String hex = Hex.encode(hash);
		System.out.println(hex);
	}
}
