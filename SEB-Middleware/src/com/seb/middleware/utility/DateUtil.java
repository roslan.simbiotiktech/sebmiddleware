package com.seb.middleware.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	public static Date[] getEffectiveBetweenClause(Date date){
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		Date [] arr = new Date[2];
		arr[0] = calendar.getTime();
 		
		calendar.add(Calendar.DATE, 1);
		calendar.add(Calendar.MILLISECOND, -1);
		arr[1] = calendar.getTime();
		
		return arr;
	}
	
	public static Date[] getEffectiveBetweenClause(String yyyyMM) throws ParseException{
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		Date date = sdf.parse(yyyyMM);
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DATE, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		Date [] arr = new Date[2];
		arr[0] = calendar.getTime();
 		
		calendar.add(Calendar.MONTH, 1);
		calendar.add(Calendar.MILLISECOND, -1);
		arr[1] = calendar.getTime();
		
		return arr;
	}
	
	
	public static void main(String [] args) throws ParseException{
		Date [] dates = getEffectiveBetweenClause("2015002");
		System.out.println(dates[0]);
		System.out.println(dates[1]);
	}
}
