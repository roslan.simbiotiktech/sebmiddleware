package com.seb.middleware.utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class PasswordUtil {

	private static char [] UPPERCASE = 
		{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
	private static char [] LOWERCASE = 
		{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
	private static char [] NUMBER = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
	
	public static String generatePassword() {
		
		ArrayList<Character> chars = new ArrayList<Character>();
		chars.add(getRandomChar(UPPERCASE));
		chars.add(getRandomChar(LOWERCASE));
		chars.add(getRandomChar(LOWERCASE));
		chars.add(getRandomChar(LOWERCASE));
		chars.add(getRandomChar(NUMBER));
		chars.add(getRandomChar(NUMBER));
		chars.add(getRandomChar(NUMBER));
		chars.add(getRandomChar(NUMBER));
		
		Collections.shuffle(chars);
		StringBuilder sb = new StringBuilder();
		
		for(Character c : chars){
			sb.append(c);
		}
		
		return sb.toString();
	}
	
	private static char getRandomChar(char [] chars){
		Random random = new Random();
		int i = random.nextInt(chars.length);
		return chars[i];
	}
	
}
