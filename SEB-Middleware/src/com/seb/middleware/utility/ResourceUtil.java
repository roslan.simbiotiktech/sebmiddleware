package com.seb.middleware.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import com.anteater.library.json.exception.FieldError;
import com.anteater.library.json.exception.FieldErrorDetail;
import com.anteater.library.json.exception.FieldErrorType;
import com.anteater.library.json.exception.FieldException;

public class ResourceUtil {

	private static final String DEFAULT_LOCALE = "en";
	private static HashMap<String, ResourceBundle> RESOURCES = new HashMap<String, ResourceBundle>();
	
	
	static {
		Locale MS_LOCALE = new Locale("ms", "MALAYSIA");
		Locale ZH_LOCALE = new Locale("zh", "MALAYSIA");
		Locale[] supportedLocales = {
			    Locale.ENGLISH,
			    MS_LOCALE,
			    ZH_LOCALE
			};
		
		for(Locale locale : supportedLocales){
			ResourceBundle res = ResourceBundle.getBundle("Resource", locale);
			RESOURCES.put(locale.getLanguage(), res);	
		}
	}
	
	private static String getLocale(String locale){
		if(locale == null || locale.isEmpty()) return DEFAULT_LOCALE;
		if(!RESOURCES.containsKey(locale)){
			return DEFAULT_LOCALE;
		}else{
			return locale;
		}
	}
	
	private static final String FIELD_PREFIX = "field.";
	private static final String ERROR_PREFIX = "validation.";
	
	public static ArrayList<String> get(FieldException ex, String locale){

		ArrayList<String> errorMessages = new ArrayList<String>();
		
		for(FieldError err : ex.getErrors()){
			for(FieldErrorDetail errDetail  : err.getErrorDetails()){
				StringBuilder errorBody = new StringBuilder(ERROR_PREFIX);
				
				if(errDetail.getErrorType() != FieldErrorType.INVALID_TYPE_CUSTOM){
					errorBody.append(errDetail.getErrorType().toString().toLowerCase());
				}else{
					errorBody.append(errDetail.getCustomErrorKey());
				}
				
				String fieldKey = null;
				if(errDetail.getLocalizationKey() == null || errDetail.getLocalizationKey().isEmpty()){
					fieldKey = FIELD_PREFIX + err.getName().toLowerCase();
				}else{
					fieldKey = FIELD_PREFIX + errDetail.getLocalizationKey().toLowerCase();
				}
				
				String localizedFieldName = get(fieldKey, locale);
				
				if(errDetail.getErrorType() == FieldErrorType.MANDATORY){
					errorMessages.add(get(errorBody.toString(), locale, localizedFieldName));
				}else if(errDetail.getErrorType() == FieldErrorType.INVALID_NUMBER_UNDER || 
						errDetail.getErrorType() == FieldErrorType.INVALID_NUMBER_EXCEED ||
						errDetail.getErrorType() == FieldErrorType.INVALID_INPUT_LENGTH_NOT_EXACT ||
						errDetail.getErrorType() == FieldErrorType.INVALID_INPUT_LENGTH_UNDER ||
						errDetail.getErrorType() == FieldErrorType.INVALID_INPUT_LENGTH_EXCEED){
					errorMessages.add(get(errorBody.toString(), locale, localizedFieldName, err.getValue(), errDetail.getReferenceValue()));
				}else{
					errorMessages.add(get(errorBody.toString(), locale, localizedFieldName, err.getValue(), errDetail.getReferenceValue()));
				}
			}
		}
		
		return errorMessages;
	}
	
	public static String get(String key, String locale){
		return get(key, locale, (String[])null);
	}

	public static String get(String key, String locale, String ... params){
		
		try{
			ResourceBundle res = RESOURCES.get(getLocale(locale));
			String resource = res.getString(key);
			
			if(params != null && params.length > 0){
				StringBuilder sb = new StringBuilder(resource);
				
				for(int i = 0; i < params.length; i++){
					String param = params[i];
					if(!StringUtil.isEmpty(param)){
						replaceAll(sb, "{" + i + "}",param);	
					}
				}
				
				return sb.toString();
			}else{
				return resource;
			}
		}catch(MissingResourceException e){
			return key;
		}
	}
	
	private static void replaceAll(StringBuilder builder, String from, String to) {
	    int index = builder.indexOf(from);
	    
	    while (index != -1){
	        builder.replace(index, index + from.length(), to);
	        index += to.length(); // Move to the end of the replacement
	        index = builder.indexOf(from, index);
	    }
	}
}
