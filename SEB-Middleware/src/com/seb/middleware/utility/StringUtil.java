package com.seb.middleware.utility;


public class StringUtil {

	public static String getString(String var, int maxLength) {
		if(var == null || var.isEmpty()) {
			return var;
		}else {
			if(var.length() <= maxLength) {
				return var;
			}else {
				return var.substring(0, maxLength);
			}
		}
	}
	
	public static String pad(int total, char padding){
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < total; i++){
			sb.append(padding);
		}
		return sb.toString();
	}
	
	public static boolean isEmpty(String value){
		return value == null || value.length() == 0;
	}
	
	public static boolean isDigit(String value){
		
		if(isEmpty(value)) return false;
		
		for(char c : value.toCharArray()){
			if(!Character.isDigit(c)){
				return false;
			}
		}
		
		return true;
	}
	
	public static long getBytesSizeFromBase64(String base64){
		if(isEmpty(base64)) return 0;
		
		long length = (long) (Math.ceil(base64.length() / 3) * 4);
		return length;
	}
	
	public static String getHumanReadableFileSize(long numberOfBytes){
		
		int divider = 1;
		String unit = "bytes";
		
		if(numberOfBytes < 1024){
			divider = 1;
			unit = "Bytes";
		}else if(numberOfBytes < (1048576)){
			divider = 1024;
			unit = "KB";
		}else if(numberOfBytes < (1073741824 )){
			divider = 1048576;
			unit = "MB";
		}else{
			divider = 1073741824;
			unit = "GB";
		}
		double d = numberOfBytes / divider;
		return String.format("%.2f%s", d, unit);
	}
	
	public static String getHumanReadableFileSizeFromBase64(String base64){
		return getHumanReadableFileSize(getBytesSizeFromBase64(base64));
	}
	
	/**
	 * ABC000001 returns 000001
	 * 123B9999 returns 9999
	 */
	public static String getNumericFromRight(String val){
		
		StringBuilder sb = new StringBuilder();
		
		char[] chars = val.toCharArray();
		for(int i = chars.length-1; i >= 0; i--){
			char c = chars[i];
			
			if(Character.isDigit(c)){
				sb.insert(0, c);
			}else{
				break;
			}
		}
		
		return sb.toString();
	}
}
