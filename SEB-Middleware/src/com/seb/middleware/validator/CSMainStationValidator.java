package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;
import com.seb.middleware.constant.CSMainStation;

public class CSMainStationValidator extends CustomValidator{

	private static final String ERROR_KEY = "invalid_cs_main_station";

	@Override
	public boolean validate(String value) {
		if(isEmpty(value)) return false;
		
		try{
			CSMainStation.valueOf(value);
			return true;
		}catch(IllegalArgumentException e){
			return false;
		}
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}
}
