package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;
import com.seb.middleware.constant.CalculatorType;

public class CalculatorTypeValidator extends CustomValidator{

	private static final String ERROR_KEY = "invalid_calc_type";

	@Override
	public boolean validate(String value) {
		if(isEmpty(value)) return false;
		
		try{
			CalculatorType.valueOf(value);
			return true;
		}catch(IllegalArgumentException e){
			return false;
		}
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}

}
