package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;
import com.seb.middleware.constant.PreferredCommunicationMethod;

public class CommunicationMethodValidator extends CustomValidator{

	private static final String ERROR_KEY = "invalid_comm_method";

	@Override
	public boolean validate(String value) {
		if(isEmpty(value)) return false;
		
		try{
			PreferredCommunicationMethod.valueOf(value);
			return true;
		}catch(IllegalArgumentException e){
			return false;
		}
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}

}
