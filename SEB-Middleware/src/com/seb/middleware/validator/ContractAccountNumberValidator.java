package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;

public class ContractAccountNumberValidator extends CustomValidator{

	private static final String ERROR_KEY = "invalid_contract_acc_no";
	
	private int[] MATRIX = new int[]{ 6, 5, 4, 3, 2, 7, 6, 5, 4, 3, 2 }; 
	
	@Override
	public boolean validate(String value) {
		
		if(isEmpty(value)) return false;
		if(value.length() != 12) return false;
		if(!isDigit(value)) return false;
		
		int sum = 0;
		for(int i = 0; i < value.length() - 1; i++){
			int multiply = Character.getNumericValue(value.charAt(i));
			int multiplicator = MATRIX[i];
			int multiplication = multiply * multiplicator;
			sum += multiplication;
		}
		
		int dividend = sum;
		int divisor = 11;
		int remainder = (dividend % divisor) % 10;
		
		int checkDigit = Character.getNumericValue(value.charAt(value.length() - 1));
		
		return remainder == checkDigit;
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}

}
