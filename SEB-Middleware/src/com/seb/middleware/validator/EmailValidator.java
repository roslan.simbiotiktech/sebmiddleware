package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;

public class EmailValidator extends CustomValidator{

	protected static final String ERROR_KEY = "invalid_email";
	
	@Override
	public boolean validate(String value) {
		if(isEmpty(value)) return false;
		
		boolean valid = org.apache.commons.validator.routines.EmailValidator.getInstance().isValid(value);
		return valid;
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}

}
