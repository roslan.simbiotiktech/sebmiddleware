package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.helper.PaymentHelper;

public class FPXBankIdValidator extends CustomValidator {

	private static final String ERROR_KEY = "invalid_fpx_bank";

	@Override
	public boolean validate(String value) {

		DatabaseFacade db = new DatabaseFacade();
		try {
			PaymentHelper pHelper = new PaymentHelper(db);
			if (pHelper.isFPXBankExist(value)) {
				return true;
			} else {
				return false;
			}
		} finally {
			db.close();
		}
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}
}
