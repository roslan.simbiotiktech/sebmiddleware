package com.seb.middleware.validator;


public class HomeNumberValidator extends PhoneNumberValidator {

	private static final String ERROR_KEY = "invalid_home_number";

	@Override
	public boolean validate(String value) {
		if(isEmpty(value)) return false;
		if(value.length() < 9 || value.length() > 12) return false;
		
		char zero = '0';
		char nine = '9';
		
		char [] values = value.toCharArray();
		for(int i = 0; i < value.length(); i++){
			char c = values[i];
			if((c >= zero && c <= nine) || (c == '+' && i == 0)){
				// only allowed first index to have +
				// or number
			}else{
				return false;
			}
		}
		
		return true;
	}
	
	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}

}
