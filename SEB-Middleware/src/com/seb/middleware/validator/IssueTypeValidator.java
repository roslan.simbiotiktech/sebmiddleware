package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;
import com.seb.middleware.constant.ReportIssueType;

public class IssueTypeValidator extends CustomValidator{

	private static final String ERROR_KEY = "invalid_issue_type";

	@Override
	public boolean validate(String value) {
		if(isEmpty(value)) return true;
		
		try{
			ReportIssueType.valueOf(value);
			return true;
		}catch(IllegalArgumentException e){
			return false;
		}
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}
}
