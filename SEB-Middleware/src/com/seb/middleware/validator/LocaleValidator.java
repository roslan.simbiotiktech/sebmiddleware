package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;
import com.seb.middleware.constant.LocaleType;

public class LocaleValidator extends CustomValidator{

	private static final String ERROR_KEY = "invalid_locale";

	@Override
	public boolean validate(String value) {
		if(isEmpty(value)) return true;
		
		try{
			LocaleType.valueOf(value);
			return true;
		}catch(IllegalArgumentException e){
			return false;
		}
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}

}
