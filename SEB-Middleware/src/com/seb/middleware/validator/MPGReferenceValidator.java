package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;
import com.seb.middleware.payment.spg.SPGTransactionType;

public class MPGReferenceValidator extends CustomValidator {

	private static final String ERROR_KEY = "invalid_payment_reference";

	@Override
	public boolean validate(String value) {

		if(!value.startsWith(SPGTransactionType.MAYBANK.getPrefix())){
			return false;
		}else{
			return true;
		}
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}
}
