package com.seb.middleware.validator;


public class OfficeNumberValidator extends PhoneNumberValidator {

	private static final String ERROR_KEY = "invalid_office_number";

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}

}
