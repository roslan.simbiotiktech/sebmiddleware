package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;
import com.seb.middleware.constant.OtpType;

public class OtpTypeValidator extends CustomValidator{

	private static final String ERROR_KEY = "invalid_otp_type";

	@Override
	public boolean validate(String value) {
		if(isEmpty(value)) return false;
		
		try{
			OtpType.valueOf(value);
			return true;
		}catch(IllegalArgumentException e){
			return false;
		}
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}
}
