package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;

public class PasswordValidator extends CustomValidator{

	private static final String ERROR_KEY_INVALID = "invalid_password";
	private static final String ERROR_KEY_INVALID_EXCEED = "invalid_password_exceed_length";
	
	private static char [] ACCEPT_SPEC_CHARACTERS = new char[]{'!', '(', ')', '-', '.', '_', '~', '@', '#'};
	
	private String errorKey;
	
	@Override
	public boolean validate(String value) {
		
		errorKey = ERROR_KEY_INVALID;
		
		if(isEmpty(value)) return false;
		if(value.length() < 8) return false;
		if(value.length() > 64) {
			errorKey = ERROR_KEY_INVALID_EXCEED;
			return false;
		}
		
		boolean hasNumeric = false;
		boolean hasAlphaCapital = false;
		boolean hasAlphaNonCapital = false;
		
		char a = 'a';
		char z = 'z';
		char A = 'A';
		char Z = 'Z';
		char zero = '0';
		char nine = '9';
		
		for(char c : value.toCharArray()){
			if(c >= a && c <= z) hasAlphaNonCapital = true;
			else if(c >= A && c <= Z) hasAlphaCapital = true;
			else if(c >= zero && c <= nine) hasNumeric = true;
			else {
				if(!isAllowedSpecialCharacters(c)){
					return false;
				}
			}
		}
		
		return hasNumeric && hasAlphaCapital && hasAlphaNonCapital;
	}

	private boolean isAllowedSpecialCharacters(char c){
		for(char cc : ACCEPT_SPEC_CHARACTERS){
			if(cc == c){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String getCustomErrorKey() {
		return errorKey;
	}
}
