package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;

public class PhoneNumberValidator extends CustomValidator{

	private static final String ERROR_KEY = "invalid_phone_number";

	@Override
	public boolean validate(String value) {
		if(isEmpty(value)) return false;
		if(value.length() < 10 || value.length() > 15) return false;
		
		char zero = '0';
		char nine = '9';
		
		char [] values = value.toCharArray();
		for(int i = 0; i < value.length(); i++){
			char c = values[i];
			if((c >= zero && c <= nine) || (c == '+' && i == 0)){
				// only allowed first index to have +
				// or number
			}else{
				return false;
			}
		}
		
		return true;
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}

}
