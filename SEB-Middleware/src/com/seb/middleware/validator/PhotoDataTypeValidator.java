package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;

public class PhotoDataTypeValidator extends CustomValidator{

	private static final String ERROR_KEY = "invalid_photo_type";
	private static final String [] ALLOWED_TYPE = 
		{"image/jpg", "image/png"};
	
	@Override
	public boolean validate(String value) {
		if(isEmpty(value)) return false;
		
		for(String type : ALLOWED_TYPE){
			if(value.equals(type)){
				return true;
			}
		}
		
		return false;
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}
}
