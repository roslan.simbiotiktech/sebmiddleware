package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.helper.SettingHelper;
import com.seb.middleware.utility.StringUtil;

public class PhotoDataValidator extends CustomValidator{

	private static final String ERROR_KEY = "invalid_photo_data";
	private String referenceValue;

	@Override
	public boolean validate(String value) {
		if(isEmpty(value)) return true;
		
		DatabaseFacade db = new DatabaseFacade();
		
		try{
			SettingHelper setting = new SettingHelper(db);
			long maxAllowed = setting.getImageMaxAllowedBytes();
			long size = StringUtil.getBytesSizeFromBase64(value);
			if(size > maxAllowed){
				StringBuilder sb = new StringBuilder();
				sb.append("Size: ").append(StringUtil.getHumanReadableFileSize(size));
				sb.append(" / ").append("Max Allowed: ").append(StringUtil.getHumanReadableFileSize(maxAllowed));
				referenceValue = sb.toString();
				return false;
			}else{
				return true;
			}	
		}finally{
			db.close();
		}
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}
	
	@Override
	public String getReferenceValue() {
		return referenceValue;
	}
}
