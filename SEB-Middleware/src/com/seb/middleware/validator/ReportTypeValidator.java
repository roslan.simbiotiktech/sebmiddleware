package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;
import com.seb.middleware.constant.ReportType;

public class ReportTypeValidator extends CustomValidator{

	private static final String ERROR_KEY = "invalid_report_type";

	@Override
	public boolean validate(String value) {
		if(isEmpty(value)) return false;
		
		try{
			ReportType.valueOf(value);
			return true;
		}catch(IllegalArgumentException e){
			return false;
		}
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}
}
