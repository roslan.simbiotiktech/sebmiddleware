package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;
import com.seb.middleware.constant.SFDCReportStatus;

public class SFDCReportStatusValidator extends CustomValidator{

	private static final String ERROR_KEY = "invalid_report_status";

	@Override
	public boolean validate(String value) {
		if(isEmpty(value)) return false;
		
		try{
			SFDCReportStatus.valueOf(value);
			return true;
		}catch(IllegalArgumentException e){
			return false;
		}
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}
}
