package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;
import com.seb.middleware.api.services.response.content.Station;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.helper.ContentHelper;

public class StationValidator extends CustomValidator{

	private static final String ERROR_KEY = "invalid_station";

	@Override
	public boolean validate(String value) {
		if(isEmpty(value)) return false;
		
		DatabaseFacade db = new DatabaseFacade();
		try{
			ContentHelper helper = new ContentHelper(db);
			Station[] stations = helper.getStations();
			for(Station station : stations){
				if(value.equals(station.getKey())){
					return true;
				}
			}
			
			return false;	
		}finally{
			db.close();
		}
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}
}
