package com.seb.middleware.validator;

import com.seb.middleware.constant.UserStatus;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;


public class UserEmailWithStatusValidator extends EmailValidator{

	private static final String ERROR_KEY_NOT_FOUND = "invalid_user";
	private static final String ERROR_KEY_STATUS_PENDING = "invalid_user_status_pending";
	private static final String ERROR_KEY_STATUS_SUSPENDED = "invalid_user_status_suspended";
	private static final String ERROR_KEY_STATUS_DEACTIVATED = "invalid_user_status_deactivated";
	
	private String errorKey;

	@Override
	public boolean validate(String value) {
		
		if(super.validate(value)){
			DatabaseFacade db = new DatabaseFacade();
			try{
				MemberHelper memberHelper = new MemberHelper(db);
				
				User user = memberHelper.getUser(value.trim().toLowerCase());
				if(user != null){
					if(user.getStatus() == UserStatus.PENDING){
						errorKey = ERROR_KEY_STATUS_PENDING;
						return false;
					}else if(user.getStatus() == UserStatus.SUSPENDED){
						errorKey = ERROR_KEY_STATUS_SUSPENDED;
						return false;
					}else if(user.getStatus() == UserStatus.DEACTIVATED){
						errorKey = ERROR_KEY_STATUS_DEACTIVATED;
						return false;
					}else{
						return true;
					}	
				}else{
					errorKey = ERROR_KEY_NOT_FOUND;
					return false;
				}
			}finally{
				db.close();
			}
			
		}else{
			errorKey = ERROR_KEY;
			return false;
		}
	}

	@Override
	public String getCustomErrorKey() {
		return errorKey;
	}

}
