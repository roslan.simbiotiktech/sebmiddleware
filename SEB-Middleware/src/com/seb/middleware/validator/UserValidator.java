package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;
import com.seb.middleware.jpa.DatabaseFacade;
import com.seb.middleware.jpa.dao.User;
import com.seb.middleware.jpa.helper.MemberHelper;


public class UserValidator extends CustomValidator{

	private static final String ERROR_KEY_NOT_FOUND = "invalid_user";
	
	private String errorKey;

	@Override
	public boolean validate(String value) {
		
		DatabaseFacade db = new DatabaseFacade();
		try{
			MemberHelper memberHelper = new MemberHelper(db);
			
			User user = memberHelper.getUser(value.trim().toLowerCase());
			if(user != null){
				return true;
			}else{
				errorKey = ERROR_KEY_NOT_FOUND;
				return false;
			}
		}finally{
			db.close();
		}
	}

	@Override
	public String getCustomErrorKey() {
		return errorKey;
	}

}
