package com.seb.middleware.validator;

import com.anteater.library.json.CustomValidator;

public class VerificationCodeValidator extends CustomValidator{

	private static final String ERROR_KEY = "invalid_verification_code";

	@Override
	public boolean validate(String value) {
		if(isEmpty(value)) return false;
		
		return isDigit(value);
	}

	@Override
	public String getCustomErrorKey() {
		return ERROR_KEY;
	}
}
