connect to seb user db2admin using db2admin;

CREATE TABLE seb.agency (
	agency_name CHAR(7) NOT NULL, 
	PRIMARY KEY(agency_name)
);

CREATE TABLE seb.billing (
	billing_id BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1), 
	agency_name VARCHAR(7) NOT NULL, 
	process_date VARCHAR(8) NOT NULL, 
	PRIMARY KEY(billing_id)
);

CREATE TABLE seb.billing_transaction (                              
	billing_id BIGINT NOT NULL,                                     
	invoice_number VARCHAR(12) NOT NULL,                            
	contract_account_number VARCHAR(12) NOT NULL,                   
	business_partner_number VARCHAR(10) NOT NULL,                   
	invoice_date VARCHAR(8) NOT NULL,                               
	due_date VARCHAR(8) NOT NULL,                                   
	current_amount DECIMAL(13,2) NOT NULL,                          
	current_total_amount DECIMAL(13,2) NOT NULL,                    
	pdf_print_document_number VARCHAR(70) NOT NULL,                 
	PRIMARY KEY(billing_id,invoice_number)                          
);

CREATE TABLE seb.contract (
	contract_account_number VARCHAR(12) NOT NULL, 
	contract_account_name VARCHAR(50) NOT NULL, 
	agency_name VARCHAR(7) NOT NULL, 
	PRIMARY KEY(contract_account_number)
);
