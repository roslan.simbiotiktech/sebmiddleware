package com.seb.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.seb.encryption.Encryption;
import com.seb.object.Bill;
import com.seb.object.BillDetail;
import com.seb.object.Customer;
import com.seb.object.CustomerDetail;
import com.seb.util.DBUtil;
import com.seb.util.ErrorUtil;
import com.seb.util.FileUtil;
import com.seb.util.StringUtil;

public class SAPReader {

	private final static Logger log = (Logger) LoggerFactory.getLogger(SAPReader.class);
	
	private final static String RECORD_TYPE_HEADER = "0";
	private final static String RECORD_TYPE_DATA = "1";
	private final static String RECORD_TYPE_FOOTER = "9";
	
	private final static String RECORD_TYPE_DELIMITER = ",";
	
	private final static String FILE_TYPE_CUSTOMER_MASTER = "ccsc";
	private final static String FILE_TYPE_CUSTOMER_WEB_SUB = "websc";
	private final static String FILE_TYPE_CUSTOMER_WEB_UNSUB = "webuc";
	private final static String FILE_TYPE_BILL = "ccsb";
	
	private final static int ARG_ACTION = 0;
	private final static int ARG_FILENAME = 1;
	private final static int ARG_DB = 2;
	private final static int ARG_FROM = 3;
	private final static int ARG_TO = 4;
	private final static int ARG_ENCRYPT_TEXT = 1;
	
	private final static int ARG_EXPORT_DB = 1;
	private final static int ARG_EXPORT_PATH = 2;
	
	private final static String ARG_ACTION_ENCRYPT = "encrypt";
	private final static String ARG_ACTION_LOAD_FILE = "loadfile";
	private final static String ARG_ACTION_GEN_SUB_FILE = "gensubfile";
	
	private final static SimpleDateFormat sdfHeader = new SimpleDateFormat("yyyyMMdd");
	private final static SimpleDateFormat sdfFile = new SimpleDateFormat("yyMMdd");
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		log.info("Start SAPReader.....");
		
		int count = 0;
		
		for(String s : args){
			log.info("args [" + count + "] = " + s);
			count++;
		}
		
		process(args);
		
//		test();		
		
		log.info("End SAPReader!");
	}
	
	public static void test(){
		
		try{
			/*Path p = Paths.get("opt/oat/ccsb150806.csv");
			String file = p.getFileName().toString();
			System.out.println(p.toString());
			System.out.println(file);*/
			String abc = "a's b'stsed";
			System.out.println(StringUtil.formatSqlString(abc.trim()));
		}catch(Throwable ex){
			ex.printStackTrace();
			log.info("hi");
		}finally{
			//DBUtil.closeConnnection();
		}
		
	}
	
	public static void process(String[] args){
		String errMinArg = "Usage: action["+ARG_ACTION_ENCRYPT+"/"+ARG_ACTION_LOAD_FILE+"/"+ARG_ACTION_GEN_SUB_FILE+"]";
		
		if(args.length == 0)
			log.info(errMinArg);
		else if(args[ARG_ACTION].equalsIgnoreCase(ARG_ACTION_ENCRYPT)){
			if(args.length < 2){
				errMinArg = "Usage: encrypt xxxxx[any text]";
				log.info(errMinArg);
			}else{
				log.info(Encryption.encrypt(args[ARG_ENCRYPT_TEXT]));
			}
		} else if(args[ARG_ACTION].equalsIgnoreCase(ARG_ACTION_LOAD_FILE)){
			if(args.length < 2){
				errMinArg = "Usage: loadfile xxxxx[file name or file path, eg : abc.txt]";
				log.info(errMinArg);
			}else{
				processFile(args);
			}
		} else if(args[ARG_ACTION].equalsIgnoreCase(ARG_ACTION_GEN_SUB_FILE)){
			String exportPath = args[ARG_EXPORT_PATH];
			StringUtil.PROPERTIES_DB_PATH = args[ARG_EXPORT_DB];
			
			genSubFile(exportPath);
		}else{
			log.info(errMinArg);
		}
		
		DBUtil.closeConnnection();
	}
	
	private static void processFile(String[] args){
		String fileName = "";
		Path p = null;
		
		if(!StringUtil.isEmpty(args[ARG_FILENAME])){
			p = Paths.get(args[ARG_FILENAME]);
			fileName = p.getFileName().toString();
		}
		
		boolean isValidFile = isValidFile(p.toString());
		
		if(isValidFile){
			StringUtil.PROPERTIES_DB_PATH = args[ARG_DB];
			
			log.info("Start {} [{}]", "reading......", fileName);
			if(fileName.toLowerCase().startsWith(FILE_TYPE_CUSTOMER_MASTER) 
					|| fileName.toLowerCase().startsWith(FILE_TYPE_CUSTOMER_WEB_SUB)
					|| fileName.toLowerCase().startsWith(FILE_TYPE_CUSTOMER_WEB_UNSUB)){
				processCustomer(args);
			}else if(fileName.toLowerCase().startsWith(FILE_TYPE_BILL)){
				processBill(args);
			}else{
				log.info("Invalid file type '{}'", p.toString());
			}
			log.info("End {}", "reading!");
		}else
			log.info("File '{}' not exist!", p.toString());
	}
	
	private static void processCustomer(String[] args){
		
		int lineNumber = 1;
		String fileName = args[ARG_FILENAME];
		Scanner sapScanner = null;
		
		try{			
			log.info("Processing {} : {}", "Customer File", fileName);
			
			sapScanner = new Scanner(new File(fileName));
			
			Customer customer = new Customer();
			
			ArrayList<CustomerDetail> customerDetails = new ArrayList<CustomerDetail>();
			
			while (sapScanner.hasNextLine()) {
                String line = sapScanner.nextLine();
                String[] lineArray = line.split(RECORD_TYPE_DELIMITER);
                
                String recordType = lineArray[0];
                
                if(recordType.equalsIgnoreCase(RECORD_TYPE_HEADER)){
                	//HEADER LINE
                	String agencyName = lineArray[1].trim();
                	String processDate = lineArray[2].trim();
                	customer.setAgencyName(agencyName);
                	customer.setProcessDate(processDate);
                }
                
                if(recordType.equalsIgnoreCase(RECORD_TYPE_DATA)){
                	//DATA LINE
                	String conAccNo = lineArray[1].trim();
                	String conAccName = lineArray[2].trim().toUpperCase();
                	
                	CustomerDetail customerDetail = new CustomerDetail();
                	
                	customerDetail.setConAccNo(conAccNo);
                	customerDetail.setConAccName(conAccName);
                	customerDetails.add(customerDetail);
                }
                
                if(recordType.equalsIgnoreCase(RECORD_TYPE_FOOTER)){
                	//FOOTER LINE
                	int totalRecord = Integer.parseInt(lineArray[3].trim());
                	customer.setTotalRecord(totalRecord);
                }
                
//                log.info(line);
                lineNumber++;
            }
			
			customer.setCustomerDetails(customerDetails);
			
//			log.info("Data Proccessed: {}", customer.toString());
			
			DBUtil.saveAgency(customer.getAgencyName());
			DBUtil.saveContract(customer);
		}catch(SQLException ex){
			String errMsg = "Error insert contract data with database error!";
			log.error(errMsg, ex);
			ErrorUtil.sendErrorToServer(fileName, errMsg);
		}catch(Throwable ex){
			String errMsg = "Error Processing File [" + fileName + "] at line number = [" + lineNumber + "]";
			log.error(errMsg, ex);
			ErrorUtil.sendErrorToServer(fileName, errMsg);
		}finally{
			if(sapScanner != null)
				sapScanner.close();
		}
	}

	private static void processBill(String[] args){
		
		int lineNumber = 1;
		String fileName = args[ARG_FILENAME];
		Scanner sapScanner = null;
		try{
			
			log.info("Processing {} : {}", "Bill File", fileName);
			
			sapScanner = new Scanner(new File(fileName));
			
			Bill bill = new Bill();
			
			ArrayList<BillDetail> billDetails = new ArrayList<BillDetail>();
			
			while (sapScanner.hasNextLine()) {
                String line = sapScanner.nextLine();
                String[] lineArray = line.split(RECORD_TYPE_DELIMITER);
                
                String recordType = lineArray[0];
                
                if(recordType.equalsIgnoreCase(RECORD_TYPE_HEADER)){
                	//HEADER LINE
                	String agencyName = lineArray[1].trim();
                	String processDate = lineArray[2].trim();
                	bill.setAgencyName(agencyName);
                	bill.setProcessDate(StringUtil.getDateTime(processDate));
                }
                
                if(recordType.equalsIgnoreCase(RECORD_TYPE_DATA)){
                	//DATA LINE
                	String invNo = lineArray[1].trim();
                	String conAccNo = lineArray[2].trim();
                	String businessPartnerNo = lineArray[3].trim();
                	String invDate = lineArray[4].trim();
                	String dueDate = lineArray[5].trim();
                	double currentAmt = StringUtil.formatAmt(StringUtil.parseDouble(lineArray[6].trim()));
                	double currentTotalAmt = StringUtil.formatAmt(StringUtil.parseDouble(lineArray[7].trim()));
                	String billStartDate = lineArray[8].trim();
                	String billEndDate = lineArray[9].trim();
                	String pdfDocNo = lineArray[10].trim();
                	
                	BillDetail billDetail = new BillDetail();
                	
                	billDetail.setInvNo(invNo);
                	billDetail.setConAccNo(conAccNo);
                	billDetail.setBusinessPartnerNo(businessPartnerNo);
                	billDetail.setInvDate(StringUtil.getDateTime(invDate));
                	billDetail.setDueDate(StringUtil.getDateTime(dueDate));
                	billDetail.setCurrentAmt(currentAmt);
                	billDetail.setCurrentTotalAmt(currentTotalAmt);
                	billDetail.setBillStartDate(StringUtil.getDateTime(billStartDate));
                	billDetail.setBillEndDate(StringUtil.getDateTime(billEndDate));
                	billDetail.setPdfDocNo(pdfDocNo);
                	billDetails.add(billDetail);
                }
                
                if(recordType.equalsIgnoreCase(RECORD_TYPE_FOOTER)){
                	//FOOTER LINE
                	int totalRecord = Integer.parseInt(lineArray[3].trim());
                	double totalAmt = StringUtil.parseDouble(lineArray[4].trim());
                	bill.setTotalRecord(totalRecord);
                	bill.setTotalAmt(totalAmt);
                }
                
//                log.info(line);
                lineNumber++;
            }
			
			bill.setBillDetails(billDetails);
			
//			log.info("Data Proccessed: {}", bill.toString());
			
			DBUtil.saveAgency(bill.getAgencyName());
			DBUtil.saveBilling(bill.getAgencyName(), bill.getProcessDate());
			bill.setBillId(DBUtil.queryBillingId(bill.getAgencyName(), bill.getProcessDate()));
			DBUtil.saveBillingTransaction(bill);
			
			List<String> warnings = new ArrayList<String>();
			if(bill.getBillDetails() == null || bill.getBillDetails().isEmpty()){
				if(bill.getTotalRecord() != 0){
					warnings.add("[" + fileName + "] have " + bill.getTotalRecord() + " records defined in footer but no billing data rows were found");
				}				
			}else if(bill.getBillDetails().size() != bill.getTotalRecord()){
				warnings.add("[" + fileName + "] have " + bill.getTotalRecord() + " records defined in footer but there are a total of "+ bill.getBillDetails().size() +" billing data rows were found");
			}
			
			if(!warnings.isEmpty()){
				ErrorUtil.sendErrorToServer(fileName, warnings.toString());
			}
			
			String source = "";
			String target = "";
			
			if(!StringUtil.isEmpty(args[ARG_FROM])){
				source = args[ARG_FROM];
			}
			
			if(!StringUtil.isEmpty(args[ARG_TO])){
				target = args[ARG_TO];
			}
			
			FileUtil.moveContractPDF(bill.getBillDetails(), source, target);
		}catch(SQLException ex){
			String errMsg = "Error insert billing data with database error!";
			log.error(errMsg, ex);
			ErrorUtil.sendErrorToServer(fileName, errMsg);
		}catch(Throwable ex){
			String errMsg = "Error Processing File [" + fileName + "] at line number = [" + lineNumber + "]";
			log.error(errMsg, ex);
			ErrorUtil.sendErrorToServer(fileName, errMsg);
		}finally{
			if(sapScanner != null)
				sapScanner.close();
		}
	}
	
	private static boolean isValidFile(String fileName){
		boolean isValidFile = false;
		try{
			File file = new File(fileName);
			if(file.exists() && file.isFile())
				isValidFile = true;
		}catch(Exception ex){
			log.error("invalid file - " + ex.getMessage(), ex);
		}
		return isValidFile;
	}
	
	private static boolean genSubFile(String exportPath){
				
		String taskDateTime = DBUtil.queryTaskDateTime();
		ArrayList<CustomerDetail> customerDetails = DBUtil.queryNewSubAccountNumber(taskDateTime);
				
		String header = "0<AGENCY><DATE>";
		String content = "1<ACC_NO><ACC_NAME>";
		String footer = "9<AGENCY><DATE><TOTAL>";		
		
		Date currentDate = new Date();
		String proccessDate = sdfHeader.format(currentDate);
		String agencyName = "";
		
		StringBuffer subFileContent = new StringBuffer();
		
		boolean haveRecord = false;
		int count = 1;
		boolean needFooter = false;
		String newLine = System.getProperty("line.separator");
		
		if(customerDetails.size() > 0){
			haveRecord = true;
			log.info("Preparing export to file......!");
		}
		
		for(CustomerDetail customerDetail : customerDetails){
			if(!agencyName.equalsIgnoreCase(customerDetail.getAgencyName())){
				agencyName = customerDetail.getAgencyName();
				subFileContent.append(header.replace("<AGENCY>", customerDetail.getAgencyName()).replace("<DATE>", proccessDate));
				subFileContent.append(newLine);
				
				//this condition is for diff agency name
				if(count > 1)
					needFooter = true;
			}
			
			subFileContent.append(content.replace("<ACC_NO>", customerDetail.getConAccNo()).replace("<ACC_NAME>", customerDetail.getConAccName()));
			subFileContent.append(newLine);
			
			if(count == customerDetails.size() || needFooter){
				String totalNewSub = StringUtil.formatTotal((count+2) + "");
				
				subFileContent.append(footer.replace("<AGENCY>", customerDetail.getAgencyName()).replace("<DATE>", proccessDate).replace("<TOTAL>", totalNewSub));
				needFooter = false;
				count = 1; //new count for diff agency name
			}
			
			count++;
		}
		
		if(!StringUtil.isEmpty(exportPath) && !exportPath.endsWith(File.separator))
			exportPath += File.separator;
		
		String fileName = exportPath + FILE_TYPE_CUSTOMER_WEB_SUB + sdfFile.format(currentDate) + ".txt";
		
		try{
			
			File exportFolder = new File(exportPath);
			if (!exportFolder.exists() || !exportFolder.isDirectory()) {
				exportFolder.mkdirs();
			}
			
			File file = new File(fileName);

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			
			if(haveRecord){
				bw.write(subFileContent.toString());
			}else{
				StringBuffer sb = new StringBuffer();
				sb.append(header.replace("<AGENCY>", DBUtil.EXPORT_AGENCY_NAME).replace("<DATE>", proccessDate));
				sb.append(newLine);
				sb.append(footer.replace("<AGENCY>", DBUtil.EXPORT_AGENCY_NAME).replace("<DATE>", proccessDate).replace("<TOTAL>", "2"));
				bw.write(sb.toString());
			}			
			
			bw.close();
			
			log.info("Done export to file [{}]!" , file.getAbsolutePath().toString());
			
			DBUtil.updateTaskDateTime();
			
			
		}catch(Exception ex){
			log.error("Error write to File ["+fileName+"]", ex);
		}		
		
		return false;
	}
}
