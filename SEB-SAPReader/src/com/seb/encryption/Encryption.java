package com.seb.encryption;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import com.sun.org.apache.xml.internal.security.utils.Base64;

public class Encryption {

	private final static String SECRET_KEY = "wirelessthisandthatpower"; //minimum 24 chars
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Encrypting");
		System.out.println(encrypt("a"));
		System.out.println(decrypt("b"));
		System.out.println("0000123".trim());
	}

	public static String encrypt(String plainText){
		String encryptedText = plainText;
		try{
			Cipher cipher = getCipher();
			byte[] plainTextByte = plainText.getBytes();
			cipher.init(Cipher.ENCRYPT_MODE, getSecretKey());
			byte[] encryptedByte = cipher.doFinal(plainTextByte);
			encryptedText = Base64.encode(encryptedByte);
		}catch(Exception ex){
//			ex.printStackTrace();
		}
		
		return encryptedText;
	}

	public static String decrypt(String encryptedText){
		String decryptedText = encryptedText;
		try{
			Cipher cipher = getCipher();
			byte[] encryptedTextByte = Base64.decode(encryptedText);
			cipher.init(Cipher.DECRYPT_MODE, getSecretKey());
			byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
			decryptedText = new String(decryptedByte);
		}catch(Exception ex){
//			ex.printStackTrace();
		}
		return decryptedText;
	}
	
	public static Cipher getCipher() throws Exception{
		Cipher cipher;
		cipher = Cipher.getInstance("DESede");
		return cipher;
	}
	
	public static SecretKey getSecretKey(){
		SecretKey key = null;
		try{
			byte[] keyBytes = SECRET_KEY.getBytes("ASCII");
	        DESedeKeySpec keySpec = new DESedeKeySpec(keyBytes);
	        SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");
	        key = factory.generateSecret(keySpec);
		}catch(Exception ex){
//			ex.printStackTrace();
		}
		
        return key;
	}
}
