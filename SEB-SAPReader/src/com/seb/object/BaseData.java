package com.seb.object;

public class BaseData {
	private String agencyName;	
	private String processDate;	
	private int totalRecord;
	public String getAgencyName() {
		return agencyName;
	}
	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}
	public String getProcessDate() {
		return processDate;
	}
	public void setProcessDate(String processDate) {
		this.processDate = processDate;
	}
	public int getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(int totalRecord) {
		this.totalRecord = totalRecord;
	}
	@Override
	public String toString() {
		return "BaseData [agencyName=" + agencyName + ", processDate="
				+ processDate + ", totalRecord=" + totalRecord + "]";
	}
}
