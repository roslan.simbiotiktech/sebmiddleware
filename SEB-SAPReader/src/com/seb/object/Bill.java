package com.seb.object;

import java.util.ArrayList;

public class Bill extends BaseData{
	private ArrayList<BillDetail> billDetails = new ArrayList<BillDetail>();
	private double totalAmt;
	private int billId;
	
	public ArrayList<BillDetail> getBillDetails() {
		return billDetails;
	}
	public void setBillDetails(ArrayList<BillDetail> billDetails) {
		this.billDetails = billDetails;
	}
	public double getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(double totalAmt) {
		this.totalAmt = totalAmt;
	}
	public int getBillId() {
		return billId;
	}
	public void setBillId(int billId) {
		this.billId = billId;
	}
	@Override
	public String toString() {
		return "Bill [billDetails=" + billDetails + ", totalAmt=" + totalAmt
				+ ", toString()=" + super.toString() + "]";
	}
}
