package com.seb.object;

public class BillDetail {
	private String invNo;
	private String conAccNo;
	private String businessPartnerNo;
	private String invDate;
	private String dueDate;
	private double currentAmt;
	private double currentTotalAmt;
	private String pdfDocNo;
	private String billStartDate;
	private String billEndDate;
	
	public String getInvNo() {
		return invNo;
	}
	public void setInvNo(String invNo) {
		this.invNo = invNo;
	}
	public String getConAccNo() {
		return conAccNo;
	}
	public void setConAccNo(String conAccNo) {
		this.conAccNo = conAccNo;
	}
	public String getBusinessPartnerNo() {
		return businessPartnerNo;
	}
	public void setBusinessPartnerNo(String businessPartnerNo) {
		this.businessPartnerNo = businessPartnerNo;
	}
	public String getInvDate() {
		return invDate;
	}
	public void setInvDate(String invDate) {
		this.invDate = invDate;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public double getCurrentAmt() {
		return currentAmt;
	}
	public void setCurrentAmt(double currentAmt) {
		this.currentAmt = currentAmt;
	}
	public double getCurrentTotalAmt() {
		return currentTotalAmt;
	}
	public void setCurrentTotalAmt(double currentTotalAmt) {
		this.currentTotalAmt = currentTotalAmt;
	}
	public String getPdfDocNo() {
		return pdfDocNo;
	}
	public void setPdfDocNo(String pdfDocNo) {
		this.pdfDocNo = pdfDocNo;
	}	
	public String getBillStartDate() {
		return billStartDate;
	}
	public void setBillStartDate(String billStartDate) {
		this.billStartDate = billStartDate;
	}
	public String getBillEndDate() {
		return billEndDate;
	}
	public void setBillEndDate(String billEndDate) {
		this.billEndDate = billEndDate;
	}
	@Override
	public String toString() {
		return "BillDetail [invNo=" + invNo + ", conAccNo=" + conAccNo
				+ ", businessPartnerNo=" + businessPartnerNo + ", invDate="
				+ invDate + ", dueDate=" + dueDate + ", currentAmt="
				+ currentAmt + ", currentTotalAmt=" + currentTotalAmt
				+ ", pdfDocNo=" + pdfDocNo + ", billStartDate=" + billStartDate
				+ ", billEndDate=" + billEndDate + "]";
	}
}
