package com.seb.object;

import java.util.ArrayList;

public class Customer extends BaseData{
	private ArrayList<CustomerDetail> customerDetails = new ArrayList<CustomerDetail>();
	
	public ArrayList<CustomerDetail> getCustomerDetails() {
		return customerDetails;
	}
	public void setCustomerDetails(ArrayList<CustomerDetail> customerDetails) {
		this.customerDetails = customerDetails;
	}
	@Override
	public String toString() {
		return "Customer [customerDetails=" + customerDetails + ", toString()="
				+ super.toString() + "]";
	}
}
