package com.seb.object;

public class CustomerDetail {
	private String conAccNo;
	private String conAccName;
	private String agencyName;
	
	public String getConAccNo() {
		return conAccNo;
	}
	public void setConAccNo(String conAccNo) {
		this.conAccNo = conAccNo;
	}
	public String getConAccName() {
		return conAccName;
	}
	public void setConAccName(String conAccName) {
		this.conAccName = conAccName;
	}
	public String getAgencyName() {
		return agencyName;
	}
	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}
	@Override
	public String toString() {
		return "CustomerDetail [conAccNo=" + conAccNo + ", conAccName="
				+ conAccName + ", agencyName=" + agencyName + "]";
	}
}
