package com.seb.util;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.seb.encryption.Encryption;
import com.seb.object.Bill;
import com.seb.object.BillDetail;
import com.seb.object.Customer;
import com.seb.object.CustomerDetail;

public class DBUtil {
	
	private final static Logger log = (Logger) LoggerFactory.getLogger(DBUtil.class);
	
//	private final static String PROPERTIES_DB = "C:\\Workspace 7.1\\SAPReader\\dist\\db.properties";
	private final static String PROPERTIES_DB = "db.properties";
	
	private static String DB_SCHEMA = "";
	
	private final static String TABLE_AGENCY = "agency";
	private final static String TABLE_CONTRACT = "contract";
	private final static String TABLE_BILLING = "billing";
	private final static String TABLE_BILLING_TRANSACTION = "billing_transaction";
	private final static String TABLE_TASK_TRACKER = "task_tracker";
	private final static String TABLE_SUBSCRIPTION = "subscription";
	
	public final static String EXPORT_AGENCY_NAME = "SEBWEBC";
	
	private final static String SETTING_TASK_KEY_GEN_SUB_FILE = "GEN_SUB_FILE";
	
	private static Connection db2Conn = null;
	
	static {
		try {
			if(db2Conn == null || db2Conn.isClosed()){
				db2Conn = getConnection();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private static Connection getConnection(){
		Properties props;
		Connection Db2Conn = null;
        
        try{
        	props = new Properties();
            props.load(new FileInputStream(StringUtil.PROPERTIES_DB_PATH + File.separator + PROPERTIES_DB));
            
            String host = Encryption.decrypt(props.getProperty("host").trim());
            String port = Encryption.decrypt(props.getProperty("port").trim());
            String dbName = Encryption.decrypt(props.getProperty("dbName").trim());
            String userName = Encryption.decrypt(props.getProperty("userName").trim());
            String userPassword = Encryption.decrypt(props.getProperty("userPassword").trim());
            DB_SCHEMA = Encryption.decrypt(props.getProperty("dbSchema").trim());
            
            if(!StringUtil.isEmpty(DB_SCHEMA))
            	DB_SCHEMA += ".";
            
         // Database connection string
            String DRIVER = "com.ibm.db2.jcc.DB2Driver";        
            String URL = "jdbc:db2://" + host + ":" + port + "/" + dbName;

            // Connect to Database                                         
            Class.forName(DRIVER);                                       
            Db2Conn = DriverManager.getConnection(URL,userName,userPassword);
        }catch(Exception ex){
        	log.error("Error Connect To DB2", ex);
        }
        
		return Db2Conn;
	}
	
	public static void closeConnnection(){
		try{
			if(db2Conn != null && !db2Conn.isClosed()){
				db2Conn.close();
			}
		}catch(Exception ex){
			
		}
	}
	
	public static void saveContract(Customer customer) throws SQLException{
		
		log.info("Preparing to insert customer records........");
		
        PreparedStatement Db2Stmt = null;
        String sql = null;
		
//		try{
			// Db2 SQL statement to insert/update data 
	        sql = "MERGE INTO "+ DB_SCHEMA + TABLE_CONTRACT + " AS c USING (SELECT * FROM TABLE (VALUES {RECORDS} ) ) AS vt(CONTRACT_ACCOUNT_NUMBER, CONTRACT_ACCOUNT_NAME, AGENCY_NAME) ON (c.CONTRACT_ACCOUNT_NUMBER = vt.CONTRACT_ACCOUNT_NUMBER) WHEN MATCHED THEN UPDATE SET c.CONTRACT_ACCOUNT_NAME = vt.CONTRACT_ACCOUNT_NAME, c.AGENCY_NAME = vt.AGENCY_NAME WHEN NOT MATCHED THEN INSERT (CONTRACT_ACCOUNT_NUMBER, CONTRACT_ACCOUNT_NAME, AGENCY_NAME) VALUES (vt.CONTRACT_ACCOUNT_NUMBER, vt.CONTRACT_ACCOUNT_NAME, vt.AGENCY_NAME)";
	        
	        StringBuffer records = new StringBuffer();
	        
	        for(CustomerDetail customerDetail : customer.getCustomerDetails()){
	        	records.append("('" + customerDetail.getConAccNo() + "', '" + StringUtil.formatSqlString(customerDetail.getConAccName()) + "', '" + customer.getAgencyName() + "'),");
	        }
	        
	        String recordsValue = records.toString();
	        
	        if(!StringUtil.isEmpty(recordsValue)){
	        	sql = sql.replace("{RECORDS}", recordsValue.substring(0, recordsValue.length() - 1 ));
	        		        	
	        	Db2Stmt = db2Conn.prepareStatement(sql);
	        	
	        	Db2Stmt.executeUpdate();
	        	
	        	log.info("Done insert customer records, total {} !", customer.getCustomerDetails().size());
	        }else
	        	log.info("No customer record to insert");
	        
		/*}catch(Exception ex){
			log.error("Error insert customer record into database", ex);
		}*/
	}
	
	public static void saveAgency(String agencyName) throws SQLException{
		
		log.info("Preparing to insert Agency records........");
		
        PreparedStatement Db2Stmt = null;
        String sql = null;
		
		//try{
			// Db2 SQL statement to insert/update data 
	        sql = "MERGE INTO "+ DB_SCHEMA + TABLE_AGENCY + " AS c USING ( SELECT * FROM TABLE ( VALUES {RECORDS} ) ) AS vt(AGENCY_NAME) ON (c.AGENCY_NAME = vt.AGENCY_NAME) WHEN NOT MATCHED THEN INSERT (AGENCY_NAME) VALUES (vt.AGENCY_NAME)";
	        
	        StringBuffer records = new StringBuffer();
	        
	        records.append("('" + agencyName + "')");
	        
	        String recordsValue = records.toString();
	        
	        if(!StringUtil.isEmpty(recordsValue)){
	        	sql = sql.replace("{RECORDS}", recordsValue);
	        	
	        	Db2Stmt = db2Conn.prepareStatement(sql);
	        	
	        	Db2Stmt.executeUpdate();
	        	
	        	log.info("Done insert agency record!");
	        }else
	        	log.info("No agency record to insert!");
	        
	        
		/*}catch(Exception ex){
			log.error("Error insert agency record into database", ex);
		}*/
	}
	
	public static int saveBilling(String agencyName, String processDate) throws SQLException{
		
		int billId = -1;
		
		log.info("Preparing to insert billing records........");
		
        PreparedStatement Db2Stmt = null;
        String sql = null;
		
		//try{
			// Db2 SQL statement to insert/update data 
	        sql = "MERGE INTO "+ DB_SCHEMA + TABLE_BILLING + " AS c USING ( SELECT * FROM TABLE ( VALUES {RECORDS} ) ) AS vt(AGENCY_NAME, PROCESS_DATE) ON (c.AGENCY_NAME = vt.AGENCY_NAME AND c.PROCESS_DATE = vt.PROCESS_DATE) WHEN NOT MATCHED THEN INSERT (AGENCY_NAME, PROCESS_DATE) VALUES (vt.AGENCY_NAME, vt.PROCESS_DATE)";
	        
	        StringBuffer records = new StringBuffer();
	        
	        records.append("('" + agencyName + "', '" + processDate + "')");
	        
	        String recordsValue = records.toString();
	        
	        if(!StringUtil.isEmpty(recordsValue)){
	        	sql = sql.replace("{RECORDS}", recordsValue);
	        	
//	        	String key[] = {"billing_id"};
//	        	log.info(sql);
//	        	Db2Stmt = Db2Conn.prepareStatement(sql, key);
	        	Db2Stmt = db2Conn.prepareStatement(sql);
	        	
	        	Db2Stmt.executeUpdate();
	        	
	        	log.info("Done insert billing record!");
	        	
	        	/*ResultSet rs = Db2Stmt.getGeneratedKeys();
		        if (rs.next()) {
		          billId = rs.getInt(1);
		          log.info("billId = {}", billId);
		        }*/
	        }else
	        	log.info("No billing record to insert!");
	        
	        
		/*}catch(Exception ex){
			log.error("Error insert billing record into database", ex);
		}*/

		return billId;
	}
	
	public static void saveBillingTransaction(Bill bill) throws SQLException{
		
		log.info("Preparing to insert bill transaction records........");
		
		int batchSize = 1000;
		
		if(bill.getBillDetails() != null && !bill.getBillDetails().isEmpty()){
			
			Iterator<BillDetail> iterate = bill.getBillDetails().iterator();
			
			List<BillDetail> details = new ArrayList<BillDetail>();
			
			while(iterate.hasNext()){
				
				BillDetail next = iterate.next();
				details.add(next);
				
				if(details.size() >= batchSize){
					saveBillingTransaction(bill.getBillId(), details);
					details.clear();
				}
			}
			
			if(!details.isEmpty()){
				saveBillingTransaction(bill.getBillId(), details);
			}
			
			log.info("Done insert bill transaction records, total {} !", bill.getBillDetails().size());
		}else{
			log.info("No bill transaction record to insert");
		}
	}
	
	private static void saveBillingTransaction(int billId, List<BillDetail> bills) throws SQLException {
		
		// Db2 SQL statement to insert/update data 
        String sql = "MERGE INTO "+ DB_SCHEMA + TABLE_BILLING_TRANSACTION + 
	        		" AS c USING ( SELECT * FROM TABLE ( VALUES {RECORDS} ) ) AS vt(BILLING_ID, INVOICE_NUMBER, CONTRACT_ACCOUNT_NUMBER, BUSINESS_PARTNER_NUMBER, INVOICE_DATE, DUE_DATE, CURRENT_AMOUNT, CURRENT_TOTAL_AMOUNT, BILL_START_DATE, BILL_END_DATE, PDF_PRINT_DOCUMENT_NUMBER) ON (c.BILLING_ID = vt.BILLING_ID and c.INVOICE_NUMBER = vt.INVOICE_NUMBER) WHEN MATCHED THEN UPDATE SET c.CONTRACT_ACCOUNT_NUMBER = vt.CONTRACT_ACCOUNT_NUMBER, c.BUSINESS_PARTNER_NUMBER = vt.BUSINESS_PARTNER_NUMBER, c.INVOICE_DATE = vt.INVOICE_DATE, c.DUE_DATE = vt.DUE_DATE, c.CURRENT_AMOUNT = vt.CURRENT_AMOUNT, c.BILL_START_DATE = vt.BILL_START_DATE, c.BILL_END_DATE = vt.BILL_END_DATE,c.CURRENT_TOTAL_AMOUNT = vt.CURRENT_TOTAL_AMOUNT, c.PDF_PRINT_DOCUMENT_NUMBER = vt.PDF_PRINT_DOCUMENT_NUMBER WHEN NOT MATCHED THEN INSERT (BILLING_ID, INVOICE_NUMBER, CONTRACT_ACCOUNT_NUMBER, BUSINESS_PARTNER_NUMBER, INVOICE_DATE, DUE_DATE, CURRENT_AMOUNT, CURRENT_TOTAL_AMOUNT, BILL_START_DATE, BILL_END_DATE, PDF_PRINT_DOCUMENT_NUMBER) VALUES (vt.BILLING_ID, vt.INVOICE_NUMBER, vt.CONTRACT_ACCOUNT_NUMBER, vt.BUSINESS_PARTNER_NUMBER, vt.INVOICE_DATE, vt.DUE_DATE, vt.CURRENT_AMOUNT, vt.CURRENT_TOTAL_AMOUNT, vt.BILL_START_DATE, vt.BILL_END_DATE, vt.PDF_PRINT_DOCUMENT_NUMBER)";
	        
        StringBuffer records = new StringBuffer();
        
        for(BillDetail billDetail : bills){
        	records.append(
        			"('" + billId + "', '" + billDetail.getInvNo() + 
        			"', '" + billDetail.getConAccNo() + "', '" + billDetail.getBusinessPartnerNo() + 
        			"', '" + billDetail.getInvDate() + "', '" + billDetail.getDueDate() + 
        			"', '" + billDetail.getCurrentAmt() + "', '" + billDetail.getCurrentTotalAmt() + 
        			"', '" + billDetail.getBillStartDate() + "', '" + billDetail.getBillEndDate() + 
        			"', '" + billDetail.getPdfDocNo() + "'),");
        }
        
        String recordsValue = records.toString();
        
        sql = sql.replace("{RECORDS}", recordsValue.substring(0, recordsValue.length() - 1 ));
    	PreparedStatement Db2Stmt = db2Conn.prepareStatement(sql);
    	
    	Db2Stmt.executeUpdate();
    	
    	log.info("Inserted bill transaction records, total {}", bills.size());
	}
	
	public static int queryBillingId(String agencyName, String processedDate){
		int billId = -1;
		
		log.info("Preparing to query billing records........");
		
        PreparedStatement Db2Stmt = null;
        String sql = null;
		
		try{
			// Db2 SQL statement to insert/update data 
	        sql = "SELECT BILLING_ID FROM "+ DB_SCHEMA + TABLE_BILLING + " WHERE AGENCY_NAME = ? AND PROCESS_DATE = ?";
	        
	        Db2Stmt = db2Conn.prepareStatement(sql);
        	
        	Db2Stmt.setString(1,agencyName);
        	Db2Stmt.setString(2,processedDate);
        	
        	ResultSet rs = Db2Stmt.executeQuery();
        	while (rs.next()) {
        		billId = rs.getInt("BILLING_ID");
        	}
        	
        	log.info("Done query billing records!");	        
	        
		}catch(Exception ex){
			log.error("Error query billing records from database", ex);
		}
		
		return billId;
	}
	
	public static String queryTaskDateTime(){
		String dateTime = "";
		
		log.info("Preparing to query task date time........");
		
        PreparedStatement Db2Stmt = null;
        String sql = null;
		
		try{
			// Db2 SQL statement to insert/update data 
	        sql = "SELECT TASK_DATETIME FROM "+ DB_SCHEMA + TABLE_TASK_TRACKER + " WHERE TASK_KEY = ?";
	        
	        Db2Stmt = db2Conn.prepareStatement(sql);
        	
        	Db2Stmt.setString(1,SETTING_TASK_KEY_GEN_SUB_FILE);
        	
        	ResultSet rs = Db2Stmt.executeQuery();
        	while (rs.next()) {
        		dateTime = rs.getString("TASK_DATETIME");
        	}
        	
        	log.info("Done query task date time records!");	        
	        
		}catch(Exception ex){
			log.error("Error query task date time records from database", ex);
		}
		return dateTime;
	}
	
	public static ArrayList<CustomerDetail> queryNewSubAccountNumber(String taskDateTime){
		ArrayList<CustomerDetail> customerDetails = new ArrayList<CustomerDetail>();
		
		log.info("Preparing to query new sub account number........");
		
        PreparedStatement Db2Stmt = null;
        String sql = null;
		
		try{
			// Db2 SQL statement to insert/update data 
	        sql = "SELECT SUB.CONTRACT_ACCOUNT_NUMBER,CON.CONTRACT_ACCOUNT_NAME,CON.AGENCY_NAME FROM "+ DB_SCHEMA + TABLE_SUBSCRIPTION + " SUB JOIN "+ DB_SCHEMA + TABLE_CONTRACT + " CON ON SUB.CONTRACT_ACCOUNT_NUMBER = CON.CONTRACT_ACCOUNT_NUMBER GROUP BY SUB.CONTRACT_ACCOUNT_NUMBER,CON.CONTRACT_ACCOUNT_NAME,CON.AGENCY_NAME HAVING MIN(SUB.CREATED_DATETIME) > ?";
	        
	        Db2Stmt = db2Conn.prepareStatement(sql);
        	
        	Db2Stmt.setString(1,taskDateTime);
        	
        	ResultSet rs = Db2Stmt.executeQuery();
        	while (rs.next()) {
        		
        		CustomerDetail customerDetail = new CustomerDetail();
        		customerDetail.setConAccName(rs.getString("CONTRACT_ACCOUNT_NAME"));
        		customerDetail.setConAccNo(rs.getString("CONTRACT_ACCOUNT_NUMBER"));
        		customerDetail.setAgencyName(EXPORT_AGENCY_NAME);
        		
        		customerDetails.add(customerDetail);
        	}
        	
        	log.info("Done query new sub account number records!");	        
	        
		}catch(Exception ex){
//			log.error("Error query new sub account number from database", ex);
			
			String errMsg = "Error query recent subscription account number from database with database error!";
			log.error(errMsg, ex);
			ErrorUtil.sendErrorToServer("Export file", errMsg);
		}

		return customerDetails;
	}
	
	public static void updateTaskDateTime(){
		
		log.info("Preparing to update task date time........");
		
        PreparedStatement Db2Stmt = null;
        String sql = null;
		
		try{
			// Db2 SQL statement to insert/update data 
			sql = "update "+ DB_SCHEMA + TABLE_TASK_TRACKER + " set TASK_DATETIME = CURRENT TIMESTAMP where TASK_KEY = ?";
	        
	        Db2Stmt = db2Conn.prepareStatement(sql);
        	Db2Stmt.setString(1,SETTING_TASK_KEY_GEN_SUB_FILE);
        	Db2Stmt.executeUpdate();
        	
        	log.info("Done update task date time !");
	        
		}catch(Exception ex){
			log.error("Error update task date time record", ex);
		}
	}
}
