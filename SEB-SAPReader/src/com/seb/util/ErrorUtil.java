package com.seb.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ErrorUtil {
	
	private final static Logger log = (Logger) LoggerFactory.getLogger(ErrorUtil.class);
	
	public static void sendErrorToServer(String fileName, String details){

		try{
			String url = "http://localhost:8080/SEB-Middleware/api/sap_file_process_warning";
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			//add reuqest header
			con.setRequestMethod("POST");
			/*con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");*/

			String urlParameters = "{'DATE' : '" + StringUtil.getDateStandard() + "', 'FILE_NAME' : '" + fileName + "', 'DETAILS' : '" + details + "'}";
			
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			log.info("\nSending Error Warning to : " + url);
			log.info("Post parameters : " + urlParameters);
			log.info("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			
			//print result
			log.info(response.toString());
		}catch(Throwable th){
			log.error("Error sending to server", th);
		}
		
	}
	
}
