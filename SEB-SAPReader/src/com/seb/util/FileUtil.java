package com.seb.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.seb.object.BillDetail;

public class FileUtil {
	
	private final static Logger log = (Logger) LoggerFactory.getLogger(FileUtil.class);
	
	public static final String PREFIX_TARGET_DIRECTORY_PATH = "to/";
	public static final String PREFIX_SOURCE_DIRECTORY_PATH = "from/";
	private static final String DELIMITER = File.separator;

	public static void moveContractPDF(ArrayList<BillDetail> billingTransactions){
		moveContractPDF(billingTransactions, PREFIX_SOURCE_DIRECTORY_PATH, PREFIX_TARGET_DIRECTORY_PATH);
	}
	
	public static void moveContractPDF(ArrayList<BillDetail> billingTransactions, String prefixSourcePath, String prefixTargetPath){
		ArrayList<String> paths = new ArrayList<String>();
		
		ArrayList<BillDetail> missingPDFs = new ArrayList<BillDetail>();
		
		for(BillDetail billingTransaction : billingTransactions){
			
			String targetPath = "";
			
			try {
				String sourcePath = prefixSourcePath + billingTransaction.getPdfDocNo();
				
				if(!isValidFile(sourcePath)){
					missingPDFs.add(billingTransaction);					
					continue;
				}
				
				StringBuffer sb = new StringBuffer();
				sb.append(prefixTargetPath);
				sb.append(StringUtil.getDateYear(billingTransaction.getInvDate()));
				sb.append(DELIMITER);
				sb.append(billingTransaction.getConAccNo());
				
				String folderPath = sb.toString();

				sb.append(DELIMITER);
				sb.append(billingTransaction.getPdfDocNo());
				
				targetPath = sb.toString();
				
				if(!paths.contains(folderPath)){
					paths.add(folderPath);

					File pdfFolder = new File(folderPath);
					if (!pdfFolder.exists() || !pdfFolder.isDirectory()) {
						pdfFolder.mkdirs();
					}
				}
				
				Path source = Paths.get(sourcePath);
			    Path target = Paths.get(targetPath);
			    
			    CopyOption[] options = new CopyOption[]{
			      StandardCopyOption.REPLACE_EXISTING,
			      StandardCopyOption.COPY_ATTRIBUTES
			    }; 
			    Files.copy(source, target, options);
				
			} catch (ParseException e) {
				log.error("Error Invoice Date For File [" + targetPath + "]", e);
			} catch (IOException e) {
				log.error("Error Copying File [" + targetPath + "]", e);
			}
		}
		
		for(BillDetail billDetail : missingPDFs){
			log.info("Missing pdf file [" + billDetail.getPdfDocNo() +"] for invoice number [" + billDetail.getInvNo() + "]" );
		}
	}
	
	private static boolean isValidFile(String fileName){
		boolean isValidFile = false;
		try{
			File file = new File(fileName);
			if(file.exists() && file.isFile())
				isValidFile = true;
		}catch(Exception ex){
			
		}
		return isValidFile;
	}
}
