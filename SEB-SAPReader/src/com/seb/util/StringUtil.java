package com.seb.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StringUtil {
	
	public static String PROPERTIES_DB_PATH = "";
	
	public static double parseDouble(String value){
		return Double.parseDouble(value);
	}
	
	public static double parseInt(String value){
		return Integer.parseInt(value);
	}
	
	public static boolean isEmpty(String value){
		if(value != null && value.length() > 0)
			return false;
		return true;
	}
	
	public static long parseLong(String value){
		try{
			return Double.valueOf(value).longValue();
		}catch(Exception ex){
			return 0;
		}
	}
	
	public static String exponentialToString(String value){
		return parseLong(value) + "";
	}
	
	public static String getDateTime(String value) throws ParseException{
		SimpleDateFormat oriDf = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = oriDf.parse(value);
		String dateString = df.format(date);
		return dateString;
	}
	
	public static String getDateStandard() throws ParseException{
		SimpleDateFormat oriDf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = oriDf.format(new Date());
		return dateString;
	}
	
	public static String getDateYear(String value) throws ParseException{
		SimpleDateFormat oriDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat df = new SimpleDateFormat("yyyy");
		Date date = oriDf.parse(value);
		String dateString = df.format(date);
		return dateString;
	}
	
	public static String formatTotal(String value){
		int blockSize = 5;
		
		int diff = blockSize - value.length();

		for(int i = 0; i < diff; i++){
			value = "0" + value;
		}		
		
		return value;
	}
	
	public static double formatAmt(double amt){
		return amt / 100;
	}
	
	public static String formatSqlString(String value){		
		
		value = value.replace("'", "''");
		
		return value;
	}
	
	public static void main(String [] args){
		try {
			System.out.println(getDateTime(""));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
